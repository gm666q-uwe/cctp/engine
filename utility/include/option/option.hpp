#pragma once

#include <functional>
#include <optional>

namespace option {
template<typename T>
class Option : public std::optional<T>
{
public:
  constexpr Option(Option<T> const& other) noexcept
    : std::optional<T>(other)
  {}

  constexpr Option(Option<T>&& other) noexcept
    : std::optional<T>(std::forward<Option<T>>(other))
  {}

  constexpr auto operator=(Option<T> const& other) & noexcept -> Option<T>&
  {
    std::optional<T>::operator=(other);
    return *this;
  }

  constexpr auto operator=(Option<T>&& other) & noexcept -> Option<T>&
  {
    std::optional<T>::operator=(std::forward<Option<T>>(other));
    return *this;
  }

  static constexpr auto None() noexcept -> Option<T> { return std::move(Option()); }

  static constexpr auto Some(T const& some) noexcept -> Option<T> { return std::move(Option(some)); }

  static constexpr auto Some(T&& some) noexcept -> Option<T> { return std::move(Option(std::forward<T>(some))); }

  auto as_mut() & noexcept -> Option<T&> { return std::move(is_some() ? Option(this->value()) : Option()); }

  constexpr auto as_ref() const& noexcept -> Option<T const&>
  {
    return std::move(is_some() ? Option(this->value()) : Option());
  }

  [[nodiscard]] constexpr auto is_none() const& noexcept -> bool { return !this->has_value(); }

  [[nodiscard]] constexpr auto is_some() const& noexcept -> bool { return this->has_value(); }

  auto unwrap() && noexcept -> T
  {
    if (is_some()) {
      return std::move(std::move(*this).value());
    } else {
      std::abort();
    }
  }

  auto unwrap_or(T&& default_) && noexcept -> T { return std::move(is_some() ? std::move(*this).value() : default_); }

  auto unwrap_or_else(std::function<T()> f) && noexcept -> T
  {
    return std::move(is_some() ? std::move(*this).value() : f());
  }

protected:
private:
  constexpr Option() noexcept
    : std::optional<T>()
  {}

  constexpr explicit Option(T const& value)
    : std::optional<T>(value)
  {}

  constexpr explicit Option(T&& value)
    : std::optional<T>(std::forward<T>(value))
  {}
};
}

#pragma once

#include <variant>

#include "option/option.hpp"

namespace result {
template<typename T, typename E>
class Result : public std::variant<T, E>
{
public:
  constexpr Result() noexcept = delete;

  constexpr Result(Result<T, E> const& other) noexcept
    : std::variant<T, E>(other)
  {}

  constexpr Result(Result<T, E>&& other) noexcept
    : std::variant<T, E>(std::forward<Result<T, E>>(other))
  {}

  constexpr auto operator=(Result<T, E> const& other) & noexcept -> Result<T, E>&
  {
    std::variant<T, E>::operator=(other);
    return *this;
  }

  constexpr auto operator=(Result<T, E>&& other) & noexcept -> Result<T, E>&
  {
    std::variant<T, E>::operator=(std::forward<Result<T, E>>(other));
    return *this;
  }

  static constexpr auto Err(E const& err) noexcept -> Result<T, E>
  {
    return std::move(Result(std::in_place_index<1>, err));
  }

  static constexpr auto Err(E&& err) noexcept -> Result<T, E>
  {
    return std::move(Result(std::in_place_index<1>, std::forward<E>(err)));
  }

  static constexpr auto Ok(T const& ok) noexcept -> Result<T, E>
  {
    return std::move(Result(std::in_place_index<0>, ok));
  }

  static constexpr auto Ok(T&& ok) noexcept -> Result<T, E>
  {
    return std::move(Result(std::in_place_index<0>, std::forward<T>(ok)));
  }

  auto as_mut() & noexcept -> Result<T&, E&>
  {
    return std::move(Result(std::in_place_index<this->index()>, std::get<this->index()>(*this)));
  }

  [[nodiscard]] constexpr auto as_ref() const& noexcept -> Result<T const&, E const&>
  {
    return std::move(Result(std::in_place_index<this->index()>, std::get<this->index()>(*this)));
  }

  auto err() && noexcept -> option::Option<E>
  {
    return std::move(is_err() ? option::Option<E>::Some(std::move(std::get<1>(std::move(*this))))
			      : option::Option<E>::None());
  }

  [[nodiscard]] constexpr auto is_err() const& noexcept -> bool { return this->index() == 1; }

  [[nodiscard]] constexpr auto is_ok() const& noexcept -> bool { return this->index() == 0; }

  auto ok() && noexcept -> option::Option<T>
  {
    return std::move(is_ok() ? option::Option<T>::Some(std::move(std::get<0>(std::move(*this))))
			     : option::Option<T>::None());
  }

  auto unwrap() && noexcept -> T
  {
    if (is_ok()) {
      return std::move(std::get<0>(std::move(*this)));
    } else {
      std::abort();
    }
  }

  auto unwrap_err() && noexcept -> E
  {
    if (is_err()) {
      return std::move(std::get<1>(std::move(*this)));
    } else {
      std::abort();
    }
  }

  auto unwrap_or(T&& default_) && noexcept -> T
  {
    return std::move(is_ok() ? std::get<0>(std::move(*this)) : default_);
  }

  auto unwrap_or_else(std::function<T(E&&)> op) && noexcept -> T
  {
    return std::move(is_ok() ? std::get<0>(std::move(*this)) : op(std::move(std::get<1>(std::move(*this)))));
  }

protected:
private:
  template<size_t Np, typename... Args>
  constexpr explicit Result(std::in_place_index_t<Np>, Args&&... args)
    : std::variant<T, E>(std::in_place_index<Np>, std::forward<Args>(args)...)
  {}
};
} // namespace project1::result

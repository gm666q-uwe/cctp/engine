cmake_minimum_required(VERSION 3.19)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
find_package(ECM 5.74.0 NO_MODULE)
if (ECM_FOUND)
    list(APPEND CMAKE_MODULE_PATH ${ECM_MODULE_PATH})
endif ()

project(Engine VERSION 0.1.0.0 DESCRIPTION "Workspace" LANGUAGES C CXX)

set(ENGINE_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
set(ENGINE_VERSION_MINOR ${PROJECT_VERSION_MINOR})
set(ENGINE_VERSION_PATCH ${PROJECT_VERSION_PATCH})
set(ENGINE_VERSION_TWEAK ${PROJECT_VERSION_TWEAK})

if (CMAKE_SYSTEM_NAME STREQUAL Linux)
    message(STATUS "Platform detection: Linux")

    set(ENGINE_IS_LINUX TRUE)
    set(ENGINE_IS_UNIX TRUE)
elseif (CMAKE_SYSTEM_NAME STREQUAL Windows)
    message(STATUS "Platform detection: Windows")

    set(ENGINE_IS_WINDOWS TRUE)
else ()
    message(STATUS "Platform detection: Unknown")
    message(FATAL_ERROR "Unsupported platform")
endif ()

find_package(Boost REQUIRED COMPONENTS)
find_package(glm CONFIG REQUIRED)
find_package(OpenGL COMPONENTS OpenGL OPTIONAL_COMPONENTS GLX EGL)
find_package(X11 COMPONENTS xkbcommon)
find_package(Vulkan REQUIRED COMPONENTS glslc OPTIONAL_COMPONENTS Vulkan)
if (ECM_FOUND)
    find_package(Wayland COMPONENTS Client Cursor Egl)
    find_package(WaylandProtocols 1.15)
    find_package(WaylandScanner)
endif ()

if (OpenGL_OpenGL_FOUND)
    set(ENGINE_HAS_GL TRUE)
endif ()

if (OpenGL_GLX_FOUND)
    set(ENGINE_HAS_GLX TRUE)
endif ()

if (OpenGL_EGL_FOUND)
    set(ENGINE_HAS_EGL TRUE)
endif ()

if (ENGINE_IS_WINDOWS)
    set(ENGINE_HAS_WGL TRUE)
endif ()

if (Vulkan_FOUND)
    set(ENGINE_HAS_VULKAN TRUE)
endif ()

if (Wayland_Client_FOUND AND Wayland_Cursor_FOUND AND Wayland_Egl_FOUND AND WaylandProtocols_FOUND AND WaylandScanner_FOUND AND X11_xkbcommon_FOUND)
    set(ENGINE_HAS_WAYLAND TRUE)
endif ()

if (X11_FOUND AND X11_xkbcommon_FOUND)
    set(ENGINE_HAS_XLIB TRUE)
endif ()

function(glslc_compile INPUT OUTPUT TARGET)
    cmake_path(GET OUTPUT FILENAME OUTPUT_FILE)
    add_custom_command(OUTPUT ${OUTPUT} COMMAND Vulkan::glslc ARGS -c --target-env=${TARGET} -O0 -mfmt=c -o ${OUTPUT} ${INPUT} MAIN_DEPENDENCY ${INPUT} COMMENT "Building GLSL shader ${OUTPUT_FILE}")
endfunction()

add_subdirectory(engine)
add_subdirectory(perlin_noise)
add_subdirectory(utility)

cmake_minimum_required(VERSION 3.19)

add_subdirectory(common)
add_subdirectory(core)
add_subdirectory(display)
add_subdirectory(graphics)
add_subdirectory(input)
add_subdirectory(sandbox)

#include "engine/input/device.hpp"

engine::input::Device::Device(engine::input::Device::Type type) noexcept
  : m_type(type)
{}

auto
engine::input::Device::has_event() const& noexcept -> bool
{
  return !m_events.empty();
}

auto
engine::input::Device::pop_event() & noexcept -> engine::input::Event
{
  auto event = m_events.front();
  m_events.pop();
  return std::move(event);
}

#include "engine/input/key.hpp"

/*constexpr*/ auto
engine::input::operator<<(std::ostream& os, engine::input::Key key) noexcept -> std::ostream&
{
  using namespace engine::input;
  switch (key) {
    case Key::KeyReserved:
      os << "KeyReserved";
      break;
    case Key::KeyEscape:
      os << "KeyEscape";
      break;
    case Key::Key1:
      os << "Key1";
      break;
    case Key::Key2:
      os << "Key2";
      break;
    case Key::Key3:
      os << "Key3";
      break;
    case Key::Key4:
      os << "Key4";
      break;
    case Key::Key5:
      os << "Key5";
      break;
    case Key::Key6:
      os << "Key6";
      break;
    case Key::Key7:
      os << "Key7";
      break;
    case Key::Key8:
      os << "Key8";
      break;
    case Key::Key9:
      os << "Key9";
      break;
    case Key::Key0:
      os << "Key0";
      break;
    case Key::KeyMinus:
      os << "KeyMinus";
      break;
    case Key::KeyEqual:
      os << "KeyEqual";
      break;
    case Key::KeyBackspace:
      os << "KeyBackspace";
      break;
    case Key::KeyTab:
      os << "KeyTab";
      break;
    case Key::KeyQ:
      os << "KeyQ";
      break;
    case Key::KeyW:
      os << "KeyW";
      break;
    case Key::KeyE:
      os << "KeyE";
      break;
    case Key::KeyR:
      os << "KeyR";
      break;
    case Key::KeyT:
      os << "KeyT";
      break;
    case Key::KeyY:
      os << "KeyY";
      break;
    case Key::KeyU:
      os << "KeyU";
      break;
    case Key::KeyI:
      os << "KeyI";
      break;
    case Key::KeyO:
      os << "KeyO";
      break;
    case Key::KeyP:
      os << "KeyP";
      break;
    case Key::KeyLeftBrace:
      os << "KeyLeftBrace";
      break;
    case Key::KeyRightBrace:
      os << "KeyRightBrace";
      break;
    case Key::KeyEnter:
      os << "KeyEnter";
      break;
    case Key::KeyLeftControl:
      os << "KeyLeftControl";
      break;
    case Key::KeyA:
      os << "KeyA";
      break;
    case Key::KeyS:
      os << "KeyS";
      break;
    case Key::KeyD:
      os << "KeyD";
      break;
    case Key::KeyF:
      os << "KeyF";
      break;
    case Key::KeyG:
      os << "KeyG";
      break;
    case Key::KeyH:
      os << "KeyH";
      break;
    case Key::KeyJ:
      os << "KeyJ";
      break;
    case Key::KeyK:
      os << "KeyK";
      break;
    case Key::KeyL:
      os << "KeyL";
      break;
    case Key::KeySemicolon:
      os << "KeySemicolon";
      break;
    case Key::KeyApostrophe:
      os << "KeyApostrophe";
      break;
    case Key::KeyGrave:
      os << "KeyGrave";
      break;
    case Key::KeyLeftShift:
      os << "KeyLeftShift";
      break;
    case Key::KeyBackslash:
      os << "KeyBackslash";
      break;
    case Key::KeyZ:
      os << "KeyZ";
      break;
    case Key::KeyX:
      os << "KeyX";
      break;
    case Key::KeyC:
      os << "KeyC";
      break;
    case Key::KeyV:
      os << "KeyV";
      break;
    case Key::KeyB:
      os << "KeyB";
      break;
    case Key::KeyN:
      os << "KeyN";
      break;
    case Key::KeyM:
      os << "KeyM";
      break;
    case Key::KeyComma:
      os << "KeyComma";
      break;
    case Key::KeyDot:
      os << "KeyDot";
      break;
    case Key::KeySlash:
      os << "KeySlash";
      break;
    case Key::KeyRightShift:
      os << "KeyRightShift";
      break;
    case Key::KeyKeypadAsterisk:
      os << "KeyKeypadAsterisk";
      break;
    case Key::KeyLeftAlternate:
      os << "KeyLeftAlternate";
      break;
    case Key::KeySpace:
      os << "KeySpace";
      break;
    case Key::KeyCapsLock:
      os << "KeyCapsLock";
      break;
    case Key::KeyF1:
      os << "KeyF1";
      break;
    case Key::KeyF2:
      os << "KeyF2";
      break;
    case Key::KeyF3:
      os << "KeyF3";
      break;
    case Key::KeyF4:
      os << "KeyF4";
      break;
    case Key::KeyF5:
      os << "KeyF5";
      break;
    case Key::KeyF6:
      os << "KeyF6";
      break;
    case Key::KeyF7:
      os << "KeyF7";
      break;
    case Key::KeyF8:
      os << "KeyF8";
      break;
    case Key::KeyF9:
      os << "KeyF9";
      break;
    case Key::KeyF10:
      os << "KeyF10";
      break;
    case Key::KeyNumericLock:
      os << "KeyNumericLock";
      break;
    case Key::KeyScrollLock:
      os << "KeyScrollLock";
      break;
    case Key::KeyKeypad7:
      os << "KeyKeypad7";
      break;
    case Key::KeyKeypad8:
      os << "KeyKeypad8";
      break;
    case Key::KeyKeypad9:
      os << "KeyKeypad9";
      break;
    case Key::KeyKeypadMinus:
      os << "KeyKeypadMinus";
      break;
    case Key::KeyKeypad4:
      os << "KeyKeypad4";
      break;
    case Key::KeyKeypad5:
      os << "KeyKeypad5";
      break;
    case Key::KeyKeypad6:
      os << "KeyKeypad6";
      break;
    case Key::KeyKeypadPlus:
      os << "KeyKeypadPlus";
      break;
    case Key::KeyKeypad1:
      os << "KeyKeypad1";
      break;
    case Key::KeyKeypad2:
      os << "KeyKeypad2";
      break;
    case Key::KeyKeypad3:
      os << "KeyKeypad3";
      break;
    case Key::KeyKeypad0:
      os << "KeyKeypad0";
      break;
    case Key::KeyKeypadDot:
      os << "KeyKeypadDot";
      break;
    case Key::KeyZenkakuHankaku:
      os << "KeyZenkakuHankaku";
      break;
    case Key::Key102ND:
      os << "Key102ND";
      break;
    case Key::KeyF11:
      os << "KeyF11";
      break;
    case Key::KeyF12:
      os << "KeyF12";
      break;
    case Key::KeyRO:
      os << "KeyRO";
      break;
    case Key::KeyKatakana:
      os << "KeyKatakana";
      break;
    case Key::KeyHiragana:
      os << "KeyHiragana";
      break;
    case Key::KeyHenkan:
      os << "KeyHenkan";
      break;
    case Key::KeyKatakanaHiragana:
      os << "KeyKatakanaHiragana";
      break;
    case Key::KeyMuhenkan:
      os << "KeyMuhenkan";
      break;
    case Key::KeyKeypadJPComma:
      os << "KeyKeypadJPComma";
      break;
    case Key::KeyKeypadEnter:
      os << "KeyKeypadEnter";
      break;
    case Key::KeyRightControl:
      os << "KeyRightControl";
      break;
    case Key::KeyKeypadSlash:
      os << "KeyKeypadSlash";
      break;
    case Key::KeySystemRequest:
      os << "KeySystemRequest";
      break;
    case Key::KeyRightAlternate:
      os << "KeyRightAlternate";
      break;
    case Key::KeyLinefeed:
      os << "KeyLinefeed";
      break;
    case Key::KeyHome:
      os << "KeyHome";
      break;
    case Key::KeyUp:
      os << "KeyUp";
      break;
    case Key::KeyPageUp:
      os << "KeyPageUp";
      break;
    case Key::KeyLeft:
      os << "KeyLeft";
      break;
    case Key::KeyRight:
      os << "KeyRight";
      break;
    case Key::KeyEnd:
      os << "KeyEnd";
      break;
    case Key::KeyDown:
      os << "KeyDown";
      break;
    case Key::KeyPageDown:
      os << "KeyPageDown";
      break;
    case Key::KeyInsert:
      os << "KeyInsert";
      break;
    case Key::KeyDelete:
      os << "KeyDelete";
      break;
    case Key::KeyMacro:
      os << "KeyMacro";
      break;
    case Key::KeyMute:
      os << "KeyMute";
      break;
    case Key::KeyVolumeDown:
      os << "KeyVolumeDown";
      break;
    case Key::KeyVolumeUp:
      os << "KeyVolumeUp";
      break;
    case Key::KeyPower:
      os << "KeyPower";
      break;
    case Key::KeyKeypadEqual:
      os << "KeyKeypadEqual";
      break;
    case Key::KeyKeypadPlusMinus:
      os << "KeyKeypadPlusMinus";
      break;
    case Key::KeyPause:
      os << "KeyPause";
      break;
    case Key::KeyScale:
      os << "KeyScale";
      break;
    case Key::KeyKeypadComma:
      os << "KeyKeypadComma";
      break;
    case Key::KeyHangeul:
      os << "KeyHangeul";
      break;
    case Key::KeyHanja:
      os << "KeyHanja";
      break;
    case Key::KeyYen:
      os << "KeyYen";
      break;
    case Key::KeyLeftMeta:
      os << "KeyLeftMeta";
      break;
    case Key::KeyRightMeta:
      os << "KeyRightMeta";
      break;
    case Key::KeyCompose:
      os << "KeyCompose";
      break;
    case Key::KeyStop:
      os << "KeyStop";
      break;
    case Key::KeyAgain:
      os << "KeyAgain";
      break;
    case Key::KeyProps:
      os << "KeyProps";
      break;
    case Key::KeyUndo:
      os << "KeyUndo";
      break;
    case Key::KeyFront:
      os << "KeyFront";
      break;
    case Key::KeyCopy:
      os << "KeyCopy";
      break;
    case Key::KeyOpen:
      os << "KeyOpen";
      break;
    case Key::KeyPaste:
      os << "KeyPaste";
      break;
    case Key::KeyFind:
      os << "KeyFind";
      break;
    case Key::KeyCut:
      os << "KeyCut";
      break;
    case Key::KeyHelp:
      os << "KeyHelp";
      break;
    case Key::KeyMenu:
      os << "KeyMenu";
      break;
    case Key::KeyCalculator:
      os << "KeyCalculator";
      break;
    case Key::KeySetup:
      os << "KeySetup";
      break;
    case Key::KeySleep:
      os << "KeySleep";
      break;
    case Key::KeyWakeup:
      os << "KeyWakeup";
      break;
    case Key::KeyFile:
      os << "KeyFile";
      break;
    case Key::KeySendFile:
      os << "KeySendFile";
      break;
    case Key::KeyDeleteFile:
      os << "KeyDeleteFile";
      break;
    case Key::KeyXFER:
      os << "KeyXFER";
      break;
    case Key::KeyProgram1:
      os << "KeyProgram1";
      break;
    case Key::KeyProgram2:
      os << "KeyProgram2";
      break;
    case Key::KeyWWW:
      os << "KeyWWW";
      break;
    case Key::KeyMsDOS:
      os << "KeyMsDOS";
      break;
    case Key::KeyCoffee:
      os << "KeyCoffee";
      break;
    case Key::KeyRotateDisplay:
      os << "KeyRotateDisplay";
      break;
    case Key::KeyCycleWindows:
      os << "KeyCycleWindows";
      break;
    case Key::KeyMail:
      os << "KeyMail";
      break;
    case Key::KeyBookmarks:
      os << "KeyBookmarks";
      break;
    case Key::KeyComputer:
      os << "KeyComputer";
      break;
    case Key::KeyBack:
      os << "KeyBack";
      break;
    case Key::KeyForward:
      os << "KeyForward";
      break;
    case Key::KeyCloseCD:
      os << "KeyCloseCD";
      break;
    case Key::KeyEjectCD:
      os << "KeyEjectCD";
      break;
    case Key::KeyEjectCloseCD:
      os << "KeyEjectCloseCD";
      break;
    case Key::KeyNextSong:
      os << "KeyNextSong";
      break;
    case Key::KeyPlayPause:
      os << "KeyPlayPause";
      break;
    case Key::KeyPreviousSong:
      os << "KeyPreviousSong";
      break;
    case Key::KeyStopCD:
      os << "KeyStopCD";
      break;
    case Key::KeyRecord:
      os << "KeyRecord";
      break;
    case Key::KeyRewind:
      os << "KeyRewind";
      break;
    case Key::KeyPhone:
      os << "KeyPhone";
      break;
    case Key::KeyIso:
      os << "KeyIso";
      break;
    case Key::KeyConfig:
      os << "KeyConfig";
      break;
    case Key::KeyHomepage:
      os << "KeyHomepage";
      break;
    case Key::KeyRefresh:
      os << "KeyRefresh";
      break;
    case Key::KeyExit:
      os << "KeyExit";
      break;
    case Key::KeyMove:
      os << "KeyMove";
      break;
    case Key::KeyEdit:
      os << "KeyEdit";
      break;
    case Key::KeyScrollUp:
      os << "KeyScrollUp";
      break;
    case Key::KeyScrollDown:
      os << "KeyScrollDown";
      break;
    case Key::KeyKeypadLeftParenthesis:
      os << "KeyKeypadLeftParenthesis";
      break;
    case Key::KeyKeypadRightParenthesis:
      os << "KeyKeypadRightParenthesis";
      break;
    case Key::KeyNew:
      os << "KeyNew";
      break;
    case Key::KeyRedo:
      os << "KeyRedo";
      break;
    case Key::KeyF13:
      os << "KeyF13";
      break;
    case Key::KeyF14:
      os << "KeyF14";
      break;
    case Key::KeyF15:
      os << "KeyF15";
      break;
    case Key::KeyF16:
      os << "KeyF16";
      break;
    case Key::KeyF17:
      os << "KeyF17";
      break;
    case Key::KeyF18:
      os << "KeyF18";
      break;
    case Key::KeyF19:
      os << "KeyF19";
      break;
    case Key::KeyF20:
      os << "KeyF20";
      break;
    case Key::KeyF21:
      os << "KeyF21";
      break;
    case Key::KeyF22:
      os << "KeyF22";
      break;
    case Key::KeyF23:
      os << "KeyF23";
      break;
    case Key::KeyF24:
      os << "KeyF24";
      break;
    case Key::KeyPlayCD:
      os << "KeyPlayCD";
      break;
    case Key::KeyPauseCD:
      os << "KeyPauseCD";
      break;
    case Key::KeyProgram3:
      os << "KeyProgram3";
      break;
    case Key::KeyProgram4:
      os << "KeyProgram4";
      break;
    case Key::KeyDashboard:
      os << "KeyDashboard";
      break;
    case Key::KeySuspend:
      os << "KeySuspend";
      break;
    case Key::KeyClose:
      os << "KeyClose";
      break;
    case Key::KeyPlay:
      os << "KeyPlay";
      break;
    case Key::KeyFastForward:
      os << "KeyFastForward";
      break;
    case Key::KeyBassBoost:
      os << "KeyBassBoost";
      break;
    case Key::KeyPrint:
      os << "KeyPrint";
      break;
    case Key::KeyHP:
      os << "KeyHP";
      break;
    case Key::KeyCamera:
      os << "KeyCamera";
      break;
    case Key::KeySound:
      os << "KeySound";
      break;
    case Key::KeyQuestion:
      os << "KeyQuestion";
      break;
    case Key::KeyEmail:
      os << "KeyEmail";
      break;
    case Key::KeyChat:
      os << "KeyChat";
      break;
    case Key::KeySearch:
      os << "KeySearch";
      break;
    case Key::KeyConnect:
      os << "KeyConnect";
      break;
    case Key::KeyFinance:
      os << "KeyFinance";
      break;
    case Key::KeySport:
      os << "KeySport";
      break;
    case Key::KeyShop:
      os << "KeyShop";
      break;
    case Key::KeyAlternateErase:
      os << "KeyAlternateErase";
      break;
    case Key::KeyCancel:
      os << "KeyCancel";
      break;
    case Key::KeyBrightnessDown:
      os << "KeyBrightnessDown";
      break;
    case Key::KeyBrightnessUp:
      os << "KeyBrightnessUp";
      break;
    case Key::KeyMedia:
      os << "KeyMedia";
      break;
    case Key::KeySwitchVideoMode:
      os << "KeySwitchVideoMode";
      break;
    case Key::KeyKeyboardIlluminationToggle:
      os << "KeyKeyboardIlluminationToggle";
      break;
    case Key::KeyKeyboardIlluminationDown:
      os << "KeyKeyboardIlluminationDown";
      break;
    case Key::KeyKeyboardIlluminationUp:
      os << "KeyKeyboardIlluminationUp";
      break;
    case Key::KeySend:
      os << "KeySend";
      break;
    case Key::KeyReply:
      os << "KeyReply";
      break;
    case Key::KeyForwardMail:
      os << "KeyForwardMail";
      break;
    case Key::KeySave:
      os << "KeySave";
      break;
    case Key::KeyDocuments:
      os << "KeyDocuments";
      break;
    case Key::KeyBattery:
      os << "KeyBattery";
      break;
    case Key::KeyBluetooth:
      os << "KeyBluetooth";
      break;
    case Key::KeyWLAN:
      os << "KeyWLAN";
      break;
    case Key::KeyUWB:
      os << "KeyUWB";
      break;
    case Key::KeyUnknown:
      os << "KeyUnknown";
      break;
    case Key::KeyVideoNext:
      os << "KeyVideoNext";
      break;
    case Key::KeyVideoPrevious:
      os << "KeyVideoPrevious";
      break;
    case Key::KeyBrightnessCycle:
      os << "KeyBrightnessCycle";
      break;
    case Key::KeyBrightnessAuto:
      os << "KeyBrightnessAuto";
      break;
    case Key::KeyDisplayOff:
      os << "KeyDisplayOff";
      break;
    case Key::KeyWWAN:
      os << "KeyWWAN";
      break;
    case Key::KeyRFKill:
      os << "KeyRFKill";
      break;
    case Key::KeyMicrophoneMute:
      os << "KeyMicrophoneMute";
      break;
    case Key::Button0:
      os << "Button0";
      break;
    case Key::Button1:
      os << "Button1";
      break;
    case Key::Button2:
      os << "Button2";
      break;
    case Key::Button3:
      os << "Button3";
      break;
    case Key::Button4:
      os << "Button4";
      break;
    case Key::Button5:
      os << "Button5";
      break;
    case Key::Button6:
      os << "Button6";
      break;
    case Key::Button7:
      os << "Button7";
      break;
    case Key::Button8:
      os << "Button8";
      break;
    case Key::Button9:
      os << "Button9";
      break;
    case Key::ButtonLeftMouse:
      os << "ButtonLeftMouse";
      break;
    case Key::ButtonRightMouse:
      os << "ButtonRightMouse";
      break;
    case Key::ButtonMiddleMouse:
      os << "ButtonMiddleMouse";
      break;
    case Key::ButtonSideMouse:
      os << "ButtonSideMouse";
      break;
    case Key::ButtonExtraMouse:
      os << "ButtonExtraMouse";
      break;
    case Key::ButtonForwardMouse:
      os << "ButtonForwardMouse";
      break;
    case Key::ButtonBackMouse:
      os << "ButtonBackMouse";
      break;
    case Key::ButtonTaskMouse:
      os << "ButtonTaskMouse";
      break;
    case Key::ButtonTrigger:
      os << "ButtonTrigger";
      break;
    case Key::ButtonThumb:
      os << "ButtonThumb";
      break;
    case Key::ButtonThumb2:
      os << "ButtonThumb2";
      break;
    case Key::ButtonTop:
      os << "ButtonTop";
      break;
    case Key::ButtonTop2:
      os << "ButtonTop2";
      break;
    case Key::ButtonPinkie:
      os << "ButtonPinkie";
      break;
    case Key::ButtonBase:
      os << "ButtonBase";
      break;
    case Key::ButtonBase2:
      os << "ButtonBase2";
      break;
    case Key::ButtonBase3:
      os << "ButtonBase3";
      break;
    case Key::ButtonBase4:
      os << "ButtonBase4";
      break;
    case Key::ButtonBase5:
      os << "ButtonBase5";
      break;
    case Key::ButtonBase6:
      os << "ButtonBase6";
      break;
    case Key::ButtonDead:
      os << "ButtonDead";
      break;
    case Key::ButtonSouth:
      os << "ButtonSouth";
      break;
    case Key::ButtonEast:
      os << "ButtonEast";
      break;
    case Key::ButtonC:
      os << "ButtonC";
      break;
    case Key::ButtonNorth:
      os << "ButtonNorth";
      break;
    case Key::ButtonWest:
      os << "ButtonWest";
      break;
    case Key::ButtonZ:
      os << "ButtonZ";
      break;
    case Key::ButtonTriggerLeft:
      os << "ButtonTriggerLeft";
      break;
    case Key::ButtonTriggerRight:
      os << "ButtonTriggerRight";
      break;
    case Key::ButtonTriggerLeft2:
      os << "ButtonTriggerLeft2";
      break;
    case Key::ButtonTriggerRight2:
      os << "ButtonTriggerRight2";
      break;
    case Key::ButtonSelect:
      os << "ButtonSelect";
      break;
    case Key::ButtonStart:
      os << "ButtonStart";
      break;
    case Key::ButtonMode:
      os << "ButtonMode";
      break;
    case Key::ButtonThumbLeft:
      os << "ButtonThumbLeft";
      break;
    case Key::ButtonThumbRight:
      os << "ButtonThumbRight";
      break;
    case Key::ButtonToolPen:
      os << "ButtonToolPen";
      break;
    case Key::ButtonToolRubber:
      os << "ButtonToolRubber";
      break;
    case Key::ButtonToolBrush:
      os << "ButtonToolBrush";
      break;
    case Key::ButtonToolPencil:
      os << "ButtonToolPencil";
      break;
    case Key::ButtonToolAirbrush:
      os << "ButtonToolAirbrush";
      break;
    case Key::ButtonToolFinger:
      os << "ButtonToolFinger";
      break;
    case Key::ButtonToolMouse:
      os << "ButtonToolMouse";
      break;
    case Key::ButtonToolLens:
      os << "ButtonToolLens";
      break;
    case Key::ButtonToolQuintupleTap:
      os << "ButtonToolQuintupleTap";
      break;
    case Key::ButtonStylus3:
      os << "ButtonStylus3";
      break;
    case Key::ButtonTouch:
      os << "ButtonTouch";
      break;
    case Key::ButtonStylus:
      os << "ButtonStylus";
      break;
    case Key::ButtonStylus2:
      os << "ButtonStylus2";
      break;
    case Key::ButtonToolDoubleTap:
      os << "ButtonToolDoubleTap";
      break;
    case Key::ButtonToolTripleTap:
      os << "ButtonToolTripleTap";
      break;
    case Key::ButtonToolQuadrupleTap:
      os << "ButtonToolQuadrupleTap";
      break;
    case Key::ButtonGearDown:
      os << "ButtonGearDown";
      break;
    case Key::ButtonGearUp:
      os << "ButtonGearUp";
      break;
    case Key::KeyOk:
      os << "KeyOk";
      break;
    case Key::KeySelect:
      os << "KeySelect";
      break;
    case Key::KeyGoto:
      os << "KeyGoto";
      break;
    case Key::KeyClear:
      os << "KeyClear";
      break;
    case Key::KeyPower2:
      os << "KeyPower2";
      break;
    case Key::KeyOption:
      os << "KeyOption";
      break;
    case Key::KeyInfo:
      os << "KeyInfo";
      break;
    case Key::KeyTime:
      os << "KeyTime";
      break;
    case Key::KeyVendor:
      os << "KeyVendor";
      break;
    case Key::KeyArchive:
      os << "KeyArchive";
      break;
    case Key::KeyProgram:
      os << "KeyProgram";
      break;
    case Key::KeyChannel:
      os << "KeyChannel";
      break;
    case Key::KeyFavorites:
      os << "KeyFavorites";
      break;
    case Key::KeyEPG:
      os << "KeyEPG";
      break;
    case Key::KeyPVR:
      os << "KeyPVR";
      break;
    case Key::KeyMHP:
      os << "KeyMHP";
      break;
    case Key::KeyLanguage:
      os << "KeyLanguage";
      break;
    case Key::KeyTitle:
      os << "KeyTitle";
      break;
    case Key::KeySubtitle:
      os << "KeySubtitle";
      break;
    case Key::KeyAngle:
      os << "KeyAngle";
      break;
    case Key::KeyFullScreen:
      os << "KeyFullScreen";
      break;
    case Key::KeyMode:
      os << "KeyMode";
      break;
    case Key::KeyKeyboard:
      os << "KeyKeyboard";
      break;
    case Key::KeyAspectRatio:
      os << "KeyAspectRatio";
      break;
    case Key::KeyPC:
      os << "KeyPC";
      break;
    case Key::KeyTV:
      os << "KeyTV";
      break;
    case Key::KeyTV2:
      os << "KeyTV2";
      break;
    case Key::KeyVCR:
      os << "KeyVCR";
      break;
    case Key::KeyVCR2:
      os << "KeyVCR2";
      break;
    case Key::KeySatellite:
      os << "KeySatellite";
      break;
    case Key::KeySatellite2:
      os << "KeySatellite2";
      break;
    case Key::KeyCD:
      os << "KeyCD";
      break;
    case Key::KeyTape:
      os << "KeyTape";
      break;
    case Key::KeyRadio:
      os << "KeyRadio";
      break;
    case Key::KeyTuner:
      os << "KeyTuner";
      break;
    case Key::KeyPlayer:
      os << "KeyPlayer";
      break;
    case Key::KeyText:
      os << "KeyText";
      break;
    case Key::KeyDVD:
      os << "KeyDVD";
      break;
    case Key::KeyAux:
      os << "KeyAux";
      break;
    case Key::KeyMP3:
      os << "KeyMP3";
      break;
    case Key::KeyAudio:
      os << "KeyAudio";
      break;
    case Key::KeyVideo:
      os << "KeyVideo";
      break;
    case Key::KeyDirectory:
      os << "KeyDirectory";
      break;
    case Key::KeyList:
      os << "KeyList";
      break;
    case Key::KeyMemo:
      os << "KeyMemo";
      break;
    case Key::KeyCalendar:
      os << "KeyCalendar";
      break;
    case Key::KeyRed:
      os << "KeyRed";
      break;
    case Key::KeyGreen:
      os << "KeyGreen";
      break;
    case Key::KeyYellow:
      os << "KeyYellow";
      break;
    case Key::KeyBlue:
      os << "KeyBlue";
      break;
    case Key::KeyChannelUp:
      os << "KeyChannelUp";
      break;
    case Key::KeyChannelDown:
      os << "KeyChannelDown";
      break;
    case Key::KeyFirst:
      os << "KeyFirst";
      break;
    case Key::KeyLast:
      os << "KeyLast";
      break;
    case Key::KeyAb:
      os << "KeyAb";
      break;
    case Key::KeyNext:
      os << "KeyNext";
      break;
    case Key::KeyRestart:
      os << "KeyRestart";
      break;
    case Key::KeySlow:
      os << "KeySlow";
      break;
    case Key::KeyShuffle:
      os << "KeyShuffle";
      break;
    case Key::KeyBreak:
      os << "KeyBreak";
      break;
    case Key::KeyPrevious:
      os << "KeyPrevious";
      break;
    case Key::KeyDigits:
      os << "KeyDigits";
      break;
    case Key::KeyTeen:
      os << "KeyTeen";
      break;
    case Key::KeyTwenty:
      os << "KeyTwenty";
      break;
    case Key::KeyVideophone:
      os << "KeyVideophone";
      break;
    case Key::KeyGames:
      os << "KeyGames";
      break;
    case Key::KeyZoomIn:
      os << "KeyZoomIn";
      break;
    case Key::KeyZoomOut:
      os << "KeyZoomOut";
      break;
    case Key::KeyZoomReset:
      os << "KeyZoomReset";
      break;
    case Key::KeyWordProcessor:
      os << "KeyWordProcessor";
      break;
    case Key::KeyEditor:
      os << "KeyEditor";
      break;
    case Key::KeySpreadSheet:
      os << "KeySpreadSheet";
      break;
    case Key::KeyGraphicsEditor:
      os << "KeyGraphicsEditor";
      break;
    case Key::KeyPresentation:
      os << "KeyPresentation";
      break;
    case Key::KeyDatabase:
      os << "KeyDatabase";
      break;
    case Key::KeyNews:
      os << "KeyNews";
      break;
    case Key::KeyVoiceMail:
      os << "KeyVoiceMail";
      break;
    case Key::KeyAddressBook:
      os << "KeyAddressBook";
      break;
    case Key::KeyMessenger:
      os << "KeyMessenger";
      break;
    case Key::KeyDisplayToggle:
      os << "KeyDisplayToggle";
      break;
    case Key::KeySpellcheck:
      os << "KeySpellcheck";
      break;
    case Key::KeyLogoff:
      os << "KeyLogoff";
      break;
    case Key::KeyDollar:
      os << "KeyDollar";
      break;
    case Key::KeyEuro:
      os << "KeyEuro";
      break;
    case Key::KeyFrameBack:
      os << "KeyFrameBack";
      break;
    case Key::KeyFrameForward:
      os << "KeyFrameForward";
      break;
    case Key::KeyContextMenu:
      os << "KeyContextMenu";
      break;
    case Key::KeyMediaRepeat:
      os << "KeyMediaRepeat";
      break;
    case Key::Key10ChannelsUp:
      os << "Key10ChannelsUp";
      break;
    case Key::Key10ChannelsDown:
      os << "Key10ChannelsDown";
      break;
    case Key::KeyImages:
      os << "KeyImages";
      break;
    case Key::KeyDeleteEOL:
      os << "KeyDeleteEOL";
      break;
    case Key::KeyDeleteEOS:
      os << "KeyDeleteEOS";
      break;
    case Key::KeyInsertLine:
      os << "KeyInsertLine";
      break;
    case Key::KeyDeleteLine:
      os << "KeyDeleteLine";
      break;
    case Key::KeyFunction:
      os << "KeyFunction";
      break;
    case Key::KeyFunctionEscape:
      os << "KeyFunctionEscape";
      break;
    case Key::KeyFunctionF1:
      os << "KeyFunctionF1";
      break;
    case Key::KeyFunctionF2:
      os << "KeyFunctionF2";
      break;
    case Key::KeyFunctionF3:
      os << "KeyFunctionF3";
      break;
    case Key::KeyFunctionF4:
      os << "KeyFunctionF4";
      break;
    case Key::KeyFunctionF5:
      os << "KeyFunctionF5";
      break;
    case Key::KeyFunctionF6:
      os << "KeyFunctionF6";
      break;
    case Key::KeyFunctionF7:
      os << "KeyFunctionF7";
      break;
    case Key::KeyFunctionF8:
      os << "KeyFunctionF8";
      break;
    case Key::KeyFunctionF9:
      os << "KeyFunctionF9";
      break;
    case Key::KeyFunctionF10:
      os << "KeyFunctionF10";
      break;
    case Key::KeyFunctionF11:
      os << "KeyFunctionF11";
      break;
    case Key::KeyFunctionF12:
      os << "KeyFunctionF12";
      break;
    case Key::KeyFunction1:
      os << "KeyFunction1";
      break;
    case Key::KeyFunction2:
      os << "KeyFunction2";
      break;
    case Key::KeyFunctionD:
      os << "KeyFunctionD";
      break;
    case Key::KeyFunctionE:
      os << "KeyFunctionE";
      break;
    case Key::KeyFunctionF:
      os << "KeyFunctionF";
      break;
    case Key::KeyFunctionS:
      os << "KeyFunctionS";
      break;
    case Key::KeyFunctionB:
      os << "KeyFunctionB";
      break;
    case Key::KeyBrailleDot1:
      os << "KeyBrailleDot1";
      break;
    case Key::KeyBrailleDot2:
      os << "KeyBrailleDot2";
      break;
    case Key::KeyBrailleDot3:
      os << "KeyBrailleDot3";
      break;
    case Key::KeyBrailleDot4:
      os << "KeyBrailleDot4";
      break;
    case Key::KeyBrailleDot5:
      os << "KeyBrailleDot5";
      break;
    case Key::KeyBrailleDot6:
      os << "KeyBrailleDot6";
      break;
    case Key::KeyBrailleDot7:
      os << "KeyBrailleDot7";
      break;
    case Key::KeyBrailleDot8:
      os << "KeyBrailleDot8";
      break;
    case Key::KeyBrailleDot9:
      os << "KeyBrailleDot9";
      break;
    case Key::KeyBrailleDot10:
      os << "KeyBrailleDot10";
      break;
    case Key::KeyNumeric0:
      os << "KeyNumeric0";
      break;
    case Key::KeyNumeric1:
      os << "KeyNumeric1";
      break;
    case Key::KeyNumeric2:
      os << "KeyNumeric2";
      break;
    case Key::KeyNumeric3:
      os << "KeyNumeric3";
      break;
    case Key::KeyNumeric4:
      os << "KeyNumeric4";
      break;
    case Key::KeyNumeric5:
      os << "KeyNumeric5";
      break;
    case Key::KeyNumeric6:
      os << "KeyNumeric6";
      break;
    case Key::KeyNumeric7:
      os << "KeyNumeric7";
      break;
    case Key::KeyNumeric8:
      os << "KeyNumeric8";
      break;
    case Key::KeyNumeric9:
      os << "KeyNumeric9";
      break;
    case Key::KeyNumericStar:
      os << "KeyNumericStar";
      break;
    case Key::KeyNumericPound:
      os << "KeyNumericPound";
      break;
    case Key::KeyNumericA:
      os << "KeyNumericA";
      break;
    case Key::KeyNumericB:
      os << "KeyNumericB";
      break;
    case Key::KeyNumericC:
      os << "KeyNumericC";
      break;
    case Key::KeyNumericD:
      os << "KeyNumericD";
      break;
    case Key::KeyCameraFocus:
      os << "KeyCameraFocus";
      break;
    case Key::KeyWPSButton:
      os << "KeyWPSButton";
      break;
    case Key::KeyTouchpadToggle:
      os << "KeyTouchpadToggle";
      break;
    case Key::KeyTouchpadOn:
      os << "KeyTouchpadOn";
      break;
    case Key::KeyTouchpadOff:
      os << "KeyTouchpadOff";
      break;
    case Key::KeyCameraZoomIn:
      os << "KeyCameraZoomIn";
      break;
    case Key::KeyCameraZoomOut:
      os << "KeyCameraZoomOut";
      break;
    case Key::KeyCameraUp:
      os << "KeyCameraUp";
      break;
    case Key::KeyCameraDown:
      os << "KeyCameraDown";
      break;
    case Key::KeyCameraLeft:
      os << "KeyCameraLeft";
      break;
    case Key::KeyCameraRight:
      os << "KeyCameraRight";
      break;
    case Key::KeyAttendantOn:
      os << "KeyAttendantOn";
      break;
    case Key::KeyAttendantOff:
      os << "KeyAttendantOff";
      break;
    case Key::KeyAttendantToggle:
      os << "KeyAttendantToggle";
      break;
    case Key::KeyLightsToggle:
      os << "KeyLightsToggle";
      break;
    case Key::ButtonDPadUp:
      os << "ButtonDPadUp";
      break;
    case Key::ButtonDPadDown:
      os << "ButtonDPadDown";
      break;
    case Key::ButtonDPadLeft:
      os << "ButtonDPadLeft";
      break;
    case Key::ButtonDPadRight:
      os << "ButtonDPadRight";
      break;
    case Key::KeyALSToggle:
      os << "KeyALSToggle";
      break;
    case Key::KeyRotateLockToggle:
      os << "KeyRotateLockToggle";
      break;
    case Key::KeyButtonConfiguration:
      os << "KeyButtonConfiguration";
      break;
    case Key::KeyTaskManager:
      os << "KeyTaskManager";
      break;
    case Key::KeyJournal:
      os << "KeyJournal";
      break;
    case Key::KeyControlPanel:
      os << "KeyControlPanel";
      break;
    case Key::KeyAppSelect:
      os << "KeyAppSelect";
      break;
    case Key::KeyScreenSaver:
      os << "KeyScreenSaver";
      break;
    case Key::KeyVoiceCommand:
      os << "KeyVoiceCommand";
      break;
    case Key::KeyAssistant:
      os << "KeyAssistant";
      break;
    case Key::KeyBrightnessMin:
      os << "KeyBrightnessMin";
      break;
    case Key::KeyBrightnessMax:
      os << "KeyBrightnessMax";
      break;
    case Key::KeyKeyboardInputAssistPrevious:
      os << "KeyKeyboardInputAssistPrevious";
      break;
    case Key::KeyKeyboardInputAssistNext:
      os << "KeyKeyboardInputAssistNext";
      break;
    case Key::KeyKeyboardInputAssistPreviousGroup:
      os << "KeyKeyboardInputAssistPreviousGroup";
      break;
    case Key::KeyKeyboardInputAssistNextGroup:
      os << "KeyKeyboardInputAssistNextGroup";
      break;
    case Key::KeyKeyboardInputAssistAccept:
      os << "KeyKeyboardInputAssistAccept";
      break;
    case Key::KeyKeyboardInputAssistCancel:
      os << "KeyKeyboardInputAssistCancel";
      break;
    case Key::KeyRightUp:
      os << "KeyRightUp";
      break;
    case Key::KeyRightDown:
      os << "KeyRightDown";
      break;
    case Key::KeyLeftUp:
      os << "KeyLeftUp";
      break;
    case Key::KeyLeftDown:
      os << "KeyLeftDown";
      break;
    case Key::KeyRootMenu:
      os << "KeyRootMenu";
      break;
    case Key::KeyMediaTopMenu:
      os << "KeyMediaTopMenu";
      break;
    case Key::KeyNumeric11:
      os << "KeyNumeric11";
      break;
    case Key::KeyNumeric12:
      os << "KeyNumeric12";
      break;
    case Key::KeyAudioDescription:
      os << "KeyAudioDescription";
      break;
    case Key::Key3DMode:
      os << "Key3DMode";
      break;
    case Key::KeyNextFavorite:
      os << "KeyNextFavorite";
      break;
    case Key::KeyStopRecord:
      os << "KeyStopRecord";
      break;
    case Key::KeyPauseRecord:
      os << "KeyPauseRecord";
      break;
    case Key::KeyVOD:
      os << "KeyVOD";
      break;
    case Key::KeyUnmute:
      os << "KeyUnmute";
      break;
    case Key::KeyFastReverse:
      os << "KeyFastReverse";
      break;
    case Key::KeySlowReverse:
      os << "KeySlowReverse";
      break;
    case Key::KeyData:
      os << "KeyData";
      break;
    case Key::KeyOnScreenKeyboard:
      os << "KeyOnScreenKeyboard";
      break;
    case Key::ButtonTriggerHappy1:
      os << "ButtonTriggerHappy1";
      break;
    case Key::ButtonTriggerHappy2:
      os << "ButtonTriggerHappy2";
      break;
    case Key::ButtonTriggerHappy3:
      os << "ButtonTriggerHappy3";
      break;
    case Key::ButtonTriggerHappy4:
      os << "ButtonTriggerHappy4";
      break;
    case Key::ButtonTriggerHappy5:
      os << "ButtonTriggerHappy5";
      break;
    case Key::ButtonTriggerHappy6:
      os << "ButtonTriggerHappy6";
      break;
    case Key::ButtonTriggerHappy7:
      os << "ButtonTriggerHappy7";
      break;
    case Key::ButtonTriggerHappy8:
      os << "ButtonTriggerHappy8";
      break;
    case Key::ButtonTriggerHappy9:
      os << "ButtonTriggerHappy9";
      break;
    case Key::ButtonTriggerHappy10:
      os << "ButtonTriggerHappy10";
      break;
    case Key::ButtonTriggerHappy11:
      os << "ButtonTriggerHappy11";
      break;
    case Key::ButtonTriggerHappy12:
      os << "ButtonTriggerHappy12";
      break;
    case Key::ButtonTriggerHappy13:
      os << "ButtonTriggerHappy13";
      break;
    case Key::ButtonTriggerHappy14:
      os << "ButtonTriggerHappy14";
      break;
    case Key::ButtonTriggerHappy15:
      os << "ButtonTriggerHappy15";
      break;
    case Key::ButtonTriggerHappy16:
      os << "ButtonTriggerHappy16";
      break;
    case Key::ButtonTriggerHappy17:
      os << "ButtonTriggerHappy17";
      break;
    case Key::ButtonTriggerHappy18:
      os << "ButtonTriggerHappy18";
      break;
    case Key::ButtonTriggerHappy19:
      os << "ButtonTriggerHappy19";
      break;
    case Key::ButtonTriggerHappy20:
      os << "ButtonTriggerHappy20";
      break;
    case Key::ButtonTriggerHappy21:
      os << "ButtonTriggerHappy21";
      break;
    case Key::ButtonTriggerHappy22:
      os << "ButtonTriggerHappy22";
      break;
    case Key::ButtonTriggerHappy23:
      os << "ButtonTriggerHappy23";
      break;
    case Key::ButtonTriggerHappy24:
      os << "ButtonTriggerHappy24";
      break;
    case Key::ButtonTriggerHappy25:
      os << "ButtonTriggerHappy25";
      break;
    case Key::ButtonTriggerHappy26:
      os << "ButtonTriggerHappy26";
      break;
    case Key::ButtonTriggerHappy27:
      os << "ButtonTriggerHappy27";
      break;
    case Key::ButtonTriggerHappy28:
      os << "ButtonTriggerHappy28";
      break;
    case Key::ButtonTriggerHappy29:
      os << "ButtonTriggerHappy29";
      break;
    case Key::ButtonTriggerHappy30:
      os << "ButtonTriggerHappy30";
      break;
    case Key::ButtonTriggerHappy31:
      os << "ButtonTriggerHappy31";
      break;
    case Key::ButtonTriggerHappy32:
      os << "ButtonTriggerHappy32";
      break;
    case Key::ButtonTriggerHappy33:
      os << "ButtonTriggerHappy33";
      break;
    case Key::ButtonTriggerHappy34:
      os << "ButtonTriggerHappy34";
      break;
    case Key::ButtonTriggerHappy35:
      os << "ButtonTriggerHappy35";
      break;
    case Key::ButtonTriggerHappy36:
      os << "ButtonTriggerHappy36";
      break;
    case Key::ButtonTriggerHappy37:
      os << "ButtonTriggerHappy37";
      break;
    case Key::ButtonTriggerHappy38:
      os << "ButtonTriggerHappy38";
      break;
    case Key::ButtonTriggerHappy39:
      os << "ButtonTriggerHappy39";
      break;
    case Key::ButtonTriggerHappy40:
      os << "ButtonTriggerHappy40";
      break;
    case Key::Max:
      os << "Max";
      break;
    case Key::Count:
      os << "Count";
      break;
  }
  return os;
}

#include "engine/input/manager.hpp"

engine::input::Manager::Manager(std::initializer_list<Device*> devices) noexcept
  : m_devices(devices)
{}

auto
engine::input::Manager::action(engine::usize index) const& noexcept -> option::Option<engine::input::Action const*>
{
  if (index >= m_actions.size()) {
    return std::move(option::Option<Action const*>::None());
  }
  return std::move(option::Option<Action const*>::Some(&m_actions[index]));
}

auto
engine::input::Manager::action(engine::CString const& name) const& noexcept
  -> option::Option<engine::input::Action const*>
{
  auto it = m_action_names.find(name);
  if (it == m_action_names.end()) {
    return std::move(option::Option<Action const*>::None());
  }
  return std::move(option::Option<Action const*>::Some(&m_actions[it->second]));
}

auto
engine::input::Manager::action_index(engine::CString const& name) const& noexcept -> option::Option<engine::usize>
{
  auto it = m_action_names.find(name);
  if (it == m_action_names.end()) {
    return std::move(option::Option<usize>::None());
  }
  return std::move(option::Option<usize>::Some(it->second));
}

auto
engine::input::Manager::add_action(engine::input::Action&& action, engine::CStr name) & noexcept -> void
{
  m_action_names.emplace(name, m_actions.size());
  m_actions.emplace_back(std::forward<Action>(action));
}

auto
engine::input::Manager::add_axis(engine::input::Axis&& axis, engine::CStr name) & noexcept -> void
{
  m_axis_names.emplace(name, m_axes.size());
  m_axes.emplace_back(std::forward<Axis>(axis));
}

auto
engine::input::Manager::axis(engine::usize index) const& noexcept -> option::Option<engine::input::Axis const*>
{
  if (index >= m_axes.size()) {
    return std::move(option::Option<Axis const*>::None());
  }
  return std::move(option::Option<Axis const*>::Some(&m_axes[index]));
}

auto
engine::input::Manager::axis(engine::CString const& name) const& noexcept -> option::Option<engine::input::Axis const*>
{
  auto it = m_axis_names.find(name);
  if (it == m_axis_names.end()) {
    return std::move(option::Option<Axis const*>::None());
  }
  return std::move(option::Option<Axis const*>::Some(&m_axes[it->second]));
}

auto
engine::input::Manager::axis_index(engine::CString const& name) const& noexcept -> option::Option<engine::usize>
{
  auto it = m_axis_names.find(name);
  if (it == m_axis_names.end()) {
    return std::move(option::Option<usize>::None());
  }
  return std::move(option::Option<usize>::Some(it->second));
}

auto
engine::input::Manager::begin_update() & noexcept -> void
{
  for (auto* device : m_devices) {
    device->begin_update();

    while (device->has_event()) {
      process_event(device->pop_event());
    }
  }

  for (auto& action : m_actions) {
    action.begin_update();
  }

  for (auto& axis : m_axes) {
    axis.begin_update();
  }
}

auto
engine::input::Manager::end_update() & noexcept -> void
{
  for (auto* device : m_devices) {
    device->end_update();
  }

  for (auto& action : m_actions) {
    action.end_update();
  }

  for (auto& axis : m_axes) {
    axis.end_update();
  }
}

auto
engine::input::Manager::process_event(engine::input::Event const& event) & noexcept -> void
{
  for (auto& action : m_actions) {
    action.process_event(event);
  }

  for (auto& axis : m_axes) {
    axis.process_event(event);
  }
}

#include "engine/input/axis.hpp"

engine::input::Axis::Axis(std::vector<engine::input::Axis::Binding>&& bindings) noexcept
  : m_bindings(std::forward<std::vector<Binding>>(bindings))
{}

auto
engine::input::Axis::begin_update() & noexcept -> void
{
  m_value = 0.0f;

  for (auto& binding : m_bindings) {
    m_value += binding.scale() * binding.value;
  }
}

auto
engine::input::Axis::end_update() & noexcept -> void
{}

auto
engine::input::Axis::new_(std::vector<engine::input::Axis::Binding>&& bindings) noexcept -> engine::input::Axis
{
  return std::move(Axis(std::forward<std::vector<Binding>>(bindings)));
}

auto
engine::input::Axis::new_pointer(std::vector<engine::input::Axis::Binding>&& bindings) noexcept -> engine::input::Axis*
{
  return new Axis(std::forward<std::vector<Binding>>(bindings));
}

auto
engine::input::Axis::process_event(engine::input::Event const& event) & noexcept -> void
{
  for (auto& binding : m_bindings) {
    if (binding.type() == event.type() && binding.number() == event.number()) {
      binding.value = event.value();
    }
  }
}

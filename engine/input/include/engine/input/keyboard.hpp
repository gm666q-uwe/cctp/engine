#pragma once

#include "engine/input/device.hpp"

namespace engine::input {
class Keyboard : public Device
{
public:
  Keyboard() noexcept;

  constexpr Keyboard(Keyboard const&) noexcept = delete;

  Keyboard(Keyboard&&) noexcept = default;

  ~Keyboard() noexcept override = default;

  constexpr auto operator=(Keyboard const&) & noexcept -> Keyboard& = delete;

  auto operator=(Keyboard&&) & noexcept -> Keyboard& = default;

  [[nodiscard]] virtual auto characters() const& noexcept -> CStr = 0;

protected:
private:
};
} // namespace engine::input

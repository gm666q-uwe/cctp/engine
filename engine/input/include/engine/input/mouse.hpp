#pragma once

#include "engine/input/device.hpp"

namespace engine::input {
class Mouse : public Device
{
public:
  Mouse() noexcept;

  constexpr Mouse(Mouse const&) noexcept = delete;

  Mouse(Mouse&&) noexcept = default;

  ~Mouse() noexcept override = default;

  constexpr auto operator=(Mouse const&) & noexcept -> Mouse& = delete;

  auto operator=(Mouse&&) & noexcept -> Mouse& = default;

  virtual auto confine() & noexcept -> void = 0;

  virtual auto lock() & noexcept -> void = 0;

protected:
private:
};
} // namespace engine::input

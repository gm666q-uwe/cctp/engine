#pragma once

#include <option/option.hpp>

#include "engine/input/absolute_axis.hpp"
#include "engine/input/key.hpp"
#include "engine/input/relative_axis.hpp"

namespace engine::input {
struct Event
{
public:
  enum class Type : u8
  {
    AbsoluteAxis = 0,
    Key = 1,
    RelativeAxis = 2,
  };

  constexpr Event() noexcept = delete;

  constexpr Event(AbsoluteAxis absolute_axis, f32 value) noexcept
    : m_number(static_cast<u16>(absolute_axis))
    , m_type(Type::AbsoluteAxis)
    , m_value(value)
  {}

  constexpr Event(Key key, f32 value) noexcept
    : m_number(static_cast<u16>(key))
    , m_type(Type::Key)
    , m_value(value)
  {}

  constexpr Event(RelativeAxis relative_axis, f32 value) noexcept
    : m_number(static_cast<u16>(relative_axis))
    , m_type(Type::RelativeAxis)
    , m_value(value)
  {}

  /*constexpr Event(u16 number, Type type, f32 value) noexcept
    : m_number(number)
    , m_type(type)
    , m_value(value)
  {}*/

  constexpr Event(Event const&) noexcept = default;

  constexpr Event(Event&&) noexcept = default;

  constexpr virtual ~Event() noexcept = default;

  constexpr auto operator=(Event const&) & noexcept -> Event& = default;

  constexpr auto operator=(Event&&) & noexcept -> Event& = default;

  [[nodiscard]] constexpr auto absolute_axis() const& noexcept -> option::Option<AbsoluteAxis>
  {
    return std::move(m_type == Type::AbsoluteAxis
		       ? option::Option<AbsoluteAxis>::Some(static_cast<AbsoluteAxis>(m_number))
		       : option::Option<AbsoluteAxis>::None());
  }

  [[nodiscard]] constexpr auto key() const& noexcept -> option::Option<Key>
  {
    return std::move(m_type == Type::Key
    ? option::Option<Key>::Some(static_cast<Key>(m_number))
    : option::Option<Key>::None());
  }

  [[nodiscard]] constexpr auto number() const& noexcept -> u16 { return m_number; }

  [[nodiscard]] constexpr auto relative_axis() const& noexcept -> option::Option<RelativeAxis>
  {
    return std::move(m_type == Type::RelativeAxis
		     ? option::Option<RelativeAxis>::Some(static_cast<RelativeAxis>(m_number))
		     : option::Option<RelativeAxis>::None());
  }

  [[nodiscard]] constexpr auto type() const& noexcept -> Type { return m_type; }

  [[nodiscard]] constexpr auto value() const& noexcept -> f32 { return m_value; }

protected:
private:
  u16 m_number = 0;
  Type m_type = Type::AbsoluteAxis;
  f32 m_value = 0.0f;
};
} // namespace engine::input

#pragma once

#include <vector>

#include "engine/input/event.hpp"

namespace engine::input {
class Action
{
public:
  struct Binding
  {
  public:
    bool previous_value = false;
    bool value = false;

    constexpr Binding() noexcept = delete;

    constexpr Binding(AbsoluteAxis absolute_axis, f32 dead_zone) noexcept
      : m_dead_zone(dead_zone)
      , m_number(static_cast<u16>(absolute_axis))
      , m_type(Event::Type::AbsoluteAxis)
    {}

    constexpr Binding(Key key, f32 dead_zone) noexcept
      : m_dead_zone(dead_zone)
      , m_number(static_cast<u16>(key))
      , m_type(Event::Type::Key)
    {}

    constexpr Binding(RelativeAxis relative_axis, f32 dead_zone) noexcept
      : m_dead_zone(dead_zone)
      , m_number(static_cast<u16>(relative_axis))
      , m_type(Event::Type::RelativeAxis)
    {}

    /*constexpr Binding(f32 dead_zone, u16 number, Event::Type type) noexcept
      : m_dead_zone(dead_zone)
      , m_number(number)
      , m_type(type)
    {}*/

    constexpr Binding(Binding const&) noexcept = default;

    constexpr Binding(Binding&&) = default;

    constexpr virtual ~Binding() noexcept = default;

    constexpr auto operator=(Binding const&) & noexcept -> Binding& = default;

    constexpr auto operator=(Binding&&) & noexcept -> Binding& = default;

    [[nodiscard]] constexpr auto dead_zone() const& noexcept -> f32 { return m_dead_zone; }

    [[nodiscard]] constexpr auto number() const& noexcept -> u16 { return m_number; }

    [[nodiscard]] constexpr auto type() const& noexcept -> Event::Type { return m_type; }

  protected:
  private:
    f32 m_dead_zone = 0.0f;
    u16 m_number = 0;
    Event::Type m_type = Event::Type::AbsoluteAxis;
  };

  constexpr Action() noexcept = delete;

  Action(Action const&) noexcept = delete;

  Action(Action&&) = default;

  virtual ~Action() noexcept = default;

  auto operator=(Action const&) & noexcept -> Action& = delete;

  auto operator=(Action&&) & noexcept -> Action& = default;

  auto begin_update() & noexcept -> void;

  auto end_update() & noexcept -> void;

  [[nodiscard]] constexpr auto get() const& noexcept -> bool { return m_value; }

  [[nodiscard]] constexpr auto get_pressed() const& noexcept -> bool { return m_value && !m_previous_value; }

  [[nodiscard]] constexpr auto get_released() const& noexcept -> bool { return !m_value && m_previous_value; }

  static auto new_(std::vector<Binding>&& bindings) noexcept -> Action;

  static auto new_pointer(std::vector<Binding>&& bindings) noexcept -> Action*;

  auto process_event(Event const& event) & noexcept -> void;

protected:
private:
  std::vector<Binding> m_bindings = std::vector<Binding>();
  bool m_previous_value = false;
  bool m_value = false;

  explicit Action(std::vector<Binding>&& bindings) noexcept;
};
} // namespace engine::input

#pragma once

#include "engine/input/action.hpp"
#include "engine/input/axis.hpp"
#include "engine/input/keyboard.hpp"
#include "engine/input/mouse.hpp"

namespace engine::input {
class IManager
{
public:
  constexpr IManager() noexcept = default;

  constexpr IManager(IManager const&) noexcept = delete;

  constexpr IManager(IManager&&) noexcept = default;

  virtual constexpr ~IManager() noexcept = default;

  constexpr auto operator=(IManager const&) & noexcept -> IManager& = delete;

  constexpr auto operator=(IManager&&) & noexcept -> IManager& = default;

  [[nodiscard]] virtual auto action(usize index) const& noexcept -> option::Option<Action const*> = 0;

  [[nodiscard]] virtual auto action(CString const& name) const& noexcept -> option::Option<Action const*> = 0;

  [[nodiscard]] virtual auto action_index(CString const& name) const& noexcept -> option::Option<usize> = 0;

  virtual auto add_action(Action&& action, CStr name) & noexcept -> void = 0;

  virtual auto add_axis(Axis&& axis, CStr name) & noexcept -> void = 0;

  [[nodiscard]] virtual auto axis(usize index) const& noexcept -> option::Option<Axis const*> = 0;

  [[nodiscard]] virtual auto axis(CString const& name) const& noexcept -> option::Option<Axis const*> = 0;

  [[nodiscard]] virtual auto axis_index(CString const& name) const& noexcept -> option::Option<usize> = 0;

  virtual auto begin_update() & noexcept -> void = 0;

  virtual auto end_update() & noexcept -> void = 0;

  [[nodiscard]] virtual auto keyboard() const& noexcept -> Keyboard const& = 0;

  [[nodiscard]] virtual auto mouse() const& noexcept -> Mouse const& = 0;

protected:
private:
};
} // namespace engine::input

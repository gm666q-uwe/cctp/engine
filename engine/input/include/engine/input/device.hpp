#pragma once

#include <queue>

#include "engine/input/event.hpp"

namespace engine::input {
class Device
{
public:
  enum class Type : u8
  {
    Controller = 0,
    Keyboard = 1,
    Mouse = 2,
    Other = 3,
    Unknown = 4,
  };

  constexpr Device() noexcept = delete;

  explicit Device(Type type) noexcept;

  Device(Device const&) noexcept = delete;

  Device(Device&&) = default;

  virtual ~Device() noexcept = default;

  auto operator=(Device const&) & noexcept -> Device& = delete;

  auto operator=(Device&&) & noexcept -> Device& = default;

  virtual auto begin_update() & noexcept -> void = 0;

  virtual auto end_update() & noexcept -> void = 0;

  [[nodiscard]] auto has_event() const& noexcept -> bool;

  auto pop_event() & noexcept -> Event;

  [[nodiscard]] constexpr auto type() const& noexcept -> Type { return m_type; }

protected:
  std::queue<Event> m_events = std::queue<Event>();
  Type m_type = Type::Controller;

private:
};
} // namespace engine::input

#pragma once

#include <vector>

#include "engine/input/event.hpp"

namespace engine::input {
class Axis
{
public:
  struct Binding
  {
  public:
    f32 value = 0.0f;

    constexpr Binding() noexcept = delete;

    constexpr Binding(AbsoluteAxis absolute_axis, f32 scale) noexcept
      : m_number(static_cast<u16>(absolute_axis))
      , m_scale(scale)
      , m_type(Event::Type::AbsoluteAxis)
    {}

    constexpr Binding(Key key, f32 scale) noexcept
      : m_number(static_cast<u16>(key))
      , m_scale(scale)
      , m_type(Event::Type::Key)
    {}

    constexpr Binding(RelativeAxis relative_axis, f32 scale) noexcept
      : m_number(static_cast<u16>(relative_axis))
      , m_scale(scale)
      , m_type(Event::Type::RelativeAxis)
    {}

    /*constexpr Binding(u16 number, f32 scale, Event::Type type) noexcept
      : m_number(number)
      , m_scale(scale)
      , m_type(type)
    {}*/

    constexpr Binding(Binding const&) noexcept = default;

    constexpr Binding(Binding&&) = default;

    constexpr virtual ~Binding() noexcept = default;

    constexpr auto operator=(Binding const&) & noexcept -> Binding& = default;

    constexpr auto operator=(Binding&&) & noexcept -> Binding& = default;

    [[nodiscard]] constexpr auto number() const& noexcept -> u16 { return m_number; }

    [[nodiscard]] constexpr auto scale() const& noexcept -> f32 { return m_scale; }

    [[nodiscard]] constexpr auto type() const& noexcept -> Event::Type { return m_type; }

  protected:
  private:
    u16 m_number = 0;
    f32 m_scale = 0.0f;
    Event::Type m_type = Event::Type::AbsoluteAxis;
  };

  constexpr Axis() noexcept = delete;

  Axis(Axis const&) noexcept = delete;

  Axis(Axis&&) = default;

  virtual ~Axis() noexcept = default;

  auto operator=(Axis const&) & noexcept -> Axis& = delete;

  auto operator=(Axis&&) & noexcept -> Axis& = default;

  auto begin_update() & noexcept -> void;

  auto end_update() & noexcept -> void;

  [[nodiscard]] constexpr auto get() const& noexcept -> float { return m_value; }

  static auto new_(std::vector<Binding>&& bindings) noexcept -> Axis;

  static auto new_pointer(std::vector<Binding>&& bindings) noexcept -> Axis*;

  auto process_event(Event const& event) & noexcept -> void;

protected:
private:
  std::vector<Binding> m_bindings = std::vector<Binding>();
  f32 m_value = 0.0f;

  explicit Axis(std::vector<Binding>&& bindings) noexcept;
};
} // namespace engine::input

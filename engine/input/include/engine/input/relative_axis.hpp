#pragma once

#include <engine/types.h>

namespace engine::input {
enum class RelativeAxis : u8
{
  LeftX = 0x00,
  LeftY = 0x01,
  LeftZ = 0x02,
  RightX = 0x03,
  RightY = 0x04,
  RightZ = 0x05,
  HorizontalWheel = 0x06,
  Dial = 0x07,
  VerticalWheel = 0x08,
  Miscellaneous = 0x09,
  Reserved = 0x0a,
  VerticalWheelHighResolution = 0x0b,
  HorizontalWheelHighResolution = 0x0c,
  Max = 0x0f,
  Count = RelativeAxis::Max + 0x01,
};
} // namespace engine::input
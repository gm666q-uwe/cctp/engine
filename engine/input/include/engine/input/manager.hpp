#pragma once

#include <memory>
#include <unordered_map>

#include "engine/input/device.hpp"
#include "engine/input/i_manager.hpp"

namespace engine::input {
class Manager : public IManager
{
public:
  constexpr Manager() noexcept = delete;

  constexpr Manager(Manager const&) noexcept = delete;

  Manager(Manager&&) noexcept = default;

  ~Manager() noexcept override = default;

  constexpr auto operator=(Manager const&) & noexcept -> Manager& = delete;

  auto operator=(Manager&&) & noexcept -> Manager& = default;

  auto action(usize index) const& noexcept -> option::Option<Action const*> override;

  auto action(CString const& name) const& noexcept -> option::Option<Action const*> override;

  auto action_index(CString const& name) const& noexcept -> option::Option<usize> override;

  auto add_action(Action&& action, CStr name) & noexcept -> void override;

  auto add_axis(Axis&& axis, CStr name) & noexcept -> void override;

  auto axis(usize index) const& noexcept -> option::Option<Axis const*> override;

  auto axis(CString const& name) const& noexcept -> option::Option<Axis const*> override;

  auto axis_index(CString const& name) const& noexcept -> option::Option<usize> override;

  auto begin_update() & noexcept -> void override;

  auto end_update() & noexcept -> void override;

protected:
  std::vector<input::Device*> m_devices;

  Manager(std::initializer_list<Device*> devices) noexcept;

private:
  std::unordered_map<CString, usize> m_action_names = std::unordered_map<CString, usize>();
  std::vector<input::Action> m_actions = std::vector<input::Action>();
  std::vector<input::Axis> m_axes = std::vector<input::Axis>();
  std::unordered_map<CString, usize> m_axis_names = std::unordered_map<CString, usize>();

  auto process_event(Event const& event) & noexcept -> void;
};
} // namespace engine::input

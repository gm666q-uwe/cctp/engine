cmake_minimum_required(VERSION 3.19)
project(engine_input VERSION 0.1.0 DESCRIPTION WIP LANGUAGES CXX)

set(PROJECT_HEADER_PATH ${PROJECT_SOURCE_DIR}/include/engine/input)
set(PROJECT_SOURCE_PATH ${PROJECT_SOURCE_DIR}/src/engine/input)

set(${PROJECT_NAME}_INCLUDE_DIRS
        ${PROJECT_SOURCE_DIR}/include)

set(${PROJECT_NAME}_HEADERS
        ${PROJECT_HEADER_PATH}/absolute_axis.hpp
        ${PROJECT_HEADER_PATH}/action.hpp
        ${PROJECT_HEADER_PATH}/axis.hpp
        ${PROJECT_HEADER_PATH}/device.hpp
        ${PROJECT_HEADER_PATH}/event.hpp
        ${PROJECT_HEADER_PATH}/i_manager.hpp
        ${PROJECT_HEADER_PATH}/key.hpp
        ${PROJECT_HEADER_PATH}/keyboard.hpp
        ${PROJECT_HEADER_PATH}/manager.hpp
        ${PROJECT_HEADER_PATH}/mouse.hpp
        ${PROJECT_HEADER_PATH}/relative_axis.hpp)

set(${PROJECT_NAME}_SOURCES
        ${PROJECT_SOURCE_PATH}/action.cpp
        ${PROJECT_SOURCE_PATH}/axis.cpp
        ${PROJECT_SOURCE_PATH}/device.cpp
        ${PROJECT_SOURCE_PATH}/key.cpp
        ${PROJECT_SOURCE_PATH}/keyboard.cpp
        ${PROJECT_SOURCE_PATH}/manager.cpp
        ${PROJECT_SOURCE_PATH}/mouse.cpp)

add_library(${PROJECT_NAME} STATIC ${${PROJECT_NAME}_HEADERS} ${${PROJECT_NAME}_SOURCES})
target_include_directories(${PROJECT_NAME} PUBLIC ${${PROJECT_NAME}_INCLUDE_DIRS})
target_link_libraries(${PROJECT_NAME} PUBLIC engine_common)
target_precompile_headers(${PROJECT_NAME} PRIVATE "$<$<COMPILE_LANGUAGE:CXX>:${PROJECT_SOURCE_DIR}/pch/pch.hpp>")

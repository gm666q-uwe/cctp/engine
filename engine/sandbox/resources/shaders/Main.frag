#version 460 core

layout (location = 0) in vec3 gNormal;
layout (location = 1) in float gHeight;

layout (location = 0) out vec4 fColor;

const vec3 LIGHT_COLOR = vec3(1.0f, 1.0f, 1.0f);
const vec3 LIGHT_DIRECTION = vec3(0.424, 0.566, 0.707);

void main(void)
{
    //vec3 unitNormal = normalize(gNormal);

    float brightness = max(0.0f, dot(gNormal, LIGHT_DIRECTION));
    vec4 diffuse = vec4(brightness * LIGHT_COLOR, 1.0f);

    vec4 color = mix(vec4(0.0f, 1.0f, 0.0f, 1.0f), vec4(1.0f, 0.0f, 0.0f, 1.0f), gHeight + 0.5f);

    fColor = diffuse * color;//vec4(0.5f * vec3(gNormal.x + 1.0f, gNormal.y + 1.0f, gNormal.z + 1.0f), 1.0f);
}

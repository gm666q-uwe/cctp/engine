#version 460 core

layout (points) in;
layout (triangle_strip, max_vertices = 15) out;

layout (location = 0) out vec3 gNormal;
layout (location = 1) out float gHeight;

layout (binding = 2) uniform sampler3D tVoxelData;
layout (binding = 3) uniform isampler2D tTriangulation;

layout (std140, binding = 0) uniform CameraMatrices {
    mat4 projection;
    mat4 view;
} uCameraMatrices;

layout (std140, binding = 1) uniform ObjectMatrices {
    mat4 model;
} uObjectMatrices;

#define BIT_COUNT 8u
const int BITS[BIT_COUNT] = int[BIT_COUNT](1, 2, 4, 8, 16, 32, 64, 128);

const vec4 STEP = vec4(0.0625f, 0.0625f, 0.0625f, 0.0f);

float voxelDataAt(vec3 position) {
    return texture(tVoxelData, (position + 1.0f) / 2.0f).r;
}

vec4 cubeCorner(vec3 offset) {
    vec3 vPosition = gl_in[0].gl_Position.xyz + offset;
    return vec4(vPosition, voxelDataAt(vPosition));
}

int triangulationValue(int i, int j) {
    return texelFetch(tTriangulation, ivec2(j, i), 0).r;
}

vec3 vertexInterpolate(float isoLevel, vec4 v0, vec4 v1){
    return mix(v0.xyz, v1.xyz, (isoLevel - v0.w) / (v1.w - v0.w));
}

vec3 vertexNormal(vec3 position) {
    float x = voxelDataAt(position + STEP.xww) - voxelDataAt(position - STEP.xww);
    float y = voxelDataAt(position + STEP.wyw) - voxelDataAt(position - STEP.wyw);
    float z = voxelDataAt(position + STEP.wwz) - voxelDataAt(position - STEP.wwz);
    return normalize(vec3(x, y, z));
}

void emitVertex(vec4 position, mat4 viewProjection) {
    gHeight = position.y;
    gNormal = (uObjectMatrices.model * vec4(vertexNormal(position.xyz), 0.0f)).xyz;
    position = uObjectMatrices.model * position;
    gl_Position = viewProjection * position;
    #if VULKAN
    gl_Position.y = -gl_Position.y;
    gl_Position.z = (gl_Position.z + gl_Position.w) / 2.0;
    #endif
    EmitVertex();
}

void main(void) {
    mat4 viewProjection = uCameraMatrices.projection * uCameraMatrices.view;

    float isoLevel = 0.5f;

    vec4 cubeCorners[8] = vec4[8](
    cubeCorner(STEP.www),
    cubeCorner(STEP.xww),
    cubeCorner(STEP.xyw),
    cubeCorner(STEP.wyw),
    cubeCorner(STEP.wwz),
    cubeCorner(STEP.xwz),
    cubeCorner(STEP.xyz),
    cubeCorner(STEP.wyz)
    );

    int cubeIndex = 0;
    for (uint i = 0; i < 8; i++) {
        cubeIndex += int(cubeCorners[i].w < isoLevel) * BITS[i];
    }

    if (cubeIndex == 0 || cubeIndex == 255) {
        return;
    }

    vec3 vertexList[12] = vec3[12](
    vertexInterpolate(isoLevel, cubeCorners[0], cubeCorners[1]),
    vertexInterpolate(isoLevel, cubeCorners[1], cubeCorners[2]),
    vertexInterpolate(isoLevel, cubeCorners[2], cubeCorners[3]),
    vertexInterpolate(isoLevel, cubeCorners[3], cubeCorners[0]),
    vertexInterpolate(isoLevel, cubeCorners[4], cubeCorners[5]),
    vertexInterpolate(isoLevel, cubeCorners[5], cubeCorners[6]),
    vertexInterpolate(isoLevel, cubeCorners[6], cubeCorners[7]),
    vertexInterpolate(isoLevel, cubeCorners[7], cubeCorners[4]),
    vertexInterpolate(isoLevel, cubeCorners[0], cubeCorners[4]),
    vertexInterpolate(isoLevel, cubeCorners[1], cubeCorners[5]),
    vertexInterpolate(isoLevel, cubeCorners[2], cubeCorners[6]),
    vertexInterpolate(isoLevel, cubeCorners[3], cubeCorners[7])
    );

    vec4 position = vec4(0.0f, 0.0f, 0.0f, 1.0f);
    for (int i = 0; triangulationValue(cubeIndex, i) != -1; i += 3) {
        emitVertex(vec4(vertexList[triangulationValue(cubeIndex, i)], 1.0f), viewProjection);
        emitVertex(vec4(vertexList[triangulationValue(cubeIndex, i + 2)], 1.0f), viewProjection);
        emitVertex(vec4(vertexList[triangulationValue(cubeIndex, i + 1)], 1.0f), viewProjection);
        EndPrimitive();
    }
}

#pragma once

#include <unordered_map>

#include <PerlinNoise.hpp>

#include "engine/sandbox/voxel_data.hpp"

namespace engine::sandbox {
class Generator
{
public:
  // constexpr Generator() noexcept = delete;

  constexpr Generator(Generator const&) noexcept = delete;

  Generator(Generator&& other) noexcept;

  virtual ~Generator() noexcept;

  constexpr auto operator=(Generator const&) & noexcept -> Generator& = delete;

  constexpr auto operator=(Generator&&) & noexcept -> Generator& = delete;

  auto get_chunk(i16 x, i16 y) & noexcept -> VoxelData const&;

  static auto new_() noexcept -> Generator;

  static auto new_pointer() noexcept -> Generator*;

  static auto with_seed(u32 seed) noexcept -> Generator;

  static auto with_seed_pointer(u32 seed) noexcept -> Generator*;

protected:
private:
  union Position
  {
    u32 index;
    struct
    {
      i16 x;
      i16 y;
    };

    constexpr explicit Position(u32 index) noexcept
      : index(index)
    {}

    constexpr Position(i16 x, i16 y) noexcept
      : x(x)
      , y(y)
    {}
  };

  static constexpr f32 const M_FREQUENCY = 1.0f;
  static constexpr i32 const M_OCTAVES = 8;

  std::unordered_map<u32, VoxelData> m_chunk_map = std::unordered_map<u32, VoxelData>();
  siv::BasicPerlinNoise<f32> m_perlin_noise = siv::BasicPerlinNoise<f32>();

  explicit Generator(u32 seed) noexcept;

  auto drop() & noexcept -> void;

  auto generate_chunk(i16 x, i16 y) & noexcept -> VoxelData const&;

  auto initialize() & noexcept -> void;
};
} // namespace engine::sandbox

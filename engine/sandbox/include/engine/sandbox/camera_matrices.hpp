#pragma once

#include <engine/graphics/glm.hpp>

namespace engine::sandbox {
struct CameraMatrices
{
public:
  glm::mat4 projection;
  glm::mat4 view;

protected:
private:
};
} // namespace engine::tmp

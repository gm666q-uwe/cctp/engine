#pragma once

#include <engine/graphics/glm.hpp>

namespace engine::sandbox {
struct Vertex
{
public:
  glm::vec3 position;

protected:
private:
};
} // namespace engine::tmp

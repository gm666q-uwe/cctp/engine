#pragma once

#include <span>

#include <engine/platform.h>

#if ENGINE_HAS_GL
#include <engine/graphics/gl/graphics_context.hpp>
#endif

#include "engine/sandbox/voxel_data.hpp"

namespace engine::sandbox::gl {
#if ENGINE_HAS_GL
class VoxelDataTexture
{
public:
  constexpr VoxelDataTexture() noexcept = delete;

  constexpr VoxelDataTexture(VoxelDataTexture const&) noexcept = delete;

  VoxelDataTexture(VoxelDataTexture&& other) noexcept;

  virtual ~VoxelDataTexture() noexcept;

  constexpr auto operator=(VoxelDataTexture const&) & noexcept -> VoxelDataTexture& = delete;

  constexpr auto operator=(VoxelDataTexture&&) & noexcept -> VoxelDataTexture& = delete;

  auto bind(GLuint unit) const& noexcept -> void;

  static auto new_(graphics::gl::Context& gl, std::span<f32 const, VoxelData::SIZE> data) noexcept -> VoxelDataTexture;

  static auto new_pointer(graphics::gl::Context& gl, std::span<f32 const, VoxelData::SIZE> data) noexcept
    -> VoxelDataTexture*;

protected:
private:
  graphics::gl::Context& m_gl;
  GLuint m_texture = 0;

  constexpr explicit VoxelDataTexture(graphics::gl::Context& gl) noexcept
    : m_gl(gl)
  {}

  auto drop() & noexcept -> void;

  auto initialize(std::span<f32 const, VoxelData::SIZE> data) & noexcept -> void;
};
#endif
} // namespace engine::sandbox::gl

#pragma once

#include <engine/platform.h>

#include "engine/sandbox/gl/transform.hpp"
#include "engine/sandbox/gl/triangulation_texture.hpp"
#include "engine/sandbox/gl/voxel_data_model.hpp"
#include "engine/sandbox/gl/voxel_data_texture.hpp"
#include "engine/sandbox/object_matrices.hpp"

namespace engine::sandbox::gl {
#if ENGINE_HAS_GL
class VoxelDataObject
{
public:
  constexpr VoxelDataObject() noexcept = delete;

  constexpr VoxelDataObject(VoxelDataObject const&) noexcept = delete;

  VoxelDataObject(VoxelDataObject&& other) noexcept;

  virtual ~VoxelDataObject() noexcept;

  constexpr auto operator=(VoxelDataObject const&) & noexcept -> VoxelDataObject& = delete;

  constexpr auto operator=(VoxelDataObject&&) & noexcept -> VoxelDataObject& = delete;

  auto draw() const& noexcept -> void;

  auto late_update(f64 delta_time, f64 time) & noexcept -> void;

  static auto new_(graphics::gl::Context& gl,
		   std::span<f32 const, VoxelData::SIZE> data,
		   VoxelDataModel& model,
		   TriangulationTexture& triangulation_texture) noexcept -> VoxelDataObject;

  static auto new_pointer(graphics::gl::Context& gl,
			  std::span<f32 const, VoxelData::SIZE> data,
			  VoxelDataModel& model,
			  TriangulationTexture& triangulation_texture) noexcept -> VoxelDataObject*;

  constexpr auto transform() & noexcept -> Transform& { return m_transform; }

  [[nodiscard]] constexpr auto transform() const& noexcept -> Transform const& { return m_transform; }

protected:
private:
  static constexpr GLbitfield const M_MAPPING_FLAGS =
    GL_MAP_COHERENT_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_READ_BIT | GL_MAP_WRITE_BIT;
  static constexpr GLbitfield const M_STORAGE_FLAGS = GL_DYNAMIC_STORAGE_BIT | M_MAPPING_FLAGS;

  std::unique_ptr<VoxelDataTexture> m_data_texture;
  graphics::gl::Context& m_gl;
  ObjectMatrices* m_matrices = nullptr;
  VoxelDataModel& m_model;
  Transform m_transform = Transform::new_();
  TriangulationTexture& m_triangulation_texture;
  GLuint m_ubo = GL_NONE;

  constexpr explicit VoxelDataObject(graphics::gl::Context& gl,
				     VoxelDataModel& model,
				     TriangulationTexture& triangulation_texture) noexcept
    : m_gl(gl)
    , m_model(model)
    , m_triangulation_texture(triangulation_texture)
  {}

  auto create_model() & noexcept -> void;

  auto drop() & noexcept -> void;

  auto initialize(std::span<f32 const, VoxelData::SIZE> data) & noexcept -> void;
};
#endif
} // namespace engine::sandbox::gl

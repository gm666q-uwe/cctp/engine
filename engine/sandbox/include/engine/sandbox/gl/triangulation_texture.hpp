#pragma once

#include <engine/platform.h>

#if ENGINE_HAS_GL
#include <engine/graphics/gl/graphics_context.hpp>
#endif

namespace engine::sandbox::gl {
#if ENGINE_HAS_GL
class TriangulationTexture
{
public:
  constexpr TriangulationTexture() noexcept = delete;

  constexpr TriangulationTexture(TriangulationTexture const&) noexcept = delete;

  TriangulationTexture(TriangulationTexture&& other) noexcept;

  virtual ~TriangulationTexture() noexcept;

  constexpr auto operator=(TriangulationTexture const&) & noexcept -> TriangulationTexture& = delete;

  constexpr auto operator=(TriangulationTexture&&) & noexcept -> TriangulationTexture& = delete;

  auto bind(GLuint unit) const& noexcept -> void;

  static auto new_(graphics::gl::Context& gl) noexcept -> TriangulationTexture;

  static auto new_pointer(graphics::gl::Context& gl) noexcept -> TriangulationTexture*;

protected:
private:
  graphics::gl::Context& m_gl;
  GLuint m_texture = 0;

  constexpr explicit TriangulationTexture(graphics::gl::Context& gl) noexcept
    : m_gl(gl)
  {}

  auto drop() & noexcept -> void;

  auto initialize() & noexcept -> void;
};
#endif
} // namespace engine::sandbox::gl

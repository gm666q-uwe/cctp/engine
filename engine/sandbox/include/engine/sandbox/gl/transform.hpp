#pragma once

#include "engine/sandbox/transform.hpp"

namespace engine::sandbox::gl {
using Transform = sandbox::Transform<true>;
}

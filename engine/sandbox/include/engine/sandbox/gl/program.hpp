#pragma once

#include <span>

#include <engine/platform.h>

#if ENGINE_HAS_GL
#include <engine/graphics/gl/graphics_context.hpp>
#endif

namespace engine::sandbox::gl {
#if ENGINE_HAS_GL
class Program
{
public:
  constexpr Program() noexcept = delete;

  constexpr Program(Program const&) noexcept = delete;

  Program(Program&& other) noexcept;

  virtual ~Program() noexcept;

  constexpr auto operator=(Program const&) & noexcept -> Program& = delete;

  constexpr auto operator=(Program&&) & noexcept -> Program& = delete;

  static auto new_(graphics::gl::Context& gl,
		   std::span<uint32_t const> fragment_binary,
		   std::span<uint32_t const> geometry_binary,
		   std::span<uint32_t const> vertex_binary) noexcept -> Program;

  static auto new_pointer(graphics::gl::Context& gl,
			  std::span<uint32_t const> fragment_binary,
			  std::span<uint32_t const> geometry_binary,
			  std::span<uint32_t const> vertex_binary) noexcept -> Program*;

  auto use() const& noexcept -> void;

protected:
private:
  graphics::gl::Context& m_gl;
  GLuint m_program = 0;

  constexpr explicit Program(graphics::gl::Context& gl) noexcept
    : m_gl(gl)
  {}

  [[nodiscard]] auto create_shader(GLenum type, std::span<uint32_t const> binary) const& noexcept -> GLuint;

  auto drop() & noexcept -> void;

  auto initialize(std::span<uint32_t const> fragment_binary,
		  std::span<uint32_t const> geometry_binary,
		  std::span<uint32_t const> vertex_binary) & noexcept -> void;
};
#endif
} // namespace engine::sandbox::gl

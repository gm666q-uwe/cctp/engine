#pragma once

#include <engine/platform.h>

#if ENGINE_HAS_GL
#include <engine/graphics/gl/graphics_context.hpp>
#endif

#include "engine/sandbox/voxel_data.hpp"

namespace engine::sandbox::gl {
#if ENGINE_HAS_GL
class VoxelDataModel
{
public:
  constexpr VoxelDataModel() noexcept = delete;

  constexpr VoxelDataModel(VoxelDataModel const&) noexcept = delete;

  VoxelDataModel(VoxelDataModel&& other) noexcept;

  virtual ~VoxelDataModel() noexcept;

  constexpr auto operator=(VoxelDataModel const&) & noexcept -> VoxelDataModel& = delete;

  constexpr auto operator=(VoxelDataModel&&) & noexcept -> VoxelDataModel& = delete;

  auto draw() const& noexcept -> void;

  static auto new_(graphics::gl::Context& gl) noexcept -> VoxelDataModel;

  static auto new_pointer(graphics::gl::Context& gl) noexcept -> VoxelDataModel*;

protected:
private:
  static constexpr usize const M_DEPTH = 32;
  static constexpr usize const M_HEIGHT = 32;
  static constexpr usize const M_WIDTH = 32;
  static constexpr usize const M_SIZE = M_WIDTH * M_HEIGHT * M_DEPTH;

  static constexpr f32 const M_STEP_X = 2.0f / static_cast<f32>(M_WIDTH);
  static constexpr f32 const M_STEP_Y = 2.0f / static_cast<f32>(M_HEIGHT);
  static constexpr f32 const M_STEP_Z = 2.0f / static_cast<f32>(M_DEPTH);

  graphics::gl::Context& m_gl;
  GLuint m_vao = GL_NONE;
  GLuint m_vbo = GL_NONE;

  constexpr explicit VoxelDataModel(graphics::gl::Context& gl) noexcept
    : m_gl(gl)
  {}

  auto drop() & noexcept -> void;

  auto initialize() & noexcept -> void;
};
#endif
} // namespace engine::sandbox::gl

#pragma once

#include <engine/platform.h>

#if ENGINE_HAS_GL
#include <engine/graphics/gl/graphics_context.hpp>
#endif

#include <engine/input/manager.hpp>

#include "engine/sandbox/camera_matrices.hpp"
#include "engine/sandbox/gl/transform.hpp"

namespace engine::sandbox::gl {
#if ENGINE_HAS_GL
class Camera
{
public:
  constexpr Camera() noexcept = delete;

  constexpr Camera(Camera const&) noexcept = delete;

  Camera(Camera&& other) noexcept;

  virtual ~Camera() noexcept;

  constexpr auto operator=(Camera const&) & noexcept -> Camera& = delete;

  constexpr auto operator=(Camera&&) & noexcept -> Camera& = delete;

  auto bind(GLuint index) const& noexcept -> void;

  auto fixed_update(f64 fixed_delta_time, f64 fixed_time) & noexcept -> void;

  auto late_update(f64 delta_time, f64 time) & noexcept -> void;

  static auto new_(graphics::gl::Context& gl, input::Manager& im) noexcept -> Camera;

  static auto new_pointer(graphics::gl::Context& gl, input::Manager& im) noexcept -> Camera*;

  auto on_resize(u16 height, u16 width) & noexcept -> void;

  auto update(f64 delta_time, f64 time) & noexcept -> void;

protected:
private:
  struct Input
  {
    usize look_x = 0;
    usize look_y = 0;
    usize move_x = 0;
    usize move_y = 0;
    usize move_z = 0;
  };

  static constexpr GLbitfield const M_MAPPING_FLAGS =
    GL_MAP_COHERENT_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_READ_BIT | GL_MAP_WRITE_BIT;
  static constexpr GLbitfield const M_STORAGE_FLAGS = GL_DYNAMIC_STORAGE_BIT | M_MAPPING_FLAGS;

  static constexpr f32 const M_FOV = 60.0f;
  static constexpr f32 const M_LOOK_SPEED = 4.0f;
  static constexpr f32 const M_MOVE_SPEED = 2.0f;
  static constexpr f32 const M_Z_FAR = 1000.0f;
  static constexpr f32 const M_Z_NEAR = 0.3f;

  f32 m_fov = M_FOV;
  graphics::gl::Context& m_gl;
  input::Manager& m_im;
  Input m_input = Input{};
  CameraMatrices* m_matrices = nullptr;
  glm::vec3 m_rotation = glm::vec3{};
  Transform m_transform = Transform::new_();
  GLuint m_ubo = GL_NONE;
  // bool m_view_changed = false;
  f32 m_z_far = M_Z_FAR;
  f32 m_z_near = M_Z_NEAR;

  constexpr explicit Camera(graphics::gl::Context& gl, input::Manager& im) noexcept
    : m_gl(gl)
    , m_im(im)
  {}

  auto create_projection(f32 height, f32 width) & noexcept -> void;

  auto create_view() & noexcept -> void;

  auto drop() & noexcept -> void;

  auto initialize() & noexcept -> void;
};
#endif
} // namespace engine::sandbox::gl

#pragma once

#include <engine/graphics/glm.hpp>

namespace engine::sandbox {
struct ObjectMatrices
{
public:
  glm::mat4 model;

protected:
private:
};
} // namespace engine::tmp

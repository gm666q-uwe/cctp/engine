#pragma once

#include <span>

#include <engine/platform.h>

#include "engine/sandbox/vulkan/utility.hpp"

namespace engine::sandbox::vulkan {
#if ENGINE_HAS_VULKAN
class GraphicsPipeline
{
public:
  constexpr GraphicsPipeline() noexcept = delete;

  constexpr GraphicsPipeline(GraphicsPipeline const&) noexcept = delete;

  GraphicsPipeline(GraphicsPipeline&& other) noexcept;

  virtual ~GraphicsPipeline() noexcept;

  constexpr auto operator=(GraphicsPipeline const&) & noexcept -> GraphicsPipeline& = delete;

  constexpr auto operator=(GraphicsPipeline&&) & noexcept -> GraphicsPipeline& = delete;

  auto bind(VkCommandBuffer command_buffer, VkExtent2D const& extent) const& noexcept -> void;

  [[nodiscard]] constexpr auto descriptor_set_layout() const& noexcept -> VkDescriptorSetLayout
  {
    return m_descriptor_set_layout;
  }

  static auto new_(graphics::vulkan::Device& device,
		   VkRenderPass render_pass,
		   std::span<uint32_t const> fragment_binary,
		   std::span<uint32_t const> geometry_binary,
		   std::span<uint32_t const> vertex_binary) noexcept -> GraphicsPipeline;

  static auto new_pointer(graphics::vulkan::Device& device,
			  VkRenderPass render_pass,
			  std::span<uint32_t const> fragment_binary,
			  std::span<uint32_t const> geometry_binary,
			  std::span<uint32_t const> vertex_binary) noexcept -> GraphicsPipeline*;

  [[nodiscard]] constexpr auto pipeline_layout() const& noexcept -> VkPipelineLayout { return m_pipeline_layout; }

protected:
private:
  VkDescriptorSetLayout m_descriptor_set_layout = VK_NULL_HANDLE;
  graphics::vulkan::Device& m_device;
  VkShaderModule m_fragment_module = VK_NULL_HANDLE;
  VkShaderModule m_geometry_module = VK_NULL_HANDLE;
  VkPipeline m_pipeline = VK_NULL_HANDLE;
  VkPipelineLayout m_pipeline_layout = VK_NULL_HANDLE;
  VkShaderModule m_vertex_module = VK_NULL_HANDLE;

  constexpr explicit GraphicsPipeline(graphics::vulkan::Device& device) noexcept
    : m_device(device)
  {}

  auto create_descriptor_set_layout() & noexcept -> void;

  auto create_graphics_pipeline(VkRenderPass render_pass) & noexcept -> void;

  auto create_pipeline_layout() & noexcept -> void;

  [[nodiscard]] auto create_shader_module(std::span<uint32_t const> binary) const& noexcept -> VkShaderModule;

  auto drop() & noexcept -> void;

  auto initialize(VkRenderPass render_pass,
		  std::span<uint32_t const> fragment_binary,
		  std::span<uint32_t const> geometry_binary,
		  std::span<uint32_t const> vertex_binary) & noexcept -> void;
};
#endif
} // namespace engine::sandbox::vulkan

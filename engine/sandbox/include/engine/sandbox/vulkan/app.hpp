#pragma once

#include <engine/app.hpp>
#include <engine/display/display.hpp>

#include "engine/sandbox/vulkan/camera.hpp"
#include "engine/sandbox/vulkan/graphics_pipeline.hpp"
#include "engine/sandbox/vulkan/voxel_data_object.hpp"

namespace engine::sandbox::vulkan {
#if ENGINE_HAS_VULKAN
class App final : public engine::App
{
public:
  constexpr App(App const&) noexcept = delete;

  constexpr App(App&&) noexcept = delete;

  ~App() noexcept override;

  constexpr auto operator=(App const&) & noexcept -> App& = delete;

  constexpr auto operator=(App&&) & noexcept -> App& = delete;

  static auto new_(display::Display* display) noexcept -> App;

  static auto new_pointer(display::Display* display) noexcept -> App*;

protected:
  auto fixed_update(f64 fixed_delta_time, f64 fixed_time) & noexcept -> void override;

  auto init(std::span<CStr> args) & noexcept -> void override;

  auto input() & noexcept -> void override;

  auto late_update(f64 delta_time, f64 time) & noexcept -> void override;

  auto render(f64 time) & noexcept -> void override;

  [[nodiscard]] auto result() const& noexcept -> int override;

  [[nodiscard]] auto running() const& noexcept -> bool override;

  auto update(f64 delta_time, f64 time) & noexcept -> void override;

private:
  static constexpr i16 const CHUNKS_RADIUS = 2;
  static constexpr i16 const CHUNK_COUNT = (CHUNKS_RADIUS * 2 + 1) * (CHUNKS_RADIUS * 2 + 1);

  usize m_action_quit = 0;
  std::unique_ptr<Camera> m_camera = std::unique_ptr<Camera>();
  VkDescriptorPool m_descriptor_pool = VK_NULL_HANDLE;
  std::unique_ptr<display::Display> m_display = std::unique_ptr<display::Display>();
  std::unique_ptr<GraphicsPipeline> m_graphics_pipeline = std::unique_ptr<GraphicsPipeline>();
  int m_result = 0;
  bool m_running = true;
  std::unique_ptr<TriangulationTexture> m_triangulation_texture = std::unique_ptr<TriangulationTexture>();
  std::unique_ptr<VoxelDataModel> m_voxel_data_model = std::unique_ptr<VoxelDataModel>();
  std::vector<VoxelDataObject> m_voxel_data_objects = std::vector<VoxelDataObject>();
  std::unique_ptr<display::Window> m_window = std::unique_ptr<display::Window>();

  explicit App(display::Display* display) noexcept;
};
#endif
} // namespace engine::sandbox::vulkan

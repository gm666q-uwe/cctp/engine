#pragma once

#include <engine/platform.h>

#if ENGINE_HAS_VULKAN
#include <engine/graphics/vulkan/graphics_context.hpp>
#endif

namespace engine::sandbox::vulkan {
#if ENGINE_HAS_VULKAN
auto
memory_type_from_properties(graphics::vulkan::Device& device,
			    uint32_t type_bits,
			    VkFlags requirements_mask,
			    uint32_t* type_index) noexcept -> bool;
#endif
} // namespace engine::sandbox::vulkan

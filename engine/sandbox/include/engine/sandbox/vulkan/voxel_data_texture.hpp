#pragma once

#include <span>

#include "engine/sandbox/voxel_data.hpp"
#include "engine/sandbox/vulkan/utility.hpp"

namespace engine::sandbox::vulkan {
#if ENGINE_HAS_VULKAN
class VoxelDataTexture
{
public:
  constexpr VoxelDataTexture() noexcept = delete;

  constexpr VoxelDataTexture(VoxelDataTexture const&) noexcept = delete;

  VoxelDataTexture(VoxelDataTexture&& other) noexcept;

  virtual ~VoxelDataTexture() noexcept;

  constexpr auto operator=(VoxelDataTexture const&) & noexcept -> VoxelDataTexture& = delete;

  constexpr auto operator=(VoxelDataTexture&&) & noexcept -> VoxelDataTexture& = delete;

  auto bind(VkDescriptorImageInfo& descriptor_image_info) const& noexcept -> void;

  static auto new_(graphics::vulkan::Device& device, std::span<f32 const, VoxelData::SIZE> data) noexcept -> VoxelDataTexture;

  static auto new_pointer(graphics::vulkan::Device& device, std::span<f32 const, VoxelData::SIZE> data) noexcept
    -> VoxelDataTexture*;

protected:
private:
  graphics::vulkan::Device& m_device;
  VkImage m_image = VK_NULL_HANDLE;
  VkDeviceMemory m_image_memory = VK_NULL_HANDLE;
  VkImageView m_image_view = VK_NULL_HANDLE;
  VkSampler m_sampler = VK_NULL_HANDLE;

  constexpr explicit VoxelDataTexture(graphics::vulkan::Device& device) noexcept
    : m_device(device)
  {}

  auto drop() & noexcept -> void;

  auto initialize(std::span<f32 const, VoxelData::SIZE> data) & noexcept -> void;
};
#endif
} // namespace engine::sandbox::gl

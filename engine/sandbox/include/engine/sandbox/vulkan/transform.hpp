#pragma once

#include "engine/sandbox/transform.hpp"

namespace engine::sandbox::vulkan {
using Transform = sandbox::Transform<false>;
}

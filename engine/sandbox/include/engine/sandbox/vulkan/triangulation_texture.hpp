#pragma once

#include "engine/sandbox/vulkan/utility.hpp"

namespace engine::sandbox::vulkan {
#if ENGINE_HAS_VULKAN
class TriangulationTexture
{
public:
  constexpr TriangulationTexture() noexcept = delete;

  constexpr TriangulationTexture(TriangulationTexture const&) noexcept = delete;

  TriangulationTexture(TriangulationTexture&& other) noexcept;

  virtual ~TriangulationTexture() noexcept;

  constexpr auto operator=(TriangulationTexture const&) & noexcept -> TriangulationTexture& = delete;

  constexpr auto operator=(TriangulationTexture&&) & noexcept -> TriangulationTexture& = delete;

  auto bind(VkDescriptorImageInfo& descriptor_image_info) const& noexcept -> void;

  static auto new_(graphics::vulkan::Device& device) noexcept -> TriangulationTexture;

  static auto new_pointer(graphics::vulkan::Device& device) noexcept -> TriangulationTexture*;

protected:
private:
  graphics::vulkan::Device& m_device;
  VkImage m_image = VK_NULL_HANDLE;
  VkDeviceMemory m_image_memory = VK_NULL_HANDLE;
  VkImageView m_image_view = VK_NULL_HANDLE;
  VkSampler m_sampler = VK_NULL_HANDLE;

  constexpr explicit TriangulationTexture(graphics::vulkan::Device& device) noexcept
    : m_device(device)
  {}

  auto drop() & noexcept -> void;

  auto initialize() & noexcept -> void;
};
#endif
} // namespace engine::sandbox::vulkan

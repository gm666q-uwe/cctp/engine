#pragma once

#include <engine/platform.h>

#include "engine/sandbox/object_matrices.hpp"
#include "engine/sandbox/vulkan/transform.hpp"
#include "engine/sandbox/vulkan/triangulation_texture.hpp"
#include "engine/sandbox/vulkan/voxel_data_model.hpp"
#include "engine/sandbox/vulkan/voxel_data_texture.hpp"

namespace engine::sandbox::vulkan {
#if ENGINE_HAS_VULKAN
class VoxelDataObject
{
public:
  constexpr VoxelDataObject() noexcept = delete;

  constexpr VoxelDataObject(VoxelDataObject const&) noexcept = delete;

  VoxelDataObject(VoxelDataObject&& other) noexcept;

  virtual ~VoxelDataObject() noexcept;

  constexpr auto operator=(VoxelDataObject const&) & noexcept -> VoxelDataObject& = delete;

  constexpr auto operator=(VoxelDataObject&&) & noexcept -> VoxelDataObject& = delete;

  auto draw(VkCommandBuffer command_buffer, VkPipelineLayout pipeline_layout) const& noexcept -> void;

  auto late_update(f64 delta_time, f64 time) & noexcept -> void;

  static auto new_(graphics::vulkan::Device& device,
		   std::span<f32 const, VoxelData::SIZE> data,
		   std::span<VkDescriptorBufferInfo> descriptor_buffer_infos,
		   std::span<VkDescriptorImageInfo> descriptor_image_infos,
		   VkDescriptorSet descriptor_set,
		   VoxelDataModel& model,
		   TriangulationTexture& triangulation_texture) noexcept -> VoxelDataObject;

  static auto new_pointer(graphics::vulkan::Device& device,
			  std::span<f32 const, VoxelData::SIZE> data,
			  std::span<VkDescriptorBufferInfo> descriptor_buffer_infos,
			  std::span<VkDescriptorImageInfo> descriptor_image_infos,
			  VkDescriptorSet descriptor_set,
			  VoxelDataModel& model,
			  TriangulationTexture& triangulation_texture) noexcept -> VoxelDataObject*;

  constexpr auto transform() & noexcept -> Transform& { return m_transform; }

  [[nodiscard]] constexpr auto transform() const& noexcept -> Transform const& { return m_transform; }

protected:
private:
  std::unique_ptr<VoxelDataTexture> m_data_texture;
  VkDescriptorSet m_descriptor_set = VK_NULL_HANDLE;
  graphics::vulkan::Device& m_device;
  ObjectMatrices* m_matrices = nullptr;
  VoxelDataModel& m_model;
  Transform m_transform = Transform::new_();
  TriangulationTexture& m_triangulation_texture;
  VkBuffer m_uniform_buffer = VK_NULL_HANDLE;
  VkDeviceMemory m_uniform_buffer_memory = VK_NULL_HANDLE;

  constexpr explicit VoxelDataObject(graphics::vulkan::Device& device,
				     VkDescriptorSet descriptor_set,
				     VoxelDataModel& model,
				     TriangulationTexture& triangulation_texture) noexcept
    : m_descriptor_set(descriptor_set)
    , m_device(device)
    , m_model(model)
    , m_triangulation_texture(triangulation_texture)
  {}

  auto bind(VkDescriptorBufferInfo& descriptor_buffer_info,
	    VkDescriptorImageInfo& descriptor_image_info) const& noexcept -> void;

  auto create_model() & noexcept -> void;

  auto drop() & noexcept -> void;

  auto initialize(std::span<f32 const, VoxelData::SIZE> data,
		  std::span<VkDescriptorBufferInfo> descriptor_buffer_infos,
		  std::span<VkDescriptorImageInfo> descriptor_image_infos) & noexcept -> void;
};
#endif
} // namespace engine::sandbox::vulkan

#pragma once

#include <engine/platform.h>

#ifdef __cplusplus
extern "C"
{
#endif

#if ENGINE_HAS_VULKAN
#if ENGINE_IS_WINDOWS
  int engine_sandbox_vulkan_app_run(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nShowCmd);
#else
  int engine_sandbox_vulkan_app_run(int argc, char const** argv);
#endif
#endif

#ifdef __cplusplus
}
#endif

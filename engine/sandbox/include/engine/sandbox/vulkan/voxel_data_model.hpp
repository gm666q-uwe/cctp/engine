#pragma once

#include "engine/sandbox/voxel_data.hpp"
#include "engine/sandbox/vulkan/utility.hpp"

namespace engine::sandbox::vulkan {
#if ENGINE_HAS_VULKAN
class VoxelDataModel
{
public:
  constexpr VoxelDataModel() noexcept = delete;

  constexpr VoxelDataModel(VoxelDataModel const&) noexcept = delete;

  VoxelDataModel(VoxelDataModel&& other) noexcept;

  virtual ~VoxelDataModel() noexcept;

  constexpr auto operator=(VoxelDataModel const&) & noexcept -> VoxelDataModel& = delete;

  constexpr auto operator=(VoxelDataModel&&) & noexcept -> VoxelDataModel& = delete;

  auto draw(VkCommandBuffer command_buffer) const& noexcept -> void;

  static auto new_(graphics::vulkan::Device& device) noexcept -> VoxelDataModel;

  static auto new_pointer(graphics::vulkan::Device& device) noexcept -> VoxelDataModel*;

protected:
private:
  static constexpr usize const M_DEPTH = 32;
  static constexpr usize const M_HEIGHT = 32;
  static constexpr usize const M_WIDTH = 32;
  static constexpr usize const M_SIZE = M_WIDTH * M_HEIGHT * M_DEPTH;

  static constexpr f32 const M_STEP_X = 2.0f / static_cast<f32>(M_WIDTH);
  static constexpr f32 const M_STEP_Y = 2.0f / static_cast<f32>(M_HEIGHT);
  static constexpr f32 const M_STEP_Z = 2.0f / static_cast<f32>(M_DEPTH);

  graphics::vulkan::Device& m_device;
  VkBuffer m_vertex_buffer = VK_NULL_HANDLE;
  VkDeviceMemory m_vertex_buffer_memory = VK_NULL_HANDLE;

  constexpr explicit VoxelDataModel(graphics::vulkan::Device& device) noexcept
    : m_device(device)
  {}

  auto drop() & noexcept -> void;

  auto initialize() & noexcept -> void;
};
#endif
} // namespace engine::sandbox::gl

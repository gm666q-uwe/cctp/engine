#pragma once

#include <engine/graphics/glm.hpp>

namespace engine::sandbox {
template<bool gl>
struct Transform
{
public:
  // constexpr Transform() noexcept = delete;

  constexpr Transform(Transform const&) noexcept = default;

  constexpr Transform(Transform&& other) noexcept = default;

  virtual constexpr ~Transform() noexcept = default;

  constexpr auto operator=(Transform const&) & noexcept -> Transform& = default;

  constexpr auto operator=(Transform&& other) & noexcept -> Transform& = default;

  [[nodiscard]] constexpr auto forward() const& noexcept -> glm::vec3 const& { return m_forward; }

  [[nodiscard]] constexpr auto matrix() const& noexcept -> glm::mat4 const& { return m_matrix; }

  static constexpr auto new_() noexcept -> Transform { return Transform(); }

  static auto new_pointer() noexcept -> Transform* { return new Transform(); }

  [[nodiscard]] constexpr auto position() const& noexcept -> glm::vec3 const& { return m_position; }

  [[nodiscard]] constexpr auto right() const& noexcept -> glm::vec3 const& { return m_right; }

  [[nodiscard]] constexpr auto rotation() const& noexcept -> glm::quat const& { return m_rotation; }

  [[nodiscard]] auto rotation_euler() const& noexcept -> glm::vec3
  {
    auto r = glm::eulerAngles(m_rotation);
    return glm::degrees(glm::vec3(r.x, r.y, r.z));
  }

  [[nodiscard]] constexpr auto scale() const& noexcept -> glm::vec3 const& { return m_scale; }

  constexpr auto set_position(glm::vec3 const& position) & noexcept -> void
  {
    m_position = position;
    recalculate_matrix();
  }

  constexpr auto set_rotation(glm::quat const& rotation) & noexcept -> void
  {
    m_rotation = rotation;
    recalculate_direction();
    recalculate_matrix();
  }

  auto set_rotation_euler(glm::vec3 const& rotation) & noexcept -> void
  {
    auto r = glm::radians(rotation);
    set_rotation(glm::toQuat(glm::eulerAngleZYX(r.z, r.y, r.x)));
  }

  constexpr auto set_scale(glm::vec3 const& scale) & noexcept -> void
  {
    m_scale = scale;
    recalculate_matrix();
  }

  [[nodiscard]] constexpr auto up() const& noexcept -> glm::vec3 const& { return m_up; }

protected:
private:
  static constexpr glm::vec3 const M_FORWARD = glm::vec3(0.0f, 0.0f, -1.0f);
  static constexpr glm::vec3 const M_RIGHT = glm::vec3(1.0f, 0.0f, 0.0f);
  static constexpr glm::vec3 const M_UP = glm::vec3(0.0f, gl ? 1.0f : -1.0f, 0.0f);

  glm::vec3 m_forward = M_FORWARD;
  glm::mat4 m_matrix = glm::identity<glm::mat4>();
  glm::vec3 m_position = glm::vec3(0.0f, 0.0f, 0.0f);
  glm::vec3 m_right = M_RIGHT;
  glm::quat m_rotation = glm::identity<glm::quat>();
  glm::vec3 m_scale = glm::vec3(1.0f, 1.0f, 1.0f);
  glm::vec3 m_up = M_UP;

  constexpr Transform() noexcept = default;

  auto recalculate_direction() & noexcept -> void
  {
    auto rotation_matrix = glm::toMat3(m_rotation);
    m_forward = -rotation_matrix[2];
    m_right = rotation_matrix[0];
    m_up = gl ? rotation_matrix[1] : -rotation_matrix[1];
  }

  auto recalculate_matrix() & noexcept -> void
  {
    m_matrix = glm::translate(m_position) * glm::toMat4(m_rotation) * glm::scale(m_scale);
  }
};
} // namespace engine::sandbox

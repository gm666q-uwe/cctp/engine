#pragma once

#include <engine/graphics/glm.hpp>

namespace engine::sandbox {
struct VoxelData
{
public:
  static constexpr usize const DEPTH = 64;
  static constexpr usize const HEIGHT = 64;
  static constexpr usize const WIDTH = 64;
  static constexpr usize const SIZE = WIDTH * HEIGHT * DEPTH;

  std::array<f32, SIZE> data;

  [[nodiscard]] constexpr auto at(usize x, usize y, usize z) & noexcept -> f32&
  {
    return data[x * HEIGHT * DEPTH + y * DEPTH + z];
  }

  [[nodiscard]] constexpr auto at(usize x, usize y, usize z) const& noexcept -> f32 const&
  {
    return data[x * HEIGHT * DEPTH + y * DEPTH + z];
  }

protected:
private:
};
} // namespace engine::sandbox

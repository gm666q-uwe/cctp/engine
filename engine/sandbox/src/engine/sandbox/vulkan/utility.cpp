#include "engine/sandbox/vulkan/utility.hpp"

auto
engine::sandbox::vulkan::memory_type_from_properties(engine::graphics::vulkan::Device& device,
						     uint32_t type_bits,
						     VkFlags requirements_mask,
						     uint32_t* type_index) noexcept -> bool
{
  for (uint32_t i = 0; i < VK_MAX_MEMORY_TYPES; i++) {
    if ((type_bits & 1) == 1) {
      if ((device.memory_properties().memoryTypes[i].propertyFlags & requirements_mask) == requirements_mask) {
	*type_index = i;
	return true;
      }
    }
    type_bits >>= 1;
  }
  return false;
}

#include "engine/sandbox/vulkan/triangulation_texture.hpp"

#include "engine/sandbox/triangulation_table.hpp"

#if ENGINE_HAS_VULKAN
engine::sandbox::vulkan::TriangulationTexture::TriangulationTexture(
  engine::sandbox::vulkan::TriangulationTexture&& other) noexcept
  : m_device(other.m_device)
  , m_image(std::exchange(other.m_image, VK_NULL_HANDLE))
  , m_image_memory(std::exchange(other.m_image_memory, VK_NULL_HANDLE))
  , m_image_view(std::exchange(other.m_image_view, VK_NULL_HANDLE))
  , m_sampler(std::exchange(other.m_sampler, VK_NULL_HANDLE))
{}

engine::sandbox::vulkan::TriangulationTexture::~TriangulationTexture() noexcept
{
  drop();
}

auto
engine::sandbox::vulkan::TriangulationTexture::bind(VkDescriptorImageInfo& descriptor_image_info) const& noexcept
  -> void
{
  descriptor_image_info.sampler = m_sampler;
  descriptor_image_info.imageView = m_image_view;
  descriptor_image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
}

auto
engine::sandbox::vulkan::TriangulationTexture::drop() & noexcept -> void
{
  if (m_sampler != VK_NULL_HANDLE) {
    vkDestroySampler(m_device.device(), m_sampler, nullptr);
    m_sampler = VK_NULL_HANDLE;
  }
  if (m_image_view != VK_NULL_HANDLE) {
    vkDestroyImageView(m_device.device(), m_image_view, nullptr);
    m_image_view = VK_NULL_HANDLE;
  }
  if (m_image != VK_NULL_HANDLE) {
    vkDestroyImage(m_device.device(), m_image, nullptr);
    m_image = VK_NULL_HANDLE;
  }
  if (m_image_memory != VK_NULL_HANDLE) {
    vkFreeMemory(m_device.device(), m_image_memory, nullptr);
    m_image_memory = VK_NULL_HANDLE;
  }
}

auto
engine::sandbox::vulkan::TriangulationTexture::initialize() & noexcept -> void
{
  auto image_create_info = VkImageCreateInfo{ VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
					      nullptr,
					      0,
					      VK_IMAGE_TYPE_2D,
					      VK_FORMAT_R8_SINT,
					      VkExtent3D{ 16, 256, 1 },
					      1,
					      1,
					      VK_SAMPLE_COUNT_1_BIT,
					      VK_IMAGE_TILING_LINEAR,
					      VK_IMAGE_USAGE_SAMPLED_BIT,
					      VK_SHARING_MODE_EXCLUSIVE,
					      0,
					      nullptr,
					      VK_IMAGE_LAYOUT_PREINITIALIZED };
  vkCreateImage(m_device.device(), &image_create_info, nullptr, &m_image);

  auto memory_requirements = VkMemoryRequirements{};
  vkGetImageMemoryRequirements(m_device.device(), m_image, &memory_requirements);

  auto memory_allocate_info =
    VkMemoryAllocateInfo{ VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO, nullptr, memory_requirements.size, 0 };
  memory_type_from_properties(m_device,
			      memory_requirements.memoryTypeBits,
			      VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			      &memory_allocate_info.memoryTypeIndex);
  vkAllocateMemory(m_device.device(), &memory_allocate_info, nullptr, &m_image_memory);
  vkBindImageMemory(m_device.device(), m_image, m_image_memory, 0);

  auto image_subresource = VkImageSubresource{
    VK_IMAGE_ASPECT_COLOR_BIT,
    0,
    0,
  };
  auto subresource_layout = VkSubresourceLayout{};
  vkGetImageSubresourceLayout(m_device.device(), m_image, &image_subresource, &subresource_layout);

  auto data = static_cast<int8_t*>(nullptr);
  vkMapMemory(m_device.device(), m_image_memory, 0, VK_WHOLE_SIZE, 0, reinterpret_cast<void**>(&data));
  for (auto& row : TRIANGULATION_TABLE) {
    memcpy(data, row.data(), row.size());
    // data += row.size();
    data += subresource_layout.rowPitch;
  }
  vkUnmapMemory(m_device.device(), m_image_memory);

  auto command_buffer = m_device.begin_command_buffer_one().unwrap();

  auto image_memory_barrier = VkImageMemoryBarrier{ VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
						    nullptr,
						    0,
						    0,
						    VK_IMAGE_LAYOUT_PREINITIALIZED,
						    VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
						    VK_QUEUE_FAMILY_IGNORED,
						    VK_QUEUE_FAMILY_IGNORED,
						    m_image,
						    VkImageSubresourceRange{ VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 } };
  vkCmdPipelineBarrier(command_buffer,
		       VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
		       VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT,
		       0,
		       0,
		       nullptr,
		       0,
		       nullptr,
		       1,
		       &image_memory_barrier);

  m_device.end_command_buffer_one(command_buffer).unwrap();

  auto sampler_create_info = VkSamplerCreateInfo{
    VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
    nullptr,
    0,
    VK_FILTER_NEAREST,
    VK_FILTER_NEAREST,
    VK_SAMPLER_MIPMAP_MODE_NEAREST,
    VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
    VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
    VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
    0.0f,
    VK_FALSE,
    1,
    VK_FALSE,
    VK_COMPARE_OP_NEVER,
    -1000.0f,
    1000.0f,
    VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK,
    VK_FALSE,
  };
  vkCreateSampler(m_device.device(), &sampler_create_info, nullptr, &m_sampler);

  auto image_view_create_info = VkImageViewCreateInfo{
    VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
    nullptr,
    0,
    m_image,
    VK_IMAGE_VIEW_TYPE_2D,
    image_create_info.format,
    VkComponentMapping{
      VK_COMPONENT_SWIZZLE_IDENTITY,
      VK_COMPONENT_SWIZZLE_IDENTITY,
      VK_COMPONENT_SWIZZLE_IDENTITY,
      VK_COMPONENT_SWIZZLE_IDENTITY,
    },
    image_memory_barrier.subresourceRange,
  };
  vkCreateImageView(m_device.device(), &image_view_create_info, nullptr, &m_image_view);
}

auto
engine::sandbox::vulkan::TriangulationTexture::new_(engine::graphics::vulkan::Device& device) noexcept
  -> engine::sandbox::vulkan::TriangulationTexture
{
  auto self = TriangulationTexture(device);
  self.initialize();
  return std::move(self);
}

auto
engine::sandbox::vulkan::TriangulationTexture::new_pointer(engine::graphics::vulkan::Device& device) noexcept
  -> engine::sandbox::vulkan::TriangulationTexture*
{
  auto* self = new TriangulationTexture(device);
  self->initialize();
  return self;
}
#endif

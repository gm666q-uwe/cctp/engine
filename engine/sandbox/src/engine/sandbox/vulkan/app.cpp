#include "engine/sandbox/vulkan/app.h"
#include "engine/sandbox/vulkan/app.hpp"

#include <cstdlib>

#include <engine/display/common/window_options.hpp>

#include "engine/sandbox/generator.hpp"

#if ENGINE_IS_WINDOWS
#include <engine/display/win32/display.hpp>
#endif

#if ENGINE_HAS_VULKAN
#include <engine/graphics/vulkan/graphics_context.hpp>
#endif

#if ENGINE_HAS_WAYLAND
#include <engine/display/wayland/display.hpp>
#endif

#if ENGINE_HAS_XLIB
#include <engine/display/xlib/display.hpp>
#endif

#if ENGINE_HAS_VULKAN
#if ENGINE_IS_WINDOWS
int
engine_sandbox_vulkan_app_run(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nShowCmd)
{
  auto options = engine::display::win32::DisplayOptions::new_(hInstance, nShowCmd);
  auto app = engine::sandbox::vulkan::App::new_(engine::display::win32::Display::new_pointer(options).unwrap());
  return app.run(hInstance, hPrevInstance, lpCmdLine, nShowCmd);
}
#else
int
engine_sandbox_vulkan_app_run(int argc, const char** argv)
{
  auto xdg_session_type = engine::CStr(std::getenv("XDG_SESSION_TYPE"));
#if ENGINE_HAS_WAYLAND
  auto use_wayland = xdg_session_type.compare("wayland") == 0;
#else
  auto use_wayland = false;
#endif

  auto display = static_cast<engine::display::Display*>(nullptr);
  if (use_wayland) {
#if ENGINE_HAS_WAYLAND
    auto display_options = engine::display::wayland::DisplayOptions::new_();
    display = engine::display::wayland::Display::new_pointer(display_options).unwrap();
#endif
  } else {
#if ENGINE_HAS_XLIB
    auto display_options = engine::display::xlib::DisplayOptions::new_();
    display = engine::display::xlib::Display::new_pointer(display_options).unwrap();
#endif
  }

  if (display == nullptr) {
    return -1;
  }

  auto app = engine::sandbox::vulkan::App::new_(display);
  return app.run(argc, argv);
}
#endif

// Can't be constexpr because P1004R2 is not supported by GCC and Clang yet.
static /*constexpr*/ std::vector<uint32_t> const MAIN_FRAG =
#include "engine/sandbox/vulkan/Main.frag.inc"
  ;

static /*constexpr*/ std::vector<uint32_t> const MAIN_GEOM =
#include "engine/sandbox/vulkan/Main.geom.inc"
  ;

static /*constexpr*/ std::vector<uint32_t> const MAIN_VERT =
#include "engine/sandbox/vulkan/Main.vert.inc"
  ;

engine::sandbox::vulkan::App::App(engine::display::Display* display) noexcept
  : engine::App()
  , m_display(display)
{}

engine::sandbox::vulkan::App::~App() noexcept
{
  auto& gc = dynamic_cast<graphics::vulkan::GraphicsContext&>(m_window->graphics_context());
  gc.make_current();
  gc.free_command_buffers();
  gc.set_resize_callback(nullptr);

  m_voxel_data_objects.clear();
  if (m_descriptor_pool != VK_NULL_HANDLE) {
    vkDestroyDescriptorPool(gc.device().device(), m_descriptor_pool, nullptr);
  }

  m_graphics_pipeline.reset();
  m_camera.reset();
  m_triangulation_texture.reset();
  m_voxel_data_model.reset();

  m_window.reset();
  m_display.reset();
}

auto
engine::sandbox::vulkan::App::fixed_update(double fixed_delta_time, double fixed_time) & noexcept -> void
{
  m_camera->fixed_update(fixed_delta_time, fixed_time);
}

auto
engine::sandbox::vulkan::App::init(std::span<CStr> args) & noexcept -> void
{
  using namespace std::literals;
  auto window_options = display::common::WindowOptions::new_();
  window_options.graphics_api(display::WindowOptions::GraphicsAPI::Vulkan).width(800).height(600);
  m_window = std::unique_ptr<display::Window>(m_display->create_window_pointer(window_options).unwrap());

  auto& im = m_window->input_manager();
  im.add_axis(engine::input::Axis::new_({ engine::input::Axis::Binding(engine::input::Key::KeyLeft, -10.0f),
					  engine::input::Axis::Binding(engine::input::Key::KeyRight, 10.0f),
					  /*engine::input::Axis::Binding(engine::input::RelativeAxis::LeftX, 1.0f)*/ }),
	      "Look X"sv);
  im.add_axis(engine::input::Axis::new_({ engine::input::Axis::Binding(engine::input::Key::KeyDown, 10.0f),
					  engine::input::Axis::Binding(engine::input::Key::KeyUp, -10.0f),
					  /*engine::input::Axis::Binding(engine::input::RelativeAxis::LeftY, 1.0f)*/ }),
	      "Look Y"sv);
  im.add_axis(engine::input::Axis::new_({ engine::input::Axis::Binding(engine::input::Key::KeyA, -1.0f),
					  engine::input::Axis::Binding(engine::input::Key::KeyD, 1.0f) }),
	      "Move X"sv);
  im.add_axis(engine::input::Axis::new_({ engine::input::Axis::Binding(engine::input::Key::KeyLeftControl, -1.0f),
					  engine::input::Axis::Binding(engine::input::Key::KeyLeftShift, 1.0f) }),
	      "Move Y"sv);
  im.add_axis(engine::input::Axis::new_({ engine::input::Axis::Binding(engine::input::Key::KeyS, -1.0f),
					  engine::input::Axis::Binding(engine::input::Key::KeyW, 1.0f) }),
	      "Move Z"sv);
  im.add_action(engine::input::Action::new_({ engine::input::Action::Binding(engine::input::Key::KeyEscape, 0.5f) }),
		"Quit"sv);
  m_action_quit = im.action_index("Quit").unwrap();

  auto& gc = dynamic_cast<graphics::vulkan::GraphicsContext&>(m_window->graphics_context());
  gc.make_current();

  m_graphics_pipeline = std::unique_ptr<GraphicsPipeline>(
    GraphicsPipeline::new_pointer(gc.device(), gc.render_pass(), MAIN_FRAG, MAIN_GEOM, MAIN_VERT));

  m_camera = std::unique_ptr<Camera>(Camera::new_pointer(gc.device(), im));
  m_triangulation_texture = std::unique_ptr<TriangulationTexture>(TriangulationTexture::new_pointer(gc.device()));
  m_voxel_data_model = std::unique_ptr<VoxelDataModel>(VoxelDataModel::new_pointer(gc.device()));

  auto descriptor_sets = std::array<VkDescriptorSet, CHUNK_COUNT>{};
  {
    auto descriptor_pool_sizes =
      std::array<VkDescriptorPoolSize, 2>{ VkDescriptorPoolSize{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, CHUNK_COUNT * 2 },
					   VkDescriptorPoolSize{ VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
								 CHUNK_COUNT * 2 } };
    auto descriptor_pool_create_info = VkDescriptorPoolCreateInfo{ VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
								   nullptr,
								   0,
								   CHUNK_COUNT,
								   descriptor_pool_sizes.size(),
								   descriptor_pool_sizes.data() };
    vkCreateDescriptorPool(gc.device().device(), &descriptor_pool_create_info, nullptr, &m_descriptor_pool);

    auto descriptor_set_layouts = std::array<VkDescriptorSetLayout, CHUNK_COUNT>{};
    for (auto& descriptor_set_layout : descriptor_set_layouts) {
      descriptor_set_layout = m_graphics_pipeline->descriptor_set_layout();
    }

    auto descriptor_set_allocate_info = VkDescriptorSetAllocateInfo{ VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
								     nullptr,
								     m_descriptor_pool,
								     descriptor_sets.size(),
								     descriptor_set_layouts.data() };
    vkAllocateDescriptorSets(gc.device().device(), &descriptor_set_allocate_info, descriptor_sets.data());
  }

  auto descriptor_buffer_infos =
    std::array<VkDescriptorBufferInfo, 2>{ VkDescriptorBufferInfo{}, VkDescriptorBufferInfo{} };
  m_camera->bind(descriptor_buffer_infos[0]);

  auto descriptor_image_infos =
    std::array<VkDescriptorImageInfo, 2>{ VkDescriptorImageInfo{}, VkDescriptorImageInfo{} };
  m_triangulation_texture->bind(descriptor_image_infos[1]);

  auto generator = Generator::with_seed(std::random_device()());
  auto i = 0;
  for (i16 y = -CHUNKS_RADIUS; y < (CHUNKS_RADIUS + 1); y++) {
    for (i16 x = -CHUNKS_RADIUS; x < (CHUNKS_RADIUS + 1); x++) {
      m_voxel_data_objects.emplace_back(VoxelDataObject::new_(gc.device(),
							      generator.get_chunk(x, y).data,
							      descriptor_buffer_infos,
							      descriptor_image_infos,
							      descriptor_sets[i++],
							      *m_voxel_data_model,
							      *m_triangulation_texture));
      m_voxel_data_objects.back().transform().set_position(glm::vec3(x * 2, 0, y * 2));
      m_voxel_data_objects.back().transform().set_rotation_euler(glm::vec3(0, 90, 0));
    }
  }

  {
    auto* camera = m_camera.get();
    gc.set_resize_callback([=](auto height, auto width) { camera->on_resize(height, width); });
  }
}

auto
engine::sandbox::vulkan::App::input() & noexcept -> void
{
  m_window->input_manager().end_update();
  m_running = m_display->run().unwrap();
  if (m_running) {
    m_running = m_window->run().unwrap();
    if (m_running) {
      m_window->input_manager().begin_update();
    }
  }
}

auto
engine::sandbox::vulkan::App::late_update(double delta_time, double time) & noexcept -> void
{
  m_camera->late_update(delta_time, time);
  for (auto& voxel_data_object : m_voxel_data_objects) {
    voxel_data_object.late_update(delta_time, time);
  }
}

auto
engine::sandbox::vulkan::App::new_(engine::display::Display* display) noexcept -> engine::sandbox::vulkan::App
{
  return App(display);
}

auto
engine::sandbox::vulkan::App::new_pointer(engine::display::Display* display) noexcept -> engine::sandbox::vulkan::App*
{
  return new App(display);
}

auto
engine::sandbox::vulkan::App::render(double time) & noexcept -> void
{
  auto& gc = dynamic_cast<graphics::vulkan::GraphicsContext&>(m_window->graphics_context());
  gc.begin_frame();
  auto is_valid = gc.is_current_command_buffer_valid();
  gc.begin_render_pass(is_valid);

  if (!is_valid) {
    auto& command_buffer = gc.current_command_buffer();
    auto pipeline_layout = m_graphics_pipeline->pipeline_layout();
    m_graphics_pipeline->bind(command_buffer, gc.extent());

    for (auto const& voxel_data_object : m_voxel_data_objects) {
      voxel_data_object.draw(command_buffer, pipeline_layout);
    }
  }

  gc.end_render_pass(is_valid);
  gc.end_frame();
}

auto
engine::sandbox::vulkan::App::result() const& noexcept -> int
{
  return m_result;
}

auto
engine::sandbox::vulkan::App::running() const& noexcept -> bool
{
  return m_running;
}

auto
engine::sandbox::vulkan::App::update(double delta_time, double time) & noexcept -> void
{
  m_camera->update(delta_time, time);
  if (m_window->input_manager().action(m_action_quit).unwrap()->get()) {
    m_running = false;
  }
}
#endif

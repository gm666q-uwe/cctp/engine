#include "engine/sandbox/vulkan/voxel_data_model.hpp"

#include "engine/sandbox/vertex.hpp"

#if ENGINE_HAS_VULKAN
engine::sandbox::vulkan::VoxelDataModel::VoxelDataModel(engine::sandbox::vulkan::VoxelDataModel&& other) noexcept
  : m_device(other.m_device)
  , m_vertex_buffer(std::exchange(other.m_vertex_buffer, VK_NULL_HANDLE))
  , m_vertex_buffer_memory(std::exchange(other.m_vertex_buffer_memory, VK_NULL_HANDLE))
{}

engine::sandbox::vulkan::VoxelDataModel::~VoxelDataModel() noexcept
{
  drop();
}

auto
engine::sandbox::vulkan::VoxelDataModel::draw(VkCommandBuffer command_buffer) const& noexcept -> void
{
  auto device_size = static_cast<VkDeviceSize>(0);
  vkCmdBindVertexBuffers(command_buffer, 0, 1, &m_vertex_buffer, &device_size);
  vkCmdDraw(command_buffer, M_SIZE, 1, 0, 0);
}

auto
engine::sandbox::vulkan::VoxelDataModel::drop() & noexcept -> void
{
  if (m_vertex_buffer != VK_NULL_HANDLE) {
    vkDestroyBuffer(m_device.device(), m_vertex_buffer, nullptr);
    m_vertex_buffer = VK_NULL_HANDLE;
  }
  if (m_vertex_buffer_memory != VK_NULL_HANDLE) {
    vkFreeMemory(m_device.device(), m_vertex_buffer_memory, nullptr);
    m_vertex_buffer_memory = VK_NULL_HANDLE;
  }
}

auto
engine::sandbox::vulkan::VoxelDataModel::initialize() & noexcept -> void
{
  auto vertices = std::vector<Vertex>(M_SIZE);
  {
    auto i = static_cast<usize>(0);
    for (auto z = -1.0f; z < 1.0f; z += M_STEP_Z) {
      for (auto y = -1.0f; y < 1.0f; y += M_STEP_Y) {
	for (auto x = -1.0f; x < 1.0f; x += M_STEP_X) {
	  vertices[i++] = Vertex{ glm::vec3{ x, y, z } };
	}
      }
    }
  }

  auto buffer_create_info = VkBufferCreateInfo{ VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
						nullptr,
						0,
						vertices.size() * sizeof(Vertex),
						VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
						VK_SHARING_MODE_EXCLUSIVE,
						0,
						nullptr };
  vkCreateBuffer(m_device.device(), &buffer_create_info, nullptr, &m_vertex_buffer);

  auto memory_requirements = VkMemoryRequirements{};
  vkGetBufferMemoryRequirements(m_device.device(), m_vertex_buffer, &memory_requirements);

  auto memory_allocate_info =
    VkMemoryAllocateInfo{ VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO, nullptr, memory_requirements.size, 0 };
  if (!memory_type_from_properties(m_device,
				   memory_requirements.memoryTypeBits,
				   VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
				   &memory_allocate_info.memoryTypeIndex)) {
    abort();
  }
  vkAllocateMemory(m_device.device(), &memory_allocate_info, nullptr, &m_vertex_buffer_memory);

  auto data = static_cast<Vertex*>(nullptr);
  vkMapMemory(m_device.device(), m_vertex_buffer_memory, 0, VK_WHOLE_SIZE, 0, reinterpret_cast<void**>(&data));
  memcpy(data, vertices.data(), buffer_create_info.size);
  vkUnmapMemory(m_device.device(), m_vertex_buffer_memory);

  vkBindBufferMemory(m_device.device(), m_vertex_buffer, m_vertex_buffer_memory, 0);
}

auto
engine::sandbox::vulkan::VoxelDataModel::new_(engine::graphics::vulkan::Device& device) noexcept
  -> engine::sandbox::vulkan::VoxelDataModel
{
  auto self = VoxelDataModel(device);
  self.initialize();
  return std::move(self);
}

auto
engine::sandbox::vulkan::VoxelDataModel::new_pointer(engine::graphics::vulkan::Device& device) noexcept
  -> engine::sandbox::vulkan::VoxelDataModel*
{
  auto* self = new VoxelDataModel(device);
  self->initialize();
  return self;
}
#endif

#include "engine/sandbox/vulkan/voxel_data_object.hpp"

#if ENGINE_HAS_VULKAN
engine::sandbox::vulkan::VoxelDataObject::VoxelDataObject(engine::sandbox::vulkan::VoxelDataObject&& other) noexcept
  : m_data_texture(std::move(other.m_data_texture))
  , m_descriptor_set(std::exchange(other.m_descriptor_set, VK_NULL_HANDLE))
  , m_device(other.m_device)
  , m_matrices(std::exchange(other.m_matrices, nullptr))
  , m_model(other.m_model)
  , m_transform(std::exchange(other.m_transform, Transform::new_()))
  , m_triangulation_texture(other.m_triangulation_texture)
  , m_uniform_buffer(std::exchange(other.m_uniform_buffer, VK_NULL_HANDLE))
  , m_uniform_buffer_memory(std::exchange(other.m_uniform_buffer_memory, VK_NULL_HANDLE))
{}

engine::sandbox::vulkan::VoxelDataObject::~VoxelDataObject() noexcept
{
  drop();
}

auto
engine::sandbox::vulkan::VoxelDataObject::bind(VkDescriptorBufferInfo& descriptor_buffer_info,
					       VkDescriptorImageInfo& descriptor_image_info) const& noexcept -> void
{
  descriptor_buffer_info.buffer = m_uniform_buffer;
  descriptor_buffer_info.offset = 0;
  descriptor_buffer_info.range = sizeof(ObjectMatrices);
  m_data_texture->bind(descriptor_image_info);
}

auto
engine::sandbox::vulkan::VoxelDataObject::create_model() & noexcept -> void
{
  m_matrices->model = m_transform.matrix();
}

auto
engine::sandbox::vulkan::VoxelDataObject::draw(VkCommandBuffer command_buffer,
					       VkPipelineLayout pipeline_layout) const& noexcept -> void
{
  vkCmdBindDescriptorSets(
    command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline_layout, 0, 1, &m_descriptor_set, 0, nullptr);
  m_model.draw(command_buffer);
}

auto
engine::sandbox::vulkan::VoxelDataObject::drop() & noexcept -> void
{
  m_data_texture.reset();

  if (m_uniform_buffer != VK_NULL_HANDLE) {
    vkDestroyBuffer(m_device.device(), m_uniform_buffer, nullptr);
    m_uniform_buffer = VK_NULL_HANDLE;
  }
  if (m_matrices != nullptr) {
    vkUnmapMemory(m_device.device(), m_uniform_buffer_memory);
    m_matrices = nullptr;
  }
  if (m_uniform_buffer_memory != VK_NULL_HANDLE) {
    vkFreeMemory(m_device.device(), m_uniform_buffer_memory, nullptr);
    m_uniform_buffer_memory = VK_NULL_HANDLE;
  }
}

auto
engine::sandbox::vulkan::VoxelDataObject::initialize(
  std::span<engine::f32 const, engine::sandbox::VoxelData::SIZE> data,
  std::span<VkDescriptorBufferInfo> descriptor_buffer_infos,
  std::span<VkDescriptorImageInfo> descriptor_image_infos) & noexcept -> void
{
  m_data_texture = std::unique_ptr<VoxelDataTexture>(VoxelDataTexture::new_pointer(m_device, data));

  {
    auto buffer_create_info = VkBufferCreateInfo{ VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
						  nullptr,
						  0,
						  sizeof(ObjectMatrices),
						  VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
						  VK_SHARING_MODE_EXCLUSIVE,
						  0,
						  nullptr };
    vkCreateBuffer(m_device.device(), &buffer_create_info, nullptr, &m_uniform_buffer);

    auto memory_requirements = VkMemoryRequirements{};
    vkGetBufferMemoryRequirements(m_device.device(), m_uniform_buffer, &memory_requirements);

    auto memory_allocate_info =
      VkMemoryAllocateInfo{ VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO, nullptr, memory_requirements.size, 0 };
    if (!memory_type_from_properties(m_device,
				     memory_requirements.memoryTypeBits,
				     VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
				     &memory_allocate_info.memoryTypeIndex)) {
      abort();
    }
    vkAllocateMemory(m_device.device(), &memory_allocate_info, nullptr, &m_uniform_buffer_memory);

    vkMapMemory(m_device.device(), m_uniform_buffer_memory, 0, VK_WHOLE_SIZE, 0, reinterpret_cast<void**>(&m_matrices));
    *m_matrices = ObjectMatrices{ glm::identity<glm::mat4>() };

    vkBindBufferMemory(m_device.device(), m_uniform_buffer, m_uniform_buffer_memory, 0);
  }

  bind(descriptor_buffer_infos[1], descriptor_image_infos[0]);
  auto write_descriptor_sets =
    std::array<VkWriteDescriptorSet, 2>{ VkWriteDescriptorSet{ VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
							       nullptr,
							       m_descriptor_set,
							       0,
							       0,
							       2,
							       VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
							       nullptr,
							       descriptor_buffer_infos.data(),
							       nullptr },
					 VkWriteDescriptorSet{ VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
							       nullptr,
							       m_descriptor_set,
							       2,
							       0,
							       2,
							       VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
							       descriptor_image_infos.data(),
							       nullptr,
							       nullptr } };
  vkUpdateDescriptorSets(m_device.device(), write_descriptor_sets.size(), write_descriptor_sets.data(), 0, nullptr);
}

auto
engine::sandbox::vulkan::VoxelDataObject::late_update(engine::f64 delta_time, engine::f64 time) & noexcept -> void
{
  create_model();
}

auto
engine::sandbox::vulkan::VoxelDataObject::new_(
  engine::graphics::vulkan::Device& device,
  std::span<engine::f32 const, engine::sandbox::VoxelData::SIZE> data,
  std::span<VkDescriptorBufferInfo> descriptor_buffer_infos,
  std::span<VkDescriptorImageInfo> descriptor_image_infos,
  VkDescriptorSet descriptor_set,
  engine::sandbox::vulkan::VoxelDataModel& model,
  engine::sandbox::vulkan::TriangulationTexture& triangulation_texture) noexcept
  -> engine::sandbox::vulkan::VoxelDataObject
{
  auto self = VoxelDataObject(device, descriptor_set, model, triangulation_texture);
  self.initialize(data, descriptor_buffer_infos, descriptor_image_infos);
  return std::move(self);
}

auto
engine::sandbox::vulkan::VoxelDataObject::new_pointer(
  engine::graphics::vulkan::Device& device,
  std::span<engine::f32 const, engine::sandbox::VoxelData::SIZE> data,
  std::span<VkDescriptorBufferInfo> descriptor_buffer_infos,
  std::span<VkDescriptorImageInfo> descriptor_image_infos,
  VkDescriptorSet descriptor_set,
  engine::sandbox::vulkan::VoxelDataModel& model,
  engine::sandbox::vulkan::TriangulationTexture& triangulation_texture) noexcept
  -> engine::sandbox::vulkan::VoxelDataObject*
{
  auto* self = new VoxelDataObject(device, descriptor_set, model, triangulation_texture);
  self->initialize(data, descriptor_buffer_infos, descriptor_image_infos);
  return self;
}
#endif

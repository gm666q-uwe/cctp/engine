#include "engine/sandbox/vulkan/camera.hpp"

#include <iostream>

#if ENGINE_HAS_GL
engine::sandbox::vulkan::Camera::Camera(engine::sandbox::vulkan::Camera&& other) noexcept
  : m_device(other.m_device)
  , m_fov(std::exchange(other.m_fov, M_FOV))
  , m_im(other.m_im)
  , m_input(std::exchange(other.m_input, Input{}))
  , m_matrices(std::exchange(other.m_matrices, nullptr))
  , m_rotation(std::exchange(other.m_rotation, glm::vec3()))
  , m_transform(std::exchange(other.m_transform, Transform::new_()))
  , m_uniform_buffer(std::exchange(other.m_uniform_buffer, VK_NULL_HANDLE))
  , m_uniform_buffer_memory(std::exchange(other.m_uniform_buffer_memory, VK_NULL_HANDLE))
  //, m_view_changed(std::exchange(other.m_view_changed, false))
  , m_z_far(std::exchange(other.m_z_far, M_Z_FAR))
  , m_z_near(std::exchange(other.m_z_near, M_Z_NEAR))
{}

engine::sandbox::vulkan::Camera::~Camera() noexcept
{
  drop();
}

auto
engine::sandbox::vulkan::Camera::bind(VkDescriptorBufferInfo& descriptor_buffer_info) const& noexcept -> void
{
  descriptor_buffer_info.buffer = m_uniform_buffer;
  descriptor_buffer_info.offset = 0;
  descriptor_buffer_info.range = sizeof(CameraMatrices);
}

auto
engine::sandbox::vulkan::Camera::create_projection(engine::f32 height, engine::f32 width) & noexcept -> void
{
  m_matrices->projection = glm::perspective(glm::radians(m_fov), width / height, m_z_near, m_z_far);
}

auto
engine::sandbox::vulkan::Camera::create_view() & noexcept -> void
{
  m_matrices->view = glm::toMat4(glm::conjugate(m_transform.rotation())) * glm::translate(-m_transform.position());
}

auto
engine::sandbox::vulkan::Camera::drop() & noexcept -> void
{
  if (m_uniform_buffer != VK_NULL_HANDLE) {
    vkDestroyBuffer(m_device.device(), m_uniform_buffer, nullptr);
    m_uniform_buffer = VK_NULL_HANDLE;
  }
  if (m_matrices != nullptr) {
    vkUnmapMemory(m_device.device(), m_uniform_buffer_memory);
    m_matrices = nullptr;
  }
  if (m_uniform_buffer_memory != VK_NULL_HANDLE) {
    vkFreeMemory(m_device.device(), m_uniform_buffer_memory, nullptr);
    m_uniform_buffer_memory = VK_NULL_HANDLE;
  }
}

auto
engine::sandbox::vulkan::Camera::fixed_update(engine::f64 fixed_delta_time, engine::f64 fixed_time) & noexcept -> void
{}

auto
engine::sandbox::vulkan::Camera::initialize() & noexcept -> void
{
  m_input = Input{ m_im.axis_index("Look X").unwrap(),
		   m_im.axis_index("Look Y").unwrap(),
		   m_im.axis_index("Move X").unwrap(),
		   m_im.axis_index("Move Y").unwrap(),
		   m_im.axis_index("Move Z").unwrap() };

  auto buffer_create_info = VkBufferCreateInfo{ VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
						nullptr,
						0,
						sizeof(CameraMatrices),
						VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
						VK_SHARING_MODE_EXCLUSIVE,
						0,
						nullptr };
  vkCreateBuffer(m_device.device(), &buffer_create_info, nullptr, &m_uniform_buffer);

  auto memory_requirements = VkMemoryRequirements{};
  vkGetBufferMemoryRequirements(m_device.device(), m_uniform_buffer, &memory_requirements);

  auto memory_allocate_info =
    VkMemoryAllocateInfo{ VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO, nullptr, memory_requirements.size, 0 };
  if (!memory_type_from_properties(m_device,
				   memory_requirements.memoryTypeBits,
				   VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
				   &memory_allocate_info.memoryTypeIndex)) {
    abort();
  }
  vkAllocateMemory(m_device.device(), &memory_allocate_info, nullptr, &m_uniform_buffer_memory);

  vkMapMemory(m_device.device(), m_uniform_buffer_memory, 0, VK_WHOLE_SIZE, 0, reinterpret_cast<void**>(&m_matrices));
  *m_matrices = CameraMatrices{ glm::identity<glm::mat4>(), glm::identity<glm::mat4>() };

  vkBindBufferMemory(m_device.device(), m_uniform_buffer, m_uniform_buffer_memory, 0);

  m_transform.set_position(glm::vec3(0.0f, 1.0f, 0.0f));
}

auto
engine::sandbox::vulkan::Camera::late_update(engine::f64 delta_time, engine::f64 time) & noexcept -> void
{
  create_view();
}

auto
engine::sandbox::vulkan::Camera::new_(engine::graphics::vulkan::Device& device, engine::input::Manager& im) noexcept
  -> engine::sandbox::vulkan::Camera
{
  auto self = Camera(device, im);
  self.initialize();
  return std::move(self);
}

auto
engine::sandbox::vulkan::Camera::new_pointer(engine::graphics::vulkan::Device& device,
					     engine::input::Manager& im) noexcept -> engine::sandbox::vulkan::Camera*
{
  auto* self = new Camera(device, im);
  self->initialize();
  return self;
}

auto
engine::sandbox::vulkan::Camera::on_resize(engine::u16 height, engine::u16 width) & noexcept -> void
{
  create_projection(height, width);
}

auto
engine::sandbox::vulkan::Camera::update(engine::f64 delta_time, engine::f64 time) & noexcept -> void
{
  auto look_x = m_im.axis(m_input.look_x).unwrap()->get();
  auto look_y = m_im.axis(m_input.look_y).unwrap()->get();
  auto move_x = m_im.axis(m_input.move_x).unwrap()->get();
  auto move_y = m_im.axis(m_input.move_y).unwrap()->get();
  auto move_z = m_im.axis(m_input.move_z).unwrap()->get();

  auto look_speed = M_LOOK_SPEED * static_cast<f32>(delta_time);
  auto move_speed = M_MOVE_SPEED * static_cast<f32>(delta_time);

  m_rotation += glm::vec3(-look_y, -look_x, 0.0f) * look_speed;
  m_transform.set_rotation_euler(m_rotation);

  auto move = glm::vec3(move_x, move_y, -move_z);
  if (glm::length2(move) > 1.0f) {
    move = glm::normalize(move);
  }
  auto direction = glm::rotate(m_transform.rotation(), move);
  m_transform.set_position(m_transform.position() + (direction * move_speed));
}
#endif

#include "engine/sandbox/vulkan/graphics_pipeline.hpp"

#include "engine/sandbox/vertex.hpp"

#if ENGINE_HAS_VULKAN
engine::sandbox::vulkan::GraphicsPipeline::GraphicsPipeline(engine::sandbox::vulkan::GraphicsPipeline&& other) noexcept
  : m_device(other.m_device)
  , m_fragment_module(std::exchange(other.m_fragment_module, VK_NULL_HANDLE))
  , m_geometry_module(std::exchange(other.m_geometry_module, VK_NULL_HANDLE))
  , m_pipeline_layout(std::exchange(other.m_pipeline_layout, VK_NULL_HANDLE))
  , m_vertex_module(std::exchange(other.m_vertex_module, VK_NULL_HANDLE))
{}

engine::sandbox::vulkan::GraphicsPipeline::~GraphicsPipeline() noexcept
{
  drop();
}

auto
engine::sandbox::vulkan::GraphicsPipeline::bind(VkCommandBuffer command_buffer,
						VkExtent2D const& extent) const& noexcept -> void
{
  vkCmdBindPipeline(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_pipeline);
  auto scissor = VkRect2D{ VkOffset2D{ 0, 0 }, extent };
  vkCmdSetScissor(command_buffer, 0, 1, &scissor);
  auto viewport =
    VkViewport{ 0.0f, 0.0f, static_cast<float>(extent.width), static_cast<float>(extent.height), 0.0f, 1.0f };
  vkCmdSetViewport(command_buffer, 0, 1, &viewport);
}

auto
engine::sandbox::vulkan::GraphicsPipeline::create_descriptor_set_layout() & noexcept -> void
{
  auto descriptor_set_layout_bindings = std::array<VkDescriptorSetLayoutBinding, 4>{
    VkDescriptorSetLayoutBinding{ 0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_GEOMETRY_BIT, nullptr },
    VkDescriptorSetLayoutBinding{ 1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_GEOMETRY_BIT, nullptr },
    VkDescriptorSetLayoutBinding{
      2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1, VK_SHADER_STAGE_GEOMETRY_BIT, nullptr },
    VkDescriptorSetLayoutBinding{
      3, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1, VK_SHADER_STAGE_GEOMETRY_BIT, nullptr }
  };

  auto descriptor_set_layout_create_info =
    VkDescriptorSetLayoutCreateInfo{ VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
				     nullptr,
				     0,
				     descriptor_set_layout_bindings.size(),
				     descriptor_set_layout_bindings.data() };
  vkCreateDescriptorSetLayout(m_device.device(), &descriptor_set_layout_create_info, nullptr, &m_descriptor_set_layout);
}

auto
engine::sandbox::vulkan::GraphicsPipeline::create_graphics_pipeline(VkRenderPass render_pass) & noexcept -> void
{
  auto color_blend_attachment_states =
    std::array<VkPipelineColorBlendAttachmentState, 1>{ VkPipelineColorBlendAttachmentState{
      VK_FALSE,
      VK_BLEND_FACTOR_ZERO,
      VK_BLEND_FACTOR_ZERO,
      VK_BLEND_OP_ADD,
      VK_BLEND_FACTOR_ZERO,
      VK_BLEND_FACTOR_ZERO,
      VK_BLEND_OP_ADD,
      VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT } };

  auto color_blend_state_create_info =
    VkPipelineColorBlendStateCreateInfo{ VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
					 nullptr,
					 0,
					 VK_FALSE,
					 VK_LOGIC_OP_CLEAR,
					 color_blend_attachment_states.size(),
					 color_blend_attachment_states.data(),
					 { 0.0f, 0.0f, 0.0f, 0.0f } };

  auto depth_stencil_state_create_info = VkPipelineDepthStencilStateCreateInfo{
    VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
    nullptr,
    0,
    VK_TRUE,
    VK_TRUE,
    VK_COMPARE_OP_LESS_OR_EQUAL,
    VK_FALSE,
    VK_FALSE,
    VkStencilOpState{ VK_STENCIL_OP_KEEP, VK_STENCIL_OP_KEEP, VK_STENCIL_OP_KEEP, VK_COMPARE_OP_ALWAYS, 0, 0, 0 },
    VkStencilOpState{ VK_STENCIL_OP_KEEP, VK_STENCIL_OP_KEEP, VK_STENCIL_OP_KEEP, VK_COMPARE_OP_ALWAYS, 0, 0, 0 },
    0.0f,
    1.0f
  };

  auto dynamic_states = std::array<VkDynamicState, 2>{ VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR };

  auto dynamic_state_create_info = VkPipelineDynamicStateCreateInfo{
    VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO, nullptr, 0, dynamic_states.size(), dynamic_states.data()
  };

  auto input_assembly_state_create_info = VkPipelineInputAssemblyStateCreateInfo{
    VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO, nullptr, 0, VK_PRIMITIVE_TOPOLOGY_POINT_LIST, VK_FALSE
  };

  auto multisample_state_create_info =
    VkPipelineMultisampleStateCreateInfo{ VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
					  nullptr,
					  0,
					  VK_SAMPLE_COUNT_1_BIT,
					  VK_FALSE,
					  0.0f,
					  nullptr,
					  VK_FALSE,
					  VK_FALSE };

  auto rasterization_state_create_info =
    VkPipelineRasterizationStateCreateInfo{ VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
					    nullptr,
					    0,
					    VK_FALSE,
					    VK_FALSE,
					    VK_POLYGON_MODE_FILL,
					    VK_CULL_MODE_BACK_BIT,
					    VK_FRONT_FACE_COUNTER_CLOCKWISE,
					    VK_FALSE,
					    0.0f,
					    0.0f,
					    0.0f,
					    1.0f };

  auto stage_create_infos = std::vector<VkPipelineShaderStageCreateInfo>();
  if (m_fragment_module != VK_NULL_HANDLE) {
    stage_create_infos.emplace_back(
      VkPipelineShaderStageCreateInfo{ VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
				       nullptr,
				       0,
				       VK_SHADER_STAGE_FRAGMENT_BIT,
				       m_fragment_module,
				       "main",
				       nullptr });
  }
  if (m_geometry_module != VK_NULL_HANDLE) {
    stage_create_infos.emplace_back(
      VkPipelineShaderStageCreateInfo{ VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
				       nullptr,
				       0,
				       VK_SHADER_STAGE_GEOMETRY_BIT,
				       m_geometry_module,
				       "main",
				       nullptr });
  }
  if (m_vertex_module != VK_NULL_HANDLE) {
    stage_create_infos.emplace_back(
      VkPipelineShaderStageCreateInfo{ VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
				       nullptr,
				       0,
				       VK_SHADER_STAGE_VERTEX_BIT,
				       m_vertex_module,
				       "main",
				       nullptr });
  }

  auto vertex_input_attribute_descriptions = std::array<VkVertexInputAttributeDescription, 1>{
    VkVertexInputAttributeDescription{ 0, 0, VK_FORMAT_R32G32B32_SFLOAT, offsetof(Vertex, position) }
  };

  auto vertex_input_binding_descriptions = std::array<VkVertexInputBindingDescription, 1>{
    VkVertexInputBindingDescription{ 0, sizeof(Vertex), VK_VERTEX_INPUT_RATE_VERTEX }
  };

  auto vertex_input_state_create_info =
    VkPipelineVertexInputStateCreateInfo{ VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
					  nullptr,
					  0,
					  vertex_input_binding_descriptions.size(),
					  vertex_input_binding_descriptions.data(),
					  vertex_input_attribute_descriptions.size(),
					  vertex_input_attribute_descriptions.data() };

  auto viewport_state_create_info = VkPipelineViewportStateCreateInfo{
    VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO, nullptr, 0, 1, nullptr, 1, nullptr
  };

  auto graphics_pipeline_create_info = VkGraphicsPipelineCreateInfo{ VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
								     nullptr,
								     0,
								     static_cast<uint32_t>(stage_create_infos.size()),
								     stage_create_infos.data(),
								     &vertex_input_state_create_info,
								     &input_assembly_state_create_info,
								     nullptr,
								     &viewport_state_create_info,
								     &rasterization_state_create_info,
								     &multisample_state_create_info,
								     &depth_stencil_state_create_info,
								     &color_blend_state_create_info,
								     &dynamic_state_create_info,
								     m_pipeline_layout,
								     render_pass,
								     0,
								     VK_NULL_HANDLE,
								     -1 };
  vkCreateGraphicsPipelines(m_device.device(), VK_NULL_HANDLE, 1, &graphics_pipeline_create_info, nullptr, &m_pipeline);
}

auto
engine::sandbox::vulkan::GraphicsPipeline::create_pipeline_layout() & noexcept -> void
{
  auto pipeline_layout_create_info = VkPipelineLayoutCreateInfo{
    VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO, nullptr, 0, 1, &m_descriptor_set_layout, 0, nullptr
  };
  vkCreatePipelineLayout(m_device.device(), &pipeline_layout_create_info, nullptr, &m_pipeline_layout);
}

auto
engine::sandbox::vulkan::GraphicsPipeline::create_shader_module(std::span<uint32_t const> binary) const& noexcept
  -> VkShaderModule
{
  auto shader_module = static_cast<VkShaderModule>(VK_NULL_HANDLE);
  auto shader_module_create_info = VkShaderModuleCreateInfo{
    VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO, nullptr, 0, binary.size() * sizeof(uint32_t), binary.data()
  };
  vkCreateShaderModule(m_device.device(), &shader_module_create_info, nullptr, &shader_module);

  return shader_module;
}

auto
engine::sandbox::vulkan::GraphicsPipeline::drop() & noexcept -> void
{
  if (m_pipeline != VK_NULL_HANDLE) {
    vkDestroyPipeline(m_device.device(), m_pipeline, nullptr);
    m_pipeline = VK_NULL_HANDLE;
  }
  if (m_pipeline_layout != VK_NULL_HANDLE) {
    vkDestroyPipelineLayout(m_device.device(), m_pipeline_layout, nullptr);
    m_pipeline_layout = VK_NULL_HANDLE;
  }
  if (m_descriptor_set_layout != VK_NULL_HANDLE) {
    vkDestroyDescriptorSetLayout(m_device.device(), m_descriptor_set_layout, nullptr);
    m_descriptor_set_layout = VK_NULL_HANDLE;
  }

  if (m_fragment_module != VK_NULL_HANDLE) {
    vkDestroyShaderModule(m_device.device(), m_fragment_module, nullptr);
    m_fragment_module = VK_NULL_HANDLE;
  }
  if (m_geometry_module != VK_NULL_HANDLE) {
    vkDestroyShaderModule(m_device.device(), m_geometry_module, nullptr);
    m_geometry_module = VK_NULL_HANDLE;
  }
  if (m_vertex_module != VK_NULL_HANDLE) {
    vkDestroyShaderModule(m_device.device(), m_vertex_module, nullptr);
    m_vertex_module = VK_NULL_HANDLE;
  }
}

auto
engine::sandbox::vulkan::GraphicsPipeline::initialize(VkRenderPass render_pass,
						      std::span<uint32_t const> fragment_binary,
						      std::span<uint32_t const> geometry_binary,
						      std::span<uint32_t const> vertex_binary) & noexcept -> void
{
  m_fragment_module = create_shader_module(fragment_binary);
  m_geometry_module = create_shader_module(geometry_binary);
  m_vertex_module = create_shader_module(vertex_binary);

  create_descriptor_set_layout();
  create_pipeline_layout();
  create_graphics_pipeline(render_pass);
}

auto
engine::sandbox::vulkan::GraphicsPipeline::new_(engine::graphics::vulkan::Device& device,
						VkRenderPass render_pass,
						std::span<uint32_t const> fragment_binary,
						std::span<uint32_t const> geometry_binary,
						std::span<uint32_t const> vertex_binary) noexcept
  -> engine::sandbox::vulkan::GraphicsPipeline
{
  auto self = GraphicsPipeline(device);
  self.initialize(render_pass, fragment_binary, geometry_binary, vertex_binary);
  return std::move(self);
}

auto
engine::sandbox::vulkan::GraphicsPipeline::new_pointer(engine::graphics::vulkan::Device& device,
						       VkRenderPass render_pass,
						       std::span<uint32_t const> fragment_binary,
						       std::span<uint32_t const> geometry_binary,
						       std::span<uint32_t const> vertex_binary) noexcept
  -> engine::sandbox::vulkan::GraphicsPipeline*
{
  auto* self = new GraphicsPipeline(device);
  self->initialize(render_pass, fragment_binary, geometry_binary, vertex_binary);
  return self;
}
#endif

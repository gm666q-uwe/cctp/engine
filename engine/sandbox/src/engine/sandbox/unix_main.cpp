#include "engine/sandbox/gl/app.h"
#include "engine/sandbox/vulkan/app.h"

auto
main(int argc, char const** argv) noexcept -> int
{
  auto use_vulkan = true;
  if (use_vulkan) {
    return engine_sandbox_vulkan_app_run(argc, argv);
  } else {
    return engine_sandbox_gl_app_run(argc, argv);
  }
}

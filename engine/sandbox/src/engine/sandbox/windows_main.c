#include "engine/sandbox/gl/app.h"
#include "engine/sandbox/vulkan/app.h"

int WINAPI
wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nShowCmd)
{
  auto use_vulkan = TRUE;
  if (use_vulkan) {
    return engine_sandbox_vulkan_app_run(hInstance, hPrevInstance, lpCmdLine, nShowCmd);
  } else {
    return engine_sandbox_gl_app_run(hInstance, hPrevInstance, lpCmdLine, nShowCmd);
  }
}

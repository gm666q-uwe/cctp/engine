#include "engine/sandbox/gl/program.hpp"

#if ENGINE_HAS_GL
engine::sandbox::gl::Program::Program(engine::sandbox::gl::Program&& other) noexcept
  : m_gl(other.m_gl)
  , m_program(std::exchange(other.m_program, 0))
{}

engine::sandbox::gl::Program::~Program() noexcept
{
  drop();
}

auto
engine::sandbox::gl::Program::create_shader(GLenum type, std::span<uint32_t const> binary) const& noexcept -> GLuint
{
  auto shader = m_gl.createShader(type);
  m_gl.shaderBinary(
    1, &shader, GL_SHADER_BINARY_FORMAT_SPIR_V, binary.data(), static_cast<GLsizei>(binary.size() * sizeof(uint32_t)));
  /*auto spir_v_binary = static_cast<GLint>(GL_FALSE);
  m_gl.getShaderiv(shader, GL_SPIR_V_BINARY, &spir_v_binary);
  if (spir_v_binary != GL_TRUE) {
    m_gl.deleteShader(shader);
    return 0;
  }*/

  m_gl.specializeShader(shader, "main", 0, nullptr, nullptr);
  auto compile_status = static_cast<GLint>(GL_FALSE);
  m_gl.getShaderiv(shader, GL_COMPILE_STATUS, &compile_status);
  if (compile_status != GL_TRUE) {
    m_gl.deleteShader(shader);
    return 0;
  }
  m_gl.attachShader(m_program, shader);

  return shader;
}

auto
engine::sandbox::gl::Program::drop() & noexcept -> void
{
  if (m_program != 0) {
    m_gl.deleteProgram(m_program);
    m_program = 0;
  }
}

auto
engine::sandbox::gl::Program::initialize(std::span<uint32_t const> fragment_binary,
					 std::span<uint32_t const> geometry_binary,
					 std::span<uint32_t const> vertex_binary) & noexcept -> void
{
  m_program = m_gl.createProgram();

  auto fragment_shader = fragment_binary.empty() ? 0 : create_shader(GL_FRAGMENT_SHADER, fragment_binary);
  auto geometry_shader = geometry_binary.empty() ? 0 : create_shader(GL_GEOMETRY_SHADER, geometry_binary);
  auto vertex_shader = vertex_binary.empty() ? 0 : create_shader(GL_VERTEX_SHADER, vertex_binary);

  m_gl.linkProgram(m_program);
  auto link_status = static_cast<GLint>(GL_FALSE);
  m_gl.getProgramiv(m_program, GL_LINK_STATUS, &link_status);
  if (link_status != GL_TRUE) {
    abort();
  }

  m_gl.validateProgram(m_program);
  auto validate_status = static_cast<GLint>(GL_FALSE);
  m_gl.getProgramiv(m_program, GL_VALIDATE_STATUS, &validate_status);
  if (validate_status != GL_TRUE) {
    abort();
  }

  m_gl.deleteShader(fragment_shader);
  m_gl.deleteShader(geometry_shader);
  m_gl.deleteShader(vertex_shader);
}

auto
engine::sandbox::gl::Program::new_(engine::graphics::gl::Context& gl,
				   std::span<uint32_t const> fragment_binary,
				   std::span<uint32_t const> geometry_binary,
				   std::span<uint32_t const> vertex_binary) noexcept -> engine::sandbox::gl::Program
{
  auto self = Program(gl);
  self.initialize(fragment_binary, geometry_binary, vertex_binary);
  return std::move(self);
}

auto
engine::sandbox::gl::Program::new_pointer(engine::graphics::gl::Context& gl,
					  std::span<uint32_t const> fragment_binary,
					  std::span<uint32_t const> geometry_binary,
					  std::span<uint32_t const> vertex_binary) noexcept
  -> engine::sandbox::gl::Program*
{
  auto* self = new Program(gl);
  self->initialize(fragment_binary, geometry_binary, vertex_binary);
  return self;
}

auto
engine::sandbox::gl::Program::use() const& noexcept -> void
{
  m_gl.useProgram(m_program);
}
#endif

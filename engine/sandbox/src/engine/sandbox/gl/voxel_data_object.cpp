#include "engine/sandbox/gl/voxel_data_object.hpp"

#if ENGINE_HAS_GL
engine::sandbox::gl::VoxelDataObject::VoxelDataObject(engine::sandbox::gl::VoxelDataObject&& other) noexcept
  : m_data_texture(std::move(other.m_data_texture))
  , m_gl(other.m_gl)
  , m_matrices(std::exchange(other.m_matrices, nullptr))
  , m_model(other.m_model)
  , m_transform(std::exchange(other.m_transform, Transform::new_()))
  , m_triangulation_texture(other.m_triangulation_texture)
  , m_ubo(std::exchange(other.m_ubo, GL_NONE))
{}

engine::sandbox::gl::VoxelDataObject::~VoxelDataObject() noexcept
{
  drop();
}

auto
engine::sandbox::gl::VoxelDataObject::create_model() & noexcept -> void
{
  m_matrices->model = m_transform.matrix();
}

auto
engine::sandbox::gl::VoxelDataObject::draw() const& noexcept -> void
{
  m_gl.bindBufferBase(GL_UNIFORM_BUFFER, 1, m_ubo);
  m_data_texture->bind(2);
  m_triangulation_texture.bind(3);

  m_model.draw();
}

auto
engine::sandbox::gl::VoxelDataObject::drop() & noexcept -> void
{
  m_data_texture.reset();

  if (m_matrices != nullptr) {
    m_gl.unmapNamedBuffer(m_ubo);
    m_matrices = nullptr;
  }
  m_gl.deleteBuffers(1, &m_ubo);
  m_ubo = GL_NONE;
}

auto
engine::sandbox::gl::VoxelDataObject::initialize(std::span<f32 const, engine::sandbox::VoxelData::SIZE> data) & noexcept
  -> void
{
  m_data_texture = std::unique_ptr<VoxelDataTexture>(VoxelDataTexture::new_pointer(m_gl, data));

  auto matrices = ObjectMatrices{ glm::identity<glm::mat4>() };
  m_gl.createBuffers(1, &m_ubo);
  m_gl.namedBufferStorage(m_ubo, sizeof(ObjectMatrices), &matrices, M_STORAGE_FLAGS);
  m_matrices =
    static_cast<ObjectMatrices*>(m_gl.mapNamedBufferRange(m_ubo, 0, sizeof(ObjectMatrices), M_MAPPING_FLAGS));

  // m_matrices->model = glm::translate(glm::vec3(2.0f, 0.0f, 0.0f));
}

auto
engine::sandbox::gl::VoxelDataObject::late_update(engine::f64 delta_time, engine::f64 time) & noexcept -> void
{
  create_model();
}

auto
engine::sandbox::gl::VoxelDataObject::new_(engine::graphics::gl::Context& gl,
					   std::span<f32 const, engine::sandbox::VoxelData::SIZE> data,
					   engine::sandbox::gl::VoxelDataModel& model,
					   engine::sandbox::gl::TriangulationTexture& triangulation_texture) noexcept
  -> engine::sandbox::gl::VoxelDataObject
{
  auto self = VoxelDataObject(gl, model, triangulation_texture);
  self.initialize(data);
  return std::move(self);
}

auto
engine::sandbox::gl::VoxelDataObject::new_pointer(
  engine::graphics::gl::Context& gl,
  std::span<f32 const, engine::sandbox::VoxelData::SIZE> data,
  engine::sandbox::gl::VoxelDataModel& model,
  engine::sandbox::gl::TriangulationTexture& triangulation_texture) noexcept -> engine::sandbox::gl::VoxelDataObject*
{
  auto* self = new VoxelDataObject(gl, model, triangulation_texture);
  self->initialize(data);
  return self;
}
#endif

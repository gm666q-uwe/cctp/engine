#include "engine/sandbox/gl/voxel_data_texture.hpp"

#if ENGINE_HAS_GL
engine::sandbox::gl::VoxelDataTexture::VoxelDataTexture(engine::sandbox::gl::VoxelDataTexture&& other) noexcept
  : m_gl(other.m_gl)
  , m_texture(std::exchange(other.m_texture, 0))
{}

engine::sandbox::gl::VoxelDataTexture::~VoxelDataTexture() noexcept
{
  drop();
}

auto
engine::sandbox::gl::VoxelDataTexture::bind(GLuint unit) const& noexcept -> void
{
  m_gl.bindTextureUnit(unit, m_texture);
}

auto
engine::sandbox::gl::VoxelDataTexture::drop() & noexcept -> void
{
  if (m_texture != 0) {
    m_gl.deleteTextures(1, &m_texture);
    m_texture = 0;
  }
}

auto
engine::sandbox::gl::VoxelDataTexture::initialize(std::span<f32 const, engine::sandbox::VoxelData::SIZE> data) & noexcept
  -> void
{
  m_gl.createTextures(GL_TEXTURE_3D, 1, &m_texture);

  m_gl.textureParameteri(m_texture, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  m_gl.textureParameteri(m_texture, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  m_gl.textureParameteri(m_texture, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
  m_gl.textureParameteri(m_texture, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  m_gl.textureParameteri(m_texture, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  m_gl.textureStorage3D(m_texture, 1, GL_R32F, VoxelData::WIDTH, VoxelData::HEIGHT, VoxelData::DEPTH);
  m_gl.textureSubImage3D(
    m_texture, 0, 0, 0, 0, VoxelData::WIDTH, VoxelData::HEIGHT, VoxelData::DEPTH, GL_RED, GL_FLOAT, data.data());
}

auto
engine::sandbox::gl::VoxelDataTexture::new_(engine::graphics::gl::Context& gl,
					    std::span<f32 const, engine::sandbox::VoxelData::SIZE> data) noexcept
  -> engine::sandbox::gl::VoxelDataTexture
{
  auto self = VoxelDataTexture(gl);
  self.initialize(data);
  return std::move(self);
}

auto
engine::sandbox::gl::VoxelDataTexture::new_pointer(engine::graphics::gl::Context& gl,
						   std::span<f32 const, engine::sandbox::VoxelData::SIZE> data) noexcept
  -> engine::sandbox::gl::VoxelDataTexture*
{
  auto* self = new VoxelDataTexture(gl);
  self->initialize(data);
  return self;
}
#endif

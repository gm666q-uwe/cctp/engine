#include "engine/sandbox/gl/camera.hpp"

#include <iostream>

#if ENGINE_HAS_GL
engine::sandbox::gl::Camera::Camera(engine::sandbox::gl::Camera&& other) noexcept
  : m_fov(std::exchange(other.m_fov, M_FOV))
  , m_gl(other.m_gl)
  , m_im(other.m_im)
  , m_input(std::exchange(other.m_input, Input{}))
  , m_matrices(std::exchange(other.m_matrices, nullptr))
  , m_ubo(std::exchange(other.m_ubo, GL_NONE))
  //, m_view_changed(std::exchange(other.m_view_changed, false))
  , m_z_far(std::exchange(other.m_z_far, M_Z_FAR))
  , m_z_near(std::exchange(other.m_z_near, M_Z_NEAR))
{}

engine::sandbox::gl::Camera::~Camera() noexcept
{
  drop();
}

auto
engine::sandbox::gl::Camera::bind(GLuint index) const& noexcept -> void
{
  m_gl.bindBufferBase(GL_UNIFORM_BUFFER, index, m_ubo);
}

auto
engine::sandbox::gl::Camera::create_projection(engine::f32 height, engine::f32 width) & noexcept -> void
{
  m_matrices->projection = glm::perspective(glm::radians(m_fov), width / height, m_z_near, m_z_far);
}

auto
engine::sandbox::gl::Camera::create_view() & noexcept -> void
{
  m_matrices->view = glm::toMat4(glm::conjugate(m_transform.rotation())) * glm::translate(-m_transform.position());
}

auto
engine::sandbox::gl::Camera::drop() & noexcept -> void
{
  if (m_matrices != nullptr) {
    m_gl.unmapNamedBuffer(m_ubo);
    m_matrices = nullptr;
  }
  m_gl.deleteBuffers(1, &m_ubo);
  m_ubo = GL_NONE;
}

auto
engine::sandbox::gl::Camera::fixed_update(engine::f64 fixed_delta_time, engine::f64 fixed_time) & noexcept -> void
{}

auto
engine::sandbox::gl::Camera::initialize() & noexcept -> void
{
  m_input = Input{ m_im.axis_index("Look X").unwrap(),
		   m_im.axis_index("Look Y").unwrap(),
		   m_im.axis_index("Move X").unwrap(),
		   m_im.axis_index("Move Y").unwrap(),
		   m_im.axis_index("Move Z").unwrap() };

  auto matrices = CameraMatrices{ glm::identity<glm::mat4>(), glm::identity<glm::mat4>() };
  m_gl.createBuffers(1, &m_ubo);
  m_gl.namedBufferStorage(m_ubo, sizeof(CameraMatrices), &matrices, M_STORAGE_FLAGS);
  m_matrices =
    static_cast<CameraMatrices*>(m_gl.mapNamedBufferRange(m_ubo, 0, sizeof(CameraMatrices), M_MAPPING_FLAGS));
}

auto
engine::sandbox::gl::Camera::late_update(engine::f64 delta_time, engine::f64 time) & noexcept -> void
{
  create_view();
}

auto
engine::sandbox::gl::Camera::new_(engine::graphics::gl::Context& gl, engine::input::Manager& im) noexcept
  -> engine::sandbox::gl::Camera
{
  auto self = Camera(gl, im);
  self.initialize();
  return std::move(self);
}

auto
engine::sandbox::gl::Camera::new_pointer(engine::graphics::gl::Context& gl, engine::input::Manager& im) noexcept
  -> engine::sandbox::gl::Camera*
{
  auto* self = new Camera(gl, im);
  self->initialize();
  return self;
}

auto
engine::sandbox::gl::Camera::on_resize(engine::u16 height, engine::u16 width) & noexcept -> void
{
  create_projection(height, width);
}

auto
engine::sandbox::gl::Camera::update(engine::f64 delta_time, engine::f64 time) & noexcept -> void
{
  auto look_x = m_im.axis(m_input.look_x).unwrap()->get();
  auto look_y = m_im.axis(m_input.look_y).unwrap()->get();
  auto move_x = m_im.axis(m_input.move_x).unwrap()->get();
  auto move_y = m_im.axis(m_input.move_y).unwrap()->get();
  auto move_z = m_im.axis(m_input.move_z).unwrap()->get();

  auto look_speed = M_LOOK_SPEED * static_cast<f32>(delta_time);
  auto move_speed = M_MOVE_SPEED * static_cast<f32>(delta_time);

  m_rotation += glm::vec3(-look_y, -look_x, 0.0f) * look_speed;
  m_transform.set_rotation_euler(m_rotation);

  auto move = glm::vec3(move_x, move_y, -move_z);
  if (glm::length2(move) > 1.0f) {
    move = glm::normalize(move);
  }
  auto direction = glm::rotate(m_transform.rotation(), move);
  m_transform.set_position(m_transform.position() + (direction * move_speed));
}
#endif

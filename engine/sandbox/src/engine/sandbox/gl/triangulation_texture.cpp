#include "engine/sandbox/gl/triangulation_texture.hpp"

#include "engine/sandbox/triangulation_table.hpp"

#if ENGINE_HAS_GL
engine::sandbox::gl::TriangulationTexture::TriangulationTexture(
  engine::sandbox::gl::TriangulationTexture&& other) noexcept
  : m_gl(other.m_gl)
  , m_texture(std::exchange(other.m_texture, 0))
{}

engine::sandbox::gl::TriangulationTexture::~TriangulationTexture() noexcept
{
  drop();
}

auto
engine::sandbox::gl::TriangulationTexture::bind(GLuint unit) const& noexcept -> void
{
  m_gl.bindTextureUnit(unit, m_texture);
}

auto
engine::sandbox::gl::TriangulationTexture::drop() & noexcept -> void
{
  if (m_texture != 0) {
    m_gl.deleteTextures(1, &m_texture);
    m_texture = 0;
  }
}

auto
engine::sandbox::gl::TriangulationTexture::initialize() & noexcept -> void
{
  m_gl.createTextures(GL_TEXTURE_2D, 1, &m_texture);

  m_gl.textureParameteri(m_texture, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  m_gl.textureParameteri(m_texture, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  m_gl.textureParameteri(m_texture, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
  m_gl.textureParameteri(m_texture, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  m_gl.textureParameteri(m_texture, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  m_gl.textureStorage2D(m_texture, 1, GL_R8I, 16, 256);
  m_gl.textureSubImage2D(m_texture, 0, 0, 0, 16, 256, GL_RED_INTEGER, GL_BYTE, TRIANGULATION_TABLE.data());
}

auto
engine::sandbox::gl::TriangulationTexture::new_(engine::graphics::gl::Context& gl) noexcept
  -> engine::sandbox::gl::TriangulationTexture
{
  auto self = TriangulationTexture(gl);
  self.initialize();
  return std::move(self);
}

auto
engine::sandbox::gl::TriangulationTexture::new_pointer(engine::graphics::gl::Context& gl) noexcept
  -> engine::sandbox::gl::TriangulationTexture*
{
  auto* self = new TriangulationTexture(gl);
  self->initialize();
  return self;
}
#endif

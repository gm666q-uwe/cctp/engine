#include "engine/sandbox/gl/voxel_data_model.hpp"

#include "engine/sandbox/vertex.hpp"

#if ENGINE_HAS_GL
engine::sandbox::gl::VoxelDataModel::VoxelDataModel(engine::sandbox::gl::VoxelDataModel&& other) noexcept
  : m_gl(other.m_gl)
  , m_vao(std::exchange(other.m_vao, GL_NONE))
  , m_vbo(std::exchange(other.m_vbo, GL_NONE))
{}

engine::sandbox::gl::VoxelDataModel::~VoxelDataModel() noexcept
{
  drop();
}

auto
engine::sandbox::gl::VoxelDataModel::draw() const& noexcept -> void
{
  m_gl.bindVertexArray(m_vao);
  m_gl.drawArrays(GL_POINTS, 0, M_SIZE);
}

auto
engine::sandbox::gl::VoxelDataModel::drop() & noexcept -> void
{
  m_gl.deleteVertexArrays(1, &m_vao);
  m_vao = GL_NONE;
  m_gl.deleteBuffers(1, &m_vbo);
  m_vbo = GL_NONE;
}

auto
engine::sandbox::gl::VoxelDataModel::initialize() & noexcept -> void
{
  auto vertices = std::vector<Vertex>(M_SIZE);
  {
    auto i = static_cast<usize>(0);
    for (auto z = -1.0f; z < 1.0f; z += M_STEP_Z) {
      for (auto y = -1.0f; y < 1.0f; y += M_STEP_Y) {
	for (auto x = -1.0f; x < 1.0f; x += M_STEP_X) {
	  vertices[i++] = Vertex{ glm::vec3{ x, y, z } };
	}
      }
    }
  }

  m_gl.createBuffers(1, &m_vbo);
  m_gl.namedBufferStorage(m_vbo, static_cast<GLsizeiptr>(vertices.size() * sizeof(Vertex)), vertices.data(), GL_NONE);

  m_gl.createVertexArrays(1, &m_vao);
  m_gl.vertexArrayVertexBuffer(m_vao, 0, m_vbo, 0, sizeof(Vertex));

  m_gl.enableVertexArrayAttrib(m_vao, 0);

  m_gl.vertexArrayAttribFormat(m_vao, 0, 3, GL_FLOAT, GL_FALSE, offsetof(Vertex, position));

  m_gl.vertexArrayAttribBinding(m_vao, 0, 0);
}

auto
engine::sandbox::gl::VoxelDataModel::new_(engine::graphics::gl::Context& gl) noexcept
  -> engine::sandbox::gl::VoxelDataModel
{
  auto self = VoxelDataModel(gl);
  self.initialize();
  return std::move(self);
}

auto
engine::sandbox::gl::VoxelDataModel::new_pointer(engine::graphics::gl::Context& gl) noexcept
  -> engine::sandbox::gl::VoxelDataModel*
{
  auto* self = new VoxelDataModel(gl);
  self->initialize();
  return self;
}
#endif

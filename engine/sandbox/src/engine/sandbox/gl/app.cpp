#include "engine/sandbox/gl/app.h"
#include "engine/sandbox/gl/app.hpp"

#include <cstdlib>
#include <iostream>
#include <random>

#include <engine/display/common/window_options.hpp>

#if ENGINE_IS_WINDOWS
#include <engine/display/win32/display.hpp>
#endif

#if ENGINE_HAS_GL
#include <engine/graphics/gl/graphics_context.hpp>
#endif

#if ENGINE_HAS_WAYLAND
#include <engine/display/wayland/display.hpp>
#endif

#if ENGINE_HAS_XLIB
#include <engine/display/xlib/display.hpp>
#endif

#include "engine/sandbox/generator.hpp"

#if ENGINE_HAS_GL
#if ENGINE_IS_WINDOWS
int
engine_sandbox_gl_app_run(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nShowCmd)
{
  auto options = engine::display::win32::DisplayOptions::new_(hInstance, nShowCmd);
  auto app = engine::sandbox::gl::App::new_(engine::display::win32::Display::new_pointer(options).unwrap());
  return app.run(hInstance, hPrevInstance, lpCmdLine, nShowCmd);
}
#else
int
engine_sandbox_gl_app_run(int argc, const char** argv)
{
  auto xdg_session_type = engine::CStr(std::getenv("XDG_SESSION_TYPE"));
#if ENGINE_HAS_WAYLAND
  auto use_wayland = xdg_session_type.compare("wayland") == 0;
#else
  auto use_wayland = false;
#endif

  auto display = static_cast<engine::display::Display*>(nullptr);
  if (use_wayland) {
#if ENGINE_HAS_WAYLAND
    auto display_options = engine::display::wayland::DisplayOptions::new_();
    display = engine::display::wayland::Display::new_pointer(display_options).unwrap();
#endif
  } else {
#if ENGINE_HAS_XLIB
    auto display_options = engine::display::xlib::DisplayOptions::new_();
    display = engine::display::xlib::Display::new_pointer(display_options).unwrap();
#endif
  }

  if (display == nullptr) {
    return -1;
  }

  auto app = engine::sandbox::gl::App::new_(display);
  return app.run(argc, argv);
}
#endif

// Can't be constexpr because P1004R2 is not supported by GCC and Clang yet.
static /*constexpr*/ std::vector<uint32_t> const MAIN_FRAG =
#include "engine/sandbox/gl/Main.frag.inc"
  ;

static /*constexpr*/ std::vector<uint32_t> const MAIN_GEOM =
#include "engine/sandbox/gl/Main.geom.inc"
  ;

static /*constexpr*/ std::vector<uint32_t> const MAIN_VERT =
#include "engine/sandbox/gl/Main.vert.inc"
  ;

engine::sandbox::gl::App::App(engine::display::Display* display) noexcept
  : engine::App()
  , m_display(display)
{}

engine::sandbox::gl::App::~App() noexcept
{
  auto& gc = m_window->graphics_context();
  gc.make_current();
  gc.set_resize_callback(nullptr);

  m_voxel_data_objects.clear();

  m_camera.reset();
  m_program.reset();
  m_triangulation_texture.reset();
  m_voxel_data_model.reset();

  m_window.reset();
  m_display.reset();
}

auto
engine::sandbox::gl::App::fixed_update(engine::f64 fixed_delta_time, engine::f64 fixed_time) & noexcept -> void
{
  m_camera->fixed_update(fixed_delta_time, fixed_time);
}

auto
engine::sandbox::gl::App::init(std::span<CStr> args) & noexcept -> void
{
  using namespace std::literals;
  auto window_options = display::common::WindowOptions::new_();
  window_options.graphics_api(display::WindowOptions::GraphicsAPI::OpenGL);
  m_window = std::unique_ptr<display::Window>(m_display->create_window_pointer(window_options).unwrap());

  auto& im = m_window->input_manager();
  im.add_axis(engine::input::Axis::new_({ engine::input::Axis::Binding(engine::input::Key::KeyLeft, -10.0f),
					  engine::input::Axis::Binding(engine::input::Key::KeyRight, 10.0f),
					  engine::input::Axis::Binding(engine::input::RelativeAxis::LeftX, 1.0f) }),
	      "Look X"sv);
  im.add_axis(engine::input::Axis::new_({ engine::input::Axis::Binding(engine::input::Key::KeyDown, 10.0f),
					  engine::input::Axis::Binding(engine::input::Key::KeyUp, -10.0f),
					  engine::input::Axis::Binding(engine::input::RelativeAxis::LeftY, 1.0f) }),
	      "Look Y"sv);
  im.add_axis(engine::input::Axis::new_({ engine::input::Axis::Binding(engine::input::Key::KeyA, -1.0f),
					  engine::input::Axis::Binding(engine::input::Key::KeyD, 1.0f) }),
	      "Move X"sv);
  im.add_axis(engine::input::Axis::new_({ engine::input::Axis::Binding(engine::input::Key::KeyLeftControl, -1.0f),
					  engine::input::Axis::Binding(engine::input::Key::KeyLeftShift, 1.0f) }),
	      "Move Y"sv);
  im.add_axis(engine::input::Axis::new_({ engine::input::Axis::Binding(engine::input::Key::KeyS, -1.0f),
					  engine::input::Axis::Binding(engine::input::Key::KeyW, 1.0f) }),
	      "Move Z"sv);
  im.add_action(engine::input::Action::new_({ engine::input::Action::Binding(engine::input::Key::KeyEscape, 0.5f) }),
		"Quit"sv);

  auto& gc = dynamic_cast<graphics::gl::GraphicsContext&>(m_window->graphics_context());
  gc.make_current();
  gc.interface_context().swap_interval(-1); //.unwrap();

  m_camera = std::unique_ptr<Camera>(Camera::new_pointer(gc.gl(), im));
  m_program = std::unique_ptr<Program>(Program::new_pointer(gc.gl(), MAIN_FRAG, MAIN_GEOM, MAIN_VERT));
  m_triangulation_texture = std::unique_ptr<TriangulationTexture>(TriangulationTexture::new_pointer(gc.gl()));
  m_voxel_data_model = std::unique_ptr<VoxelDataModel>(VoxelDataModel::new_pointer(gc.gl()));

  auto generator = Generator::with_seed(std::random_device()());
  for (auto y = -2; y < 3; y++) {
    for (auto x = -2; x < 3; x++) {
      m_voxel_data_objects.emplace_back(
	VoxelDataObject::new_(gc.gl(), generator.get_chunk(x, y).data, *m_voxel_data_model, *m_triangulation_texture));
      m_voxel_data_objects.back().transform().set_position(glm::vec3(x * 2, 0, y * 2));
      m_voxel_data_objects.back().transform().set_rotation_euler(glm::vec3(0, 90, 0));
    }
  }

  {
    auto* camera = m_camera.get();
    gc.set_resize_callback([=](auto height, auto width) { camera->on_resize(height, width); });
  }
}

auto
engine::sandbox::gl::App::input() & noexcept -> void
{
  m_window->input_manager().end_update();
  m_running = m_display->run().unwrap();
  if (m_running) {
    m_running = m_window->run().unwrap();
    if (m_running) {
      m_window->input_manager().begin_update();
    }
  }
}

auto
engine::sandbox::gl::App::late_update(engine::f64 delta_time, engine::f64 time) & noexcept -> void
{
  m_camera->late_update(delta_time, time);
  for (auto& voxel_data_object : m_voxel_data_objects) {
    voxel_data_object.late_update(delta_time, time);
  }
}

auto
engine::sandbox::gl::App::new_(engine::display::Display* display) noexcept -> engine::sandbox::gl::App
{
  return App(display);
}

auto
engine::sandbox::gl::App::new_pointer(engine::display::Display* display) noexcept -> engine::sandbox::gl::App*
{
  return new App(display);
}

auto
engine::sandbox::gl::App::render(double time) & noexcept -> void
{
  auto& gc = m_window->graphics_context();
  gc.begin_frame();
  m_program->use();
  m_camera->bind(0);

  for (auto& voxel_data_object : m_voxel_data_objects) {
    voxel_data_object.draw();
  }

  gc.end_frame();
}

auto
engine::sandbox::gl::App::result() const& noexcept -> int
{
  return m_result;
}

auto
engine::sandbox::gl::App::running() const& noexcept -> bool
{
  return m_running;
}

auto
engine::sandbox::gl::App::update(engine::f64 delta_time, engine::f64 time) & noexcept -> void
{
  m_camera->update(delta_time, time);
}
#endif

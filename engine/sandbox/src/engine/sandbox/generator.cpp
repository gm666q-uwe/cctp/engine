#include "engine/sandbox/generator.hpp"

#include <memory>
#include <unordered_map>

engine::sandbox::Generator::Generator(engine::u32 seed) noexcept
  : m_perlin_noise(seed)
{}

engine::sandbox::Generator::Generator(engine::sandbox::Generator&& other) noexcept {}

engine::sandbox::Generator::~Generator() noexcept
{
  drop();
}

auto
engine::sandbox::Generator::drop() & noexcept -> void
{}

auto
engine::sandbox::Generator::generate_chunk(engine::i16 x, engine::i16 y) & noexcept -> const engine::sandbox::VoxelData&
{
  constexpr auto const data_size = glm::vec3(static_cast<float>(VoxelData::WIDTH) / M_FREQUENCY,
					     static_cast<float>(VoxelData::HEIGHT),
					     static_cast<float>(VoxelData::DEPTH) / M_FREQUENCY);
  auto position = Position(x, y);
  auto tmp = std::make_unique<VoxelData>();
  auto it = m_chunk_map.emplace(position.index, *tmp);
  auto& voxel_data = it.first->second;
  for (auto k = 0; k < VoxelData::DEPTH; k++) {
    for (auto i = 0; i < VoxelData::WIDTH; i++) {
      auto nx = (static_cast<f32>(i) / data_size.x) + static_cast<f32>(x);
      auto ny = (static_cast<f32>(k) / data_size.z) - static_cast<f32>(y);
      auto v = m_perlin_noise.accumulatedOctaveNoise2D_0_1(nx, ny, M_OCTAVES);
      auto height = (v * (data_size.y / 2.0f)) + (data_size.y / 4.0f);
      for (auto j = 0; j < VoxelData::HEIGHT; j++) {
	voxel_data.at(i, j, k) = static_cast<f32>(j) < height ? 0.0f : 1.0f;
      }
    }
  }
  return voxel_data;
}

auto
engine::sandbox::Generator::get_chunk(engine::i16 x, engine::i16 y) & noexcept -> const engine::sandbox::VoxelData&
{
  auto position = Position(x, y);
  auto it = m_chunk_map.find(position.index);
  if (it == m_chunk_map.end()) {
    return generate_chunk(x, y);
  }
  return it->second;
}

auto
engine::sandbox::Generator::initialize() & noexcept -> void
{}

auto
engine::sandbox::Generator::new_() noexcept -> engine::sandbox::Generator
{
  auto self = Generator(std::default_random_engine::default_seed);
  self.initialize();
  return std::move(self);
}

auto
engine::sandbox::Generator::new_pointer() noexcept -> engine::sandbox::Generator*
{
  auto* self = new Generator(std::default_random_engine::default_seed);
  self->initialize();
  return self;
}

auto
engine::sandbox::Generator::with_seed(engine::u32 seed) noexcept -> engine::sandbox::Generator
{
  auto self = Generator(seed);
  self.initialize();
  return std::move(self);
}

auto
engine::sandbox::Generator::with_seed_pointer(engine::u32 seed) noexcept -> engine::sandbox::Generator*
{
  auto* self = new Generator(seed);
  self->initialize();
  return self;
}

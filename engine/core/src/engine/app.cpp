#include "engine/app.hpp"

#include <chrono>
#include <vector>

#include "engine/platform_data/windows_data.hpp"

auto
engine::App::run(std::span<CStr> args) & noexcept -> int
{
  init(args);

  auto accumulator = 0.0;
  auto current_time = std::chrono::high_resolution_clock::now();
  auto fixed_time = 0.0;
  auto time = 0.0;

  while (running()) {
    auto new_time = std::chrono::high_resolution_clock::now();
    auto delta_time = std::chrono::duration<double>(new_time - current_time).count();
    accumulator += delta_time;
    current_time = new_time;

    // Input
    input();

    // Update
    while (accumulator >= M_TARGET_DELTA_TIME) {
      fixed_update(M_TARGET_DELTA_TIME, fixed_time);
      accumulator -= M_TARGET_DELTA_TIME;
      fixed_time += M_TARGET_DELTA_TIME;
    }
    update(delta_time, time);
    late_update(delta_time, time);

    // Render
    render(time);

    time += delta_time;
  }

  return result();
}

auto
engine::App::run(int argc, char const** argv) & noexcept -> int
{
  auto args = std::vector<CStr>();
  args.reserve(argc);
  for (auto i = 0; i < argc; i++) {
    args.emplace_back(argv[i]);
  }
  return run(args);
}

#if ENGINE_IS_WINDOWS
auto
engine::App::run(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nShowCmd) & noexcept -> int
{
  m_platform_data = std::shared_ptr<platform_data::WindowsData>(
    platform_data::WindowsData::new_pointer(hInstance, hPrevInstance, nShowCmd));

  auto args = std::vector<CStr>();
  return run(args);
}
#endif

#pragma once

#include <memory>
#include <span>

#include "engine/platform.h"
#include "engine/platform_data/platform_data.hpp"

namespace engine {
class App
{
public:
  constexpr App(App const&) noexcept = delete;

  App(App&&) noexcept = default;

  virtual ~App() noexcept = default;

  constexpr auto operator=(App const&) & noexcept -> App& = delete;

  auto operator=(App&&) & noexcept -> App& = default;

  virtual auto run(int argc, char const** argv) & noexcept -> int;

#if ENGINE_IS_WINDOWS
  virtual auto run(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nShowCmd) & noexcept -> int;
#endif

protected:
  static constexpr f64 const M_TARGET_DELTA_TIME = 1.0 / 60.0;

  std::shared_ptr<platform_data::PlatformData> m_platform_data = std::shared_ptr<platform_data::PlatformData>();

  constexpr App() noexcept = default;

  virtual auto fixed_update(f64 fixed_delta_time, f64 fixed_time) & noexcept -> void = 0;

  virtual auto init(std::span<CStr> args) & noexcept -> void = 0;

  virtual auto input() & noexcept -> void = 0;

  virtual auto late_update(f64 delta_time, f64 time) & noexcept -> void = 0;

  virtual auto render(f64 time) & noexcept -> void = 0;

  [[nodiscard]] virtual auto result() const& noexcept -> int = 0;

  auto run(std::span<CStr> args) & noexcept -> int;

  [[nodiscard]] virtual auto running() const& noexcept -> bool = 0;

  virtual auto update(f64 delta_time, f64 time) & noexcept -> void = 0;

private:
};
} // namespace engine

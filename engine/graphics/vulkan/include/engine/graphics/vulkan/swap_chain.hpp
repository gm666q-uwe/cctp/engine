#pragma once

#include <result/result.hpp>

#include <engine/error.hpp>

#include "engine/graphics/vulkan/vulkan.h"

namespace engine::graphics::vulkan {
class SwapChain
{
  friend class Device;

public:
  struct Image
  {
    VkCommandBuffer command_buffer = VK_NULL_HANDLE;
    VkFramebuffer framebuffer = VK_NULL_HANDLE;
    VkImage image = VK_NULL_HANDLE;
    VkImageView image_view = VK_NULL_HANDLE;
    bool is_command_buffer_valid = false;
    VkCommandBuffer transfer_command_buffer = VK_NULL_HANDLE;

    auto drop_1(VkDevice device) & noexcept -> void;

    auto drop_2(VkDevice device, VkCommandPool present_command_pool) & noexcept -> void;

    auto drop_3(VkDevice device, VkCommandPool graphics_command_pool) & noexcept -> void;
  };

  struct Sync
  {
    VkSemaphore draw_complete_semaphore = VK_NULL_HANDLE;
    VkFence fence = VK_NULL_HANDLE;
    VkSemaphore image_acquired_semaphore = VK_NULL_HANDLE;
    VkSemaphore image_transfer_semaphore = VK_NULL_HANDLE;

    auto drop(VkDevice device) & noexcept -> void;
  };

  constexpr SwapChain() noexcept = delete;

  constexpr SwapChain(SwapChain const&) noexcept = delete;

  SwapChain(SwapChain&& other) noexcept;

  virtual ~SwapChain() noexcept;

  constexpr auto operator=(SwapChain const&) & noexcept -> SwapChain& = delete;

  auto operator=(SwapChain&& other) & noexcept -> SwapChain&;

  auto acquire_image() & noexcept -> void;

  [[nodiscard]] auto current_image() & noexcept -> Image&;

  [[nodiscard]] constexpr auto current_sync() const& noexcept -> Sync const& { return m_syncs[m_current_frame]; }

  //[[nodiscard]] constexpr auto depth_format() const& noexcept -> VkFormat { return m_depth.format; }

  [[nodiscard]] constexpr auto extent() const& noexcept -> VkExtent2D const& { return m_extent; }

  //[[nodiscard]] constexpr auto format() const& noexcept -> VkFormat { return m_format; }

  auto free_command_buffers(VkCommandPool graphics_command_pool) & noexcept -> void;

  auto on_resize(u16 height, u16 width) & noexcept -> void;

  auto present() & noexcept -> void;

  [[nodiscard]] constexpr auto render_pass() const& noexcept -> VkRenderPass { return m_render_pass; }

protected:
private:
  struct Depth
  {
    VkFormat format = VK_FORMAT_UNDEFINED;
    VkImage image = VK_NULL_HANDLE;
    VkImageView image_view = VK_NULL_HANDLE;
    VkDeviceMemory memory = VK_NULL_HANDLE;

    auto drop(VkDevice device) & noexcept -> void;
  };

  static constexpr uint32_t const M_DESIRED_IMAGE_COUNT = 3;
  static constexpr usize const M_MAX_FRAMES_IN_FLIGHT = 1;

  usize m_current_frame = 0;
  uint32_t m_current_image = 0;
  Depth m_depth = Depth{};
  VkDevice m_device = VK_NULL_HANDLE;
  VkExtent2D m_extent = VkExtent2D{};
  VkFormat m_format = VK_FORMAT_UNDEFINED;
  Vec<Image> m_images = Vec<Image>();
  VkPhysicalDevice m_physical_device = VK_NULL_HANDLE;
  VkCommandPool m_present_command_pool = VK_NULL_HANDLE;
  VkQueue m_present_queue = VK_NULL_HANDLE;
  std::array<uint32_t, 2> m_queue_family_indices = std::array<uint32_t, 2>{};
  VkRenderPass m_render_pass = VK_NULL_HANDLE;
  VkSurfaceKHR m_surface_khr = VK_NULL_HANDLE;
  std::array<Sync, M_MAX_FRAMES_IN_FLIGHT> m_syncs = std::array<Sync, M_MAX_FRAMES_IN_FLIGHT>{};
  VkSwapchainKHR m_swapchain_khr = VK_NULL_HANDLE;

  explicit SwapChain(VkDevice device,
		     uint32_t graphics_queue_family_index,
		     VkPhysicalDevice physical_device,
		     VkCommandPool present_command_pool,
		     VkQueue present_queue,
		     uint32_t present_queue_family_index,
		     VkSurfaceKHR surface_khr) noexcept;

  auto drop() & noexcept -> void;

  auto initialize(VkExtent2D extent) & noexcept -> option::Option<Error>;

  auto initialize_depth() & noexcept -> option::Option<Error>;

  auto initialize_framebuffers() & noexcept -> option::Option<Error>;

  auto initialize_render_pass() & noexcept -> option::Option<Error>;

  auto initialize_swapchain(VkExtent2D extent) & noexcept -> option::Option<Error>;

  auto initialize_transfer_command_buffer(Image& image) & noexcept -> option::Option<Error>;

  static auto new_(VkDevice device,
		   VkExtent2D extent,
		   uint32_t graphics_queue_family_index,
		   VkPhysicalDevice physical_device,
		   VkCommandPool present_command_pool,
		   VkQueue present_queue,
		   uint32_t present_queue_family_index,
		   VkSurfaceKHR surface_khr) noexcept -> result::Result<SwapChain, Error>;

  static auto new_pointer(VkDevice device,
			  VkExtent2D extent,
			  uint32_t graphics_queue_family_index,
			  VkPhysicalDevice physical_device,
			  VkCommandPool present_command_pool,
			  VkQueue present_queue,
			  uint32_t present_queue_family_index,
			  VkSurfaceKHR surface_khr) noexcept -> result::Result<SwapChain*, Error>;
};
} // namespace engine::graphics::vulkan

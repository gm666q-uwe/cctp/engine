#pragma once

#include "engine/graphics/vulkan/swap_chain.hpp"

namespace engine::graphics::vulkan {
class Device final
{
  friend class Instance;

public:
  // constexpr Device() noexcept = delete;

  constexpr Device(Device const&) noexcept = delete;

  Device(Device&& other) noexcept;

  virtual ~Device() noexcept;

  constexpr auto operator=(Device const&) & noexcept -> Device& = delete;

  auto operator=(Device&& other) & noexcept -> Device&;

  [[nodiscard]] auto begin_command_buffer(VkCommandBuffer& command_buffer) const& noexcept
    -> result::Result<VkCommandBuffer, Error>;

  [[nodiscard]] auto begin_command_buffer_one() const& noexcept -> result::Result<VkCommandBuffer, Error>;

  [[nodiscard]] auto create_swap_chain(VkExtent2D extent, VkSurfaceKHR surface_khr) const& noexcept
    -> result::Result<SwapChain, Error>;

  [[nodiscard]] auto create_swap_chain_pointer(VkExtent2D extent, VkSurfaceKHR surface_khr) const& noexcept
    -> result::Result<SwapChain*, Error>;

  [[nodiscard]] constexpr auto device() const& noexcept -> VkDevice { return m_device; }

  [[nodiscard]] auto end_command_buffer(VkCommandBuffer command_buffer,
					bool submit_only,
					SwapChain::Sync const& sync) const& noexcept
    -> result::Result<std::nullptr_t, Error>;

  [[nodiscard]] auto end_command_buffer_one(VkCommandBuffer command_buffer) const& noexcept
    -> result::Result<std::nullptr_t, Error>;

  [[nodiscard]] constexpr auto graphics_command_pool() const& noexcept -> VkCommandPool
  {
    return m_graphics_command_pool;
  }

  [[nodiscard]] constexpr auto graphics_is_present() const& noexcept -> bool
  {
    return m_graphics_queue_family_index == m_present_queue_family_index;
  }

  [[nodiscard]] constexpr auto graphics_queue_family_index() const& noexcept -> uint32_t
  {
    return m_graphics_queue_family_index;
  }

  [[nodiscard]] constexpr auto memory_properties() const& noexcept -> VkPhysicalDeviceMemoryProperties const&
  {
    return m_physical_device_memory_properties;
  }

  [[nodiscard]] constexpr auto present_queue_family_index() const& noexcept -> uint32_t
  {
    return m_present_queue_family_index;
  }

protected:
private:
  VkDevice m_device = VK_NULL_HANDLE;
  VkCommandPool m_graphics_command_pool = VK_NULL_HANDLE;
  VkQueue m_graphics_queue = VK_NULL_HANDLE;
  uint32_t m_graphics_queue_family_index = 0;
  VkPhysicalDevice m_physical_device = VK_NULL_HANDLE;
  VkPhysicalDeviceMemoryProperties m_physical_device_memory_properties = VkPhysicalDeviceMemoryProperties{};
  VkCommandPool m_present_command_pool = VK_NULL_HANDLE;
  VkQueue m_present_queue = VK_NULL_HANDLE;
  uint32_t m_present_queue_family_index = 0;

  constexpr Device() noexcept = default;

  auto drop() & noexcept -> void;

  auto initialize(VkInstance instance, VkSurfaceKHR surface_khr) & noexcept -> option::Option<Error>;

  auto is_physical_device_usable(VkPhysicalDevice physical_device, VkSurfaceKHR surface_khr) & noexcept -> bool;

  static auto new_(VkInstance instance, VkSurfaceKHR surface_khr) noexcept -> result::Result<Device, Error>;

  static auto new_pointer(VkInstance instance, VkSurfaceKHR surface_khr) noexcept -> result::Result<Device*, Error>;
};
} // namespace engine::graphics::vulkan

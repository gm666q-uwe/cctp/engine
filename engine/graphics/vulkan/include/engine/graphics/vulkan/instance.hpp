#pragma once

#include "engine/graphics/vulkan/device.hpp"
#include "engine/graphics/vulkan/instance_data_ext_debug_utils.hpp"

namespace engine::graphics::vulkan {
class Instance final
{
  friend class GraphicsContext;

public:
  // constexpr Instance() noexcept = delete;

  constexpr Instance(Instance const&) noexcept = delete;

  Instance(Instance&& other) noexcept;

  virtual ~Instance() noexcept;

  constexpr auto operator=(Instance const&) & noexcept -> Instance& = delete;

  auto operator=(Instance&& other) & noexcept -> Instance&;

  [[nodiscard]] auto create_device(VkSurfaceKHR surface_khr) const& noexcept -> result::Result<Device, Error>;

  [[nodiscard]] auto create_device_pointer(VkSurfaceKHR surface_khr) const& noexcept -> result::Result<Device*, Error>;

  [[nodiscard]] constexpr auto instance() const& noexcept -> VkInstance { return m_instance; }

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool { return is_ext_debug_utils_loaded(); }

  [[nodiscard]] constexpr auto is_ext_debug_utils_loaded() const& noexcept -> bool
  {
    return config::is_debug ? m_data_ext_debug_utils.is_loaded() : true;
  }

  auto load() & noexcept -> void;

  auto load_ext_debug_utils() & noexcept -> void;

  // VK_EXT_debug_utils
  auto vkSetDebugUtilsObjectNameEXT(VkDevice device, VkDebugUtilsObjectNameInfoEXT const* pNameInfo) & noexcept
    -> VkResult;

  auto vkSetDebugUtilsObjectTagEXT(VkDevice device, VkDebugUtilsObjectTagInfoEXT const* pTagInfo) & noexcept
    -> VkResult;

  auto vkQueueBeginDebugUtilsLabelEXT(VkQueue queue, VkDebugUtilsLabelEXT const* pLabelInfo) & noexcept -> void;

  auto vkQueueEndDebugUtilsLabelEXT(VkQueue queue) & noexcept -> void;

  auto vkQueueInsertDebugUtilsLabelEXT(VkQueue queue, VkDebugUtilsLabelEXT const* pLabelInfo) & noexcept -> void;

  auto vkCmdBeginDebugUtilsLabelEXT(VkCommandBuffer commandBuffer, VkDebugUtilsLabelEXT const* pLabelInfo) & noexcept
    -> void;

  auto vkCmdEndDebugUtilsLabelEXT(VkCommandBuffer commandBuffer) & noexcept -> void;

  auto vkCmdInsertDebugUtilsLabelEXT(VkCommandBuffer commandBuffer, VkDebugUtilsLabelEXT const* pLabelInfo) & noexcept
    -> void;

  auto vkCreateDebugUtilsMessengerEXT(VkInstance instance,
				      VkDebugUtilsMessengerCreateInfoEXT const* pCreateInfo,
				      VkAllocationCallbacks const* pAllocator,
				      VkDebugUtilsMessengerEXT* pMessenger) & noexcept -> VkResult;

  auto vkDestroyDebugUtilsMessengerEXT(VkInstance instance,
				       VkDebugUtilsMessengerEXT messenger,
				       VkAllocationCallbacks const* pAllocator) & noexcept -> void;

  auto vkSubmitDebugUtilsMessageEXT(VkInstance instance,
				    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
				    VkDebugUtilsMessageTypeFlagsEXT messageTypes,
				    VkDebugUtilsMessengerCallbackDataEXT const* pCallbackData) & noexcept -> void;

protected:
  virtual auto vk_debug_utils_messenger_callback_ext(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageTypes,
    VkDebugUtilsMessengerCallbackDataEXT const* pCallbackData) & noexcept -> VkBool32;

private:
  // VK_EXT_debug_utils
  InstanceDataEXTDebugUtils m_data_ext_debug_utils = {};

  VkDebugUtilsMessengerEXT m_debug_utils_messenger_ext = VK_NULL_HANDLE;
  VkInstance m_instance = VK_NULL_HANDLE;

  constexpr Instance() noexcept = default;

  auto drop() & noexcept -> void;

  auto initialize(CStr application_name, uint32_t application_version, char const* surface_extension_name) & noexcept
    -> option::Option<Error>;

  static auto new_(CStr application_name, uint32_t application_version, char const* surface_extension_name) noexcept
    -> result::Result<Instance, Error>;

  static auto new_pointer(CStr application_name,
			  uint32_t application_version,
			  char const* surface_extension_name) noexcept -> result::Result<Instance*, Error>;

  static auto vk_debug_utils_messenger_callback_ext(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
						    VkDebugUtilsMessageTypeFlagsEXT messageTypes,
						    VkDebugUtilsMessengerCallbackDataEXT const* pCallbackData,
						    void* pUserData) noexcept -> VkBool32;
};
} // namespace engine::graphics::vulkan

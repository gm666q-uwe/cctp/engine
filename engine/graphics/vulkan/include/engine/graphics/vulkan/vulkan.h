#pragma once

#include <engine/platform.h>

#if ENGINE_HAS_WAYLAND
#define VK_USE_PLATFORM_WAYLAND_KHR
#endif
#if ENGINE_IS_WINDOWS
#define VK_USE_PLATFORM_WIN32_KHR
#endif
#if ENGINE_HAS_XCB
#define VK_USE_PLATFORM_XCB_KHR
#endif
#if ENGINE_HAS_XLIB
#define VK_USE_PLATFORM_XLIB_KHR
#endif
#include <vulkan/vulkan.h>

#ifdef None
constexpr int const XLIB_NONE = None;
#undef None
#endif

#pragma once

#include <engine/graphics/graphics_context.hpp>

#include "engine/graphics/vulkan/instance.hpp"

namespace engine::graphics::vulkan {
class GraphicsContext final : public graphics::GraphicsContext
{
public:
  // ENGINE_BITFLAGS(Flags, u8, 1, ((HAS_RESIZE, 0x01)))

  // constexpr GraphicsContext() noexcept = delete;

  constexpr GraphicsContext(GraphicsContext const&) noexcept = delete;

  GraphicsContext(GraphicsContext&& other) noexcept;

  ~GraphicsContext() noexcept override;

  constexpr auto operator=(GraphicsContext const&) & noexcept -> GraphicsContext& = delete;

  auto operator=(GraphicsContext&& other) & noexcept -> GraphicsContext&;

  auto begin_frame() & noexcept -> void override;

  auto begin_render_pass(bool submit_only) & noexcept -> void;

  [[nodiscard]] auto create_camera() const& noexcept -> result::Result<Camera, Error> override;

  [[nodiscard]] auto create_camera_pointer() const& noexcept -> result::Result<Camera*, Error> override;

  [[nodiscard]] auto create_uniform_buffer_pointer(isize size) const& noexcept
    -> result::Result<graphics::UniformBuffer<void>*, Error> override;

  [[nodiscard]] auto current_command_buffer() const& noexcept -> VkCommandBuffer&;

  [[nodiscard]] auto device() const& noexcept -> Device&;

  auto end_frame() & noexcept -> void override;

  auto end_render_pass(bool submit_only) & noexcept -> void;

  [[nodiscard]] auto extent() const& noexcept -> VkExtent2D const&;

  auto free_command_buffers() const & noexcept -> void;

  [[nodiscard]] auto is_current_command_buffer_valid() const& noexcept -> bool;

  auto make_current() const& noexcept -> void override;

  static auto new_(CStr application_name,
		   uint32_t application_version,
		   std::function<VkResult(VkInstance, VkSurfaceKHR*)> create_surface,
		   u16 height,
		   char const* surface_extension_name,
		   u16 width) noexcept -> result::Result<GraphicsContext, Error>;

  static auto new_pointer(CStr application_name,
			  uint32_t application_version,
			  std::function<VkResult(VkInstance, VkSurfaceKHR*)> create_surface,
			  u16 height,
			  char const* surface_extension_name,
			  u16 width) noexcept -> result::Result<GraphicsContext*, Error>;

  auto on_resize(u16 height, u16 width) & noexcept -> void override;

  [[nodiscard]] auto render_pass() const& noexcept -> VkRenderPass;

  auto set_camera(Camera* camera) & noexcept -> void override;

  auto set_clear_color(glm::vec4 const& color) & noexcept -> void override;

  auto set_resize_callback(std::function<void(u16, u16)> resize_callback) & noexcept -> void override;

protected:
private:
  Camera* m_camera = nullptr;
  glm::vec4 m_clear_color = glm::vec4{ 0.0, 0.0f, 0.0f, 1.0f };
  std::shared_ptr<VkCommandBuffer> m_current_command_buffer = std::shared_ptr<VkCommandBuffer>();
  std::unique_ptr<Device> m_device = std::unique_ptr<Device>();
  // Flags m_flags = Flags::HAS_RESIZE();
  std::unique_ptr<Instance> m_instance = std::unique_ptr<Instance>();
  std::function<void(u16, u16)> m_resize_callback = std::function<void(u16, u16)>();
  VkSurfaceKHR m_surface_khr = VK_NULL_HANDLE;
  std::unique_ptr<SwapChain> m_swap_chain = std::unique_ptr<SwapChain>();

  GraphicsContext() noexcept;

  auto drop() & noexcept -> void;

  auto initialize(CStr application_name,
		  uint32_t application_version,
		  std::function<VkResult(VkInstance, VkSurfaceKHR*)> create_surface,
		  u16 height,
		  char const* surface_extension_name,
		  u16 width) & noexcept -> option::Option<Error>;
};
} // namespace engine::graphics::vulkan

// ENGINE_BITFLAGS_HASH(engine::graphics::gl::GraphicsContext::Flags, engine::u8)

#pragma once

#include "engine/graphics/vulkan/i_instance_data.hpp"
#include "engine/graphics/vulkan/vulkan.h"

namespace engine::graphics::vulkan {
struct InstanceDataEXTDebugUtils final : public IInstanceData
{
public:
  // VK_EXT_debug_utils
  PFN_vkSetDebugUtilsObjectNameEXT vkSetDebugUtilsObjectNameEXT = nullptr;
  PFN_vkSetDebugUtilsObjectTagEXT vkSetDebugUtilsObjectTagEXT = nullptr;
  PFN_vkQueueBeginDebugUtilsLabelEXT vkQueueBeginDebugUtilsLabelEXT = nullptr;
  PFN_vkQueueEndDebugUtilsLabelEXT vkQueueEndDebugUtilsLabelEXT = nullptr;
  PFN_vkQueueInsertDebugUtilsLabelEXT vkQueueInsertDebugUtilsLabelEXT = nullptr;
  PFN_vkCmdBeginDebugUtilsLabelEXT vkCmdBeginDebugUtilsLabelEXT = nullptr;
  PFN_vkCmdEndDebugUtilsLabelEXT vkCmdEndDebugUtilsLabelEXT = nullptr;
  PFN_vkCmdInsertDebugUtilsLabelEXT vkCmdInsertDebugUtilsLabelEXT = nullptr;
  PFN_vkCreateDebugUtilsMessengerEXT vkCreateDebugUtilsMessengerEXT = nullptr;
  PFN_vkDestroyDebugUtilsMessengerEXT vkDestroyDebugUtilsMessengerEXT = nullptr;
  PFN_vkSubmitDebugUtilsMessageEXT vkSubmitDebugUtilsMessageEXT = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return vkSetDebugUtilsObjectNameEXT != nullptr && vkSetDebugUtilsObjectTagEXT != nullptr &&
	   vkQueueBeginDebugUtilsLabelEXT != nullptr && vkQueueEndDebugUtilsLabelEXT != nullptr &&
	   vkQueueInsertDebugUtilsLabelEXT != nullptr && vkCmdBeginDebugUtilsLabelEXT != nullptr &&
	   vkCmdEndDebugUtilsLabelEXT != nullptr && vkCmdInsertDebugUtilsLabelEXT != nullptr &&
	   vkCreateDebugUtilsMessengerEXT != nullptr && vkDestroyDebugUtilsMessengerEXT != nullptr &&
	   vkSubmitDebugUtilsMessageEXT != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::vulkan

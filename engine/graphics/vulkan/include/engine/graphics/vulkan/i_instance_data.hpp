#pragma once

namespace engine::graphics::vulkan {
class IInstanceData
  {
  public:
    constexpr IInstanceData() noexcept = default;

    constexpr IInstanceData(IInstanceData const&) noexcept = default;

    constexpr IInstanceData(IInstanceData&&) noexcept = default;

    virtual constexpr ~IInstanceData() noexcept = default;

    constexpr auto operator=(IInstanceData const&) & noexcept -> IInstanceData& = default;

    constexpr auto operator=(IInstanceData&&) & noexcept -> IInstanceData& = default;

    [[nodiscard]] virtual constexpr auto is_loaded() const& noexcept -> bool = 0;

  protected:
  private:
  };
} // namespace engine::graphics::gl

#include "engine/graphics/vulkan/instance.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_VK_EXT_debug_utils_LOAD(name)                                                                           \
  m_data_ext_debug_utils.name = reinterpret_cast<BOOST_PP_CAT(PFN_, name)>(vkGetInstanceProcAddr(m_instance, #name));

#define ENGINE_VK_EXT_debug_utils_FUNCTION(name, ...)                                                                  \
  if (m_data_ext_debug_utils.name == nullptr) [[unlikely]] {                                                           \
    ENGINE_VK_EXT_debug_utils_LOAD(name)                                                                               \
  }                                                                                                                    \
  return m_data_ext_debug_utils.name(__VA_ARGS__);

auto
engine::graphics::vulkan::Instance::load_ext_debug_utils() & noexcept -> void
{
  ENGINE_VK_EXT_debug_utils_LOAD(vkSetDebugUtilsObjectNameEXT)
  ENGINE_VK_EXT_debug_utils_LOAD(vkSetDebugUtilsObjectTagEXT)
  ENGINE_VK_EXT_debug_utils_LOAD(vkQueueBeginDebugUtilsLabelEXT)
  ENGINE_VK_EXT_debug_utils_LOAD(vkQueueEndDebugUtilsLabelEXT)
  ENGINE_VK_EXT_debug_utils_LOAD(vkQueueInsertDebugUtilsLabelEXT)
  ENGINE_VK_EXT_debug_utils_LOAD(vkCmdBeginDebugUtilsLabelEXT)
  ENGINE_VK_EXT_debug_utils_LOAD(vkCmdEndDebugUtilsLabelEXT)
  ENGINE_VK_EXT_debug_utils_LOAD(vkCmdInsertDebugUtilsLabelEXT)
  ENGINE_VK_EXT_debug_utils_LOAD(vkCreateDebugUtilsMessengerEXT)
  ENGINE_VK_EXT_debug_utils_LOAD(vkDestroyDebugUtilsMessengerEXT)
  ENGINE_VK_EXT_debug_utils_LOAD(vkSubmitDebugUtilsMessageEXT)

}

auto
engine::graphics::vulkan::Instance::vkSetDebugUtilsObjectNameEXT(
  VkDevice device,
  VkDebugUtilsObjectNameInfoEXT const* pNameInfo) & noexcept -> VkResult
{
  ENGINE_VK_EXT_debug_utils_FUNCTION(vkSetDebugUtilsObjectNameEXT, device, pNameInfo)
}

auto
engine::graphics::vulkan::Instance::vkSetDebugUtilsObjectTagEXT(VkDevice device,
								VkDebugUtilsObjectTagInfoEXT const* pTagInfo) & noexcept
  -> VkResult
{
  ENGINE_VK_EXT_debug_utils_FUNCTION(vkSetDebugUtilsObjectTagEXT, device, pTagInfo)
}

auto
engine::graphics::vulkan::Instance::vkQueueBeginDebugUtilsLabelEXT(VkQueue queue,
								   VkDebugUtilsLabelEXT const* pLabelInfo) & noexcept
  -> void
{
  ENGINE_VK_EXT_debug_utils_FUNCTION(vkQueueBeginDebugUtilsLabelEXT, queue, pLabelInfo)
}

auto
engine::graphics::vulkan::Instance::vkQueueEndDebugUtilsLabelEXT(VkQueue queue) & noexcept -> void
{
  ENGINE_VK_EXT_debug_utils_FUNCTION(vkQueueEndDebugUtilsLabelEXT, queue)
}

auto
engine::graphics::vulkan::Instance::vkQueueInsertDebugUtilsLabelEXT(VkQueue queue,
								    VkDebugUtilsLabelEXT const* pLabelInfo) & noexcept
  -> void
{
  ENGINE_VK_EXT_debug_utils_FUNCTION(vkQueueInsertDebugUtilsLabelEXT, queue, pLabelInfo)
}

auto
engine::graphics::vulkan::Instance::vkCmdBeginDebugUtilsLabelEXT(VkCommandBuffer commandBuffer,
								 VkDebugUtilsLabelEXT const* pLabelInfo) & noexcept
  -> void
{
  ENGINE_VK_EXT_debug_utils_FUNCTION(vkCmdBeginDebugUtilsLabelEXT, commandBuffer, pLabelInfo)
}

auto
engine::graphics::vulkan::Instance::vkCmdEndDebugUtilsLabelEXT(VkCommandBuffer commandBuffer) & noexcept -> void
{
  ENGINE_VK_EXT_debug_utils_FUNCTION(vkCmdEndDebugUtilsLabelEXT, commandBuffer)
}

auto
engine::graphics::vulkan::Instance::vkCmdInsertDebugUtilsLabelEXT(VkCommandBuffer commandBuffer,
								  VkDebugUtilsLabelEXT const* pLabelInfo) & noexcept
  -> void
{
  ENGINE_VK_EXT_debug_utils_FUNCTION(vkCmdInsertDebugUtilsLabelEXT, commandBuffer, pLabelInfo)
}

auto
engine::graphics::vulkan::Instance::vkCreateDebugUtilsMessengerEXT(
  VkInstance instance,
  VkDebugUtilsMessengerCreateInfoEXT const* pCreateInfo,
  VkAllocationCallbacks const* pAllocator,
  VkDebugUtilsMessengerEXT* pMessenger) & noexcept -> VkResult
{
  ENGINE_VK_EXT_debug_utils_FUNCTION(vkCreateDebugUtilsMessengerEXT, instance, pCreateInfo, pAllocator, pMessenger)
}

auto
engine::graphics::vulkan::Instance::vkDestroyDebugUtilsMessengerEXT(VkInstance instance,
								    VkDebugUtilsMessengerEXT messenger,
								    VkAllocationCallbacks const* pAllocator) & noexcept
  -> void
{
  ENGINE_VK_EXT_debug_utils_FUNCTION(vkDestroyDebugUtilsMessengerEXT, instance, messenger, pAllocator)
}

auto
engine::graphics::vulkan::Instance::vkSubmitDebugUtilsMessageEXT(
  VkInstance instance,
  VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
  VkDebugUtilsMessageTypeFlagsEXT messageTypes,
  VkDebugUtilsMessengerCallbackDataEXT const* pCallbackData) & noexcept -> void
{
  ENGINE_VK_EXT_debug_utils_FUNCTION(
    vkSubmitDebugUtilsMessageEXT, instance, messageSeverity, messageTypes, pCallbackData)
}

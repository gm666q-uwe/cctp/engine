#include "engine/graphics/vulkan/device.hpp"

#include <memory>
#include <set>

engine::graphics::vulkan::Device::Device(engine::graphics::vulkan::Device&& other) noexcept
  : m_device(std::exchange(other.m_device, VK_NULL_HANDLE))
  , m_graphics_command_pool(std::exchange(other.m_graphics_command_pool, VK_NULL_HANDLE))
  , m_graphics_queue(std::exchange(other.m_graphics_queue, VK_NULL_HANDLE))
  , m_graphics_queue_family_index(std::exchange(other.m_graphics_queue_family_index, 0))
  , m_physical_device(std::exchange(other.m_physical_device, VK_NULL_HANDLE))
  , m_present_command_pool(std::exchange(other.m_present_command_pool, VK_NULL_HANDLE))
  , m_present_queue(std::exchange(other.m_present_queue, VK_NULL_HANDLE))
  , m_present_queue_family_index(std::exchange(other.m_present_queue_family_index, 0))
{}

engine::graphics::vulkan::Device::~Device() noexcept
{
  drop();
}

auto
engine::graphics::vulkan::Device::operator=(engine::graphics::vulkan::Device&& other) & noexcept
  -> engine::graphics::vulkan::Device&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_device = std::exchange(other.m_device, m_device);
  m_graphics_command_pool = std::exchange(other.m_graphics_command_pool, m_graphics_command_pool);
  m_graphics_queue = std::exchange(other.m_graphics_queue, m_graphics_queue);
  m_graphics_queue_family_index = std::exchange(other.m_graphics_queue_family_index, m_graphics_queue_family_index);
  m_physical_device = std::exchange(other.m_physical_device, m_physical_device);
  m_present_command_pool = std::exchange(other.m_present_command_pool, m_present_command_pool);
  m_present_queue = std::exchange(other.m_present_queue, m_present_queue);
  m_present_queue_family_index = std::exchange(other.m_present_queue_family_index, m_present_queue_family_index);

  return *this;
}

auto
engine::graphics::vulkan::Device::begin_command_buffer(VkCommandBuffer& command_buffer) const& noexcept
  -> result::Result<VkCommandBuffer, engine::Error>
{
  if (command_buffer == VK_NULL_HANDLE) {
    auto command_buffer_allocate_info = VkCommandBufferAllocateInfo{ VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
								     nullptr,
								     m_graphics_command_pool,
								     VK_COMMAND_BUFFER_LEVEL_PRIMARY,
								     1 };
    if (vkAllocateCommandBuffers(m_device, &command_buffer_allocate_info, &command_buffer) != VK_SUCCESS) {
      return std::move(result::Result<VkCommandBuffer, Error>::Err(Error::new_(0, -1)));
    }
  } else {
    if (vkResetCommandBuffer(command_buffer, 0) != VK_SUCCESS) {
      return std::move(result::Result<VkCommandBuffer, Error>::Err(Error::new_(0, -1)));
    }
  }

  auto command_buffer_begin_info =
    VkCommandBufferBeginInfo{ VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO, nullptr, 0, nullptr };
  if (vkBeginCommandBuffer(command_buffer, &command_buffer_begin_info) != VK_SUCCESS) {
    return std::move(result::Result<VkCommandBuffer, Error>::Err(Error::new_(0, -1)));
  }

  return std::move(result::Result<VkCommandBuffer, Error>::Ok(command_buffer));
}

auto
engine::graphics::vulkan::Device::begin_command_buffer_one() const& noexcept -> result::Result<VkCommandBuffer, Error>
{
  auto command_buffer = static_cast<VkCommandBuffer>(VK_NULL_HANDLE);
  auto command_buffer_allocate_info = VkCommandBufferAllocateInfo{
    VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO, nullptr, m_graphics_command_pool, VK_COMMAND_BUFFER_LEVEL_PRIMARY, 1
  };
  if (vkAllocateCommandBuffers(m_device, &command_buffer_allocate_info, &command_buffer) != VK_SUCCESS) {
    return std::move(result::Result<VkCommandBuffer, Error>::Err(Error::new_(0, -1)));
  }

  auto command_buffer_begin_info = VkCommandBufferBeginInfo{
    VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO, nullptr, VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT, nullptr
  };
  if (vkBeginCommandBuffer(command_buffer, &command_buffer_begin_info) != VK_SUCCESS) {
    return std::move(result::Result<VkCommandBuffer, Error>::Err(Error::new_(0, -1)));
  }

  return std::move(result::Result<VkCommandBuffer, Error>::Ok(command_buffer));
}

auto
engine::graphics::vulkan::Device::create_swap_chain(VkExtent2D extent, VkSurfaceKHR surface_khr) const& noexcept
  -> result::Result<SwapChain, Error>
{
  return std::move(SwapChain::new_(m_device,
				   extent,
				   m_graphics_queue_family_index,
				   m_physical_device,
				   m_present_command_pool,
				   m_present_queue,
				   m_present_queue_family_index,
				   surface_khr));
}

auto
engine::graphics::vulkan::Device::create_swap_chain_pointer(VkExtent2D extent, VkSurfaceKHR surface_khr) const& noexcept
  -> result::Result<SwapChain*, Error>
{
  return std::move(SwapChain::new_pointer(m_device,
					  extent,
					  m_graphics_queue_family_index,
					  m_physical_device,
					  m_present_command_pool,
					  m_present_queue,
					  m_present_queue_family_index,
					  surface_khr));
}

auto
engine::graphics::vulkan::Device::drop() & noexcept -> void
{
  if (m_present_command_pool != VK_NULL_HANDLE) {
    vkDestroyCommandPool(m_device, m_present_command_pool, nullptr);
    m_present_command_pool = VK_NULL_HANDLE;
  }
  if (m_graphics_command_pool != VK_NULL_HANDLE) {
    if (m_graphics_queue_family_index != m_present_queue_family_index) {
      vkDestroyCommandPool(m_device, m_graphics_command_pool, nullptr);
    }
    m_graphics_command_pool = VK_NULL_HANDLE;
  }

  m_present_queue = VK_NULL_HANDLE;
  m_graphics_queue = VK_NULL_HANDLE;
  if (m_device != VK_NULL_HANDLE) {
    vkDestroyDevice(m_device, nullptr);
    m_device = VK_NULL_HANDLE;
  }

  m_present_queue_family_index = 0;
  m_graphics_queue_family_index = 0;
  m_physical_device = VK_NULL_HANDLE;
}

auto
engine::graphics::vulkan::Device::end_command_buffer(
  VkCommandBuffer command_buffer,
  bool submit_only,
  engine::graphics::vulkan::SwapChain::Sync const& sync) const& noexcept -> result::Result<std::nullptr_t, Error>
{
  if (!submit_only) {
    if (vkEndCommandBuffer(command_buffer) != VK_SUCCESS) {
      return std::move(result::Result<std::nullptr_t, Error>::Err(Error::new_(0, -1)));
    }
  }

  auto pipe_stage_flags = static_cast<VkPipelineStageFlags>(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);
  auto submit_info = VkSubmitInfo{
    VK_STRUCTURE_TYPE_SUBMIT_INFO, nullptr, 1, &sync.image_acquired_semaphore, &pipe_stage_flags, 1, &command_buffer, 1,
    &sync.draw_complete_semaphore
  };
  if (vkQueueSubmit(m_graphics_queue, 1, &submit_info, sync.fence)) {
    return std::move(result::Result<std::nullptr_t, Error>::Err(Error::new_(0, -1)));
  }

  return result::Result<std::nullptr_t, Error>::Ok(nullptr);
}

auto
engine::graphics::vulkan::Device::end_command_buffer_one(VkCommandBuffer command_buffer) const& noexcept
  -> result::Result<std::nullptr_t, Error>
{
  if (vkEndCommandBuffer(command_buffer) != VK_SUCCESS) {
    return std::move(result::Result<std::nullptr_t, Error>::Err(Error::new_(0, -1)));
  }

  auto submit_info =
    VkSubmitInfo{ VK_STRUCTURE_TYPE_SUBMIT_INFO, nullptr, 0, nullptr, nullptr, 1, &command_buffer, 0, 0 };
  if (vkQueueSubmit(m_graphics_queue, 1, &submit_info, VK_NULL_HANDLE)) {
    return std::move(result::Result<std::nullptr_t, Error>::Err(Error::new_(0, -1)));
  }
  vkQueueWaitIdle(m_graphics_queue);
  vkFreeCommandBuffers(m_device, m_graphics_command_pool, 1, &command_buffer);

  return result::Result<std::nullptr_t, Error>::Ok(nullptr);
}

auto
engine::graphics::vulkan::Device::initialize(VkInstance instance, VkSurfaceKHR surface_khr) & noexcept
  -> option::Option<Error>
{
  auto physical_device_count = static_cast<uint32_t>(0);
  vkEnumeratePhysicalDevices(instance, &physical_device_count, nullptr);
  auto physical_devices = Vec<VkPhysicalDevice>(physical_device_count);
  vkEnumeratePhysicalDevices(instance, &physical_device_count, physical_devices.data());

  for (auto physical_device : physical_devices) {
    if (is_physical_device_usable(physical_device, surface_khr)) {
      m_physical_device = physical_device;
      break;
    }
  }

  if (m_physical_device == VK_NULL_HANDLE) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  vkGetPhysicalDeviceMemoryProperties(m_physical_device, &m_physical_device_memory_properties);

  static constexpr auto const queue_priorities = 0.0f;

  auto device_queue_create_infos = Vec<VkDeviceQueueCreateInfo>();
  std::set unique_queue_family_indices = { m_graphics_queue_family_index, m_present_queue_family_index };
  for (auto unique_queue_family_index : unique_queue_family_indices) {
    device_queue_create_infos.push_back(VkDeviceQueueCreateInfo{ .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
								 .pNext = nullptr,
								 .flags = 0,
								 .queueFamilyIndex = unique_queue_family_index,
								 .queueCount = 1,
								 .pQueuePriorities = &queue_priorities });
  }

  auto physical_device_features = VkPhysicalDeviceFeatures{ .geometryShader = VK_TRUE, .tessellationShader = VK_TRUE };

  auto device_extensions = Vec<char const*>{ VK_KHR_SWAPCHAIN_EXTENSION_NAME };

  auto device_create_info = VkDeviceCreateInfo{ VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
						nullptr,
						0,
						static_cast<uint32_t>(device_queue_create_infos.size()),
						device_queue_create_infos.data(),
						0,
						nullptr,
						static_cast<uint32_t>(device_extensions.size()),
						device_extensions.data(),
						&physical_device_features };

  if (vkCreateDevice(m_physical_device, &device_create_info, nullptr, &m_device) != VK_SUCCESS) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  vkGetDeviceQueue(m_device, m_graphics_queue_family_index, 0, &m_graphics_queue);
  vkGetDeviceQueue(m_device, m_present_queue_family_index, 0, &m_present_queue);

  auto command_pool_create_info = VkCommandPoolCreateInfo{ VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
							   nullptr,
							   VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
							   m_graphics_queue_family_index };
  if (vkCreateCommandPool(m_device, &command_pool_create_info, nullptr, &m_graphics_command_pool) != VK_SUCCESS) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  if (m_graphics_queue_family_index == m_present_queue_family_index) {
    m_present_command_pool = m_graphics_command_pool;
  } else {
    command_pool_create_info.queueFamilyIndex = m_present_queue_family_index;
    if (vkCreateCommandPool(m_device, &command_pool_create_info, nullptr, &m_present_command_pool) != VK_SUCCESS) {
      return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
    }
  }

  return option::Option<Error>::None();
}

auto
engine::graphics::vulkan::Device::is_physical_device_usable(VkPhysicalDevice physical_device,
							    VkSurfaceKHR surface_khr) & noexcept -> bool
{
  auto has_graphics_family = false;
  auto has_present_family = false;

  auto physical_device_features = VkPhysicalDeviceFeatures{};
  vkGetPhysicalDeviceFeatures(physical_device, &physical_device_features);

  auto physical_device_properties = VkPhysicalDeviceProperties{};
  vkGetPhysicalDeviceProperties(physical_device, &physical_device_properties);

  auto physical_device_queue_family_properties_count = static_cast<uint32_t>(0);
  vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &physical_device_queue_family_properties_count, nullptr);
  auto physical_device_queue_family_properties =
    Vec<VkQueueFamilyProperties>(physical_device_queue_family_properties_count);
  vkGetPhysicalDeviceQueueFamilyProperties(
    physical_device, &physical_device_queue_family_properties_count, physical_device_queue_family_properties.data());

  for (usize i = 0; i < physical_device_queue_family_properties.size(); i++) {
    if (!has_graphics_family &&
	(physical_device_queue_family_properties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) == VK_QUEUE_GRAPHICS_BIT) {
      has_graphics_family = true;
      m_graphics_queue_family_index = i;
    }
    if (!has_present_family) {
      auto present_support = static_cast<VkBool32>(VK_FALSE);
      vkGetPhysicalDeviceSurfaceSupportKHR(physical_device, i, surface_khr, &present_support);
      if (present_support == VK_TRUE) {
	has_present_family = true;
	m_present_queue_family_index = i;
      }
    }
    if (has_graphics_family && has_present_family) {
      break;
    }
  }

  return has_graphics_family && has_present_family && physical_device_features.geometryShader == VK_TRUE &&
	 physical_device_features.tessellationShader == VK_TRUE &&
	 physical_device_properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;
}

auto
engine::graphics::vulkan::Device::new_(VkInstance instance, VkSurfaceKHR surface_khr) noexcept
  -> result::Result<Device, Error>
{
  auto self = Device();
  if (auto result = self.initialize(instance, surface_khr); result.is_some()) {
    return std::move(result::Result<Device, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Device, Error>::Ok(std::move(self)));
}

auto
engine::graphics::vulkan::Device::new_pointer(VkInstance instance, VkSurfaceKHR surface_khr) noexcept
  -> result::Result<Device*, Error>
{
  auto self = std::unique_ptr<Device>(new Device());
  if (auto result = self->initialize(instance, surface_khr); result.is_some()) {
    return std::move(result::Result<Device*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Device*, Error>::Ok(self.release()));
}

#include "engine/graphics/vulkan/swap_chain.hpp"

#include <iostream>
#include <limits>
#include <memory>

#include "utility.hpp"

engine::graphics::vulkan::SwapChain::SwapChain(VkDevice device,
					       uint32_t graphics_queue_family_index,
					       VkPhysicalDevice physical_device,
					       VkCommandPool present_command_pool,
					       VkQueue present_queue,
					       uint32_t present_queue_family_index,
					       VkSurfaceKHR surface_khr) noexcept
  : m_device(device)
  , m_physical_device(physical_device)
  , m_present_command_pool(present_command_pool)
  , m_present_queue(present_queue)
  , m_queue_family_indices(std::array<uint32_t, 2>{ graphics_queue_family_index, present_queue_family_index })
  , m_surface_khr(surface_khr)
{}

engine::graphics::vulkan::SwapChain::SwapChain(engine::graphics::vulkan::SwapChain&& other) noexcept
  : m_current_frame(std::exchange(other.m_current_frame, 0))
  , m_current_image(std::exchange(other.m_current_image, 0))
  , m_depth(std::exchange(other.m_depth, Depth{}))
  , m_device(other.m_device)
  , m_extent(std::exchange(other.m_extent, VkExtent2D{}))
  , m_format(std::exchange(other.m_format, VK_FORMAT_UNDEFINED))
  , m_images(std::move(other.m_images))
  , m_physical_device(other.m_physical_device)
  , m_present_command_pool(other.m_present_command_pool)
  , m_present_queue(other.m_present_queue)
  , m_queue_family_indices(other.m_queue_family_indices)
  , m_render_pass(std::exchange(other.m_render_pass, VK_NULL_HANDLE))
  , m_syncs(std::exchange(other.m_syncs, std::array<Sync, M_MAX_FRAMES_IN_FLIGHT>{}))
  , m_swapchain_khr(std::exchange(other.m_swapchain_khr, VK_NULL_HANDLE))
{}

engine::graphics::vulkan::SwapChain::~SwapChain() noexcept
{
  drop();
}

auto
engine::graphics::vulkan::SwapChain::operator=(engine::graphics::vulkan::SwapChain&& other) & noexcept
  -> engine::graphics::vulkan::SwapChain&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_current_frame = std::exchange(other.m_current_frame, 0);
  m_current_image = std::exchange(other.m_current_image, 0);
  m_depth = std::exchange(other.m_depth, Depth{});
  m_device = other.m_device;
  m_extent = std::exchange(other.m_extent, VkExtent2D{});
  m_format = std::exchange(other.m_format, VK_FORMAT_UNDEFINED);
  m_images = std::move(other.m_images);
  m_physical_device = other.m_physical_device;
  m_present_command_pool = other.m_present_command_pool;
  m_present_queue = other.m_present_queue;
  m_queue_family_indices = other.m_queue_family_indices;
  m_render_pass = std::exchange(other.m_render_pass, VK_NULL_HANDLE);
  m_syncs = std::exchange(other.m_syncs, std::array<Sync, M_MAX_FRAMES_IN_FLIGHT>{});
  m_swapchain_khr = std::exchange(other.m_swapchain_khr, VK_NULL_HANDLE);

  return *this;
}

auto
engine::graphics::vulkan::SwapChain::acquire_image() & noexcept -> void
{
  vkWaitForFences(m_device, 1, &m_syncs[m_current_frame].fence, VK_TRUE, std::numeric_limits<uint64_t>::max());
  vkResetFences(m_device, 1, &m_syncs[m_current_frame].fence);

  auto result = vkAcquireNextImageKHR(m_device,
				      m_swapchain_khr,
				      std::numeric_limits<uint64_t>::max(),
				      m_syncs[m_current_frame].image_acquired_semaphore,
				      VK_NULL_HANDLE,
				      &m_current_image);
  switch (result) {
    case VK_SUCCESS:
      break;
    case VK_NOT_READY:
      std::cout << "VK_NOT_READY" << std::endl;
      break;
    case VK_TIMEOUT:
      std::cout << "VK_TIMEOUT" << std::endl;
      break;
    case VK_ERROR_OUT_OF_HOST_MEMORY:
      std::cout << "VK_ERROR_OUT_OF_HOST_MEMORY" << std::endl;
      break;
    case VK_ERROR_OUT_OF_DEVICE_MEMORY:
      std::cout << "VK_ERROR_OUT_OF_DEVICE_MEMORY" << std::endl;
      break;
    case VK_ERROR_DEVICE_LOST:
      std::cout << "VK_ERROR_DEVICE_LOST" << std::endl;
      break;
    case VK_ERROR_SURFACE_LOST_KHR:
      std::cout << "VK_ERROR_SURFACE_LOST_KHR" << std::endl;
      break;
    case VK_SUBOPTIMAL_KHR:
      std::cout << "VK_SUBOPTIMAL_KHR" << std::endl;
      break;
    case VK_ERROR_OUT_OF_DATE_KHR:
      std::cout << "VK_ERROR_OUT_OF_DATE_KHR" << std::endl;
      break;
    case VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT:
      std::cout << "VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT" << std::endl;
      break;
    default:
      break;
  }
}

auto
engine::graphics::vulkan::SwapChain::current_image() & noexcept -> engine::graphics::vulkan::SwapChain::Image&
{
  return m_images[m_current_image];
}

auto
engine::graphics::vulkan::SwapChain::drop() & noexcept -> void
{
  for (auto& sync : m_syncs) {
    sync.drop(m_device);
  }

  if (m_render_pass != VK_NULL_HANDLE) {
    vkDestroyRenderPass(m_device, m_render_pass, nullptr);
    m_render_pass = VK_NULL_HANDLE;
  }

  for (auto& image : m_images) {
    image.drop_1(m_device);
  }

  m_depth.drop(m_device);

  for (auto& image : m_images) {
    image.drop_2(m_device, m_present_command_pool);
  }
  m_images.clear();

  m_format = VK_FORMAT_UNDEFINED;
  m_extent = VkExtent2D{};
  if (m_swapchain_khr != VK_NULL_HANDLE) {
    vkDestroySwapchainKHR(m_device, m_swapchain_khr, nullptr);
    m_swapchain_khr = VK_NULL_HANDLE;
  }
}

auto
engine::graphics::vulkan::SwapChain::free_command_buffers(VkCommandPool graphics_command_pool) & noexcept -> void
{
  for (auto& image : m_images) {
    image.drop_3(m_device, graphics_command_pool);
  }
}

auto
engine::graphics::vulkan::SwapChain::initialize(VkExtent2D extent) & noexcept -> option::Option<Error>
{
  auto fence_create_info =
    VkFenceCreateInfo{ VK_STRUCTURE_TYPE_FENCE_CREATE_INFO, nullptr, VK_FENCE_CREATE_SIGNALED_BIT };
  auto semaphore_create_info = VkSemaphoreCreateInfo{ VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO, nullptr, 0 };
  for (auto& sync : m_syncs) {
    if (vkCreateFence(m_device, &fence_create_info, nullptr, &sync.fence) != VK_SUCCESS) {
      return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
    }
    if (vkCreateSemaphore(m_device, &semaphore_create_info, nullptr, &sync.draw_complete_semaphore) != VK_SUCCESS) {
      return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
    }
    if (vkCreateSemaphore(m_device, &semaphore_create_info, nullptr, &sync.image_acquired_semaphore) != VK_SUCCESS) {
      return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
    }
    if (vkCreateSemaphore(m_device, &semaphore_create_info, nullptr, &sync.image_transfer_semaphore) != VK_SUCCESS) {
      return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
    }
  }

  if (auto result = initialize_swapchain(extent); result.is_some()) {
    return std::move(result);
  }

  if (auto result = initialize_depth(); result.is_some()) {
    return std::move(result);
  }

  if (auto result = initialize_render_pass(); result.is_some()) {
    return std::move(result);
  }

  if (auto result = initialize_framebuffers(); result.is_some()) {
    return std::move(result);
  }

  return std::move(option::Option<Error>::None());
}

auto
engine::graphics::vulkan::SwapChain::initialize_depth() & noexcept -> option::Option<Error>
{
  static constexpr auto const formats =
    // std::array<VkFormat, 3>{ VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT, VK_FORMAT_D16_UNORM_S8_UINT
    // };
    std::array<VkFormat, 2>{ VK_FORMAT_D32_SFLOAT, VK_FORMAT_D16_UNORM };

  if (m_depth.format == VK_FORMAT_UNDEFINED) {
    m_depth.format = select_format(
      formats, VkFormatProperties{ 0, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT, 0 }, m_physical_device);
  }

  auto image_create_info = VkImageCreateInfo{ VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
					      nullptr,
					      0,
					      VK_IMAGE_TYPE_2D,
					      m_depth.format,
					      VkExtent3D{ m_extent.width, m_extent.height, 1 },
					      1,
					      1,
					      VK_SAMPLE_COUNT_1_BIT,
					      VK_IMAGE_TILING_OPTIMAL,
					      VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
					      VK_SHARING_MODE_EXCLUSIVE,
					      0,
					      nullptr,
					      VK_IMAGE_LAYOUT_UNDEFINED };
  if (image_create_info.format == VK_FORMAT_UNDEFINED) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }
  if (vkCreateImage(m_device, &image_create_info, nullptr, &m_depth.image) != VK_SUCCESS) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  auto memory_requirements = VkMemoryRequirements{};
  vkGetImageMemoryRequirements(m_device, m_depth.image, &memory_requirements);
  auto memory_allocate_info =
    VkMemoryAllocateInfo{ VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO, nullptr, memory_requirements.size, 0 };
  if (!select_memory_type(m_physical_device,
			  VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
			  memory_requirements.memoryTypeBits,
			  &memory_allocate_info.memoryTypeIndex)) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }
  if (vkAllocateMemory(m_device, &memory_allocate_info, nullptr, &m_depth.memory) != VK_SUCCESS) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }
  vkBindImageMemory(m_device, m_depth.image, m_depth.memory, 0);

  auto image_view_create_info = VkImageViewCreateInfo{
    VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
    nullptr,
    0,
    m_depth.image,
    VK_IMAGE_VIEW_TYPE_2D,
    image_create_info.format,
    VkComponentMapping{},
    VkImageSubresourceRange{ VK_IMAGE_ASPECT_DEPTH_BIT /*| VK_IMAGE_ASPECT_STENCIL_BIT*/, 0, 1, 0, 1 }
  };
  if (vkCreateImageView(m_device, &image_view_create_info, nullptr, &m_depth.image_view) != VK_SUCCESS) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  return option::Option<Error>::None();
}

auto
engine::graphics::vulkan::SwapChain::initialize_framebuffers() & noexcept -> option::Option<Error>
{
  auto attachments = std::array<VkImageView, 2>{ VK_NULL_HANDLE, m_depth.image_view };
  auto framebuffer_create_info = VkFramebufferCreateInfo{ VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
							  nullptr,
							  0,
							  m_render_pass,
							  attachments.size(),
							  attachments.data(),
							  m_extent.width,
							  m_extent.height,
							  1 };
  for (auto& image : m_images) {
    attachments[0] = image.image_view;
    if (vkCreateFramebuffer(m_device, &framebuffer_create_info, nullptr, &image.framebuffer)) {
      return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
    }
  }

  return option::Option<Error>::None();
}

auto
engine::graphics::vulkan::SwapChain::initialize_render_pass() & noexcept -> option::Option<Error>
{
  auto attachment_descriptions = std::array<VkAttachmentDescription, 2>{
    VkAttachmentDescription{
      0,
      m_format,
      VK_SAMPLE_COUNT_1_BIT,
      VK_ATTACHMENT_LOAD_OP_CLEAR,
      VK_ATTACHMENT_STORE_OP_STORE,
      VK_ATTACHMENT_LOAD_OP_DONT_CARE,
      VK_ATTACHMENT_STORE_OP_DONT_CARE,
      VK_IMAGE_LAYOUT_UNDEFINED,
      VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
    },
    VkAttachmentDescription{ 0,
			     m_depth.format,
			     VK_SAMPLE_COUNT_1_BIT,
			     VK_ATTACHMENT_LOAD_OP_CLEAR,
			     VK_ATTACHMENT_STORE_OP_DONT_CARE,
			     VK_ATTACHMENT_LOAD_OP_DONT_CARE,
			     VK_ATTACHMENT_STORE_OP_DONT_CARE,
			     VK_IMAGE_LAYOUT_UNDEFINED,
			     VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL }
  };
  auto color_attachment_reference = VkAttachmentReference{ 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };
  auto depth_attachment_reference = VkAttachmentReference{ 1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL };

  auto subpass_description =
    VkSubpassDescription{ 0,	   VK_PIPELINE_BIND_POINT_GRAPHICS, 0, nullptr, 1, &color_attachment_reference,
			  nullptr, &depth_attachment_reference,	    0, nullptr };

  auto subpass_dependencies = std::array<VkSubpassDependency, 2>{
    VkSubpassDependency{ VK_SUBPASS_EXTERNAL,
			 0,
			 VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT,
			 VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT,
			 VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
			 VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
			 0 },
    VkSubpassDependency{ VK_SUBPASS_EXTERNAL,
			 0,
			 VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
			 VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
			 0,
			 VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
			 0 }
  };

  auto render_pass_create_info = VkRenderPassCreateInfo{ VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
							 nullptr,
							 0,
							 attachment_descriptions.size(),
							 attachment_descriptions.data(),
							 1,
							 &subpass_description,
							 subpass_dependencies.size(),
							 subpass_dependencies.data() };

  if (vkCreateRenderPass(m_device, &render_pass_create_info, nullptr, &m_render_pass) != VK_SUCCESS) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  return option::Option<Error>::None();
}

auto
engine::graphics::vulkan::SwapChain::initialize_swapchain(VkExtent2D extent) & noexcept -> option::Option<Error>
{
  auto old_swapchain_khr = m_swapchain_khr;

  auto graphic_is_present = m_queue_family_indices[0] == m_queue_family_indices[1];

  auto swapchain_create_info_khr =
    VkSwapchainCreateInfoKHR{ VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
			      nullptr,
			      0,
			      m_surface_khr,
			      M_DESIRED_IMAGE_COUNT,
			      VK_FORMAT_UNDEFINED,
			      VK_COLOR_SPACE_SRGB_NONLINEAR_KHR,
			      extent,
			      1,
			      VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
			      graphic_is_present ? VK_SHARING_MODE_EXCLUSIVE : VK_SHARING_MODE_CONCURRENT,
			      graphic_is_present ? 0 : static_cast<uint32_t>(m_queue_family_indices.size()),
			      graphic_is_present ? nullptr : m_queue_family_indices.data(),
			      VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR,
			      VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
			      VK_PRESENT_MODE_FIFO_KHR,
			      VK_TRUE,
			      old_swapchain_khr };

  // Capabilities check
  auto surface_capabilities_khr = VkSurfaceCapabilitiesKHR{};
  if (vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_physical_device, m_surface_khr, &surface_capabilities_khr) !=
      VK_SUCCESS) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  if (surface_capabilities_khr.currentExtent.height == std::numeric_limits<uint32_t>::max() &&
      surface_capabilities_khr.currentExtent.width == std::numeric_limits<uint32_t>::max()) {
    if (swapchain_create_info_khr.imageExtent.height < surface_capabilities_khr.minImageExtent.height) {
      swapchain_create_info_khr.imageExtent.height = surface_capabilities_khr.minImageExtent.height;
    } else if (swapchain_create_info_khr.imageExtent.height > surface_capabilities_khr.maxImageExtent.height) {
      swapchain_create_info_khr.imageExtent.height = surface_capabilities_khr.maxImageExtent.height;
    }

    if (swapchain_create_info_khr.imageExtent.width < surface_capabilities_khr.minImageExtent.width) {
      swapchain_create_info_khr.imageExtent.width = surface_capabilities_khr.minImageExtent.width;
    } else if (swapchain_create_info_khr.imageExtent.width > surface_capabilities_khr.maxImageExtent.width) {
      swapchain_create_info_khr.imageExtent.width = surface_capabilities_khr.maxImageExtent.width;
    }
  } else {
    swapchain_create_info_khr.imageExtent = surface_capabilities_khr.currentExtent;
  }
  if (swapchain_create_info_khr.minImageCount < surface_capabilities_khr.minImageCount) {
    swapchain_create_info_khr.minImageCount = surface_capabilities_khr.minImageCount;
  }
  if (surface_capabilities_khr.maxImageCount > 0 &&
      swapchain_create_info_khr.minImageCount > surface_capabilities_khr.maxImageCount) {
    swapchain_create_info_khr.minImageCount = surface_capabilities_khr.maxImageCount;
  }
  for (auto i = 0; i < 4; i++) {
    if ((surface_capabilities_khr.supportedCompositeAlpha & (1 << i)) == (1 << i)) {
      swapchain_create_info_khr.compositeAlpha = static_cast<VkCompositeAlphaFlagBitsKHR>(1 << i);
      break;
    }
  }
  if ((surface_capabilities_khr.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) !=
      VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) {
    swapchain_create_info_khr.preTransform = surface_capabilities_khr.currentTransform;
  }

  // Format check
  auto surface_format_khr_count = static_cast<uint32_t>(0);
  vkGetPhysicalDeviceSurfaceFormatsKHR(m_physical_device, m_surface_khr, &surface_format_khr_count, nullptr);
  auto surface_format_khrs = Vec<VkSurfaceFormatKHR>(surface_format_khr_count);
  vkGetPhysicalDeviceSurfaceFormatsKHR(
    m_physical_device, m_surface_khr, &surface_format_khr_count, surface_format_khrs.data());

  swapchain_create_info_khr.imageColorSpace = surface_format_khrs[0].colorSpace;
  swapchain_create_info_khr.imageFormat = surface_format_khrs[0].format;
  for (auto surface_format_khr : surface_format_khrs) {
    if (surface_format_khr.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR &&
	surface_format_khr.format == VK_FORMAT_B8G8R8A8_SRGB) {
      swapchain_create_info_khr.imageColorSpace = surface_format_khr.colorSpace;
      swapchain_create_info_khr.imageFormat = surface_format_khr.format;
      break;
    }
  }

  // Present mode check
  auto present_mode_khr_count = static_cast<uint32_t>(0);
  vkGetPhysicalDeviceSurfacePresentModesKHR(m_physical_device, m_surface_khr, &present_mode_khr_count, nullptr);
  auto present_mode_khrs = Vec<VkPresentModeKHR>(present_mode_khr_count);
  vkGetPhysicalDeviceSurfacePresentModesKHR(
    m_physical_device, m_surface_khr, &present_mode_khr_count, present_mode_khrs.data());

  for (auto present_mode_khr : present_mode_khrs) {
    if (present_mode_khr == VK_PRESENT_MODE_MAILBOX_KHR) {
      swapchain_create_info_khr.presentMode = present_mode_khr;
    }
  }

  if (vkCreateSwapchainKHR(m_device, &swapchain_create_info_khr, nullptr, &m_swapchain_khr) != VK_SUCCESS) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }
  if (old_swapchain_khr != VK_NULL_HANDLE) {
    vkDestroySwapchainKHR(m_device, old_swapchain_khr, nullptr);
  }

  m_extent = swapchain_create_info_khr.imageExtent;
  m_format = swapchain_create_info_khr.imageFormat;

  auto swapchain_khr_image_count = static_cast<uint32_t>(0);
  vkGetSwapchainImagesKHR(m_device, m_swapchain_khr, &swapchain_khr_image_count, nullptr);
  auto images = Vec<VkImage>(swapchain_khr_image_count);
  vkGetSwapchainImagesKHR(m_device, m_swapchain_khr, &swapchain_khr_image_count, images.data());

  m_images.resize(images.size());
  for (usize i = 0; i < images.size(); i++) {
    auto& image = m_images[i];
    image.image = images[i];

    auto image_view_create_info =
      VkImageViewCreateInfo{ VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
			     nullptr,
			     0,
			     image.image,
			     VK_IMAGE_VIEW_TYPE_2D,
			     m_format,
			     VkComponentMapping{ VK_COMPONENT_SWIZZLE_IDENTITY,
						 VK_COMPONENT_SWIZZLE_IDENTITY,
						 VK_COMPONENT_SWIZZLE_IDENTITY,
						 VK_COMPONENT_SWIZZLE_IDENTITY },
			     VkImageSubresourceRange{ VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 } };
    if (vkCreateImageView(m_device, &image_view_create_info, nullptr, &image.image_view) != VK_SUCCESS) {
      return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
    }

    initialize_transfer_command_buffer(image);
  }

  return option::Option<Error>::None();
}

auto
engine::graphics::vulkan::SwapChain::initialize_transfer_command_buffer(
  engine::graphics::vulkan::SwapChain::Image& image) & noexcept -> option::Option<Error>
{
  auto command_buffer_allocate_info = VkCommandBufferAllocateInfo{
    VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO, nullptr, m_present_command_pool, VK_COMMAND_BUFFER_LEVEL_PRIMARY, 1
  };
  if (vkAllocateCommandBuffers(m_device, &command_buffer_allocate_info, &image.transfer_command_buffer) != VK_SUCCESS) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  auto command_buffer_begin_info = VkCommandBufferBeginInfo{
    VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO, nullptr, VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT, nullptr
  };
  if (vkBeginCommandBuffer(image.transfer_command_buffer, &command_buffer_begin_info) != VK_SUCCESS) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  auto image_memory_barrier = VkImageMemoryBarrier{ VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
						    nullptr,
						    0,
						    0,
						    VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
						    VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
						    m_queue_family_indices[0],
						    m_queue_family_indices[1],
						    image.image,
						    VkImageSubresourceRange{ VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 } };
  vkCmdPipelineBarrier(image.transfer_command_buffer,
		       VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
		       VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
		       0,
		       0,
		       nullptr,
		       0,
		       nullptr,
		       1,
		       &image_memory_barrier);

  if (vkEndCommandBuffer(image.transfer_command_buffer) != VK_SUCCESS) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  return option::Option<Error>::None();
}

auto
engine::graphics::vulkan::SwapChain::new_(VkDevice device,
					  VkExtent2D extent,
					  uint32_t graphics_queue_family_index,
					  VkPhysicalDevice physical_device,
					  VkCommandPool present_command_pool,
					  VkQueue present_queue,
					  uint32_t present_queue_family_index,
					  VkSurfaceKHR surface_khr) noexcept -> result::Result<SwapChain, Error>
{
  auto self = SwapChain(device,
			graphics_queue_family_index,
			physical_device,
			present_command_pool,
			present_queue,
			present_queue_family_index,
			surface_khr);
  if (auto result = self.initialize(extent); result.is_some()) {
    return std::move(result::Result<SwapChain, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<SwapChain, Error>::Ok(std::move(self)));
}

auto
engine::graphics::vulkan::SwapChain::new_pointer(VkDevice device,
						 VkExtent2D extent,
						 uint32_t graphics_queue_family_index,
						 VkPhysicalDevice physical_device,
						 VkCommandPool present_command_pool,
						 VkQueue present_queue,
						 uint32_t present_queue_family_index,
						 VkSurfaceKHR surface_khr) noexcept -> result::Result<SwapChain*, Error>
{
  auto self = std::unique_ptr<SwapChain>(new SwapChain(device,
						       graphics_queue_family_index,
						       physical_device,
						       present_command_pool,
						       present_queue,
						       present_queue_family_index,
						       surface_khr));
  if (auto result = self->initialize(extent); result.is_some()) {
    return std::move(result::Result<SwapChain*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<SwapChain*, Error>::Ok(self.release()));
}

auto
engine::graphics::vulkan::SwapChain::on_resize(engine::u16 height, engine::u16 width) & noexcept -> void
{
  vkDeviceWaitIdle(m_device);

  {
    if (m_render_pass != VK_NULL_HANDLE) {
      vkDestroyRenderPass(m_device, m_render_pass, nullptr);
      m_render_pass = VK_NULL_HANDLE;
    }

    for (auto& image : m_images) {
      image.drop_1(m_device);
    }

    m_depth.drop(m_device);

    for (auto& image : m_images) {
      image.drop_2(m_device, m_present_command_pool);
    }
    m_images.clear();
  }

  if (auto result = initialize_swapchain(VkExtent2D{ width, height }); result.is_some()) {
    // return std::move(result);
  }

  if (auto result = initialize_depth(); result.is_some()) {
    // return std::move(result);
  }

  if (auto result = initialize_render_pass(); result.is_some()) {
    // return std::move(result);
  }

  if (auto result = initialize_framebuffers(); result.is_some()) {
    // return std::move(result);
  }
}

auto
engine::graphics::vulkan::SwapChain::present() & noexcept -> void
{
  auto graphics_is_present = m_queue_family_indices[0] == m_queue_family_indices[1];
  auto const& sync = m_syncs[m_current_frame];

  if (!graphics_is_present) {
    auto pipe_stage_flags = static_cast<VkPipelineStageFlags>(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);
    auto submit_info = VkSubmitInfo{ VK_STRUCTURE_TYPE_SUBMIT_INFO,
				     nullptr,
				     1,
				     &sync.draw_complete_semaphore,
				     &pipe_stage_flags,
				     1,
				     &m_images[m_current_image].transfer_command_buffer,
				     1,
				     &sync.image_transfer_semaphore };

    if (vkQueueSubmit(m_present_queue, 1, &submit_info, VK_NULL_HANDLE) != VK_SUCCESS) {
      // return std::move(option::Option<Error>::Some(Error{ .source = 0, .code = -1 }));
    }
    if (vkQueueWaitIdle(m_present_queue) != VK_SUCCESS) {
      // return std::move(option::Option<Error>::Some(Error{ .source = 0, .code = -1 }));
    }
  }

  auto present_info_khr =
    VkPresentInfoKHR{ VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
		      nullptr,
		      1,
		      graphics_is_present ? &sync.draw_complete_semaphore : &sync.image_transfer_semaphore,
		      1,
		      &m_swapchain_khr,
		      &m_current_image,
		      nullptr };

  auto result = vkQueuePresentKHR(m_present_queue, &present_info_khr);
  switch (result) {
    case VK_SUCCESS:
      break;
    case VK_ERROR_OUT_OF_HOST_MEMORY:
      std::cout << "VK_ERROR_OUT_OF_HOST_MEMORY" << std::endl;
      break;
    case VK_ERROR_OUT_OF_DEVICE_MEMORY:
      std::cout << "VK_ERROR_OUT_OF_DEVICE_MEMORY" << std::endl;
      break;
    case VK_ERROR_DEVICE_LOST:
      std::cout << "VK_ERROR_DEVICE_LOST" << std::endl;
      break;
    case VK_ERROR_SURFACE_LOST_KHR:
      std::cout << "VK_ERROR_SURFACE_LOST_KHR" << std::endl;
      break;
    case VK_SUBOPTIMAL_KHR:
      std::cout << "VK_SUBOPTIMAL_KHR" << std::endl;
      break;
    case VK_ERROR_OUT_OF_DATE_KHR:
      std::cout << "VK_ERROR_OUT_OF_DATE_KHR" << std::endl;
      // on_resize(0, 0);
      break;
    case VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT:
      std::cout << "VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT" << std::endl;
      break;
    default:
      break;
  }
  m_current_frame = (m_current_frame + 1) % M_MAX_FRAMES_IN_FLIGHT;
}

auto
engine::graphics::vulkan::SwapChain::Depth::drop(VkDevice device) & noexcept -> void
{
  // format = VK_FORMAT_UNDEFINED;
  if (image_view != VK_NULL_HANDLE) {
    vkDestroyImageView(device, image_view, nullptr);
    image_view = VK_NULL_HANDLE;
  }

  if (memory != VK_NULL_HANDLE) {
    vkFreeMemory(device, memory, nullptr);
    memory = VK_NULL_HANDLE;
  }

  if (image != VK_NULL_HANDLE) {
    vkDestroyImage(device, image, nullptr);
    image = VK_NULL_HANDLE;
  }
}

auto
engine::graphics::vulkan::SwapChain::Image::drop_1(VkDevice device) & noexcept -> void
{
  is_command_buffer_valid = false;
  if (framebuffer != VK_NULL_HANDLE) {
    vkDestroyFramebuffer(device, framebuffer, nullptr);
    framebuffer = VK_NULL_HANDLE;
  }
}

auto
engine::graphics::vulkan::SwapChain::Image::drop_2(VkDevice device, VkCommandPool present_command_pool) & noexcept
  -> void
{
  if (transfer_command_buffer != VK_NULL_HANDLE) {
    vkFreeCommandBuffers(device, present_command_pool, 1, &transfer_command_buffer);
    transfer_command_buffer = VK_NULL_HANDLE;
  }

  if (image_view != VK_NULL_HANDLE) {
    vkDestroyImageView(device, image_view, nullptr);
    image_view = VK_NULL_HANDLE;
  }

  image = VK_NULL_HANDLE;
}

auto
engine::graphics::vulkan::SwapChain::Image::drop_3(VkDevice device, VkCommandPool graphics_command_pool) & noexcept
  -> void
{
  if (command_buffer != VK_NULL_HANDLE) {
    vkFreeCommandBuffers(device, graphics_command_pool, 1, &command_buffer);
    command_buffer = VK_NULL_HANDLE;
  }
}

auto
engine::graphics::vulkan::SwapChain::Sync::drop(VkDevice device) & noexcept -> void
{
  if (fence != VK_NULL_HANDLE) {
    vkWaitForFences(device, 1, &fence, VK_TRUE, std::numeric_limits<uint64_t>::max());
    vkDestroyFence(device, fence, nullptr);
    fence = VK_NULL_HANDLE;
  }

  if (draw_complete_semaphore != VK_NULL_HANDLE) {
    vkDestroySemaphore(device, draw_complete_semaphore, nullptr);
    draw_complete_semaphore = VK_NULL_HANDLE;
  }

  if (image_acquired_semaphore != VK_NULL_HANDLE) {
    vkDestroySemaphore(device, image_acquired_semaphore, nullptr);
    image_acquired_semaphore = VK_NULL_HANDLE;
  }

  if (image_transfer_semaphore != VK_NULL_HANDLE) {
    vkDestroySemaphore(device, image_transfer_semaphore, nullptr);
    image_transfer_semaphore = VK_NULL_HANDLE;
  }
}

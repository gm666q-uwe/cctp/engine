#include "engine/graphics/vulkan/graphics_context.hpp"

#include <iostream>

engine::graphics::vulkan::GraphicsContext::GraphicsContext() noexcept
  : graphics::GraphicsContext()
  , m_current_command_buffer(std::make_shared<VkCommandBuffer>(VK_NULL_HANDLE))
{}

engine::graphics::vulkan::GraphicsContext::GraphicsContext(engine::graphics::vulkan::GraphicsContext&& other) noexcept
  : graphics::GraphicsContext(std::move(other))
  , m_camera(std::exchange(other.m_camera, nullptr))
  //, m_flags(std::exchange(other.m_flags, Flags::empty()))
  , m_instance(std::exchange(other.m_instance, VK_NULL_HANDLE))
  , m_resize_callback(std::exchange(other.m_resize_callback, nullptr))
{}

engine::graphics::vulkan::GraphicsContext::~GraphicsContext() noexcept
{
  drop();
}

auto
engine::graphics::vulkan::GraphicsContext::operator=(engine::graphics::vulkan::GraphicsContext&& other) & noexcept
  -> engine::graphics::vulkan::GraphicsContext&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_camera = std::exchange(other.m_camera, m_camera);
  // m_flags = std::exchange(other.m_flags, m_flags);
  m_instance = std::move(other.m_instance);
  m_resize_callback = std::exchange(other.m_resize_callback, m_resize_callback);

  graphics::GraphicsContext::operator=(std::move(other));
  return *this;
}

auto
engine::graphics::vulkan::GraphicsContext::begin_frame() & noexcept -> void
{
  m_swap_chain->acquire_image();
}

auto
engine::graphics::vulkan::GraphicsContext::begin_render_pass(bool submit_only) & noexcept -> void
{
  auto& image = m_swap_chain->current_image();
  if (!submit_only) {
    m_device->begin_command_buffer(image.command_buffer).unwrap();
  }
  *m_current_command_buffer = image.command_buffer;
  if (submit_only) {
    return;
  }
  image.is_command_buffer_valid = false;

  auto clear_values = std::array<VkClearValue, 2>{
    VkClearValue{
      .color = VkClearColorValue{ .float32 = { m_clear_color.r, m_clear_color.g, m_clear_color.b, m_clear_color.a } } },
    VkClearValue{ .depthStencil = VkClearDepthStencilValue{ 1.0f, 0 } }
  };
  auto render_pass_begin_info = VkRenderPassBeginInfo{ VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
						       nullptr,
						       m_swap_chain->render_pass(),
						       image.framebuffer,
						       VkRect2D{ VkOffset2D{ 0, 0 }, m_swap_chain->extent() },
						       clear_values.size(),
						       clear_values.data() };
  vkCmdBeginRenderPass(*m_current_command_buffer, &render_pass_begin_info, VK_SUBPASS_CONTENTS_INLINE);
}

auto
engine::graphics::vulkan::GraphicsContext::drop() & noexcept -> void
{
  m_camera = nullptr;
  // m_flags = Flags::empty();
  m_resize_callback = nullptr;

  m_swap_chain.reset();

  m_device.reset();

  if (m_surface_khr != VK_NULL_HANDLE) {
    vkDestroySurfaceKHR(m_instance->instance(), m_surface_khr, nullptr);
    m_surface_khr = VK_NULL_HANDLE;
  }

  m_instance.reset();
}

auto
engine::graphics::vulkan::GraphicsContext::create_camera() const& noexcept
  -> result::Result<engine::graphics::Camera, engine::Error>
{
  return std::move(result::Result<Camera, Error>::Err(Error::new_(0, -1)));
  /*if (auto result = create_uniform_buffer_pointer<Camera::Matrices>(); result.is_err()) {
    return std::move(result::Result<Camera, Error>::Err(std::move(result).unwrap_err()));
  } else {
    return std::move(result::Result<Camera, Error>::Ok(Camera::new_(std::move(result).unwrap())));
  }*/
}

auto
engine::graphics::vulkan::GraphicsContext::create_camera_pointer() const& noexcept
  -> result::Result<engine::graphics::Camera*, engine::Error>
{
  return std::move(result::Result<Camera*, Error>::Err(Error::new_(0, -1)));
  /*if (auto result = create_uniform_buffer_pointer<Camera::Matrices>(); result.is_err()) {
    return std::move(result::Result<Camera*, Error>::Err(std::move(result).unwrap_err()));
  } else {
    return std::move(result::Result<Camera*, Error>::Ok(Camera::new_pointer(std::move(result).unwrap())));
  }*/
}

auto
engine::graphics::vulkan::GraphicsContext::create_uniform_buffer_pointer(engine::isize size) const& noexcept
  -> result::Result<engine::graphics::UniformBuffer<void>*, engine::Error>
{
  return std::move(result::Result<UniformBuffer<void>*, Error>::Err(Error::new_(0, -1)));
  /*if (auto result = UniformBuffer<void>::new_pointer(m_gl, size); result.is_err()) {
    return std::move(result::Result<graphics::UniformBuffer<void>*, Error>::Err(std::move(result).unwrap_err()));
  } else {
    return std::move(result::Result<graphics::UniformBuffer<void>*, Error>::Ok(std::move(result).unwrap()));
  }*/
}

auto
engine::graphics::vulkan::GraphicsContext::current_command_buffer() const& noexcept -> VkCommandBuffer&
{
  return *m_current_command_buffer;
}

auto
engine::graphics::vulkan::GraphicsContext::device() const& noexcept -> engine::graphics::vulkan::Device&
{
  return *m_device;
}

auto
engine::graphics::vulkan::GraphicsContext::end_frame() & noexcept -> void
{
  m_swap_chain->present();
}

auto
engine::graphics::vulkan::GraphicsContext::end_render_pass(bool submit_only) & noexcept -> void
{
  if (!submit_only) {
    vkCmdEndRenderPass(*m_current_command_buffer);

    if (!m_device->graphics_is_present()) {
      auto const& image = m_swap_chain->current_image();
      auto image_memory_barrier =
	VkImageMemoryBarrier{ VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
			      nullptr,
			      0,
			      0,
			      VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
			      VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
			      m_device->graphics_queue_family_index(),
			      m_device->present_queue_family_index(),
			      image.image,
			      VkImageSubresourceRange{ VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 } };

      vkCmdPipelineBarrier(*m_current_command_buffer,
			   VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
			   VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
			   0,
			   0,
			   nullptr,
			   0,
			   nullptr,
			   1,
			   &image_memory_barrier);
    }
  }

  m_device->end_command_buffer(*m_current_command_buffer, submit_only, m_swap_chain->current_sync()).unwrap();
  if (!submit_only) {
    m_swap_chain->current_image().is_command_buffer_valid = true;
  }
}

auto
engine::graphics::vulkan::GraphicsContext::extent() const& noexcept -> VkExtent2D const&
{
  return m_swap_chain->extent();
}

auto
engine::graphics::vulkan::GraphicsContext::free_command_buffers() const& noexcept -> void
{
  vkDeviceWaitIdle(m_device->device());
  m_swap_chain->free_command_buffers(m_device->graphics_command_pool());
}

auto
engine::graphics::vulkan::GraphicsContext::initialize(CStr application_name,
						      uint32_t application_version,
						      std::function<VkResult(VkInstance, VkSurfaceKHR*)> create_surface,
						      engine::u16 height,
						      char const* surface_extension_name,
						      engine::u16 width) & noexcept -> option::Option<engine::Error>
{
  if (auto result = Instance::new_pointer(application_name, application_version, surface_extension_name);
      result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  } else {
    m_instance = std::unique_ptr<Instance>(std::move(result).unwrap());
  }

  if (create_surface(m_instance->instance(), &m_surface_khr) != VK_SUCCESS) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  if (auto result = m_instance->create_device_pointer(m_surface_khr); result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  } else {
    m_device = std::unique_ptr<Device>(std::move(result).unwrap());
  }

  if (auto result = m_device->create_swap_chain_pointer(VkExtent2D{ width, height }, m_surface_khr); result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  } else {
    m_swap_chain = std::unique_ptr<SwapChain>(std::move(result).unwrap());
  }

  return std::move(option::Option<Error>::None());
}

auto
engine::graphics::vulkan::GraphicsContext::is_current_command_buffer_valid() const& noexcept -> bool
{
  return m_swap_chain->current_image().is_command_buffer_valid;
}

auto
engine::graphics::vulkan::GraphicsContext::make_current() const& noexcept -> void
{}

auto
engine::graphics::vulkan::GraphicsContext::new_(CStr application_name,
						uint32_t application_version,
						std::function<VkResult(VkInstance, VkSurfaceKHR*)> create_surface,
						engine::u16 height,
						char const* surface_extension_name,
						engine::u16 width) noexcept -> result::Result<GraphicsContext, Error>
{
  auto self = GraphicsContext();
  if (auto result = self.initialize(application_name,
				    application_version,
				    std::forward<std::function<VkResult(VkInstance, VkSurfaceKHR*)>>(create_surface),
				    height,
				    surface_extension_name,
				    width);
      result.is_some()) {
    return std::move(result::Result<GraphicsContext, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<GraphicsContext, Error>::Ok(std::move(self)));
}

auto
engine::graphics::vulkan::GraphicsContext::new_pointer(
  CStr application_name,
  uint32_t application_version,
  std::function<VkResult(VkInstance, VkSurfaceKHR*)> create_surface,
  engine::u16 height,
  char const* surface_extension_name,
  engine::u16 width) noexcept -> result::Result<GraphicsContext*, Error>
{
  auto self = std::unique_ptr<GraphicsContext>(new GraphicsContext());
  if (auto result = self->initialize(application_name,
				     application_version,
				     std::forward<std::function<VkResult(VkInstance, VkSurfaceKHR*)>>(create_surface),
				     height,
				     surface_extension_name,
				     width);
      result.is_some()) {
    return std::move(result::Result<GraphicsContext*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<GraphicsContext*, Error>::Ok(self.release()));
}

auto
engine::graphics::vulkan::GraphicsContext::on_resize(engine::u16 height, engine::u16 width) & noexcept -> void
{
  // m_flags.insert(Flags::HAS_RESIZE());
  m_swap_chain->on_resize(height, width);
  if (m_resize_callback) {
    m_resize_callback(height, width);
  }
}

auto
engine::graphics::vulkan::GraphicsContext::render_pass() const& noexcept -> VkRenderPass
{
  return m_swap_chain->render_pass();
}

auto
engine::graphics::vulkan::GraphicsContext::set_camera(engine::graphics::Camera* camera) & noexcept -> void
{
  m_camera = camera;
  if (m_camera != nullptr) {
    auto const& extent = m_swap_chain->extent();
    m_camera->on_resize(extent.height, extent.width);
  }
}

auto
engine::graphics::vulkan::GraphicsContext::set_clear_color(glm::vec4 const& color) & noexcept -> void
{
  m_clear_color = color;
}

auto
engine::graphics::vulkan::GraphicsContext::set_resize_callback(std::function<void(u16, u16)> resize_callback) & noexcept
  -> void
{
  m_resize_callback = resize_callback;
  if (m_resize_callback) {
    m_resize_callback(m_swap_chain->extent().height, m_swap_chain->extent().width);
  }
}

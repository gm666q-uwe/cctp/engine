#pragma once

#include <span>

#include "engine/graphics/vulkan/vulkan.h"

namespace engine::graphics::vulkan {
auto
select_format(std::span<const VkFormat> formats,
	      VkFormatProperties format_properties,
	      VkPhysicalDevice physical_device) noexcept -> VkFormat;

auto
select_memory_type(VkPhysicalDevice physical_device,
		   VkMemoryPropertyFlags property_flags,
		   uint32_t type_bits,
		   uint32_t* type_index) noexcept -> bool;
} // namespace engine::graphics::vulkan

#include "utility.hpp"

auto
engine::graphics::vulkan::select_format(std::span<const VkFormat> formats,
					VkFormatProperties format_properties,
					VkPhysicalDevice physical_device) noexcept -> VkFormat
{
  auto fp = VkFormatProperties{};
  for (auto format : formats) {
    vkGetPhysicalDeviceFormatProperties(physical_device, format, &fp);
    if ((fp.bufferFeatures & format_properties.bufferFeatures) == format_properties.bufferFeatures &&
	(fp.linearTilingFeatures & format_properties.linearTilingFeatures) == format_properties.linearTilingFeatures &&
	(fp.optimalTilingFeatures & format_properties.optimalTilingFeatures) ==
	  format_properties.optimalTilingFeatures) {
      return format;
    }
  }

  return VK_FORMAT_UNDEFINED;
}

auto
engine::graphics::vulkan::select_memory_type(VkPhysicalDevice physical_device,
					     VkMemoryPropertyFlags property_flags,
					     uint32_t type_bits,
					     uint32_t* type_index) noexcept -> bool
{
  auto physical_device_memory_properties = VkPhysicalDeviceMemoryProperties{};
  vkGetPhysicalDeviceMemoryProperties(physical_device, &physical_device_memory_properties);

  for (auto i = 0; i < physical_device_memory_properties.memoryTypeCount; i++) {
    if ((type_bits & (1 << i)) == (1 << i) &&
	(physical_device_memory_properties.memoryTypes[i].propertyFlags & property_flags) == property_flags) {
      *type_index = i;
      return true;
    }
  }

  return false;
}

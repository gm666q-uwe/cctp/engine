#include "engine/graphics/vulkan/instance.hpp"

#include <iostream>
#include <memory>

engine::graphics::vulkan::Instance::Instance(engine::graphics::vulkan::Instance&& other) noexcept
  : m_data_ext_debug_utils(std::move(other.m_data_ext_debug_utils))
  , m_debug_utils_messenger_ext(std::exchange(other.m_debug_utils_messenger_ext, VK_NULL_HANDLE))
  , m_instance(std::exchange(other.m_instance, VK_NULL_HANDLE))
{}

engine::graphics::vulkan::Instance::~Instance() noexcept
{
  drop();
}

auto
engine::graphics::vulkan::Instance::operator=(engine::graphics::vulkan::Instance&& other) & noexcept
  -> engine::graphics::vulkan::Instance&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_data_ext_debug_utils = std::move(other.m_data_ext_debug_utils);
  m_debug_utils_messenger_ext = std::exchange(other.m_debug_utils_messenger_ext, m_debug_utils_messenger_ext);
  m_instance = std::exchange(other.m_instance, m_instance);

  return *this;
}

auto
engine::graphics::vulkan::Instance::create_device(VkSurfaceKHR surface_khr) const& noexcept
  -> result::Result<Device, Error>
{
  return std::move(Device::new_(m_instance, surface_khr));
}

auto
engine::graphics::vulkan::Instance::create_device_pointer(VkSurfaceKHR surface_khr) const& noexcept
  -> result::Result<Device*, Error>
{
  return std::move(Device::new_pointer(m_instance, surface_khr));
}

auto
engine::graphics::vulkan::Instance::drop() & noexcept -> void
{
  if (m_debug_utils_messenger_ext != VK_NULL_HANDLE) {
    vkDestroyDebugUtilsMessengerEXT(m_instance, m_debug_utils_messenger_ext, nullptr);
    m_debug_utils_messenger_ext = VK_NULL_HANDLE;
  }

  if (m_instance != VK_NULL_HANDLE) {
    vkDestroyInstance(m_instance, nullptr);
    m_instance = VK_NULL_HANDLE;
  }
}

auto
engine::graphics::vulkan::Instance::initialize(engine::CStr application_name,
					       uint32_t application_version,
					       char const* surface_extension_name) & noexcept -> option::Option<Error>
{
  auto application_info =
    VkApplicationInfo{ VK_STRUCTURE_TYPE_APPLICATION_INFO,
		       nullptr,
		       application_name.data(),
		       application_version,
		       "space.gm666q.engine",
		       VK_MAKE_VERSION(ENGINE_VERSION_MAJOR, ENGINE_VERSION_MINOR, ENGINE_VERSION_PATCH),
		       VK_API_VERSION_1_2 };

  static auto const debug_utils_messenger_create_info_ext = VkDebugUtilsMessengerCreateInfoEXT{
    VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
    nullptr,
    0,
    VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
      VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
    VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
      VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
    vk_debug_utils_messenger_callback_ext,
    this
  };

  auto instance_extensions = Vec<char const*>{ VK_KHR_SURFACE_EXTENSION_NAME, surface_extension_name };
  auto instance_layers = Vec<char const*>();

  if (config::is_debug) {
    instance_extensions.emplace_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    instance_layers.emplace_back("VK_LAYER_KHRONOS_validation");
  }

  auto instance_create_info = VkInstanceCreateInfo{ VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
						    config::is_debug ? &debug_utils_messenger_create_info_ext : nullptr,
						    0,
						    &application_info,
						    static_cast<uint32_t>(instance_layers.size()),
						    instance_layers.data(),
						    static_cast<uint32_t>(instance_extensions.size()),
						    instance_extensions.data() };

  if (vkCreateInstance(&instance_create_info, nullptr, &m_instance) != VK_SUCCESS) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  if (config::is_debug) {
    if (vkCreateDebugUtilsMessengerEXT(
	  m_instance, &debug_utils_messenger_create_info_ext, nullptr, &m_debug_utils_messenger_ext) != VK_SUCCESS) {
      return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
    }
  }

  return option::Option<Error>::None();
}

auto
engine::graphics::vulkan::Instance::load() & noexcept -> void
{
  load_ext_debug_utils();
}

auto
engine::graphics::vulkan::Instance::new_(engine::CStr application_name,
					 uint32_t application_version,
					 char const* surface_extension_name) noexcept -> result::Result<Instance, Error>
{
  auto self = Instance();
  if (auto result = self.initialize(application_name, application_version, surface_extension_name); result.is_some()) {
    return std::move(result::Result<Instance, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Instance, Error>::Ok(std::move(self)));
}

auto
engine::graphics::vulkan::Instance::new_pointer(engine::CStr application_name,
						uint32_t application_version,
						char const* surface_extension_name) noexcept
  -> result::Result<Instance*, Error>
{
  auto self = std::unique_ptr<Instance>(new Instance());
  if (auto result = self->initialize(application_name, application_version, surface_extension_name); result.is_some()) {
    return std::move(result::Result<Instance*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Instance*, Error>::Ok(self.release()));
}

auto
engine::graphics::vulkan::Instance::vk_debug_utils_messenger_callback_ext(
  VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
  VkDebugUtilsMessageTypeFlagsEXT messageTypes,
  VkDebugUtilsMessengerCallbackDataEXT const* pCallbackData) & noexcept -> VkBool32
{
  switch (messageSeverity) {
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
      std::cerr << "[VERBOSE]";
      break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
      std::cerr << "[INFO]";
      break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
      std::cerr << "[WARNING]";
      break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
      std::cerr << "[ERROR]";
      break;
    default:
      break;
  }
  switch (messageTypes) {
    case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT:
      std::cerr << "[GENERAL]";
      break;
    case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT:
      std::cerr << "[VALIDATION]";
      break;
    case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT:
      std::cerr << "[PERFORMANCE]";
      break;
    default:
      break;
  }
  std::cerr << ": " << pCallbackData->pMessage << std::endl;
  return VK_FALSE;
}

auto
engine::graphics::vulkan::Instance::vk_debug_utils_messenger_callback_ext(
  VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
  VkDebugUtilsMessageTypeFlagsEXT messageTypes,
  VkDebugUtilsMessengerCallbackDataEXT const* pCallbackData,
  void* pUserData) noexcept -> VkBool32
{
  return static_cast<Instance*>(pUserData)->vk_debug_utils_messenger_callback_ext(
    messageSeverity, messageTypes, pCallbackData);
}

cmake_minimum_required(VERSION 3.19)

add_subdirectory(common)
add_subdirectory(core)
add_subdirectory(gl)
add_subdirectory(vulkan)

#pragma once

#include "engine/graphics/gl/egl/egl.h"

namespace engine::graphics::gl::egl {
enum class Platform : EGLenum
{
  Wayland = EGL_PLATFORM_WAYLAND_KHR,
  Xlib = EGL_PLATFORM_X11_KHR,
  XCB = 0x31DC
};
}

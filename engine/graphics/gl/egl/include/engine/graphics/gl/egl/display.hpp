#pragma once

#include "engine/graphics/gl/egl/context.hpp"
#include "engine/graphics/gl/egl/platform.hpp"

namespace engine::graphics::gl::egl {
class Display final
{
public:
  constexpr Display(Display const&) noexcept = delete;

  Display(Display&& other) noexcept;

  virtual ~Display() noexcept;

  constexpr auto operator=(Display const&) & noexcept -> Display& = delete;

  auto operator=(Display&& other) & noexcept -> Display&;

  [[nodiscard]] auto choose_config() const& noexcept -> result::Result<EGLConfig, Error>;

  auto create_context(EGLConfig config, void* native_window, bool pre_load) const& noexcept
    -> result::Result<Context, Error>;

  auto create_context_pointer(EGLConfig config, void* native_window, bool pre_load) const& noexcept
    -> result::Result<Context*, Error>;

  auto get_config_attribute(EGLConfig config, EGLint attribute) const& noexcept -> result::Result<EGLint, Error>;

  static auto new_(void* native_display, Platform platform, bool pre_load) noexcept -> result::Result<Display, Error>;

  static auto new_pointer(void* native_display, Platform platform, bool pre_load) noexcept
    -> result::Result<Display*, Error>;

protected:
private:
  std::shared_ptr<void> m_display = std::shared_ptr<void>();
  std::unique_ptr<Context> m_context = std::unique_ptr<Context>();

  constexpr Display() noexcept = default;

  auto drop() & noexcept -> void;

  static auto check_client_extensions(std::string_view extensions, Platform platform) noexcept -> u8;

  static auto check_display_extensions(std::string_view extensions) noexcept -> bool;

  [[nodiscard]] auto create_window_surface(EGLConfig config, void* native_window) const& noexcept
    -> result::Result<EGLSurface, Error>;

  static auto debug_callback(EGLenum error,
			     char const* command,
			     EGLint messageType,
			     EGLLabelKHR threadLabel,
			     EGLLabelKHR objectLabel,
			     char const* message) noexcept -> void;

  auto initialize(void* native_display, Platform platform, bool pre_load) & noexcept -> option::Option<Error>;
};
} // namespace engine::graphics::gl::egl

#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/egl/egl.h"

namespace engine::graphics::gl::egl {
struct ContextDataKHRDebug final : public IContextData
{
public:
  // EGL_KHR_debug
  PFNEGLDEBUGMESSAGECONTROLKHRPROC eglDebugMessageControlKHR = nullptr;
  PFNEGLQUERYDEBUGKHRPROC eglQueryDebugKHR = nullptr;
  PFNEGLLABELOBJECTKHRPROC eglLabelObjectKHR = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return eglDebugMessageControlKHR != nullptr && eglQueryDebugKHR != nullptr && eglLabelObjectKHR != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl::egl

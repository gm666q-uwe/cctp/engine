#pragma once

#include <functional>
#include <memory>

#include <engine/error.hpp>
#include <engine/graphics/gl/context.hpp>
#include <engine/graphics/gl/interface_context.hpp>

#include "engine/graphics/gl/egl/context_data_khr_debug.hpp"

namespace engine::graphics::gl::egl {
class Context final : public InterfaceContext
{
  friend class Display;

public:
  constexpr Context(Context const&) noexcept = delete;

  Context(Context&& other) noexcept;

  ~Context() noexcept override;

  constexpr auto operator=(Context const&) & noexcept -> Context& = delete;

  auto operator=(Context&& other) & noexcept -> Context&;

  // EGL_KHR_debug
  auto debugMessageControlKHR(EGLDEBUGPROCKHR callback, EGLAttrib const* attrib_list) & noexcept -> EGLint;

  auto queryDebugKHR(EGLint attribute, EGLAttrib* value) & noexcept -> EGLBoolean;

  auto labelObjectKHR(EGLDisplay display, EGLenum objectType, EGLObjectKHR object, EGLLabelKHR label) & noexcept
    -> EGLint;

  [[nodiscard]] auto get_proc_address(std::string_view name) const& noexcept
    -> result::Result<void (*)(void), Error> override;

  [[nodiscard]] auto gl() const& noexcept -> gl::Context& override;

  [[nodiscard]] auto is_current() const& noexcept -> bool override;

  [[nodiscard]] constexpr auto is_loaded(bool include_gl) const& noexcept -> bool override
  {
    return (is_khr_debug_loaded()) && (!include_gl || m_gl->is_loaded());
  }

  [[nodiscard]] constexpr auto is_khr_debug_loaded() const& noexcept -> bool { return m_data_khr_debug.is_loaded(); }

  auto load(bool include_gl) & noexcept -> void override;

  auto load_khr_debug() & noexcept -> void;

  [[nodiscard]] auto make_current() const& noexcept -> result::Result<std::nullptr_t, Error> override;

  [[nodiscard]] auto release_current() const& noexcept -> result::Result<std::nullptr_t, Error> override;

  [[nodiscard]] auto swap_buffers() const& noexcept -> result::Result<std::nullptr_t, Error> override;

  [[nodiscard]]auto swap_interval(i32 interval) const& noexcept -> result::Result<std::nullptr_t, Error> override;

protected:
private:
  std::unique_ptr<void, std::function<void(EGLContext)>> m_context =
    std::unique_ptr<void, std::function<void(EGLContext)>>();
  std::shared_ptr<void> m_display = std::shared_ptr<void>();
  std::shared_ptr<gl::Context> m_gl = std::shared_ptr<gl::Context>();
  std::unique_ptr<void, std::function<void(EGLSurface)>> m_surface =
    std::unique_ptr<void, std::function<void(EGLSurface)>>();

  // EGL_KHR_debug
  ContextDataKHRDebug m_data_khr_debug = {};

  Context(std::shared_ptr<void> display, bool pre_load, EGLSurface surface) noexcept;

  Context(Context const& context, std::shared_ptr<void> display, bool pre_load, EGLSurface surface) noexcept;

  auto drop() & noexcept -> void;

  auto initialize(EGLConfig config, EGLContext context) & noexcept -> option::Option<Error>;

  static auto new_(std::shared_ptr<void> display, bool pre_load) noexcept -> result::Result<Context, Error>;

  static auto new_pointer(std::shared_ptr<void> display, bool pre_load) noexcept -> result::Result<Context*, Error>;

  static auto with_surface(EGLConfig config,
			   Context const& context,
			   std::shared_ptr<void> display,
			   bool pre_load,
			   EGLSurface surface) noexcept -> result::Result<Context, Error>;

  static auto with_surface_pointer(EGLConfig config,
				   Context const& context,
				   std::shared_ptr<void> display,
				   bool pre_load,
				   EGLSurface surface) noexcept -> result::Result<Context*, Error>;
};
} // namespace engine::graphics::gl::egl

#pragma once

#include <engine/types.h>

namespace engine::graphics::gl::egl {
enum class Error : i8
{
  MissingExtensions = -3,
  NoConfig = -6,
  NoDisplay = -4,
  NoSurface = -7,
  NotInitialized = -5,
  Unknown = -1,
  VersionTooOld = -2,
};
}

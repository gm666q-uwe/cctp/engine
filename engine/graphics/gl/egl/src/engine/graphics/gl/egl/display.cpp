#include "engine/graphics/gl/egl/display.hpp"

#include <array>
#include <iostream>
#include <string_view>

#include "engine/graphics/gl/egl/error.hpp"

engine::graphics::gl::egl::Display::Display(engine::graphics::gl::egl::Display&& other) noexcept
  : m_context(std::move(other.m_context))
  , m_display(std::move(other.m_display))
{}

engine::graphics::gl::egl::Display::~Display() noexcept
{
  drop();
}

auto
engine::graphics::gl::egl::Display::operator=(engine::graphics::gl::egl::Display&& other) & noexcept
  -> engine::graphics::gl::egl::Display&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_context = std::move(other.m_context);
  m_display = std::move(other.m_display);

  return *this;
}

auto
engine::graphics::gl::egl::Display::debug_callback(EGLenum error,
						   const char* command,
						   EGLint messageType,
						   EGLLabelKHR threadLabel,
						   EGLLabelKHR objectLabel,
						   const char* message) noexcept -> void
{
  switch (messageType) {
    case EGL_DEBUG_MSG_CRITICAL_KHR:
      std::cerr << "[CRITICAL][";
      break;
    case EGL_DEBUG_MSG_ERROR_KHR:
      std::cerr << "[ERROR][";
      break;
    case EGL_DEBUG_MSG_WARN_KHR:
      std::cerr << "[WARN][";
      break;
    case EGL_DEBUG_MSG_INFO_KHR:
      std::cerr << "[INFO][";
      break;
    default:
      break;
  }
  /*if (error != EGL_SUCCESS) {
    std::cerr << egl_error_string(static_cast<EGLint>(error)) << "][";
  }*/
  std::cerr << command << "]: " << message << std::endl;
}

auto
engine::graphics::gl::egl::Display::drop() & noexcept -> void
{}

auto
engine::graphics::gl::egl::Display::check_client_extensions(std::string_view extensions,
							    engine::graphics::gl::egl::Platform platform) noexcept -> u8
{
  using namespace std::literals;

  auto egl_ext_client_extensions = extensions.find("EGL_EXT_client_extensions"sv) != std::string_view::npos;
  auto egl_ext_platform_wayland = extensions.find("EGL_EXT_platform_wayland"sv) != std::string_view::npos;
  auto egl_ext_platform_x11 = extensions.find("EGL_EXT_platform_x11"sv) != std::string_view::npos;
  auto egl_ext_platform_xcb = extensions.find("EGL_EXT_platform_xcb"sv) != std::string_view::npos;
  auto egl_khr_client_get_all_proc_addresses =
    extensions.find("EGL_KHR_client_get_all_proc_addresses"sv) != std::string_view::npos;
  auto egl_khr_debug = (extensions.find("EGL_KHR_debug"sv) != std::string_view::npos);
  auto egl_khr_platform_wayland = extensions.find("EGL_KHR_platform_wayland"sv) != std::string_view::npos;
  auto egl_khr_platform_x11 = extensions.find("EGL_KHR_platform_x11"sv) != std::string_view::npos;
  auto egl_mesa_platform_xcb = extensions.find("EGL_MESA_platform_xcb"sv) != std::string_view::npos;

#pragma clang diagnostic push
#pragma ide diagnostic ignored "Simplify"
  auto egl_debug = egl_khr_debug && config::is_debug;
#pragma clang diagnostic pop
  auto egl_platform = false;
  switch (platform) {
    case Platform::Wayland:
      egl_platform = egl_ext_platform_wayland || egl_khr_platform_wayland;
      break;
    case Platform::Xlib:
      egl_platform = egl_ext_platform_x11 || egl_khr_platform_x11;
      break;
    case Platform::XCB:
      egl_platform = egl_ext_platform_xcb || egl_mesa_platform_xcb;
      break;
  }

  if (!egl_ext_client_extensions || !egl_khr_client_get_all_proc_addresses || !egl_platform) {
    return 0;
  }

  return egl_debug ? 2 : 1;
}

auto
engine::graphics::gl::egl::Display::check_display_extensions(std::string_view extensions) noexcept -> bool
{
  using namespace std::literals;

  auto egl_khr_get_all_proc_addresses = extensions.find("EGL_KHR_get_all_proc_addresses"sv) != std::string_view::npos;
  auto egl_khr_gl_colorspace = extensions.find("EGL_KHR_gl_colorspace"sv) != std::string_view::npos;

  return egl_khr_get_all_proc_addresses && egl_khr_gl_colorspace;
}

auto
engine::graphics::gl::egl::Display::choose_config() const& noexcept -> result::Result<EGLConfig, engine::Error>
{
  auto attributes = std::array<EGLint, 27>{ EGL_ALPHA_SIZE,
					    8,
					    /*EGL_BIND_TO_TEXTURE_RGBA,
					    EGL_TRUE,*/
					    EGL_BLUE_SIZE,
					    8,
					    EGL_BUFFER_SIZE,
					    32,
					    EGL_COLOR_BUFFER_TYPE,
					    EGL_RGB_BUFFER,
					    EGL_CONFIG_CAVEAT,
					    EGL_NONE,
					    EGL_CONFORMANT,
					    EGL_OPENGL_BIT,
					    EGL_DEPTH_SIZE,
					    24,
					    EGL_GREEN_SIZE,
					    8,
					    EGL_LEVEL,
					    0,
					    EGL_RED_SIZE,
					    8,
					    EGL_RENDERABLE_TYPE,
					    EGL_OPENGL_BIT,
					    EGL_STENCIL_SIZE,
					    8,
					    EGL_SURFACE_TYPE,
					    EGL_WINDOW_BIT,
					    /*EGL_TRANSPARENT_TYPE,
					    EGL_TRANSPARENT_RGB,
					    EGL_TRANSPARENT_RED_VALUE,
					    8,
					    EGL_TRANSPARENT_GREEN_VALUE,
					    8,
					    EGL_TRANSPARENT_BLUE_VALUE,
					    8,*/
					    EGL_NONE };

  auto config = static_cast<EGLConfig>(EGL_NO_CONFIG_KHR);
  auto config_count = static_cast<EGLint>(0);
  if (eglChooseConfig(m_display.get(), attributes.data(), &config, 1, &config_count) != EGL_TRUE || config_count != 1) {
    return std::move(
      result::Result<EGLConfig, engine::Error>::Err(engine::Error::new_(0, static_cast<i8>(Error::NoConfig))));
  } else {
    return std::move(result::Result<EGLConfig, engine::Error>::Ok(config));
  }
}

auto
engine::graphics::gl::egl::Display::create_context(EGLConfig config, void* native_window, bool pre_load) const& noexcept
  -> result::Result<engine::graphics::gl::egl::Context, engine::Error>
{
  if (auto result = create_window_surface(config, native_window); result.is_err()) {
    return std::move(result::Result<Context, engine::Error>::Err(std::move(result).unwrap_err()));
  } else {
    return std::move(Context::with_surface(config, *m_context, m_display, pre_load, std::move(result).unwrap()));
  }
}

auto
engine::graphics::gl::egl::Display::create_context_pointer(EGLConfig config,
							   void* native_window,
							   bool pre_load) const& noexcept
  -> result::Result<engine::graphics::gl::egl::Context*, engine::Error>
{
  if (auto result = create_window_surface(config, native_window); result.is_err()) {
    return std::move(result::Result<Context*, engine::Error>::Err(std::move(result).unwrap_err()));
  } else {
    return std::move(
      Context::with_surface_pointer(config, *m_context, m_display, pre_load, std::move(result).unwrap()));
  }
}

auto
engine::graphics::gl::egl::Display::create_window_surface(EGLConfig config, void* native_window) const& noexcept
  -> result::Result<EGLSurface, engine::Error>
{
  auto attributes = std::array<EGLAttrib, 3>{ EGL_GL_COLORSPACE_KHR, EGL_GL_COLORSPACE_SRGB_KHR, EGL_NONE };
  if (auto* surface = eglCreatePlatformWindowSurface(m_display.get(), config, native_window, attributes.data());
      surface == nullptr) {
    return std::move(
      result::Result<EGLSurface, engine::Error>::Err(engine::Error::new_(0, static_cast<i8>(Error::NoSurface))));
  } else {
    return std::move(result::Result<EGLSurface, engine::Error>::Ok(surface));
  }
}

auto
engine::graphics::gl::egl::Display::get_config_attribute(EGLConfig config, EGLint attribute) const& noexcept
  -> result::Result<EGLint, engine::Error>
{
  auto value = static_cast<EGLint>(0);
  if (auto result = eglGetConfigAttrib(m_display.get(), config, attribute, &value); result != EGL_TRUE) {
    return std::move(
      result::Result<EGLint, engine::Error>::Err(engine::Error::new_(0, static_cast<i8>(Error::Unknown))));
  } else {
    return std::move(result::Result<EGLint, engine::Error>::Ok(value));
  }
}

auto
engine::graphics::gl::egl::Display::initialize(void* native_display, Platform platform, bool pre_load) & noexcept
  -> option::Option<engine::Error>
{
  auto const* extensions = eglQueryString(EGL_NO_DISPLAY, EGL_EXTENSIONS);
  if (extensions == nullptr) {
    if (eglGetError() == EGL_BAD_DISPLAY) {
      return std::move(
	option::Option<engine::Error>::Some(engine::Error::new_(0, static_cast<i8>(Error::VersionTooOld))));
    } else {
      return std::move(option::Option<engine::Error>::Some(engine::Error::new_(0, static_cast<i8>(Error::Unknown))));
    }
  }

  if (auto result = check_client_extensions(std::string_view(extensions), platform); result == 0) {
    return std::move(
      option::Option<engine::Error>::Some(engine::Error::new_(0, static_cast<i8>(Error::MissingExtensions))));
  } else if (result == 2) {
    auto* eglDebugMessageControlKHR =
      reinterpret_cast<PFNEGLDEBUGMESSAGECONTROLKHRPROC>(eglGetProcAddress("eglDebugMessageControlKHR"));
    constexpr auto attrib_list =
      std::array<EGLAttrib, 5>{ EGL_DEBUG_MSG_WARN_KHR, EGL_TRUE, EGL_DEBUG_MSG_INFO_KHR, EGL_TRUE, EGL_NONE };
    eglDebugMessageControlKHR(debug_callback, attrib_list.data());
  }

  eglBindAPI(EGL_OPENGL_API);

  m_display = std::shared_ptr<void>(eglGetPlatformDisplay(static_cast<EGLenum>(platform), native_display, nullptr),
				    [](auto* display) { eglTerminate(display); });
  if (!m_display) {
    return std::move(option::Option<engine::Error>::Some(engine::Error::new_(0, static_cast<i8>(Error::NoDisplay))));
  }

  auto minor = static_cast<EGLint>(0);
  auto major = static_cast<EGLint>(0);
  if (eglInitialize(m_display.get(), &major, &minor) != EGL_TRUE) {
    return std::move(
      option::Option<engine::Error>::Some(engine::Error::new_(0, static_cast<i8>(Error::NotInitialized))));
  }

  extensions = eglQueryString(m_display.get(), EGL_EXTENSIONS);
  if (extensions == nullptr) {
    return std::move(option::Option<engine::Error>::Some(engine::Error::new_(0, static_cast<i8>(Error::Unknown))));
  }
  if (!check_display_extensions(std::string_view(extensions))) {
    return std::move(
      option::Option<engine::Error>::Some(engine::Error::new_(0, static_cast<i8>(Error::MissingExtensions))));
  }

  std::cout << "[engine::graphics::gl::egl][Display][initialize]: EGL " << major << '.' << minor << " initialized."
	    << std::endl;

  if (auto result = Context::new_pointer(m_display, pre_load); result.is_err()) {
    return std::move(option::Option<engine::Error>::Some(std::move(result).unwrap_err()));
  } else {
    m_context = std::unique_ptr<Context>(std::move(result).unwrap());
  }

  if (pre_load) {
    std::cout << "[engine::graphics::gl::egl][Display][initialize]: Pre loading functions: "
	      << (m_context->is_loaded(true) ? "success" : "failed") << '.' << std::endl;
  }

  return option::Option<engine::Error>::None();
}

auto
engine::graphics::gl::egl::Display::new_(void* native_display, Platform platform, bool pre_load) noexcept
  -> result::Result<Display, engine::Error>
{
  auto self = Display();
  if (auto result = self.initialize(native_display, platform, pre_load); result.is_some()) {
    return std::move(result::Result<Display, engine::Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Display, engine::Error>::Ok(std::move(self)));
}

auto
engine::graphics::gl::egl::Display::new_pointer(void* native_display, Platform platform, bool pre_load) noexcept
  -> result::Result<Display*, engine::Error>
{
  auto self = std::unique_ptr<Display>(new Display());
  if (auto result = self->initialize(native_display, platform, pre_load); result.is_some()) {
    return std::move(result::Result<Display*, engine::Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Display*, engine::Error>::Ok(self.release()));
}

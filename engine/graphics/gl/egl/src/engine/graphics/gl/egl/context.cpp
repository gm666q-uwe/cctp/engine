#include "engine/graphics/gl/egl/context.hpp"

#include <utility>

#include "engine/graphics/gl/egl/error.hpp"

engine::graphics::gl::egl::Context::Context(std::shared_ptr<void> display, bool pre_load, EGLSurface surface) noexcept
  : InterfaceContext()
  , m_display(display)
  , m_gl(std::shared_ptr<gl::Context>(gl::Context::new_pointer(this, pre_load)))
  , m_surface(surface, [=](auto* s) {
    if (s != nullptr) {
      eglDestroySurface(display.get(), s);
    }
  })
{
  if (pre_load) {
    load(false);
  }
}

engine::graphics::gl::egl::Context::Context(engine::graphics::gl::egl::Context const& context,
					    std::shared_ptr<void> display,
					    bool pre_load,
					    EGLSurface surface) noexcept
  : InterfaceContext()
  , m_display(display)
  , m_gl(context.m_gl)
  , m_surface(surface, [=](auto* s) {
    if (s != nullptr) {
      eglDestroySurface(display.get(), s);
    }
  })
{
  if (pre_load) {
    load(true);
  }
}

engine::graphics::gl::egl::Context::Context(engine::graphics::gl::egl::Context&& other) noexcept
  : InterfaceContext(std::move(other))
  , m_context(std::move(other.m_context))
  , m_data_khr_debug(std::move(other.m_data_khr_debug))
  , m_display(std::move(other.m_display))
  , m_gl(std::move(other.m_gl))
  , m_surface(std::move(other.m_surface))
{
  m_gl->set_interface_context(&other, this);
}

engine::graphics::gl::egl::Context::~Context() noexcept
{
  drop();
}

auto
engine::graphics::gl::egl::Context::operator=(engine::graphics::gl::egl::Context&& other) & noexcept
  -> engine::graphics::gl::egl::Context&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_context = std::move(other.m_context);
  m_data_khr_debug = std::move(other.m_data_khr_debug);
  m_display = std::move(other.m_display);
  m_gl = std::move(other.m_gl);
  m_surface = std::move(other.m_surface);

  m_gl->set_interface_context(&other, this);

  InterfaceContext::operator=(std::move(other));
  return *this;
}

auto
engine::graphics::gl::egl::Context::drop() & noexcept -> void
{
  /*if (is_current()) {
    release_current().unwrap();
  }
  if (m_surface != EGL_NO_SURFACE) {
    eglDestroySurface(m_display, m_surface);
    m_surface = EGL_NO_SURFACE;
  }
  if (m_context != EGL_NO_CONTEXT) {
    eglDestroyContext(m_display, m_context);
    m_context = EGL_NO_CONTEXT;
  }*/
}

auto
engine::graphics::gl::egl::Context::get_proc_address(std::string_view name) const& noexcept
  -> result::Result<void (*)(void), engine::Error>
{
  if (auto result = eglGetProcAddress(name.data()); result == nullptr) {
    return std::move(
      result::Result<void (*)(void), engine::Error>::Err(engine::Error::new_(0, static_cast<i8>(Error::Unknown))));
  } else {
    return std::move(result::Result<void (*)(void), engine::Error>::Ok(result));
  }
}

auto
engine::graphics::gl::egl::Context::gl() const& noexcept -> gl::Context&
{
  return *m_gl;
}

auto
engine::graphics::gl::egl::Context::initialize(EGLConfig config, EGLContext context) & noexcept
  -> option::Option<engine::Error>
{
  auto attributes = std::array<EGLint, 15>{ EGL_CONTEXT_MAJOR_VERSION,
					    4,
					    EGL_CONTEXT_MINOR_VERSION,
					    6,
					    EGL_CONTEXT_OPENGL_PROFILE_MASK,
					    EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT,
					    EGL_CONTEXT_OPENGL_DEBUG,
					    config::is_debug ? EGL_TRUE : EGL_FALSE,
					    EGL_CONTEXT_OPENGL_FORWARD_COMPATIBLE,
					    EGL_TRUE,
					    EGL_CONTEXT_OPENGL_ROBUST_ACCESS,
					    EGL_FALSE,
					    EGL_CONTEXT_OPENGL_RESET_NOTIFICATION_STRATEGY,
					    EGL_NO_RESET_NOTIFICATION,
					    EGL_NONE };

  auto display = m_display.get();
  m_context = std::unique_ptr<void, std::function<void(EGLContext)>>(
    eglCreateContext(m_display.get(), config, context, attributes.data()),
    [=](auto* context) { eglDestroyContext(display, context); });
  if (!m_context) {
    return std::move(option::Option<engine::Error>::Some(engine::Error::new_(0, static_cast<i8>(Error::Unknown))));
  }

  return std::move(option::Option<engine::Error>::None());
}

auto
engine::graphics::gl::egl::Context::is_current() const& noexcept -> bool
{
  return m_context && eglGetCurrentContext() == m_context.get();
}

auto
engine::graphics::gl::egl::Context::load(bool include_gl) & noexcept -> void
{
  load_khr_debug();
  if (include_gl) {
    m_gl->load();
  }
}

auto
engine::graphics::gl::egl::Context::make_current() const& noexcept -> result::Result<std::nullptr_t, engine::Error>
{
  if (!m_surface) {
    return std::move(
      result::Result<std::nullptr_t, engine::Error>::Err(engine::Error::new_(0, static_cast<i8>(Error::NoSurface))));
  }
  if (eglMakeCurrent(m_display.get(), m_surface.get(), m_surface.get(), m_context.get()) == EGL_TRUE) {
    return std::move(result::Result<std::nullptr_t, engine::Error>::Ok(nullptr));
  } else {
    return std::move(
      result::Result<std::nullptr_t, engine::Error>::Err(engine::Error::new_(0, static_cast<i8>(Error::Unknown))));
  }
}

auto
engine::graphics::gl::egl::Context::new_(std::shared_ptr<void> display, bool pre_load) noexcept
  -> result::Result<Context, engine::Error>
{
  auto config = static_cast<EGLConfig>(nullptr);
  auto config_count = static_cast<EGLint>(0);
  if (eglChooseConfig(display.get(), nullptr, &config, 1, &config_count) != EGL_TRUE || config_count != 1) {
    return std::move(
      result::Result<Context, engine::Error>::Err(engine::Error::new_(0, static_cast<i8>(Error::NoConfig))));
  }
  auto self = Context(std::forward<std::shared_ptr<void>>(display),
		      std::forward<bool>(pre_load),
		      std::forward<EGLSurface>(EGL_NO_SURFACE));
  if (auto result = self.initialize(config, EGL_NO_CONTEXT); result.is_some()) {
    return std::move(result::Result<Context, engine::Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Context, engine::Error>::Ok(std::move(self)));
}

auto
engine::graphics::gl::egl::Context::new_pointer(std::shared_ptr<void> display, bool pre_load) noexcept
  -> result::Result<Context*, engine::Error>
{
  auto config = static_cast<EGLConfig>(nullptr);
  auto config_count = static_cast<EGLint>(0);
  if (eglChooseConfig(display.get(), nullptr, &config, 1, &config_count) != EGL_TRUE || config_count != 1) {
    return std::move(
      result::Result<Context*, engine::Error>::Err(engine::Error::new_(0, static_cast<i8>(Error::NoConfig))));
  }
  auto self = std::unique_ptr<Context>(new Context(std::forward<std::shared_ptr<void>>(display),
						   std::forward<bool>(pre_load),
						   std::forward<EGLSurface>(EGL_NO_SURFACE)));
  if (auto result = self->initialize(config, EGL_NO_CONTEXT); result.is_some()) {
    return std::move(result::Result<Context*, engine::Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Context*, engine::Error>::Ok(self.release()));
}

auto
engine::graphics::gl::egl::Context::release_current() const& noexcept -> result::Result<std::nullptr_t, engine::Error>
{
  if (eglMakeCurrent(m_display.get(), EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT) == EGL_TRUE) {
    return std::move(result::Result<std::nullptr_t, engine::Error>::Ok(nullptr));
  } else {
    return std::move(
      result::Result<std::nullptr_t, engine::Error>::Err(engine::Error::new_(0, static_cast<i8>(Error::Unknown))));
  }
}

auto
engine::graphics::gl::egl::Context::swap_buffers() const& noexcept -> result::Result<std::nullptr_t, engine::Error>
{
  if (!m_surface) {
    return std::move(
      result::Result<std::nullptr_t, engine::Error>::Err(engine::Error::new_(0, static_cast<i8>(Error::NoSurface))));
  }
  /*if (auto result = make_current(); result.is_err()) {
    return std::move(result);
  }*/
  if (eglSwapBuffers(m_display.get(), m_surface.get()) != EGL_TRUE) {
    return std::move(
      result::Result<std::nullptr_t, engine::Error>::Err(engine::Error::new_(0, static_cast<i8>(Error::Unknown))));
  }
  return std::move(result::Result<std::nullptr_t, engine::Error>::Ok(nullptr));
}

auto
engine::graphics::gl::egl::Context::swap_interval(engine::i32 interval) const& noexcept
  -> result::Result<std::nullptr_t, engine::Error>
{
  if (!m_surface) {
    return std::move(
      result::Result<std::nullptr_t, engine::Error>::Err(engine::Error::new_(0, static_cast<i8>(Error::NoSurface))));
  }
  /*if (auto result = make_current(); result.is_err()) {
    return std::move(result);
  }*/
  if (eglSwapInterval(m_display.get(), interval) != EGL_TRUE) {
    return std::move(
      result::Result<std::nullptr_t, engine::Error>::Err(engine::Error::new_(0, static_cast<i8>(Error::Unknown))));
  }
  return std::move(result::Result<std::nullptr_t, engine::Error>::Ok(nullptr));
}

auto
engine::graphics::gl::egl::Context::with_surface(EGLConfig config,
						 const engine::graphics::gl::egl::Context& context,
						 std::shared_ptr<void> display,
						 bool pre_load,
						 EGLSurface surface) noexcept -> result::Result<Context, engine::Error>
{
  auto self = Context(std::forward<Context const&>(context),
		      std::forward<std::shared_ptr<void>>(display),
		      std::forward<bool>(pre_load),
		      std::forward<EGLSurface>(surface));
  if (auto result = self.initialize(config, context.m_context.get()); result.is_some()) {
    return std::move(result::Result<Context, engine::Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Context, engine::Error>::Ok(std::move(self)));
}

auto
engine::graphics::gl::egl::Context::with_surface_pointer(EGLConfig config,
							 const engine::graphics::gl::egl::Context& context,
							 std::shared_ptr<void> display,
							 bool pre_load,
							 EGLSurface surface) noexcept
  -> result::Result<Context*, engine::Error>
{
  auto self = std::unique_ptr<Context>(new Context(std::forward<Context const&>(context),
						   std::forward<std::shared_ptr<void>>(display),
						   std::forward<bool>(pre_load),
						   std::forward<EGLSurface>(surface)));
  if (auto result = self->initialize(config, context.m_context.get()); result.is_some()) {
    return std::move(result::Result<Context*, engine::Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Context*, engine::Error>::Ok(self.release()));
}

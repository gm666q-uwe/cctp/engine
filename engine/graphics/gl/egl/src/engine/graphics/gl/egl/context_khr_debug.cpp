#include "engine/graphics/gl/egl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_EGL_KHR_debug_LOAD(name, type)                                                                          \
  m_data_khr_debug.name = reinterpret_cast<type>(get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_EGL_KHR_debug_FUNCTION(name, type, ...)                                                                 \
  using namespace std::literals;                                                                                       \
  if (m_data_khr_debug.name == nullptr) [[unlikely]] {                                                                 \
    ENGINE_EGL_KHR_debug_LOAD(name, type)                                                                              \
  }                                                                                                                    \
  return m_data_khr_debug.name(__VA_ARGS__);

auto
engine::graphics::gl::egl::Context::debugMessageControlKHR(EGLDEBUGPROCKHR callback,
							   EGLAttrib const* attrib_list) & noexcept -> EGLint
{
  ENGINE_EGL_KHR_debug_FUNCTION(eglDebugMessageControlKHR, PFNEGLDEBUGMESSAGECONTROLKHRPROC, callback, attrib_list)
}

auto
engine::graphics::gl::egl::Context::queryDebugKHR(EGLint attribute, EGLAttrib* value) & noexcept -> EGLBoolean
{
  ENGINE_EGL_KHR_debug_FUNCTION(eglQueryDebugKHR, PFNEGLQUERYDEBUGKHRPROC, attribute, value)
}

auto
engine::graphics::gl::egl::Context::labelObjectKHR(EGLDisplay display,
						   EGLenum objectType,
						   EGLObjectKHR object,
						   EGLLabelKHR label) & noexcept -> EGLint
{
  ENGINE_EGL_KHR_debug_FUNCTION(eglLabelObjectKHR, PFNEGLLABELOBJECTKHRPROC, display, objectType, object, label)
}

auto
engine::graphics::gl::egl::Context::load_khr_debug() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_EGL_KHR_debug_LOAD(eglDebugMessageControlKHR, PFNEGLDEBUGMESSAGECONTROLKHRPROC)
    ENGINE_EGL_KHR_debug_LOAD(eglQueryDebugKHR, PFNEGLQUERYDEBUGKHRPROC)
      ENGINE_EGL_KHR_debug_LOAD(eglLabelObjectKHR, PFNEGLLABELOBJECTKHRPROC)
}

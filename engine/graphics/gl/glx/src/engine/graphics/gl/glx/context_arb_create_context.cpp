#include "engine/graphics/gl/glx/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GLX_ARB_create_context_LOAD(name, type)                                                                 \
  m_data_arb_create_context.name = reinterpret_cast<type>(get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GLX_ARB_create_context_FUNCTION(name, type, ...)                                                        \
  using namespace std::literals;                                                                                       \
  if (m_data_arb_create_context.name == nullptr) {                                                                     \
    ENGINE_GLX_ARB_create_context_LOAD(name, type)                                                                     \
  }                                                                                                                    \
  return m_data_arb_create_context.name(__VA_ARGS__);

auto
engine::graphics::gl::glx::Context::createContextAttribsARB(Display* dpy,
							    GLXFBConfig config,
							    GLXContext share_context,
							    int direct,
							    int const* attrib_list) & noexcept -> GLXContext
{
  ENGINE_GLX_ARB_create_context_FUNCTION(
    glXCreateContextAttribsARB, PFNGLXCREATECONTEXTATTRIBSARBPROC, dpy, config, share_context, direct, attrib_list)
}

auto
engine::graphics::gl::glx::Context::load_arb_create_context() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GLX_ARB_create_context_LOAD(glXCreateContextAttribsARB, PFNGLXCREATECONTEXTATTRIBSARBPROC)
}

#include "engine/graphics/gl/glx/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GLX_EXT_swap_control_LOAD(name, type)                                                                   \
  m_data_ext_swap_control.name = reinterpret_cast<type>(get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GLX_EXT_swap_control_FUNCTION(name, type, ...)                                                          \
  using namespace std::literals;                                                                                       \
  if (m_data_ext_swap_control.name == nullptr) {                                                                       \
    ENGINE_GLX_EXT_swap_control_LOAD(name, type)                                                                       \
  }                                                                                                                    \
  return m_data_ext_swap_control.name(__VA_ARGS__);

auto
engine::graphics::gl::glx::Context::swapIntervalEXT(Display* dpy, GLXDrawable drawable, int interval) & noexcept -> void
{
  ENGINE_GLX_EXT_swap_control_FUNCTION(glXSwapIntervalEXT, PFNGLXSWAPINTERVALEXTPROC, dpy, drawable, interval)
}

auto
engine::graphics::gl::glx::Context::load_ext_swap_control() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GLX_EXT_swap_control_LOAD(glXSwapIntervalEXT, PFNGLXSWAPINTERVALEXTPROC)
}

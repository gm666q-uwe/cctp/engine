#include "engine/graphics/gl/glx/context.hpp"

#include <iostream>
#include <span>

engine::graphics::gl::glx::Context::Context(Display* display, GLXDrawable drawable, bool pre_load) noexcept
  : InterfaceContext()
  , m_display(display)
  , m_drawable(drawable)
  , m_gl(std::shared_ptr<gl::Context>(gl::Context::new_pointer(this, pre_load)))
{}

engine::graphics::gl::glx::Context::Context(engine::graphics::gl::glx::Context const& context,
					    Display* display,
					    GLXDrawable drawable) noexcept
  : InterfaceContext()
  , m_display(display)
  , m_drawable(drawable)
  , m_gl(context.m_gl)
{}

engine::graphics::gl::glx::Context::Context(engine::graphics::gl::glx::Context&& other) noexcept
  : InterfaceContext(std::move(other))
  , m_context(std::exchange(other.m_context, nullptr))
  , m_data_arb_create_context(std::move(other.m_data_arb_create_context))
  , m_display(other.m_display)
  , m_drawable(other.m_drawable)
  , m_gl(std::move(other.m_gl))
{
  m_gl->set_interface_context(&other, this);
}

engine::graphics::gl::glx::Context::~Context() noexcept
{
  drop();
}

auto
engine::graphics::gl::glx::Context::operator=(engine::graphics::gl::glx::Context&& other) & noexcept
  -> engine::graphics::gl::glx::Context&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_context = std::exchange(other.m_context, m_context);
  m_data_arb_create_context = std::move(other.m_data_arb_create_context);
  m_display = other.m_display;
  m_drawable = other.m_drawable;
  m_gl = std::move(other.m_gl);

  m_gl->set_interface_context(&other, this);

  InterfaceContext::operator=(std::move(other));
  return *this;
}

auto
engine::graphics::gl::glx::Context::check_extensions(engine::CStr extensions) noexcept -> bool
{
  using namespace std::literals;

  auto glx_arb_create_context = extensions.find("GLX_ARB_create_context"sv) != std::string_view::npos;
  auto glx_arb_create_context_profile = extensions.find("GLX_ARB_create_context_profile"sv) != std::string_view::npos;
  auto glx_arb_framebuffer_srgb = extensions.find("GLX_ARB_framebuffer_sRGB"sv) != std::string_view::npos;
  auto glx_arb_get_proc_address = extensions.find("GLX_ARB_get_proc_address"sv) != std::string_view::npos;
  auto glx_ext_framebuffer_srgb = extensions.find("GLX_EXT_framebuffer_sRGB"sv) != std::string_view::npos;
  auto glx_ext_swap_control = extensions.find("GLX_EXT_swap_control"sv) != std::string_view::npos;
  auto glx_ext_swap_control_tear = extensions.find("GLX_EXT_swap_control_tear"sv) != std::string_view::npos;

  return glx_arb_create_context && glx_arb_create_context_profile &&
	 (glx_arb_framebuffer_srgb || glx_ext_framebuffer_srgb) && glx_arb_get_proc_address && glx_ext_swap_control &&
	 glx_ext_swap_control_tear;
}

auto
engine::graphics::gl::glx::Context::choose_fb_config(Display* display) noexcept
  -> result::Result<GLXFBConfig, engine::Error>
{
  auto attributes = std::array<int, 25>{ GLX_DOUBLEBUFFER,
					 True,
					 GLX_RED_SIZE,
					 8,
					 GLX_GREEN_SIZE,
					 8,
					 GLX_BLUE_SIZE,
					 8,
					 GLX_ALPHA_SIZE,
					 8,
					 GLX_DEPTH_SIZE,
					 24,
					 GLX_RENDER_TYPE,
					 GLX_RGBA_BIT,
					 GLX_DRAWABLE_TYPE,
					 GLX_WINDOW_BIT,
					 GLX_X_RENDERABLE,
					 True,
					 GLX_X_VISUAL_TYPE,
					 GLX_TRUE_COLOR,
					 GLX_CONFIG_CAVEAT,
					 GLX_NONE,
					 // GLX_TRANSPARENT_TYPE,  GLX_TRANSPARENT_RGB,
					 GLX_FRAMEBUFFER_SRGB_CAPABLE_ARB,
					 True,
					 XLIB_NONE };

  auto config_count = 0;
  auto configs = glXChooseFBConfig(display, DefaultScreen(display), attributes.data(), &config_count);
  if (configs == nullptr || config_count <= 0) {
    return std::move(result::Result<GLXFBConfig, Error>::Err(Error::new_(0, -1)));
  }

  auto config = configs[0];
  XFree(configs);

  return std::move(result::Result<GLXFBConfig, Error>::Ok(config));
}

auto
engine::graphics::gl::glx::Context::drop() & noexcept -> void
{
  if (is_current()) {
    release_current().unwrap();
  }
  if (m_context != nullptr) {
    glXDestroyContext(m_display, m_context);
    m_context = nullptr;
  }
}

auto
engine::graphics::gl::glx::Context::get_proc_address(std::string_view name) const& noexcept
  -> result::Result<void (*)(void), engine::Error>
{
  if (auto result = glXGetProcAddressARB(reinterpret_cast<GLubyte const*>(name.data())); result == nullptr) {
    return std::move(result::Result<void (*)(void), Error>::Err(Error::new_(0, -1)));
  } else {
    return std::move(result::Result<void (*)(void), Error>::Ok(result));
  }
}

auto
engine::graphics::gl::glx::Context::gl() const& noexcept -> engine::graphics::gl::Context&
{
  return *m_gl;
}

auto
engine::graphics::gl::glx::Context::initialize(GLXFBConfig config, GLXContext context) & noexcept
  -> option::Option<engine::Error>
{
  using namespace std::literals;

  auto extensions = glXQueryExtensionsString(m_display, DefaultScreen(m_display));
  if (extensions == nullptr) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }
  if (!check_extensions(extensions)) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  auto attributes =
    std::array<int, 9>{ GLX_CONTEXT_MAJOR_VERSION_ARB,
			4,
			GLX_CONTEXT_MINOR_VERSION_ARB,
			6,
			GLX_CONTEXT_FLAGS_ARB,
			config::is_debug ? GLX_CONTEXT_DEBUG_BIT_ARB | GLX_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB
					 : GLX_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
			GLX_CONTEXT_PROFILE_MASK_ARB,
			GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
			XLIB_NONE };

  if (m_context = createContextAttribsARB(m_display, config, context, True, attributes.data()); m_context == nullptr) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  load_ext_swap_control();

  return std::move(option::Option<Error>::None());
}

auto
engine::graphics::gl::glx::Context::is_current() const& noexcept -> bool
{
  return m_context != nullptr && glXGetCurrentContext() == m_context;
}

auto
engine::graphics::gl::glx::Context::load(bool include_gl) & noexcept -> void
{
  load_arb_create_context();
  load_ext_swap_control();
  if (include_gl) {
    m_gl->load();
  }
}

auto
engine::graphics::gl::glx::Context::make_current() const& noexcept -> result::Result<std::nullptr_t, engine::Error>
{
  if (m_drawable == XLIB_NONE) {
    return std::move(result::Result<std::nullptr_t, Error>::Err(Error::new_(0, -1)));
  }
  if (glXMakeContextCurrent(m_display, m_drawable, m_drawable, m_context) == True) {
    return std::move(result::Result<std::nullptr_t, Error>::Ok(nullptr));
  } else {
    return std::move(result::Result<std::nullptr_t, Error>::Err(Error::new_(0, -1)));
  }
}

auto
engine::graphics::gl::glx::Context::new_(GLXFBConfig config, Display* display) noexcept
  -> result::Result<engine::graphics::gl::glx::Context, engine::Error>
{
  auto self = Context(display, XLIB_NONE, false);
  if (auto result = self.initialize(config, nullptr); result.is_some()) {
    return std::move(result::Result<Context, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Context, Error>::Ok(std::move(self)));
}

auto
engine::graphics::gl::glx::Context::new_pointer(GLXFBConfig config, Display* display) noexcept
  -> result::Result<engine::graphics::gl::glx::Context*, engine::Error>
{
  auto self = std::unique_ptr<Context>(new Context(display, XLIB_NONE, false));
  if (auto result = self->initialize(config, nullptr); result.is_some()) {
    return std::move(result::Result<Context*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Context*, Error>::Ok(self.release()));
}

auto
engine::graphics::gl::glx::Context::release_current() const& noexcept -> result::Result<std::nullptr_t, engine::Error>
{
  if (glXMakeContextCurrent(m_display, XLIB_NONE, XLIB_NONE, nullptr) == True) {
    return std::move(result::Result<std::nullptr_t, Error>::Ok(nullptr));
  } else {
    return std::move(result::Result<std::nullptr_t, Error>::Err(Error::new_(0, -1)));
  }
}

auto
engine::graphics::gl::glx::Context::swap_buffers() const& noexcept -> result::Result<std::nullptr_t, engine::Error>
{
  if (m_drawable == XLIB_NONE) {
    return std::move(result::Result<std::nullptr_t, Error>::Err(Error::new_(0, -1)));
  }
  /*if (!is_current()) {
    if (auto result = make_current(); result.is_err()) {
      return std::move(result);
    }
  }*/
  glXSwapBuffers(m_display, m_drawable);
  return std::move(result::Result<std::nullptr_t, Error>::Ok(nullptr));
}

auto
engine::graphics::gl::glx::Context::swap_interval(engine::i32 interval) const& noexcept
  -> result::Result<std::nullptr_t, Error>
{
  const_cast<Context*>(this)->swapIntervalEXT(m_display, m_drawable, interval);
  return std::move(result::Result<std::nullptr_t, Error>::Ok(nullptr));
}

auto
engine::graphics::gl::glx::Context::with_drawable(GLXFBConfig config,
						  engine::graphics::gl::glx::Context const& context,
						  GLXDrawable drawable) noexcept
  -> result::Result<engine::graphics::gl::glx::Context, engine::Error>
{
  auto self = Context(context, context.m_display, drawable);
  if (auto result = self.initialize(config, context.m_context); result.is_some()) {
    return std::move(result::Result<Context, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Context, Error>::Ok(std::move(self)));
}

auto
engine::graphics::gl::glx::Context::with_drawable_pointer(GLXFBConfig config,
							  engine::graphics::gl::glx::Context const& context,
							  GLXDrawable drawable) noexcept
  -> result::Result<engine::graphics::gl::glx::Context*, engine::Error>
{
  auto self = std::unique_ptr<Context>(new Context(context, context.m_display, drawable));
  if (auto result = self->initialize(config, context.m_context); result.is_some()) {
    return std::move(result::Result<Context*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Context*, Error>::Ok(self.release()));
}

#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/glx/glx.h"

namespace engine::graphics::gl::glx {
struct ContextDataARBCreateContext : public IContextData
{
public:
  // GLX_ARB_create_context
  PFNGLXCREATECONTEXTATTRIBSARBPROC glXCreateContextAttribsARB = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return glXCreateContextAttribsARB != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl::glx

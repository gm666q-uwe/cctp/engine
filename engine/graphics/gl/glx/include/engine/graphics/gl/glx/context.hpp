#pragma once

#include <memory>

#include <engine/graphics/gl/context.hpp>
#include <engine/graphics/gl/interface_context.hpp>

#include "engine/graphics/gl/glx/context_data_arb_create_context.hpp"
#include "engine/graphics/gl/glx/context_data_ext_swap_control.hpp"

namespace engine::graphics::gl::glx {
class Context final : public InterfaceContext
{
public:
  constexpr Context(Context const&) noexcept = delete;

  Context(Context&& other) noexcept;

  ~Context() noexcept override;

  constexpr auto operator=(Context const&) & noexcept -> Context& = delete;

  auto operator=(Context&& other) & noexcept -> Context&;

  static auto choose_fb_config(Display* display) noexcept -> result::Result<GLXFBConfig, Error>;

  [[nodiscard]] auto get_proc_address(std::string_view name) const& noexcept
    -> result::Result<void (*)(void), Error> override;

  [[nodiscard]] auto gl() const& noexcept -> gl::Context& override;

  // GLX_ARB_create_context
  auto createContextAttribsARB(Display* dpy,
			       GLXFBConfig config,
			       GLXContext share_context,
			       Bool direct,
			       int const* attrib_list) & noexcept -> GLXContext;

  // GLX_EXT_swap_control
  auto swapIntervalEXT(Display* dpy, GLXDrawable drawable, int interval) & noexcept -> void;

  [[nodiscard]] auto is_current() const& noexcept -> bool override;

  [[nodiscard]] constexpr auto is_loaded(bool include_gl) const& noexcept -> bool override
  {
    return (is_arb_create_context_loaded() && is_ext_swap_control()) && (!include_gl || m_gl->is_loaded());
  }

  [[nodiscard]] constexpr auto is_arb_create_context_loaded() const& noexcept -> bool
  {
    return m_data_arb_create_context.is_loaded();
  }

  [[nodiscard]] constexpr auto is_ext_swap_control() const& noexcept -> bool
  {
    return m_data_ext_swap_control.is_loaded();
  }

  auto load(bool include_gl) & noexcept -> void override;

  auto load_arb_create_context() & noexcept -> void;

  auto load_ext_swap_control() & noexcept -> void;

  [[nodiscard]] auto make_current() const& noexcept -> result::Result<std::nullptr_t, Error> override;

  static auto new_(GLXFBConfig config, Display* display) noexcept -> result::Result<Context, Error>;

  static auto new_pointer(GLXFBConfig config, Display* display) noexcept -> result::Result<Context*, Error>;

  [[nodiscard]] auto release_current() const& noexcept -> result::Result<std::nullptr_t, Error> override;

  [[nodiscard]] auto swap_buffers() const& noexcept -> result::Result<std::nullptr_t, Error> override;

  [[nodiscard]] auto swap_interval(i32 interval) const& noexcept -> result::Result<std::nullptr_t, Error> override;

  static auto with_drawable(GLXFBConfig config, Context const& context, GLXDrawable drawable) noexcept
    -> result::Result<Context, Error>;

  static auto with_drawable_pointer(GLXFBConfig config, Context const& context, GLXDrawable drawable) noexcept
    -> result::Result<Context*, Error>;

protected:
private:
  GLXContext m_context = nullptr;
  Display* m_display = nullptr;
  GLXDrawable m_drawable = XLIB_NONE;
  std::shared_ptr<gl::Context> m_gl = std::shared_ptr<gl::Context>();

  // GLX_ARB_create_context
  ContextDataARBCreateContext m_data_arb_create_context = {};

  // GLX_EXT_swap_control
  ContextDataEXTSwapControl m_data_ext_swap_control = {};

  Context(Display* display, GLXDrawable drawable, bool pre_load) noexcept;

  Context(Context const& context, Display* display, GLXDrawable drawable) noexcept;

  auto check_extensions(CStr extensions) noexcept -> bool;

  auto drop() & noexcept -> void;

  auto initialize(GLXFBConfig config, GLXContext context) & noexcept -> option::Option<Error>;
};
} // namespace engine::graphics::gl::glx

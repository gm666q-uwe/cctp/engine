#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/glx/glx.h"

namespace engine::graphics::gl::glx {
struct ContextDataEXTSwapControl : public IContextData
{
public:
  // GLX_EXT_swap_control
  PFNGLXSWAPINTERVALEXTPROC glXSwapIntervalEXT = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override { return glXSwapIntervalEXT != nullptr; }

protected:
private:
};
} // namespace engine::graphics::gl::glx

#pragma once

#include "engine/graphics/gl/gl.h"

#if ENGINE_HAS_GLX
#include <GL/glx.h>
#include <GL/glxext.h>
#endif

#ifdef None
constexpr int const XLIB_NONE = None;
#undef None
#endif

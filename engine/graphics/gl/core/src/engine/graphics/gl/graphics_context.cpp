#include "engine/graphics/gl/graphics_context.hpp"

#include <iostream>
#include <memory>
#include <valarray>

#include "engine/graphics/gl/context.hpp"

engine::graphics::gl::GraphicsContext::GraphicsContext(engine::u16 height,
						       engine::graphics::gl::InterfaceContext const& interface_context,
						       engine::u16 width) noexcept
  : graphics::GraphicsContext()
  , m_gl(interface_context.gl())
  , m_height(height)
  , m_interface_context(interface_context)
  , m_width(width)
{}

engine::graphics::gl::GraphicsContext::GraphicsContext(engine::graphics::gl::GraphicsContext&& other) noexcept
  : graphics::GraphicsContext(std::move(other))
  , m_camera(std::exchange(other.m_camera, nullptr))
  , m_flags(std::exchange(other.m_flags, Flags::empty()))
  , m_gl(other.m_gl)
  , m_height(std::exchange(other.m_height, 0))
  , m_interface_context(other.m_interface_context)
  , m_resize_callback(std::exchange(other.m_resize_callback, nullptr))
  , m_width(std::exchange(other.m_width, 0))
{}

engine::graphics::gl::GraphicsContext::~GraphicsContext() noexcept
{
  drop();
}

auto
engine::graphics::gl::GraphicsContext::operator=(engine::graphics::gl::GraphicsContext&& other) & noexcept
  -> engine::graphics::gl::GraphicsContext&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_camera = std::exchange(other.m_camera, m_camera);
  m_flags = std::exchange(other.m_flags, m_flags);
  m_gl = other.m_gl;
  m_height = std::exchange(other.m_height, m_height);
  const_cast<InterfaceContext&>(m_interface_context) = other.m_interface_context;
  m_resize_callback = std::exchange(other.m_resize_callback, m_resize_callback);
  m_width = std::exchange(other.m_width, m_width);

  graphics::GraphicsContext::operator=(std::move(other));
  return *this;
}

auto
engine::graphics::gl::GraphicsContext::begin_frame() & noexcept -> void
{
  make_current();
  m_gl.clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  if (m_flags.contains(Flags::HAS_RESIZE())) {
    m_gl.scissor(0, 0, m_width, m_height);
    m_gl.viewport(0, 0, m_width, m_height);
    if (m_camera != nullptr) {
      m_camera->on_resize(m_height, m_width);
    }
  }
  m_flags.remove(Flags::HAS_RESIZE());
}

auto
engine::graphics::gl::GraphicsContext::debug_message_callback(GLenum source,
							      GLenum type,
							      GLuint id,
							      GLenum severity,
							      GLsizei length,
							      GLchar const* message) const& noexcept -> void
{
  // std::cout << "[engine::graphics][Renderer][debug_message_callback]: " << message << std::endl;
  auto const src_str = [source]() {
    switch (source) {
      case GL_DEBUG_SOURCE_API:
	return "API";
      case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
	return "WINDOW SYSTEM";
      case GL_DEBUG_SOURCE_SHADER_COMPILER:
	return "SHADER COMPILER";
      case GL_DEBUG_SOURCE_THIRD_PARTY:
	return "THIRD PARTY";
      case GL_DEBUG_SOURCE_APPLICATION:
	return "APPLICATION";
      case GL_DEBUG_SOURCE_OTHER:
	return "OTHER";
    }
  }();

  auto const type_str = [type]() {
    switch (type) {
      case GL_DEBUG_TYPE_ERROR:
	return "ERROR";
      case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
	return "DEPRECATED_BEHAVIOR";
      case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
	return "UNDEFINED_BEHAVIOR";
      case GL_DEBUG_TYPE_PORTABILITY:
	return "PORTABILITY";
      case GL_DEBUG_TYPE_PERFORMANCE:
	return "PERFORMANCE";
      case GL_DEBUG_TYPE_MARKER:
	return "MARKER";
      case GL_DEBUG_TYPE_OTHER:
	return "OTHER";
    }
  }();

  auto const severity_str = [severity]() {
    switch (severity) {
      case GL_DEBUG_SEVERITY_NOTIFICATION:
	return "NOTIFICATION";
      case GL_DEBUG_SEVERITY_LOW:
	return "LOW";
      case GL_DEBUG_SEVERITY_MEDIUM:
	return "MEDIUM";
      case GL_DEBUG_SEVERITY_HIGH:
	return "HIGH";
    }
  }();

  std::cout << src_str << ", " << type_str << ", " << severity_str << ", " << id << ": " << message << '\n';
}

auto
engine::graphics::gl::GraphicsContext::debug_message_callback(GLenum source,
							      GLenum type,
							      GLuint id,
							      GLenum severity,
							      GLsizei length,
							      GLchar const* message,
							      void const* userParam) noexcept -> void
{
  static_cast<GraphicsContext const*>(userParam)->debug_message_callback(source, type, id, severity, length, message);
}

auto
engine::graphics::gl::GraphicsContext::drop() & noexcept -> void
{
  m_camera = nullptr;
  m_flags = Flags::empty();
  m_resize_callback = nullptr;
}

auto
engine::graphics::gl::GraphicsContext::create_camera() const& noexcept
  -> result::Result<engine::graphics::Camera, engine::Error>
{
  if (auto result = create_uniform_buffer_pointer<Camera::Matrices>(); result.is_err()) {
    return std::move(result::Result<Camera, Error>::Err(std::move(result).unwrap_err()));
  } else {
    return std::move(result::Result<Camera, Error>::Ok(Camera::new_(std::move(result).unwrap())));
  }
}

auto
engine::graphics::gl::GraphicsContext::create_camera_pointer() const& noexcept
  -> result::Result<engine::graphics::Camera*, engine::Error>
{
  if (auto result = create_uniform_buffer_pointer<Camera::Matrices>(); result.is_err()) {
    return std::move(result::Result<Camera*, Error>::Err(std::move(result).unwrap_err()));
  } else {
    return std::move(result::Result<Camera*, Error>::Ok(Camera::new_pointer(std::move(result).unwrap())));
  }
}

auto
engine::graphics::gl::GraphicsContext::create_uniform_buffer(engine::isize size) const& noexcept
  -> result::Result<engine::graphics::gl::UniformBuffer<void>, engine::Error>
{
  return std::move(UniformBuffer<void>::new_(m_gl, size));
}

auto
engine::graphics::gl::GraphicsContext::create_uniform_buffer_pointer(engine::isize size) const& noexcept
  -> result::Result<engine::graphics::UniformBuffer<void>*, engine::Error>
{
  if (auto result = UniformBuffer<void>::new_pointer(m_gl, size); result.is_err()) {
    return std::move(result::Result<graphics::UniformBuffer<void>*, Error>::Err(std::move(result).unwrap_err()));
  } else {
    return std::move(result::Result<graphics::UniformBuffer<void>*, Error>::Ok(std::move(result).unwrap()));
  }
}

auto
engine::graphics::gl::GraphicsContext::end_frame() & noexcept -> void
{
  m_interface_context.swap_buffers().unwrap();
  // m_interface_context.release_current().unwrap();
}

auto
engine::graphics::gl::GraphicsContext::gl() const& noexcept -> engine::graphics::gl::Context&
{
  return m_gl;
}

auto
engine::graphics::gl::GraphicsContext::initialize() & noexcept -> option::Option<Error>
{
  if (auto result = m_interface_context.make_current(); result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  }

#pragma clang diagnostic push
#pragma ide diagnostic ignored "Simplify"
#pragma ide diagnostic ignored "UnreachableCode"
  if constexpr (config::is_debug) {
    m_gl.enable(GL_DEBUG_OUTPUT);
    m_gl.debugMessageCallback(debug_message_callback, this);
  }
#pragma clang diagnostic pop

  auto gl_major = static_cast<GLint>(0);
  auto gl_minor = static_cast<GLint>(0);
  m_gl.getIntegerv(GL_MAJOR_VERSION, &gl_major);
  m_gl.getIntegerv(GL_MINOR_VERSION, &gl_minor);
  std::cout << "[engine::graphics::gl][GraphicsContext][initialize]: GL " << gl_major << '.' << gl_minor
	    << " context created." << std::endl;

  m_gl.clearColor(0.0f, 0.0f, 0.0f, 1.0f);
  m_gl.enable(GL_CULL_FACE);
  m_gl.enable(GL_DEPTH_TEST);
  m_gl.enable(GL_PRIMITIVE_RESTART_FIXED_INDEX);

  return std::move(option::Option<Error>::None());
}

auto
engine::graphics::gl::GraphicsContext::interface_context() const& noexcept
  -> const engine::graphics::gl::InterfaceContext&
{
  return m_interface_context;
}

auto
engine::graphics::gl::GraphicsContext::make_current() const& noexcept -> void
{
  m_interface_context.make_current().unwrap();
}

auto
engine::graphics::gl::GraphicsContext::new_(engine::u16 height,
					    engine::graphics::gl::InterfaceContext const& interface_context,
					    engine::u16 width) noexcept -> result::Result<GraphicsContext, Error>
{
  auto self = GraphicsContext(height, interface_context, width);
  if (auto result = self.initialize(); result.is_some()) {
    return std::move(result::Result<GraphicsContext, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<GraphicsContext, Error>::Ok(std::move(self)));
}

auto
engine::graphics::gl::GraphicsContext::new_pointer(engine::u16 height,
						   engine::graphics::gl::InterfaceContext const& interface_context,
						   engine::u16 width) noexcept
  -> result::Result<GraphicsContext*, Error>
{
  auto self = std::unique_ptr<GraphicsContext>(new GraphicsContext(height, interface_context, width));
  if (auto result = self->initialize(); result.is_some()) {
    return std::move(result::Result<GraphicsContext*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<GraphicsContext*, Error>::Ok(self.release()));
}

auto
engine::graphics::gl::GraphicsContext::on_resize(std::uint16_t height, std::uint16_t width) & noexcept -> void
{
  m_height = height;
  m_width = width;
  m_flags.insert(Flags::HAS_RESIZE());
  if (m_resize_callback) {
    m_resize_callback(m_height, m_width);
  }
}

auto
engine::graphics::gl::GraphicsContext::set_camera(engine::graphics::Camera* camera) & noexcept -> void
{
  m_camera = camera;
  if (m_camera != nullptr) {
    m_camera->on_resize(m_height, m_width);
  }
}

auto
engine::graphics::gl::GraphicsContext::set_clear_color(glm::vec4 const& color) & noexcept -> void
{
  m_gl.clearColor(color.r, color.g, color.b, color.a);
}

auto
engine::graphics::gl::GraphicsContext::set_resize_callback(std::function<void(u16, u16)> resize_callback) & noexcept -> void
{
  m_resize_callback = resize_callback;
  if (m_resize_callback) {
    m_resize_callback(m_height, m_width);
  }
}

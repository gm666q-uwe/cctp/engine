#include "engine/graphics/gl/uniform_buffer.hpp"

auto
engine::graphics::gl::UniformBuffer<void>::bind() const& noexcept -> void
{
  m_gl.bindBufferBase(GL_UNIFORM_BUFFER, 0, m_ubo);
}

auto
engine::graphics::gl::UniformBuffer<void>::drop() & noexcept -> void
{
  /*if (is_bound()) {
    unbind();
  }*/
  if (m_ubo_data != nullptr) {
    m_gl.unmapNamedBuffer(m_ubo);
    m_ubo_data = nullptr;
  }
  m_gl.deleteBuffers(1, &m_ubo);
  m_ubo = GL_NONE;
}

auto
engine::graphics::gl::UniformBuffer<void>::initialize(engine::isize size) & noexcept -> option::Option<Error>
{
  m_gl.createBuffers(1, &m_ubo);
  m_gl.namedBufferStorage(m_ubo, size, nullptr, M_STORAGE_FLAGS);
  m_ubo_data = m_gl.mapNamedBufferRange(m_ubo, 0, size, M_MAPPING_FLAGS);
  if (m_ubo_data == nullptr) {
    return option::Option<Error>::Some(Error::new_(0, -1));
  }

  return option::Option<Error>::None();
}

auto
engine::graphics::gl::UniformBuffer<void>::new_(engine::graphics::gl::Context& gl, engine::isize size) noexcept
-> result::Result<engine::graphics::gl::UniformBuffer<void>, engine::Error>
{
  auto self = UniformBuffer<void>(std::forward<Context&>(gl));
  if (auto result = self.initialize(size); result.is_some()) {
    return std::move(result::Result<UniformBuffer<void>, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<UniformBuffer<void>, Error>::Ok(std::move(self)));
}

auto
engine::graphics::gl::UniformBuffer<void>::new_pointer(engine::graphics::gl::Context& gl, engine::isize size) noexcept
-> result::Result<engine::graphics::gl::UniformBuffer<void>*, engine::Error>
{
  auto self = std::unique_ptr<UniformBuffer<void>>(new UniformBuffer<void>(std::forward<Context&>(gl)));
  if (auto result = self->initialize(size); result.is_some()) {
    return std::move(result::Result<UniformBuffer<void>*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<UniformBuffer<void>*, Error>::Ok(self.release()));
}

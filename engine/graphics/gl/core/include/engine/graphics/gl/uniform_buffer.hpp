#pragma once

#include <memory>

#include <engine/graphics/uniform_buffer.hpp>

#include "engine/graphics/gl/context.hpp"

namespace engine::graphics::gl {
template<typename T>
class UniformBuffer final : public graphics::UniformBuffer<T>
{
  friend class GraphicsContext;

public:
  constexpr UniformBuffer<T>() noexcept = delete;

  constexpr UniformBuffer<T>(UniformBuffer<T> const&) noexcept = delete;

  /*constexpr explicit UniformBuffer<T>(UniformBuffer<void> const& other) noexcept
    : graphics::UniformBuffer<T>()
    , m_gl(other.m_gl)
    , m_ubo(other.m_ubo)
    , m_ubo_data(static_cast<T*>(other.m_ubo_data))
  {}*/

  constexpr UniformBuffer<T>(UniformBuffer<T>&& other) noexcept
    : graphics::UniformBuffer<T>(std::move(other))
    , m_gl(other.m_gl)
    , m_ubo(std::exchange(other.m_ubo, GL_NONE))
    , m_ubo_data(std::exchange(other.m_ubo_data, nullptr))
  {}

  ~UniformBuffer() noexcept override { drop(); }

  constexpr auto operator=(UniformBuffer<T> const&) & noexcept -> UniformBuffer<T>& = delete;

  constexpr auto operator=(UniformBuffer<T>&& other) & noexcept -> UniformBuffer<T>&
  {
    if (this == &other) {
      return *this;
    }

    drop();

    m_gl = other.m_gl;
    m_ubo = std::exchange(other.m_ubo, m_ubo);
    m_ubo_data = std::exchange(other.m_ubo_data, m_ubo_data);

    graphics::UniformBuffer<T>::operator=(std::move(other));
    return *this;
  }

  auto bind() const& noexcept -> void override { m_gl.bindBufferBase(GL_UNIFORM_BUFFER, 0, m_ubo); }

  [[nodiscard]] constexpr auto data() const& noexcept -> T* override { return m_ubo_data; }

protected:
private:
  static constexpr GLbitfield const M_MAPPING_FLAGS =
    GL_MAP_COHERENT_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_READ_BIT | GL_MAP_WRITE_BIT;
  static constexpr GLbitfield const M_STORAGE_FLAGS = GL_DYNAMIC_STORAGE_BIT | M_MAPPING_FLAGS;

  Context& m_gl;
  GLuint m_ubo = GL_NONE;
  T* m_ubo_data = nullptr;

  constexpr explicit UniformBuffer(Context& gl) noexcept
    : m_gl(gl)
  {}

  auto drop() & noexcept -> void {
    /*if (is_bound()) {
    unbind();
  }*/
    if (m_ubo_data != nullptr) {
      m_gl.unmapNamedBuffer(m_ubo);
      m_ubo_data = nullptr;
    }
    m_gl.deleteBuffers(1, &m_ubo);
    m_ubo = GL_NONE;
  }

  auto initialize() & noexcept -> option::Option<Error>
  {
    m_gl.createBuffers(1, &m_ubo);
    m_gl.namedBufferStorage(m_ubo, sizeof(T), nullptr, M_STORAGE_FLAGS);
    m_ubo_data = static_cast<T*>(m_gl.mapNamedBufferRange(m_ubo, 0, sizeof(T), M_MAPPING_FLAGS));
    if (m_ubo_data == nullptr) {
      return option::Option<Error>::Some(Error::new_( 0, -1 ));
    }

    return option::Option<Error>::None();
  }

  static auto new_(Context& gl) noexcept -> result::Result<UniformBuffer<T>, Error>
  {
    auto self = UniformBuffer<T>(std::forward<Context&>(gl));
    if (auto result = self.initialize(); result.is_some()) {
      return std::move(result::Result<UniformBuffer<T>, Error>::Err(std::move(result).unwrap()));
    }
    return std::move(result::Result<UniformBuffer<T>, Error>::Ok(std::move(self)));
  }

  static auto new_pointer(Context& gl) noexcept -> result::Result<UniformBuffer<T>*, Error>
  {
    auto self = std::unique_ptr<UniformBuffer<T>>(new UniformBuffer<T>(std::forward<Context&>(gl)));
    if (auto result = self->initialize(); result.is_some()) {
      return std::move(result::Result<UniformBuffer<T>*, Error>::Err(std::move(result).unwrap()));
    }
    return std::move(result::Result<UniformBuffer<T>*, Error>::Ok(self.release()));
  }
};

template<>
class UniformBuffer<void> final : public graphics::UniformBuffer<void>
{
  friend class GraphicsContext;

public:
  constexpr UniformBuffer() noexcept = delete;

  constexpr UniformBuffer(UniformBuffer const&) noexcept = delete;

  constexpr UniformBuffer(UniformBuffer&& other) noexcept
    : graphics::UniformBuffer<void>(std::move(other))
    , m_gl(other.m_gl)
    , m_ubo(std::exchange(other.m_ubo, GL_NONE))
    , m_ubo_data(std::exchange(other.m_ubo_data, nullptr))
  {}

  ~UniformBuffer() noexcept override { drop(); }

  constexpr auto operator=(UniformBuffer const&) & noexcept -> UniformBuffer& = delete;

  constexpr auto operator=(UniformBuffer&& other) & noexcept -> UniformBuffer&
  {
    if (this == &other) {
      return *this;
    }

    drop();

    m_gl = other.m_gl;
    m_ubo = std::exchange(other.m_ubo, m_ubo);
    m_ubo_data = std::exchange(other.m_ubo_data, m_ubo_data);

    graphics::UniformBuffer<void>::operator=(std::move(other));
    return *this;
  }

  template<typename T>
  constexpr explicit operator UniformBuffer<T>() const& noexcept
  {
    return UniformBuffer<T>(*this);
  }

  auto bind() const& noexcept -> void override;

  [[nodiscard]] constexpr auto data() const& noexcept -> void* override { return m_ubo_data; }

protected:
private:
  static constexpr GLbitfield const M_MAPPING_FLAGS =
    GL_MAP_COHERENT_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_READ_BIT | GL_MAP_WRITE_BIT;
  static constexpr GLbitfield const M_STORAGE_FLAGS = GL_DYNAMIC_STORAGE_BIT | M_MAPPING_FLAGS;

  Context& m_gl;
  GLuint m_ubo = GL_NONE;
  void* m_ubo_data = nullptr;

  constexpr explicit UniformBuffer(Context& gl) noexcept
    : m_gl(gl)
  {}

  auto drop() & noexcept -> void;

  auto initialize(isize size) & noexcept -> option::Option<Error>;

  static auto new_(Context& gl, isize size) noexcept -> result::Result<UniformBuffer<void>, Error>;

  static auto new_pointer(Context& gl, isize size) noexcept -> result::Result<UniformBuffer<void>*, Error>;
};
} // namespace engine::graphics::gl

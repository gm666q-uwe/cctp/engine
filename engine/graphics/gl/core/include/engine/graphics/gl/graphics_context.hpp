#pragma once

#include <engine/bitflags.hpp>
#include <engine/graphics/graphics_context.hpp>

#include "engine/graphics/gl/interface_context.hpp"
#include "engine/graphics/gl/uniform_buffer.hpp"

namespace engine::graphics::gl {
class GraphicsContext final : public graphics::GraphicsContext
{
public:
  ENGINE_BITFLAGS(Flags, u8, 1, ((HAS_RESIZE, 0x01)))

  constexpr GraphicsContext() noexcept = delete;

  constexpr GraphicsContext(GraphicsContext const&) noexcept = delete;

  GraphicsContext(GraphicsContext&& other) noexcept;

  ~GraphicsContext() noexcept override;

  constexpr auto operator=(GraphicsContext const&) & noexcept -> GraphicsContext& = delete;

  auto operator=(GraphicsContext&& other) & noexcept -> GraphicsContext&;

  auto begin_frame() & noexcept -> void override;

  [[nodiscard]] auto create_camera() const& noexcept -> result::Result<Camera, Error> override;

  [[nodiscard]] auto create_camera_pointer() const& noexcept -> result::Result<Camera*, Error> override;

  template<typename T>
  [[nodiscard]] auto create_uniform_buffer() const& noexcept -> result::Result<UniformBuffer<T>, Error>
  {
    return std::move(UniformBuffer<T>::new_(m_gl));
  }

  [[nodiscard]] auto create_uniform_buffer(isize size) const& noexcept -> result::Result<UniformBuffer<void>, Error>;

  template<typename T>
  [[nodiscard]] auto create_uniform_buffer_pointer() const& noexcept -> result::Result<UniformBuffer<T>*, Error>
  {
    return std::move(UniformBuffer<T>::new_pointer(m_gl));
  }

  [[nodiscard]] auto create_uniform_buffer_pointer(isize size) const& noexcept
    -> result::Result<graphics::UniformBuffer<void>*, Error> override;

  auto end_frame() & noexcept -> void override;

  [[nodiscard]] auto gl() const& noexcept -> Context&;

  [[nodiscard]] auto interface_context() const& noexcept -> InterfaceContext const&;

  auto make_current() const& noexcept -> void override;

  static auto new_(u16 height, InterfaceContext const& interface_context, u16 width) noexcept
    -> result::Result<GraphicsContext, Error>;

  static auto new_pointer(u16 height, InterfaceContext const& interface_context, u16 width) noexcept
    -> result::Result<GraphicsContext*, Error>;

  auto on_resize(u16 height, u16 width) & noexcept -> void override;

  auto set_camera(Camera* camera) & noexcept -> void override;

  auto set_clear_color(glm::vec4 const& color) & noexcept -> void override;

  auto set_resize_callback(std::function<void(u16, u16)> resize_callback) & noexcept -> void override;

protected:
  virtual auto debug_message_callback(GLenum source,
				      GLenum type,
				      GLuint id,
				      GLenum severity,
				      GLsizei length,
				      GLchar const* message) const& noexcept -> void;

private:
  Camera* m_camera = nullptr;
  Context& m_gl;
  Flags m_flags = Flags::HAS_RESIZE();
  u16 m_height = 0;
  InterfaceContext const& m_interface_context;
  std::function<void(u16, u16)> m_resize_callback = std::function<void(u16, u16)>();
  u16 m_width = 0;

  GraphicsContext(u16 height, InterfaceContext const& interface_context, u16 width) noexcept;

  static auto debug_message_callback(GLenum source,
				     GLenum type,
				     GLuint id,
				     GLenum severity,
				     GLsizei length,
				     GLchar const* message,
				     void const* userParam) noexcept -> void;

  auto drop() & noexcept -> void;

  auto initialize() & noexcept -> option::Option<Error>;
};
} // namespace engine::graphics::gl

ENGINE_BITFLAGS_HASH(engine::graphics::gl::GraphicsContext::Flags, engine::u8)

#include "engine/graphics/gl/wgl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_WGL_ARB_extensions_string_LOAD(name, type)                                                              \
  m_data_arb_extensions_string.name =                                                                                  \
    reinterpret_cast<type>(get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_WGL_ARB_extensions_string_FUNCTION(name, type, ...)                                                     \
  using namespace std::literals;                                                                                       \
  if (m_data_arb_extensions_string.name == nullptr) {                                                                  \
    ENGINE_WGL_ARB_extensions_string_LOAD(name, type)                                                                  \
  }                                                                                                                    \
  return m_data_arb_extensions_string.name(__VA_ARGS__);

auto
engine::graphics::gl::wgl::Context::getExtensionsStringARB(HDC hdc) & noexcept -> char const*
{
  ENGINE_WGL_ARB_extensions_string_FUNCTION(wglGetExtensionsStringARB, PFNWGLGETEXTENSIONSSTRINGARBPROC, hdc)
}

auto
engine::graphics::gl::wgl::Context::load_arb_extensions_string() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_WGL_ARB_extensions_string_LOAD(wglGetExtensionsStringARB, PFNWGLGETEXTENSIONSSTRINGARBPROC)
}

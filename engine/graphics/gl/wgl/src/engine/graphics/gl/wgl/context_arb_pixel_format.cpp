#include "engine/graphics/gl/wgl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_WGL_ARB_pixel_format_LOAD(name, type)                                                                   \
  m_data_arb_pixel_format.name = reinterpret_cast<type>(get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_WGL_ARB_pixel_format_FUNCTION(name, type, ...)                                                          \
  using namespace std::literals;                                                                                       \
  if (m_data_arb_pixel_format.name == nullptr) {                                                                       \
    ENGINE_WGL_ARB_pixel_format_LOAD(name, type)                                                                       \
  }                                                                                                                    \
  return m_data_arb_pixel_format.name(__VA_ARGS__);

auto
engine::graphics::gl::wgl::Context::getPixelFormatAttribivARB(HDC hdc,
							      int iPixelFormat,
							      int iLayerPlane,
							      UINT nAttributes,
							      int const* piAttributes,
							      int* piValues) & noexcept -> BOOL
{
  ENGINE_WGL_ARB_pixel_format_FUNCTION(wglGetPixelFormatAttribivARB,
				       PFNWGLGETPIXELFORMATATTRIBIVARBPROC,
				       hdc,
				       iPixelFormat,
				       iLayerPlane,
				       nAttributes,
				       piAttributes,
				       piValues)
}

auto
engine::graphics::gl::wgl::Context::getPixelFormatAttribfvARB(HDC hdc,
							      int iPixelFormat,
							      int iLayerPlane,
							      UINT nAttributes,
							      int const* piAttributes,
							      FLOAT* pfValues) & noexcept -> BOOL
{
  ENGINE_WGL_ARB_pixel_format_FUNCTION(wglGetPixelFormatAttribfvARB,
				       PFNWGLGETPIXELFORMATATTRIBFVARBPROC,
				       hdc,
				       iPixelFormat,
				       iLayerPlane,
				       nAttributes,
				       piAttributes,
				       pfValues)
}

auto
engine::graphics::gl::wgl::Context::choosePixelFormatARB(HDC hdc,
							 int const* piAttribIList,
							 FLOAT const* pfAttribFList,
							 UINT nMaxFormats,
							 int* piFormats,
							 UINT* nNumFormats) & noexcept -> BOOL
{
  ENGINE_WGL_ARB_pixel_format_FUNCTION(wglChoosePixelFormatARB,
				       PFNWGLCHOOSEPIXELFORMATARBPROC,
				       hdc,
				       piAttribIList,
				       pfAttribFList,
				       nMaxFormats,
				       piFormats,
				       nNumFormats)
}

auto
engine::graphics::gl::wgl::Context::load_arb_pixel_format() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_WGL_ARB_pixel_format_LOAD(wglGetPixelFormatAttribivARB, PFNWGLGETPIXELFORMATATTRIBIVARBPROC)
    ENGINE_WGL_ARB_pixel_format_LOAD(wglGetPixelFormatAttribfvARB, PFNWGLGETPIXELFORMATATTRIBFVARBPROC)
      ENGINE_WGL_ARB_pixel_format_LOAD(wglChoosePixelFormatARB, PFNWGLCHOOSEPIXELFORMATARBPROC)
}

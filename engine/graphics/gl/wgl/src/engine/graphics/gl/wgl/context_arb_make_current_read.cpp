#include "engine/graphics/gl/wgl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_WGL_ARB_make_current_read_LOAD(name, type)                                                              \
  m_data_arb_make_current_read.name =                                                                                  \
    reinterpret_cast<type>(get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_WGL_ARB_make_current_read_FUNCTION(name, type, ...)                                                     \
  using namespace std::literals;                                                                                       \
  if (m_data_arb_make_current_read.name == nullptr) {                                                                  \
    ENGINE_WGL_ARB_make_current_read_LOAD(name, type)                                                                  \
  }                                                                                                                    \
  return m_data_arb_make_current_read.name(__VA_ARGS__);

auto
engine::graphics::gl::wgl::Context::makeContextCurrentARB(HDC hDrawDC, HDC hReadDC, HGLRC hglrc) & noexcept -> BOOL
{
  ENGINE_WGL_ARB_make_current_read_FUNCTION(
    wglMakeContextCurrentARB, PFNWGLMAKECONTEXTCURRENTARBPROC, hDrawDC, hReadDC, hglrc)
}

auto
engine::graphics::gl::wgl::Context::getCurrentReadDCARB() & noexcept -> HDC
{
  ENGINE_WGL_ARB_make_current_read_FUNCTION(wglGetCurrentReadDCARB, PFNWGLGETCURRENTREADDCARBPROC)
}

auto
engine::graphics::gl::wgl::Context::load_arb_make_current_read() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_WGL_ARB_make_current_read_LOAD(wglMakeContextCurrentARB, PFNWGLMAKECONTEXTCURRENTARBPROC)
  ENGINE_WGL_ARB_make_current_read_LOAD(wglGetCurrentReadDCARB, PFNWGLGETCURRENTREADDCARBPROC)
}

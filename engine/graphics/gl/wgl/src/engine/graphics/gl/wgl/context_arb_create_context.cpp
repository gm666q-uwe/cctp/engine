#include "engine/graphics/gl/wgl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_WGL_ARB_create_context_LOAD(name, type)                                                                 \
  m_data_arb_create_context.name = reinterpret_cast<type>(get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_WGL_ARB_create_context_FUNCTION(name, type, ...)                                                        \
  using namespace std::literals;                                                                                       \
  if (m_data_arb_create_context.name == nullptr) [[unlikely]] {                                                        \
    ENGINE_WGL_ARB_create_context_LOAD(name, type)                                                                     \
  }                                                                                                                    \
  return m_data_arb_create_context.name(__VA_ARGS__);

auto
engine::graphics::gl::wgl::Context::createContextAttribsARB(HDC hDC,
							    HGLRC hShareContext,
							    int const* attribList) & noexcept -> HGLRC
{
  ENGINE_WGL_ARB_create_context_FUNCTION(
    wglCreateContextAttribsARB, PFNWGLCREATECONTEXTATTRIBSARBPROC, hDC, hShareContext, attribList)
}

auto
engine::graphics::gl::wgl::Context::load_arb_create_context() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_WGL_ARB_create_context_LOAD(wglCreateContextAttribsARB, PFNWGLCREATECONTEXTATTRIBSARBPROC)
}

#include "engine/graphics/gl/wgl/fake_context.hpp"

#include <memory>

auto
engine::graphics::gl::wgl::FakeContext::check_extensions(engine::CStr extensions) noexcept -> bool
{
  using namespace std::literals;

  auto wgl_arb_create_context = extensions.find("WGL_ARB_create_context"sv) != std::string_view::npos;
  auto wgl_arb_extensions_string = extensions.find("WGL_ARB_extensions_string"sv) != std::string_view::npos;
  auto wgl_arb_make_current_read = extensions.find("WGL_ARB_make_current_read"sv) != std::string_view::npos;
  auto wgl_arb_pixel_format = extensions.find("WGL_ARB_pixel_format"sv) != std::string_view::npos;
  auto wgl_ext_framebuffer_srgb = extensions.find("WGL_EXT_framebuffer_sRGB"sv) != std::string_view::npos;

  return wgl_arb_create_context && wgl_arb_extensions_string && wgl_arb_make_current_read && wgl_arb_pixel_format &&
	 wgl_ext_framebuffer_srgb;
}

auto
engine::graphics::gl::wgl::FakeContext::get_proc_address(std::string_view name) const& noexcept
  -> result::Result<void (*)(void), engine::Error>
{
  if (auto result = wglGetProcAddress(name.data());
      result == nullptr || result == reinterpret_cast<PROC>(1) || result == reinterpret_cast<PROC>(2) ||
      result == reinterpret_cast<PROC>(3) || result == reinterpret_cast<PROC>(-1)) {
    return std::move(result::Result<void (*)(void), Error>::Err(Error::new_(0, -1)));
  } else {
    return std::move(result::Result<void (*)(void), Error>::Ok(reinterpret_cast<void (*)()>(result)));
  }
}

auto
engine::graphics::gl::wgl::FakeContext::gl() const& noexcept -> engine::graphics::gl::Context&
{
  return *static_cast<gl::Context*>(nullptr);
}

auto
engine::graphics::gl::wgl::FakeContext::initialize(HDC device_context) & noexcept -> option::Option<engine::Error>
{
  constexpr auto const pixel_format_descriptor =
    PIXELFORMATDESCRIPTOR{ sizeof(PIXELFORMATDESCRIPTOR),
			   1,
			   PFD_DOUBLEBUFFER | PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL,
			   PFD_TYPE_RGBA,
			   24,
			   0,
			   0,
			   0,
			   0,
			   0,
			   0,
			   8,
			   0,
			   0,
			   0,
			   0,
			   0,
			   0,
			   24,
			   8,
			   0,
			   PFD_MAIN_PLANE,
			   0,
			   0,
			   0,
			   0 };

  auto pixel_format = ChoosePixelFormat(device_context, &pixel_format_descriptor);
  if (pixel_format == 0) {
    return option::Option<Error>::Some(Error::new_(0, GetLastError()));
  }

  if (SetPixelFormat(device_context, pixel_format, &pixel_format_descriptor) != TRUE) {
    return option::Option<Error>::Some(Error::new_(0, GetLastError()));
  }

  auto context = wglCreateContext(device_context);
  if (context == nullptr) {
    return option::Option<Error>::Some(Error::new_(0, GetLastError()));
  }

  wglMakeCurrent(device_context, context);

  load(false);
  auto result = is_loaded(false);
  if (result) {
    auto extensions = Context::getExtensionsStringARB(device_context);
    if (extensions != nullptr) {
      result = check_extensions(extensions);
    }
  }

  wglMakeCurrent(device_context, nullptr);
  wglDeleteContext(context);

  if (!result) {
    return option::Option<Error>::Some(Error::new_(0, GetLastError()));
  }

  return option::Option<Error>::None();
}

auto
engine::graphics::gl::wgl::FakeContext::is_current() const& noexcept -> bool
{
  return false;
}

auto
engine::graphics::gl::wgl::FakeContext::load(bool include_gl) & noexcept -> void
{
  load_arb_create_context();
  load_arb_extensions_string();
  load_arb_make_current_read();
  load_arb_pixel_format();
}

auto
engine::graphics::gl::wgl::FakeContext::make_current() const& noexcept -> result::Result<std::nullptr_t, engine::Error>
{
  return result::Result<std::nullptr_t, Error>::Err(Error::new_(0, -1));
}

auto
engine::graphics::gl::wgl::FakeContext::new_(HDC device_context) noexcept -> result::Result<FakeContext, engine::Error>
{
  auto self = FakeContext();
  if (auto result = self.initialize(device_context); result.is_some()) {
    return std::move(result::Result<FakeContext, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<FakeContext, Error>::Ok(std::move(self)));
}

auto
engine::graphics::gl::wgl::FakeContext::new_pointer(HDC device_context) noexcept
  -> result::Result<FakeContext*, engine::Error>
{
  auto self = std::unique_ptr<FakeContext>(new FakeContext());
  if (auto result = self->initialize(device_context); result.is_some()) {
    return std::move(result::Result<FakeContext*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<FakeContext*, Error>::Ok(self.release()));
}

auto
engine::graphics::gl::wgl::FakeContext::release_current() const& noexcept
  -> result::Result<std::nullptr_t, engine::Error>
{
  return result::Result<std::nullptr_t, Error>::Err(Error::new_(0, -1));
}

auto
engine::graphics::gl::wgl::FakeContext::swap_buffers() const& noexcept -> result::Result<std::nullptr_t, engine::Error>
{
  return result::Result<std::nullptr_t, Error>::Err(Error::new_(0, -1));
}

auto
engine::graphics::gl::wgl::FakeContext::swap_interval(engine::i32 interval) const& noexcept
  -> result::Result<std::nullptr_t, Error>
{
  return result::Result<std::nullptr_t, Error>::Err(Error::new_(0, -1));
}

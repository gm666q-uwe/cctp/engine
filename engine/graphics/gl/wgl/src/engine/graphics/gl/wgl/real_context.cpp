#include "engine/graphics/gl/wgl/real_context.hpp"

#include <iostream>

engine::graphics::gl::wgl::RealContext::RealContext(FakeContext const& context,
						    HDC device_context,
						    bool pre_load) noexcept
  : Context(context)
  , m_device_context(device_context)
  , m_gl(std::unique_ptr<gl::Context>(gl::Context::new_pointer(this, pre_load)))
{}

engine::graphics::gl::wgl::RealContext::RealContext(engine::graphics::gl::wgl::RealContext&& other) noexcept
  : Context(std::move(other))
  , m_context(std::exchange(other.m_context, m_context))
  , m_device_context(other.m_device_context)
  , m_gl(std::move(other.m_gl))
{
  m_gl->set_interface_context(&other, this);
}

engine::graphics::gl::wgl::RealContext::~RealContext() noexcept
{
  drop();
}

auto
engine::graphics::gl::wgl::RealContext::operator=(engine::graphics::gl::wgl::RealContext&& other) & noexcept
  -> engine::graphics::gl::wgl::RealContext&
{
  if (this == &Context::operator=(std::move(other))) {
    return *this;
  }

  drop();

  m_context = std::exchange(other.m_context, m_context);
  m_device_context = other.m_device_context;
  m_gl = std::move(other.m_gl);
  m_gl->set_interface_context(&other, this);

  return *this;
}

auto
engine::graphics::gl::wgl::RealContext::check_extensions(engine::CStr extensions) noexcept -> bool
{
  using namespace std::literals;

  auto wgl_arb_create_context = extensions.find("WGL_ARB_create_context"sv) != std::string_view::npos;
  auto wgl_arb_extensions_string = extensions.find("WGL_ARB_extensions_string"sv) != std::string_view::npos;
  auto wgl_arb_make_current_read = extensions.find("WGL_ARB_make_current_read"sv) != std::string_view::npos;
  auto wgl_arb_pixel_format = extensions.find("WGL_ARB_pixel_format"sv) != std::string_view::npos;
  auto wgl_ext_framebuffer_srgb = extensions.find("WGL_EXT_framebuffer_sRGB"sv) != std::string_view::npos;
  auto wgl_ext_swap_control = extensions.find("WGL_EXT_swap_control"sv) != std::string_view::npos;
  auto wgl_ext_swap_control_tear = extensions.find("WGL_EXT_swap_control_tear"sv) != std::string_view::npos;

  return wgl_arb_create_context && wgl_arb_extensions_string && wgl_arb_make_current_read && wgl_arb_pixel_format &&
	 wgl_ext_framebuffer_srgb && wgl_ext_swap_control && wgl_ext_swap_control_tear;
}

auto
engine::graphics::gl::wgl::RealContext::drop() & noexcept -> void
{
  if (is_current()) {
    release_current().unwrap();
  }
  if (m_context != nullptr) {
    wglDeleteContext(m_context);
    m_context = nullptr;
  }
}

auto
engine::graphics::gl::wgl::RealContext::get_proc_address(std::string_view name) const& noexcept
  -> result::Result<void (*)(void), engine::Error>
{
  if (auto result = wglGetProcAddress(name.data());
      result == nullptr || result == reinterpret_cast<PROC>(1) || result == reinterpret_cast<PROC>(2) ||
      result == reinterpret_cast<PROC>(3) || result == reinterpret_cast<PROC>(-1)) {
    auto module = LoadLibraryW(L"opengl32.dll");
    if (result = GetProcAddress(module, name.data()); result == nullptr) {
      return std::move(result::Result<void (*)(void), Error>::Err(Error::new_(0, -1)));
    } else {
      return std::move(result::Result<void (*)(void), Error>::Ok(reinterpret_cast<void (*)()>(result)));
    }
  } else {
    return std::move(result::Result<void (*)(void), Error>::Ok(reinterpret_cast<void (*)()>(result)));
  }
}

auto
engine::graphics::gl::wgl::RealContext::gl() const& noexcept -> engine::graphics::gl::Context&
{
  return *m_gl;
}

auto
engine::graphics::gl::wgl::RealContext::initialize() & noexcept -> option::Option<engine::Error>
{
  using namespace std::literals;
  // TODO: Check extensions

  auto pixel_format = static_cast<int>(0);
  auto pixel_format_count = static_cast<UINT>(0);
  auto pixel_format_int_attributes = std::array<int, 25>{ WGL_DRAW_TO_WINDOW_ARB,
							  TRUE,
							  WGL_ACCELERATION_ARB,
							  WGL_FULL_ACCELERATION_ARB,
							  WGL_TRANSPARENT_ARB,
							  TRUE,
							  WGL_SUPPORT_OPENGL_ARB,
							  TRUE,
							  WGL_DOUBLE_BUFFER_ARB,
							  TRUE,
							  WGL_PIXEL_TYPE_ARB,
							  WGL_TYPE_RGBA_ARB,
							  WGL_RED_BITS_ARB,
							  8,
							  WGL_GREEN_BITS_ARB,
							  8,
							  WGL_BLUE_BITS_ARB,
							  8,
							  WGL_ALPHA_BITS_ARB,
							  8,
							  WGL_DEPTH_BITS_ARB,
							  24,
							  WGL_FRAMEBUFFER_SRGB_CAPABLE_ARB,
							  TRUE,
							  0 };
  auto pixel_format_float_attributes = std::array<FLOAT, 1>{ 0.0f };
  choosePixelFormatARB(m_device_context,
		       pixel_format_int_attributes.data(),
		       pixel_format_float_attributes.data(),
		       1,
		       &pixel_format,
		       &pixel_format_count);

  if (pixel_format == 0 || pixel_format_count == 0) {
    return std::move(option::Option<Error>::Some(Error::new_(0, GetLastError())));
  }

  auto pixel_format_descriptor = PIXELFORMATDESCRIPTOR{};
  if (DescribePixelFormat(m_device_context, pixel_format, sizeof(PIXELFORMATDESCRIPTOR), &pixel_format_descriptor) ==
      0) {
    return std::move(option::Option<Error>::Some(Error::new_(0, GetLastError())));
  }

  if (SetPixelFormat(m_device_context, pixel_format, &pixel_format_descriptor) != TRUE) {
    return std::move(option::Option<Error>::Some(Error::new_(0, GetLastError())));
  }

  auto attributes =
    std::array<int, 9>{ WGL_CONTEXT_MAJOR_VERSION_ARB,
			4,
			WGL_CONTEXT_MINOR_VERSION_ARB,
			6,
			WGL_CONTEXT_FLAGS_ARB,
			config::is_debug ? WGL_CONTEXT_DEBUG_BIT_ARB | WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB
					 : WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
			WGL_CONTEXT_PROFILE_MASK_ARB,
			WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
			0 };
  m_context = createContextAttribsARB(m_device_context, nullptr, attributes.data());
  if (m_context == nullptr) {
    return std::move(option::Option<Error>::Some(Error::new_(0, GetLastError())));
  }

  if (auto result = make_current(); result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  }

  auto extensions = Context::getExtensionsStringARB(m_device_context);
  auto result = extensions != nullptr;
  if (result) {
    result = check_extensions(extensions);
  }
  load(true);

  if (auto result = release_current(); result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  }

  if (!result) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  return std::move(option::Option<Error>::None());
}

auto
engine::graphics::gl::wgl::RealContext::is_current() const& noexcept -> bool
{
  return m_context != nullptr && wglGetCurrentContext() == m_context;
}

auto
engine::graphics::gl::wgl::RealContext::load(bool include_gl) & noexcept -> void
{
  load_arb_create_context();
  load_arb_extensions_string();
  load_arb_make_current_read();
  load_arb_pixel_format();
  if (include_gl) {
    m_gl->load();
  }
}

auto
engine::graphics::gl::wgl::RealContext::make_current() const& noexcept -> result::Result<std::nullptr_t, engine::Error>
{
  if (const_cast<RealContext*>(this)->makeContextCurrentARB(m_device_context, m_device_context, m_context) == TRUE) {
    return std::move(result::Result<std::nullptr_t, Error>::Ok(nullptr));
  } else {
    return std::move(result::Result<std::nullptr_t, Error>::Err(Error::new_(0, -1)));
  }
}

auto
engine::graphics::gl::wgl::RealContext::new_(engine::graphics::gl::wgl::FakeContext const& context,
					     HDC device_context) noexcept -> result::Result<RealContext, engine::Error>
{
  auto self = RealContext(context, device_context, false);
  if (auto result = self.initialize(); result.is_some()) {
    return std::move(result::Result<RealContext, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<RealContext, Error>::Ok(std::move(self)));
}

auto
engine::graphics::gl::wgl::RealContext::new_pointer(engine::graphics::gl::wgl::FakeContext const& context,
						    HDC device_context) noexcept
  -> result::Result<RealContext*, engine::Error>
{
  auto* self = new RealContext(context, device_context, false);
  if (auto result = self->initialize(); result.is_some()) {
    delete self;
    return std::move(result::Result<RealContext*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<RealContext*, Error>::Ok(self));
}

auto
engine::graphics::gl::wgl::RealContext::release_current() const& noexcept
  -> result::Result<std::nullptr_t, engine::Error>
{
  if (const_cast<RealContext*>(this)->makeContextCurrentARB(m_device_context, m_device_context, nullptr) == TRUE) {
    return std::move(result::Result<std::nullptr_t, Error>::Ok(nullptr));
  } else {
    return std::move(result::Result<std::nullptr_t, Error>::Err(Error::new_(0, -1)));
  }
}

auto
engine::graphics::gl::wgl::RealContext::swap_buffers() const& noexcept -> result::Result<std::nullptr_t, engine::Error>
{
  if (!is_current()) {
    if (auto result = make_current(); result.is_err()) {
      return std::move(result);
    }
  }
  if (SwapBuffers(m_device_context) != TRUE) {
    return std::move(result::Result<std::nullptr_t, Error>::Err(Error::new_(0, -1)));
  }
  return std::move(result::Result<std::nullptr_t, Error>::Ok(nullptr));
}

auto
engine::graphics::gl::wgl::RealContext::swap_interval(engine::i32 interval) const& noexcept
  -> result::Result<std::nullptr_t, Error>
{
  return std::move(result::Result<std::nullptr_t, Error>::Err(Error::new_(0, -1)));
}

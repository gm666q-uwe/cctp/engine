#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/wgl/wgl.h"

namespace engine::graphics::gl::wgl {
struct ContextDataARBMakeCurrentRead : public IContextData
{
public:
  // WGL_ARB_make_current_read
  PFNWGLMAKECONTEXTCURRENTARBPROC wglMakeContextCurrentARB = nullptr;
  PFNWGLGETCURRENTREADDCARBPROC wglGetCurrentReadDCARB = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return wglMakeContextCurrentARB != nullptr && wglGetCurrentReadDCARB != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl::wgl

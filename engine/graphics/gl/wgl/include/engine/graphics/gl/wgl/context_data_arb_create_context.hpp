#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/wgl/wgl.h"

namespace engine::graphics::gl::wgl {
struct ContextDataARBCreateContext : public IContextData
{
public:
  // WGL_ARB_create_context
  PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return wglCreateContextAttribsARB != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl::wgl

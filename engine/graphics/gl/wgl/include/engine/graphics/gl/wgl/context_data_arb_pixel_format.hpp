#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/wgl/wgl.h"

namespace engine::graphics::gl::wgl {
struct ContextDataARBPixelFormat : public IContextData
{
public:
  // WGL_ARB_pixel_format
  PFNWGLGETPIXELFORMATATTRIBIVARBPROC wglGetPixelFormatAttribivARB = nullptr;
  PFNWGLGETPIXELFORMATATTRIBFVARBPROC wglGetPixelFormatAttribfvARB = nullptr;
  PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return wglGetPixelFormatAttribivARB != nullptr && wglGetPixelFormatAttribfvARB != nullptr &&
	   wglChoosePixelFormatARB != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl::wgl

#pragma once

#include <memory>

#include <engine/graphics/gl/context.hpp>

#include "engine/graphics/gl/wgl/fake_context.hpp"

namespace engine::graphics::gl::wgl {
class RealContext final : public Context
{
public:
  constexpr RealContext(RealContext const&) noexcept = delete;

  RealContext(RealContext&& other) noexcept;

  ~RealContext() noexcept override;

  constexpr auto operator=(RealContext const&) & noexcept -> RealContext& = delete;

  auto operator=(RealContext&& other) & noexcept -> RealContext&;

  [[nodiscard]] auto get_proc_address(std::string_view name) const& noexcept
    -> result::Result<void (*)(void), Error> override;

  [[nodiscard]] auto gl() const& noexcept -> gl::Context& override;

  [[nodiscard]] auto is_current() const& noexcept -> bool override;

  [[nodiscard]] constexpr auto is_loaded(bool include_gl) const& noexcept -> bool override
  {
    return (is_arb_create_context_loaded() && is_arb_extensions_string_loaded() && is_arb_make_current_read_loaded() &&
	    is_arb_pixel_format_loaded()) &&
	   (!include_gl || m_gl->is_loaded());
  }

  auto load(bool include_gl) & noexcept -> void override;

  [[nodiscard]] auto make_current() const& noexcept -> result::Result<std::nullptr_t, Error> override;

  static auto new_(FakeContext const& context, HDC device_context) noexcept
    -> result::Result<RealContext, Error>;

  static auto new_pointer(FakeContext const& context, HDC device_context) noexcept
    -> result::Result<RealContext*, Error>;

  [[nodiscard]] auto release_current() const& noexcept -> result::Result<std::nullptr_t, Error> override;

  [[nodiscard]] auto swap_buffers() const& noexcept -> result::Result<std::nullptr_t, Error> override;

  [[nodiscard]] auto swap_interval(i32 interval) const& noexcept -> result::Result<std::nullptr_t, Error> override;

protected:
private:
  HGLRC m_context = nullptr;
  HDC m_device_context = nullptr;
  std::unique_ptr<gl::Context> m_gl = std::unique_ptr<gl::Context>();

  RealContext(FakeContext const& context, HDC device_context,bool pre_load) noexcept;

  auto check_extensions(CStr extensions) noexcept -> bool;

  auto drop() & noexcept -> void;

  auto initialize() & noexcept -> option::Option<Error>;
};
} // namespace engine::graphics::gl::wgl

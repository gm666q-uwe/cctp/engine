#pragma once

#include <engine/graphics/gl/interface_context.hpp>

#include "engine/graphics/gl/wgl/wgl.h"

namespace engine::graphics::gl::wgl {
class IContext : public InterfaceContext
{
public:
  constexpr IContext() noexcept
    : InterfaceContext()
  {}

  constexpr IContext(IContext const&) noexcept = delete;

  constexpr IContext(IContext&& other) noexcept
    : InterfaceContext(std::move(other))
  {}

  ~IContext() noexcept override = default;

  constexpr virtual auto operator=(IContext const&) & noexcept -> IContext& = delete;

  constexpr auto operator=(IContext&& other) & noexcept -> IContext&
  {
    if (this == &InterfaceContext::operator=(std::move(other))) {
      return *this;
    }

    return *this;
  }

  [[nodiscard]] virtual constexpr auto is_arb_create_context_loaded() const& noexcept -> bool = 0;

  [[nodiscard]] virtual constexpr auto is_arb_extensions_string_loaded() const& noexcept -> bool = 0;

  [[nodiscard]] virtual constexpr auto is_arb_make_current_read_loaded() const& noexcept -> bool = 0;

  [[nodiscard]] virtual constexpr auto is_arb_pixel_format_loaded() const& noexcept -> bool = 0;

  virtual auto load_arb_create_context() & noexcept -> void = 0;

  virtual auto load_arb_extensions_string() & noexcept -> void = 0;

  virtual auto load_arb_make_current_read() & noexcept -> void = 0;

  virtual auto load_arb_pixel_format() & noexcept -> void = 0;

  // WGL_ARB_create_context
  virtual auto createContextAttribsARB(HDC hDC, HGLRC hShareContext, int const* attribList) & noexcept -> HGLRC = 0;

  // WGL_ARB_extensions_string
  virtual auto getExtensionsStringARB(HDC hdc) & noexcept -> char const* = 0;

  // WGL_ARB_make_current_read
  virtual auto makeContextCurrentARB(HDC hDrawDC, HDC hReadDC, HGLRC hglrc) & noexcept -> BOOL = 0;

  virtual auto getCurrentReadDCARB() & noexcept -> HDC = 0;

  // WGL_ARB_pixel_format
  virtual auto getPixelFormatAttribivARB(HDC hdc,
					 int iPixelFormat,
					 int iLayerPlane,
					 UINT nAttributes,
					 int const* piAttributes,
					 int* piValues) & noexcept -> BOOL = 0;

  virtual auto getPixelFormatAttribfvARB(HDC hdc,
					 int iPixelFormat,
					 int iLayerPlane,
					 UINT nAttributes,
					 int const* piAttributes,
					 FLOAT* pfValues) & noexcept -> BOOL = 0;

  virtual auto choosePixelFormatARB(HDC hdc,
				    int const* piAttribIList,
				    FLOAT const* pfAttribFList,
				    UINT nMaxFormats,
				    int* piFormats,
				    UINT* nNumFormats) & noexcept -> BOOL = 0;

protected:
private:
};
} // namespace engine::graphics::gl::wgl

#pragma once

#include "engine/graphics/gl/wgl/context_data_arb_create_context.hpp"
#include "engine/graphics/gl/wgl/context_data_arb_extensions_string.hpp"
#include "engine/graphics/gl/wgl/context_data_arb_make_current_read.hpp"
#include "engine/graphics/gl/wgl/context_data_arb_pixel_format.hpp"
#include "engine/graphics/gl/wgl/i_context.hpp"

namespace engine::graphics::gl::wgl {
class Context : public IContext
{
public:
  constexpr Context() noexcept
    : IContext()
  {}

  // constexpr Context(Context const&) noexcept = delete;

  constexpr Context(Context&& other) noexcept
    : IContext(std::move(other))
    , m_data_arb_create_context(std::move(other.m_data_arb_create_context))
    , m_data_arb_extensions_string(std::move(other.m_data_arb_extensions_string))
    , m_data_arb_make_current_read(std::move(other.m_data_arb_make_current_read))
    , m_data_arb_pixel_format(std::move(other.m_data_arb_pixel_format))
  {}

  ~Context() noexcept override = default;

  constexpr auto operator=(Context const&) & noexcept -> Context& = delete;

  constexpr auto operator=(Context&& other) & noexcept -> Context&
  {
    if (this == &other) {
      return *this;
    }

    m_data_arb_create_context = std::move(other.m_data_arb_create_context);
    m_data_arb_extensions_string = std::move(other.m_data_arb_extensions_string);
    m_data_arb_make_current_read = std::move(other.m_data_arb_make_current_read);
    m_data_arb_pixel_format = std::move(other.m_data_arb_pixel_format);

    IContext::operator=(std::move(other));
    return *this;
  }

  [[nodiscard]] constexpr auto is_arb_create_context_loaded() const& noexcept -> bool override
  {
    return m_data_arb_create_context.is_loaded();
  }

  [[nodiscard]] constexpr auto is_arb_extensions_string_loaded() const& noexcept -> bool override
  {
    return m_data_arb_extensions_string.is_loaded();
  }

  [[nodiscard]] constexpr auto is_arb_make_current_read_loaded() const& noexcept -> bool override
  {
    return m_data_arb_make_current_read.is_loaded();
  }

  [[nodiscard]] constexpr auto is_arb_pixel_format_loaded() const& noexcept -> bool override
  {
    return m_data_arb_pixel_format.is_loaded();
  }

  auto load_arb_create_context() & noexcept -> void override;

  auto load_arb_extensions_string() & noexcept -> void override;

  auto load_arb_make_current_read() & noexcept -> void override;

  auto load_arb_pixel_format() & noexcept -> void override;

  // WGL_ARB_create_context
  auto createContextAttribsARB(HDC hDC, HGLRC hShareContext, int const* attribList) & noexcept -> HGLRC override;

  // WGL_ARB_extensions_string
  auto getExtensionsStringARB(HDC hdc) & noexcept -> char const* override;

  // WGL_ARB_make_current_read
  auto makeContextCurrentARB(HDC hDrawDC, HDC hReadDC, HGLRC hglrc) & noexcept -> BOOL override;

  auto getCurrentReadDCARB() & noexcept -> HDC override;

  // WGL_ARB_pixel_format
  auto getPixelFormatAttribivARB(HDC hdc,
				 int iPixelFormat,
				 int iLayerPlane,
				 UINT nAttributes,
				 int const* piAttributes,
				 int* piValues) & noexcept -> BOOL override;

  auto getPixelFormatAttribfvARB(HDC hdc,
				 int iPixelFormat,
				 int iLayerPlane,
				 UINT nAttributes,
				 int const* piAttributes,
				 FLOAT* pfValues) & noexcept -> BOOL override;

  auto choosePixelFormatARB(HDC hdc,
			    int const* piAttribIList,
			    FLOAT const* pfAttribFList,
			    UINT nMaxFormats,
			    int* piFormats,
			    UINT* nNumFormats) & noexcept -> BOOL override;

protected:
  constexpr Context(Context const& other) noexcept
    : IContext()
    , m_data_arb_create_context(other.m_data_arb_create_context)
    , m_data_arb_extensions_string(other.m_data_arb_extensions_string)
    , m_data_arb_make_current_read(other.m_data_arb_make_current_read)
    , m_data_arb_pixel_format(other.m_data_arb_pixel_format)
  {}

  //auto bootstrap() & noexcept -> bool;

private:
  // WGL_ARB_create_context
  ContextDataARBCreateContext m_data_arb_create_context = {};
  // WGL_ARB_extensions_string
  ContextDataARBExtensionsString m_data_arb_extensions_string = {};
  // WGL_ARB_make_current_read
  ContextDataARBMakeCurrentRead m_data_arb_make_current_read = {};
  // WGL_ARB_pixel_format
  ContextDataARBPixelFormat m_data_arb_pixel_format = {};
};
} // namespace engine::graphics::gl::wgl

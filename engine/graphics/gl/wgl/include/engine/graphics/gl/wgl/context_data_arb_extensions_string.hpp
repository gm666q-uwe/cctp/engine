#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/wgl/wgl.h"

namespace engine::graphics::gl::wgl {
struct ContextDataARBExtensionsString : public IContextData
{
public:
  // WGL_ARB_extensions_string
  PFNWGLGETEXTENSIONSSTRINGARBPROC wglGetExtensionsStringARB = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return wglGetExtensionsStringARB != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl::wgl

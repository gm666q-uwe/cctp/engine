#pragma once

#include "engine/graphics/gl/wgl/context.hpp"

namespace engine::graphics::gl::wgl {
class FakeContext final : public Context
{
public:
  constexpr FakeContext(FakeContext const&) noexcept = delete;

  constexpr FakeContext(FakeContext&& other) noexcept
    : Context(std::move(other))
  {}

  ~FakeContext() noexcept override = default;

  constexpr auto operator=(FakeContext const&) & noexcept -> FakeContext& = delete;

  constexpr auto operator=(FakeContext&& other) & noexcept -> FakeContext&
  {
    if (this == &Context::operator=(std::move(other))) {
      return *this;
    }

    return *this;
  }

  [[nodiscard]] auto get_proc_address(std::string_view name) const& noexcept
    -> result::Result<void (*)(void), Error> override;

  [[nodiscard]] auto gl() const& noexcept -> gl::Context& override;

  [[nodiscard]] auto is_current() const& noexcept -> bool override;

  [[nodiscard]] constexpr auto is_loaded(bool include_gl) const& noexcept -> bool override
  {
    return (is_arb_create_context_loaded() && is_arb_extensions_string_loaded() && is_arb_make_current_read_loaded() &&
	    is_arb_pixel_format_loaded()) &&
	   !include_gl;
  }

  auto load(bool include_gl) & noexcept -> void override;

  [[nodiscard]] auto make_current() const& noexcept -> result::Result<std::nullptr_t, Error> override;

  static auto new_(HDC device_context) noexcept -> result::Result<FakeContext, Error>;

  static auto new_pointer(HDC device_context) noexcept -> result::Result<FakeContext*, Error>;

  [[nodiscard]] auto release_current() const& noexcept -> result::Result<std::nullptr_t, Error> override;

  [[nodiscard]] auto swap_buffers() const& noexcept -> result::Result<std::nullptr_t, Error> override;

  [[nodiscard]] auto swap_interval(i32 interval) const& noexcept -> result::Result<std::nullptr_t, Error> override;

protected:
private:
  constexpr FakeContext() noexcept
    : Context()
  {}

  auto check_extensions(CStr extensions) noexcept -> bool;

  auto initialize(HDC device_context) & noexcept -> option::Option<Error>;
};
} // namespace engine::graphics::gl::wgl

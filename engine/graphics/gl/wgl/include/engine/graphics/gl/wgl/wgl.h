#pragma once

#include "engine/graphics/gl/gl.h"

#if ENGINE_HAS_WGL
#include <GL/wgl.h>
#include <GL/wglext.h>
#endif

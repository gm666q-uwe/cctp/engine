#pragma once

#include <string_view>

#include <boost/preprocessor.hpp>

#include <result/result.hpp>

#include <engine/error.hpp>
#include <engine/graphics/gl/i_context_data.hpp>
#include <engine/graphics/gl/i_interface_context.hpp>
#include <engine/platform.h>

#include <GL/glcorearb.h>
#define __gl_h_

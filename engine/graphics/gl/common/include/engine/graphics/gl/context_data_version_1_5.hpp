#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/gl.h"

namespace engine::graphics::gl {
struct ContextDataVersion15 final : public IContextData
{
public:
  // GL_VERSION_1_5
  PFNGLGENQUERIESPROC glGenQueries = nullptr;
  PFNGLDELETEQUERIESPROC glDeleteQueries = nullptr;
  PFNGLISQUERYPROC glIsQuery = nullptr;
  PFNGLBEGINQUERYPROC glBeginQuery = nullptr;
  PFNGLENDQUERYPROC glEndQuery = nullptr;
  PFNGLGETQUERYIVPROC glGetQueryiv = nullptr;
  PFNGLGETQUERYOBJECTIVPROC glGetQueryObjectiv = nullptr;
  PFNGLGETQUERYOBJECTUIVPROC glGetQueryObjectuiv = nullptr;
  PFNGLBINDBUFFERPROC glBindBuffer = nullptr;
  PFNGLDELETEBUFFERSPROC glDeleteBuffers = nullptr;
  PFNGLGENBUFFERSPROC glGenBuffers = nullptr;
  PFNGLISBUFFERPROC glIsBuffer = nullptr;
  PFNGLBUFFERDATAPROC glBufferData = nullptr;
  PFNGLBUFFERSUBDATAPROC glBufferSubData = nullptr;
  PFNGLGETBUFFERSUBDATAPROC glGetBufferSubData = nullptr;
  PFNGLMAPBUFFERPROC glMapBuffer = nullptr;
  PFNGLUNMAPBUFFERPROC glUnmapBuffer = nullptr;
  PFNGLGETBUFFERPARAMETERIVPROC glGetBufferParameteriv = nullptr;
  PFNGLGETBUFFERPOINTERVPROC glGetBufferPointerv = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return glGenQueries != nullptr && glDeleteQueries != nullptr && glIsQuery != nullptr && glBeginQuery != nullptr &&
	   glEndQuery != nullptr && glGetQueryiv != nullptr && glGetQueryObjectiv != nullptr &&
	   glGetQueryObjectuiv != nullptr && glBindBuffer != nullptr && glDeleteBuffers != nullptr &&
	   glGenBuffers != nullptr && glIsBuffer != nullptr && glBufferData != nullptr && glBufferSubData != nullptr &&
	   glGetBufferSubData != nullptr && glMapBuffer != nullptr && glUnmapBuffer != nullptr &&
	   glGetBufferParameteriv != nullptr && glGetBufferPointerv != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl

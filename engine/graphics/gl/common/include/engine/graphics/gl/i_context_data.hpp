#pragma once

namespace engine::graphics::gl {
class IContextData
{
public:
  constexpr IContextData() noexcept = default;

  constexpr IContextData(IContextData const&) noexcept = default;

  constexpr IContextData(IContextData&&) noexcept = default;

  virtual constexpr ~IContextData() noexcept = default;

  constexpr auto operator=(IContextData const&) & noexcept -> IContextData& = default;

  constexpr auto operator=(IContextData&&) & noexcept -> IContextData& = default;

  [[nodiscard]] virtual constexpr auto is_loaded() const& noexcept -> bool = 0;

protected:
private:
};
} // namespace engine::graphics::gl

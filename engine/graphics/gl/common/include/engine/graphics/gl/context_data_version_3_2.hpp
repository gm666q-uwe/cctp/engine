#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/gl.h"

namespace engine::graphics::gl {
struct ContextDataVersion32 final : public IContextData
{
public:
  // GL_VERSION_3_2
  PFNGLDRAWELEMENTSBASEVERTEXPROC glDrawElementsBaseVertex = nullptr;
  PFNGLDRAWRANGEELEMENTSBASEVERTEXPROC glDrawRangeElementsBaseVertex = nullptr;
  PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXPROC glDrawElementsInstancedBaseVertex = nullptr;
  PFNGLMULTIDRAWELEMENTSBASEVERTEXPROC glMultiDrawElementsBaseVertex = nullptr;
  PFNGLPROVOKINGVERTEXPROC glProvokingVertex = nullptr;
  PFNGLFENCESYNCPROC glFenceSync = nullptr;
  PFNGLISSYNCPROC glIsSync = nullptr;
  PFNGLDELETESYNCPROC glDeleteSync = nullptr;
  PFNGLCLIENTWAITSYNCPROC glClientWaitSync = nullptr;
  PFNGLWAITSYNCPROC glWaitSync = nullptr;
  PFNGLGETINTEGER64VPROC glGetInteger64v = nullptr;
  PFNGLGETSYNCIVPROC glGetSynciv = nullptr;
  PFNGLGETINTEGER64I_VPROC glGetInteger64i_v = nullptr;
  PFNGLGETBUFFERPARAMETERI64VPROC glGetBufferParameteri64v = nullptr;
  PFNGLFRAMEBUFFERTEXTUREPROC glFramebufferTexture = nullptr;
  PFNGLTEXIMAGE2DMULTISAMPLEPROC glTexImage2DMultisample = nullptr;
  PFNGLTEXIMAGE3DMULTISAMPLEPROC glTexImage3DMultisample = nullptr;
  PFNGLGETMULTISAMPLEFVPROC glGetMultisamplefv = nullptr;
  PFNGLSAMPLEMASKIPROC glSampleMaski = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return glDrawElementsBaseVertex != nullptr && glDrawRangeElementsBaseVertex != nullptr &&
	   glDrawElementsInstancedBaseVertex != nullptr && glMultiDrawElementsBaseVertex != nullptr &&
	   glProvokingVertex != nullptr && glFenceSync != nullptr && glIsSync != nullptr && glDeleteSync != nullptr &&
	   glClientWaitSync != nullptr && glWaitSync != nullptr && glGetInteger64v != nullptr &&
	   glGetSynciv != nullptr && glGetInteger64i_v != nullptr && glGetBufferParameteri64v != nullptr &&
	   glFramebufferTexture != nullptr && glTexImage2DMultisample != nullptr &&
	   glTexImage3DMultisample != nullptr && glGetMultisamplefv != nullptr && glSampleMaski != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl

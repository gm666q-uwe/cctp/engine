#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/gl.h"

namespace engine::graphics::gl {
struct ContextDataVersion43 final : public IContextData
{
public:
  // GL_VERSION_4_3
  PFNGLCLEARBUFFERDATAPROC glClearBufferData = nullptr;
  PFNGLCLEARBUFFERSUBDATAPROC glClearBufferSubData = nullptr;
  PFNGLDISPATCHCOMPUTEPROC glDispatchCompute = nullptr;
  PFNGLDISPATCHCOMPUTEINDIRECTPROC glDispatchComputeIndirect = nullptr;
  PFNGLCOPYIMAGESUBDATAPROC glCopyImageSubData = nullptr;
  PFNGLFRAMEBUFFERPARAMETERIPROC glFramebufferParameteri = nullptr;
  PFNGLGETFRAMEBUFFERPARAMETERIVPROC glGetFramebufferParameteriv = nullptr;
  PFNGLGETINTERNALFORMATI64VPROC glGetInternalformati64v = nullptr;
  PFNGLINVALIDATETEXSUBIMAGEPROC glInvalidateTexSubImage = nullptr;
  PFNGLINVALIDATETEXIMAGEPROC glInvalidateTexImage = nullptr;
  PFNGLINVALIDATEBUFFERSUBDATAPROC glInvalidateBufferSubData = nullptr;
  PFNGLINVALIDATEBUFFERDATAPROC glInvalidateBufferData = nullptr;
  PFNGLINVALIDATEFRAMEBUFFERPROC glInvalidateFramebuffer = nullptr;
  PFNGLINVALIDATESUBFRAMEBUFFERPROC glInvalidateSubFramebuffer = nullptr;
  PFNGLMULTIDRAWARRAYSINDIRECTPROC glMultiDrawArraysIndirect = nullptr;
  PFNGLMULTIDRAWELEMENTSINDIRECTPROC glMultiDrawElementsIndirect = nullptr;
  PFNGLGETPROGRAMINTERFACEIVPROC glGetProgramInterfaceiv = nullptr;
  PFNGLGETPROGRAMRESOURCEINDEXPROC glGetProgramResourceIndex = nullptr;
  PFNGLGETPROGRAMRESOURCENAMEPROC glGetProgramResourceName = nullptr;
  PFNGLGETPROGRAMRESOURCEIVPROC glGetProgramResourceiv = nullptr;
  PFNGLGETPROGRAMRESOURCELOCATIONPROC glGetProgramResourceLocation = nullptr;
  PFNGLGETPROGRAMRESOURCELOCATIONINDEXPROC glGetProgramResourceLocationIndex = nullptr;
  PFNGLSHADERSTORAGEBLOCKBINDINGPROC glShaderStorageBlockBinding = nullptr;
  PFNGLTEXBUFFERRANGEPROC glTexBufferRange = nullptr;
  PFNGLTEXSTORAGE2DMULTISAMPLEPROC glTexStorage2DMultisample = nullptr;
  PFNGLTEXSTORAGE3DMULTISAMPLEPROC glTexStorage3DMultisample = nullptr;
  PFNGLTEXTUREVIEWPROC glTextureView = nullptr;
  PFNGLBINDVERTEXBUFFERPROC glBindVertexBuffer = nullptr;
  PFNGLVERTEXATTRIBFORMATPROC glVertexAttribFormat = nullptr;
  PFNGLVERTEXATTRIBIFORMATPROC glVertexAttribIFormat = nullptr;
  PFNGLVERTEXATTRIBLFORMATPROC glVertexAttribLFormat = nullptr;
  PFNGLVERTEXATTRIBBINDINGPROC glVertexAttribBinding = nullptr;
  PFNGLVERTEXBINDINGDIVISORPROC glVertexBindingDivisor = nullptr;
  PFNGLDEBUGMESSAGECONTROLPROC glDebugMessageControl = nullptr;
  PFNGLDEBUGMESSAGEINSERTPROC glDebugMessageInsert = nullptr;
  PFNGLDEBUGMESSAGECALLBACKPROC glDebugMessageCallback = nullptr;
  PFNGLGETDEBUGMESSAGELOGPROC glGetDebugMessageLog = nullptr;
  PFNGLPUSHDEBUGGROUPPROC glPushDebugGroup = nullptr;
  PFNGLPOPDEBUGGROUPPROC glPopDebugGroup = nullptr;
  PFNGLOBJECTLABELPROC glObjectLabel = nullptr;
  PFNGLGETOBJECTLABELPROC glGetObjectLabel = nullptr;
  PFNGLOBJECTPTRLABELPROC glObjectPtrLabel = nullptr;
  PFNGLGETOBJECTPTRLABELPROC glGetObjectPtrLabel = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return glClearBufferData != nullptr && glClearBufferSubData != nullptr && glDispatchCompute != nullptr &&
	   glDispatchComputeIndirect != nullptr && glCopyImageSubData != nullptr &&
	   glFramebufferParameteri != nullptr && glGetFramebufferParameteriv != nullptr &&
	   glGetInternalformati64v != nullptr && glInvalidateTexSubImage != nullptr &&
	   glInvalidateTexImage != nullptr && glInvalidateBufferSubData != nullptr &&
	   glInvalidateBufferData != nullptr && glInvalidateFramebuffer != nullptr &&
	   glInvalidateSubFramebuffer != nullptr && glMultiDrawArraysIndirect != nullptr &&
	   glMultiDrawElementsIndirect != nullptr && glGetProgramInterfaceiv != nullptr &&
	   glGetProgramResourceIndex != nullptr && glGetProgramResourceName != nullptr &&
	   glGetProgramResourceiv != nullptr && glGetProgramResourceLocation != nullptr &&
	   glGetProgramResourceLocationIndex != nullptr && glShaderStorageBlockBinding != nullptr &&
	   glTexBufferRange != nullptr && glTexStorage2DMultisample != nullptr &&
	   glTexStorage3DMultisample != nullptr && glTextureView != nullptr && glBindVertexBuffer != nullptr &&
	   glVertexAttribFormat != nullptr && glVertexAttribIFormat != nullptr && glVertexAttribLFormat != nullptr &&
	   glVertexAttribBinding != nullptr && glVertexBindingDivisor != nullptr && glDebugMessageControl != nullptr &&
	   glDebugMessageInsert != nullptr && glDebugMessageCallback != nullptr && glGetDebugMessageLog != nullptr &&
	   glPushDebugGroup != nullptr && glPopDebugGroup != nullptr && glObjectLabel != nullptr &&
	   glGetObjectLabel != nullptr && glObjectPtrLabel != nullptr && glGetObjectPtrLabel != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl

#pragma once

#include <engine/graphics/gl/interface_context.hpp>

#include "engine/graphics/gl/context_data_version_1_0.hpp"
#include "engine/graphics/gl/context_data_version_1_1.hpp"
#include "engine/graphics/gl/context_data_version_1_2.hpp"
#include "engine/graphics/gl/context_data_version_1_3.hpp"
#include "engine/graphics/gl/context_data_version_1_4.hpp"
#include "engine/graphics/gl/context_data_version_1_5.hpp"
#include "engine/graphics/gl/context_data_version_2_0.hpp"
#include "engine/graphics/gl/context_data_version_2_1.hpp"
#include "engine/graphics/gl/context_data_version_3_0.hpp"
#include "engine/graphics/gl/context_data_version_3_1.hpp"
#include "engine/graphics/gl/context_data_version_3_2.hpp"
#include "engine/graphics/gl/context_data_version_3_3.hpp"
#include "engine/graphics/gl/context_data_version_4_0.hpp"
#include "engine/graphics/gl/context_data_version_4_1.hpp"
#include "engine/graphics/gl/context_data_version_4_2.hpp"
#include "engine/graphics/gl/context_data_version_4_3.hpp"
#include "engine/graphics/gl/context_data_version_4_4.hpp"
#include "engine/graphics/gl/context_data_version_4_5.hpp"
#include "engine/graphics/gl/context_data_version_4_6.hpp"

namespace engine::graphics::gl {
#if ENGINE_HAS_GLX
namespace glx {
class Context;
} // namespace glx
#endif
#if ENGINE_HAS_EGL
namespace egl {
class Context;
} // namespace egl
#endif
#if ENGINE_HAS_WGL
namespace wgl {
class RealContext;
} // namespace wgl
#endif

class Context
{
#if ENGINE_HAS_GLX
  friend glx::Context;
#endif
#if ENGINE_HAS_EGL
  friend egl::Context;
#endif
#if ENGINE_HAS_WGL
  friend wgl::RealContext;
#endif

public:
  constexpr Context() noexcept = delete;

  constexpr Context(Context const&) noexcept = default;

  constexpr Context(Context&& other) noexcept = default;

  virtual constexpr ~Context() noexcept = default;

  constexpr auto operator=(Context const&) & noexcept -> Context& = default;

  constexpr auto operator=(Context&& other) & noexcept -> Context& = default;

  // GL_VERSION_1_0
  auto cullFace(GLenum mode) & noexcept -> void;

  auto frontFace(GLenum mode) & noexcept -> void;

  auto hint(GLenum target, GLenum mode) & noexcept -> void;

  auto lineWidth(GLfloat width) & noexcept -> void;

  auto pointSize(GLfloat size) & noexcept -> void;

  auto polygonMode(GLenum face, GLenum mode) & noexcept -> void;

  auto scissor(GLint x, GLint y, GLsizei width, GLsizei height) & noexcept -> void;

  auto texParameterf(GLenum target, GLenum pname, GLfloat param) & noexcept -> void;

  auto texParameterfv(GLenum target, GLenum pname, GLfloat const* params) & noexcept -> void;

  auto texParameteri(GLenum target, GLenum pname, GLint param) & noexcept -> void;

  auto texParameteriv(GLenum target, GLenum pname, GLint const* params) & noexcept -> void;

  auto texImage1D(GLenum target,
		  GLint level,
		  GLint internalformat,
		  GLsizei width,
		  GLint border,
		  GLenum format,
		  GLenum type,
		  void const* pixels) & noexcept -> void;

  auto texImage2D(GLenum target,
		  GLint level,
		  GLint internalformat,
		  GLsizei width,
		  GLsizei height,
		  GLint border,
		  GLenum format,
		  GLenum type,
		  void const* pixels) & noexcept -> void;

  auto drawBuffer(GLenum buf) & noexcept -> void;

  auto clear(GLbitfield mask) & noexcept -> void;

  auto clearColor(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha) & noexcept -> void;

  auto clearStencil(GLint s) & noexcept -> void;

  auto clearDepth(GLdouble depth) & noexcept -> void;

  auto stencilMask(GLuint mask) & noexcept -> void;

  auto colorMask(GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha) & noexcept -> void;

  auto depthMask(GLboolean flag) & noexcept -> void;

  auto disable(GLenum cap) & noexcept -> void;

  auto enable(GLenum cap) & noexcept -> void;

  auto finish() & noexcept -> void;

  auto flush() & noexcept -> void;

  auto blendFunc(GLenum sfactor, GLenum dfactor) & noexcept -> void;

  auto logicOp(GLenum opcode) & noexcept -> void;

  auto stencilFunc(GLenum func, GLint ref, GLuint mask) & noexcept -> void;

  auto stencilOp(GLenum fail, GLenum zfail, GLenum zpass) & noexcept -> void;

  auto depthFunc(GLenum func) & noexcept -> void;

  auto pixelStoref(GLenum pname, GLfloat param) & noexcept -> void;

  auto pixelStorei(GLenum pname, GLint param) & noexcept -> void;

  auto readBuffer(GLenum src) & noexcept -> void;

  auto readPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, void* pixels) & noexcept
    -> void;

  auto getBooleanv(GLenum pname, GLboolean* data) & noexcept -> void;

  auto getDoublev(GLenum pname, GLdouble* data) & noexcept -> void;

  auto getError() & noexcept -> GLenum;

  auto getFloatv(GLenum pname, GLfloat* data) & noexcept -> void;

  auto getIntegerv(GLenum pname, GLint* data) & noexcept -> void;

  auto getString(GLenum name) & noexcept -> GLubyte const*;

  auto getTexImage(GLenum target, GLint level, GLenum format, GLenum type, void* pixels) & noexcept -> void;

  auto getTexParameterfv(GLenum target, GLenum pname, GLfloat* params) & noexcept -> void;

  auto getTexParameteriv(GLenum target, GLenum pname, GLint* params) & noexcept -> void;

  auto getTexLevelParameterfv(GLenum target, GLint level, GLenum pname, GLfloat* params) & noexcept -> void;

  auto getTexLevelParameteriv(GLenum target, GLint level, GLenum pname, GLint* params) & noexcept -> void;

  auto isEnabled(GLenum cap) & noexcept -> GLboolean;

  auto depthRange(GLdouble n, GLdouble f) & noexcept -> void;

  auto viewport(GLint x, GLint y, GLsizei width, GLsizei height) & noexcept -> void;

  // GL_VERSION_1_1
  auto drawArrays(GLenum mode, GLint first, GLsizei count) & noexcept -> void;

  auto drawElements(GLenum mode, GLsizei count, GLenum type, void const* indices) & noexcept -> void;

  auto getPointerv(GLenum pname, void** params) & noexcept -> void;

  auto polygonOffset(GLfloat factor, GLfloat units) & noexcept -> void;

  auto copyTexImage1D(GLenum target,
		      GLint level,
		      GLenum internalformat,
		      GLint x,
		      GLint y,
		      GLsizei width,
		      GLint border) & noexcept -> void;

  auto copyTexImage2D(GLenum target,
		      GLint level,
		      GLenum internalformat,
		      GLint x,
		      GLint y,
		      GLsizei width,
		      GLsizei height,
		      GLint border) & noexcept -> void;

  auto copyTexSubImage1D(GLenum target, GLint level, GLint xoffset, GLint x, GLint y, GLsizei width) & noexcept -> void;

  auto copyTexSubImage2D(GLenum target,
			 GLint level,
			 GLint xoffset,
			 GLint yoffset,
			 GLint x,
			 GLint y,
			 GLsizei width,
			 GLsizei height) & noexcept -> void;

  auto texSubImage1D(GLenum target,
		     GLint level,
		     GLint xoffset,
		     GLsizei width,
		     GLenum format,
		     GLenum type,
		     void const* pixels) & noexcept -> void;

  auto texSubImage2D(GLenum target,
		     GLint level,
		     GLint xoffset,
		     GLint yoffset,
		     GLsizei width,
		     GLsizei height,
		     GLenum format,
		     GLenum type,
		     void const* pixels) & noexcept -> void;

  auto bindTexture(GLenum target, GLuint texture) & noexcept -> void;

  auto deleteTextures(GLsizei n, GLuint const* textures) & noexcept -> void;

  auto genTextures(GLsizei n, GLuint* textures) & noexcept -> void;

  auto isTexture(GLuint texture) & noexcept -> GLboolean;

  // GL_VERSION_1_2
  auto drawRangeElements(GLenum mode,
			 GLuint start,
			 GLuint end,
			 GLsizei count,
			 GLenum type,
			 void const* indices) & noexcept -> void;

  auto texImage3D(GLenum target,
		  GLint level,
		  GLint internalformat,
		  GLsizei width,
		  GLsizei height,
		  GLsizei depth,
		  GLint border,
		  GLenum format,
		  GLenum type,
		  void const* pixels) & noexcept -> void;

  auto texSubImage3D(GLenum target,
		     GLint level,
		     GLint xoffset,
		     GLint yoffset,
		     GLint zoffset,
		     GLsizei width,
		     GLsizei height,
		     GLsizei depth,
		     GLenum format,
		     GLenum type,
		     void const* pixels) & noexcept -> void;

  auto copyTexSubImage3D(GLenum target,
			 GLint level,
			 GLint xoffset,
			 GLint yoffset,
			 GLint zoffset,
			 GLint x,
			 GLint y,
			 GLsizei width,
			 GLsizei height) & noexcept -> void;

  // GL_VERSION_1_3
  auto activeTexture(GLenum texture) & noexcept -> void;

  auto sampleCoverage(GLfloat value, GLboolean invert) & noexcept -> void;

  auto compressedTexImage3D(GLenum target,
			    GLint level,
			    GLenum internalformat,
			    GLsizei width,
			    GLsizei height,
			    GLsizei depth,
			    GLint border,
			    GLsizei imageSize,
			    void const* data) & noexcept -> void;

  auto compressedTexImage2D(GLenum target,
			    GLint level,
			    GLenum internalformat,
			    GLsizei width,
			    GLsizei height,
			    GLint border,
			    GLsizei imageSize,
			    void const* data) & noexcept -> void;

  auto compressedTexImage1D(GLenum target,
			    GLint level,
			    GLenum internalformat,
			    GLsizei width,
			    GLint border,
			    GLsizei imageSize,
			    void const* data) & noexcept -> void;

  auto compressedTexSubImage3D(GLenum target,
			       GLint level,
			       GLint xoffset,
			       GLint yoffset,
			       GLint zoffset,
			       GLsizei width,
			       GLsizei height,
			       GLsizei depth,
			       GLenum format,
			       GLsizei imageSize,
			       void const* data) & noexcept -> void;

  auto compressedTexSubImage2D(GLenum target,
			       GLint level,
			       GLint xoffset,
			       GLint yoffset,
			       GLsizei width,
			       GLsizei height,
			       GLenum format,
			       GLsizei imageSize,
			       void const* data) & noexcept -> void;

  auto compressedTexSubImage1D(GLenum target,
			       GLint level,
			       GLint xoffset,
			       GLsizei width,
			       GLenum format,
			       GLsizei imageSize,
			       void const* data) & noexcept -> void;

  auto getCompressedTexImage(GLenum target, GLint level, void* img) & noexcept -> void;

  // GL_VERSION_1_4
  auto blendFuncSeparate(GLenum sfactorRGB, GLenum dfactorRGB, GLenum sfactorAlpha, GLenum dfactorAlpha) & noexcept
    -> void;

  auto multiDrawArrays(GLenum mode, GLint const* first, GLsizei const* count, GLsizei drawcount) & noexcept -> void;

  auto multiDrawElements(GLenum mode,
			 GLsizei const* count,
			 GLenum type,
			 void const* const* indices,
			 GLsizei drawcount) & noexcept -> void;

  auto pointParameterf(GLenum pname, GLfloat param) & noexcept -> void;

  auto pointParameterfv(GLenum pname, GLfloat const* params) & noexcept -> void;

  auto pointParameteri(GLenum pname, GLint param) & noexcept -> void;

  auto pointParameteriv(GLenum pname, GLint const* params) & noexcept -> void;

  auto blendColor(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha) & noexcept -> void;

  auto blendEquation(GLenum mode) & noexcept -> void;

  // GL_VERSION_1_5
  auto genQueries(GLsizei n, GLuint* ids) & noexcept -> void;

  auto deleteQueries(GLsizei n, GLuint const* ids) & noexcept -> void;

  auto isQuery(GLuint id) & noexcept -> GLboolean;

  auto beginQuery(GLenum target, GLuint id) & noexcept -> void;

  auto endQuery(GLenum target) & noexcept -> void;

  auto getQueryiv(GLenum target, GLenum pname, GLint* params) & noexcept -> void;

  auto getQueryObjectiv(GLuint id, GLenum pname, GLint* params) & noexcept -> void;

  auto getQueryObjectuiv(GLuint id, GLenum pname, GLuint* params) & noexcept -> void;

  auto bindBuffer(GLenum target, GLuint buffer) & noexcept -> void;

  auto deleteBuffers(GLsizei n, GLuint const* buffers) & noexcept -> void;

  auto genBuffers(GLsizei n, GLuint* buffers) & noexcept -> void;

  auto isBuffer(GLuint buffer) & noexcept -> GLboolean;

  auto bufferData(GLenum target, GLsizeiptr size, void const* data, GLenum usage) & noexcept -> void;

  auto bufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, void const* data) & noexcept -> void;

  auto getBufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, void* data) & noexcept -> void;

  auto mapBuffer(GLenum target, GLenum access) & noexcept -> void*;

  auto unmapBuffer(GLenum target) & noexcept -> GLboolean;

  auto getBufferParameteriv(GLenum target, GLenum pname, GLint* params) & noexcept -> void;

  auto getBufferPointerv(GLenum target, GLenum pname, void** params) & noexcept -> void;

  // GL_VERSION_2_0
  auto blendEquationSeparate(GLenum modeRGB, GLenum modeAlpha) & noexcept -> void;

  auto drawBuffers(GLsizei n, GLenum const* bufs) & noexcept -> void;

  auto stencilOpSeparate(GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass) & noexcept -> void;

  auto stencilFuncSeparate(GLenum face, GLenum func, GLint ref, GLuint mask) & noexcept -> void;

  auto stencilMaskSeparate(GLenum face, GLuint mask) & noexcept -> void;

  auto attachShader(GLuint program, GLuint shader) & noexcept -> void;

  auto bindAttribLocation(GLuint program, GLuint index, GLchar const* name) & noexcept -> void;

  auto compileShader(GLuint shader) & noexcept -> void;

  auto createProgram() & noexcept -> GLuint;

  auto createShader(GLenum type) & noexcept -> GLuint;

  auto deleteProgram(GLuint program) & noexcept -> void;

  auto deleteShader(GLuint shader) & noexcept -> void;

  auto detachShader(GLuint program, GLuint shader) & noexcept -> void;

  auto disableVertexAttribArray(GLuint index) & noexcept -> void;

  auto enableVertexAttribArray(GLuint index) & noexcept -> void;

  auto getActiveAttrib(GLuint program,
		       GLuint index,
		       GLsizei bufSize,
		       GLsizei* length,
		       GLint* size,
		       GLenum* type,
		       GLchar* name) & noexcept -> void;

  auto getActiveUniform(GLuint program,
			GLuint index,
			GLsizei bufSize,
			GLsizei* length,
			GLint* size,
			GLenum* type,
			GLchar* name) & noexcept -> void;

  auto getAttachedShaders(GLuint program, GLsizei maxCount, GLsizei* count, GLuint* shaders) & noexcept -> void;

  auto getAttribLocation(GLuint program, GLchar const* name) & noexcept -> GLint;

  auto getProgramiv(GLuint program, GLenum pname, GLint* params) & noexcept -> void;

  auto getProgramInfoLog(GLuint program, GLsizei bufSize, GLsizei* length, GLchar* infoLog) & noexcept -> void;

  auto getShaderiv(GLuint shader, GLenum pname, GLint* params) & noexcept -> void;

  auto getShaderInfoLog(GLuint shader, GLsizei bufSize, GLsizei* length, GLchar* infoLog) & noexcept -> void;

  auto getShaderSource(GLuint shader, GLsizei bufSize, GLsizei* length, GLchar* source) & noexcept -> void;

  auto getUniformLocation(GLuint program, GLchar const* name) & noexcept -> GLint;

  auto getUniformfv(GLuint program, GLint location, GLfloat* params) & noexcept -> void;

  auto getUniformiv(GLuint program, GLint location, GLint* params) & noexcept -> void;

  auto getVertexAttribdv(GLuint index, GLenum pname, GLdouble* params) & noexcept -> void;

  auto getVertexAttribfv(GLuint index, GLenum pname, GLfloat* params) & noexcept -> void;

  auto getVertexAttribiv(GLuint index, GLenum pname, GLint* params) & noexcept -> void;

  auto getVertexAttribPointerv(GLuint index, GLenum pname, void** pointer) & noexcept -> void;

  auto isProgram(GLuint program) & noexcept -> GLboolean;

  auto isShader(GLuint shader) & noexcept -> GLboolean;

  auto linkProgram(GLuint program) & noexcept -> void;

  auto shaderSource(GLuint shader, GLsizei count, GLchar const* const* string, GLint const* length) & noexcept -> void;

  auto useProgram(GLuint program) & noexcept -> void;

  auto uniform1f(GLint location, GLfloat v0) & noexcept -> void;

  auto uniform2f(GLint location, GLfloat v0, GLfloat v1) & noexcept -> void;

  auto uniform3f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2) & noexcept -> void;

  auto uniform4f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3) & noexcept -> void;

  auto uniform1i(GLint location, GLint v0) & noexcept -> void;

  auto uniform2i(GLint location, GLint v0, GLint v1) & noexcept -> void;

  auto uniform3i(GLint location, GLint v0, GLint v1, GLint v2) & noexcept -> void;

  auto uniform4i(GLint location, GLint v0, GLint v1, GLint v2, GLint v3) & noexcept -> void;

  auto uniform1fv(GLint location, GLsizei count, GLfloat const* value) & noexcept -> void;

  auto uniform2fv(GLint location, GLsizei count, GLfloat const* value) & noexcept -> void;

  auto uniform3fv(GLint location, GLsizei count, GLfloat const* value) & noexcept -> void;

  auto uniform4fv(GLint location, GLsizei count, GLfloat const* value) & noexcept -> void;

  auto uniform1iv(GLint location, GLsizei count, GLint const* value) & noexcept -> void;

  auto uniform2iv(GLint location, GLsizei count, GLint const* value) & noexcept -> void;

  auto uniform3iv(GLint location, GLsizei count, GLint const* value) & noexcept -> void;

  auto uniform4iv(GLint location, GLsizei count, GLint const* value) & noexcept -> void;

  auto uniformMatrix2fv(GLint location, GLsizei count, GLboolean transpose, GLfloat const* value) & noexcept -> void;

  auto uniformMatrix3fv(GLint location, GLsizei count, GLboolean transpose, GLfloat const* value) & noexcept -> void;

  auto uniformMatrix4fv(GLint location, GLsizei count, GLboolean transpose, GLfloat const* value) & noexcept -> void;

  auto validateProgram(GLuint program) & noexcept -> void;

  auto vertexAttrib1d(GLuint index, GLdouble x) & noexcept -> void;

  auto vertexAttrib1dv(GLuint index, GLdouble const* v) & noexcept -> void;

  auto vertexAttrib1f(GLuint index, GLfloat x) & noexcept -> void;

  auto vertexAttrib1fv(GLuint index, GLfloat const* v) & noexcept -> void;

  auto vertexAttrib1s(GLuint index, GLshort x) & noexcept -> void;

  auto vertexAttrib1sv(GLuint index, GLshort const* v) & noexcept -> void;

  auto vertexAttrib2d(GLuint index, GLdouble x, GLdouble y) & noexcept -> void;

  auto vertexAttrib2dv(GLuint index, GLdouble const* v) & noexcept -> void;

  auto vertexAttrib2f(GLuint index, GLfloat x, GLfloat y) & noexcept -> void;

  auto vertexAttrib2fv(GLuint index, GLfloat const* v) & noexcept -> void;

  auto vertexAttrib2s(GLuint index, GLshort x, GLshort y) & noexcept -> void;

  auto vertexAttrib2sv(GLuint index, GLshort const* v) & noexcept -> void;

  auto vertexAttrib3d(GLuint index, GLdouble x, GLdouble y, GLdouble z) & noexcept -> void;

  auto vertexAttrib3dv(GLuint index, GLdouble const* v) & noexcept -> void;

  auto vertexAttrib3f(GLuint index, GLfloat x, GLfloat y, GLfloat z) & noexcept -> void;

  auto vertexAttrib3fv(GLuint index, GLfloat const* v) & noexcept -> void;

  auto vertexAttrib3s(GLuint index, GLshort x, GLshort y, GLshort z) & noexcept -> void;

  auto vertexAttrib3sv(GLuint index, GLshort const* v) & noexcept -> void;

  auto vertexAttrib4Nbv(GLuint index, GLbyte const* v) & noexcept -> void;

  auto vertexAttrib4Niv(GLuint index, GLint const* v) & noexcept -> void;

  auto vertexAttrib4Nsv(GLuint index, GLshort const* v) & noexcept -> void;

  auto vertexAttrib4Nub(GLuint index, GLubyte x, GLubyte y, GLubyte z, GLubyte w) & noexcept -> void;

  auto vertexAttrib4Nubv(GLuint index, GLubyte const* v) & noexcept -> void;

  auto vertexAttrib4Nuiv(GLuint index, GLuint const* v) & noexcept -> void;

  auto vertexAttrib4Nusv(GLuint index, GLushort const* v) & noexcept -> void;

  auto vertexAttrib4bv(GLuint index, GLbyte const* v) & noexcept -> void;

  auto vertexAttrib4d(GLuint index, GLdouble x, GLdouble y, GLdouble z, GLdouble w) & noexcept -> void;

  auto vertexAttrib4dv(GLuint index, GLdouble const* v) & noexcept -> void;

  auto vertexAttrib4f(GLuint index, GLfloat x, GLfloat y, GLfloat z, GLfloat w) & noexcept -> void;

  auto vertexAttrib4fv(GLuint index, GLfloat const* v) & noexcept -> void;

  auto vertexAttrib4iv(GLuint index, GLint const* v) & noexcept -> void;

  auto vertexAttrib4s(GLuint index, GLshort x, GLshort y, GLshort z, GLshort w) & noexcept -> void;

  auto vertexAttrib4sv(GLuint index, GLshort const* v) & noexcept -> void;

  auto vertexAttrib4ubv(GLuint index, GLubyte const* v) & noexcept -> void;

  auto vertexAttrib4uiv(GLuint index, GLuint const* v) & noexcept -> void;

  auto vertexAttrib4usv(GLuint index, GLushort const* v) & noexcept -> void;

  auto vertexAttribPointer(GLuint index,
			   GLint size,
			   GLenum type,
			   GLboolean normalized,
			   GLsizei stride,
			   void const* pointer) & noexcept -> void;

  // GL_VERSION_2_1
  auto uniformMatrix2x3fv(GLint location, GLsizei count, GLboolean transpose, GLfloat const* value) & noexcept -> void;

  auto uniformMatrix3x2fv(GLint location, GLsizei count, GLboolean transpose, GLfloat const* value) & noexcept -> void;

  auto uniformMatrix2x4fv(GLint location, GLsizei count, GLboolean transpose, GLfloat const* value) & noexcept -> void;

  auto uniformMatrix4x2fv(GLint location, GLsizei count, GLboolean transpose, GLfloat const* value) & noexcept -> void;

  auto uniformMatrix3x4fv(GLint location, GLsizei count, GLboolean transpose, GLfloat const* value) & noexcept -> void;

  auto uniformMatrix4x3fv(GLint location, GLsizei count, GLboolean transpose, GLfloat const* value) & noexcept -> void;

  // GL_VERSION_3_0
  auto colorMaski(GLuint index, GLboolean r, GLboolean g, GLboolean b, GLboolean a) & noexcept -> void;

  auto getBooleani_v(GLenum target, GLuint index, GLboolean* data) & noexcept -> void;

  auto getIntegeri_v(GLenum target, GLuint index, GLint* data) & noexcept -> void;

  auto enablei(GLenum target, GLuint index) & noexcept -> void;

  auto disablei(GLenum target, GLuint index) & noexcept -> void;

  auto isEnabledi(GLenum target, GLuint index) & noexcept -> GLboolean;

  auto beginTransformFeedback(GLenum primitiveMode) & noexcept -> void;

  auto endTransformFeedback() & noexcept -> void;

  auto bindBufferRange(GLenum target, GLuint index, GLuint buffer, GLintptr offset, GLsizeiptr size) & noexcept -> void;

  auto bindBufferBase(GLenum target, GLuint index, GLuint buffer) & noexcept -> void;

  auto transformFeedbackVaryings(GLuint program,
				 GLsizei count,
				 GLchar const* const* varyings,
				 GLenum bufferMode) & noexcept -> void;

  auto getTransformFeedbackVarying(GLuint program,
				   GLuint index,
				   GLsizei bufSize,
				   GLsizei* length,
				   GLsizei* size,
				   GLenum* type,
				   GLchar* name) & noexcept -> void;

  auto clampColor(GLenum target, GLenum clamp) & noexcept -> void;

  auto beginConditionalRender(GLuint id, GLenum mode) & noexcept -> void;

  auto endConditionalRender() & noexcept -> void;

  auto vertexAttribIPointer(GLuint index, GLint size, GLenum type, GLsizei stride, void const* pointer) & noexcept
    -> void;

  auto getVertexAttribIiv(GLuint index, GLenum pname, GLint* params) & noexcept -> void;

  auto getVertexAttribIuiv(GLuint index, GLenum pname, GLuint* params) & noexcept -> void;

  auto vertexAttribI1i(GLuint index, GLint x) & noexcept -> void;

  auto vertexAttribI2i(GLuint index, GLint x, GLint y) & noexcept -> void;

  auto vertexAttribI3i(GLuint index, GLint x, GLint y, GLint z) & noexcept -> void;

  auto vertexAttribI4i(GLuint index, GLint x, GLint y, GLint z, GLint w) & noexcept -> void;

  auto vertexAttribI1ui(GLuint index, GLuint x) & noexcept -> void;

  auto vertexAttribI2ui(GLuint index, GLuint x, GLuint y) & noexcept -> void;

  auto vertexAttribI3ui(GLuint index, GLuint x, GLuint y, GLuint z) & noexcept -> void;

  auto vertexAttribI4ui(GLuint index, GLuint x, GLuint y, GLuint z, GLuint w) & noexcept -> void;

  auto vertexAttribI1iv(GLuint index, GLint const* v) & noexcept -> void;

  auto vertexAttribI2iv(GLuint index, GLint const* v) & noexcept -> void;

  auto vertexAttribI3iv(GLuint index, GLint const* v) & noexcept -> void;

  auto vertexAttribI4iv(GLuint index, GLint const* v) & noexcept -> void;

  auto vertexAttribI1uiv(GLuint index, GLuint const* v) & noexcept -> void;

  auto vertexAttribI2uiv(GLuint index, GLuint const* v) & noexcept -> void;

  auto vertexAttribI3uiv(GLuint index, GLuint const* v) & noexcept -> void;

  auto vertexAttribI4uiv(GLuint index, GLuint const* v) & noexcept -> void;

  auto vertexAttribI4bv(GLuint index, GLbyte const* v) & noexcept -> void;

  auto vertexAttribI4sv(GLuint index, GLshort const* v) & noexcept -> void;

  auto vertexAttribI4ubv(GLuint index, GLubyte const* v) & noexcept -> void;

  auto vertexAttribI4usv(GLuint index, GLushort const* v) & noexcept -> void;

  auto getUniformuiv(GLuint program, GLint location, GLuint* params) & noexcept -> void;

  auto bindFragDataLocation(GLuint program, GLuint color, GLchar const* name) & noexcept -> void;

  auto getFragDataLocation(GLuint program, GLchar const* name) & noexcept -> GLint;

  auto uniform1ui(GLint location, GLuint v0) & noexcept -> void;

  auto uniform2ui(GLint location, GLuint v0, GLuint v1) & noexcept -> void;

  auto uniform3ui(GLint location, GLuint v0, GLuint v1, GLuint v2) & noexcept -> void;

  auto uniform4ui(GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3) & noexcept -> void;

  auto uniform1uiv(GLint location, GLsizei count, GLuint const* value) & noexcept -> void;

  auto uniform2uiv(GLint location, GLsizei count, GLuint const* value) & noexcept -> void;

  auto uniform3uiv(GLint location, GLsizei count, GLuint const* value) & noexcept -> void;

  auto uniform4uiv(GLint location, GLsizei count, GLuint const* value) & noexcept -> void;

  auto texParameterIiv(GLenum target, GLenum pname, GLint const* params) & noexcept -> void;

  auto texParameterIuiv(GLenum target, GLenum pname, GLuint const* params) & noexcept -> void;

  auto getTexParameterIiv(GLenum target, GLenum pname, GLint* params) & noexcept -> void;

  auto getTexParameterIuiv(GLenum target, GLenum pname, GLuint* params) & noexcept -> void;

  auto clearBufferiv(GLenum buffer, GLint drawbuffer, GLint const* value) & noexcept -> void;

  auto clearBufferuiv(GLenum buffer, GLint drawbuffer, GLuint const* value) & noexcept -> void;

  auto clearBufferfv(GLenum buffer, GLint drawbuffer, GLfloat const* value) & noexcept -> void;

  auto clearBufferfi(GLenum buffer, GLint drawbuffer, GLfloat depth, GLint stencil) & noexcept -> void;

  auto getStringi(GLenum name, GLuint index) & noexcept -> GLubyte const*;

  auto isRenderbuffer(GLuint renderbuffer) & noexcept -> GLboolean;

  auto bindRenderbuffer(GLenum target, GLuint renderbuffer) & noexcept -> void;

  auto deleteRenderbuffers(GLsizei n, GLuint const* renderbuffers) & noexcept -> void;

  auto genRenderbuffers(GLsizei n, GLuint* renderbuffers) & noexcept -> void;

  auto renderbufferStorage(GLenum target, GLenum internalformat, GLsizei width, GLsizei height) & noexcept -> void;

  auto getRenderbufferParameteriv(GLenum target, GLenum pname, GLint* params) & noexcept -> void;

  auto isFramebuffer(GLuint framebuffer) & noexcept -> GLboolean;

  auto bindFramebuffer(GLenum target, GLuint framebuffer) & noexcept -> void;

  auto deleteFramebuffers(GLsizei n, GLuint const* framebuffers) & noexcept -> void;

  auto genFramebuffers(GLsizei n, GLuint* framebuffers) & noexcept -> void;

  auto checkFramebufferStatus(GLenum target) & noexcept -> GLenum;

  auto framebufferTexture1D(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level) & noexcept
    -> void;

  auto framebufferTexture2D(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level) & noexcept
    -> void;

  auto framebufferTexture3D(GLenum target,
			    GLenum attachment,
			    GLenum textarget,
			    GLuint texture,
			    GLint level,
			    GLint zoffset) & noexcept -> void;

  auto framebufferRenderbuffer(GLenum target,
			       GLenum attachment,
			       GLenum renderbuffertarget,
			       GLuint renderbuffer) & noexcept -> void;

  auto getFramebufferAttachmentParameteriv(GLenum target, GLenum attachment, GLenum pname, GLint* params) & noexcept
    -> void;

  auto generateMipmap(GLenum target) & noexcept -> void;

  auto blitFramebuffer(GLint srcX0,
		       GLint srcY0,
		       GLint srcX1,
		       GLint srcY1,
		       GLint dstX0,
		       GLint dstY0,
		       GLint dstX1,
		       GLint dstY1,
		       GLbitfield mask,
		       GLenum filter) & noexcept -> void;

  auto renderbufferStorageMultisample(GLenum target,
				      GLsizei samples,
				      GLenum internalformat,
				      GLsizei width,
				      GLsizei height) & noexcept -> void;

  auto framebufferTextureLayer(GLenum target, GLenum attachment, GLuint texture, GLint level, GLint layer) & noexcept
    -> void;

  auto mapBufferRange(GLenum target, GLintptr offset, GLsizeiptr length, GLbitfield access) & noexcept -> void*;

  auto flushMappedBufferRange(GLenum target, GLintptr offset, GLsizeiptr length) & noexcept -> void;

  auto bindVertexArray(GLuint array) & noexcept -> void;

  auto deleteVertexArrays(GLsizei n, GLuint const* arrays) & noexcept -> void;

  auto genVertexArrays(GLsizei n, GLuint* arrays) & noexcept -> void;

  auto isVertexArray(GLuint array) & noexcept -> GLboolean;

  // GL_VERSION_3_1
  auto drawArraysInstanced(GLenum mode, GLint first, GLsizei count, GLsizei instancecount) & noexcept -> void;

  auto drawElementsInstanced(GLenum mode,
			     GLsizei count,
			     GLenum type,
			     void const* indices,
			     GLsizei instancecount) & noexcept -> void;

  auto texBuffer(GLenum target, GLenum internalformat, GLuint buffer) & noexcept -> void;

  auto primitiveRestartIndex(GLuint index) & noexcept -> void;

  auto copyBufferSubData(GLenum readTarget,
			 GLenum writeTarget,
			 GLintptr readOffset,
			 GLintptr writeOffset,
			 GLsizeiptr size) & noexcept -> void;

  auto getUniformIndices(GLuint program,
			 GLsizei uniformCount,
			 GLchar const* const* uniformNames,
			 GLuint* uniformIndices) & noexcept -> void;

  auto getActiveUniformsiv(GLuint program,
			   GLsizei uniformCount,
			   GLuint const* uniformIndices,
			   GLenum pname,
			   GLint* params) & noexcept -> void;

  auto getActiveUniformName(GLuint program,
			    GLuint uniformIndex,
			    GLsizei bufSize,
			    GLsizei* length,
			    GLchar* uniformName) & noexcept -> void;

  auto getUniformBlockIndex(GLuint program, GLchar const* uniformBlockName) & noexcept -> GLuint;

  auto getActiveUniformBlockiv(GLuint program, GLuint uniformBlockIndex, GLenum pname, GLint* params) & noexcept
    -> void;

  auto getActiveUniformBlockName(GLuint program,
				 GLuint uniformBlockIndex,
				 GLsizei bufSize,
				 GLsizei* length,
				 GLchar* uniformBlockName) & noexcept -> void;

  auto uniformBlockBinding(GLuint program, GLuint uniformBlockIndex, GLuint uniformBlockBinding) & noexcept -> void;

  // GL_VERSION_3_2
  auto drawElementsBaseVertex(GLenum mode, GLsizei count, GLenum type, void const* indices, GLint basevertex) & noexcept
    -> void;

  auto drawRangeElementsBaseVertex(GLenum mode,
				   GLuint start,
				   GLuint end,
				   GLsizei count,
				   GLenum type,
				   void const* indices,
				   GLint basevertex) & noexcept -> void;

  auto drawElementsInstancedBaseVertex(GLenum mode,
				       GLsizei count,
				       GLenum type,
				       void const* indices,
				       GLsizei instancecount,
				       GLint basevertex) & noexcept -> void;

  auto multiDrawElementsBaseVertex(GLenum mode,
				   GLsizei const* count,
				   GLenum type,
				   void const* const* indices,
				   GLsizei drawcount,
				   GLint const* basevertex) & noexcept -> void;

  auto provokingVertex(GLenum mode) & noexcept -> void;

  auto fenceSync(GLenum condition, GLbitfield flags) & noexcept -> GLsync;

  auto isSync(GLsync sync) & noexcept -> GLboolean;

  auto deleteSync(GLsync sync) & noexcept -> void;

  auto clientWaitSync(GLsync sync, GLbitfield flags, GLuint64 timeout) & noexcept -> GLenum;

  auto waitSync(GLsync sync, GLbitfield flags, GLuint64 timeout) & noexcept -> void;

  auto getInteger64v(GLenum pname, GLint64* data) & noexcept -> void;

  auto getSynciv(GLsync sync, GLenum pname, GLsizei bufSize, GLsizei* length, GLint* values) & noexcept -> void;

  auto getInteger64i_v(GLenum target, GLuint index, GLint64* data) & noexcept -> void;

  auto getBufferParameteri64v(GLenum target, GLenum pname, GLint64* params) & noexcept -> void;

  auto framebufferTexture(GLenum target, GLenum attachment, GLuint texture, GLint level) & noexcept -> void;

  auto texImage2DMultisample(GLenum target,
			     GLsizei samples,
			     GLenum internalformat,
			     GLsizei width,
			     GLsizei height,
			     GLboolean fixedsamplelocations) & noexcept -> void;

  auto texImage3DMultisample(GLenum target,
			     GLsizei samples,
			     GLenum internalformat,
			     GLsizei width,
			     GLsizei height,
			     GLsizei depth,
			     GLboolean fixedsamplelocations) & noexcept -> void;

  auto getMultisamplefv(GLenum pname, GLuint index, GLfloat* val) & noexcept -> void;

  auto sampleMaski(GLuint maskNumber, GLbitfield mask) & noexcept -> void;

  // GL_VERSION_3_3
  auto bindFragDataLocationIndexed(GLuint program, GLuint colorNumber, GLuint index, GLchar const* name) & noexcept
    -> void;

  auto getFragDataIndex(GLuint program, GLchar const* name) & noexcept -> GLint;

  auto genSamplers(GLsizei count, GLuint* samplers) & noexcept -> void;

  auto deleteSamplers(GLsizei count, GLuint const* samplers) & noexcept -> void;

  auto isSampler(GLuint sampler) & noexcept -> GLboolean;

  auto bindSampler(GLuint unit, GLuint sampler) & noexcept -> void;

  auto samplerParameteri(GLuint sampler, GLenum pname, GLint param) & noexcept -> void;

  auto samplerParameteriv(GLuint sampler, GLenum pname, GLint const* param) & noexcept -> void;

  auto samplerParameterf(GLuint sampler, GLenum pname, GLfloat param) & noexcept -> void;

  auto samplerParameterfv(GLuint sampler, GLenum pname, GLfloat const* param) & noexcept -> void;

  auto samplerParameterIiv(GLuint sampler, GLenum pname, GLint const* param) & noexcept -> void;

  auto samplerParameterIuiv(GLuint sampler, GLenum pname, GLuint const* param) & noexcept -> void;

  auto getSamplerParameteriv(GLuint sampler, GLenum pname, GLint* params) & noexcept -> void;

  auto getSamplerParameterIiv(GLuint sampler, GLenum pname, GLint* params) & noexcept -> void;

  auto getSamplerParameterfv(GLuint sampler, GLenum pname, GLfloat* params) & noexcept -> void;

  auto getSamplerParameterIuiv(GLuint sampler, GLenum pname, GLuint* params) & noexcept -> void;

  auto queryCounter(GLuint id, GLenum target) & noexcept -> void;

  auto getQueryObjecti64v(GLuint id, GLenum pname, GLint64* params) & noexcept -> void;

  auto getQueryObjectui64v(GLuint id, GLenum pname, GLuint64* params) & noexcept -> void;

  auto vertexAttribDivisor(GLuint index, GLuint divisor) & noexcept -> void;

  auto vertexAttribP1ui(GLuint index, GLenum type, GLboolean normalized, GLuint value) & noexcept -> void;

  auto vertexAttribP1uiv(GLuint index, GLenum type, GLboolean normalized, GLuint const* value) & noexcept -> void;

  auto vertexAttribP2ui(GLuint index, GLenum type, GLboolean normalized, GLuint value) & noexcept -> void;

  auto vertexAttribP2uiv(GLuint index, GLenum type, GLboolean normalized, GLuint const* value) & noexcept -> void;

  auto vertexAttribP3ui(GLuint index, GLenum type, GLboolean normalized, GLuint value) & noexcept -> void;

  auto vertexAttribP3uiv(GLuint index, GLenum type, GLboolean normalized, GLuint const* value) & noexcept -> void;

  auto vertexAttribP4ui(GLuint index, GLenum type, GLboolean normalized, GLuint value) & noexcept -> void;

  auto vertexAttribP4uiv(GLuint index, GLenum type, GLboolean normalized, GLuint const* value) & noexcept -> void;

  // GL_VERSION_4_0
  auto minSampleShading(GLfloat value) & noexcept -> void;

  auto blendEquationi(GLuint buf, GLenum mode) & noexcept -> void;

  auto blendEquationSeparatei(GLuint buf, GLenum modeRGB, GLenum modeAlpha) & noexcept -> void;

  auto blendFunci(GLuint buf, GLenum src, GLenum dst) & noexcept -> void;

  auto blendFuncSeparatei(GLuint buf, GLenum srcRGB, GLenum dstRGB, GLenum srcAlpha, GLenum dstAlpha) & noexcept
    -> void;

  auto drawArraysIndirect(GLenum mode, void const* indirect) & noexcept -> void;

  auto drawElementsIndirect(GLenum mode, GLenum type, void const* indirect) & noexcept -> void;

  auto uniform1d(GLint location, GLdouble x) & noexcept -> void;

  auto uniform2d(GLint location, GLdouble x, GLdouble y) & noexcept -> void;

  auto uniform3d(GLint location, GLdouble x, GLdouble y, GLdouble z) & noexcept -> void;

  auto uniform4d(GLint location, GLdouble x, GLdouble y, GLdouble z, GLdouble w) & noexcept -> void;

  auto uniform1dv(GLint location, GLsizei count, GLdouble const* value) & noexcept -> void;

  auto uniform2dv(GLint location, GLsizei count, GLdouble const* value) & noexcept -> void;

  auto uniform3dv(GLint location, GLsizei count, GLdouble const* value) & noexcept -> void;

  auto uniform4dv(GLint location, GLsizei count, GLdouble const* value) & noexcept -> void;

  auto uniformMatrix2dv(GLint location, GLsizei count, GLboolean transpose, GLdouble const* value) & noexcept -> void;

  auto uniformMatrix3dv(GLint location, GLsizei count, GLboolean transpose, GLdouble const* value) & noexcept -> void;

  auto uniformMatrix4dv(GLint location, GLsizei count, GLboolean transpose, GLdouble const* value) & noexcept -> void;

  auto uniformMatrix2x3dv(GLint location, GLsizei count, GLboolean transpose, GLdouble const* value) & noexcept -> void;

  auto uniformMatrix2x4dv(GLint location, GLsizei count, GLboolean transpose, GLdouble const* value) & noexcept -> void;

  auto uniformMatrix3x2dv(GLint location, GLsizei count, GLboolean transpose, GLdouble const* value) & noexcept -> void;

  auto uniformMatrix3x4dv(GLint location, GLsizei count, GLboolean transpose, GLdouble const* value) & noexcept -> void;

  auto uniformMatrix4x2dv(GLint location, GLsizei count, GLboolean transpose, GLdouble const* value) & noexcept -> void;

  auto uniformMatrix4x3dv(GLint location, GLsizei count, GLboolean transpose, GLdouble const* value) & noexcept -> void;

  auto getUniformdv(GLuint program, GLint location, GLdouble* params) & noexcept -> void;

  auto getSubroutineUniformLocation(GLuint program, GLenum shadertype, GLchar const* name) & noexcept -> GLint;

  auto getSubroutineIndex(GLuint program, GLenum shadertype, GLchar const* name) & noexcept -> GLuint;

  auto getActiveSubroutineUniformiv(GLuint program,
				    GLenum shadertype,
				    GLuint index,
				    GLenum pname,
				    GLint* values) & noexcept -> void;

  auto getActiveSubroutineUniformName(GLuint program,
				      GLenum shadertype,
				      GLuint index,
				      GLsizei bufsize,
				      GLsizei* length,
				      GLchar* name) & noexcept -> void;

  auto getActiveSubroutineName(GLuint program,
			       GLenum shadertype,
			       GLuint index,
			       GLsizei bufsize,
			       GLsizei* length,
			       GLchar* name) & noexcept -> void;

  auto uniformSubroutinesuiv(GLenum shadertype, GLsizei count, GLuint const* indices) & noexcept -> void;

  auto getUniformSubroutineuiv(GLenum shadertype, GLint location, GLuint* params) & noexcept -> void;

  auto getProgramStageiv(GLuint program, GLenum shadertype, GLenum pname, GLint* values) & noexcept -> void;

  auto patchParameteri(GLenum pname, GLint value) & noexcept -> void;

  auto patchParameterfv(GLenum pname, GLfloat const* values) & noexcept -> void;

  auto bindTransformFeedback(GLenum target, GLuint id) & noexcept -> void;

  auto deleteTransformFeedbacks(GLsizei n, GLuint const* ids) & noexcept -> void;

  auto genTransformFeedbacks(GLsizei n, GLuint* ids) & noexcept -> void;

  auto isTransformFeedback(GLuint id) & noexcept -> GLboolean;

  auto pauseTransformFeedback() & noexcept -> void;

  auto resumeTransformFeedback() & noexcept -> void;

  auto drawTransformFeedback(GLenum mode, GLuint id) & noexcept -> void;

  auto drawTransformFeedbackStream(GLenum mode, GLuint id, GLuint stream) & noexcept -> void;

  auto beginQueryIndexed(GLenum target, GLuint index, GLuint id) & noexcept -> void;

  auto endQueryIndexed(GLenum target, GLuint index) & noexcept -> void;

  auto getQueryIndexediv(GLenum target, GLuint index, GLenum pname, GLint* params) & noexcept -> void;

  // GL_VERSION_4_1
  auto releaseShaderCompiler() & noexcept -> void;

  auto shaderBinary(GLsizei count,
		    GLuint const* shaders,
		    GLenum binaryformat,
		    void const* binary,
		    GLsizei length) & noexcept -> void;

  auto getShaderPrecisionFormat(GLenum shadertype, GLenum precisiontype, GLint* range, GLint* precision) & noexcept
    -> void;

  auto depthRangef(GLfloat n, GLfloat f) & noexcept -> void;

  auto clearDepthf(GLfloat d) & noexcept -> void;

  auto getProgramBinary(GLuint program, GLsizei bufSize, GLsizei* length, GLenum* binaryFormat, void* binary) & noexcept
    -> void;

  auto programBinary(GLuint program, GLenum binaryFormat, void const* binary, GLsizei length) & noexcept -> void;

  auto programParameteri(GLuint program, GLenum pname, GLint value) & noexcept -> void;

  auto useProgramStages(GLuint pipeline, GLbitfield stages, GLuint program) & noexcept -> void;

  auto activeShaderProgram(GLuint pipeline, GLuint program) & noexcept -> void;

  auto createShaderProgramv(GLenum type, GLsizei count, GLchar const* const* strings) & noexcept -> GLuint;

  auto bindProgramPipeline(GLuint pipeline) & noexcept -> void;

  auto deleteProgramPipelines(GLsizei n, GLuint const* pipelines) & noexcept -> void;

  auto genProgramPipelines(GLsizei n, GLuint* pipelines) & noexcept -> void;

  auto isProgramPipeline(GLuint pipeline) & noexcept -> GLboolean;

  auto getProgramPipelineiv(GLuint pipeline, GLenum pname, GLint* params) & noexcept -> void;

  auto programUniform1i(GLuint program, GLint location, GLint v0) & noexcept -> void;

  auto programUniform1iv(GLuint program, GLint location, GLsizei count, GLint const* value) & noexcept -> void;

  auto programUniform1f(GLuint program, GLint location, GLfloat v0) & noexcept -> void;

  auto programUniform1fv(GLuint program, GLint location, GLsizei count, GLfloat const* value) & noexcept -> void;

  auto programUniform1d(GLuint program, GLint location, GLdouble v0) & noexcept -> void;

  auto programUniform1dv(GLuint program, GLint location, GLsizei count, GLdouble const* value) & noexcept -> void;

  auto programUniform1ui(GLuint program, GLint location, GLuint v0) & noexcept -> void;

  auto programUniform1uiv(GLuint program, GLint location, GLsizei count, GLuint const* value) & noexcept -> void;

  auto programUniform2i(GLuint program, GLint location, GLint v0, GLint v1) & noexcept -> void;

  auto programUniform2iv(GLuint program, GLint location, GLsizei count, GLint const* value) & noexcept -> void;

  auto programUniform2f(GLuint program, GLint location, GLfloat v0, GLfloat v1) & noexcept -> void;

  auto programUniform2fv(GLuint program, GLint location, GLsizei count, GLfloat const* value) & noexcept -> void;

  auto programUniform2d(GLuint program, GLint location, GLdouble v0, GLdouble v1) & noexcept -> void;

  auto programUniform2dv(GLuint program, GLint location, GLsizei count, GLdouble const* value) & noexcept -> void;

  auto programUniform2ui(GLuint program, GLint location, GLuint v0, GLuint v1) & noexcept -> void;

  auto programUniform2uiv(GLuint program, GLint location, GLsizei count, GLuint const* value) & noexcept -> void;

  auto programUniform3i(GLuint program, GLint location, GLint v0, GLint v1, GLint v2) & noexcept -> void;

  auto programUniform3iv(GLuint program, GLint location, GLsizei count, GLint const* value) & noexcept -> void;

  auto programUniform3f(GLuint program, GLint location, GLfloat v0, GLfloat v1, GLfloat v2) & noexcept -> void;

  auto programUniform3fv(GLuint program, GLint location, GLsizei count, GLfloat const* value) & noexcept -> void;

  auto programUniform3d(GLuint program, GLint location, GLdouble v0, GLdouble v1, GLdouble v2) & noexcept -> void;

  auto programUniform3dv(GLuint program, GLint location, GLsizei count, GLdouble const* value) & noexcept -> void;

  auto programUniform3ui(GLuint program, GLint location, GLuint v0, GLuint v1, GLuint v2) & noexcept -> void;

  auto programUniform3uiv(GLuint program, GLint location, GLsizei count, GLuint const* value) & noexcept -> void;

  auto programUniform4i(GLuint program, GLint location, GLint v0, GLint v1, GLint v2, GLint v3) & noexcept -> void;

  auto programUniform4iv(GLuint program, GLint location, GLsizei count, GLint const* value) & noexcept -> void;

  auto programUniform4f(GLuint program, GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3) & noexcept
    -> void;

  auto programUniform4fv(GLuint program, GLint location, GLsizei count, GLfloat const* value) & noexcept -> void;

  auto programUniform4d(GLuint program, GLint location, GLdouble v0, GLdouble v1, GLdouble v2, GLdouble v3) & noexcept
    -> void;

  auto programUniform4dv(GLuint program, GLint location, GLsizei count, GLdouble const* value) & noexcept -> void;

  auto programUniform4ui(GLuint program, GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3) & noexcept -> void;

  auto programUniform4uiv(GLuint program, GLint location, GLsizei count, GLuint const* value) & noexcept -> void;

  auto programUniformMatrix2fv(GLuint program,
			       GLint location,
			       GLsizei count,
			       GLboolean transpose,
			       GLfloat const* value) & noexcept -> void;

  auto programUniformMatrix3fv(GLuint program,
			       GLint location,
			       GLsizei count,
			       GLboolean transpose,
			       GLfloat const* value) & noexcept -> void;

  auto programUniformMatrix4fv(GLuint program,
			       GLint location,
			       GLsizei count,
			       GLboolean transpose,
			       GLfloat const* value) & noexcept -> void;

  auto programUniformMatrix2dv(GLuint program,
			       GLint location,
			       GLsizei count,
			       GLboolean transpose,
			       GLdouble const* value) & noexcept -> void;

  auto programUniformMatrix3dv(GLuint program,
			       GLint location,
			       GLsizei count,
			       GLboolean transpose,
			       GLdouble const* value) & noexcept -> void;

  auto programUniformMatrix4dv(GLuint program,
			       GLint location,
			       GLsizei count,
			       GLboolean transpose,
			       GLdouble const* value) & noexcept -> void;

  auto programUniformMatrix2x3fv(GLuint program,
				 GLint location,
				 GLsizei count,
				 GLboolean transpose,
				 GLfloat const* value) & noexcept -> void;

  auto programUniformMatrix3x2fv(GLuint program,
				 GLint location,
				 GLsizei count,
				 GLboolean transpose,
				 GLfloat const* value) & noexcept -> void;

  auto programUniformMatrix2x4fv(GLuint program,
				 GLint location,
				 GLsizei count,
				 GLboolean transpose,
				 GLfloat const* value) & noexcept -> void;

  auto programUniformMatrix4x2fv(GLuint program,
				 GLint location,
				 GLsizei count,
				 GLboolean transpose,
				 GLfloat const* value) & noexcept -> void;

  auto programUniformMatrix3x4fv(GLuint program,
				 GLint location,
				 GLsizei count,
				 GLboolean transpose,
				 GLfloat const* value) & noexcept -> void;

  auto programUniformMatrix4x3fv(GLuint program,
				 GLint location,
				 GLsizei count,
				 GLboolean transpose,
				 GLfloat const* value) & noexcept -> void;

  auto programUniformMatrix2x3dv(GLuint program,
				 GLint location,
				 GLsizei count,
				 GLboolean transpose,
				 GLdouble const* value) & noexcept -> void;

  auto programUniformMatrix3x2dv(GLuint program,
				 GLint location,
				 GLsizei count,
				 GLboolean transpose,
				 GLdouble const* value) & noexcept -> void;

  auto programUniformMatrix2x4dv(GLuint program,
				 GLint location,
				 GLsizei count,
				 GLboolean transpose,
				 GLdouble const* value) & noexcept -> void;

  auto programUniformMatrix4x2dv(GLuint program,
				 GLint location,
				 GLsizei count,
				 GLboolean transpose,
				 GLdouble const* value) & noexcept -> void;

  auto programUniformMatrix3x4dv(GLuint program,
				 GLint location,
				 GLsizei count,
				 GLboolean transpose,
				 GLdouble const* value) & noexcept -> void;

  auto programUniformMatrix4x3dv(GLuint program,
				 GLint location,
				 GLsizei count,
				 GLboolean transpose,
				 GLdouble const* value) & noexcept -> void;

  auto validateProgramPipeline(GLuint pipeline) & noexcept -> void;

  auto getProgramPipelineInfoLog(GLuint pipeline, GLsizei bufSize, GLsizei* length, GLchar* infoLog) & noexcept -> void;

  auto vertexAttribL1d(GLuint index, GLdouble x) & noexcept -> void;

  auto vertexAttribL2d(GLuint index, GLdouble x, GLdouble y) & noexcept -> void;

  auto vertexAttribL3d(GLuint index, GLdouble x, GLdouble y, GLdouble z) & noexcept -> void;

  auto vertexAttribL4d(GLuint index, GLdouble x, GLdouble y, GLdouble z, GLdouble w) & noexcept -> void;

  auto vertexAttribL1dv(GLuint index, GLdouble const* v) & noexcept -> void;

  auto vertexAttribL2dv(GLuint index, GLdouble const* v) & noexcept -> void;

  auto vertexAttribL3dv(GLuint index, GLdouble const* v) & noexcept -> void;

  auto vertexAttribL4dv(GLuint index, GLdouble const* v) & noexcept -> void;

  auto vertexAttribLPointer(GLuint index, GLint size, GLenum type, GLsizei stride, void const* pointer) & noexcept
    -> void;

  auto getVertexAttribLdv(GLuint index, GLenum pname, GLdouble* params) & noexcept -> void;

  auto viewportArrayv(GLuint first, GLsizei count, GLfloat const* v) & noexcept -> void;

  auto viewportIndexedf(GLuint index, GLfloat x, GLfloat y, GLfloat w, GLfloat h) & noexcept -> void;

  auto viewportIndexedfv(GLuint index, GLfloat const* v) & noexcept -> void;

  auto scissorArrayv(GLuint first, GLsizei count, GLint const* v) & noexcept -> void;

  auto scissorIndexed(GLuint index, GLint left, GLint bottom, GLsizei width, GLsizei height) & noexcept -> void;

  auto scissorIndexedv(GLuint index, GLint const* v) & noexcept -> void;

  auto depthRangeArrayv(GLuint first, GLsizei count, GLdouble const* v) & noexcept -> void;

  auto depthRangeIndexed(GLuint index, GLdouble n, GLdouble f) & noexcept -> void;

  auto getFloati_v(GLenum target, GLuint index, GLfloat* data) & noexcept -> void;

  auto getDoublei_v(GLenum target, GLuint index, GLdouble* data) & noexcept -> void;

  // GL_VERSION_4_2
  auto drawArraysInstancedBaseInstance(GLenum mode,
				       GLint first,
				       GLsizei count,
				       GLsizei instancecount,
				       GLuint baseinstance) & noexcept -> void;

  auto drawElementsInstancedBaseInstance(GLenum mode,
					 GLsizei count,
					 GLenum type,
					 void const* indices,
					 GLsizei instancecount,
					 GLuint baseinstance) & noexcept -> void;

  auto drawElementsInstancedBaseVertexBaseInstance(GLenum mode,
						   GLsizei count,
						   GLenum type,
						   void const* indices,
						   GLsizei instancecount,
						   GLint basevertex,
						   GLuint baseinstance) & noexcept -> void;

  auto getInternalformativ(GLenum target,
			   GLenum internalformat,
			   GLenum pname,
			   GLsizei bufSize,
			   GLint* params) & noexcept -> void;

  auto getActiveAtomicCounterBufferiv(GLuint program, GLuint bufferIndex, GLenum pname, GLint* params) & noexcept
    -> void;

  auto bindImageTexture(GLuint unit,
			GLuint texture,
			GLint level,
			GLboolean layered,
			GLint layer,
			GLenum access,
			GLenum format) & noexcept -> void;

  auto memoryBarrier(GLbitfield barriers) & noexcept -> void;

  auto texStorage1D(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width) & noexcept -> void;

  auto texStorage2D(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height) & noexcept
    -> void;

  auto texStorage3D(GLenum target,
		    GLsizei levels,
		    GLenum internalformat,
		    GLsizei width,
		    GLsizei height,
		    GLsizei depth) & noexcept -> void;

  auto drawTransformFeedbackInstanced(GLenum mode, GLuint id, GLsizei instancecount) & noexcept -> void;

  auto drawTransformFeedbackStreamInstanced(GLenum mode, GLuint id, GLuint stream, GLsizei instancecount) & noexcept
    -> void;

  // GL_VERSION_4_3
  auto clearBufferData(GLenum target, GLenum internalformat, GLenum format, GLenum type, void const* data) & noexcept
    -> void;

  auto clearBufferSubData(GLenum target,
			  GLenum internalformat,
			  GLintptr offset,
			  GLsizeiptr size,
			  GLenum format,
			  GLenum type,
			  void const* data) & noexcept -> void;

  auto dispatchCompute(GLuint num_groups_x, GLuint num_groups_y, GLuint num_groups_z) & noexcept -> void;

  auto dispatchComputeIndirect(GLintptr indirect) & noexcept -> void;

  auto copyImageSubData(GLuint srcName,
			GLenum srcTarget,
			GLint srcLevel,
			GLint srcX,
			GLint srcY,
			GLint srcZ,
			GLuint dstName,
			GLenum dstTarget,
			GLint dstLevel,
			GLint dstX,
			GLint dstY,
			GLint dstZ,
			GLsizei srcWidth,
			GLsizei srcHeight,
			GLsizei srcDepth) & noexcept -> void;

  auto framebufferParameteri(GLenum target, GLenum pname, GLint param) & noexcept -> void;

  auto getFramebufferParameteriv(GLenum target, GLenum pname, GLint* params) & noexcept -> void;

  auto getInternalformati64v(GLenum target,
			     GLenum internalformat,
			     GLenum pname,
			     GLsizei bufSize,
			     GLint64* params) & noexcept -> void;

  auto invalidateTexSubImage(GLuint texture,
			     GLint level,
			     GLint xoffset,
			     GLint yoffset,
			     GLint zoffset,
			     GLsizei width,
			     GLsizei height,
			     GLsizei depth) & noexcept -> void;

  auto invalidateTexImage(GLuint texture, GLint level) & noexcept -> void;

  auto invalidateBufferSubData(GLuint buffer, GLintptr offset, GLsizeiptr length) & noexcept -> void;

  auto invalidateBufferData(GLuint buffer) & noexcept -> void;

  auto invalidateFramebuffer(GLenum target, GLsizei numAttachments, GLenum const* attachments) & noexcept -> void;

  auto invalidateSubFramebuffer(GLenum target,
				GLsizei numAttachments,
				GLenum const* attachments,
				GLint x,
				GLint y,
				GLsizei width,
				GLsizei height) & noexcept -> void;

  auto multiDrawArraysIndirect(GLenum mode, void const* indirect, GLsizei drawcount, GLsizei stride) & noexcept -> void;

  auto multiDrawElementsIndirect(GLenum mode,
				 GLenum type,
				 void const* indirect,
				 GLsizei drawcount,
				 GLsizei stride) & noexcept -> void;

  auto getProgramInterfaceiv(GLuint program, GLenum programInterface, GLenum pname, GLint* params) & noexcept -> void;

  auto getProgramResourceIndex(GLuint program, GLenum programInterface, GLchar const* name) & noexcept -> GLuint;

  auto getProgramResourceName(GLuint program,
			      GLenum programInterface,
			      GLuint index,
			      GLsizei bufSize,
			      GLsizei* length,
			      GLchar* name) & noexcept -> void;

  auto getProgramResourceiv(GLuint program,
			    GLenum programInterface,
			    GLuint index,
			    GLsizei propCount,
			    GLenum const* props,
			    GLsizei bufSize,
			    GLsizei* length,
			    GLint* params) & noexcept -> void;

  auto getProgramResourceLocation(GLuint program, GLenum programInterface, GLchar const* name) & noexcept -> GLint;

  auto getProgramResourceLocationIndex(GLuint program, GLenum programInterface, GLchar const* name) & noexcept -> GLint;

  auto shaderStorageBlockBinding(GLuint program, GLuint storageBlockIndex, GLuint storageBlockBinding) & noexcept
    -> void;

  auto texBufferRange(GLenum target, GLenum internalformat, GLuint buffer, GLintptr offset, GLsizeiptr size) & noexcept
    -> void;

  auto texStorage2DMultisample(GLenum target,
			       GLsizei samples,
			       GLenum internalformat,
			       GLsizei width,
			       GLsizei height,
			       GLboolean fixedsamplelocations) & noexcept -> void;

  auto texStorage3DMultisample(GLenum target,
			       GLsizei samples,
			       GLenum internalformat,
			       GLsizei width,
			       GLsizei height,
			       GLsizei depth,
			       GLboolean fixedsamplelocations) & noexcept -> void;

  auto textureView(GLuint texture,
		   GLenum target,
		   GLuint origtexture,
		   GLenum internalformat,
		   GLuint minlevel,
		   GLuint numlevels,
		   GLuint minlayer,
		   GLuint numlayers) & noexcept -> void;

  auto bindVertexBuffer(GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride) & noexcept -> void;

  auto vertexAttribFormat(GLuint attribindex,
			  GLint size,
			  GLenum type,
			  GLboolean normalized,
			  GLuint relativeoffset) & noexcept -> void;

  auto vertexAttribIFormat(GLuint attribindex, GLint size, GLenum type, GLuint relativeoffset) & noexcept -> void;

  auto vertexAttribLFormat(GLuint attribindex, GLint size, GLenum type, GLuint relativeoffset) & noexcept -> void;

  auto vertexAttribBinding(GLuint attribindex, GLuint bindingindex) & noexcept -> void;

  auto vertexBindingDivisor(GLuint bindingindex, GLuint divisor) & noexcept -> void;

  auto debugMessageControl(GLenum source,
			   GLenum type,
			   GLenum severity,
			   GLsizei count,
			   GLuint const* ids,
			   GLboolean enabled) & noexcept -> void;

  auto debugMessageInsert(GLenum source,
			  GLenum type,
			  GLuint id,
			  GLenum severity,
			  GLsizei length,
			  GLchar const* buf) & noexcept -> void;

  auto debugMessageCallback(GLDEBUGPROC callback, void const* userParam) & noexcept -> void;

  auto getDebugMessageLog(GLuint count,
			  GLsizei bufSize,
			  GLenum* sources,
			  GLenum* types,
			  GLuint* ids,
			  GLenum* severities,
			  GLsizei* lengths,
			  GLchar* messageLog) & noexcept -> GLuint;

  auto pushDebugGroup(GLenum source, GLuint id, GLsizei length, GLchar const* message) & noexcept -> void;

  auto popDebugGroup() & noexcept -> void;

  auto objectLabel(GLenum identifier, GLuint name, GLsizei length, GLchar const* label) & noexcept -> void;

  auto getObjectLabel(GLenum identifier, GLuint name, GLsizei bufSize, GLsizei* length, GLchar* label) & noexcept
    -> void;

  auto objectPtrLabel(void const* ptr, GLsizei length, GLchar const* label) & noexcept -> void;

  auto getObjectPtrLabel(void const* ptr, GLsizei bufSize, GLsizei* length, GLchar* label) & noexcept -> void;

  // GL_VERSION_4_4
  auto bufferStorage(GLenum target, GLsizeiptr size, void const* data, GLbitfield flags) & noexcept -> void;

  auto clearTexImage(GLuint texture, GLint level, GLenum format, GLenum type, void const* data) & noexcept -> void;

  auto clearTexSubImage(GLuint texture,
			GLint level,
			GLint xoffset,
			GLint yoffset,
			GLint zoffset,
			GLsizei width,
			GLsizei height,
			GLsizei depth,
			GLenum format,
			GLenum type,
			void const* data) & noexcept -> void;

  auto bindBuffersBase(GLenum target, GLuint first, GLsizei count, GLuint const* buffers) & noexcept -> void;

  auto bindBuffersRange(GLenum target,
			GLuint first,
			GLsizei count,
			GLuint const* buffers,
			GLintptr const* offsets,
			GLsizeiptr const* sizes) & noexcept -> void;

  auto bindTextures(GLuint first, GLsizei count, GLuint const* textures) & noexcept -> void;

  auto bindSamplers(GLuint first, GLsizei count, GLuint const* samplers) & noexcept -> void;

  auto bindImageTextures(GLuint first, GLsizei count, GLuint const* textures) & noexcept -> void;

  auto bindVertexBuffers(GLuint first,
			 GLsizei count,
			 GLuint const* buffers,
			 GLintptr const* offsets,
			 GLsizei const* strides) & noexcept -> void;

  // GL_VERSION_4_5
  auto clipControl(GLenum origin, GLenum depth) & noexcept -> void;

  auto createTransformFeedbacks(GLsizei n, GLuint* ids) & noexcept -> void;

  auto transformFeedbackBufferBase(GLuint xfb, GLuint index, GLuint buffer) & noexcept -> void;

  auto transformFeedbackBufferRange(GLuint xfb,
				    GLuint index,
				    GLuint buffer,
				    GLintptr offset,
				    GLsizeiptr size) & noexcept -> void;

  auto getTransformFeedbackiv(GLuint xfb, GLenum pname, GLint* param) & noexcept -> void;

  auto getTransformFeedbacki_v(GLuint xfb, GLenum pname, GLuint index, GLint* param) & noexcept -> void;

  auto getTransformFeedbacki64_v(GLuint xfb, GLenum pname, GLuint index, GLint64* param) & noexcept -> void;

  auto createBuffers(GLsizei n, GLuint* buffers) & noexcept -> void;

  auto namedBufferStorage(GLuint buffer, GLsizeiptr size, void const* data, GLbitfield flags) & noexcept -> void;

  auto namedBufferData(GLuint buffer, GLsizeiptr size, void const* data, GLenum usage) & noexcept -> void;

  auto namedBufferSubData(GLuint buffer, GLintptr offset, GLsizeiptr size, void const* data) & noexcept -> void;

  auto copyNamedBufferSubData(GLuint readBuffer,
			      GLuint writeBuffer,
			      GLintptr readOffset,
			      GLintptr writeOffset,
			      GLsizeiptr size) & noexcept -> void;

  auto clearNamedBufferData(GLuint buffer,
			    GLenum internalformat,
			    GLenum format,
			    GLenum type,
			    void const* data) & noexcept -> void;

  auto clearNamedBufferSubData(GLuint buffer,
			       GLenum internalformat,
			       GLintptr offset,
			       GLsizeiptr size,
			       GLenum format,
			       GLenum type,
			       void const* data) & noexcept -> void;

  auto mapNamedBuffer(GLuint buffer, GLenum access) & noexcept -> void*;

  auto mapNamedBufferRange(GLuint buffer, GLintptr offset, GLsizeiptr length, GLbitfield access) & noexcept -> void*;

  auto unmapNamedBuffer(GLuint buffer) & noexcept -> GLboolean;

  auto flushMappedNamedBufferRange(GLuint buffer, GLintptr offset, GLsizeiptr length) & noexcept -> void;

  auto getNamedBufferParameteriv(GLuint buffer, GLenum pname, GLint* params) & noexcept -> void;

  auto getNamedBufferParameteri64v(GLuint buffer, GLenum pname, GLint64* params) & noexcept -> void;

  auto getNamedBufferPointerv(GLuint buffer, GLenum pname, void** params) & noexcept -> void;

  auto getNamedBufferSubData(GLuint buffer, GLintptr offset, GLsizeiptr size, void* data) & noexcept -> void;

  auto createFramebuffers(GLsizei n, GLuint* framebuffers) & noexcept -> void;

  auto namedFramebufferRenderbuffer(GLuint framebuffer,
				    GLenum attachment,
				    GLenum renderbuffertarget,
				    GLuint renderbuffer) & noexcept -> void;

  auto namedFramebufferParameteri(GLuint framebuffer, GLenum pname, GLint param) & noexcept -> void;

  auto namedFramebufferTexture(GLuint framebuffer, GLenum attachment, GLuint texture, GLint level) & noexcept -> void;

  auto namedFramebufferTextureLayer(GLuint framebuffer,
				    GLenum attachment,
				    GLuint texture,
				    GLint level,
				    GLint layer) & noexcept -> void;

  auto namedFramebufferDrawBuffer(GLuint framebuffer, GLenum buf) & noexcept -> void;

  auto namedFramebufferDrawBuffers(GLuint framebuffer, GLsizei n, GLenum const* bufs) & noexcept -> void;

  auto namedFramebufferReadBuffer(GLuint framebuffer, GLenum src) & noexcept -> void;

  auto invalidateNamedFramebufferData(GLuint framebuffer, GLsizei numAttachments, GLenum const* attachments) & noexcept
    -> void;

  auto invalidateNamedFramebufferSubData(GLuint framebuffer,
					 GLsizei numAttachments,
					 GLenum const* attachments,
					 GLint x,
					 GLint y,
					 GLsizei width,
					 GLsizei height) & noexcept -> void;

  auto clearNamedFramebufferiv(GLuint framebuffer, GLenum buffer, GLint drawbuffer, GLint const* value) & noexcept
    -> void;

  auto clearNamedFramebufferuiv(GLuint framebuffer, GLenum buffer, GLint drawbuffer, GLuint const* value) & noexcept
    -> void;

  auto clearNamedFramebufferfv(GLuint framebuffer, GLenum buffer, GLint drawbuffer, GLfloat const* value) & noexcept
    -> void;

  auto clearNamedFramebufferfi(GLuint framebuffer,
			       GLenum buffer,
			       GLint drawbuffer,
			       GLfloat depth,
			       GLint stencil) & noexcept -> void;

  auto blitNamedFramebuffer(GLuint readFramebuffer,
			    GLuint drawFramebuffer,
			    GLint srcX0,
			    GLint srcY0,
			    GLint srcX1,
			    GLint srcY1,
			    GLint dstX0,
			    GLint dstY0,
			    GLint dstX1,
			    GLint dstY1,
			    GLbitfield mask,
			    GLenum filter) & noexcept -> void;

  auto checkNamedFramebufferStatus(GLuint framebuffer, GLenum target) & noexcept -> GLenum;

  auto getNamedFramebufferParameteriv(GLuint framebuffer, GLenum pname, GLint* param) & noexcept -> void;

  auto getNamedFramebufferAttachmentParameteriv(GLuint framebuffer,
						GLenum attachment,
						GLenum pname,
						GLint* params) & noexcept -> void;

  auto createRenderbuffers(GLsizei n, GLuint* renderbuffers) & noexcept -> void;

  auto namedRenderbufferStorage(GLuint renderbuffer, GLenum internalformat, GLsizei width, GLsizei height) & noexcept
    -> void;

  auto namedRenderbufferStorageMultisample(GLuint renderbuffer,
					   GLsizei samples,
					   GLenum internalformat,
					   GLsizei width,
					   GLsizei height) & noexcept -> void;

  auto getNamedRenderbufferParameteriv(GLuint renderbuffer, GLenum pname, GLint* params) & noexcept -> void;

  auto createTextures(GLenum target, GLsizei n, GLuint* textures) & noexcept -> void;

  auto textureBuffer(GLuint texture, GLenum internalformat, GLuint buffer) & noexcept -> void;

  auto textureBufferRange(GLuint texture,
			  GLenum internalformat,
			  GLuint buffer,
			  GLintptr offset,
			  GLsizeiptr size) & noexcept -> void;

  auto textureStorage1D(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width) & noexcept -> void;

  auto textureStorage2D(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height) & noexcept
    -> void;

  auto textureStorage3D(GLuint texture,
			GLsizei levels,
			GLenum internalformat,
			GLsizei width,
			GLsizei height,
			GLsizei depth) & noexcept -> void;

  auto textureStorage2DMultisample(GLuint texture,
				   GLsizei samples,
				   GLenum internalformat,
				   GLsizei width,
				   GLsizei height,
				   GLboolean fixedsamplelocations) & noexcept -> void;

  auto textureStorage3DMultisample(GLuint texture,
				   GLsizei samples,
				   GLenum internalformat,
				   GLsizei width,
				   GLsizei height,
				   GLsizei depth,
				   GLboolean fixedsamplelocations) & noexcept -> void;

  auto textureSubImage1D(GLuint texture,
			 GLint level,
			 GLint xoffset,
			 GLsizei width,
			 GLenum format,
			 GLenum type,
			 void const* pixels) & noexcept -> void;

  auto textureSubImage2D(GLuint texture,
			 GLint level,
			 GLint xoffset,
			 GLint yoffset,
			 GLsizei width,
			 GLsizei height,
			 GLenum format,
			 GLenum type,
			 void const* pixels) & noexcept -> void;

  auto textureSubImage3D(GLuint texture,
			 GLint level,
			 GLint xoffset,
			 GLint yoffset,
			 GLint zoffset,
			 GLsizei width,
			 GLsizei height,
			 GLsizei depth,
			 GLenum format,
			 GLenum type,
			 void const* pixels) & noexcept -> void;

  auto compressedTextureSubImage1D(GLuint texture,
				   GLint level,
				   GLint xoffset,
				   GLsizei width,
				   GLenum format,
				   GLsizei imageSize,
				   void const* data) & noexcept -> void;

  auto compressedTextureSubImage2D(GLuint texture,
				   GLint level,
				   GLint xoffset,
				   GLint yoffset,
				   GLsizei width,
				   GLsizei height,
				   GLenum format,
				   GLsizei imageSize,
				   void const* data) & noexcept -> void;

  auto compressedTextureSubImage3D(GLuint texture,
				   GLint level,
				   GLint xoffset,
				   GLint yoffset,
				   GLint zoffset,
				   GLsizei width,
				   GLsizei height,
				   GLsizei depth,
				   GLenum format,
				   GLsizei imageSize,
				   void const* data) & noexcept -> void;

  auto copyTextureSubImage1D(GLuint texture, GLint level, GLint xoffset, GLint x, GLint y, GLsizei width) & noexcept
    -> void;

  auto copyTextureSubImage2D(GLuint texture,
			     GLint level,
			     GLint xoffset,
			     GLint yoffset,
			     GLint x,
			     GLint y,
			     GLsizei width,
			     GLsizei height) & noexcept -> void;

  auto copyTextureSubImage3D(GLuint texture,
			     GLint level,
			     GLint xoffset,
			     GLint yoffset,
			     GLint zoffset,
			     GLint x,
			     GLint y,
			     GLsizei width,
			     GLsizei height) & noexcept -> void;

  auto textureParameterf(GLuint texture, GLenum pname, GLfloat param) & noexcept -> void;

  auto textureParameterfv(GLuint texture, GLenum pname, GLfloat const* param) & noexcept -> void;

  auto textureParameteri(GLuint texture, GLenum pname, GLint param) & noexcept -> void;

  auto textureParameterIiv(GLuint texture, GLenum pname, GLint const* params) & noexcept -> void;

  auto textureParameterIuiv(GLuint texture, GLenum pname, GLuint const* params) & noexcept -> void;

  auto textureParameteriv(GLuint texture, GLenum pname, GLint const* param) & noexcept -> void;

  auto generateTextureMipmap(GLuint texture) & noexcept -> void;

  auto bindTextureUnit(GLuint unit, GLuint texture) & noexcept -> void;

  auto getTextureImage(GLuint texture,
		       GLint level,
		       GLenum format,
		       GLenum type,
		       GLsizei bufSize,
		       void* pixels) & noexcept -> void;

  auto getCompressedTextureImage(GLuint texture, GLint level, GLsizei bufSize, void* pixels) & noexcept -> void;

  auto getTextureLevelParameterfv(GLuint texture, GLint level, GLenum pname, GLfloat* params) & noexcept -> void;

  auto getTextureLevelParameteriv(GLuint texture, GLint level, GLenum pname, GLint* params) & noexcept -> void;

  auto getTextureParameterfv(GLuint texture, GLenum pname, GLfloat* params) & noexcept -> void;

  auto getTextureParameterIiv(GLuint texture, GLenum pname, GLint* params) & noexcept -> void;

  auto getTextureParameterIuiv(GLuint texture, GLenum pname, GLuint* params) & noexcept -> void;

  auto getTextureParameteriv(GLuint texture, GLenum pname, GLint* params) & noexcept -> void;

  auto createVertexArrays(GLsizei n, GLuint* arrays) & noexcept -> void;

  auto disableVertexArrayAttrib(GLuint vaobj, GLuint index) & noexcept -> void;

  auto enableVertexArrayAttrib(GLuint vaobj, GLuint index) & noexcept -> void;

  auto vertexArrayElementBuffer(GLuint vaobj, GLuint buffer) & noexcept -> void;

  auto vertexArrayVertexBuffer(GLuint vaobj,
			       GLuint bindingindex,
			       GLuint buffer,
			       GLintptr offset,
			       GLsizei stride) & noexcept -> void;

  auto vertexArrayVertexBuffers(GLuint vaobj,
				GLuint first,
				GLsizei count,
				GLuint const* buffers,
				GLintptr const* offsets,
				GLsizei const* strides) & noexcept -> void;

  auto vertexArrayAttribBinding(GLuint vaobj, GLuint attribindex, GLuint bindingindex) & noexcept -> void;

  auto vertexArrayAttribFormat(GLuint vaobj,
			       GLuint attribindex,
			       GLint size,
			       GLenum type,
			       GLboolean normalized,
			       GLuint relativeoffset) & noexcept -> void;

  auto vertexArrayAttribIFormat(GLuint vaobj,
				GLuint attribindex,
				GLint size,
				GLenum type,
				GLuint relativeoffset) & noexcept -> void;

  auto vertexArrayAttribLFormat(GLuint vaobj,
				GLuint attribindex,
				GLint size,
				GLenum type,
				GLuint relativeoffset) & noexcept -> void;

  auto vertexArrayBindingDivisor(GLuint vaobj, GLuint bindingindex, GLuint divisor) & noexcept -> void;

  auto getVertexArrayiv(GLuint vaobj, GLenum pname, GLint* param) & noexcept -> void;

  auto getVertexArrayIndexediv(GLuint vaobj, GLuint index, GLenum pname, GLint* param) & noexcept -> void;

  auto getVertexArrayIndexed64iv(GLuint vaobj, GLuint index, GLenum pname, GLint64* param) & noexcept -> void;

  auto createSamplers(GLsizei n, GLuint* samplers) & noexcept -> void;

  auto createProgramPipelines(GLsizei n, GLuint* pipelines) & noexcept -> void;

  auto createQueries(GLenum target, GLsizei n, GLuint* ids) & noexcept -> void;

  auto getQueryBufferObjecti64v(GLuint id, GLuint buffer, GLenum pname, GLintptr offset) & noexcept -> void;

  auto getQueryBufferObjectiv(GLuint id, GLuint buffer, GLenum pname, GLintptr offset) & noexcept -> void;

  auto getQueryBufferObjectui64v(GLuint id, GLuint buffer, GLenum pname, GLintptr offset) & noexcept -> void;

  auto getQueryBufferObjectuiv(GLuint id, GLuint buffer, GLenum pname, GLintptr offset) & noexcept -> void;

  auto memoryBarrierByRegion(GLbitfield barriers) & noexcept -> void;

  auto getTextureSubImage(GLuint texture,
			  GLint level,
			  GLint xoffset,
			  GLint yoffset,
			  GLint zoffset,
			  GLsizei width,
			  GLsizei height,
			  GLsizei depth,
			  GLenum format,
			  GLenum type,
			  GLsizei bufSize,
			  void* pixels) & noexcept -> void;

  auto getCompressedTextureSubImage(GLuint texture,
				    GLint level,
				    GLint xoffset,
				    GLint yoffset,
				    GLint zoffset,
				    GLsizei width,
				    GLsizei height,
				    GLsizei depth,
				    GLsizei bufSize,
				    void* pixels) & noexcept -> void;

  auto getGraphicsResetStatus() & noexcept -> GLenum;

  auto getnCompressedTexImage(GLenum target, GLint lod, GLsizei bufSize, void* pixels) & noexcept -> void;

  auto getnTexImage(GLenum target, GLint level, GLenum format, GLenum type, GLsizei bufSize, void* pixels) & noexcept
    -> void;

  auto getnUniformdv(GLuint program, GLint location, GLsizei bufSize, GLdouble* params) & noexcept -> void;

  auto getnUniformfv(GLuint program, GLint location, GLsizei bufSize, GLfloat* params) & noexcept -> void;

  auto getnUniformiv(GLuint program, GLint location, GLsizei bufSize, GLint* params) & noexcept -> void;

  auto getnUniformuiv(GLuint program, GLint location, GLsizei bufSize, GLuint* params) & noexcept -> void;

  auto readnPixels(GLint x,
		   GLint y,
		   GLsizei width,
		   GLsizei height,
		   GLenum format,
		   GLenum type,
		   GLsizei bufSize,
		   void* data) & noexcept -> void;

  auto textureBarrier() & noexcept -> void;

  // GL_VERSION_4_6
  auto specializeShader(GLuint shader,
			GLchar const* pEntryPoint,
			GLuint numSpecializationConstants,
			GLuint const* pConstantIndex,
			GLuint const* pConstantValue) & noexcept -> void;

  auto multiDrawArraysIndirectCount(GLenum mode,
				    void const* indirect,
				    GLintptr drawcount,
				    GLsizei maxdrawcount,
				    GLsizei stride) & noexcept -> void;

  auto multiDrawElementsIndirectCount(GLenum mode,
				      GLenum type,
				      void const* indirect,
				      GLintptr drawcount,
				      GLsizei maxdrawcount,
				      GLsizei stride) & noexcept -> void;

  auto polygonOffsetClamp(GLfloat factor, GLfloat units, GLfloat clamp) & noexcept -> void;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool
  {
    return is_version_1_0_loaded() && is_version_1_1_loaded() && is_version_1_2_loaded() && is_version_1_3_loaded() &&
	   is_version_1_4_loaded() && is_version_1_5_loaded() && is_version_2_0_loaded() && is_version_2_1_loaded() &&
	   is_version_3_0_loaded() && is_version_3_1_loaded() && is_version_3_2_loaded() && is_version_3_3_loaded() &&
	   is_version_4_0_loaded() && is_version_4_1_loaded() && is_version_4_2_loaded() && is_version_4_3_loaded() &&
	   is_version_4_4_loaded() && is_version_4_5_loaded() && is_version_4_6_loaded();
  }

  [[nodiscard]] constexpr auto is_version_1_0_loaded() const& noexcept -> bool
  {
    return m_data_version_1_0.is_loaded();
  }

  [[nodiscard]] constexpr auto is_version_1_1_loaded() const& noexcept -> bool
  {
    return m_data_version_1_1.is_loaded();
  }

  [[nodiscard]] constexpr auto is_version_1_2_loaded() const& noexcept -> bool
  {
    return m_data_version_1_2.is_loaded();
  }

  [[nodiscard]] constexpr auto is_version_1_3_loaded() const& noexcept -> bool
  {
    return m_data_version_1_3.is_loaded();
  }

  [[nodiscard]] constexpr auto is_version_1_4_loaded() const& noexcept -> bool
  {
    return m_data_version_1_4.is_loaded();
  }

  [[nodiscard]] constexpr auto is_version_1_5_loaded() const& noexcept -> bool
  {
    return m_data_version_1_5.is_loaded();
  }

  [[nodiscard]] constexpr auto is_version_2_0_loaded() const& noexcept -> bool
  {
    return m_data_version_2_0.is_loaded();
  }

  [[nodiscard]] constexpr auto is_version_2_1_loaded() const& noexcept -> bool
  {
    return m_data_version_2_1.is_loaded();
  }

  [[nodiscard]] constexpr auto is_version_3_0_loaded() const& noexcept -> bool
  {
    return m_data_version_3_0.is_loaded();
  }

  [[nodiscard]] constexpr auto is_version_3_1_loaded() const& noexcept -> bool
  {
    return m_data_version_3_1.is_loaded();
  }

  [[nodiscard]] constexpr auto is_version_3_2_loaded() const& noexcept -> bool
  {
    return m_data_version_3_2.is_loaded();
  }

  [[nodiscard]] constexpr auto is_version_3_3_loaded() const& noexcept -> bool
  {
    return m_data_version_3_3.is_loaded();
  }

  [[nodiscard]] constexpr auto is_version_4_0_loaded() const& noexcept -> bool
  {
    return m_data_version_4_0.is_loaded();
  }

  [[nodiscard]] constexpr auto is_version_4_1_loaded() const& noexcept -> bool
  {
    return m_data_version_4_1.is_loaded();
  }

  [[nodiscard]] constexpr auto is_version_4_2_loaded() const& noexcept -> bool
  {
    return m_data_version_4_2.is_loaded();
  }

  [[nodiscard]] constexpr auto is_version_4_3_loaded() const& noexcept -> bool
  {
    return m_data_version_4_3.is_loaded();
  }

  [[nodiscard]] constexpr auto is_version_4_4_loaded() const& noexcept -> bool
  {
    return m_data_version_4_4.is_loaded();
  }

  [[nodiscard]] constexpr auto is_version_4_5_loaded() const& noexcept -> bool
  {
    return m_data_version_4_5.is_loaded();
  }

  [[nodiscard]] constexpr auto is_version_4_6_loaded() const& noexcept -> bool
  {
    return m_data_version_4_6.is_loaded();
  }

  auto load() & noexcept -> void;

  auto load_version_1_0() & noexcept -> void;

  auto load_version_1_1() & noexcept -> void;

  auto load_version_1_2() & noexcept -> void;

  auto load_version_1_3() & noexcept -> void;

  auto load_version_1_4() & noexcept -> void;

  auto load_version_1_5() & noexcept -> void;

  auto load_version_2_0() & noexcept -> void;

  auto load_version_2_1() & noexcept -> void;

  auto load_version_3_0() & noexcept -> void;

  auto load_version_3_1() & noexcept -> void;

  auto load_version_3_2() & noexcept -> void;

  auto load_version_3_3() & noexcept -> void;

  auto load_version_4_0() & noexcept -> void;

  auto load_version_4_1() & noexcept -> void;

  auto load_version_4_2() & noexcept -> void;

  auto load_version_4_3() & noexcept -> void;

  auto load_version_4_4() & noexcept -> void;

  auto load_version_4_5() & noexcept -> void;

  auto load_version_4_6() & noexcept -> void;

  static auto new_(InterfaceContext const* interface_context, bool pre_load) noexcept -> Context;

  static auto new_pointer(InterfaceContext const* interface_context, bool pre_load) noexcept -> Context*;

protected:
private:
  // GL_VERSION_1_0
  ContextDataVersion10 m_data_version_1_0 = {};
  // GL_VERSION_1_1
  ContextDataVersion11 m_data_version_1_1 = {};
  // GL_VERSION_1_2
  ContextDataVersion12 m_data_version_1_2 = {};
  // GL_VERSION_1_3
  ContextDataVersion13 m_data_version_1_3 = {};
  // GL_VERSION_1_4
  ContextDataVersion14 m_data_version_1_4 = {};
  // GL_VERSION_1_5
  ContextDataVersion15 m_data_version_1_5 = {};
  // GL_VERSION_2_0
  ContextDataVersion20 m_data_version_2_0 = {};
  // GL_VERSION_2_1
  ContextDataVersion21 m_data_version_2_1 = {};
  // GL_VERSION_3_0
  ContextDataVersion30 m_data_version_3_0 = {};
  // GL_VERSION_3_1
  ContextDataVersion31 m_data_version_3_1 = {};
  // GL_VERSION_3_2
  ContextDataVersion32 m_data_version_3_2 = {};
  // GL_VERSION_3_3
  ContextDataVersion33 m_data_version_3_3 = {};
  // GL_VERSION_4_0
  ContextDataVersion40 m_data_version_4_0 = {};
  // GL_VERSION_4_1
  ContextDataVersion41 m_data_version_4_1 = {};
  // GL_VERSION_4_2
  ContextDataVersion42 m_data_version_4_2 = {};
  // GL_VERSION_4_3
  ContextDataVersion43 m_data_version_4_3 = {};
  // GL_VERSION_4_4
  ContextDataVersion44 m_data_version_4_4 = {};
  // GL_VERSION_4_5
  ContextDataVersion45 m_data_version_4_5 = {};
  // GL_VERSION_4_6
  ContextDataVersion46 m_data_version_4_6 = {};

  InterfaceContext const* m_interface_context = nullptr;

  Context(InterfaceContext const* interface_context, bool pre_load) noexcept;

  constexpr auto set_interface_context(InterfaceContext const* old, InterfaceContext const* new_) & noexcept
  {
    if (m_interface_context == old) {
      m_interface_context = new_;
    }
  }
};
} // namespace engine::graphics::gl

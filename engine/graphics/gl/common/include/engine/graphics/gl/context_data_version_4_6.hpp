#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/gl.h"

namespace engine::graphics::gl {
struct ContextDataVersion46 final : public IContextData
{
public:
  // GL_VERSION_4_6
  PFNGLSPECIALIZESHADERPROC glSpecializeShader = nullptr;
  PFNGLMULTIDRAWARRAYSINDIRECTCOUNTPROC glMultiDrawArraysIndirectCount = nullptr;
  PFNGLMULTIDRAWELEMENTSINDIRECTCOUNTPROC glMultiDrawElementsIndirectCount = nullptr;
  PFNGLPOLYGONOFFSETCLAMPPROC glPolygonOffsetClamp = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return glSpecializeShader != nullptr && glMultiDrawArraysIndirectCount != nullptr &&
	   glMultiDrawElementsIndirectCount != nullptr && glPolygonOffsetClamp != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl

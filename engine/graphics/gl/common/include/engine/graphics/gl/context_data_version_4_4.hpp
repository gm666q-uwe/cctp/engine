#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/gl.h"

namespace engine::graphics::gl {
struct ContextDataVersion44 final : public IContextData
{
public:
  // GL_VERSION_4_4
  PFNGLBUFFERSTORAGEPROC glBufferStorage = nullptr;
  PFNGLCLEARTEXIMAGEPROC glClearTexImage = nullptr;
  PFNGLCLEARTEXSUBIMAGEPROC glClearTexSubImage = nullptr;
  PFNGLBINDBUFFERSBASEPROC glBindBuffersBase = nullptr;
  PFNGLBINDBUFFERSRANGEPROC glBindBuffersRange = nullptr;
  PFNGLBINDTEXTURESPROC glBindTextures = nullptr;
  PFNGLBINDSAMPLERSPROC glBindSamplers = nullptr;
  PFNGLBINDIMAGETEXTURESPROC glBindImageTextures = nullptr;
  PFNGLBINDVERTEXBUFFERSPROC glBindVertexBuffers = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return glBufferStorage != nullptr && glClearTexImage != nullptr && glClearTexSubImage != nullptr &&
	   glBindBuffersBase != nullptr && glBindBuffersRange != nullptr && glBindTextures != nullptr &&
	   glBindSamplers != nullptr && glBindImageTextures != nullptr && glBindVertexBuffers != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl

#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/gl.h"

namespace engine::graphics::gl {
struct ContextDataVersion31 final : public IContextData
{
public:
  // GL_VERSION_3_1
  PFNGLDRAWARRAYSINSTANCEDPROC glDrawArraysInstanced = nullptr;
  PFNGLDRAWELEMENTSINSTANCEDPROC glDrawElementsInstanced = nullptr;
  PFNGLTEXBUFFERPROC glTexBuffer = nullptr;
  PFNGLPRIMITIVERESTARTINDEXPROC glPrimitiveRestartIndex = nullptr;
  PFNGLCOPYBUFFERSUBDATAPROC glCopyBufferSubData = nullptr;
  PFNGLGETUNIFORMINDICESPROC glGetUniformIndices = nullptr;
  PFNGLGETACTIVEUNIFORMSIVPROC glGetActiveUniformsiv = nullptr;
  PFNGLGETACTIVEUNIFORMNAMEPROC glGetActiveUniformName = nullptr;
  PFNGLGETUNIFORMBLOCKINDEXPROC glGetUniformBlockIndex = nullptr;
  PFNGLGETACTIVEUNIFORMBLOCKIVPROC glGetActiveUniformBlockiv = nullptr;
  PFNGLGETACTIVEUNIFORMBLOCKNAMEPROC glGetActiveUniformBlockName = nullptr;
  PFNGLUNIFORMBLOCKBINDINGPROC glUniformBlockBinding = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return glDrawArraysInstanced != nullptr && glDrawElementsInstanced != nullptr && glTexBuffer != nullptr &&
	   glPrimitiveRestartIndex != nullptr && glCopyBufferSubData != nullptr && glGetUniformIndices != nullptr &&
	   glGetActiveUniformsiv != nullptr && glGetActiveUniformName != nullptr && glGetUniformBlockIndex != nullptr &&
	   glGetActiveUniformBlockiv != nullptr && glGetActiveUniformBlockName != nullptr &&
	   glUniformBlockBinding != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl

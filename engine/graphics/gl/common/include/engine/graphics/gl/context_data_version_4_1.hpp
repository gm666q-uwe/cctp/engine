#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/gl.h"

namespace engine::graphics::gl {
struct ContextDataVersion41 final : public IContextData
{
public:
  // GL_VERSION_4_1
  PFNGLRELEASESHADERCOMPILERPROC glReleaseShaderCompiler = nullptr;
  PFNGLSHADERBINARYPROC glShaderBinary = nullptr;
  PFNGLGETSHADERPRECISIONFORMATPROC glGetShaderPrecisionFormat = nullptr;
  PFNGLDEPTHRANGEFPROC glDepthRangef = nullptr;
  PFNGLCLEARDEPTHFPROC glClearDepthf = nullptr;
  PFNGLGETPROGRAMBINARYPROC glGetProgramBinary = nullptr;
  PFNGLPROGRAMBINARYPROC glProgramBinary = nullptr;
  PFNGLPROGRAMPARAMETERIPROC glProgramParameteri = nullptr;
  PFNGLUSEPROGRAMSTAGESPROC glUseProgramStages = nullptr;
  PFNGLACTIVESHADERPROGRAMPROC glActiveShaderProgram = nullptr;
  PFNGLCREATESHADERPROGRAMVPROC glCreateShaderProgramv = nullptr;
  PFNGLBINDPROGRAMPIPELINEPROC glBindProgramPipeline = nullptr;
  PFNGLDELETEPROGRAMPIPELINESPROC glDeleteProgramPipelines = nullptr;
  PFNGLGENPROGRAMPIPELINESPROC glGenProgramPipelines = nullptr;
  PFNGLISPROGRAMPIPELINEPROC glIsProgramPipeline = nullptr;
  PFNGLGETPROGRAMPIPELINEIVPROC glGetProgramPipelineiv = nullptr;
  PFNGLPROGRAMUNIFORM1IPROC glProgramUniform1i = nullptr;
  PFNGLPROGRAMUNIFORM1IVPROC glProgramUniform1iv = nullptr;
  PFNGLPROGRAMUNIFORM1FPROC glProgramUniform1f = nullptr;
  PFNGLPROGRAMUNIFORM1FVPROC glProgramUniform1fv = nullptr;
  PFNGLPROGRAMUNIFORM1DPROC glProgramUniform1d = nullptr;
  PFNGLPROGRAMUNIFORM1DVPROC glProgramUniform1dv = nullptr;
  PFNGLPROGRAMUNIFORM1UIPROC glProgramUniform1ui = nullptr;
  PFNGLPROGRAMUNIFORM1UIVPROC glProgramUniform1uiv = nullptr;
  PFNGLPROGRAMUNIFORM2IPROC glProgramUniform2i = nullptr;
  PFNGLPROGRAMUNIFORM2IVPROC glProgramUniform2iv = nullptr;
  PFNGLPROGRAMUNIFORM2FPROC glProgramUniform2f = nullptr;
  PFNGLPROGRAMUNIFORM2FVPROC glProgramUniform2fv = nullptr;
  PFNGLPROGRAMUNIFORM2DPROC glProgramUniform2d = nullptr;
  PFNGLPROGRAMUNIFORM2DVPROC glProgramUniform2dv = nullptr;
  PFNGLPROGRAMUNIFORM2UIPROC glProgramUniform2ui = nullptr;
  PFNGLPROGRAMUNIFORM2UIVPROC glProgramUniform2uiv = nullptr;
  PFNGLPROGRAMUNIFORM3IPROC glProgramUniform3i = nullptr;
  PFNGLPROGRAMUNIFORM3IVPROC glProgramUniform3iv = nullptr;
  PFNGLPROGRAMUNIFORM3FPROC glProgramUniform3f = nullptr;
  PFNGLPROGRAMUNIFORM3FVPROC glProgramUniform3fv = nullptr;
  PFNGLPROGRAMUNIFORM3DPROC glProgramUniform3d = nullptr;
  PFNGLPROGRAMUNIFORM3DVPROC glProgramUniform3dv = nullptr;
  PFNGLPROGRAMUNIFORM3UIPROC glProgramUniform3ui = nullptr;
  PFNGLPROGRAMUNIFORM3UIVPROC glProgramUniform3uiv = nullptr;
  PFNGLPROGRAMUNIFORM4IPROC glProgramUniform4i = nullptr;
  PFNGLPROGRAMUNIFORM4IVPROC glProgramUniform4iv = nullptr;
  PFNGLPROGRAMUNIFORM4FPROC glProgramUniform4f = nullptr;
  PFNGLPROGRAMUNIFORM4FVPROC glProgramUniform4fv = nullptr;
  PFNGLPROGRAMUNIFORM4DPROC glProgramUniform4d = nullptr;
  PFNGLPROGRAMUNIFORM4DVPROC glProgramUniform4dv = nullptr;
  PFNGLPROGRAMUNIFORM4UIPROC glProgramUniform4ui = nullptr;
  PFNGLPROGRAMUNIFORM4UIVPROC glProgramUniform4uiv = nullptr;
  PFNGLPROGRAMUNIFORMMATRIX2FVPROC glProgramUniformMatrix2fv = nullptr;
  PFNGLPROGRAMUNIFORMMATRIX3FVPROC glProgramUniformMatrix3fv = nullptr;
  PFNGLPROGRAMUNIFORMMATRIX4FVPROC glProgramUniformMatrix4fv = nullptr;
  PFNGLPROGRAMUNIFORMMATRIX2DVPROC glProgramUniformMatrix2dv = nullptr;
  PFNGLPROGRAMUNIFORMMATRIX3DVPROC glProgramUniformMatrix3dv = nullptr;
  PFNGLPROGRAMUNIFORMMATRIX4DVPROC glProgramUniformMatrix4dv = nullptr;
  PFNGLPROGRAMUNIFORMMATRIX2X3FVPROC glProgramUniformMatrix2x3fv = nullptr;
  PFNGLPROGRAMUNIFORMMATRIX3X2FVPROC glProgramUniformMatrix3x2fv = nullptr;
  PFNGLPROGRAMUNIFORMMATRIX2X4FVPROC glProgramUniformMatrix2x4fv = nullptr;
  PFNGLPROGRAMUNIFORMMATRIX4X2FVPROC glProgramUniformMatrix4x2fv = nullptr;
  PFNGLPROGRAMUNIFORMMATRIX3X4FVPROC glProgramUniformMatrix3x4fv = nullptr;
  PFNGLPROGRAMUNIFORMMATRIX4X3FVPROC glProgramUniformMatrix4x3fv = nullptr;
  PFNGLPROGRAMUNIFORMMATRIX2X3DVPROC glProgramUniformMatrix2x3dv = nullptr;
  PFNGLPROGRAMUNIFORMMATRIX3X2DVPROC glProgramUniformMatrix3x2dv = nullptr;
  PFNGLPROGRAMUNIFORMMATRIX2X4DVPROC glProgramUniformMatrix2x4dv = nullptr;
  PFNGLPROGRAMUNIFORMMATRIX4X2DVPROC glProgramUniformMatrix4x2dv = nullptr;
  PFNGLPROGRAMUNIFORMMATRIX3X4DVPROC glProgramUniformMatrix3x4dv = nullptr;
  PFNGLPROGRAMUNIFORMMATRIX4X3DVPROC glProgramUniformMatrix4x3dv = nullptr;
  PFNGLVALIDATEPROGRAMPIPELINEPROC glValidateProgramPipeline = nullptr;
  PFNGLGETPROGRAMPIPELINEINFOLOGPROC glGetProgramPipelineInfoLog = nullptr;
  PFNGLVERTEXATTRIBL1DPROC glVertexAttribL1d = nullptr;
  PFNGLVERTEXATTRIBL2DPROC glVertexAttribL2d = nullptr;
  PFNGLVERTEXATTRIBL3DPROC glVertexAttribL3d = nullptr;
  PFNGLVERTEXATTRIBL4DPROC glVertexAttribL4d = nullptr;
  PFNGLVERTEXATTRIBL1DVPROC glVertexAttribL1dv = nullptr;
  PFNGLVERTEXATTRIBL2DVPROC glVertexAttribL2dv = nullptr;
  PFNGLVERTEXATTRIBL3DVPROC glVertexAttribL3dv = nullptr;
  PFNGLVERTEXATTRIBL4DVPROC glVertexAttribL4dv = nullptr;
  PFNGLVERTEXATTRIBLPOINTERPROC glVertexAttribLPointer = nullptr;
  PFNGLGETVERTEXATTRIBLDVPROC glGetVertexAttribLdv = nullptr;
  PFNGLVIEWPORTARRAYVPROC glViewportArrayv = nullptr;
  PFNGLVIEWPORTINDEXEDFPROC glViewportIndexedf = nullptr;
  PFNGLVIEWPORTINDEXEDFVPROC glViewportIndexedfv = nullptr;
  PFNGLSCISSORARRAYVPROC glScissorArrayv = nullptr;
  PFNGLSCISSORINDEXEDPROC glScissorIndexed = nullptr;
  PFNGLSCISSORINDEXEDVPROC glScissorIndexedv = nullptr;
  PFNGLDEPTHRANGEARRAYVPROC glDepthRangeArrayv = nullptr;
  PFNGLDEPTHRANGEINDEXEDPROC glDepthRangeIndexed = nullptr;
  PFNGLGETFLOATI_VPROC glGetFloati_v = nullptr;
  PFNGLGETDOUBLEI_VPROC glGetDoublei_v = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return glReleaseShaderCompiler != nullptr && glShaderBinary != nullptr && glGetShaderPrecisionFormat != nullptr &&
	   glDepthRangef != nullptr && glClearDepthf != nullptr && glGetProgramBinary != nullptr &&
	   glProgramBinary != nullptr && glProgramParameteri != nullptr && glUseProgramStages != nullptr &&
	   glActiveShaderProgram != nullptr && glCreateShaderProgramv != nullptr && glBindProgramPipeline != nullptr &&
	   glDeleteProgramPipelines != nullptr && glGenProgramPipelines != nullptr && glIsProgramPipeline != nullptr &&
	   glGetProgramPipelineiv != nullptr && glProgramUniform1i != nullptr && glProgramUniform1iv != nullptr &&
	   glProgramUniform1f != nullptr && glProgramUniform1fv != nullptr && glProgramUniform1d != nullptr &&
	   glProgramUniform1dv != nullptr && glProgramUniform1ui != nullptr && glProgramUniform1uiv != nullptr &&
	   glProgramUniform2i != nullptr && glProgramUniform2iv != nullptr && glProgramUniform2f != nullptr &&
	   glProgramUniform2fv != nullptr && glProgramUniform2d != nullptr && glProgramUniform2dv != nullptr &&
	   glProgramUniform2ui != nullptr && glProgramUniform2uiv != nullptr && glProgramUniform3i != nullptr &&
	   glProgramUniform3iv != nullptr && glProgramUniform3f != nullptr && glProgramUniform3fv != nullptr &&
	   glProgramUniform3d != nullptr && glProgramUniform3dv != nullptr && glProgramUniform3ui != nullptr &&
	   glProgramUniform3uiv != nullptr && glProgramUniform4i != nullptr && glProgramUniform4iv != nullptr &&
	   glProgramUniform4f != nullptr && glProgramUniform4fv != nullptr && glProgramUniform4d != nullptr &&
	   glProgramUniform4dv != nullptr && glProgramUniform4ui != nullptr && glProgramUniform4uiv != nullptr &&
	   glProgramUniformMatrix2fv != nullptr && glProgramUniformMatrix3fv != nullptr &&
	   glProgramUniformMatrix4fv != nullptr && glProgramUniformMatrix2dv != nullptr &&
	   glProgramUniformMatrix3dv != nullptr && glProgramUniformMatrix4dv != nullptr &&
	   glProgramUniformMatrix2x3fv != nullptr && glProgramUniformMatrix3x2fv != nullptr &&
	   glProgramUniformMatrix2x4fv != nullptr && glProgramUniformMatrix4x2fv != nullptr &&
	   glProgramUniformMatrix3x4fv != nullptr && glProgramUniformMatrix4x3fv != nullptr &&
	   glProgramUniformMatrix2x3dv != nullptr && glProgramUniformMatrix3x2dv != nullptr &&
	   glProgramUniformMatrix2x4dv != nullptr && glProgramUniformMatrix4x2dv != nullptr &&
	   glProgramUniformMatrix3x4dv != nullptr && glProgramUniformMatrix4x3dv != nullptr &&
	   glValidateProgramPipeline != nullptr && glGetProgramPipelineInfoLog != nullptr &&
	   glVertexAttribL1d != nullptr && glVertexAttribL2d != nullptr && glVertexAttribL3d != nullptr &&
	   glVertexAttribL4d != nullptr && glVertexAttribL1dv != nullptr && glVertexAttribL2dv != nullptr &&
	   glVertexAttribL3dv != nullptr && glVertexAttribL4dv != nullptr && glVertexAttribLPointer != nullptr &&
	   glGetVertexAttribLdv != nullptr && glViewportArrayv != nullptr && glViewportIndexedf != nullptr &&
	   glViewportIndexedfv != nullptr && glScissorArrayv != nullptr && glScissorIndexed != nullptr &&
	   glScissorIndexedv != nullptr && glDepthRangeArrayv != nullptr && glDepthRangeIndexed != nullptr &&
	   glGetFloati_v != nullptr && glGetDoublei_v != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl

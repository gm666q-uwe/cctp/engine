#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/gl.h"

namespace engine::graphics::gl {
struct ContextDataVersion33 final : public IContextData
{
public:
  // GL_VERSION_3_3
  PFNGLBINDFRAGDATALOCATIONINDEXEDPROC glBindFragDataLocationIndexed = nullptr;
  PFNGLGETFRAGDATAINDEXPROC glGetFragDataIndex = nullptr;
  PFNGLGENSAMPLERSPROC glGenSamplers = nullptr;
  PFNGLDELETESAMPLERSPROC glDeleteSamplers = nullptr;
  PFNGLISSAMPLERPROC glIsSampler = nullptr;
  PFNGLBINDSAMPLERPROC glBindSampler = nullptr;
  PFNGLSAMPLERPARAMETERIPROC glSamplerParameteri = nullptr;
  PFNGLSAMPLERPARAMETERIVPROC glSamplerParameteriv = nullptr;
  PFNGLSAMPLERPARAMETERFPROC glSamplerParameterf = nullptr;
  PFNGLSAMPLERPARAMETERFVPROC glSamplerParameterfv = nullptr;
  PFNGLSAMPLERPARAMETERIIVPROC glSamplerParameterIiv = nullptr;
  PFNGLSAMPLERPARAMETERIUIVPROC glSamplerParameterIuiv = nullptr;
  PFNGLGETSAMPLERPARAMETERIVPROC glGetSamplerParameteriv = nullptr;
  PFNGLGETSAMPLERPARAMETERIIVPROC glGetSamplerParameterIiv = nullptr;
  PFNGLGETSAMPLERPARAMETERFVPROC glGetSamplerParameterfv = nullptr;
  PFNGLGETSAMPLERPARAMETERIUIVPROC glGetSamplerParameterIuiv = nullptr;
  PFNGLQUERYCOUNTERPROC glQueryCounter = nullptr;
  PFNGLGETQUERYOBJECTI64VPROC glGetQueryObjecti64v = nullptr;
  PFNGLGETQUERYOBJECTUI64VPROC glGetQueryObjectui64v = nullptr;
  PFNGLVERTEXATTRIBDIVISORPROC glVertexAttribDivisor = nullptr;
  PFNGLVERTEXATTRIBP1UIPROC glVertexAttribP1ui = nullptr;
  PFNGLVERTEXATTRIBP1UIVPROC glVertexAttribP1uiv = nullptr;
  PFNGLVERTEXATTRIBP2UIPROC glVertexAttribP2ui = nullptr;
  PFNGLVERTEXATTRIBP2UIVPROC glVertexAttribP2uiv = nullptr;
  PFNGLVERTEXATTRIBP3UIPROC glVertexAttribP3ui = nullptr;
  PFNGLVERTEXATTRIBP3UIVPROC glVertexAttribP3uiv = nullptr;
  PFNGLVERTEXATTRIBP4UIPROC glVertexAttribP4ui = nullptr;
  PFNGLVERTEXATTRIBP4UIVPROC glVertexAttribP4uiv = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return glBindFragDataLocationIndexed != nullptr && glGetFragDataIndex != nullptr && glGenSamplers != nullptr &&
	   glDeleteSamplers != nullptr && glIsSampler != nullptr && glBindSampler != nullptr &&
	   glSamplerParameteri != nullptr && glSamplerParameteriv != nullptr && glSamplerParameterf != nullptr &&
	   glSamplerParameterfv != nullptr && glSamplerParameterIiv != nullptr && glSamplerParameterIuiv != nullptr &&
	   glGetSamplerParameteriv != nullptr && glGetSamplerParameterIiv != nullptr &&
	   glGetSamplerParameterfv != nullptr && glGetSamplerParameterIuiv != nullptr && glQueryCounter != nullptr &&
	   glGetQueryObjecti64v != nullptr && glGetQueryObjectui64v != nullptr && glVertexAttribDivisor != nullptr &&
	   glVertexAttribP1ui != nullptr && glVertexAttribP1uiv != nullptr && glVertexAttribP2ui != nullptr &&
	   glVertexAttribP2uiv != nullptr && glVertexAttribP3ui != nullptr && glVertexAttribP3uiv != nullptr &&
	   glVertexAttribP4ui != nullptr && glVertexAttribP4uiv != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl

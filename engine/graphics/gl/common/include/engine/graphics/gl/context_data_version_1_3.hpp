#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/gl.h"

namespace engine::graphics::gl {
struct ContextDataVersion13 final : public IContextData
{
public:
  // GL_VERSION_1_3
  PFNGLACTIVETEXTUREPROC glActiveTexture = nullptr;
  PFNGLSAMPLECOVERAGEPROC glSampleCoverage = nullptr;
  PFNGLCOMPRESSEDTEXIMAGE3DPROC glCompressedTexImage3D = nullptr;
  PFNGLCOMPRESSEDTEXIMAGE2DPROC glCompressedTexImage2D = nullptr;
  PFNGLCOMPRESSEDTEXIMAGE1DPROC glCompressedTexImage1D = nullptr;
  PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC glCompressedTexSubImage3D = nullptr;
  PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC glCompressedTexSubImage2D = nullptr;
  PFNGLCOMPRESSEDTEXSUBIMAGE1DPROC glCompressedTexSubImage1D = nullptr;
  PFNGLGETCOMPRESSEDTEXIMAGEPROC glGetCompressedTexImage = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return glActiveTexture != nullptr && glSampleCoverage != nullptr && glCompressedTexImage3D != nullptr &&
	   glCompressedTexImage2D != nullptr && glCompressedTexImage1D != nullptr &&
	   glCompressedTexSubImage3D != nullptr && glCompressedTexSubImage2D != nullptr &&
	   glCompressedTexSubImage1D != nullptr && glGetCompressedTexImage != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl

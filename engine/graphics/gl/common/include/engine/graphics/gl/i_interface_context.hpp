#pragma once

#include <string_view>

#include <result/result.hpp>

#include <engine/error.hpp>

namespace engine::graphics::gl {
class Context;

class IInterfaceContext
{
public:
  constexpr IInterfaceContext() noexcept = default;

  constexpr IInterfaceContext(IInterfaceContext const&) noexcept = default;

  constexpr IInterfaceContext(IInterfaceContext&&) noexcept = default;

  virtual constexpr ~IInterfaceContext() noexcept = default;

  constexpr auto operator=(IInterfaceContext const&) & noexcept -> IInterfaceContext& = default;

  constexpr auto operator=(IInterfaceContext&&) & noexcept -> IInterfaceContext& = default;

  [[nodiscard]] virtual auto get_proc_address(std::string_view name) const& noexcept
    -> result::Result<void (*)(void), Error> = 0;

  [[nodiscard]] virtual auto gl() const& noexcept -> Context& = 0;

  [[nodiscard]] virtual auto is_current() const& noexcept -> bool = 0;

  [[nodiscard]] virtual constexpr auto is_loaded(bool include_gl) const& noexcept -> bool = 0;

  virtual auto load(bool include_gl) & noexcept -> void = 0;

  [[nodiscard]] virtual auto make_current() const& noexcept -> result::Result<std::nullptr_t, Error> = 0;

  [[nodiscard]] virtual auto release_current() const& noexcept -> result::Result<std::nullptr_t, Error> = 0;

  [[nodiscard]] virtual auto swap_buffers() const& noexcept -> result::Result<std::nullptr_t, Error> = 0;

  [[nodiscard]] virtual auto swap_interval(i32 interval) const& noexcept -> result::Result<std::nullptr_t, Error> = 0;

protected:
private:
};
} // namespace engine::graphics::gl

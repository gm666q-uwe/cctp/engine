#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/gl.h"

namespace engine::graphics::gl {
struct ContextDataVersion42 final : public IContextData
{
public:
  // GL_VERSION_4_2
  PFNGLDRAWARRAYSINSTANCEDBASEINSTANCEPROC glDrawArraysInstancedBaseInstance = nullptr;
  PFNGLDRAWELEMENTSINSTANCEDBASEINSTANCEPROC glDrawElementsInstancedBaseInstance = nullptr;
  PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXBASEINSTANCEPROC glDrawElementsInstancedBaseVertexBaseInstance = nullptr;
  PFNGLGETINTERNALFORMATIVPROC glGetInternalformativ = nullptr;
  PFNGLGETACTIVEATOMICCOUNTERBUFFERIVPROC glGetActiveAtomicCounterBufferiv = nullptr;
  PFNGLBINDIMAGETEXTUREPROC glBindImageTexture = nullptr;
  PFNGLMEMORYBARRIERPROC glMemoryBarrier = nullptr;
  PFNGLTEXSTORAGE1DPROC glTexStorage1D = nullptr;
  PFNGLTEXSTORAGE2DPROC glTexStorage2D = nullptr;
  PFNGLTEXSTORAGE3DPROC glTexStorage3D = nullptr;
  PFNGLDRAWTRANSFORMFEEDBACKINSTANCEDPROC glDrawTransformFeedbackInstanced = nullptr;
  PFNGLDRAWTRANSFORMFEEDBACKSTREAMINSTANCEDPROC glDrawTransformFeedbackStreamInstanced = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return glDrawArraysInstancedBaseInstance != nullptr && glDrawElementsInstancedBaseInstance != nullptr &&
	   glDrawElementsInstancedBaseVertexBaseInstance != nullptr && glGetInternalformativ != nullptr &&
	   glGetActiveAtomicCounterBufferiv != nullptr && glBindImageTexture != nullptr && glMemoryBarrier != nullptr &&
	   glTexStorage1D != nullptr && glTexStorage2D != nullptr && glTexStorage3D != nullptr &&
	   glDrawTransformFeedbackInstanced != nullptr && glDrawTransformFeedbackStreamInstanced != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl

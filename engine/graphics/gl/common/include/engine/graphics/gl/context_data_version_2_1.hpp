#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/gl.h"

namespace engine::graphics::gl {
struct ContextDataVersion21 final : public IContextData
{
public:
  // GL_VERSION_2_1
  PFNGLUNIFORMMATRIX2X3FVPROC glUniformMatrix2x3fv = nullptr;
  PFNGLUNIFORMMATRIX3X2FVPROC glUniformMatrix3x2fv = nullptr;
  PFNGLUNIFORMMATRIX2X4FVPROC glUniformMatrix2x4fv = nullptr;
  PFNGLUNIFORMMATRIX4X2FVPROC glUniformMatrix4x2fv = nullptr;
  PFNGLUNIFORMMATRIX3X4FVPROC glUniformMatrix3x4fv = nullptr;
  PFNGLUNIFORMMATRIX4X3FVPROC glUniformMatrix4x3fv = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override  {
    return glUniformMatrix2x3fv != nullptr && glUniformMatrix3x2fv != nullptr && glUniformMatrix2x4fv != nullptr &&
	   glUniformMatrix4x2fv != nullptr && glUniformMatrix3x4fv != nullptr && glUniformMatrix4x3fv != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl

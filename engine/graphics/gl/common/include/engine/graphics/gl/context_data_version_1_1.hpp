#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/gl.h"

namespace engine::graphics::gl {
struct ContextDataVersion11 final : public IContextData
{
public:
  // GL_VERSION_1_1
  PFNGLDRAWARRAYSPROC glDrawArrays = nullptr;
  PFNGLDRAWELEMENTSPROC glDrawElements = nullptr;
  PFNGLGETPOINTERVPROC glGetPointerv = nullptr;
  PFNGLPOLYGONOFFSETPROC glPolygonOffset = nullptr;
  PFNGLCOPYTEXIMAGE1DPROC glCopyTexImage1D = nullptr;
  PFNGLCOPYTEXIMAGE2DPROC glCopyTexImage2D = nullptr;
  PFNGLCOPYTEXSUBIMAGE1DPROC glCopyTexSubImage1D = nullptr;
  PFNGLCOPYTEXSUBIMAGE2DPROC glCopyTexSubImage2D = nullptr;
  PFNGLTEXSUBIMAGE1DPROC glTexSubImage1D = nullptr;
  PFNGLTEXSUBIMAGE2DPROC glTexSubImage2D = nullptr;
  PFNGLBINDTEXTUREPROC glBindTexture = nullptr;
  PFNGLDELETETEXTURESPROC glDeleteTextures = nullptr;
  PFNGLGENTEXTURESPROC glGenTextures = nullptr;
  PFNGLISTEXTUREPROC glIsTexture = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return glDrawArrays != nullptr && glDrawElements != nullptr && glGetPointerv != nullptr &&
	   glPolygonOffset != nullptr && glCopyTexImage1D != nullptr && glCopyTexImage2D != nullptr &&
	   glCopyTexSubImage1D != nullptr && glCopyTexSubImage2D != nullptr && glTexSubImage1D != nullptr &&
	   glTexSubImage2D != nullptr && glBindTexture != nullptr && glDeleteTextures != nullptr &&
	   glGenTextures != nullptr && glIsTexture;
  }

protected:
private:
};
} // namespace engine::graphics::gl

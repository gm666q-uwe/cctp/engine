#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/gl.h"

namespace engine::graphics::gl {
struct ContextDataVersion10 final : public IContextData
{
public:
  // GL_VERSION_1_0
  PFNGLCULLFACEPROC glCullFace = nullptr;
  PFNGLFRONTFACEPROC glFrontFace = nullptr;
  PFNGLHINTPROC glHint = nullptr;
  PFNGLLINEWIDTHPROC glLineWidth = nullptr;
  PFNGLPOINTSIZEPROC glPointSize = nullptr;
  PFNGLPOLYGONMODEPROC glPolygonMode = nullptr;
  PFNGLSCISSORPROC glScissor = nullptr;
  PFNGLTEXPARAMETERFPROC glTexParameterf = nullptr;
  PFNGLTEXPARAMETERFVPROC glTexParameterfv = nullptr;
  PFNGLTEXPARAMETERIPROC glTexParameteri = nullptr;
  PFNGLTEXPARAMETERIVPROC glTexParameteriv = nullptr;
  PFNGLTEXIMAGE1DPROC glTexImage1D = nullptr;
  PFNGLTEXIMAGE2DPROC glTexImage2D = nullptr;
  PFNGLDRAWBUFFERPROC glDrawBuffer = nullptr;
  PFNGLCLEARPROC glClear = nullptr;
  PFNGLCLEARCOLORPROC glClearColor = nullptr;
  PFNGLCLEARSTENCILPROC glClearStencil = nullptr;
  PFNGLCLEARDEPTHPROC glClearDepth = nullptr;
  PFNGLSTENCILMASKPROC glStencilMask = nullptr;
  PFNGLCOLORMASKPROC glColorMask = nullptr;
  PFNGLDEPTHMASKPROC glDepthMask = nullptr;
  PFNGLDISABLEPROC glDisable = nullptr;
  PFNGLENABLEPROC glEnable = nullptr;
  PFNGLFINISHPROC glFinish = nullptr;
  PFNGLFLUSHPROC glFlush = nullptr;
  PFNGLBLENDFUNCPROC glBlendFunc = nullptr;
  PFNGLLOGICOPPROC glLogicOp = nullptr;
  PFNGLSTENCILFUNCPROC glStencilFunc = nullptr;
  PFNGLSTENCILOPPROC glStencilOp = nullptr;
  PFNGLDEPTHFUNCPROC glDepthFunc = nullptr;
  PFNGLPIXELSTOREFPROC glPixelStoref = nullptr;
  PFNGLPIXELSTOREIPROC glPixelStorei = nullptr;
  PFNGLREADBUFFERPROC glReadBuffer = nullptr;
  PFNGLREADPIXELSPROC glReadPixels = nullptr;
  PFNGLGETBOOLEANVPROC glGetBooleanv = nullptr;
  PFNGLGETDOUBLEVPROC glGetDoublev = nullptr;
  PFNGLGETERRORPROC glGetError = nullptr;
  PFNGLGETFLOATVPROC glGetFloatv = nullptr;
  PFNGLGETINTEGERVPROC glGetIntegerv = nullptr;
  PFNGLGETSTRINGPROC glGetString = nullptr;
  PFNGLGETTEXIMAGEPROC glGetTexImage = nullptr;
  PFNGLGETTEXPARAMETERFVPROC glGetTexParameterfv = nullptr;
  PFNGLGETTEXPARAMETERIVPROC glGetTexParameteriv = nullptr;
  PFNGLGETTEXLEVELPARAMETERFVPROC glGetTexLevelParameterfv = nullptr;
  PFNGLGETTEXLEVELPARAMETERIVPROC glGetTexLevelParameteriv = nullptr;
  PFNGLISENABLEDPROC glIsEnabled = nullptr;
  PFNGLDEPTHRANGEPROC glDepthRange = nullptr;
  PFNGLVIEWPORTPROC glViewport = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return glCullFace != nullptr && glFrontFace != nullptr && glHint != nullptr && glLineWidth != nullptr &&
	   glPointSize != nullptr && glPolygonMode != nullptr && glScissor != nullptr && glTexParameterf != nullptr &&
	   glTexParameterfv != nullptr && glTexParameteri != nullptr && glTexParameteriv != nullptr &&
	   glTexImage1D != nullptr && glTexImage2D != nullptr && glDrawBuffer != nullptr && glClear != nullptr &&
	   glClearColor != nullptr && glClearStencil != nullptr && glClearDepth != nullptr &&
	   glStencilMask != nullptr && glColorMask != nullptr && glDepthMask != nullptr && glDisable != nullptr &&
	   glEnable != nullptr && glFinish != nullptr && glFlush != nullptr && glBlendFunc != nullptr &&
	   glLogicOp != nullptr && glStencilFunc != nullptr && glStencilOp != nullptr && glDepthFunc != nullptr &&
	   glPixelStoref != nullptr && glPixelStorei != nullptr && glReadBuffer != nullptr && glReadPixels != nullptr &&
	   glGetBooleanv != nullptr && glGetDoublev != nullptr && glGetError != nullptr && glGetFloatv != nullptr &&
	   glGetIntegerv != nullptr && glGetString != nullptr && glGetTexImage != nullptr &&
	   glGetTexParameterfv != nullptr && glGetTexParameteriv != nullptr && glGetTexLevelParameterfv != nullptr &&
	   glGetTexLevelParameteriv != nullptr && glIsEnabled != nullptr && glDepthRange != nullptr &&
	   glViewport != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl

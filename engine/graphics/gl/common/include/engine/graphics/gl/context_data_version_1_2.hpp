#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/gl.h"

namespace engine::graphics::gl {
struct ContextDataVersion12 final : public IContextData
{
public:
  // GL_VERSION_1_2
  PFNGLDRAWRANGEELEMENTSPROC glDrawRangeElements = nullptr;
  PFNGLTEXIMAGE3DPROC glTexImage3D = nullptr;
  PFNGLTEXSUBIMAGE3DPROC glTexSubImage3D = nullptr;
  PFNGLCOPYTEXSUBIMAGE3DPROC glCopyTexSubImage3D = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return glDrawRangeElements != nullptr && glTexImage3D != nullptr && glTexSubImage3D != nullptr &&
	   glCopyTexSubImage3D != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl

#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/gl.h"

namespace engine::graphics::gl {
struct ContextDataVersion14 final : public IContextData
{
public:
  // GL_VERSION_1_4
  PFNGLBLENDFUNCSEPARATEPROC glBlendFuncSeparate = nullptr;
  PFNGLMULTIDRAWARRAYSPROC glMultiDrawArrays = nullptr;
  PFNGLMULTIDRAWELEMENTSPROC glMultiDrawElements = nullptr;
  PFNGLPOINTPARAMETERFPROC glPointParameterf = nullptr;
  PFNGLPOINTPARAMETERFVPROC glPointParameterfv = nullptr;
  PFNGLPOINTPARAMETERIPROC glPointParameteri = nullptr;
  PFNGLPOINTPARAMETERIVPROC glPointParameteriv = nullptr;
  PFNGLBLENDCOLORPROC glBlendColor = nullptr;
  PFNGLBLENDEQUATIONPROC glBlendEquation = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return glBlendFuncSeparate != nullptr && glMultiDrawArrays != nullptr && glMultiDrawElements != nullptr &&
	   glPointParameterf != nullptr && glPointParameterfv != nullptr && glPointParameteri != nullptr &&
	   glPointParameteriv != nullptr && glBlendColor != nullptr && glBlendEquation != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl

#pragma once

#include <engine/graphics/gl/i_context_data.hpp>

#include "engine/graphics/gl/gl.h"

namespace engine::graphics::gl {
struct ContextDataVersion45 final : public IContextData
{
public:
  // GL_VERSION_4_5
  PFNGLCLIPCONTROLPROC glClipControl = nullptr;
  PFNGLCREATETRANSFORMFEEDBACKSPROC glCreateTransformFeedbacks = nullptr;
  PFNGLTRANSFORMFEEDBACKBUFFERBASEPROC glTransformFeedbackBufferBase = nullptr;
  PFNGLTRANSFORMFEEDBACKBUFFERRANGEPROC glTransformFeedbackBufferRange = nullptr;
  PFNGLGETTRANSFORMFEEDBACKIVPROC glGetTransformFeedbackiv = nullptr;
  PFNGLGETTRANSFORMFEEDBACKI_VPROC glGetTransformFeedbacki_v = nullptr;
  PFNGLGETTRANSFORMFEEDBACKI64_VPROC glGetTransformFeedbacki64_v = nullptr;
  PFNGLCREATEBUFFERSPROC glCreateBuffers = nullptr;
  PFNGLNAMEDBUFFERSTORAGEPROC glNamedBufferStorage = nullptr;
  PFNGLNAMEDBUFFERDATAPROC glNamedBufferData = nullptr;
  PFNGLNAMEDBUFFERSUBDATAPROC glNamedBufferSubData = nullptr;
  PFNGLCOPYNAMEDBUFFERSUBDATAPROC glCopyNamedBufferSubData = nullptr;
  PFNGLCLEARNAMEDBUFFERDATAPROC glClearNamedBufferData = nullptr;
  PFNGLCLEARNAMEDBUFFERSUBDATAPROC glClearNamedBufferSubData = nullptr;
  PFNGLMAPNAMEDBUFFERPROC glMapNamedBuffer = nullptr;
  PFNGLMAPNAMEDBUFFERRANGEPROC glMapNamedBufferRange = nullptr;
  PFNGLUNMAPNAMEDBUFFERPROC glUnmapNamedBuffer = nullptr;
  PFNGLFLUSHMAPPEDNAMEDBUFFERRANGEPROC glFlushMappedNamedBufferRange = nullptr;
  PFNGLGETNAMEDBUFFERPARAMETERIVPROC glGetNamedBufferParameteriv = nullptr;
  PFNGLGETNAMEDBUFFERPARAMETERI64VPROC glGetNamedBufferParameteri64v = nullptr;
  PFNGLGETNAMEDBUFFERPOINTERVPROC glGetNamedBufferPointerv = nullptr;
  PFNGLGETNAMEDBUFFERSUBDATAPROC glGetNamedBufferSubData = nullptr;
  PFNGLCREATEFRAMEBUFFERSPROC glCreateFramebuffers = nullptr;
  PFNGLNAMEDFRAMEBUFFERRENDERBUFFERPROC glNamedFramebufferRenderbuffer = nullptr;
  PFNGLNAMEDFRAMEBUFFERPARAMETERIPROC glNamedFramebufferParameteri = nullptr;
  PFNGLNAMEDFRAMEBUFFERTEXTUREPROC glNamedFramebufferTexture = nullptr;
  PFNGLNAMEDFRAMEBUFFERTEXTURELAYERPROC glNamedFramebufferTextureLayer = nullptr;
  PFNGLNAMEDFRAMEBUFFERDRAWBUFFERPROC glNamedFramebufferDrawBuffer = nullptr;
  PFNGLNAMEDFRAMEBUFFERDRAWBUFFERSPROC glNamedFramebufferDrawBuffers = nullptr;
  PFNGLNAMEDFRAMEBUFFERREADBUFFERPROC glNamedFramebufferReadBuffer = nullptr;
  PFNGLINVALIDATENAMEDFRAMEBUFFERDATAPROC glInvalidateNamedFramebufferData = nullptr;
  PFNGLINVALIDATENAMEDFRAMEBUFFERSUBDATAPROC glInvalidateNamedFramebufferSubData = nullptr;
  PFNGLCLEARNAMEDFRAMEBUFFERIVPROC glClearNamedFramebufferiv = nullptr;
  PFNGLCLEARNAMEDFRAMEBUFFERUIVPROC glClearNamedFramebufferuiv = nullptr;
  PFNGLCLEARNAMEDFRAMEBUFFERFVPROC glClearNamedFramebufferfv = nullptr;
  PFNGLCLEARNAMEDFRAMEBUFFERFIPROC glClearNamedFramebufferfi = nullptr;
  PFNGLBLITNAMEDFRAMEBUFFERPROC glBlitNamedFramebuffer = nullptr;
  PFNGLCHECKNAMEDFRAMEBUFFERSTATUSPROC glCheckNamedFramebufferStatus = nullptr;
  PFNGLGETNAMEDFRAMEBUFFERPARAMETERIVPROC glGetNamedFramebufferParameteriv = nullptr;
  PFNGLGETNAMEDFRAMEBUFFERATTACHMENTPARAMETERIVPROC glGetNamedFramebufferAttachmentParameteriv = nullptr;
  PFNGLCREATERENDERBUFFERSPROC glCreateRenderbuffers = nullptr;
  PFNGLNAMEDRENDERBUFFERSTORAGEPROC glNamedRenderbufferStorage = nullptr;
  PFNGLNAMEDRENDERBUFFERSTORAGEMULTISAMPLEPROC glNamedRenderbufferStorageMultisample = nullptr;
  PFNGLGETNAMEDRENDERBUFFERPARAMETERIVPROC glGetNamedRenderbufferParameteriv = nullptr;
  PFNGLCREATETEXTURESPROC glCreateTextures = nullptr;
  PFNGLTEXTUREBUFFERPROC glTextureBuffer = nullptr;
  PFNGLTEXTUREBUFFERRANGEPROC glTextureBufferRange = nullptr;
  PFNGLTEXTURESTORAGE1DPROC glTextureStorage1D = nullptr;
  PFNGLTEXTURESTORAGE2DPROC glTextureStorage2D = nullptr;
  PFNGLTEXTURESTORAGE3DPROC glTextureStorage3D = nullptr;
  PFNGLTEXTURESTORAGE2DMULTISAMPLEPROC glTextureStorage2DMultisample = nullptr;
  PFNGLTEXTURESTORAGE3DMULTISAMPLEPROC glTextureStorage3DMultisample = nullptr;
  PFNGLTEXTURESUBIMAGE1DPROC glTextureSubImage1D = nullptr;
  PFNGLTEXTURESUBIMAGE2DPROC glTextureSubImage2D = nullptr;
  PFNGLTEXTURESUBIMAGE3DPROC glTextureSubImage3D = nullptr;
  PFNGLCOMPRESSEDTEXTURESUBIMAGE1DPROC glCompressedTextureSubImage1D = nullptr;
  PFNGLCOMPRESSEDTEXTURESUBIMAGE2DPROC glCompressedTextureSubImage2D = nullptr;
  PFNGLCOMPRESSEDTEXTURESUBIMAGE3DPROC glCompressedTextureSubImage3D = nullptr;
  PFNGLCOPYTEXTURESUBIMAGE1DPROC glCopyTextureSubImage1D = nullptr;
  PFNGLCOPYTEXTURESUBIMAGE2DPROC glCopyTextureSubImage2D = nullptr;
  PFNGLCOPYTEXTURESUBIMAGE3DPROC glCopyTextureSubImage3D = nullptr;
  PFNGLTEXTUREPARAMETERFPROC glTextureParameterf = nullptr;
  PFNGLTEXTUREPARAMETERFVPROC glTextureParameterfv = nullptr;
  PFNGLTEXTUREPARAMETERIPROC glTextureParameteri = nullptr;
  PFNGLTEXTUREPARAMETERIIVPROC glTextureParameterIiv = nullptr;
  PFNGLTEXTUREPARAMETERIUIVPROC glTextureParameterIuiv = nullptr;
  PFNGLTEXTUREPARAMETERIVPROC glTextureParameteriv = nullptr;
  PFNGLGENERATETEXTUREMIPMAPPROC glGenerateTextureMipmap = nullptr;
  PFNGLBINDTEXTUREUNITPROC glBindTextureUnit = nullptr;
  PFNGLGETTEXTUREIMAGEPROC glGetTextureImage = nullptr;
  PFNGLGETCOMPRESSEDTEXTUREIMAGEPROC glGetCompressedTextureImage = nullptr;
  PFNGLGETTEXTURELEVELPARAMETERFVPROC glGetTextureLevelParameterfv = nullptr;
  PFNGLGETTEXTURELEVELPARAMETERIVPROC glGetTextureLevelParameteriv = nullptr;
  PFNGLGETTEXTUREPARAMETERFVPROC glGetTextureParameterfv = nullptr;
  PFNGLGETTEXTUREPARAMETERIIVPROC glGetTextureParameterIiv = nullptr;
  PFNGLGETTEXTUREPARAMETERIUIVPROC glGetTextureParameterIuiv = nullptr;
  PFNGLGETTEXTUREPARAMETERIVPROC glGetTextureParameteriv = nullptr;
  PFNGLCREATEVERTEXARRAYSPROC glCreateVertexArrays = nullptr;
  PFNGLDISABLEVERTEXARRAYATTRIBPROC glDisableVertexArrayAttrib = nullptr;
  PFNGLENABLEVERTEXARRAYATTRIBPROC glEnableVertexArrayAttrib = nullptr;
  PFNGLVERTEXARRAYELEMENTBUFFERPROC glVertexArrayElementBuffer = nullptr;
  PFNGLVERTEXARRAYVERTEXBUFFERPROC glVertexArrayVertexBuffer = nullptr;
  PFNGLVERTEXARRAYVERTEXBUFFERSPROC glVertexArrayVertexBuffers = nullptr;
  PFNGLVERTEXARRAYATTRIBBINDINGPROC glVertexArrayAttribBinding = nullptr;
  PFNGLVERTEXARRAYATTRIBFORMATPROC glVertexArrayAttribFormat = nullptr;
  PFNGLVERTEXARRAYATTRIBIFORMATPROC glVertexArrayAttribIFormat = nullptr;
  PFNGLVERTEXARRAYATTRIBLFORMATPROC glVertexArrayAttribLFormat = nullptr;
  PFNGLVERTEXARRAYBINDINGDIVISORPROC glVertexArrayBindingDivisor = nullptr;
  PFNGLGETVERTEXARRAYIVPROC glGetVertexArrayiv = nullptr;
  PFNGLGETVERTEXARRAYINDEXEDIVPROC glGetVertexArrayIndexediv = nullptr;
  PFNGLGETVERTEXARRAYINDEXED64IVPROC glGetVertexArrayIndexed64iv = nullptr;
  PFNGLCREATESAMPLERSPROC glCreateSamplers = nullptr;
  PFNGLCREATEPROGRAMPIPELINESPROC glCreateProgramPipelines = nullptr;
  PFNGLCREATEQUERIESPROC glCreateQueries = nullptr;
  PFNGLGETQUERYBUFFEROBJECTI64VPROC glGetQueryBufferObjecti64v = nullptr;
  PFNGLGETQUERYBUFFEROBJECTIVPROC glGetQueryBufferObjectiv = nullptr;
  PFNGLGETQUERYBUFFEROBJECTUI64VPROC glGetQueryBufferObjectui64v = nullptr;
  PFNGLGETQUERYBUFFEROBJECTUIVPROC glGetQueryBufferObjectuiv = nullptr;
  PFNGLMEMORYBARRIERBYREGIONPROC glMemoryBarrierByRegion = nullptr;
  PFNGLGETTEXTURESUBIMAGEPROC glGetTextureSubImage = nullptr;
  PFNGLGETCOMPRESSEDTEXTURESUBIMAGEPROC glGetCompressedTextureSubImage = nullptr;
  PFNGLGETGRAPHICSRESETSTATUSPROC glGetGraphicsResetStatus = nullptr;
  PFNGLGETNCOMPRESSEDTEXIMAGEPROC glGetnCompressedTexImage = nullptr;
  PFNGLGETNTEXIMAGEPROC glGetnTexImage = nullptr;
  PFNGLGETNUNIFORMDVPROC glGetnUniformdv = nullptr;
  PFNGLGETNUNIFORMFVPROC glGetnUniformfv = nullptr;
  PFNGLGETNUNIFORMIVPROC glGetnUniformiv = nullptr;
  PFNGLGETNUNIFORMUIVPROC glGetnUniformuiv = nullptr;
  PFNGLREADNPIXELSPROC glReadnPixels = nullptr;
  PFNGLTEXTUREBARRIERPROC glTextureBarrier = nullptr;

  [[nodiscard]] constexpr auto is_loaded() const& noexcept -> bool override
  {
    return glClipControl != nullptr && glCreateTransformFeedbacks != nullptr &&
	   glTransformFeedbackBufferBase != nullptr && glTransformFeedbackBufferRange != nullptr &&
	   glGetTransformFeedbackiv != nullptr && glGetTransformFeedbacki_v != nullptr &&
	   glGetTransformFeedbacki64_v != nullptr && glCreateBuffers != nullptr && glNamedBufferStorage != nullptr &&
	   glNamedBufferData != nullptr && glNamedBufferSubData != nullptr && glCopyNamedBufferSubData != nullptr &&
	   glClearNamedBufferData != nullptr && glClearNamedBufferSubData != nullptr && glMapNamedBuffer != nullptr &&
	   glMapNamedBufferRange != nullptr && glUnmapNamedBuffer != nullptr &&
	   glFlushMappedNamedBufferRange != nullptr && glGetNamedBufferParameteriv != nullptr &&
	   glGetNamedBufferParameteri64v != nullptr && glGetNamedBufferPointerv != nullptr &&
	   glGetNamedBufferSubData != nullptr && glCreateFramebuffers != nullptr &&
	   glNamedFramebufferRenderbuffer != nullptr && glNamedFramebufferParameteri != nullptr &&
	   glNamedFramebufferTexture != nullptr && glNamedFramebufferTextureLayer != nullptr &&
	   glNamedFramebufferDrawBuffer != nullptr && glNamedFramebufferDrawBuffers != nullptr &&
	   glNamedFramebufferReadBuffer != nullptr && glInvalidateNamedFramebufferData != nullptr &&
	   glInvalidateNamedFramebufferSubData != nullptr && glClearNamedFramebufferiv != nullptr &&
	   glClearNamedFramebufferuiv != nullptr && glClearNamedFramebufferfv != nullptr &&
	   glClearNamedFramebufferfi != nullptr && glBlitNamedFramebuffer != nullptr &&
	   glCheckNamedFramebufferStatus != nullptr && glGetNamedFramebufferParameteriv != nullptr &&
	   glGetNamedFramebufferAttachmentParameteriv != nullptr && glCreateRenderbuffers != nullptr &&
	   glNamedRenderbufferStorage != nullptr && glNamedRenderbufferStorageMultisample != nullptr &&
	   glGetNamedRenderbufferParameteriv != nullptr && glCreateTextures != nullptr && glTextureBuffer != nullptr &&
	   glTextureBufferRange != nullptr && glTextureStorage1D != nullptr && glTextureStorage2D != nullptr &&
	   glTextureStorage3D != nullptr && glTextureStorage2DMultisample != nullptr &&
	   glTextureStorage3DMultisample != nullptr && glTextureSubImage1D != nullptr &&
	   glTextureSubImage2D != nullptr && glTextureSubImage3D != nullptr &&
	   glCompressedTextureSubImage1D != nullptr && glCompressedTextureSubImage2D != nullptr &&
	   glCompressedTextureSubImage3D != nullptr && glCopyTextureSubImage1D != nullptr &&
	   glCopyTextureSubImage2D != nullptr && glCopyTextureSubImage3D != nullptr && glTextureParameterf != nullptr &&
	   glTextureParameterfv != nullptr && glTextureParameteri != nullptr && glTextureParameterIiv != nullptr &&
	   glTextureParameterIuiv != nullptr && glTextureParameteriv != nullptr && glGenerateTextureMipmap != nullptr &&
	   glBindTextureUnit != nullptr && glGetTextureImage != nullptr && glGetCompressedTextureImage != nullptr &&
	   glGetTextureLevelParameterfv != nullptr && glGetTextureLevelParameteriv != nullptr &&
	   glGetTextureParameterfv != nullptr && glGetTextureParameterIiv != nullptr &&
	   glGetTextureParameterIuiv != nullptr && glGetTextureParameteriv != nullptr &&
	   glCreateVertexArrays != nullptr && glDisableVertexArrayAttrib != nullptr &&
	   glEnableVertexArrayAttrib != nullptr && glVertexArrayElementBuffer != nullptr &&
	   glVertexArrayVertexBuffer != nullptr && glVertexArrayVertexBuffers != nullptr &&
	   glVertexArrayAttribBinding != nullptr && glVertexArrayAttribFormat != nullptr &&
	   glVertexArrayAttribIFormat != nullptr && glVertexArrayAttribLFormat != nullptr &&
	   glVertexArrayBindingDivisor != nullptr && glGetVertexArrayiv != nullptr &&
	   glGetVertexArrayIndexediv != nullptr && glGetVertexArrayIndexed64iv != nullptr &&
	   glCreateSamplers != nullptr && glCreateProgramPipelines != nullptr && glCreateQueries != nullptr &&
	   glGetQueryBufferObjecti64v != nullptr && glGetQueryBufferObjectiv != nullptr &&
	   glGetQueryBufferObjectui64v != nullptr && glGetQueryBufferObjectuiv != nullptr &&
	   glMemoryBarrierByRegion != nullptr && glGetTextureSubImage != nullptr &&
	   glGetCompressedTextureSubImage != nullptr && glGetGraphicsResetStatus != nullptr &&
	   glGetnCompressedTexImage != nullptr && glGetnTexImage != nullptr && glGetnUniformdv != nullptr &&
	   glGetnUniformfv != nullptr && glGetnUniformiv != nullptr && glGetnUniformuiv != nullptr &&
	   glReadnPixels != nullptr && glTextureBarrier != nullptr;
  }

protected:
private:
};
} // namespace engine::graphics::gl

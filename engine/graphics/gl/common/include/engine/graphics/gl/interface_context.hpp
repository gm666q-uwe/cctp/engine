#pragma once

#include "engine/graphics/gl/i_interface_context.hpp"

namespace engine::graphics::gl {
class InterfaceContext : public IInterfaceContext
{
public:
  constexpr InterfaceContext() noexcept = default;

  constexpr InterfaceContext(InterfaceContext const&) noexcept = default;

  constexpr InterfaceContext(InterfaceContext&& other) noexcept = default;

  ~InterfaceContext() noexcept override = default;

  constexpr auto operator=(InterfaceContext const&) & noexcept -> InterfaceContext& = default;

  constexpr auto operator=(InterfaceContext&& other) & noexcept -> InterfaceContext& = default;

protected:
private:
};
} // namespace engine::graphics::gl

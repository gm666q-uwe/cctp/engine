#include "engine/graphics/gl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GL_VERSION_2_0_LOAD(name, type)                                                                         \
  m_data_version_2_0.name =                                                                                            \
    reinterpret_cast<type>(m_interface_context->get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GL_VERSION_2_0_FUNCTION(name, type, ...)                                                                \
  using namespace std::literals;                                                                                       \
  if (m_data_version_2_0.name == nullptr) [[unlikely]] {                                                               \
    ENGINE_GL_VERSION_2_0_LOAD(name, type)                                                                             \
  }                                                                                                                    \
  return m_data_version_2_0.name(__VA_ARGS__);

auto
engine::graphics::gl::Context::blendEquationSeparate(GLenum modeRGB, GLenum modeAlpha) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glBlendEquationSeparate, PFNGLBLENDEQUATIONSEPARATEPROC, modeRGB, modeAlpha)
}

auto
engine::graphics::gl::Context::drawBuffers(GLsizei n, GLenum const* bufs) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glDrawBuffers, PFNGLDRAWBUFFERSPROC, n, bufs)
}

auto
engine::graphics::gl::Context::stencilOpSeparate(GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass) & noexcept
  -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glStencilOpSeparate, PFNGLSTENCILOPSEPARATEPROC, face, sfail, dpfail, dppass)
}

auto
engine::graphics::gl::Context::stencilFuncSeparate(GLenum face, GLenum func, GLint ref, GLuint mask) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glStencilFuncSeparate, PFNGLSTENCILFUNCSEPARATEPROC, face, func, ref, mask)
}

auto
engine::graphics::gl::Context::stencilMaskSeparate(GLenum face, GLuint mask) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glStencilMaskSeparate, PFNGLSTENCILMASKSEPARATEPROC, face, mask)
}

auto
engine::graphics::gl::Context::attachShader(GLuint program, GLuint shader) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glAttachShader, PFNGLATTACHSHADERPROC, program, shader)
}

auto
engine::graphics::gl::Context::bindAttribLocation(GLuint program, GLuint index, GLchar const* name) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glBindAttribLocation, PFNGLBINDATTRIBLOCATIONPROC, program, index, name)
}

auto
engine::graphics::gl::Context::compileShader(GLuint shader) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glCompileShader, PFNGLCOMPILESHADERPROC, shader)
}

auto
engine::graphics::gl::Context::createProgram() & noexcept -> GLuint
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glCreateProgram, PFNGLCREATEPROGRAMPROC)
}

auto
engine::graphics::gl::Context::createShader(GLenum type) & noexcept -> GLuint
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glCreateShader, PFNGLCREATESHADERPROC, type)
}

auto
engine::graphics::gl::Context::deleteProgram(GLuint program) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glDeleteProgram, PFNGLDELETEPROGRAMPROC, program)
}

auto
engine::graphics::gl::Context::deleteShader(GLuint shader) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glDeleteShader, PFNGLDELETESHADERPROC, shader)
}

auto
engine::graphics::gl::Context::detachShader(GLuint program, GLuint shader) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glDetachShader, PFNGLDETACHSHADERPROC, program, shader)
}

auto
engine::graphics::gl::Context::disableVertexAttribArray(GLuint index) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glDisableVertexAttribArray, PFNGLDISABLEVERTEXATTRIBARRAYPROC, index)
}

auto
engine::graphics::gl::Context::enableVertexAttribArray(GLuint index) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glEnableVertexAttribArray, PFNGLENABLEVERTEXATTRIBARRAYPROC, index)
}

auto
engine::graphics::gl::Context::getActiveAttrib(GLuint program,
					       GLuint index,
					       GLsizei bufSize,
					       GLsizei* length,
					       GLint* size,
					       GLenum* type,
					       GLchar* name) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(
    glGetActiveAttrib, PFNGLGETACTIVEATTRIBPROC, program, index, bufSize, length, size, type, name)
}

auto
engine::graphics::gl::Context::getActiveUniform(GLuint program,
						GLuint index,
						GLsizei bufSize,
						GLsizei* length,
						GLint* size,
						GLenum* type,
						GLchar* name) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(
    glGetActiveUniform, PFNGLGETACTIVEUNIFORMPROC, program, index, bufSize, length, size, type, name)
}

auto
engine::graphics::gl::Context::getAttachedShaders(GLuint program,
						  GLsizei maxCount,
						  GLsizei* count,
						  GLuint* shaders) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glGetAttachedShaders, PFNGLGETATTACHEDSHADERSPROC, program, maxCount, count, shaders)
}

auto
engine::graphics::gl::Context::getAttribLocation(GLuint program, GLchar const* name) & noexcept -> GLint
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glGetAttribLocation, PFNGLGETATTRIBLOCATIONPROC, program, name)
}

auto
engine::graphics::gl::Context::getProgramiv(GLuint program, GLenum pname, GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glGetProgramiv, PFNGLGETPROGRAMIVPROC, program, pname, params)
}

auto
engine::graphics::gl::Context::getProgramInfoLog(GLuint program,
						 GLsizei bufSize,
						 GLsizei* length,
						 GLchar* infoLog) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glGetProgramInfoLog, PFNGLGETPROGRAMINFOLOGPROC, program, bufSize, length, infoLog)
}

auto
engine::graphics::gl::Context::getShaderiv(GLuint shader, GLenum pname, GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glGetShaderiv, PFNGLGETSHADERIVPROC, shader, pname, params)
}

auto
engine::graphics::gl::Context::getShaderInfoLog(GLuint shader,
						GLsizei bufSize,
						GLsizei* length,
						GLchar* infoLog) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glGetShaderInfoLog, PFNGLGETSHADERINFOLOGPROC, shader, bufSize, length, infoLog)
}

auto
engine::graphics::gl::Context::getShaderSource(GLuint shader,
					       GLsizei bufSize,
					       GLsizei* length,
					       GLchar* source) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glGetShaderSource, PFNGLGETSHADERSOURCEPROC, shader, bufSize, length, source)
}

auto
engine::graphics::gl::Context::getUniformLocation(GLuint program, GLchar const* name) & noexcept -> GLint
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glGetUniformLocation, PFNGLGETUNIFORMLOCATIONPROC, program, name)
}

auto
engine::graphics::gl::Context::getUniformfv(GLuint program, GLint location, GLfloat* params) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glGetUniformfv, PFNGLGETUNIFORMFVPROC, program, location, params)
}

auto
engine::graphics::gl::Context::getUniformiv(GLuint program, GLint location, GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glGetUniformiv, PFNGLGETUNIFORMIVPROC, program, location, params)
}

auto
engine::graphics::gl::Context::getVertexAttribdv(GLuint index, GLenum pname, GLdouble* params) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glGetVertexAttribdv, PFNGLGETVERTEXATTRIBDVPROC, index, pname, params)
}

auto
engine::graphics::gl::Context::getVertexAttribfv(GLuint index, GLenum pname, GLfloat* params) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glGetVertexAttribfv, PFNGLGETVERTEXATTRIBFVPROC, index, pname, params)
}

auto
engine::graphics::gl::Context::getVertexAttribiv(GLuint index, GLenum pname, GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glGetVertexAttribiv, PFNGLGETVERTEXATTRIBIVPROC, index, pname, params)
}

auto
engine::graphics::gl::Context::getVertexAttribPointerv(GLuint index, GLenum pname, void** pointer) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glGetVertexAttribPointerv, PFNGLGETVERTEXATTRIBPOINTERVPROC, index, pname, pointer)
}

auto
engine::graphics::gl::Context::isProgram(GLuint program) & noexcept -> GLboolean
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glIsProgram, PFNGLISPROGRAMPROC, program)
}

auto
engine::graphics::gl::Context::isShader(GLuint shader) & noexcept -> GLboolean
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glIsShader, PFNGLISSHADERPROC, shader)
}

auto
engine::graphics::gl::Context::linkProgram(GLuint program) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glLinkProgram, PFNGLLINKPROGRAMPROC, program)
}

auto
engine::graphics::gl::Context::shaderSource(GLuint shader,
					    GLsizei count,
					    GLchar const* const* string,
					    GLint const* length) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glShaderSource, PFNGLSHADERSOURCEPROC, shader, count, string, length)
}

auto
engine::graphics::gl::Context::useProgram(GLuint program) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUseProgram, PFNGLUSEPROGRAMPROC, program)
}

auto
engine::graphics::gl::Context::uniform1f(GLint location, GLfloat v0) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUniform1f, PFNGLUNIFORM1FPROC, location, v0)
}

auto
engine::graphics::gl::Context::uniform2f(GLint location, GLfloat v0, GLfloat v1) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUniform2f, PFNGLUNIFORM2FPROC, location, v0, v1)
}

auto
engine::graphics::gl::Context::uniform3f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUniform3f, PFNGLUNIFORM3FPROC, location, v0, v1, v2)
}

auto
engine::graphics::gl::Context::uniform4f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3) & noexcept
  -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUniform4f, PFNGLUNIFORM4FPROC, location, v0, v1, v2, v3)
}

auto
engine::graphics::gl::Context::uniform1i(GLint location, GLint v0) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUniform1i, PFNGLUNIFORM1IPROC, location, v0)
}

auto
engine::graphics::gl::Context::uniform2i(GLint location, GLint v0, GLint v1) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUniform2i, PFNGLUNIFORM2IPROC, location, v0, v1)
}

auto
engine::graphics::gl::Context::uniform3i(GLint location, GLint v0, GLint v1, GLint v2) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUniform3i, PFNGLUNIFORM3IPROC, location, v0, v1, v2)
}

auto
engine::graphics::gl::Context::uniform4i(GLint location, GLint v0, GLint v1, GLint v2, GLint v3) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUniform4i, PFNGLUNIFORM4IPROC, location, v0, v1, v2, v3)
}

auto
engine::graphics::gl::Context::uniform1fv(GLint location, GLsizei count, GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUniform1fv, PFNGLUNIFORM1FVPROC, location, count, value)
}

auto
engine::graphics::gl::Context::uniform2fv(GLint location, GLsizei count, GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUniform2fv, PFNGLUNIFORM2FVPROC, location, count, value)
}

auto
engine::graphics::gl::Context::uniform3fv(GLint location, GLsizei count, GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUniform3fv, PFNGLUNIFORM3FVPROC, location, count, value)
}

auto
engine::graphics::gl::Context::uniform4fv(GLint location, GLsizei count, GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUniform4fv, PFNGLUNIFORM4FVPROC, location, count, value)
}

auto
engine::graphics::gl::Context::uniform1iv(GLint location, GLsizei count, GLint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUniform1iv, PFNGLUNIFORM1IVPROC, location, count, value)
}

auto
engine::graphics::gl::Context::uniform2iv(GLint location, GLsizei count, GLint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUniform2iv, PFNGLUNIFORM2IVPROC, location, count, value)
}

auto
engine::graphics::gl::Context::uniform3iv(GLint location, GLsizei count, GLint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUniform3iv, PFNGLUNIFORM3IVPROC, location, count, value)
}

auto
engine::graphics::gl::Context::uniform4iv(GLint location, GLsizei count, GLint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUniform4iv, PFNGLUNIFORM4IVPROC, location, count, value)
}

auto
engine::graphics::gl::Context::uniformMatrix2fv(GLint location,
						GLsizei count,
						GLboolean transpose,
						GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUniformMatrix2fv, PFNGLUNIFORMMATRIX2FVPROC, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::uniformMatrix3fv(GLint location,
						GLsizei count,
						GLboolean transpose,
						GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUniformMatrix3fv, PFNGLUNIFORMMATRIX3FVPROC, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::uniformMatrix4fv(GLint location,
						GLsizei count,
						GLboolean transpose,
						GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glUniformMatrix4fv, PFNGLUNIFORMMATRIX4FVPROC, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::validateProgram(GLuint program) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glValidateProgram, PFNGLVALIDATEPROGRAMPROC, program)
}

auto
engine::graphics::gl::Context::vertexAttrib1d(GLuint index, GLdouble x) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib1d, PFNGLVERTEXATTRIB1DPROC, index, x)
}

auto
engine::graphics::gl::Context::vertexAttrib1dv(GLuint index, GLdouble const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib1dv, PFNGLVERTEXATTRIB1DVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib1f(GLuint index, GLfloat x) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib1f, PFNGLVERTEXATTRIB1FPROC, index, x)
}

auto
engine::graphics::gl::Context::vertexAttrib1fv(GLuint index, GLfloat const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib1fv, PFNGLVERTEXATTRIB1FVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib1s(GLuint index, GLshort x) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib1s, PFNGLVERTEXATTRIB1SPROC, index, x)
}

auto
engine::graphics::gl::Context::vertexAttrib1sv(GLuint index, GLshort const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib1sv, PFNGLVERTEXATTRIB1SVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib2d(GLuint index, GLdouble x, GLdouble y) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib2d, PFNGLVERTEXATTRIB2DPROC, index, x, y)
}

auto
engine::graphics::gl::Context::vertexAttrib2dv(GLuint index, GLdouble const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib2dv, PFNGLVERTEXATTRIB2DVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib2f(GLuint index, GLfloat x, GLfloat y) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib2f, PFNGLVERTEXATTRIB2FPROC, index, x, y)
}

auto
engine::graphics::gl::Context::vertexAttrib2fv(GLuint index, GLfloat const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib2fv, PFNGLVERTEXATTRIB2FVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib2s(GLuint index, GLshort x, GLshort y) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib2s, PFNGLVERTEXATTRIB2SPROC, index, x, y)
}

auto
engine::graphics::gl::Context::vertexAttrib2sv(GLuint index, GLshort const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib2sv, PFNGLVERTEXATTRIB2SVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib3d(GLuint index, GLdouble x, GLdouble y, GLdouble z) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib3d, PFNGLVERTEXATTRIB3DPROC, index, x, y, z)
}

auto
engine::graphics::gl::Context::vertexAttrib3dv(GLuint index, GLdouble const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib3dv, PFNGLVERTEXATTRIB3DVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib3f(GLuint index, GLfloat x, GLfloat y, GLfloat z) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib3f, PFNGLVERTEXATTRIB3FPROC, index, x, y, z)
}

auto
engine::graphics::gl::Context::vertexAttrib3fv(GLuint index, GLfloat const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib3fv, PFNGLVERTEXATTRIB3FVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib3s(GLuint index, GLshort x, GLshort y, GLshort z) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib3s, PFNGLVERTEXATTRIB3SPROC, index, x, y, z)
}

auto
engine::graphics::gl::Context::vertexAttrib3sv(GLuint index, GLshort const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib3sv, PFNGLVERTEXATTRIB3SVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib4Nbv(GLuint index, GLbyte const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib4Nbv, PFNGLVERTEXATTRIB4NBVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib4Niv(GLuint index, GLint const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib4Niv, PFNGLVERTEXATTRIB4NIVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib4Nsv(GLuint index, GLshort const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib4Nsv, PFNGLVERTEXATTRIB4NSVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib4Nub(GLuint index, GLubyte x, GLubyte y, GLubyte z, GLubyte w) & noexcept
  -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib4Nub, PFNGLVERTEXATTRIB4NUBPROC, index, x, y, z, w)
}

auto
engine::graphics::gl::Context::vertexAttrib4Nubv(GLuint index, GLubyte const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib4Nubv, PFNGLVERTEXATTRIB4NUBVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib4Nuiv(GLuint index, GLuint const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib4Nuiv, PFNGLVERTEXATTRIB4NUIVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib4Nusv(GLuint index, GLushort const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib4Nusv, PFNGLVERTEXATTRIB4NUSVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib4bv(GLuint index, GLbyte const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib4bv, PFNGLVERTEXATTRIB4BVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib4d(GLuint index, GLdouble x, GLdouble y, GLdouble z, GLdouble w) & noexcept
  -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib4d, PFNGLVERTEXATTRIB4DPROC, index, x, y, z, w)
}

auto
engine::graphics::gl::Context::vertexAttrib4dv(GLuint index, GLdouble const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib4dv, PFNGLVERTEXATTRIB4DVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib4f(GLuint index, GLfloat x, GLfloat y, GLfloat z, GLfloat w) & noexcept
  -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib4f, PFNGLVERTEXATTRIB4FPROC, index, x, y, z, w)
}

auto
engine::graphics::gl::Context::vertexAttrib4fv(GLuint index, GLfloat const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib4fv, PFNGLVERTEXATTRIB4FVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib4iv(GLuint index, GLint const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib4iv, PFNGLVERTEXATTRIB4IVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib4s(GLuint index, GLshort x, GLshort y, GLshort z, GLshort w) & noexcept
  -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib4s, PFNGLVERTEXATTRIB4SPROC, index, x, y, z, w)
}

auto
engine::graphics::gl::Context::vertexAttrib4sv(GLuint index, GLshort const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib4sv, PFNGLVERTEXATTRIB4SVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib4ubv(GLuint index, GLubyte const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib4ubv, PFNGLVERTEXATTRIB4UBVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib4uiv(GLuint index, GLuint const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib4uiv, PFNGLVERTEXATTRIB4UIVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttrib4usv(GLuint index, GLushort const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(glVertexAttrib4usv, PFNGLVERTEXATTRIB4USVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttribPointer(GLuint index,
						   GLint size,
						   GLenum type,
						   GLboolean normalized,
						   GLsizei stride,
						   void const* pointer) & noexcept -> void
{
  ENGINE_GL_VERSION_2_0_FUNCTION(
    glVertexAttribPointer, PFNGLVERTEXATTRIBPOINTERPROC, index, size, type, normalized, stride, pointer)
}

auto
engine::graphics::gl::Context::load_version_2_0() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GL_VERSION_2_0_LOAD(glBlendEquationSeparate, PFNGLBLENDEQUATIONSEPARATEPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glDrawBuffers, PFNGLDRAWBUFFERSPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glStencilOpSeparate, PFNGLSTENCILOPSEPARATEPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glStencilFuncSeparate, PFNGLSTENCILFUNCSEPARATEPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glStencilMaskSeparate, PFNGLSTENCILMASKSEPARATEPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glAttachShader, PFNGLATTACHSHADERPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glBindAttribLocation, PFNGLBINDATTRIBLOCATIONPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glCompileShader, PFNGLCOMPILESHADERPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glCreateProgram, PFNGLCREATEPROGRAMPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glCreateShader, PFNGLCREATESHADERPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glDeleteProgram, PFNGLDELETEPROGRAMPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glDeleteShader, PFNGLDELETESHADERPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glDetachShader, PFNGLDETACHSHADERPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glDisableVertexAttribArray, PFNGLDISABLEVERTEXATTRIBARRAYPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glEnableVertexAttribArray, PFNGLENABLEVERTEXATTRIBARRAYPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glGetActiveAttrib, PFNGLGETACTIVEATTRIBPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glGetActiveUniform, PFNGLGETACTIVEUNIFORMPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glGetAttachedShaders, PFNGLGETATTACHEDSHADERSPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glGetAttribLocation, PFNGLGETATTRIBLOCATIONPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glGetProgramiv, PFNGLGETPROGRAMIVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glGetProgramInfoLog, PFNGLGETPROGRAMINFOLOGPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glGetShaderiv, PFNGLGETSHADERIVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glGetShaderInfoLog, PFNGLGETSHADERINFOLOGPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glGetShaderSource, PFNGLGETSHADERSOURCEPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glGetUniformLocation, PFNGLGETUNIFORMLOCATIONPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glGetUniformfv, PFNGLGETUNIFORMFVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glGetUniformiv, PFNGLGETUNIFORMIVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glGetVertexAttribdv, PFNGLGETVERTEXATTRIBDVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glGetVertexAttribfv, PFNGLGETVERTEXATTRIBFVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glGetVertexAttribiv, PFNGLGETVERTEXATTRIBIVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glGetVertexAttribPointerv, PFNGLGETVERTEXATTRIBPOINTERVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glIsProgram, PFNGLISPROGRAMPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glIsShader, PFNGLISSHADERPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glLinkProgram, PFNGLLINKPROGRAMPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glShaderSource, PFNGLSHADERSOURCEPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUseProgram, PFNGLUSEPROGRAMPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUniform1f, PFNGLUNIFORM1FPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUniform2f, PFNGLUNIFORM2FPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUniform3f, PFNGLUNIFORM3FPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUniform4f, PFNGLUNIFORM4FPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUniform1i, PFNGLUNIFORM1IPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUniform2i, PFNGLUNIFORM2IPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUniform3i, PFNGLUNIFORM3IPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUniform4i, PFNGLUNIFORM4IPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUniform1fv, PFNGLUNIFORM1FVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUniform2fv, PFNGLUNIFORM2FVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUniform3fv, PFNGLUNIFORM3FVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUniform4fv, PFNGLUNIFORM4FVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUniform1iv, PFNGLUNIFORM1IVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUniform2iv, PFNGLUNIFORM2IVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUniform3iv, PFNGLUNIFORM3IVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUniform4iv, PFNGLUNIFORM4IVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUniformMatrix2fv, PFNGLUNIFORMMATRIX2FVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUniformMatrix3fv, PFNGLUNIFORMMATRIX3FVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glUniformMatrix4fv, PFNGLUNIFORMMATRIX4FVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glValidateProgram, PFNGLVALIDATEPROGRAMPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib1d, PFNGLVERTEXATTRIB1DPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib1dv, PFNGLVERTEXATTRIB1DVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib1f, PFNGLVERTEXATTRIB1FPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib1fv, PFNGLVERTEXATTRIB1FVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib1s, PFNGLVERTEXATTRIB1SPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib1sv, PFNGLVERTEXATTRIB1SVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib2d, PFNGLVERTEXATTRIB2DPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib2dv, PFNGLVERTEXATTRIB2DVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib2f, PFNGLVERTEXATTRIB2FPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib2fv, PFNGLVERTEXATTRIB2FVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib2s, PFNGLVERTEXATTRIB2SPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib2sv, PFNGLVERTEXATTRIB2SVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib3d, PFNGLVERTEXATTRIB3DPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib3dv, PFNGLVERTEXATTRIB3DVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib3f, PFNGLVERTEXATTRIB3FPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib3fv, PFNGLVERTEXATTRIB3FVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib3s, PFNGLVERTEXATTRIB3SPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib3sv, PFNGLVERTEXATTRIB3SVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib4Nbv, PFNGLVERTEXATTRIB4NBVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib4Niv, PFNGLVERTEXATTRIB4NIVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib4Nsv, PFNGLVERTEXATTRIB4NSVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib4Nub, PFNGLVERTEXATTRIB4NUBPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib4Nubv, PFNGLVERTEXATTRIB4NUBVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib4Nuiv, PFNGLVERTEXATTRIB4NUIVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib4Nusv, PFNGLVERTEXATTRIB4NUSVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib4bv, PFNGLVERTEXATTRIB4BVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib4d, PFNGLVERTEXATTRIB4DPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib4dv, PFNGLVERTEXATTRIB4DVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib4f, PFNGLVERTEXATTRIB4FPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib4fv, PFNGLVERTEXATTRIB4FVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib4iv, PFNGLVERTEXATTRIB4IVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib4s, PFNGLVERTEXATTRIB4SPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib4sv, PFNGLVERTEXATTRIB4SVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib4ubv, PFNGLVERTEXATTRIB4UBVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib4uiv, PFNGLVERTEXATTRIB4UIVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttrib4usv, PFNGLVERTEXATTRIB4USVPROC)
  ENGINE_GL_VERSION_2_0_LOAD(glVertexAttribPointer, PFNGLVERTEXATTRIBPOINTERPROC)
}

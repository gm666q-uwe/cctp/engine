#include "engine/graphics/gl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GL_VERSION_4_5_LOAD(name, type)                                                                         \
  m_data_version_4_5.name =                                                                                            \
    reinterpret_cast<type>(m_interface_context->get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GL_VERSION_4_5_FUNCTION(name, type, ...)                                                                \
  using namespace std::literals;                                                                                       \
  if (m_data_version_4_5.name == nullptr) [[unlikely]] {                                                               \
    ENGINE_GL_VERSION_4_5_LOAD(name, type)                                                                             \
  }                                                                                                                    \
  return m_data_version_4_5.name(__VA_ARGS__);

auto
engine::graphics::gl::Context::clipControl(GLenum origin, GLenum depth) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glClipControl, PFNGLCLIPCONTROLPROC, origin, depth)
}

auto
engine::graphics::gl::Context::createTransformFeedbacks(GLsizei n, GLuint* ids) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glCreateTransformFeedbacks, PFNGLCREATETRANSFORMFEEDBACKSPROC, n, ids)
}

auto
engine::graphics::gl::Context::transformFeedbackBufferBase(GLuint xfb, GLuint index, GLuint buffer) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glTransformFeedbackBufferBase, PFNGLTRANSFORMFEEDBACKBUFFERBASEPROC, xfb, index, buffer)
}

auto
engine::graphics::gl::Context::transformFeedbackBufferRange(GLuint xfb,
							    GLuint index,
							    GLuint buffer,
							    GLintptr offset,
							    GLsizeiptr size) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glTransformFeedbackBufferRange, PFNGLTRANSFORMFEEDBACKBUFFERRANGEPROC, xfb, index, buffer, offset, size)
}

auto
engine::graphics::gl::Context::getTransformFeedbackiv(GLuint xfb, GLenum pname, GLint* param) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetTransformFeedbackiv, PFNGLGETTRANSFORMFEEDBACKIVPROC, xfb, pname, param)
}

auto
engine::graphics::gl::Context::getTransformFeedbacki_v(GLuint xfb, GLenum pname, GLuint index, GLint* param) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetTransformFeedbacki_v, PFNGLGETTRANSFORMFEEDBACKI_VPROC, xfb, pname, index, param)
}

auto
engine::graphics::gl::Context::getTransformFeedbacki64_v(GLuint xfb,
							 GLenum pname,
							 GLuint index,
							 GLint64* param) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glGetTransformFeedbacki64_v, PFNGLGETTRANSFORMFEEDBACKI64_VPROC, xfb, pname, index, param)
}

auto
engine::graphics::gl::Context::createBuffers(GLsizei n, GLuint* buffers) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glCreateBuffers, PFNGLCREATEBUFFERSPROC, n, buffers)
}

auto
engine::graphics::gl::Context::namedBufferStorage(GLuint buffer,
						  GLsizeiptr size,
						  void const* data,
						  GLbitfield flags) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glNamedBufferStorage, PFNGLNAMEDBUFFERSTORAGEPROC, buffer, size, data, flags)
}

auto
engine::graphics::gl::Context::namedBufferData(GLuint buffer,
					       GLsizeiptr size,
					       void const* data,
					       GLenum usage) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glNamedBufferData, PFNGLNAMEDBUFFERDATAPROC, buffer, size, data, usage)
}

auto
engine::graphics::gl::Context::namedBufferSubData(GLuint buffer,
						  GLintptr offset,
						  GLsizeiptr size,
						  void const* data) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glNamedBufferSubData, PFNGLNAMEDBUFFERSUBDATAPROC, buffer, offset, size, data)
}

auto
engine::graphics::gl::Context::copyNamedBufferSubData(GLuint readBuffer,
						      GLuint writeBuffer,
						      GLintptr readOffset,
						      GLintptr writeOffset,
						      GLsizeiptr size) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glCopyNamedBufferSubData, PFNGLCOPYNAMEDBUFFERSUBDATAPROC, readBuffer, writeBuffer, readOffset, writeOffset, size)
}

auto
engine::graphics::gl::Context::clearNamedBufferData(GLuint buffer,
						    GLenum internalformat,
						    GLenum format,
						    GLenum type,
						    void const* data) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glClearNamedBufferData, PFNGLCLEARNAMEDBUFFERDATAPROC, buffer, internalformat, format, type, data)
}

auto
engine::graphics::gl::Context::clearNamedBufferSubData(GLuint buffer,
						       GLenum internalformat,
						       GLintptr offset,
						       GLsizeiptr size,
						       GLenum format,
						       GLenum type,
						       void const* data) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glClearNamedBufferSubData,
				 PFNGLCLEARNAMEDBUFFERSUBDATAPROC,
				 buffer,
				 internalformat,
				 offset,
				 size,
				 format,
				 type,
				 data)
}

auto
engine::graphics::gl::Context::mapNamedBuffer(GLuint buffer, GLenum access) & noexcept -> void*
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glMapNamedBuffer, PFNGLMAPNAMEDBUFFERPROC, buffer, access)
}

auto
engine::graphics::gl::Context::mapNamedBufferRange(GLuint buffer,
						   GLintptr offset,
						   GLsizeiptr length,
						   GLbitfield access) & noexcept -> void*
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glMapNamedBufferRange, PFNGLMAPNAMEDBUFFERRANGEPROC, buffer, offset, length, access)
}

auto
engine::graphics::gl::Context::unmapNamedBuffer(GLuint buffer) & noexcept -> GLboolean
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glUnmapNamedBuffer, PFNGLUNMAPNAMEDBUFFERPROC, buffer)
}

auto
engine::graphics::gl::Context::flushMappedNamedBufferRange(GLuint buffer, GLintptr offset, GLsizeiptr length) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glFlushMappedNamedBufferRange, PFNGLFLUSHMAPPEDNAMEDBUFFERRANGEPROC, buffer, offset, length)
}

auto
engine::graphics::gl::Context::getNamedBufferParameteriv(GLuint buffer, GLenum pname, GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetNamedBufferParameteriv, PFNGLGETNAMEDBUFFERPARAMETERIVPROC, buffer, pname, params)
}

auto
engine::graphics::gl::Context::getNamedBufferParameteri64v(GLuint buffer, GLenum pname, GLint64* params) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glGetNamedBufferParameteri64v, PFNGLGETNAMEDBUFFERPARAMETERI64VPROC, buffer, pname, params)
}

auto
engine::graphics::gl::Context::getNamedBufferPointerv(GLuint buffer, GLenum pname, void** params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetNamedBufferPointerv, PFNGLGETNAMEDBUFFERPOINTERVPROC, buffer, pname, params)
}

auto
engine::graphics::gl::Context::getNamedBufferSubData(GLuint buffer,
						     GLintptr offset,
						     GLsizeiptr size,
						     void* data) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetNamedBufferSubData, PFNGLGETNAMEDBUFFERSUBDATAPROC, buffer, offset, size, data)
}

auto
engine::graphics::gl::Context::createFramebuffers(GLsizei n, GLuint* framebuffers) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glCreateFramebuffers, PFNGLCREATEFRAMEBUFFERSPROC, n, framebuffers)
}

auto
engine::graphics::gl::Context::namedFramebufferRenderbuffer(GLuint framebuffer,
							    GLenum attachment,
							    GLenum renderbuffertarget,
							    GLuint renderbuffer) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glNamedFramebufferRenderbuffer,
				 PFNGLNAMEDFRAMEBUFFERRENDERBUFFERPROC,
				 framebuffer,
				 attachment,
				 renderbuffertarget,
				 renderbuffer)
}

auto
engine::graphics::gl::Context::namedFramebufferParameteri(GLuint framebuffer, GLenum pname, GLint param) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glNamedFramebufferParameteri, PFNGLNAMEDFRAMEBUFFERPARAMETERIPROC, framebuffer, pname, param)
}

auto
engine::graphics::gl::Context::namedFramebufferTexture(GLuint framebuffer,
						       GLenum attachment,
						       GLuint texture,
						       GLint level) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glNamedFramebufferTexture, PFNGLNAMEDFRAMEBUFFERTEXTUREPROC, framebuffer, attachment, texture, level)
}

auto
engine::graphics::gl::Context::namedFramebufferTextureLayer(GLuint framebuffer,
							    GLenum attachment,
							    GLuint texture,
							    GLint level,
							    GLint layer) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glNamedFramebufferTextureLayer,
				 PFNGLNAMEDFRAMEBUFFERTEXTURELAYERPROC,
				 framebuffer,
				 attachment,
				 texture,
				 level,
				 layer)
}

auto
engine::graphics::gl::Context::namedFramebufferDrawBuffer(GLuint framebuffer, GLenum buf) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glNamedFramebufferDrawBuffer, PFNGLNAMEDFRAMEBUFFERDRAWBUFFERPROC, framebuffer, buf)
}

auto
engine::graphics::gl::Context::namedFramebufferDrawBuffers(GLuint framebuffer, GLsizei n, GLenum const* bufs) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glNamedFramebufferDrawBuffers, PFNGLNAMEDFRAMEBUFFERDRAWBUFFERSPROC, framebuffer, n, bufs)
}

auto
engine::graphics::gl::Context::namedFramebufferReadBuffer(GLuint framebuffer, GLenum src) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glNamedFramebufferReadBuffer, PFNGLNAMEDFRAMEBUFFERREADBUFFERPROC, framebuffer, src)
}

auto
engine::graphics::gl::Context::invalidateNamedFramebufferData(GLuint framebuffer,
							      GLsizei numAttachments,
							      GLenum const* attachments) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glInvalidateNamedFramebufferData, PFNGLINVALIDATENAMEDFRAMEBUFFERDATAPROC, framebuffer, numAttachments, attachments)
}

auto
engine::graphics::gl::Context::invalidateNamedFramebufferSubData(GLuint framebuffer,
								 GLsizei numAttachments,
								 GLenum const* attachments,
								 GLint x,
								 GLint y,
								 GLsizei width,
								 GLsizei height) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glInvalidateNamedFramebufferSubData,
				 PFNGLINVALIDATENAMEDFRAMEBUFFERSUBDATAPROC,
				 framebuffer,
				 numAttachments,
				 attachments,
				 x,
				 y,
				 width,
				 height)
}

auto
engine::graphics::gl::Context::clearNamedFramebufferiv(GLuint framebuffer,
						       GLenum buffer,
						       GLint drawbuffer,
						       GLint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glClearNamedFramebufferiv, PFNGLCLEARNAMEDFRAMEBUFFERIVPROC, framebuffer, buffer, drawbuffer, value)
}

auto
engine::graphics::gl::Context::clearNamedFramebufferuiv(GLuint framebuffer,
							GLenum buffer,
							GLint drawbuffer,
							GLuint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glClearNamedFramebufferuiv, PFNGLCLEARNAMEDFRAMEBUFFERUIVPROC, framebuffer, buffer, drawbuffer, value)
}

auto
engine::graphics::gl::Context::clearNamedFramebufferfv(GLuint framebuffer,
						       GLenum buffer,
						       GLint drawbuffer,
						       GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glClearNamedFramebufferfv, PFNGLCLEARNAMEDFRAMEBUFFERFVPROC, framebuffer, buffer, drawbuffer, value)
}

auto
engine::graphics::gl::Context::clearNamedFramebufferfi(GLuint framebuffer,
						       GLenum buffer,
						       GLint drawbuffer,
						       GLfloat depth,
						       GLint stencil) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glClearNamedFramebufferfi, PFNGLCLEARNAMEDFRAMEBUFFERFIPROC, framebuffer, buffer, drawbuffer, depth, stencil)
}

auto
engine::graphics::gl::Context::blitNamedFramebuffer(GLuint readFramebuffer,
						    GLuint drawFramebuffer,
						    GLint srcX0,
						    GLint srcY0,
						    GLint srcX1,
						    GLint srcY1,
						    GLint dstX0,
						    GLint dstY0,
						    GLint dstX1,
						    GLint dstY1,
						    GLbitfield mask,
						    GLenum filter) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glBlitNamedFramebuffer,
				 PFNGLBLITNAMEDFRAMEBUFFERPROC,
				 readFramebuffer,
				 drawFramebuffer,
				 srcX0,
				 srcY0,
				 srcX1,
				 srcY1,
				 dstX0,
				 dstY0,
				 dstX1,
				 dstY1,
				 mask,
				 filter)
}

auto
engine::graphics::gl::Context::checkNamedFramebufferStatus(GLuint framebuffer, GLenum target) & noexcept -> GLenum
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glCheckNamedFramebufferStatus, PFNGLCHECKNAMEDFRAMEBUFFERSTATUSPROC, framebuffer, target)
}

auto
engine::graphics::gl::Context::getNamedFramebufferParameteriv(GLuint framebuffer, GLenum pname, GLint* param) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glGetNamedFramebufferParameteriv, PFNGLGETNAMEDFRAMEBUFFERPARAMETERIVPROC, framebuffer, pname, param)
}

auto
engine::graphics::gl::Context::getNamedFramebufferAttachmentParameteriv(GLuint framebuffer,
									GLenum attachment,
									GLenum pname,
									GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetNamedFramebufferAttachmentParameteriv,
				 PFNGLGETNAMEDFRAMEBUFFERATTACHMENTPARAMETERIVPROC,
				 framebuffer,
				 attachment,
				 pname,
				 params)
}

auto
engine::graphics::gl::Context::createRenderbuffers(GLsizei n, GLuint* renderbuffers) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glCreateRenderbuffers, PFNGLCREATERENDERBUFFERSPROC, n, renderbuffers)
}

auto
engine::graphics::gl::Context::namedRenderbufferStorage(GLuint renderbuffer,
							GLenum internalformat,
							GLsizei width,
							GLsizei height) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glNamedRenderbufferStorage, PFNGLNAMEDRENDERBUFFERSTORAGEPROC, renderbuffer, internalformat, width, height)
}

auto
engine::graphics::gl::Context::namedRenderbufferStorageMultisample(GLuint renderbuffer,
								   GLsizei samples,
								   GLenum internalformat,
								   GLsizei width,
								   GLsizei height) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glNamedRenderbufferStorageMultisample,
				 PFNGLNAMEDRENDERBUFFERSTORAGEMULTISAMPLEPROC,
				 renderbuffer,
				 samples,
				 internalformat,
				 width,
				 height)
}

auto
engine::graphics::gl::Context::getNamedRenderbufferParameteriv(GLuint renderbuffer,
							       GLenum pname,
							       GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glGetNamedRenderbufferParameteriv, PFNGLGETNAMEDRENDERBUFFERPARAMETERIVPROC, renderbuffer, pname, params)
}

auto
engine::graphics::gl::Context::createTextures(GLenum target, GLsizei n, GLuint* textures) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glCreateTextures, PFNGLCREATETEXTURESPROC, target, n, textures)
}

auto
engine::graphics::gl::Context::textureBuffer(GLuint texture, GLenum internalformat, GLuint buffer) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glTextureBuffer, PFNGLTEXTUREBUFFERPROC, texture, internalformat, buffer)
}

auto
engine::graphics::gl::Context::textureBufferRange(GLuint texture,
						  GLenum internalformat,
						  GLuint buffer,
						  GLintptr offset,
						  GLsizeiptr size) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glTextureBufferRange, PFNGLTEXTUREBUFFERRANGEPROC, texture, internalformat, buffer, offset, size)
}

auto
engine::graphics::gl::Context::textureStorage1D(GLuint texture,
						GLsizei levels,
						GLenum internalformat,
						GLsizei width) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glTextureStorage1D, PFNGLTEXTURESTORAGE1DPROC, texture, levels, internalformat, width)
}

auto
engine::graphics::gl::Context::textureStorage2D(GLuint texture,
						GLsizei levels,
						GLenum internalformat,
						GLsizei width,
						GLsizei height) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glTextureStorage2D, PFNGLTEXTURESTORAGE2DPROC, texture, levels, internalformat, width, height)
}

auto
engine::graphics::gl::Context::textureStorage3D(GLuint texture,
						GLsizei levels,
						GLenum internalformat,
						GLsizei width,
						GLsizei height,
						GLsizei depth) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glTextureStorage3D, PFNGLTEXTURESTORAGE3DPROC, texture, levels, internalformat, width, height, depth)
}

auto
engine::graphics::gl::Context::textureStorage2DMultisample(GLuint texture,
							   GLsizei samples,
							   GLenum internalformat,
							   GLsizei width,
							   GLsizei height,
							   GLboolean fixedsamplelocations) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glTextureStorage2DMultisample,
				 PFNGLTEXTURESTORAGE2DMULTISAMPLEPROC,
				 texture,
				 samples,
				 internalformat,
				 width,
				 height,
				 fixedsamplelocations)
}

auto
engine::graphics::gl::Context::textureStorage3DMultisample(GLuint texture,
							   GLsizei samples,
							   GLenum internalformat,
							   GLsizei width,
							   GLsizei height,
							   GLsizei depth,
							   GLboolean fixedsamplelocations) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glTextureStorage3DMultisample,
				 PFNGLTEXTURESTORAGE3DMULTISAMPLEPROC,
				 texture,
				 samples,
				 internalformat,
				 width,
				 height,
				 depth,
				 fixedsamplelocations)
}

auto
engine::graphics::gl::Context::textureSubImage1D(GLuint texture,
						 GLint level,
						 GLint xoffset,
						 GLsizei width,
						 GLenum format,
						 GLenum type,
						 void const* pixels) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glTextureSubImage1D, PFNGLTEXTURESUBIMAGE1DPROC, texture, level, xoffset, width, format, type, pixels)
}

auto
engine::graphics::gl::Context::textureSubImage2D(GLuint texture,
						 GLint level,
						 GLint xoffset,
						 GLint yoffset,
						 GLsizei width,
						 GLsizei height,
						 GLenum format,
						 GLenum type,
						 void const* pixels) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glTextureSubImage2D,
				 PFNGLTEXTURESUBIMAGE2DPROC,
				 texture,
				 level,
				 xoffset,
				 yoffset,
				 width,
				 height,
				 format,
				 type,
				 pixels)
}

auto
engine::graphics::gl::Context::textureSubImage3D(GLuint texture,
						 GLint level,
						 GLint xoffset,
						 GLint yoffset,
						 GLint zoffset,
						 GLsizei width,
						 GLsizei height,
						 GLsizei depth,
						 GLenum format,
						 GLenum type,
						 void const* pixels) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glTextureSubImage3D,
				 PFNGLTEXTURESUBIMAGE3DPROC,
				 texture,
				 level,
				 xoffset,
				 yoffset,
				 zoffset,
				 width,
				 height,
				 depth,
				 format,
				 type,
				 pixels)
}

auto
engine::graphics::gl::Context::compressedTextureSubImage1D(GLuint texture,
							   GLint level,
							   GLint xoffset,
							   GLsizei width,
							   GLenum format,
							   GLsizei imageSize,
							   void const* data) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glCompressedTextureSubImage1D,
				 PFNGLCOMPRESSEDTEXTURESUBIMAGE1DPROC,
				 texture,
				 level,
				 xoffset,
				 width,
				 format,
				 imageSize,
				 data)
}

auto
engine::graphics::gl::Context::compressedTextureSubImage2D(GLuint texture,
							   GLint level,
							   GLint xoffset,
							   GLint yoffset,
							   GLsizei width,
							   GLsizei height,
							   GLenum format,
							   GLsizei imageSize,
							   void const* data) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glCompressedTextureSubImage2D,
				 PFNGLCOMPRESSEDTEXTURESUBIMAGE2DPROC,
				 texture,
				 level,
				 xoffset,
				 yoffset,
				 width,
				 height,
				 format,
				 imageSize,
				 data)
}

auto
engine::graphics::gl::Context::compressedTextureSubImage3D(GLuint texture,
							   GLint level,
							   GLint xoffset,
							   GLint yoffset,
							   GLint zoffset,
							   GLsizei width,
							   GLsizei height,
							   GLsizei depth,
							   GLenum format,
							   GLsizei imageSize,
							   void const* data) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glCompressedTextureSubImage3D,
				 PFNGLCOMPRESSEDTEXTURESUBIMAGE3DPROC,
				 texture,
				 level,
				 xoffset,
				 yoffset,
				 zoffset,
				 width,
				 height,
				 depth,
				 format,
				 imageSize,
				 data)
}

auto
engine::graphics::gl::Context::copyTextureSubImage1D(GLuint texture,
						     GLint level,
						     GLint xoffset,
						     GLint x,
						     GLint y,
						     GLsizei width) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glCopyTextureSubImage1D, PFNGLCOPYTEXTURESUBIMAGE1DPROC, texture, level, xoffset, x, y, width)
}

auto
engine::graphics::gl::Context::copyTextureSubImage2D(GLuint texture,
						     GLint level,
						     GLint xoffset,
						     GLint yoffset,
						     GLint x,
						     GLint y,
						     GLsizei width,
						     GLsizei height) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glCopyTextureSubImage2D, PFNGLCOPYTEXTURESUBIMAGE2DPROC, texture, level, xoffset, yoffset, x, y, width, height)
}

auto
engine::graphics::gl::Context::copyTextureSubImage3D(GLuint texture,
						     GLint level,
						     GLint xoffset,
						     GLint yoffset,
						     GLint zoffset,
						     GLint x,
						     GLint y,
						     GLsizei width,
						     GLsizei height) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glCopyTextureSubImage3D,
				 PFNGLCOPYTEXTURESUBIMAGE3DPROC,
				 texture,
				 level,
				 xoffset,
				 yoffset,
				 zoffset,
				 x,
				 y,
				 width,
				 height)
}

auto
engine::graphics::gl::Context::textureParameterf(GLuint texture, GLenum pname, GLfloat param) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glTextureParameterf, PFNGLTEXTUREPARAMETERFPROC, texture, pname, param)
}

auto
engine::graphics::gl::Context::textureParameterfv(GLuint texture, GLenum pname, GLfloat const* param) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glTextureParameterfv, PFNGLTEXTUREPARAMETERFVPROC, texture, pname, param)
}

auto
engine::graphics::gl::Context::textureParameteri(GLuint texture, GLenum pname, GLint param) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glTextureParameteri, PFNGLTEXTUREPARAMETERIPROC, texture, pname, param)
}

auto
engine::graphics::gl::Context::textureParameterIiv(GLuint texture, GLenum pname, GLint const* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glTextureParameterIiv, PFNGLTEXTUREPARAMETERIIVPROC, texture, pname, params)
}

auto
engine::graphics::gl::Context::textureParameterIuiv(GLuint texture, GLenum pname, GLuint const* params) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glTextureParameterIuiv, PFNGLTEXTUREPARAMETERIUIVPROC, texture, pname, params)
}

auto
engine::graphics::gl::Context::textureParameteriv(GLuint texture, GLenum pname, GLint const* param) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glTextureParameteriv, PFNGLTEXTUREPARAMETERIVPROC, texture, pname, param)
}

auto
engine::graphics::gl::Context::generateTextureMipmap(GLuint texture) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGenerateTextureMipmap, PFNGLGENERATETEXTUREMIPMAPPROC, texture)
}

auto
engine::graphics::gl::Context::bindTextureUnit(GLuint unit, GLuint texture) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glBindTextureUnit, PFNGLBINDTEXTUREUNITPROC, unit, texture)
}

auto
engine::graphics::gl::Context::getTextureImage(GLuint texture,
					       GLint level,
					       GLenum format,
					       GLenum type,
					       GLsizei bufSize,
					       void* pixels) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glGetTextureImage, PFNGLGETTEXTUREIMAGEPROC, texture, level, format, type, bufSize, pixels)
}

auto
engine::graphics::gl::Context::getCompressedTextureImage(GLuint texture,
							 GLint level,
							 GLsizei bufSize,
							 void* pixels) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glGetCompressedTextureImage, PFNGLGETCOMPRESSEDTEXTUREIMAGEPROC, texture, level, bufSize, pixels)
}

auto
engine::graphics::gl::Context::getTextureLevelParameterfv(GLuint texture,
							  GLint level,
							  GLenum pname,
							  GLfloat* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glGetTextureLevelParameterfv, PFNGLGETTEXTURELEVELPARAMETERFVPROC, texture, level, pname, params)
}

auto
engine::graphics::gl::Context::getTextureLevelParameteriv(GLuint texture,
							  GLint level,
							  GLenum pname,
							  GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glGetTextureLevelParameteriv, PFNGLGETTEXTURELEVELPARAMETERIVPROC, texture, level, pname, params)
}

auto
engine::graphics::gl::Context::getTextureParameterfv(GLuint texture, GLenum pname, GLfloat* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetTextureParameterfv, PFNGLGETTEXTUREPARAMETERFVPROC, texture, pname, params)
}

auto
engine::graphics::gl::Context::getTextureParameterIiv(GLuint texture, GLenum pname, GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetTextureParameterIiv, PFNGLGETTEXTUREPARAMETERIIVPROC, texture, pname, params)
}

auto
engine::graphics::gl::Context::getTextureParameterIuiv(GLuint texture, GLenum pname, GLuint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetTextureParameterIuiv, PFNGLGETTEXTUREPARAMETERIUIVPROC, texture, pname, params)
}

auto
engine::graphics::gl::Context::getTextureParameteriv(GLuint texture, GLenum pname, GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetTextureParameteriv, PFNGLGETTEXTUREPARAMETERIVPROC, texture, pname, params)
}

auto
engine::graphics::gl::Context::createVertexArrays(GLsizei n, GLuint* arrays) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glCreateVertexArrays, PFNGLCREATEVERTEXARRAYSPROC, n, arrays)
}

auto
engine::graphics::gl::Context::disableVertexArrayAttrib(GLuint vaobj, GLuint index) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glDisableVertexArrayAttrib, PFNGLDISABLEVERTEXARRAYATTRIBPROC, vaobj, index)
}

auto
engine::graphics::gl::Context::enableVertexArrayAttrib(GLuint vaobj, GLuint index) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glEnableVertexArrayAttrib, PFNGLENABLEVERTEXARRAYATTRIBPROC, vaobj, index)
}

auto
engine::graphics::gl::Context::vertexArrayElementBuffer(GLuint vaobj, GLuint buffer) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glVertexArrayElementBuffer, PFNGLVERTEXARRAYELEMENTBUFFERPROC, vaobj, buffer)
}

auto
engine::graphics::gl::Context::vertexArrayVertexBuffer(GLuint vaobj,
						       GLuint bindingindex,
						       GLuint buffer,
						       GLintptr offset,
						       GLsizei stride) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glVertexArrayVertexBuffer, PFNGLVERTEXARRAYVERTEXBUFFERPROC, vaobj, bindingindex, buffer, offset, stride)
}

auto
engine::graphics::gl::Context::vertexArrayVertexBuffers(GLuint vaobj,
							GLuint first,
							GLsizei count,
							GLuint const* buffers,
							GLintptr const* offsets,
							GLsizei const* strides) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glVertexArrayVertexBuffers, PFNGLVERTEXARRAYVERTEXBUFFERSPROC, vaobj, first, count, buffers, offsets, strides)
}

auto
engine::graphics::gl::Context::vertexArrayAttribBinding(GLuint vaobj,
							GLuint attribindex,
							GLuint bindingindex) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glVertexArrayAttribBinding, PFNGLVERTEXARRAYATTRIBBINDINGPROC, vaobj, attribindex, bindingindex)
}

auto
engine::graphics::gl::Context::vertexArrayAttribFormat(GLuint vaobj,
						       GLuint attribindex,
						       GLint size,
						       GLenum type,
						       GLboolean normalized,
						       GLuint relativeoffset) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glVertexArrayAttribFormat,
				 PFNGLVERTEXARRAYATTRIBFORMATPROC,
				 vaobj,
				 attribindex,
				 size,
				 type,
				 normalized,
				 relativeoffset)
}

auto
engine::graphics::gl::Context::vertexArrayAttribIFormat(GLuint vaobj,
							GLuint attribindex,
							GLint size,
							GLenum type,
							GLuint relativeoffset) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glVertexArrayAttribIFormat, PFNGLVERTEXARRAYATTRIBIFORMATPROC, vaobj, attribindex, size, type, relativeoffset)
}

auto
engine::graphics::gl::Context::vertexArrayAttribLFormat(GLuint vaobj,
							GLuint attribindex,
							GLint size,
							GLenum type,
							GLuint relativeoffset) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glVertexArrayAttribLFormat, PFNGLVERTEXARRAYATTRIBLFORMATPROC, vaobj, attribindex, size, type, relativeoffset)
}

auto
engine::graphics::gl::Context::vertexArrayBindingDivisor(GLuint vaobj, GLuint bindingindex, GLuint divisor) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glVertexArrayBindingDivisor, PFNGLVERTEXARRAYBINDINGDIVISORPROC, vaobj, bindingindex, divisor)
}

auto
engine::graphics::gl::Context::getVertexArrayiv(GLuint vaobj, GLenum pname, GLint* param) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetVertexArrayiv, PFNGLGETVERTEXARRAYIVPROC, vaobj, pname, param)
}

auto
engine::graphics::gl::Context::getVertexArrayIndexediv(GLuint vaobj,
						       GLuint index,
						       GLenum pname,
						       GLint* param) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glGetVertexArrayIndexediv, PFNGLGETVERTEXARRAYINDEXEDIVPROC, vaobj, index, pname, param)
}

auto
engine::graphics::gl::Context::getVertexArrayIndexed64iv(GLuint vaobj,
							 GLuint index,
							 GLenum pname,
							 GLint64* param) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glGetVertexArrayIndexed64iv, PFNGLGETVERTEXARRAYINDEXED64IVPROC, vaobj, index, pname, param)
}

auto
engine::graphics::gl::Context::createSamplers(GLsizei n, GLuint* samplers) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glCreateSamplers, PFNGLCREATESAMPLERSPROC, n, samplers)
}

auto
engine::graphics::gl::Context::createProgramPipelines(GLsizei n, GLuint* pipelines) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glCreateProgramPipelines, PFNGLCREATEPROGRAMPIPELINESPROC, n, pipelines)
}

auto
engine::graphics::gl::Context::createQueries(GLenum target, GLsizei n, GLuint* ids) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glCreateQueries, PFNGLCREATEQUERIESPROC, target, n, ids)
}

auto
engine::graphics::gl::Context::getQueryBufferObjecti64v(GLuint id,
							GLuint buffer,
							GLenum pname,
							GLintptr offset) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glGetQueryBufferObjecti64v, PFNGLGETQUERYBUFFEROBJECTI64VPROC, id, buffer, pname, offset)
}

auto
engine::graphics::gl::Context::getQueryBufferObjectiv(GLuint id,
						      GLuint buffer,
						      GLenum pname,
						      GLintptr offset) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetQueryBufferObjectiv, PFNGLGETQUERYBUFFEROBJECTIVPROC, id, buffer, pname, offset)
}

auto
engine::graphics::gl::Context::getQueryBufferObjectui64v(GLuint id,
							 GLuint buffer,
							 GLenum pname,
							 GLintptr offset) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glGetQueryBufferObjectui64v, PFNGLGETQUERYBUFFEROBJECTUI64VPROC, id, buffer, pname, offset)
}

auto
engine::graphics::gl::Context::getQueryBufferObjectuiv(GLuint id,
						       GLuint buffer,
						       GLenum pname,
						       GLintptr offset) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetQueryBufferObjectuiv, PFNGLGETQUERYBUFFEROBJECTUIVPROC, id, buffer, pname, offset)
}

auto
engine::graphics::gl::Context::memoryBarrierByRegion(GLbitfield barriers) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glMemoryBarrierByRegion, PFNGLMEMORYBARRIERBYREGIONPROC, barriers)
}

auto
engine::graphics::gl::Context::getTextureSubImage(GLuint texture,
						  GLint level,
						  GLint xoffset,
						  GLint yoffset,
						  GLint zoffset,
						  GLsizei width,
						  GLsizei height,
						  GLsizei depth,
						  GLenum format,
						  GLenum type,
						  GLsizei bufSize,
						  void* pixels) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetTextureSubImage,
				 PFNGLGETTEXTURESUBIMAGEPROC,
				 texture,
				 level,
				 xoffset,
				 yoffset,
				 zoffset,
				 width,
				 height,
				 depth,
				 format,
				 type,
				 bufSize,
				 pixels)
}

auto
engine::graphics::gl::Context::getCompressedTextureSubImage(GLuint texture,
							    GLint level,
							    GLint xoffset,
							    GLint yoffset,
							    GLint zoffset,
							    GLsizei width,
							    GLsizei height,
							    GLsizei depth,
							    GLsizei bufSize,
							    void* pixels) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetCompressedTextureSubImage,
				 PFNGLGETCOMPRESSEDTEXTURESUBIMAGEPROC,
				 texture,
				 level,
				 xoffset,
				 yoffset,
				 zoffset,
				 width,
				 height,
				 depth,
				 bufSize,
				 pixels)
}

auto
engine::graphics::gl::Context::getGraphicsResetStatus() & noexcept -> GLenum
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetGraphicsResetStatus, PFNGLGETGRAPHICSRESETSTATUSPROC)
}

auto
engine::graphics::gl::Context::getnCompressedTexImage(GLenum target,
						      GLint lod,
						      GLsizei bufSize,
						      void* pixels) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(
    glGetnCompressedTexImage, PFNGLGETNCOMPRESSEDTEXIMAGEPROC, target, lod, bufSize, pixels)
}

auto
engine::graphics::gl::Context::getnTexImage(GLenum target,
					    GLint level,
					    GLenum format,
					    GLenum type,
					    GLsizei bufSize,
					    void* pixels) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetnTexImage, PFNGLGETNTEXIMAGEPROC, target, level, format, type, bufSize, pixels)
}

auto
engine::graphics::gl::Context::getnUniformdv(GLuint program,
					     GLint location,
					     GLsizei bufSize,
					     GLdouble* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetnUniformdv, PFNGLGETNUNIFORMDVPROC, program, location, bufSize, params)
}

auto
engine::graphics::gl::Context::getnUniformfv(GLuint program,
					     GLint location,
					     GLsizei bufSize,
					     GLfloat* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetnUniformfv, PFNGLGETNUNIFORMFVPROC, program, location, bufSize, params)
}

auto
engine::graphics::gl::Context::getnUniformiv(GLuint program, GLint location, GLsizei bufSize, GLint* params) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetnUniformiv, PFNGLGETNUNIFORMIVPROC, program, location, bufSize, params)
}

auto
engine::graphics::gl::Context::getnUniformuiv(GLuint program,
					      GLint location,
					      GLsizei bufSize,
					      GLuint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glGetnUniformuiv, PFNGLGETNUNIFORMUIVPROC, program, location, bufSize, params)
}

auto
engine::graphics::gl::Context::readnPixels(GLint x,
					   GLint y,
					   GLsizei width,
					   GLsizei height,
					   GLenum format,
					   GLenum type,
					   GLsizei bufSize,
					   void* data) & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glReadnPixels, PFNGLREADNPIXELSPROC, x, y, width, height, format, type, bufSize, data)
}

auto
engine::graphics::gl::Context::textureBarrier() & noexcept -> void
{
  ENGINE_GL_VERSION_4_5_FUNCTION(glTextureBarrier, PFNGLTEXTUREBARRIERPROC)
}

auto
engine::graphics::gl::Context::load_version_4_5() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GL_VERSION_4_5_LOAD(glClipControl, PFNGLCLIPCONTROLPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glCreateTransformFeedbacks, PFNGLCREATETRANSFORMFEEDBACKSPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glTransformFeedbackBufferBase, PFNGLTRANSFORMFEEDBACKBUFFERBASEPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glTransformFeedbackBufferRange, PFNGLTRANSFORMFEEDBACKBUFFERRANGEPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetTransformFeedbackiv, PFNGLGETTRANSFORMFEEDBACKIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetTransformFeedbacki_v, PFNGLGETTRANSFORMFEEDBACKI_VPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetTransformFeedbacki64_v, PFNGLGETTRANSFORMFEEDBACKI64_VPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glCreateBuffers, PFNGLCREATEBUFFERSPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glNamedBufferStorage, PFNGLNAMEDBUFFERSTORAGEPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glNamedBufferData, PFNGLNAMEDBUFFERDATAPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glNamedBufferSubData, PFNGLNAMEDBUFFERSUBDATAPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glCopyNamedBufferSubData, PFNGLCOPYNAMEDBUFFERSUBDATAPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glClearNamedBufferData, PFNGLCLEARNAMEDBUFFERDATAPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glClearNamedBufferSubData, PFNGLCLEARNAMEDBUFFERSUBDATAPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glMapNamedBuffer, PFNGLMAPNAMEDBUFFERPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glMapNamedBufferRange, PFNGLMAPNAMEDBUFFERRANGEPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glUnmapNamedBuffer, PFNGLUNMAPNAMEDBUFFERPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glFlushMappedNamedBufferRange, PFNGLFLUSHMAPPEDNAMEDBUFFERRANGEPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetNamedBufferParameteriv, PFNGLGETNAMEDBUFFERPARAMETERIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetNamedBufferParameteri64v, PFNGLGETNAMEDBUFFERPARAMETERI64VPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetNamedBufferPointerv, PFNGLGETNAMEDBUFFERPOINTERVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetNamedBufferSubData, PFNGLGETNAMEDBUFFERSUBDATAPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glCreateFramebuffers, PFNGLCREATEFRAMEBUFFERSPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glNamedFramebufferRenderbuffer, PFNGLNAMEDFRAMEBUFFERRENDERBUFFERPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glNamedFramebufferParameteri, PFNGLNAMEDFRAMEBUFFERPARAMETERIPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glNamedFramebufferTexture, PFNGLNAMEDFRAMEBUFFERTEXTUREPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glNamedFramebufferTextureLayer, PFNGLNAMEDFRAMEBUFFERTEXTURELAYERPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glNamedFramebufferDrawBuffer, PFNGLNAMEDFRAMEBUFFERDRAWBUFFERPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glNamedFramebufferDrawBuffers, PFNGLNAMEDFRAMEBUFFERDRAWBUFFERSPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glNamedFramebufferReadBuffer, PFNGLNAMEDFRAMEBUFFERREADBUFFERPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glInvalidateNamedFramebufferData, PFNGLINVALIDATENAMEDFRAMEBUFFERDATAPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glInvalidateNamedFramebufferSubData, PFNGLINVALIDATENAMEDFRAMEBUFFERSUBDATAPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glClearNamedFramebufferiv, PFNGLCLEARNAMEDFRAMEBUFFERIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glClearNamedFramebufferuiv, PFNGLCLEARNAMEDFRAMEBUFFERUIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glClearNamedFramebufferfv, PFNGLCLEARNAMEDFRAMEBUFFERFVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glClearNamedFramebufferfi, PFNGLCLEARNAMEDFRAMEBUFFERFIPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glBlitNamedFramebuffer, PFNGLBLITNAMEDFRAMEBUFFERPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glCheckNamedFramebufferStatus, PFNGLCHECKNAMEDFRAMEBUFFERSTATUSPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetNamedFramebufferParameteriv, PFNGLGETNAMEDFRAMEBUFFERPARAMETERIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetNamedFramebufferAttachmentParameteriv,
			     PFNGLGETNAMEDFRAMEBUFFERATTACHMENTPARAMETERIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glCreateRenderbuffers, PFNGLCREATERENDERBUFFERSPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glNamedRenderbufferStorage, PFNGLNAMEDRENDERBUFFERSTORAGEPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glNamedRenderbufferStorageMultisample, PFNGLNAMEDRENDERBUFFERSTORAGEMULTISAMPLEPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetNamedRenderbufferParameteriv, PFNGLGETNAMEDRENDERBUFFERPARAMETERIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glCreateTextures, PFNGLCREATETEXTURESPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glTextureBuffer, PFNGLTEXTUREBUFFERPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glTextureBufferRange, PFNGLTEXTUREBUFFERRANGEPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glTextureStorage1D, PFNGLTEXTURESTORAGE1DPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glTextureStorage2D, PFNGLTEXTURESTORAGE2DPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glTextureStorage3D, PFNGLTEXTURESTORAGE3DPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glTextureStorage2DMultisample, PFNGLTEXTURESTORAGE2DMULTISAMPLEPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glTextureStorage3DMultisample, PFNGLTEXTURESTORAGE3DMULTISAMPLEPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glTextureSubImage1D, PFNGLTEXTURESUBIMAGE1DPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glTextureSubImage2D, PFNGLTEXTURESUBIMAGE2DPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glTextureSubImage3D, PFNGLTEXTURESUBIMAGE3DPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glCompressedTextureSubImage1D, PFNGLCOMPRESSEDTEXTURESUBIMAGE1DPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glCompressedTextureSubImage2D, PFNGLCOMPRESSEDTEXTURESUBIMAGE2DPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glCompressedTextureSubImage3D, PFNGLCOMPRESSEDTEXTURESUBIMAGE3DPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glCopyTextureSubImage1D, PFNGLCOPYTEXTURESUBIMAGE1DPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glCopyTextureSubImage2D, PFNGLCOPYTEXTURESUBIMAGE2DPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glCopyTextureSubImage3D, PFNGLCOPYTEXTURESUBIMAGE3DPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glTextureParameterf, PFNGLTEXTUREPARAMETERFPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glTextureParameterfv, PFNGLTEXTUREPARAMETERFVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glTextureParameteri, PFNGLTEXTUREPARAMETERIPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glTextureParameterIiv, PFNGLTEXTUREPARAMETERIIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glTextureParameterIuiv, PFNGLTEXTUREPARAMETERIUIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glTextureParameteriv, PFNGLTEXTUREPARAMETERIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGenerateTextureMipmap, PFNGLGENERATETEXTUREMIPMAPPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glBindTextureUnit, PFNGLBINDTEXTUREUNITPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetTextureImage, PFNGLGETTEXTUREIMAGEPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetCompressedTextureImage, PFNGLGETCOMPRESSEDTEXTUREIMAGEPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetTextureLevelParameterfv, PFNGLGETTEXTURELEVELPARAMETERFVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetTextureLevelParameteriv, PFNGLGETTEXTURELEVELPARAMETERIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetTextureParameterfv, PFNGLGETTEXTUREPARAMETERFVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetTextureParameterIiv, PFNGLGETTEXTUREPARAMETERIIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetTextureParameterIuiv, PFNGLGETTEXTUREPARAMETERIUIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetTextureParameteriv, PFNGLGETTEXTUREPARAMETERIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glCreateVertexArrays, PFNGLCREATEVERTEXARRAYSPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glDisableVertexArrayAttrib, PFNGLDISABLEVERTEXARRAYATTRIBPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glEnableVertexArrayAttrib, PFNGLENABLEVERTEXARRAYATTRIBPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glVertexArrayElementBuffer, PFNGLVERTEXARRAYELEMENTBUFFERPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glVertexArrayVertexBuffer, PFNGLVERTEXARRAYVERTEXBUFFERPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glVertexArrayVertexBuffers, PFNGLVERTEXARRAYVERTEXBUFFERSPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glVertexArrayAttribBinding, PFNGLVERTEXARRAYATTRIBBINDINGPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glVertexArrayAttribFormat, PFNGLVERTEXARRAYATTRIBFORMATPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glVertexArrayAttribIFormat, PFNGLVERTEXARRAYATTRIBIFORMATPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glVertexArrayAttribLFormat, PFNGLVERTEXARRAYATTRIBLFORMATPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glVertexArrayBindingDivisor, PFNGLVERTEXARRAYBINDINGDIVISORPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetVertexArrayiv, PFNGLGETVERTEXARRAYIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetVertexArrayIndexediv, PFNGLGETVERTEXARRAYINDEXEDIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetVertexArrayIndexed64iv, PFNGLGETVERTEXARRAYINDEXED64IVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glCreateSamplers, PFNGLCREATESAMPLERSPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glCreateProgramPipelines, PFNGLCREATEPROGRAMPIPELINESPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glCreateQueries, PFNGLCREATEQUERIESPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetQueryBufferObjecti64v, PFNGLGETQUERYBUFFEROBJECTI64VPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetQueryBufferObjectiv, PFNGLGETQUERYBUFFEROBJECTIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetQueryBufferObjectui64v, PFNGLGETQUERYBUFFEROBJECTUI64VPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetQueryBufferObjectuiv, PFNGLGETQUERYBUFFEROBJECTUIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glMemoryBarrierByRegion, PFNGLMEMORYBARRIERBYREGIONPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetTextureSubImage, PFNGLGETTEXTURESUBIMAGEPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetCompressedTextureSubImage, PFNGLGETCOMPRESSEDTEXTURESUBIMAGEPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetGraphicsResetStatus, PFNGLGETGRAPHICSRESETSTATUSPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetnCompressedTexImage, PFNGLGETNCOMPRESSEDTEXIMAGEPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetnTexImage, PFNGLGETNTEXIMAGEPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetnUniformdv, PFNGLGETNUNIFORMDVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetnUniformfv, PFNGLGETNUNIFORMFVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetnUniformiv, PFNGLGETNUNIFORMIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glGetnUniformuiv, PFNGLGETNUNIFORMUIVPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glReadnPixels, PFNGLREADNPIXELSPROC)
  ENGINE_GL_VERSION_4_5_LOAD(glTextureBarrier, PFNGLTEXTUREBARRIERPROC)
}

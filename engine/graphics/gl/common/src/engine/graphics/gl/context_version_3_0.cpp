#include "engine/graphics/gl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GL_VERSION_3_0_LOAD(name, type)                                                                         \
  m_data_version_3_0.name =                                                                                            \
    reinterpret_cast<type>(m_interface_context->get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GL_VERSION_3_0_FUNCTION(name, type, ...)                                                                \
  using namespace std::literals;                                                                                       \
  if (m_data_version_3_0.name == nullptr) [[unlikely]] {                                                               \
    ENGINE_GL_VERSION_3_0_LOAD(name, type)                                                                             \
  }                                                                                                                    \
  return m_data_version_3_0.name(__VA_ARGS__);

auto
engine::graphics::gl::Context::colorMaski(GLuint index, GLboolean r, GLboolean g, GLboolean b, GLboolean a) & noexcept
  -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glColorMaski, PFNGLCOLORMASKIPROC, index, r, g, b, a)
}

auto
engine::graphics::gl::Context::getBooleani_v(GLenum target, GLuint index, GLboolean* data) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glGetBooleani_v, PFNGLGETBOOLEANI_VPROC, target, index, data)
}

auto
engine::graphics::gl::Context::getIntegeri_v(GLenum target, GLuint index, GLint* data) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glGetIntegeri_v, PFNGLGETINTEGERI_VPROC, target, index, data)
}

auto
engine::graphics::gl::Context::enablei(GLenum target, GLuint index) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glEnablei, PFNGLENABLEIPROC, target, index)
}

auto
engine::graphics::gl::Context::disablei(GLenum target, GLuint index) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glDisablei, PFNGLDISABLEIPROC, target, index)
}

auto
engine::graphics::gl::Context::isEnabledi(GLenum target, GLuint index) & noexcept -> GLboolean
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glIsEnabledi, PFNGLISENABLEDIPROC, target, index)
}

auto
engine::graphics::gl::Context::beginTransformFeedback(GLenum primitiveMode) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glBeginTransformFeedback, PFNGLBEGINTRANSFORMFEEDBACKPROC, primitiveMode)
}

auto
engine::graphics::gl::Context::endTransformFeedback() & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glEndTransformFeedback, PFNGLENDTRANSFORMFEEDBACKPROC)
}

auto
engine::graphics::gl::Context::bindBufferRange(GLenum target,
					       GLuint index,
					       GLuint buffer,
					       GLintptr offset,
					       GLsizeiptr size) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glBindBufferRange, PFNGLBINDBUFFERRANGEPROC, target, index, buffer, offset, size)
}

auto
engine::graphics::gl::Context::bindBufferBase(GLenum target, GLuint index, GLuint buffer) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glBindBufferBase, PFNGLBINDBUFFERBASEPROC, target, index, buffer)
}

auto
engine::graphics::gl::Context::transformFeedbackVaryings(GLuint program,
							 GLsizei count,
							 GLchar const* const* varyings,
							 GLenum bufferMode) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(
    glTransformFeedbackVaryings, PFNGLTRANSFORMFEEDBACKVARYINGSPROC, program, count, varyings, bufferMode)
}

auto
engine::graphics::gl::Context::getTransformFeedbackVarying(GLuint program,
							   GLuint index,
							   GLsizei bufSize,
							   GLsizei* length,
							   GLsizei* size,
							   GLenum* type,
							   GLchar* name) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glGetTransformFeedbackVarying,
				 PFNGLGETTRANSFORMFEEDBACKVARYINGPROC,
				 program,
				 index,
				 bufSize,
				 length,
				 size,
				 type,
				 name)
}

auto
engine::graphics::gl::Context::clampColor(GLenum target, GLenum clamp) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glClampColor, PFNGLCLAMPCOLORPROC, target, clamp)
}

auto
engine::graphics::gl::Context::beginConditionalRender(GLuint id, GLenum mode) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glBeginConditionalRender, PFNGLBEGINCONDITIONALRENDERPROC, id, mode)
}

auto
engine::graphics::gl::Context::endConditionalRender() & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glEndConditionalRender, PFNGLENDCONDITIONALRENDERPROC)
}

auto
engine::graphics::gl::Context::vertexAttribIPointer(GLuint index,
						    GLint size,
						    GLenum type,
						    GLsizei stride,
						    void const* pointer) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(
    glVertexAttribIPointer, PFNGLVERTEXATTRIBIPOINTERPROC, index, size, type, stride, pointer)
}

auto
engine::graphics::gl::Context::getVertexAttribIiv(GLuint index, GLenum pname, GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glGetVertexAttribIiv, PFNGLGETVERTEXATTRIBIIVPROC, index, pname, params)
}

auto
engine::graphics::gl::Context::getVertexAttribIuiv(GLuint index, GLenum pname, GLuint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glGetVertexAttribIuiv, PFNGLGETVERTEXATTRIBIUIVPROC, index, pname, params)
}

auto
engine::graphics::gl::Context::vertexAttribI1i(GLuint index, GLint x) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI1i, PFNGLVERTEXATTRIBI1IPROC, index, x)
}

auto
engine::graphics::gl::Context::vertexAttribI2i(GLuint index, GLint x, GLint y) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI2i, PFNGLVERTEXATTRIBI2IPROC, index, x, y)
}

auto
engine::graphics::gl::Context::vertexAttribI3i(GLuint index, GLint x, GLint y, GLint z) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI3i, PFNGLVERTEXATTRIBI3IPROC, index, x, y, z)
}

auto
engine::graphics::gl::Context::vertexAttribI4i(GLuint index, GLint x, GLint y, GLint z, GLint w) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI4i, PFNGLVERTEXATTRIBI4IPROC, index, x, y, z, w)
}

auto
engine::graphics::gl::Context::vertexAttribI1ui(GLuint index, GLuint x) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI1ui, PFNGLVERTEXATTRIBI1UIPROC, index, x)
}

auto
engine::graphics::gl::Context::vertexAttribI2ui(GLuint index, GLuint x, GLuint y) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI2ui, PFNGLVERTEXATTRIBI2UIPROC, index, x, y)
}

auto
engine::graphics::gl::Context::vertexAttribI3ui(GLuint index, GLuint x, GLuint y, GLuint z) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI3ui, PFNGLVERTEXATTRIBI3UIPROC, index, x, y, z)
}

auto
engine::graphics::gl::Context::vertexAttribI4ui(GLuint index, GLuint x, GLuint y, GLuint z, GLuint w) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI4ui, PFNGLVERTEXATTRIBI4UIPROC, index, x, y, z, w)
}

auto
engine::graphics::gl::Context::vertexAttribI1iv(GLuint index, GLint const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI1iv, PFNGLVERTEXATTRIBI1IVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttribI2iv(GLuint index, GLint const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI2iv, PFNGLVERTEXATTRIBI2IVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttribI3iv(GLuint index, GLint const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI3iv, PFNGLVERTEXATTRIBI3IVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttribI4iv(GLuint index, GLint const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI4iv, PFNGLVERTEXATTRIBI4IVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttribI1uiv(GLuint index, GLuint const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI1uiv, PFNGLVERTEXATTRIBI1UIVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttribI2uiv(GLuint index, GLuint const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI2uiv, PFNGLVERTEXATTRIBI2UIVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttribI3uiv(GLuint index, GLuint const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI3uiv, PFNGLVERTEXATTRIBI3UIVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttribI4uiv(GLuint index, GLuint const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI4uiv, PFNGLVERTEXATTRIBI4UIVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttribI4bv(GLuint index, GLbyte const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI4bv, PFNGLVERTEXATTRIBI4BVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttribI4sv(GLuint index, GLshort const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI4sv, PFNGLVERTEXATTRIBI4SVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttribI4ubv(GLuint index, GLubyte const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI4ubv, PFNGLVERTEXATTRIBI4UBVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttribI4usv(GLuint index, GLushort const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glVertexAttribI4usv, PFNGLVERTEXATTRIBI4USVPROC, index, v)
}

auto
engine::graphics::gl::Context::getUniformuiv(GLuint program, GLint location, GLuint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glGetUniformuiv, PFNGLGETUNIFORMUIVPROC, program, location, params)
}

auto
engine::graphics::gl::Context::bindFragDataLocation(GLuint program, GLuint color, GLchar const* name) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glBindFragDataLocation, PFNGLBINDFRAGDATALOCATIONPROC, program, color, name)
}

auto
engine::graphics::gl::Context::getFragDataLocation(GLuint program, GLchar const* name) & noexcept -> GLint
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glGetFragDataLocation, PFNGLGETFRAGDATALOCATIONPROC, program, name)
}

auto
engine::graphics::gl::Context::uniform1ui(GLint location, GLuint v0) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glUniform1ui, PFNGLUNIFORM1UIPROC, location, v0)
}

auto
engine::graphics::gl::Context::uniform2ui(GLint location, GLuint v0, GLuint v1) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glUniform2ui, PFNGLUNIFORM2UIPROC, location, v0, v1)
}

auto
engine::graphics::gl::Context::uniform3ui(GLint location, GLuint v0, GLuint v1, GLuint v2) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glUniform3ui, PFNGLUNIFORM3UIPROC, location, v0, v1, v2)
}

auto
engine::graphics::gl::Context::uniform4ui(GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glUniform4ui, PFNGLUNIFORM4UIPROC, location, v0, v1, v2, v3)
}

auto
engine::graphics::gl::Context::uniform1uiv(GLint location, GLsizei count, GLuint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glUniform1uiv, PFNGLUNIFORM1UIVPROC, location, count, value)
}

auto
engine::graphics::gl::Context::uniform2uiv(GLint location, GLsizei count, GLuint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glUniform2uiv, PFNGLUNIFORM2UIVPROC, location, count, value)
}

auto
engine::graphics::gl::Context::uniform3uiv(GLint location, GLsizei count, GLuint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glUniform3uiv, PFNGLUNIFORM3UIVPROC, location, count, value)
}

auto
engine::graphics::gl::Context::uniform4uiv(GLint location, GLsizei count, GLuint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glUniform4uiv, PFNGLUNIFORM4UIVPROC, location, count, value)
}

auto
engine::graphics::gl::Context::texParameterIiv(GLenum target, GLenum pname, GLint const* params) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glTexParameterIiv, PFNGLTEXPARAMETERIIVPROC, target, pname, params)
}

auto
engine::graphics::gl::Context::texParameterIuiv(GLenum target, GLenum pname, GLuint const* params) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glTexParameterIuiv, PFNGLTEXPARAMETERIUIVPROC, target, pname, params)
}

auto
engine::graphics::gl::Context::getTexParameterIiv(GLenum target, GLenum pname, GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glGetTexParameterIiv, PFNGLGETTEXPARAMETERIIVPROC, target, pname, params)
}

auto
engine::graphics::gl::Context::getTexParameterIuiv(GLenum target, GLenum pname, GLuint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glGetTexParameterIuiv, PFNGLGETTEXPARAMETERIUIVPROC, target, pname, params)
}

auto
engine::graphics::gl::Context::clearBufferiv(GLenum buffer, GLint drawbuffer, GLint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glClearBufferiv, PFNGLCLEARBUFFERIVPROC, buffer, drawbuffer, value)
}

auto
engine::graphics::gl::Context::clearBufferuiv(GLenum buffer, GLint drawbuffer, GLuint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glClearBufferuiv, PFNGLCLEARBUFFERUIVPROC, buffer, drawbuffer, value)
}

auto
engine::graphics::gl::Context::clearBufferfv(GLenum buffer, GLint drawbuffer, GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glClearBufferfv, PFNGLCLEARBUFFERFVPROC, buffer, drawbuffer, value)
}

auto
engine::graphics::gl::Context::clearBufferfi(GLenum buffer, GLint drawbuffer, GLfloat depth, GLint stencil) & noexcept
  -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glClearBufferfi, PFNGLCLEARBUFFERFIPROC, buffer, drawbuffer, depth, stencil)
}

auto
engine::graphics::gl::Context::getStringi(GLenum name, GLuint index) & noexcept -> GLubyte const*
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glGetStringi, PFNGLGETSTRINGIPROC, name, index)
}

auto
engine::graphics::gl::Context::isRenderbuffer(GLuint renderbuffer) & noexcept -> GLboolean
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glIsRenderbuffer, PFNGLISRENDERBUFFERPROC, renderbuffer)
}

auto
engine::graphics::gl::Context::bindRenderbuffer(GLenum target, GLuint renderbuffer) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glBindRenderbuffer, PFNGLBINDRENDERBUFFERPROC, target, renderbuffer)
}

auto
engine::graphics::gl::Context::deleteRenderbuffers(GLsizei n, GLuint const* renderbuffers) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glDeleteRenderbuffers, PFNGLDELETERENDERBUFFERSPROC, n, renderbuffers)
}

auto
engine::graphics::gl::Context::genRenderbuffers(GLsizei n, GLuint* renderbuffers) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glGenRenderbuffers, PFNGLGENRENDERBUFFERSPROC, n, renderbuffers)
}

auto
engine::graphics::gl::Context::renderbufferStorage(GLenum target,
						   GLenum internalformat,
						   GLsizei width,
						   GLsizei height) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(
    glRenderbufferStorage, PFNGLRENDERBUFFERSTORAGEPROC, target, internalformat, width, height)
}

auto
engine::graphics::gl::Context::getRenderbufferParameteriv(GLenum target, GLenum pname, GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(
    glGetRenderbufferParameteriv, PFNGLGETRENDERBUFFERPARAMETERIVPROC, target, pname, params)
}

auto
engine::graphics::gl::Context::isFramebuffer(GLuint framebuffer) & noexcept -> GLboolean
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glIsFramebuffer, PFNGLISFRAMEBUFFERPROC, framebuffer)
}

auto
engine::graphics::gl::Context::bindFramebuffer(GLenum target, GLuint framebuffer) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glBindFramebuffer, PFNGLBINDFRAMEBUFFERPROC, target, framebuffer)
}

auto
engine::graphics::gl::Context::deleteFramebuffers(GLsizei n, GLuint const* framebuffers) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glDeleteFramebuffers, PFNGLDELETEFRAMEBUFFERSPROC, n, framebuffers)
}

auto
engine::graphics::gl::Context::genFramebuffers(GLsizei n, GLuint* framebuffers) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glGenFramebuffers, PFNGLGENFRAMEBUFFERSPROC, n, framebuffers)
}

auto
engine::graphics::gl::Context::checkFramebufferStatus(GLenum target) & noexcept -> GLenum
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glCheckFramebufferStatus, PFNGLCHECKFRAMEBUFFERSTATUSPROC, target)
}

auto
engine::graphics::gl::Context::framebufferTexture1D(GLenum target,
						    GLenum attachment,
						    GLenum textarget,
						    GLuint texture,
						    GLint level) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(
    glFramebufferTexture1D, PFNGLFRAMEBUFFERTEXTURE1DPROC, target, attachment, textarget, texture, level)
}

auto
engine::graphics::gl::Context::framebufferTexture2D(GLenum target,
						    GLenum attachment,
						    GLenum textarget,
						    GLuint texture,
						    GLint level) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(
    glFramebufferTexture2D, PFNGLFRAMEBUFFERTEXTURE2DPROC, target, attachment, textarget, texture, level)
}

auto
engine::graphics::gl::Context::framebufferTexture3D(GLenum target,
						    GLenum attachment,
						    GLenum textarget,
						    GLuint texture,
						    GLint level,
						    GLint zoffset) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(
    glFramebufferTexture3D, PFNGLFRAMEBUFFERTEXTURE3DPROC, target, attachment, textarget, texture, level, zoffset)
}

auto
engine::graphics::gl::Context::framebufferRenderbuffer(GLenum target,
						       GLenum attachment,
						       GLenum renderbuffertarget,
						       GLuint renderbuffer) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(
    glFramebufferRenderbuffer, PFNGLFRAMEBUFFERRENDERBUFFERPROC, target, attachment, renderbuffertarget, renderbuffer)
}

auto
engine::graphics::gl::Context::getFramebufferAttachmentParameteriv(GLenum target,
								   GLenum attachment,
								   GLenum pname,
								   GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glGetFramebufferAttachmentParameteriv,
				 PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVPROC,
				 target,
				 attachment,
				 pname,
				 params)
}

auto
engine::graphics::gl::Context::generateMipmap(GLenum target) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glGenerateMipmap, PFNGLGENERATEMIPMAPPROC, target)
}

auto
engine::graphics::gl::Context::blitFramebuffer(GLint srcX0,
					       GLint srcY0,
					       GLint srcX1,
					       GLint srcY1,
					       GLint dstX0,
					       GLint dstY0,
					       GLint dstX1,
					       GLint dstY1,
					       GLbitfield mask,
					       GLenum filter) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(
    glBlitFramebuffer, PFNGLBLITFRAMEBUFFERPROC, srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, mask, filter)
}

auto
engine::graphics::gl::Context::renderbufferStorageMultisample(GLenum target,
							      GLsizei samples,
							      GLenum internalformat,
							      GLsizei width,
							      GLsizei height) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glRenderbufferStorageMultisample,
				 PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC,
				 target,
				 samples,
				 internalformat,
				 width,
				 height)
}

auto
engine::graphics::gl::Context::framebufferTextureLayer(GLenum target,
						       GLenum attachment,
						       GLuint texture,
						       GLint level,
						       GLint layer) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(
    glFramebufferTextureLayer, PFNGLFRAMEBUFFERTEXTURELAYERPROC, target, attachment, texture, level, layer)
}

auto
engine::graphics::gl::Context::mapBufferRange(GLenum target,
					      GLintptr offset,
					      GLsizeiptr length,
					      GLbitfield access) & noexcept -> void*
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glMapBufferRange, PFNGLMAPBUFFERRANGEPROC, target, offset, length, access)
}

auto
engine::graphics::gl::Context::flushMappedBufferRange(GLenum target, GLintptr offset, GLsizeiptr length) & noexcept
  -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glFlushMappedBufferRange, PFNGLFLUSHMAPPEDBUFFERRANGEPROC, target, offset, length)
}

auto
engine::graphics::gl::Context::bindVertexArray(GLuint array) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glBindVertexArray, PFNGLBINDVERTEXARRAYPROC, array)
}

auto
engine::graphics::gl::Context::deleteVertexArrays(GLsizei n, GLuint const* arrays) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glDeleteVertexArrays, PFNGLDELETEVERTEXARRAYSPROC, n, arrays)
}

auto
engine::graphics::gl::Context::genVertexArrays(GLsizei n, GLuint* arrays) & noexcept -> void
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glGenVertexArrays, PFNGLGENVERTEXARRAYSPROC, n, arrays)
}

auto
engine::graphics::gl::Context::isVertexArray(GLuint array) & noexcept -> GLboolean
{
  ENGINE_GL_VERSION_3_0_FUNCTION(glIsVertexArray, PFNGLISVERTEXARRAYPROC, array)
}

auto
engine::graphics::gl::Context::load_version_3_0() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GL_VERSION_3_0_LOAD(glColorMaski, PFNGLCOLORMASKIPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glGetBooleani_v, PFNGLGETBOOLEANI_VPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glGetIntegeri_v, PFNGLGETINTEGERI_VPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glEnablei, PFNGLENABLEIPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glDisablei, PFNGLDISABLEIPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glIsEnabledi, PFNGLISENABLEDIPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glBeginTransformFeedback, PFNGLBEGINTRANSFORMFEEDBACKPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glEndTransformFeedback, PFNGLENDTRANSFORMFEEDBACKPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glBindBufferRange, PFNGLBINDBUFFERRANGEPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glBindBufferBase, PFNGLBINDBUFFERBASEPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glTransformFeedbackVaryings, PFNGLTRANSFORMFEEDBACKVARYINGSPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glGetTransformFeedbackVarying, PFNGLGETTRANSFORMFEEDBACKVARYINGPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glClampColor, PFNGLCLAMPCOLORPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glBeginConditionalRender, PFNGLBEGINCONDITIONALRENDERPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glEndConditionalRender, PFNGLENDCONDITIONALRENDERPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribIPointer, PFNGLVERTEXATTRIBIPOINTERPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glGetVertexAttribIiv, PFNGLGETVERTEXATTRIBIIVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glGetVertexAttribIuiv, PFNGLGETVERTEXATTRIBIUIVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI1i, PFNGLVERTEXATTRIBI1IPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI2i, PFNGLVERTEXATTRIBI2IPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI3i, PFNGLVERTEXATTRIBI3IPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI4i, PFNGLVERTEXATTRIBI4IPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI1ui, PFNGLVERTEXATTRIBI1UIPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI2ui, PFNGLVERTEXATTRIBI2UIPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI3ui, PFNGLVERTEXATTRIBI3UIPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI4ui, PFNGLVERTEXATTRIBI4UIPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI1iv, PFNGLVERTEXATTRIBI1IVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI2iv, PFNGLVERTEXATTRIBI2IVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI3iv, PFNGLVERTEXATTRIBI3IVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI4iv, PFNGLVERTEXATTRIBI4IVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI1uiv, PFNGLVERTEXATTRIBI1UIVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI2uiv, PFNGLVERTEXATTRIBI2UIVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI3uiv, PFNGLVERTEXATTRIBI3UIVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI4uiv, PFNGLVERTEXATTRIBI4UIVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI4bv, PFNGLVERTEXATTRIBI4BVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI4sv, PFNGLVERTEXATTRIBI4SVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI4ubv, PFNGLVERTEXATTRIBI4UBVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glVertexAttribI4usv, PFNGLVERTEXATTRIBI4USVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glGetUniformuiv, PFNGLGETUNIFORMUIVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glBindFragDataLocation, PFNGLBINDFRAGDATALOCATIONPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glGetFragDataLocation, PFNGLGETFRAGDATALOCATIONPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glUniform1ui, PFNGLUNIFORM1UIPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glUniform2ui, PFNGLUNIFORM2UIPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glUniform3ui, PFNGLUNIFORM3UIPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glUniform4ui, PFNGLUNIFORM4UIPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glUniform1uiv, PFNGLUNIFORM1UIVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glUniform2uiv, PFNGLUNIFORM2UIVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glUniform3uiv, PFNGLUNIFORM3UIVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glUniform4uiv, PFNGLUNIFORM4UIVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glTexParameterIiv, PFNGLTEXPARAMETERIIVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glTexParameterIuiv, PFNGLTEXPARAMETERIUIVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glGetTexParameterIiv, PFNGLGETTEXPARAMETERIIVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glGetTexParameterIuiv, PFNGLGETTEXPARAMETERIUIVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glClearBufferiv, PFNGLCLEARBUFFERIVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glClearBufferuiv, PFNGLCLEARBUFFERUIVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glClearBufferfv, PFNGLCLEARBUFFERFVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glClearBufferfi, PFNGLCLEARBUFFERFIPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glGetStringi, PFNGLGETSTRINGIPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glIsRenderbuffer, PFNGLISRENDERBUFFERPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glBindRenderbuffer, PFNGLBINDRENDERBUFFERPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glDeleteRenderbuffers, PFNGLDELETERENDERBUFFERSPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glGenRenderbuffers, PFNGLGENRENDERBUFFERSPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glRenderbufferStorage, PFNGLRENDERBUFFERSTORAGEPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glGetRenderbufferParameteriv, PFNGLGETRENDERBUFFERPARAMETERIVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glIsFramebuffer, PFNGLISFRAMEBUFFERPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glBindFramebuffer, PFNGLBINDFRAMEBUFFERPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glDeleteFramebuffers, PFNGLDELETEFRAMEBUFFERSPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glGenFramebuffers, PFNGLGENFRAMEBUFFERSPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glCheckFramebufferStatus, PFNGLCHECKFRAMEBUFFERSTATUSPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glFramebufferTexture1D, PFNGLFRAMEBUFFERTEXTURE1DPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glFramebufferTexture2D, PFNGLFRAMEBUFFERTEXTURE2DPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glFramebufferTexture3D, PFNGLFRAMEBUFFERTEXTURE3DPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glFramebufferRenderbuffer, PFNGLFRAMEBUFFERRENDERBUFFERPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glGetFramebufferAttachmentParameteriv, PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glGenerateMipmap, PFNGLGENERATEMIPMAPPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glBlitFramebuffer, PFNGLBLITFRAMEBUFFERPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glRenderbufferStorageMultisample, PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glFramebufferTextureLayer, PFNGLFRAMEBUFFERTEXTURELAYERPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glMapBufferRange, PFNGLMAPBUFFERRANGEPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glFlushMappedBufferRange, PFNGLFLUSHMAPPEDBUFFERRANGEPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glBindVertexArray, PFNGLBINDVERTEXARRAYPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glDeleteVertexArrays, PFNGLDELETEVERTEXARRAYSPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glGenVertexArrays, PFNGLGENVERTEXARRAYSPROC)
  ENGINE_GL_VERSION_3_0_LOAD(glIsVertexArray, PFNGLISVERTEXARRAYPROC)
}

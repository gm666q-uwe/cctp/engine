#include "engine/graphics/gl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GL_VERSION_4_0_LOAD(name, type)                                                                         \
  m_data_version_4_0.name =                                                                                            \
    reinterpret_cast<type>(m_interface_context->get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GL_VERSION_4_0_FUNCTION(name, type, ...)                                                                \
  using namespace std::literals;                                                                                       \
  if (m_data_version_4_0.name == nullptr) [[unlikely]] {                                                               \
    ENGINE_GL_VERSION_4_0_LOAD(name, type)                                                                             \
  }                                                                                                                    \
  return m_data_version_4_0.name(__VA_ARGS__);

auto
engine::graphics::gl::Context::minSampleShading(GLfloat value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glMinSampleShading, PFNGLMINSAMPLESHADINGPROC, value)
}

auto
engine::graphics::gl::Context::blendEquationi(GLuint buf, GLenum mode) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glBlendEquationi, PFNGLBLENDEQUATIONIPROC, buf, mode)
}

auto
engine::graphics::gl::Context::blendEquationSeparatei(GLuint buf, GLenum modeRGB, GLenum modeAlpha) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glBlendEquationSeparatei, PFNGLBLENDEQUATIONSEPARATEIPROC, buf, modeRGB, modeAlpha)
}

auto
engine::graphics::gl::Context::blendFunci(GLuint buf, GLenum src, GLenum dst) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glBlendFunci, PFNGLBLENDFUNCIPROC, buf, src, dst)
}

auto
engine::graphics::gl::Context::blendFuncSeparatei(GLuint buf,
						  GLenum srcRGB,
						  GLenum dstRGB,
						  GLenum srcAlpha,
						  GLenum dstAlpha) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(
    glBlendFuncSeparatei, PFNGLBLENDFUNCSEPARATEIPROC, buf, srcRGB, dstRGB, srcAlpha, dstAlpha)
}

auto
engine::graphics::gl::Context::drawArraysIndirect(GLenum mode, void const* indirect) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glDrawArraysIndirect, PFNGLDRAWARRAYSINDIRECTPROC, mode, indirect)
}

auto
engine::graphics::gl::Context::drawElementsIndirect(GLenum mode, GLenum type, void const* indirect) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glDrawElementsIndirect, PFNGLDRAWELEMENTSINDIRECTPROC, mode, type, indirect)
}

auto
engine::graphics::gl::Context::uniform1d(GLint location, GLdouble x) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glUniform1d, PFNGLUNIFORM1DPROC, location, x)
}

auto
engine::graphics::gl::Context::uniform2d(GLint location, GLdouble x, GLdouble y) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glUniform2d, PFNGLUNIFORM2DPROC, location, x, y)
}

auto
engine::graphics::gl::Context::uniform3d(GLint location, GLdouble x, GLdouble y, GLdouble z) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glUniform3d, PFNGLUNIFORM3DPROC, location, x, y, z)
}

auto
engine::graphics::gl::Context::uniform4d(GLint location, GLdouble x, GLdouble y, GLdouble z, GLdouble w) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glUniform4d, PFNGLUNIFORM4DPROC, location, x, y, z, w)
}

auto
engine::graphics::gl::Context::uniform1dv(GLint location, GLsizei count, GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glUniform1dv, PFNGLUNIFORM1DVPROC, location, count, value)
}

auto
engine::graphics::gl::Context::uniform2dv(GLint location, GLsizei count, GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glUniform2dv, PFNGLUNIFORM2DVPROC, location, count, value)
}

auto
engine::graphics::gl::Context::uniform3dv(GLint location, GLsizei count, GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glUniform3dv, PFNGLUNIFORM3DVPROC, location, count, value)
}

auto
engine::graphics::gl::Context::uniform4dv(GLint location, GLsizei count, GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glUniform4dv, PFNGLUNIFORM4DVPROC, location, count, value)
}

auto
engine::graphics::gl::Context::uniformMatrix2dv(GLint location,
						GLsizei count,
						GLboolean transpose,
						GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glUniformMatrix2dv, PFNGLUNIFORMMATRIX2DVPROC, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::uniformMatrix3dv(GLint location,
						GLsizei count,
						GLboolean transpose,
						GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glUniformMatrix3dv, PFNGLUNIFORMMATRIX3DVPROC, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::uniformMatrix4dv(GLint location,
						GLsizei count,
						GLboolean transpose,
						GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glUniformMatrix4dv, PFNGLUNIFORMMATRIX4DVPROC, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::uniformMatrix2x3dv(GLint location,
						  GLsizei count,
						  GLboolean transpose,
						  GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glUniformMatrix2x3dv, PFNGLUNIFORMMATRIX2X3DVPROC, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::uniformMatrix2x4dv(GLint location,
						  GLsizei count,
						  GLboolean transpose,
						  GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glUniformMatrix2x4dv, PFNGLUNIFORMMATRIX2X4DVPROC, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::uniformMatrix3x2dv(GLint location,
						  GLsizei count,
						  GLboolean transpose,
						  GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glUniformMatrix3x2dv, PFNGLUNIFORMMATRIX3X2DVPROC, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::uniformMatrix3x4dv(GLint location,
						  GLsizei count,
						  GLboolean transpose,
						  GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glUniformMatrix3x4dv, PFNGLUNIFORMMATRIX3X4DVPROC, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::uniformMatrix4x2dv(GLint location,
						  GLsizei count,
						  GLboolean transpose,
						  GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glUniformMatrix4x2dv, PFNGLUNIFORMMATRIX4X2DVPROC, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::uniformMatrix4x3dv(GLint location,
						  GLsizei count,
						  GLboolean transpose,
						  GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glUniformMatrix4x3dv, PFNGLUNIFORMMATRIX4X3DVPROC, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::getUniformdv(GLuint program, GLint location, GLdouble* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glGetUniformdv, PFNGLGETUNIFORMDVPROC, program, location, params)
}

auto
engine::graphics::gl::Context::getSubroutineUniformLocation(GLuint program,
							    GLenum shadertype,
							    GLchar const* name) & noexcept -> GLint
{
  ENGINE_GL_VERSION_4_0_FUNCTION(
    glGetSubroutineUniformLocation, PFNGLGETSUBROUTINEUNIFORMLOCATIONPROC, program, shadertype, name)
}

auto
engine::graphics::gl::Context::getSubroutineIndex(GLuint program, GLenum shadertype, GLchar const* name) & noexcept
  -> GLuint
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glGetSubroutineIndex, PFNGLGETSUBROUTINEINDEXPROC, program, shadertype, name)
}

auto
engine::graphics::gl::Context::getActiveSubroutineUniformiv(GLuint program,
							    GLenum shadertype,
							    GLuint index,
							    GLenum pname,
							    GLint* values) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(
    glGetActiveSubroutineUniformiv, PFNGLGETACTIVESUBROUTINEUNIFORMIVPROC, program, shadertype, index, pname, values)
}

auto
engine::graphics::gl::Context::getActiveSubroutineUniformName(GLuint program,
							      GLenum shadertype,
							      GLuint index,
							      GLsizei bufsize,
							      GLsizei* length,
							      GLchar* name) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glGetActiveSubroutineUniformName,
				 PFNGLGETACTIVESUBROUTINEUNIFORMNAMEPROC,
				 program,
				 shadertype,
				 index,
				 bufsize,
				 length,
				 name)
}

auto
engine::graphics::gl::Context::getActiveSubroutineName(GLuint program,
						       GLenum shadertype,
						       GLuint index,
						       GLsizei bufsize,
						       GLsizei* length,
						       GLchar* name) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(
    glGetActiveSubroutineName, PFNGLGETACTIVESUBROUTINENAMEPROC, program, shadertype, index, bufsize, length, name)
}

auto
engine::graphics::gl::Context::uniformSubroutinesuiv(GLenum shadertype, GLsizei count, GLuint const* indices) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glUniformSubroutinesuiv, PFNGLUNIFORMSUBROUTINESUIVPROC, shadertype, count, indices)
}

auto
engine::graphics::gl::Context::getUniformSubroutineuiv(GLenum shadertype, GLint location, GLuint* params) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(
    glGetUniformSubroutineuiv, PFNGLGETUNIFORMSUBROUTINEUIVPROC, shadertype, location, params)
}

auto
engine::graphics::gl::Context::getProgramStageiv(GLuint program,
						 GLenum shadertype,
						 GLenum pname,
						 GLint* values) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glGetProgramStageiv, PFNGLGETPROGRAMSTAGEIVPROC, program, shadertype, pname, values)
}

auto
engine::graphics::gl::Context::patchParameteri(GLenum pname, GLint value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glPatchParameteri, PFNGLPATCHPARAMETERIPROC, pname, value)
}

auto
engine::graphics::gl::Context::patchParameterfv(GLenum pname, GLfloat const* values) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glPatchParameterfv, PFNGLPATCHPARAMETERFVPROC, pname, values)
}

auto
engine::graphics::gl::Context::bindTransformFeedback(GLenum target, GLuint id) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glBindTransformFeedback, PFNGLBINDTRANSFORMFEEDBACKPROC, target, id)
}

auto
engine::graphics::gl::Context::deleteTransformFeedbacks(GLsizei n, GLuint const* ids) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glDeleteTransformFeedbacks, PFNGLDELETETRANSFORMFEEDBACKSPROC, n, ids)
}

auto
engine::graphics::gl::Context::genTransformFeedbacks(GLsizei n, GLuint* ids) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glGenTransformFeedbacks, PFNGLGENTRANSFORMFEEDBACKSPROC, n, ids)
}

auto
engine::graphics::gl::Context::isTransformFeedback(GLuint id) & noexcept -> GLboolean
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glIsTransformFeedback, PFNGLISTRANSFORMFEEDBACKPROC, id)
}

auto
engine::graphics::gl::Context::pauseTransformFeedback() & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glPauseTransformFeedback, PFNGLPAUSETRANSFORMFEEDBACKPROC)
}

auto
engine::graphics::gl::Context::resumeTransformFeedback() & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glResumeTransformFeedback, PFNGLRESUMETRANSFORMFEEDBACKPROC)
}

auto
engine::graphics::gl::Context::drawTransformFeedback(GLenum mode, GLuint id) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glDrawTransformFeedback, PFNGLDRAWTRANSFORMFEEDBACKPROC, mode, id)
}

auto
engine::graphics::gl::Context::drawTransformFeedbackStream(GLenum mode, GLuint id, GLuint stream) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glDrawTransformFeedbackStream, PFNGLDRAWTRANSFORMFEEDBACKSTREAMPROC, mode, id, stream)
}

auto
engine::graphics::gl::Context::beginQueryIndexed(GLenum target, GLuint index, GLuint id) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glBeginQueryIndexed, PFNGLBEGINQUERYINDEXEDPROC, target, index, id)
}

auto
engine::graphics::gl::Context::endQueryIndexed(GLenum target, GLuint index) & noexcept -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glEndQueryIndexed, PFNGLENDQUERYINDEXEDPROC, target, index)
}

auto
engine::graphics::gl::Context::getQueryIndexediv(GLenum target, GLuint index, GLenum pname, GLint* params) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_0_FUNCTION(glGetQueryIndexediv, PFNGLGETQUERYINDEXEDIVPROC, target, index, pname, params)
}

auto
engine::graphics::gl::Context::load_version_4_0() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GL_VERSION_4_0_LOAD(glMinSampleShading, PFNGLMINSAMPLESHADINGPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glBlendEquationi, PFNGLBLENDEQUATIONIPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glBlendEquationSeparatei, PFNGLBLENDEQUATIONSEPARATEIPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glBlendFunci, PFNGLBLENDFUNCIPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glBlendFuncSeparatei, PFNGLBLENDFUNCSEPARATEIPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glDrawArraysIndirect, PFNGLDRAWARRAYSINDIRECTPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glDrawElementsIndirect, PFNGLDRAWELEMENTSINDIRECTPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glUniform1d, PFNGLUNIFORM1DPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glUniform2d, PFNGLUNIFORM2DPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glUniform3d, PFNGLUNIFORM3DPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glUniform4d, PFNGLUNIFORM4DPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glUniform1dv, PFNGLUNIFORM1DVPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glUniform2dv, PFNGLUNIFORM2DVPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glUniform3dv, PFNGLUNIFORM3DVPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glUniform4dv, PFNGLUNIFORM4DVPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glUniformMatrix2dv, PFNGLUNIFORMMATRIX2DVPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glUniformMatrix3dv, PFNGLUNIFORMMATRIX3DVPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glUniformMatrix4dv, PFNGLUNIFORMMATRIX4DVPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glUniformMatrix2x3dv, PFNGLUNIFORMMATRIX2X3DVPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glUniformMatrix2x4dv, PFNGLUNIFORMMATRIX2X4DVPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glUniformMatrix3x2dv, PFNGLUNIFORMMATRIX3X2DVPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glUniformMatrix3x4dv, PFNGLUNIFORMMATRIX3X4DVPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glUniformMatrix4x2dv, PFNGLUNIFORMMATRIX4X2DVPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glUniformMatrix4x3dv, PFNGLUNIFORMMATRIX4X3DVPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glGetUniformdv, PFNGLGETUNIFORMDVPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glGetSubroutineUniformLocation, PFNGLGETSUBROUTINEUNIFORMLOCATIONPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glGetSubroutineIndex, PFNGLGETSUBROUTINEINDEXPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glGetActiveSubroutineUniformiv, PFNGLGETACTIVESUBROUTINEUNIFORMIVPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glGetActiveSubroutineUniformName, PFNGLGETACTIVESUBROUTINEUNIFORMNAMEPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glGetActiveSubroutineName, PFNGLGETACTIVESUBROUTINENAMEPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glUniformSubroutinesuiv, PFNGLUNIFORMSUBROUTINESUIVPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glGetUniformSubroutineuiv, PFNGLGETUNIFORMSUBROUTINEUIVPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glGetProgramStageiv, PFNGLGETPROGRAMSTAGEIVPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glPatchParameteri, PFNGLPATCHPARAMETERIPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glPatchParameterfv, PFNGLPATCHPARAMETERFVPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glBindTransformFeedback, PFNGLBINDTRANSFORMFEEDBACKPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glDeleteTransformFeedbacks, PFNGLDELETETRANSFORMFEEDBACKSPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glGenTransformFeedbacks, PFNGLGENTRANSFORMFEEDBACKSPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glIsTransformFeedback, PFNGLISTRANSFORMFEEDBACKPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glPauseTransformFeedback, PFNGLPAUSETRANSFORMFEEDBACKPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glResumeTransformFeedback, PFNGLRESUMETRANSFORMFEEDBACKPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glDrawTransformFeedback, PFNGLDRAWTRANSFORMFEEDBACKPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glDrawTransformFeedbackStream, PFNGLDRAWTRANSFORMFEEDBACKSTREAMPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glBeginQueryIndexed, PFNGLBEGINQUERYINDEXEDPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glEndQueryIndexed, PFNGLENDQUERYINDEXEDPROC)
  ENGINE_GL_VERSION_4_0_LOAD(glGetQueryIndexediv, PFNGLGETQUERYINDEXEDIVPROC)
}

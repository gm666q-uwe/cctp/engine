#include "engine/graphics/gl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GL_VERSION_4_3_LOAD(name, type)                                                                         \
  m_data_version_4_3.name =                                                                                            \
    reinterpret_cast<type>(m_interface_context->get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GL_VERSION_4_3_FUNCTION(name, type, ...)                                                                \
  using namespace std::literals;                                                                                       \
  if (m_data_version_4_3.name == nullptr) [[unlikely]] {                                                               \
    ENGINE_GL_VERSION_4_3_LOAD(name, type)                                                                             \
  }                                                                                                                    \
  return m_data_version_4_3.name(__VA_ARGS__);

auto
engine::graphics::gl::Context::clearBufferData(GLenum target,
					       GLenum internalformat,
					       GLenum format,
					       GLenum type,
					       void const* data) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(
    glClearBufferData, PFNGLCLEARBUFFERDATAPROC, target, internalformat, format, type, data)
}

auto
engine::graphics::gl::Context::clearBufferSubData(GLenum target,
						  GLenum internalformat,
						  GLintptr offset,
						  GLsizeiptr size,
						  GLenum format,
						  GLenum type,
						  void const* data) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(
    glClearBufferSubData, PFNGLCLEARBUFFERSUBDATAPROC, target, internalformat, offset, size, format, type, data)
}

auto
engine::graphics::gl::Context::dispatchCompute(GLuint num_groups_x, GLuint num_groups_y, GLuint num_groups_z) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glDispatchCompute, PFNGLDISPATCHCOMPUTEPROC, num_groups_x, num_groups_y, num_groups_z)
}

auto
engine::graphics::gl::Context::dispatchComputeIndirect(GLintptr indirect) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glDispatchComputeIndirect, PFNGLDISPATCHCOMPUTEINDIRECTPROC, indirect)
}

auto
engine::graphics::gl::Context::copyImageSubData(GLuint srcName,
						GLenum srcTarget,
						GLint srcLevel,
						GLint srcX,
						GLint srcY,
						GLint srcZ,
						GLuint dstName,
						GLenum dstTarget,
						GLint dstLevel,
						GLint dstX,
						GLint dstY,
						GLint dstZ,
						GLsizei srcWidth,
						GLsizei srcHeight,
						GLsizei srcDepth) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glCopyImageSubData,
				 PFNGLCOPYIMAGESUBDATAPROC,
				 srcName,
				 srcTarget,
				 srcLevel,
				 srcX,
				 srcY,
				 srcZ,
				 dstName,
				 dstTarget,
				 dstLevel,
				 dstX,
				 dstY,
				 dstZ,
				 srcWidth,
				 srcHeight,
				 srcDepth)
}

auto
engine::graphics::gl::Context::framebufferParameteri(GLenum target, GLenum pname, GLint param) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glFramebufferParameteri, PFNGLFRAMEBUFFERPARAMETERIPROC, target, pname, param)
}

auto
engine::graphics::gl::Context::getFramebufferParameteriv(GLenum target, GLenum pname, GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glGetFramebufferParameteriv, PFNGLGETFRAMEBUFFERPARAMETERIVPROC, target, pname, params)
}

auto
engine::graphics::gl::Context::getInternalformati64v(GLenum target,
						     GLenum internalformat,
						     GLenum pname,
						     GLsizei bufSize,
						     GLint64* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(
    glGetInternalformati64v, PFNGLGETINTERNALFORMATI64VPROC, target, internalformat, pname, bufSize, params)
}

auto
engine::graphics::gl::Context::invalidateTexSubImage(GLuint texture,
						     GLint level,
						     GLint xoffset,
						     GLint yoffset,
						     GLint zoffset,
						     GLsizei width,
						     GLsizei height,
						     GLsizei depth) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glInvalidateTexSubImage,
				 PFNGLINVALIDATETEXSUBIMAGEPROC,
				 texture,
				 level,
				 xoffset,
				 yoffset,
				 zoffset,
				 width,
				 height,
				 depth)
}

auto
engine::graphics::gl::Context::invalidateTexImage(GLuint texture, GLint level) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glInvalidateTexImage, PFNGLINVALIDATETEXIMAGEPROC, texture, level)
}

auto
engine::graphics::gl::Context::invalidateBufferSubData(GLuint buffer, GLintptr offset, GLsizeiptr length) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glInvalidateBufferSubData, PFNGLINVALIDATEBUFFERSUBDATAPROC, buffer, offset, length)
}

auto
engine::graphics::gl::Context::invalidateBufferData(GLuint buffer) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glInvalidateBufferData, PFNGLINVALIDATEBUFFERDATAPROC, buffer)
}

auto
engine::graphics::gl::Context::invalidateFramebuffer(GLenum target,
						     GLsizei numAttachments,
						     GLenum const* attachments) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(
    glInvalidateFramebuffer, PFNGLINVALIDATEFRAMEBUFFERPROC, target, numAttachments, attachments)
}

auto
engine::graphics::gl::Context::invalidateSubFramebuffer(GLenum target,
							GLsizei numAttachments,
							GLenum const* attachments,
							GLint x,
							GLint y,
							GLsizei width,
							GLsizei height) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glInvalidateSubFramebuffer,
				 PFNGLINVALIDATESUBFRAMEBUFFERPROC,
				 target,
				 numAttachments,
				 attachments,
				 x,
				 y,
				 width,
				 height)
}

auto
engine::graphics::gl::Context::multiDrawArraysIndirect(GLenum mode,
						       void const* indirect,
						       GLsizei drawcount,
						       GLsizei stride) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(
    glMultiDrawArraysIndirect, PFNGLMULTIDRAWARRAYSINDIRECTPROC, mode, indirect, drawcount, stride)
}

auto
engine::graphics::gl::Context::multiDrawElementsIndirect(GLenum mode,
							 GLenum type,
							 void const* indirect,
							 GLsizei drawcount,
							 GLsizei stride) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(
    glMultiDrawElementsIndirect, PFNGLMULTIDRAWELEMENTSINDIRECTPROC, mode, type, indirect, drawcount, stride)
}

auto
engine::graphics::gl::Context::getProgramInterfaceiv(GLuint program,
						     GLenum programInterface,
						     GLenum pname,
						     GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(
    glGetProgramInterfaceiv, PFNGLGETPROGRAMINTERFACEIVPROC, program, programInterface, pname, params)
}

auto
engine::graphics::gl::Context::getProgramResourceIndex(GLuint program,
						       GLenum programInterface,
						       GLchar const* name) & noexcept -> GLuint
{
  ENGINE_GL_VERSION_4_3_FUNCTION(
    glGetProgramResourceIndex, PFNGLGETPROGRAMRESOURCEINDEXPROC, program, programInterface, name)
}

auto
engine::graphics::gl::Context::getProgramResourceName(GLuint program,
						      GLenum programInterface,
						      GLuint index,
						      GLsizei bufSize,
						      GLsizei* length,
						      GLchar* name) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(
    glGetProgramResourceName, PFNGLGETPROGRAMRESOURCENAMEPROC, program, programInterface, index, bufSize, length, name)
}

auto
engine::graphics::gl::Context::getProgramResourceiv(GLuint program,
						    GLenum programInterface,
						    GLuint index,
						    GLsizei propCount,
						    GLenum const* props,
						    GLsizei bufSize,
						    GLsizei* length,
						    GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glGetProgramResourceiv,
				 PFNGLGETPROGRAMRESOURCEIVPROC,
				 program,
				 programInterface,
				 index,
				 propCount,
				 props,
				 bufSize,
				 length,
				 params)
}

auto
engine::graphics::gl::Context::getProgramResourceLocation(GLuint program,
							  GLenum programInterface,
							  GLchar const* name) & noexcept -> GLint
{
  ENGINE_GL_VERSION_4_3_FUNCTION(
    glGetProgramResourceLocation, PFNGLGETPROGRAMRESOURCELOCATIONPROC, program, programInterface, name)
}

auto
engine::graphics::gl::Context::getProgramResourceLocationIndex(GLuint program,
							       GLenum programInterface,
							       GLchar const* name) & noexcept -> GLint
{
  ENGINE_GL_VERSION_4_3_FUNCTION(
    glGetProgramResourceLocationIndex, PFNGLGETPROGRAMRESOURCELOCATIONINDEXPROC, program, programInterface, name)
}

auto
engine::graphics::gl::Context::shaderStorageBlockBinding(GLuint program,
							 GLuint storageBlockIndex,
							 GLuint storageBlockBinding) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(
    glShaderStorageBlockBinding, PFNGLSHADERSTORAGEBLOCKBINDINGPROC, program, storageBlockIndex, storageBlockBinding)
}

auto
engine::graphics::gl::Context::texBufferRange(GLenum target,
					      GLenum internalformat,
					      GLuint buffer,
					      GLintptr offset,
					      GLsizeiptr size) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(
    glTexBufferRange, PFNGLTEXBUFFERRANGEPROC, target, internalformat, buffer, offset, size)
}

auto
engine::graphics::gl::Context::texStorage2DMultisample(GLenum target,
						       GLsizei samples,
						       GLenum internalformat,
						       GLsizei width,
						       GLsizei height,
						       GLboolean fixedsamplelocations) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glTexStorage2DMultisample,
				 PFNGLTEXSTORAGE2DMULTISAMPLEPROC,
				 target,
				 samples,
				 internalformat,
				 width,
				 height,
				 fixedsamplelocations)
}

auto
engine::graphics::gl::Context::texStorage3DMultisample(GLenum target,
						       GLsizei samples,
						       GLenum internalformat,
						       GLsizei width,
						       GLsizei height,
						       GLsizei depth,
						       GLboolean fixedsamplelocations) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glTexStorage3DMultisample,
				 PFNGLTEXSTORAGE3DMULTISAMPLEPROC,
				 target,
				 samples,
				 internalformat,
				 width,
				 height,
				 depth,
				 fixedsamplelocations)
}

auto
engine::graphics::gl::Context::textureView(GLuint texture,
					   GLenum target,
					   GLuint origtexture,
					   GLenum internalformat,
					   GLuint minlevel,
					   GLuint numlevels,
					   GLuint minlayer,
					   GLuint numlayers) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glTextureView,
				 PFNGLTEXTUREVIEWPROC,
				 texture,
				 target,
				 origtexture,
				 internalformat,
				 minlevel,
				 numlevels,
				 minlayer,
				 numlayers)
}

auto
engine::graphics::gl::Context::bindVertexBuffer(GLuint bindingindex,
						GLuint buffer,
						GLintptr offset,
						GLsizei stride) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glBindVertexBuffer, PFNGLBINDVERTEXBUFFERPROC, bindingindex, buffer, offset, stride)
}

auto
engine::graphics::gl::Context::vertexAttribFormat(GLuint attribindex,
						  GLint size,
						  GLenum type,
						  GLboolean normalized,
						  GLuint relativeoffset) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(
    glVertexAttribFormat, PFNGLVERTEXATTRIBFORMATPROC, attribindex, size, type, normalized, relativeoffset)
}

auto
engine::graphics::gl::Context::vertexAttribIFormat(GLuint attribindex,
						   GLint size,
						   GLenum type,
						   GLuint relativeoffset) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(
    glVertexAttribIFormat, PFNGLVERTEXATTRIBIFORMATPROC, attribindex, size, type, relativeoffset)
}

auto
engine::graphics::gl::Context::vertexAttribLFormat(GLuint attribindex,
						   GLint size,
						   GLenum type,
						   GLuint relativeoffset) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(
    glVertexAttribLFormat, PFNGLVERTEXATTRIBLFORMATPROC, attribindex, size, type, relativeoffset)
}

auto
engine::graphics::gl::Context::vertexAttribBinding(GLuint attribindex, GLuint bindingindex) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glVertexAttribBinding, PFNGLVERTEXATTRIBBINDINGPROC, attribindex, bindingindex)
}

auto
engine::graphics::gl::Context::vertexBindingDivisor(GLuint bindingindex, GLuint divisor) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glVertexBindingDivisor, PFNGLVERTEXBINDINGDIVISORPROC, bindingindex, divisor)
}

auto
engine::graphics::gl::Context::debugMessageControl(GLenum source,
						   GLenum type,
						   GLenum severity,
						   GLsizei count,
						   GLuint const* ids,
						   GLboolean enabled) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(
    glDebugMessageControl, PFNGLDEBUGMESSAGECONTROLPROC, source, type, severity, count, ids, enabled)
}

auto
engine::graphics::gl::Context::debugMessageInsert(GLenum source,
						  GLenum type,
						  GLuint id,
						  GLenum severity,
						  GLsizei length,
						  GLchar const* buf) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(
    glDebugMessageInsert, PFNGLDEBUGMESSAGEINSERTPROC, source, type, id, severity, length, buf)
}

auto
engine::graphics::gl::Context::debugMessageCallback(GLDEBUGPROC callback, void const* userParam) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glDebugMessageCallback, PFNGLDEBUGMESSAGECALLBACKPROC, callback, userParam)
}

auto
engine::graphics::gl::Context::getDebugMessageLog(GLuint count,
						  GLsizei bufSize,
						  GLenum* sources,
						  GLenum* types,
						  GLuint* ids,
						  GLenum* severities,
						  GLsizei* lengths,
						  GLchar* messageLog) & noexcept -> GLuint
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glGetDebugMessageLog,
				 PFNGLGETDEBUGMESSAGELOGPROC,
				 count,
				 bufSize,
				 sources,
				 types,
				 ids,
				 severities,
				 lengths,
				 messageLog)
}

auto
engine::graphics::gl::Context::pushDebugGroup(GLenum source,
					      GLuint id,
					      GLsizei length,
					      GLchar const* message) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glPushDebugGroup, PFNGLPUSHDEBUGGROUPPROC, source, id, length, message)
}

auto
engine::graphics::gl::Context::popDebugGroup() & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glPopDebugGroup, PFNGLPOPDEBUGGROUPPROC)
}

auto
engine::graphics::gl::Context::objectLabel(GLenum identifier,
					   GLuint name,
					   GLsizei length,
					   GLchar const* label) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glObjectLabel, PFNGLOBJECTLABELPROC, identifier, name, length, label)
}

auto
engine::graphics::gl::Context::getObjectLabel(GLenum identifier,
					      GLuint name,
					      GLsizei bufSize,
					      GLsizei* length,
					      GLchar* label) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glGetObjectLabel, PFNGLGETOBJECTLABELPROC, identifier, name, bufSize, length, label)
}

auto
engine::graphics::gl::Context::objectPtrLabel(void const* ptr, GLsizei length, GLchar const* label) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glObjectPtrLabel, PFNGLOBJECTPTRLABELPROC, ptr, length, label)
}

auto
engine::graphics::gl::Context::getObjectPtrLabel(void const* ptr,
						 GLsizei bufSize,
						 GLsizei* length,
						 GLchar* label) & noexcept -> void
{
  ENGINE_GL_VERSION_4_3_FUNCTION(glGetObjectPtrLabel, PFNGLGETOBJECTPTRLABELPROC, ptr, bufSize, length, label)
}

auto
engine::graphics::gl::Context::load_version_4_3() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GL_VERSION_4_3_LOAD(glClearBufferData, PFNGLCLEARBUFFERDATAPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glClearBufferSubData, PFNGLCLEARBUFFERSUBDATAPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glDispatchCompute, PFNGLDISPATCHCOMPUTEPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glDispatchComputeIndirect, PFNGLDISPATCHCOMPUTEINDIRECTPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glCopyImageSubData, PFNGLCOPYIMAGESUBDATAPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glFramebufferParameteri, PFNGLFRAMEBUFFERPARAMETERIPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glGetFramebufferParameteriv, PFNGLGETFRAMEBUFFERPARAMETERIVPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glGetInternalformati64v, PFNGLGETINTERNALFORMATI64VPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glInvalidateTexSubImage, PFNGLINVALIDATETEXSUBIMAGEPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glInvalidateTexImage, PFNGLINVALIDATETEXIMAGEPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glInvalidateBufferSubData, PFNGLINVALIDATEBUFFERSUBDATAPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glInvalidateBufferData, PFNGLINVALIDATEBUFFERDATAPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glInvalidateFramebuffer, PFNGLINVALIDATEFRAMEBUFFERPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glInvalidateSubFramebuffer, PFNGLINVALIDATESUBFRAMEBUFFERPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glMultiDrawArraysIndirect, PFNGLMULTIDRAWARRAYSINDIRECTPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glMultiDrawElementsIndirect, PFNGLMULTIDRAWELEMENTSINDIRECTPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glGetProgramInterfaceiv, PFNGLGETPROGRAMINTERFACEIVPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glGetProgramResourceIndex, PFNGLGETPROGRAMRESOURCEINDEXPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glGetProgramResourceName, PFNGLGETPROGRAMRESOURCENAMEPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glGetProgramResourceiv, PFNGLGETPROGRAMRESOURCEIVPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glGetProgramResourceLocation, PFNGLGETPROGRAMRESOURCELOCATIONPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glGetProgramResourceLocationIndex, PFNGLGETPROGRAMRESOURCELOCATIONINDEXPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glShaderStorageBlockBinding, PFNGLSHADERSTORAGEBLOCKBINDINGPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glTexBufferRange, PFNGLTEXBUFFERRANGEPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glTexStorage2DMultisample, PFNGLTEXSTORAGE2DMULTISAMPLEPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glTexStorage3DMultisample, PFNGLTEXSTORAGE3DMULTISAMPLEPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glTextureView, PFNGLTEXTUREVIEWPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glBindVertexBuffer, PFNGLBINDVERTEXBUFFERPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glVertexAttribFormat, PFNGLVERTEXATTRIBFORMATPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glVertexAttribIFormat, PFNGLVERTEXATTRIBIFORMATPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glVertexAttribLFormat, PFNGLVERTEXATTRIBLFORMATPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glVertexAttribBinding, PFNGLVERTEXATTRIBBINDINGPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glVertexBindingDivisor, PFNGLVERTEXBINDINGDIVISORPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glDebugMessageControl, PFNGLDEBUGMESSAGECONTROLPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glDebugMessageInsert, PFNGLDEBUGMESSAGEINSERTPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glDebugMessageCallback, PFNGLDEBUGMESSAGECALLBACKPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glGetDebugMessageLog, PFNGLGETDEBUGMESSAGELOGPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glPushDebugGroup, PFNGLPUSHDEBUGGROUPPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glPopDebugGroup, PFNGLPOPDEBUGGROUPPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glObjectLabel, PFNGLOBJECTLABELPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glGetObjectLabel, PFNGLGETOBJECTLABELPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glObjectPtrLabel, PFNGLOBJECTPTRLABELPROC)
  ENGINE_GL_VERSION_4_3_LOAD(glGetObjectPtrLabel, PFNGLGETOBJECTPTRLABELPROC)
}

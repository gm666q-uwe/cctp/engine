#include "engine/graphics/gl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GL_VERSION_4_6_LOAD(name, type)                                                                         \
  m_data_version_4_6.name =                                                                                            \
    reinterpret_cast<type>(m_interface_context->get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GL_VERSION_4_6_FUNCTION(name, type, ...)                                                                \
  using namespace std::literals;                                                                                       \
  if (m_data_version_4_6.name == nullptr) [[unlikely]] {                                                               \
    ENGINE_GL_VERSION_4_6_LOAD(name, type)                                                                             \
  }                                                                                                                    \
  return m_data_version_4_6.name(__VA_ARGS__);

auto
engine::graphics::gl::Context::specializeShader(GLuint shader,
						GLchar const* pEntryPoint,
						GLuint numSpecializationConstants,
						GLuint const* pConstantIndex,
						GLuint const* pConstantValue) & noexcept -> void
{
  ENGINE_GL_VERSION_4_6_FUNCTION(glSpecializeShader,
				 PFNGLSPECIALIZESHADERPROC,
				 shader,
				 pEntryPoint,
				 numSpecializationConstants,
				 pConstantIndex,
				 pConstantValue)
}

auto
engine::graphics::gl::Context::multiDrawArraysIndirectCount(GLenum mode,
							    void const* indirect,
							    GLintptr drawcount,
							    GLsizei maxdrawcount,
							    GLsizei stride) & noexcept -> void
{
  ENGINE_GL_VERSION_4_6_FUNCTION(glMultiDrawArraysIndirectCount,
				 PFNGLMULTIDRAWARRAYSINDIRECTCOUNTPROC,
				 mode,
				 indirect,
				 drawcount,
				 maxdrawcount,
				 stride)
}

auto
engine::graphics::gl::Context::multiDrawElementsIndirectCount(GLenum mode,
							      GLenum type,
							      void const* indirect,
							      GLintptr drawcount,
							      GLsizei maxdrawcount,
							      GLsizei stride) & noexcept -> void
{
  ENGINE_GL_VERSION_4_6_FUNCTION(glMultiDrawElementsIndirectCount,
				 PFNGLMULTIDRAWELEMENTSINDIRECTCOUNTPROC,
				 mode,
				 type,
				 indirect,
				 drawcount,
				 maxdrawcount,
				 stride)
}

auto
engine::graphics::gl::Context::polygonOffsetClamp(GLfloat factor, GLfloat units, GLfloat clamp) & noexcept -> void
{
  ENGINE_GL_VERSION_4_6_FUNCTION(glPolygonOffsetClamp, PFNGLPOLYGONOFFSETCLAMPPROC, factor, units, clamp)
}

auto
engine::graphics::gl::Context::load_version_4_6() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GL_VERSION_4_6_LOAD(glSpecializeShader, PFNGLSPECIALIZESHADERPROC)
  ENGINE_GL_VERSION_4_6_LOAD(glMultiDrawArraysIndirectCount, PFNGLMULTIDRAWARRAYSINDIRECTCOUNTPROC)
  ENGINE_GL_VERSION_4_6_LOAD(glMultiDrawElementsIndirectCount, PFNGLMULTIDRAWELEMENTSINDIRECTCOUNTPROC)
  ENGINE_GL_VERSION_4_6_LOAD(glPolygonOffsetClamp, PFNGLPOLYGONOFFSETCLAMPPROC)
}

#include "engine/graphics/gl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GL_VERSION_4_1_LOAD(name, type)                                                                         \
  m_data_version_4_1.name =                                                                                            \
    reinterpret_cast<type>(m_interface_context->get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GL_VERSION_4_1_FUNCTION(name, type, ...)                                                                \
  using namespace std::literals;                                                                                       \
  if (m_data_version_4_1.name == nullptr) [[unlikely]] {                                                               \
    ENGINE_GL_VERSION_4_1_LOAD(name, type)                                                                             \
  }                                                                                                                    \
  return m_data_version_4_1.name(__VA_ARGS__);

auto
engine::graphics::gl::Context::releaseShaderCompiler() & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glReleaseShaderCompiler, PFNGLRELEASESHADERCOMPILERPROC)
}

auto
engine::graphics::gl::Context::shaderBinary(GLsizei count,
					    GLuint const* shaders,
					    GLenum binaryformat,
					    void const* binary,
					    GLsizei length) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glShaderBinary, PFNGLSHADERBINARYPROC, count, shaders, binaryformat, binary, length)
}

auto
engine::graphics::gl::Context::getShaderPrecisionFormat(GLenum shadertype,
							GLenum precisiontype,
							GLint* range,
							GLint* precision) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glGetShaderPrecisionFormat, PFNGLGETSHADERPRECISIONFORMATPROC, shadertype, precisiontype, range, precision)
}

auto
engine::graphics::gl::Context::depthRangef(GLfloat n, GLfloat f) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glDepthRangef, PFNGLDEPTHRANGEFPROC, n, f)
}

auto
engine::graphics::gl::Context::clearDepthf(GLfloat d) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glClearDepthf, PFNGLCLEARDEPTHFPROC, d)
}

auto
engine::graphics::gl::Context::getProgramBinary(GLuint program,
						GLsizei bufSize,
						GLsizei* length,
						GLenum* binaryFormat,
						void* binary) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glGetProgramBinary, PFNGLGETPROGRAMBINARYPROC, program, bufSize, length, binaryFormat, binary)
}

auto
engine::graphics::gl::Context::programBinary(GLuint program,
					     GLenum binaryFormat,
					     void const* binary,
					     GLsizei length) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramBinary, PFNGLPROGRAMBINARYPROC, program, binaryFormat, binary, length)
}

auto
engine::graphics::gl::Context::programParameteri(GLuint program, GLenum pname, GLint value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramParameteri, PFNGLPROGRAMPARAMETERIPROC, program, pname, value)
}

auto
engine::graphics::gl::Context::useProgramStages(GLuint pipeline, GLbitfield stages, GLuint program) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glUseProgramStages, PFNGLUSEPROGRAMSTAGESPROC, pipeline, stages, program)
}

auto
engine::graphics::gl::Context::activeShaderProgram(GLuint pipeline, GLuint program) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glActiveShaderProgram, PFNGLACTIVESHADERPROGRAMPROC, pipeline, program)
}

auto
engine::graphics::gl::Context::createShaderProgramv(GLenum type, GLsizei count, GLchar const* const* strings) & noexcept
  -> GLuint
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glCreateShaderProgramv, PFNGLCREATESHADERPROGRAMVPROC, type, count, strings)
}

auto
engine::graphics::gl::Context::bindProgramPipeline(GLuint pipeline) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glBindProgramPipeline, PFNGLBINDPROGRAMPIPELINEPROC, pipeline)
}

auto
engine::graphics::gl::Context::deleteProgramPipelines(GLsizei n, GLuint const* pipelines) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glDeleteProgramPipelines, PFNGLDELETEPROGRAMPIPELINESPROC, n, pipelines)
}

auto
engine::graphics::gl::Context::genProgramPipelines(GLsizei n, GLuint* pipelines) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glGenProgramPipelines, PFNGLGENPROGRAMPIPELINESPROC, n, pipelines)
}

auto
engine::graphics::gl::Context::isProgramPipeline(GLuint pipeline) & noexcept -> GLboolean
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glIsProgramPipeline, PFNGLISPROGRAMPIPELINEPROC, pipeline)
}

auto
engine::graphics::gl::Context::getProgramPipelineiv(GLuint pipeline, GLenum pname, GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glGetProgramPipelineiv, PFNGLGETPROGRAMPIPELINEIVPROC, pipeline, pname, params)
}

auto
engine::graphics::gl::Context::programUniform1i(GLuint program, GLint location, GLint v0) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform1i, PFNGLPROGRAMUNIFORM1IPROC, program, location, v0)
}

auto
engine::graphics::gl::Context::programUniform1iv(GLuint program,
						 GLint location,
						 GLsizei count,
						 GLint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform1iv, PFNGLPROGRAMUNIFORM1IVPROC, program, location, count, value)
}

auto
engine::graphics::gl::Context::programUniform1f(GLuint program, GLint location, GLfloat v0) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform1f, PFNGLPROGRAMUNIFORM1FPROC, program, location, v0)
}

auto
engine::graphics::gl::Context::programUniform1fv(GLuint program,
						 GLint location,
						 GLsizei count,
						 GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform1fv, PFNGLPROGRAMUNIFORM1FVPROC, program, location, count, value)
}

auto
engine::graphics::gl::Context::programUniform1d(GLuint program, GLint location, GLdouble v0) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform1d, PFNGLPROGRAMUNIFORM1DPROC, program, location, v0)
}

auto
engine::graphics::gl::Context::programUniform1dv(GLuint program,
						 GLint location,
						 GLsizei count,
						 GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform1dv, PFNGLPROGRAMUNIFORM1DVPROC, program, location, count, value)
}

auto
engine::graphics::gl::Context::programUniform1ui(GLuint program, GLint location, GLuint v0) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform1ui, PFNGLPROGRAMUNIFORM1UIPROC, program, location, v0)
}

auto
engine::graphics::gl::Context::programUniform1uiv(GLuint program,
						  GLint location,
						  GLsizei count,
						  GLuint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform1uiv, PFNGLPROGRAMUNIFORM1UIVPROC, program, location, count, value)
}

auto
engine::graphics::gl::Context::programUniform2i(GLuint program, GLint location, GLint v0, GLint v1) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform2i, PFNGLPROGRAMUNIFORM2IPROC, program, location, v0, v1)
}

auto
engine::graphics::gl::Context::programUniform2iv(GLuint program,
						 GLint location,
						 GLsizei count,
						 GLint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform2iv, PFNGLPROGRAMUNIFORM2IVPROC, program, location, count, value)
}

auto
engine::graphics::gl::Context::programUniform2f(GLuint program, GLint location, GLfloat v0, GLfloat v1) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform2f, PFNGLPROGRAMUNIFORM2FPROC, program, location, v0, v1)
}

auto
engine::graphics::gl::Context::programUniform2fv(GLuint program,
						 GLint location,
						 GLsizei count,
						 GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform2fv, PFNGLPROGRAMUNIFORM2FVPROC, program, location, count, value)
}

auto
engine::graphics::gl::Context::programUniform2d(GLuint program, GLint location, GLdouble v0, GLdouble v1) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform2d, PFNGLPROGRAMUNIFORM2DPROC, program, location, v0, v1)
}

auto
engine::graphics::gl::Context::programUniform2dv(GLuint program,
						 GLint location,
						 GLsizei count,
						 GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform2dv, PFNGLPROGRAMUNIFORM2DVPROC, program, location, count, value)
}

auto
engine::graphics::gl::Context::programUniform2ui(GLuint program, GLint location, GLuint v0, GLuint v1) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform2ui, PFNGLPROGRAMUNIFORM2UIPROC, program, location, v0, v1)
}

auto
engine::graphics::gl::Context::programUniform2uiv(GLuint program,
						  GLint location,
						  GLsizei count,
						  GLuint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform2uiv, PFNGLPROGRAMUNIFORM2UIVPROC, program, location, count, value)
}

auto
engine::graphics::gl::Context::programUniform3i(GLuint program, GLint location, GLint v0, GLint v1, GLint v2) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform3i, PFNGLPROGRAMUNIFORM3IPROC, program, location, v0, v1, v2)
}

auto
engine::graphics::gl::Context::programUniform3iv(GLuint program,
						 GLint location,
						 GLsizei count,
						 GLint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform3iv, PFNGLPROGRAMUNIFORM3IVPROC, program, location, count, value)
}

auto
engine::graphics::gl::Context::programUniform3f(GLuint program,
						GLint location,
						GLfloat v0,
						GLfloat v1,
						GLfloat v2) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform3f, PFNGLPROGRAMUNIFORM3FPROC, program, location, v0, v1, v2)
}

auto
engine::graphics::gl::Context::programUniform3fv(GLuint program,
						 GLint location,
						 GLsizei count,
						 GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform3fv, PFNGLPROGRAMUNIFORM3FVPROC, program, location, count, value)
}

auto
engine::graphics::gl::Context::programUniform3d(GLuint program,
						GLint location,
						GLdouble v0,
						GLdouble v1,
						GLdouble v2) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform3d, PFNGLPROGRAMUNIFORM3DPROC, program, location, v0, v1, v2)
}

auto
engine::graphics::gl::Context::programUniform3dv(GLuint program,
						 GLint location,
						 GLsizei count,
						 GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform3dv, PFNGLPROGRAMUNIFORM3DVPROC, program, location, count, value)
}

auto
engine::graphics::gl::Context::programUniform3ui(GLuint program,
						 GLint location,
						 GLuint v0,
						 GLuint v1,
						 GLuint v2) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform3ui, PFNGLPROGRAMUNIFORM3UIPROC, program, location, v0, v1, v2)
}

auto
engine::graphics::gl::Context::programUniform3uiv(GLuint program,
						  GLint location,
						  GLsizei count,
						  GLuint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform3uiv, PFNGLPROGRAMUNIFORM3UIVPROC, program, location, count, value)
}

auto
engine::graphics::gl::Context::programUniform4i(GLuint program,
						GLint location,
						GLint v0,
						GLint v1,
						GLint v2,
						GLint v3) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform4i, PFNGLPROGRAMUNIFORM4IPROC, program, location, v0, v1, v2, v3)
}

auto
engine::graphics::gl::Context::programUniform4iv(GLuint program,
						 GLint location,
						 GLsizei count,
						 GLint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform4iv, PFNGLPROGRAMUNIFORM4IVPROC, program, location, count, value)
}

auto
engine::graphics::gl::Context::programUniform4f(GLuint program,
						GLint location,
						GLfloat v0,
						GLfloat v1,
						GLfloat v2,
						GLfloat v3) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform4f, PFNGLPROGRAMUNIFORM4FPROC, program, location, v0, v1, v2, v3)
}

auto
engine::graphics::gl::Context::programUniform4fv(GLuint program,
						 GLint location,
						 GLsizei count,
						 GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform4fv, PFNGLPROGRAMUNIFORM4FVPROC, program, location, count, value)
}

auto
engine::graphics::gl::Context::programUniform4d(GLuint program,
						GLint location,
						GLdouble v0,
						GLdouble v1,
						GLdouble v2,
						GLdouble v3) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform4d, PFNGLPROGRAMUNIFORM4DPROC, program, location, v0, v1, v2, v3)
}

auto
engine::graphics::gl::Context::programUniform4dv(GLuint program,
						 GLint location,
						 GLsizei count,
						 GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform4dv, PFNGLPROGRAMUNIFORM4DVPROC, program, location, count, value)
}

auto
engine::graphics::gl::Context::programUniform4ui(GLuint program,
						 GLint location,
						 GLuint v0,
						 GLuint v1,
						 GLuint v2,
						 GLuint v3) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform4ui, PFNGLPROGRAMUNIFORM4UIPROC, program, location, v0, v1, v2, v3)
}

auto
engine::graphics::gl::Context::programUniform4uiv(GLuint program,
						  GLint location,
						  GLsizei count,
						  GLuint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glProgramUniform4uiv, PFNGLPROGRAMUNIFORM4UIVPROC, program, location, count, value)
}

auto
engine::graphics::gl::Context::programUniformMatrix2fv(GLuint program,
						       GLint location,
						       GLsizei count,
						       GLboolean transpose,
						       GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glProgramUniformMatrix2fv, PFNGLPROGRAMUNIFORMMATRIX2FVPROC, program, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::programUniformMatrix3fv(GLuint program,
						       GLint location,
						       GLsizei count,
						       GLboolean transpose,
						       GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glProgramUniformMatrix3fv, PFNGLPROGRAMUNIFORMMATRIX3FVPROC, program, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::programUniformMatrix4fv(GLuint program,
						       GLint location,
						       GLsizei count,
						       GLboolean transpose,
						       GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glProgramUniformMatrix4fv, PFNGLPROGRAMUNIFORMMATRIX4FVPROC, program, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::programUniformMatrix2dv(GLuint program,
						       GLint location,
						       GLsizei count,
						       GLboolean transpose,
						       GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glProgramUniformMatrix2dv, PFNGLPROGRAMUNIFORMMATRIX2DVPROC, program, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::programUniformMatrix3dv(GLuint program,
						       GLint location,
						       GLsizei count,
						       GLboolean transpose,
						       GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glProgramUniformMatrix3dv, PFNGLPROGRAMUNIFORMMATRIX3DVPROC, program, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::programUniformMatrix4dv(GLuint program,
						       GLint location,
						       GLsizei count,
						       GLboolean transpose,
						       GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glProgramUniformMatrix4dv, PFNGLPROGRAMUNIFORMMATRIX4DVPROC, program, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::programUniformMatrix2x3fv(GLuint program,
							 GLint location,
							 GLsizei count,
							 GLboolean transpose,
							 GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glProgramUniformMatrix2x3fv, PFNGLPROGRAMUNIFORMMATRIX2X3FVPROC, program, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::programUniformMatrix3x2fv(GLuint program,
							 GLint location,
							 GLsizei count,
							 GLboolean transpose,
							 GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glProgramUniformMatrix3x2fv, PFNGLPROGRAMUNIFORMMATRIX3X2FVPROC, program, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::programUniformMatrix2x4fv(GLuint program,
							 GLint location,
							 GLsizei count,
							 GLboolean transpose,
							 GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glProgramUniformMatrix2x4fv, PFNGLPROGRAMUNIFORMMATRIX2X4FVPROC, program, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::programUniformMatrix4x2fv(GLuint program,
							 GLint location,
							 GLsizei count,
							 GLboolean transpose,
							 GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glProgramUniformMatrix4x2fv, PFNGLPROGRAMUNIFORMMATRIX4X2FVPROC, program, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::programUniformMatrix3x4fv(GLuint program,
							 GLint location,
							 GLsizei count,
							 GLboolean transpose,
							 GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glProgramUniformMatrix3x4fv, PFNGLPROGRAMUNIFORMMATRIX3X4FVPROC, program, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::programUniformMatrix4x3fv(GLuint program,
							 GLint location,
							 GLsizei count,
							 GLboolean transpose,
							 GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glProgramUniformMatrix4x3fv, PFNGLPROGRAMUNIFORMMATRIX4X3FVPROC, program, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::programUniformMatrix2x3dv(GLuint program,
							 GLint location,
							 GLsizei count,
							 GLboolean transpose,
							 GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glProgramUniformMatrix2x3dv, PFNGLPROGRAMUNIFORMMATRIX2X3DVPROC, program, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::programUniformMatrix3x2dv(GLuint program,
							 GLint location,
							 GLsizei count,
							 GLboolean transpose,
							 GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glProgramUniformMatrix3x2dv, PFNGLPROGRAMUNIFORMMATRIX3X2DVPROC, program, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::programUniformMatrix2x4dv(GLuint program,
							 GLint location,
							 GLsizei count,
							 GLboolean transpose,
							 GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glProgramUniformMatrix2x4dv, PFNGLPROGRAMUNIFORMMATRIX2X4DVPROC, program, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::programUniformMatrix4x2dv(GLuint program,
							 GLint location,
							 GLsizei count,
							 GLboolean transpose,
							 GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glProgramUniformMatrix4x2dv, PFNGLPROGRAMUNIFORMMATRIX4X2DVPROC, program, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::programUniformMatrix3x4dv(GLuint program,
							 GLint location,
							 GLsizei count,
							 GLboolean transpose,
							 GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glProgramUniformMatrix3x4dv, PFNGLPROGRAMUNIFORMMATRIX3X4DVPROC, program, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::programUniformMatrix4x3dv(GLuint program,
							 GLint location,
							 GLsizei count,
							 GLboolean transpose,
							 GLdouble const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glProgramUniformMatrix4x3dv, PFNGLPROGRAMUNIFORMMATRIX4X3DVPROC, program, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::validateProgramPipeline(GLuint pipeline) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glValidateProgramPipeline, PFNGLVALIDATEPROGRAMPIPELINEPROC, pipeline)
}

auto
engine::graphics::gl::Context::getProgramPipelineInfoLog(GLuint pipeline,
							 GLsizei bufSize,
							 GLsizei* length,
							 GLchar* infoLog) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glGetProgramPipelineInfoLog, PFNGLGETPROGRAMPIPELINEINFOLOGPROC, pipeline, bufSize, length, infoLog)
}

auto
engine::graphics::gl::Context::vertexAttribL1d(GLuint index, GLdouble x) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glVertexAttribL1d, PFNGLVERTEXATTRIBL1DPROC, index, x)
}

auto
engine::graphics::gl::Context::vertexAttribL2d(GLuint index, GLdouble x, GLdouble y) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glVertexAttribL2d, PFNGLVERTEXATTRIBL2DPROC, index, x, y)
}

auto
engine::graphics::gl::Context::vertexAttribL3d(GLuint index, GLdouble x, GLdouble y, GLdouble z) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glVertexAttribL3d, PFNGLVERTEXATTRIBL3DPROC, index, x, y, z)
}

auto
engine::graphics::gl::Context::vertexAttribL4d(GLuint index, GLdouble x, GLdouble y, GLdouble z, GLdouble w) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glVertexAttribL4d, PFNGLVERTEXATTRIBL4DPROC, index, x, y, z, w)
}

auto
engine::graphics::gl::Context::vertexAttribL1dv(GLuint index, GLdouble const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glVertexAttribL1dv, PFNGLVERTEXATTRIBL1DVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttribL2dv(GLuint index, GLdouble const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glVertexAttribL2dv, PFNGLVERTEXATTRIBL2DVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttribL3dv(GLuint index, GLdouble const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glVertexAttribL3dv, PFNGLVERTEXATTRIBL3DVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttribL4dv(GLuint index, GLdouble const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glVertexAttribL4dv, PFNGLVERTEXATTRIBL4DVPROC, index, v)
}

auto
engine::graphics::gl::Context::vertexAttribLPointer(GLuint index,
						    GLint size,
						    GLenum type,
						    GLsizei stride,
						    void const* pointer) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(
    glVertexAttribLPointer, PFNGLVERTEXATTRIBLPOINTERPROC, index, size, type, stride, pointer)
}

auto
engine::graphics::gl::Context::getVertexAttribLdv(GLuint index, GLenum pname, GLdouble* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glGetVertexAttribLdv, PFNGLGETVERTEXATTRIBLDVPROC, index, pname, params)
}

auto
engine::graphics::gl::Context::viewportArrayv(GLuint first, GLsizei count, GLfloat const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glViewportArrayv, PFNGLVIEWPORTARRAYVPROC, first, count, v)
}

auto
engine::graphics::gl::Context::viewportIndexedf(GLuint index, GLfloat x, GLfloat y, GLfloat w, GLfloat h) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glViewportIndexedf, PFNGLVIEWPORTINDEXEDFPROC, index, x, y, w, h)
}

auto
engine::graphics::gl::Context::viewportIndexedfv(GLuint index, GLfloat const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glViewportIndexedfv, PFNGLVIEWPORTINDEXEDFVPROC, index, v)
}

auto
engine::graphics::gl::Context::scissorArrayv(GLuint first, GLsizei count, GLint const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glScissorArrayv, PFNGLSCISSORARRAYVPROC, first, count, v)
}

auto
engine::graphics::gl::Context::scissorIndexed(GLuint index,
					      GLint left,
					      GLint bottom,
					      GLsizei width,
					      GLsizei height) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glScissorIndexed, PFNGLSCISSORINDEXEDPROC, index, left, bottom, width, height)
}

auto
engine::graphics::gl::Context::scissorIndexedv(GLuint index, GLint const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glScissorIndexedv, PFNGLSCISSORINDEXEDVPROC, index, v)
}

auto
engine::graphics::gl::Context::depthRangeArrayv(GLuint first, GLsizei count, GLdouble const* v) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glDepthRangeArrayv, PFNGLDEPTHRANGEARRAYVPROC, first, count, v)
}

auto
engine::graphics::gl::Context::depthRangeIndexed(GLuint index, GLdouble n, GLdouble f) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glDepthRangeIndexed, PFNGLDEPTHRANGEINDEXEDPROC, index, n, f)
}

auto
engine::graphics::gl::Context::getFloati_v(GLenum target, GLuint index, GLfloat* data) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glGetFloati_v, PFNGLGETFLOATI_VPROC, target, index, data)
}

auto
engine::graphics::gl::Context::getDoublei_v(GLenum target, GLuint index, GLdouble* data) & noexcept -> void
{
  ENGINE_GL_VERSION_4_1_FUNCTION(glGetDoublei_v, PFNGLGETDOUBLEI_VPROC, target, index, data)
}

auto
engine::graphics::gl::Context::load_version_4_1() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GL_VERSION_4_1_LOAD(glReleaseShaderCompiler, PFNGLRELEASESHADERCOMPILERPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glShaderBinary, PFNGLSHADERBINARYPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glGetShaderPrecisionFormat, PFNGLGETSHADERPRECISIONFORMATPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glDepthRangef, PFNGLDEPTHRANGEFPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glClearDepthf, PFNGLCLEARDEPTHFPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glGetProgramBinary, PFNGLGETPROGRAMBINARYPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramBinary, PFNGLPROGRAMBINARYPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramParameteri, PFNGLPROGRAMPARAMETERIPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glUseProgramStages, PFNGLUSEPROGRAMSTAGESPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glActiveShaderProgram, PFNGLACTIVESHADERPROGRAMPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glCreateShaderProgramv, PFNGLCREATESHADERPROGRAMVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glBindProgramPipeline, PFNGLBINDPROGRAMPIPELINEPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glDeleteProgramPipelines, PFNGLDELETEPROGRAMPIPELINESPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glGenProgramPipelines, PFNGLGENPROGRAMPIPELINESPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glIsProgramPipeline, PFNGLISPROGRAMPIPELINEPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glGetProgramPipelineiv, PFNGLGETPROGRAMPIPELINEIVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform1i, PFNGLPROGRAMUNIFORM1IPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform1iv, PFNGLPROGRAMUNIFORM1IVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform1f, PFNGLPROGRAMUNIFORM1FPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform1fv, PFNGLPROGRAMUNIFORM1FVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform1d, PFNGLPROGRAMUNIFORM1DPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform1dv, PFNGLPROGRAMUNIFORM1DVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform1ui, PFNGLPROGRAMUNIFORM1UIPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform1uiv, PFNGLPROGRAMUNIFORM1UIVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform2i, PFNGLPROGRAMUNIFORM2IPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform2iv, PFNGLPROGRAMUNIFORM2IVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform2f, PFNGLPROGRAMUNIFORM2FPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform2fv, PFNGLPROGRAMUNIFORM2FVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform2d, PFNGLPROGRAMUNIFORM2DPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform2dv, PFNGLPROGRAMUNIFORM2DVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform2ui, PFNGLPROGRAMUNIFORM2UIPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform2uiv, PFNGLPROGRAMUNIFORM2UIVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform3i, PFNGLPROGRAMUNIFORM3IPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform3iv, PFNGLPROGRAMUNIFORM3IVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform3f, PFNGLPROGRAMUNIFORM3FPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform3fv, PFNGLPROGRAMUNIFORM3FVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform3d, PFNGLPROGRAMUNIFORM3DPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform3dv, PFNGLPROGRAMUNIFORM3DVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform3ui, PFNGLPROGRAMUNIFORM3UIPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform3uiv, PFNGLPROGRAMUNIFORM3UIVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform4i, PFNGLPROGRAMUNIFORM4IPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform4iv, PFNGLPROGRAMUNIFORM4IVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform4f, PFNGLPROGRAMUNIFORM4FPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform4fv, PFNGLPROGRAMUNIFORM4FVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform4d, PFNGLPROGRAMUNIFORM4DPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform4dv, PFNGLPROGRAMUNIFORM4DVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform4ui, PFNGLPROGRAMUNIFORM4UIPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniform4uiv, PFNGLPROGRAMUNIFORM4UIVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniformMatrix2fv, PFNGLPROGRAMUNIFORMMATRIX2FVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniformMatrix3fv, PFNGLPROGRAMUNIFORMMATRIX3FVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniformMatrix4fv, PFNGLPROGRAMUNIFORMMATRIX4FVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniformMatrix2dv, PFNGLPROGRAMUNIFORMMATRIX2DVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniformMatrix3dv, PFNGLPROGRAMUNIFORMMATRIX3DVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniformMatrix4dv, PFNGLPROGRAMUNIFORMMATRIX4DVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniformMatrix2x3fv, PFNGLPROGRAMUNIFORMMATRIX2X3FVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniformMatrix3x2fv, PFNGLPROGRAMUNIFORMMATRIX3X2FVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniformMatrix2x4fv, PFNGLPROGRAMUNIFORMMATRIX2X4FVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniformMatrix4x2fv, PFNGLPROGRAMUNIFORMMATRIX4X2FVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniformMatrix3x4fv, PFNGLPROGRAMUNIFORMMATRIX3X4FVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniformMatrix4x3fv, PFNGLPROGRAMUNIFORMMATRIX4X3FVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniformMatrix2x3dv, PFNGLPROGRAMUNIFORMMATRIX2X3DVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniformMatrix3x2dv, PFNGLPROGRAMUNIFORMMATRIX3X2DVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniformMatrix2x4dv, PFNGLPROGRAMUNIFORMMATRIX2X4DVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniformMatrix4x2dv, PFNGLPROGRAMUNIFORMMATRIX4X2DVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniformMatrix3x4dv, PFNGLPROGRAMUNIFORMMATRIX3X4DVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glProgramUniformMatrix4x3dv, PFNGLPROGRAMUNIFORMMATRIX4X3DVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glValidateProgramPipeline, PFNGLVALIDATEPROGRAMPIPELINEPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glGetProgramPipelineInfoLog, PFNGLGETPROGRAMPIPELINEINFOLOGPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glVertexAttribL1d, PFNGLVERTEXATTRIBL1DPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glVertexAttribL2d, PFNGLVERTEXATTRIBL2DPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glVertexAttribL3d, PFNGLVERTEXATTRIBL3DPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glVertexAttribL4d, PFNGLVERTEXATTRIBL4DPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glVertexAttribL1dv, PFNGLVERTEXATTRIBL1DVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glVertexAttribL2dv, PFNGLVERTEXATTRIBL2DVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glVertexAttribL3dv, PFNGLVERTEXATTRIBL3DVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glVertexAttribL4dv, PFNGLVERTEXATTRIBL4DVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glVertexAttribLPointer, PFNGLVERTEXATTRIBLPOINTERPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glGetVertexAttribLdv, PFNGLGETVERTEXATTRIBLDVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glViewportArrayv, PFNGLVIEWPORTARRAYVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glViewportIndexedf, PFNGLVIEWPORTINDEXEDFPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glViewportIndexedfv, PFNGLVIEWPORTINDEXEDFVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glScissorArrayv, PFNGLSCISSORARRAYVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glScissorIndexed, PFNGLSCISSORINDEXEDPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glScissorIndexedv, PFNGLSCISSORINDEXEDVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glDepthRangeArrayv, PFNGLDEPTHRANGEARRAYVPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glDepthRangeIndexed, PFNGLDEPTHRANGEINDEXEDPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glGetFloati_v, PFNGLGETFLOATI_VPROC)
  ENGINE_GL_VERSION_4_1_LOAD(glGetDoublei_v, PFNGLGETDOUBLEI_VPROC)
}

#include "engine/graphics/gl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GL_VERSION_1_1_LOAD(name, type)                                                                         \
  m_data_version_1_1.name =                                                                                            \
    reinterpret_cast<type>(m_interface_context->get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GL_VERSION_1_1_FUNCTION(name, type, ...)                                                                \
  using namespace std::literals;                                                                                       \
  if (m_data_version_1_1.name == nullptr) [[unlikely]] {                                                               \
    ENGINE_GL_VERSION_1_1_LOAD(name, type)                                                                             \
  }                                                                                                                    \
  return m_data_version_1_1.name(__VA_ARGS__);

auto
engine::graphics::gl::Context::drawArrays(GLenum mode, GLint first, GLsizei count) & noexcept -> void
{
  ENGINE_GL_VERSION_1_1_FUNCTION(glDrawArrays, PFNGLDRAWARRAYSPROC, mode, first, count)
}

auto
engine::graphics::gl::Context::drawElements(GLenum mode, GLsizei count, GLenum type, void const* indices) & noexcept
  -> void
{
  ENGINE_GL_VERSION_1_1_FUNCTION(glDrawElements, PFNGLDRAWELEMENTSPROC, mode, count, type, indices)
}

auto
engine::graphics::gl::Context::getPointerv(GLenum pname, void** params) & noexcept -> void
{
  ENGINE_GL_VERSION_1_1_FUNCTION(glGetPointerv, PFNGLGETPOINTERVPROC, pname, params)
}

auto
engine::graphics::gl::Context::polygonOffset(GLfloat factor, GLfloat units) & noexcept -> void
{
  ENGINE_GL_VERSION_1_1_FUNCTION(glPolygonOffset, PFNGLPOLYGONOFFSETPROC, factor, units)
}

auto
engine::graphics::gl::Context::copyTexImage1D(GLenum target,
					      GLint level,
					      GLenum internalformat,
					      GLint x,
					      GLint y,
					      GLsizei width,
					      GLint border) & noexcept -> void
{
  ENGINE_GL_VERSION_1_1_FUNCTION(
    glCopyTexImage1D, PFNGLCOPYTEXIMAGE1DPROC, target, level, internalformat, x, y, width, border)
}

auto
engine::graphics::gl::Context::copyTexImage2D(GLenum target,
					      GLint level,
					      GLenum internalformat,
					      GLint x,
					      GLint y,
					      GLsizei width,
					      GLsizei height,
					      GLint border) & noexcept -> void
{
  ENGINE_GL_VERSION_1_1_FUNCTION(
    glCopyTexImage2D, PFNGLCOPYTEXIMAGE2DPROC, target, level, internalformat, x, y, width, height, border)
}

auto
engine::graphics::gl::Context::copyTexSubImage1D(GLenum target,
						 GLint level,
						 GLint xoffset,
						 GLint x,
						 GLint y,
						 GLsizei width) & noexcept -> void
{
  ENGINE_GL_VERSION_1_1_FUNCTION(glCopyTexSubImage1D, PFNGLCOPYTEXSUBIMAGE1DPROC, target, level, xoffset, x, y, width)
}

auto
engine::graphics::gl::Context::copyTexSubImage2D(GLenum target,
						 GLint level,
						 GLint xoffset,
						 GLint yoffset,
						 GLint x,
						 GLint y,
						 GLsizei width,
						 GLsizei height) & noexcept -> void
{
  ENGINE_GL_VERSION_1_1_FUNCTION(
    glCopyTexSubImage2D, PFNGLCOPYTEXSUBIMAGE2DPROC, target, level, xoffset, yoffset, x, y, width, height)
}

auto
engine::graphics::gl::Context::texSubImage1D(GLenum target,
					     GLint level,
					     GLint xoffset,
					     GLsizei width,
					     GLenum format,
					     GLenum type,
					     void const* pixels) & noexcept -> void
{
  ENGINE_GL_VERSION_1_1_FUNCTION(
    glTexSubImage1D, PFNGLTEXSUBIMAGE1DPROC, target, level, xoffset, width, format, type, pixels)
}

auto
engine::graphics::gl::Context::texSubImage2D(GLenum target,
					     GLint level,
					     GLint xoffset,
					     GLint yoffset,
					     GLsizei width,
					     GLsizei height,
					     GLenum format,
					     GLenum type,
					     void const* pixels) & noexcept -> void
{
  ENGINE_GL_VERSION_1_1_FUNCTION(
    glTexSubImage2D, PFNGLTEXSUBIMAGE2DPROC, target, level, xoffset, yoffset, width, height, format, type, pixels)
}

auto
engine::graphics::gl::Context::bindTexture(GLenum target, GLuint texture) & noexcept -> void
{
  ENGINE_GL_VERSION_1_1_FUNCTION(glBindTexture, PFNGLBINDTEXTUREPROC, target, texture)
}

auto
engine::graphics::gl::Context::deleteTextures(GLsizei n, GLuint const* textures) & noexcept -> void
{
  ENGINE_GL_VERSION_1_1_FUNCTION(glDeleteTextures, PFNGLDELETETEXTURESPROC, n, textures)
}

auto
engine::graphics::gl::Context::genTextures(GLsizei n, GLuint* textures) & noexcept -> void
{
  ENGINE_GL_VERSION_1_1_FUNCTION(glGenTextures, PFNGLGENTEXTURESPROC, n, textures)
}

auto
engine::graphics::gl::Context::isTexture(GLuint texture) & noexcept -> GLboolean
{
  ENGINE_GL_VERSION_1_1_FUNCTION(glIsTexture, PFNGLISTEXTUREPROC, texture)
}

auto
engine::graphics::gl::Context::load_version_1_1() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GL_VERSION_1_1_LOAD(glDrawArrays, PFNGLDRAWARRAYSPROC)
  ENGINE_GL_VERSION_1_1_LOAD(glDrawElements, PFNGLDRAWELEMENTSPROC)
  ENGINE_GL_VERSION_1_1_LOAD(glGetPointerv, PFNGLGETPOINTERVPROC)
  ENGINE_GL_VERSION_1_1_LOAD(glPolygonOffset, PFNGLPOLYGONOFFSETPROC)
  ENGINE_GL_VERSION_1_1_LOAD(glCopyTexImage1D, PFNGLCOPYTEXIMAGE1DPROC)
  ENGINE_GL_VERSION_1_1_LOAD(glCopyTexImage2D, PFNGLCOPYTEXIMAGE2DPROC)
  ENGINE_GL_VERSION_1_1_LOAD(glCopyTexSubImage1D, PFNGLCOPYTEXSUBIMAGE1DPROC)
  ENGINE_GL_VERSION_1_1_LOAD(glCopyTexSubImage2D, PFNGLCOPYTEXSUBIMAGE2DPROC)
  ENGINE_GL_VERSION_1_1_LOAD(glTexSubImage1D, PFNGLTEXSUBIMAGE1DPROC)
  ENGINE_GL_VERSION_1_1_LOAD(glTexSubImage2D, PFNGLTEXSUBIMAGE2DPROC)
  ENGINE_GL_VERSION_1_1_LOAD(glBindTexture, PFNGLBINDTEXTUREPROC)
  ENGINE_GL_VERSION_1_1_LOAD(glDeleteTextures, PFNGLDELETETEXTURESPROC)
  ENGINE_GL_VERSION_1_1_LOAD(glGenTextures, PFNGLGENTEXTURESPROC)
  ENGINE_GL_VERSION_1_1_LOAD(glIsTexture, PFNGLISTEXTUREPROC)
}

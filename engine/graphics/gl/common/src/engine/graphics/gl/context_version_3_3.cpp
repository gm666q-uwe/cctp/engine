#include "engine/graphics/gl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GL_VERSION_3_3_LOAD(name, type)                                                                         \
  m_data_version_3_3.name =                                                                                            \
    reinterpret_cast<type>(m_interface_context->get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GL_VERSION_3_3_FUNCTION(name, type, ...)                                                                \
  using namespace std::literals;                                                                                       \
  if (m_data_version_3_3.name == nullptr) [[unlikely]] {                                                               \
    ENGINE_GL_VERSION_3_3_LOAD(name, type)                                                                             \
  }                                                                                                                    \
  return m_data_version_3_3.name(__VA_ARGS__);

auto
engine::graphics::gl::Context::bindFragDataLocationIndexed(GLuint program,
							   GLuint colorNumber,
							   GLuint index,
							   GLchar const* name) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(
    glBindFragDataLocationIndexed, PFNGLBINDFRAGDATALOCATIONINDEXEDPROC, program, colorNumber, index, name)
}

auto
engine::graphics::gl::Context::getFragDataIndex(GLuint program, GLchar const* name) & noexcept -> GLint
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glGetFragDataIndex, PFNGLGETFRAGDATAINDEXPROC, program, name)
}

auto
engine::graphics::gl::Context::genSamplers(GLsizei count, GLuint* samplers) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glGenSamplers, PFNGLGENSAMPLERSPROC, count, samplers)
}

auto
engine::graphics::gl::Context::deleteSamplers(GLsizei count, GLuint const* samplers) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glDeleteSamplers, PFNGLDELETESAMPLERSPROC, count, samplers)
}

auto
engine::graphics::gl::Context::isSampler(GLuint sampler) & noexcept -> GLboolean
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glIsSampler, PFNGLISSAMPLERPROC, sampler)
}

auto
engine::graphics::gl::Context::bindSampler(GLuint unit, GLuint sampler) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glBindSampler, PFNGLBINDSAMPLERPROC, unit, sampler)
}

auto
engine::graphics::gl::Context::samplerParameteri(GLuint sampler, GLenum pname, GLint param) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glSamplerParameteri, PFNGLSAMPLERPARAMETERIPROC, sampler, pname, param)
}

auto
engine::graphics::gl::Context::samplerParameteriv(GLuint sampler, GLenum pname, GLint const* param) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glSamplerParameteriv, PFNGLSAMPLERPARAMETERIVPROC, sampler, pname, param)
}

auto
engine::graphics::gl::Context::samplerParameterf(GLuint sampler, GLenum pname, GLfloat param) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glSamplerParameterf, PFNGLSAMPLERPARAMETERFPROC, sampler, pname, param)
}

auto
engine::graphics::gl::Context::samplerParameterfv(GLuint sampler, GLenum pname, GLfloat const* param) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glSamplerParameterfv, PFNGLSAMPLERPARAMETERFVPROC, sampler, pname, param)
}

auto
engine::graphics::gl::Context::samplerParameterIiv(GLuint sampler, GLenum pname, GLint const* param) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glSamplerParameterIiv, PFNGLSAMPLERPARAMETERIIVPROC, sampler, pname, param)
}

auto
engine::graphics::gl::Context::samplerParameterIuiv(GLuint sampler, GLenum pname, GLuint const* param) & noexcept
  -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glSamplerParameterIuiv, PFNGLSAMPLERPARAMETERIUIVPROC, sampler, pname, param)
}

auto
engine::graphics::gl::Context::getSamplerParameteriv(GLuint sampler, GLenum pname, GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glGetSamplerParameteriv, PFNGLGETSAMPLERPARAMETERIVPROC, sampler, pname, params)
}

auto
engine::graphics::gl::Context::getSamplerParameterIiv(GLuint sampler, GLenum pname, GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glGetSamplerParameterIiv, PFNGLGETSAMPLERPARAMETERIIVPROC, sampler, pname, params)
}

auto
engine::graphics::gl::Context::getSamplerParameterfv(GLuint sampler, GLenum pname, GLfloat* params) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glGetSamplerParameterfv, PFNGLGETSAMPLERPARAMETERFVPROC, sampler, pname, params)
}

auto
engine::graphics::gl::Context::getSamplerParameterIuiv(GLuint sampler, GLenum pname, GLuint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glGetSamplerParameterIuiv, PFNGLGETSAMPLERPARAMETERIUIVPROC, sampler, pname, params)
}

auto
engine::graphics::gl::Context::queryCounter(GLuint id, GLenum target) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glQueryCounter, PFNGLQUERYCOUNTERPROC, id, target)
}

auto
engine::graphics::gl::Context::getQueryObjecti64v(GLuint id, GLenum pname, GLint64* params) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glGetQueryObjecti64v, PFNGLGETQUERYOBJECTI64VPROC, id, pname, params)
}

auto
engine::graphics::gl::Context::getQueryObjectui64v(GLuint id, GLenum pname, GLuint64* params) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glGetQueryObjectui64v, PFNGLGETQUERYOBJECTUI64VPROC, id, pname, params)
}

auto
engine::graphics::gl::Context::vertexAttribDivisor(GLuint index, GLuint divisor) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glVertexAttribDivisor, PFNGLVERTEXATTRIBDIVISORPROC, index, divisor)
}

auto
engine::graphics::gl::Context::vertexAttribP1ui(GLuint index,
						GLenum type,
						GLboolean normalized,
						GLuint value) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glVertexAttribP1ui, PFNGLVERTEXATTRIBP1UIPROC, index, type, normalized, value)
}

auto
engine::graphics::gl::Context::vertexAttribP1uiv(GLuint index,
						 GLenum type,
						 GLboolean normalized,
						 GLuint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glVertexAttribP1uiv, PFNGLVERTEXATTRIBP1UIVPROC, index, type, normalized, value)
}

auto
engine::graphics::gl::Context::vertexAttribP2ui(GLuint index,
						GLenum type,
						GLboolean normalized,
						GLuint value) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glVertexAttribP2ui, PFNGLVERTEXATTRIBP2UIPROC, index, type, normalized, value)
}

auto
engine::graphics::gl::Context::vertexAttribP2uiv(GLuint index,
						 GLenum type,
						 GLboolean normalized,
						 GLuint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glVertexAttribP2uiv, PFNGLVERTEXATTRIBP2UIVPROC, index, type, normalized, value)
}

auto
engine::graphics::gl::Context::vertexAttribP3ui(GLuint index,
						GLenum type,
						GLboolean normalized,
						GLuint value) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glVertexAttribP3ui, PFNGLVERTEXATTRIBP3UIPROC, index, type, normalized, value)
}

auto
engine::graphics::gl::Context::vertexAttribP3uiv(GLuint index,
						 GLenum type,
						 GLboolean normalized,
						 GLuint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glVertexAttribP3uiv, PFNGLVERTEXATTRIBP3UIVPROC, index, type, normalized, value)
}

auto
engine::graphics::gl::Context::vertexAttribP4ui(GLuint index,
						GLenum type,
						GLboolean normalized,
						GLuint value) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glVertexAttribP4ui, PFNGLVERTEXATTRIBP4UIPROC, index, type, normalized, value)
}

auto
engine::graphics::gl::Context::vertexAttribP4uiv(GLuint index,
						 GLenum type,
						 GLboolean normalized,
						 GLuint const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_3_3_FUNCTION(glVertexAttribP4uiv, PFNGLVERTEXATTRIBP4UIVPROC, index, type, normalized, value)
}

auto
engine::graphics::gl::Context::load_version_3_3() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GL_VERSION_3_3_LOAD(glBindFragDataLocationIndexed, PFNGLBINDFRAGDATALOCATIONINDEXEDPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glGetFragDataIndex, PFNGLGETFRAGDATAINDEXPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glGenSamplers, PFNGLGENSAMPLERSPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glDeleteSamplers, PFNGLDELETESAMPLERSPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glIsSampler, PFNGLISSAMPLERPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glBindSampler, PFNGLBINDSAMPLERPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glSamplerParameteri, PFNGLSAMPLERPARAMETERIPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glSamplerParameteriv, PFNGLSAMPLERPARAMETERIVPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glSamplerParameterf, PFNGLSAMPLERPARAMETERFPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glSamplerParameterfv, PFNGLSAMPLERPARAMETERFVPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glSamplerParameterIiv, PFNGLSAMPLERPARAMETERIIVPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glSamplerParameterIuiv, PFNGLSAMPLERPARAMETERIUIVPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glGetSamplerParameteriv, PFNGLGETSAMPLERPARAMETERIVPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glGetSamplerParameterIiv, PFNGLGETSAMPLERPARAMETERIIVPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glGetSamplerParameterfv, PFNGLGETSAMPLERPARAMETERFVPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glGetSamplerParameterIuiv, PFNGLGETSAMPLERPARAMETERIUIVPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glQueryCounter, PFNGLQUERYCOUNTERPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glGetQueryObjecti64v, PFNGLGETQUERYOBJECTI64VPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glGetQueryObjectui64v, PFNGLGETQUERYOBJECTUI64VPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glVertexAttribDivisor, PFNGLVERTEXATTRIBDIVISORPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glVertexAttribP1ui, PFNGLVERTEXATTRIBP1UIPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glVertexAttribP1uiv, PFNGLVERTEXATTRIBP1UIVPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glVertexAttribP2ui, PFNGLVERTEXATTRIBP2UIPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glVertexAttribP2uiv, PFNGLVERTEXATTRIBP2UIVPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glVertexAttribP3ui, PFNGLVERTEXATTRIBP3UIPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glVertexAttribP3uiv, PFNGLVERTEXATTRIBP3UIVPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glVertexAttribP4ui, PFNGLVERTEXATTRIBP4UIPROC)
  ENGINE_GL_VERSION_3_3_LOAD(glVertexAttribP4uiv, PFNGLVERTEXATTRIBP4UIVPROC)
}

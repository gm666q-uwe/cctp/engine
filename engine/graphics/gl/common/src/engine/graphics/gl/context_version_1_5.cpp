#include "engine/graphics/gl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GL_VERSION_1_5_LOAD(name, type)                                                                         \
  m_data_version_1_5.name =                                                                                            \
    reinterpret_cast<type>(m_interface_context->get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GL_VERSION_1_5_FUNCTION(name, type, ...)                                                                \
  using namespace std::literals;                                                                                       \
  if (m_data_version_1_5.name == nullptr) [[unlikely]] {                                                               \
    ENGINE_GL_VERSION_1_5_LOAD(name, type)                                                                             \
  }                                                                                                                    \
  return m_data_version_1_5.name(__VA_ARGS__);

auto
engine::graphics::gl::Context::genQueries(GLsizei n, GLuint* ids) & noexcept -> void
{
  ENGINE_GL_VERSION_1_5_FUNCTION(glGenQueries, PFNGLGENQUERIESPROC, n, ids)
}

auto
engine::graphics::gl::Context::deleteQueries(GLsizei n, GLuint const* ids) & noexcept -> void
{
  ENGINE_GL_VERSION_1_5_FUNCTION(glDeleteQueries, PFNGLDELETEQUERIESPROC, n, ids)
}

auto
engine::graphics::gl::Context::isQuery(GLuint id) & noexcept -> GLboolean
{
  ENGINE_GL_VERSION_1_5_FUNCTION(glIsQuery, PFNGLISQUERYPROC, id)
}

auto
engine::graphics::gl::Context::beginQuery(GLenum target, GLuint id) & noexcept -> void
{
  ENGINE_GL_VERSION_1_5_FUNCTION(glBeginQuery, PFNGLBEGINQUERYPROC, target, id)
}

auto
engine::graphics::gl::Context::endQuery(GLenum target) & noexcept -> void
{
  ENGINE_GL_VERSION_1_5_FUNCTION(glEndQuery, PFNGLENDQUERYPROC, target)
}

auto
engine::graphics::gl::Context::getQueryiv(GLenum target, GLenum pname, GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_1_5_FUNCTION(glGetQueryiv, PFNGLGETQUERYIVPROC, target, pname, params)
}

auto
engine::graphics::gl::Context::getQueryObjectiv(GLuint id, GLenum pname, GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_1_5_FUNCTION(glGetQueryObjectiv, PFNGLGETQUERYOBJECTIVPROC, id, pname, params)
}

auto
engine::graphics::gl::Context::getQueryObjectuiv(GLuint id, GLenum pname, GLuint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_1_5_FUNCTION(glGetQueryObjectuiv, PFNGLGETQUERYOBJECTUIVPROC, id, pname, params)
}

auto
engine::graphics::gl::Context::bindBuffer(GLenum target, GLuint buffer) & noexcept -> void
{
  ENGINE_GL_VERSION_1_5_FUNCTION(glBindBuffer, PFNGLBINDBUFFERPROC, target, buffer)
}

auto
engine::graphics::gl::Context::deleteBuffers(GLsizei n, GLuint const* buffers) & noexcept -> void
{
  ENGINE_GL_VERSION_1_5_FUNCTION(glDeleteBuffers, PFNGLDELETEBUFFERSPROC, n, buffers)
}

auto
engine::graphics::gl::Context::genBuffers(GLsizei n, GLuint* buffers) & noexcept -> void
{
  ENGINE_GL_VERSION_1_5_FUNCTION(glGenBuffers, PFNGLGENBUFFERSPROC, n, buffers)
}

auto
engine::graphics::gl::Context::isBuffer(GLuint buffer) & noexcept -> GLboolean
{
  ENGINE_GL_VERSION_1_5_FUNCTION(glIsBuffer, PFNGLISBUFFERPROC, buffer)
}

auto
engine::graphics::gl::Context::bufferData(GLenum target, GLsizeiptr size, void const* data, GLenum usage) & noexcept
  -> void
{
  ENGINE_GL_VERSION_1_5_FUNCTION(glBufferData, PFNGLBUFFERDATAPROC, target, size, data, usage)
}

auto
engine::graphics::gl::Context::bufferSubData(GLenum target,
					     GLintptr offset,
					     GLsizeiptr size,
					     void const* data) & noexcept -> void
{
  ENGINE_GL_VERSION_1_5_FUNCTION(glBufferSubData, PFNGLBUFFERSUBDATAPROC, target, offset, size, data)
}

auto
engine::graphics::gl::Context::getBufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, void* data) & noexcept
  -> void
{
  ENGINE_GL_VERSION_1_5_FUNCTION(glGetBufferSubData, PFNGLGETBUFFERSUBDATAPROC, target, offset, size, data)
}

auto
engine::graphics::gl::Context::mapBuffer(GLenum target, GLenum access) & noexcept -> void*
{
  ENGINE_GL_VERSION_1_5_FUNCTION(glMapBuffer, PFNGLMAPBUFFERPROC, target, access)
}

auto
engine::graphics::gl::Context::unmapBuffer(GLenum target) & noexcept -> GLboolean
{
  ENGINE_GL_VERSION_1_5_FUNCTION(glUnmapBuffer, PFNGLUNMAPBUFFERPROC, target)
}

auto
engine::graphics::gl::Context::getBufferParameteriv(GLenum target, GLenum pname, GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_1_5_FUNCTION(glGetBufferParameteriv, PFNGLGETBUFFERPARAMETERIVPROC, target, pname, params)
}

auto
engine::graphics::gl::Context::getBufferPointerv(GLenum target, GLenum pname, void** params) & noexcept -> void
{
  ENGINE_GL_VERSION_1_5_FUNCTION(glGetBufferPointerv, PFNGLGETBUFFERPOINTERVPROC, target, pname, params)
}

auto
engine::graphics::gl::Context::load_version_1_5() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GL_VERSION_1_5_LOAD(glGenQueries, PFNGLGENQUERIESPROC)
  ENGINE_GL_VERSION_1_5_LOAD(glDeleteQueries, PFNGLDELETEQUERIESPROC)
  ENGINE_GL_VERSION_1_5_LOAD(glIsQuery, PFNGLISQUERYPROC)
  ENGINE_GL_VERSION_1_5_LOAD(glBeginQuery, PFNGLBEGINQUERYPROC)
  ENGINE_GL_VERSION_1_5_LOAD(glEndQuery, PFNGLENDQUERYPROC)
  ENGINE_GL_VERSION_1_5_LOAD(glGetQueryiv, PFNGLGETQUERYIVPROC)
  ENGINE_GL_VERSION_1_5_LOAD(glGetQueryObjectiv, PFNGLGETQUERYOBJECTIVPROC)
  ENGINE_GL_VERSION_1_5_LOAD(glGetQueryObjectuiv, PFNGLGETQUERYOBJECTUIVPROC)
  ENGINE_GL_VERSION_1_5_LOAD(glBindBuffer, PFNGLBINDBUFFERPROC)
  ENGINE_GL_VERSION_1_5_LOAD(glDeleteBuffers, PFNGLDELETEBUFFERSPROC)
  ENGINE_GL_VERSION_1_5_LOAD(glGenBuffers, PFNGLGENBUFFERSPROC)
  ENGINE_GL_VERSION_1_5_LOAD(glIsBuffer, PFNGLISBUFFERPROC)
  ENGINE_GL_VERSION_1_5_LOAD(glBufferData, PFNGLBUFFERDATAPROC)
  ENGINE_GL_VERSION_1_5_LOAD(glBufferSubData, PFNGLBUFFERSUBDATAPROC)
  ENGINE_GL_VERSION_1_5_LOAD(glGetBufferSubData, PFNGLGETBUFFERSUBDATAPROC)
  ENGINE_GL_VERSION_1_5_LOAD(glMapBuffer, PFNGLMAPBUFFERPROC)
  ENGINE_GL_VERSION_1_5_LOAD(glUnmapBuffer, PFNGLUNMAPBUFFERPROC)
  ENGINE_GL_VERSION_1_5_LOAD(glGetBufferParameteriv, PFNGLGETBUFFERPARAMETERIVPROC)
  ENGINE_GL_VERSION_1_5_LOAD(glGetBufferPointerv, PFNGLGETBUFFERPOINTERVPROC)
}

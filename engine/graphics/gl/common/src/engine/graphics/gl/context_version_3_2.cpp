#include "engine/graphics/gl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GL_VERSION_3_2_LOAD(name, type)                                                                         \
  m_data_version_3_2.name =                                                                                            \
    reinterpret_cast<type>(m_interface_context->get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GL_VERSION_3_2_FUNCTION(name, type, ...)                                                                \
  using namespace std::literals;                                                                                       \
  if (m_data_version_3_2.name == nullptr) [[unlikely]] {                                                               \
    ENGINE_GL_VERSION_3_2_LOAD(name, type)                                                                             \
  }                                                                                                                    \
  return m_data_version_3_2.name(__VA_ARGS__);

auto
engine::graphics::gl::Context::drawElementsBaseVertex(GLenum mode,
						      GLsizei count,
						      GLenum type,
						      void const* indices,
						      GLint basevertex) & noexcept -> void
{
  ENGINE_GL_VERSION_3_2_FUNCTION(
    glDrawElementsBaseVertex, PFNGLDRAWELEMENTSBASEVERTEXPROC, mode, count, type, indices, basevertex)
}

auto
engine::graphics::gl::Context::drawRangeElementsBaseVertex(GLenum mode,
							   GLuint start,
							   GLuint end,
							   GLsizei count,
							   GLenum type,
							   void const* indices,
							   GLint basevertex) & noexcept -> void
{
  ENGINE_GL_VERSION_3_2_FUNCTION(glDrawRangeElementsBaseVertex,
				 PFNGLDRAWRANGEELEMENTSBASEVERTEXPROC,
				 mode,
				 start,
				 end,
				 count,
				 type,
				 indices,
				 basevertex)
}

auto
engine::graphics::gl::Context::drawElementsInstancedBaseVertex(GLenum mode,
							       GLsizei count,
							       GLenum type,
							       void const* indices,
							       GLsizei instancecount,
							       GLint basevertex) & noexcept -> void
{
  ENGINE_GL_VERSION_3_2_FUNCTION(glDrawElementsInstancedBaseVertex,
				 PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXPROC,
				 mode,
				 count,
				 type,
				 indices,
				 instancecount,
				 basevertex)
}

auto
engine::graphics::gl::Context::multiDrawElementsBaseVertex(GLenum mode,
							   GLsizei const* count,
							   GLenum type,
							   void const* const* indices,
							   GLsizei drawcount,
							   GLint const* basevertex) & noexcept -> void
{
  ENGINE_GL_VERSION_3_2_FUNCTION(glMultiDrawElementsBaseVertex,
				 PFNGLMULTIDRAWELEMENTSBASEVERTEXPROC,
				 mode,
				 count,
				 type,
				 indices,
				 drawcount,
				 basevertex)
}

auto
engine::graphics::gl::Context::provokingVertex(GLenum mode) & noexcept -> void
{
  ENGINE_GL_VERSION_3_2_FUNCTION(glProvokingVertex, PFNGLPROVOKINGVERTEXPROC, mode)
}

auto
engine::graphics::gl::Context::fenceSync(GLenum condition, GLbitfield flags) & noexcept -> GLsync
{
  ENGINE_GL_VERSION_3_2_FUNCTION(glFenceSync, PFNGLFENCESYNCPROC, condition, flags)
}

auto
engine::graphics::gl::Context::isSync(GLsync sync) & noexcept -> GLboolean
{
  ENGINE_GL_VERSION_3_2_FUNCTION(glIsSync, PFNGLISSYNCPROC, sync)
}

auto
engine::graphics::gl::Context::deleteSync(GLsync sync) & noexcept -> void
{
  ENGINE_GL_VERSION_3_2_FUNCTION(glDeleteSync, PFNGLDELETESYNCPROC, sync)
}

auto
engine::graphics::gl::Context::clientWaitSync(GLsync sync, GLbitfield flags, GLuint64 timeout) & noexcept -> GLenum
{
  ENGINE_GL_VERSION_3_2_FUNCTION(glClientWaitSync, PFNGLCLIENTWAITSYNCPROC, sync, flags, timeout)
}

auto
engine::graphics::gl::Context::waitSync(GLsync sync, GLbitfield flags, GLuint64 timeout) & noexcept -> void
{
  ENGINE_GL_VERSION_3_2_FUNCTION(glWaitSync, PFNGLWAITSYNCPROC, sync, flags, timeout)
}

auto
engine::graphics::gl::Context::getInteger64v(GLenum pname, GLint64* data) & noexcept -> void
{
  ENGINE_GL_VERSION_3_2_FUNCTION(glGetInteger64v, PFNGLGETINTEGER64VPROC, pname, data)
}

auto
engine::graphics::gl::Context::getSynciv(GLsync sync,
					 GLenum pname,
					 GLsizei bufSize,
					 GLsizei* length,
					 GLint* values) & noexcept -> void
{
  ENGINE_GL_VERSION_3_2_FUNCTION(glGetSynciv, PFNGLGETSYNCIVPROC, sync, pname, bufSize, length, values)
}

auto
engine::graphics::gl::Context::getInteger64i_v(GLenum target, GLuint index, GLint64* data) & noexcept -> void
{
  ENGINE_GL_VERSION_3_2_FUNCTION(glGetInteger64i_v, PFNGLGETINTEGER64I_VPROC, target, index, data)
}

auto
engine::graphics::gl::Context::getBufferParameteri64v(GLenum target, GLenum pname, GLint64* params) & noexcept -> void
{
  ENGINE_GL_VERSION_3_2_FUNCTION(glGetBufferParameteri64v, PFNGLGETBUFFERPARAMETERI64VPROC, target, pname, params)
}

auto
engine::graphics::gl::Context::framebufferTexture(GLenum target,
						  GLenum attachment,
						  GLuint texture,
						  GLint level) & noexcept -> void
{
  ENGINE_GL_VERSION_3_2_FUNCTION(glFramebufferTexture, PFNGLFRAMEBUFFERTEXTUREPROC, target, attachment, texture, level)
}

auto
engine::graphics::gl::Context::texImage2DMultisample(GLenum target,
						     GLsizei samples,
						     GLenum internalformat,
						     GLsizei width,
						     GLsizei height,
						     GLboolean fixedsamplelocations) & noexcept -> void
{
  ENGINE_GL_VERSION_3_2_FUNCTION(glTexImage2DMultisample,
				 PFNGLTEXIMAGE2DMULTISAMPLEPROC,
				 target,
				 samples,
				 internalformat,
				 width,
				 height,
				 fixedsamplelocations)
}

auto
engine::graphics::gl::Context::texImage3DMultisample(GLenum target,
						     GLsizei samples,
						     GLenum internalformat,
						     GLsizei width,
						     GLsizei height,
						     GLsizei depth,
						     GLboolean fixedsamplelocations) & noexcept -> void
{
  ENGINE_GL_VERSION_3_2_FUNCTION(glTexImage3DMultisample,
				 PFNGLTEXIMAGE3DMULTISAMPLEPROC,
				 target,
				 samples,
				 internalformat,
				 width,
				 height,
				 depth,
				 fixedsamplelocations)
}

auto
engine::graphics::gl::Context::getMultisamplefv(GLenum pname, GLuint index, GLfloat* val) & noexcept -> void
{
  ENGINE_GL_VERSION_3_2_FUNCTION(glGetMultisamplefv, PFNGLGETMULTISAMPLEFVPROC, pname, index, val)
}

auto
engine::graphics::gl::Context::sampleMaski(GLuint maskNumber, GLbitfield mask) & noexcept -> void
{
  ENGINE_GL_VERSION_3_2_FUNCTION(glSampleMaski, PFNGLSAMPLEMASKIPROC, maskNumber, mask)
}

auto
engine::graphics::gl::Context::load_version_3_2() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GL_VERSION_3_2_LOAD(glDrawElementsBaseVertex, PFNGLDRAWELEMENTSBASEVERTEXPROC)
  ENGINE_GL_VERSION_3_2_LOAD(glDrawRangeElementsBaseVertex, PFNGLDRAWRANGEELEMENTSBASEVERTEXPROC)
  ENGINE_GL_VERSION_3_2_LOAD(glDrawElementsInstancedBaseVertex, PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXPROC)
  ENGINE_GL_VERSION_3_2_LOAD(glMultiDrawElementsBaseVertex, PFNGLMULTIDRAWELEMENTSBASEVERTEXPROC)
  ENGINE_GL_VERSION_3_2_LOAD(glProvokingVertex, PFNGLPROVOKINGVERTEXPROC)
  ENGINE_GL_VERSION_3_2_LOAD(glFenceSync, PFNGLFENCESYNCPROC)
  ENGINE_GL_VERSION_3_2_LOAD(glIsSync, PFNGLISSYNCPROC)
  ENGINE_GL_VERSION_3_2_LOAD(glDeleteSync, PFNGLDELETESYNCPROC)
  ENGINE_GL_VERSION_3_2_LOAD(glClientWaitSync, PFNGLCLIENTWAITSYNCPROC)
  ENGINE_GL_VERSION_3_2_LOAD(glWaitSync, PFNGLWAITSYNCPROC)
  ENGINE_GL_VERSION_3_2_LOAD(glGetInteger64v, PFNGLGETINTEGER64VPROC)
  ENGINE_GL_VERSION_3_2_LOAD(glGetSynciv, PFNGLGETSYNCIVPROC)
  ENGINE_GL_VERSION_3_2_LOAD(glGetInteger64i_v, PFNGLGETINTEGER64I_VPROC)
  ENGINE_GL_VERSION_3_2_LOAD(glGetBufferParameteri64v, PFNGLGETBUFFERPARAMETERI64VPROC)
  ENGINE_GL_VERSION_3_2_LOAD(glFramebufferTexture, PFNGLFRAMEBUFFERTEXTUREPROC)
  ENGINE_GL_VERSION_3_2_LOAD(glTexImage2DMultisample, PFNGLTEXIMAGE2DMULTISAMPLEPROC)
  ENGINE_GL_VERSION_3_2_LOAD(glTexImage3DMultisample, PFNGLTEXIMAGE3DMULTISAMPLEPROC)
  ENGINE_GL_VERSION_3_2_LOAD(glGetMultisamplefv, PFNGLGETMULTISAMPLEFVPROC)
  ENGINE_GL_VERSION_3_2_LOAD(glSampleMaski, PFNGLSAMPLEMASKIPROC)
}

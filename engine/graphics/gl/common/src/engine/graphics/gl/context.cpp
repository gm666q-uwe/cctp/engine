#include "engine/graphics/gl/context.hpp"

engine::graphics::gl::Context::Context(const engine::graphics::gl::InterfaceContext* interface_context,
				       bool pre_load) noexcept
  : m_interface_context(interface_context)
{
  if (pre_load) {
    load();
  }
}

auto
engine::graphics::gl::Context::load() & noexcept -> void
{
  load_version_1_0();
  load_version_1_1();
  load_version_1_2();
  load_version_1_3();
  load_version_1_4();
  load_version_1_5();
  load_version_2_0();
  load_version_2_1();
  load_version_3_0();
  load_version_3_1();
  load_version_3_2();
  load_version_3_3();
  load_version_4_0();
  load_version_4_1();
  load_version_4_2();
  load_version_4_3();
  load_version_4_4();
  load_version_4_5();
  load_version_4_6();
}

auto
engine::graphics::gl::Context::new_(engine::graphics::gl::InterfaceContext const* interface_context,
				    bool pre_load) noexcept -> engine::graphics::gl::Context
{
  return std::move(Context(interface_context, pre_load));
}

auto
engine::graphics::gl::Context::new_pointer(engine::graphics::gl::InterfaceContext const* interface_context,
					   bool pre_load) noexcept -> engine::graphics::gl::Context*
{
  return new Context(interface_context, pre_load);
}

#include "engine/graphics/gl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GL_VERSION_1_4_LOAD(name, type)                                                                         \
  m_data_version_1_4.name =                                                                                            \
    reinterpret_cast<type>(m_interface_context->get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GL_VERSION_1_4_FUNCTION(name, type, ...)                                                                \
  using namespace std::literals;                                                                                       \
  if (m_data_version_1_4.name == nullptr) [[unlikely]] {                                                               \
    ENGINE_GL_VERSION_1_4_LOAD(name, type)                                                                             \
  }                                                                                                                    \
  return m_data_version_1_4.name(__VA_ARGS__);

auto
engine::graphics::gl::Context::blendFuncSeparate(GLenum sfactorRGB,
						 GLenum dfactorRGB,
						 GLenum sfactorAlpha,
						 GLenum dfactorAlpha) & noexcept -> void
{
  ENGINE_GL_VERSION_1_4_FUNCTION(
    glBlendFuncSeparate, PFNGLBLENDFUNCSEPARATEPROC, sfactorRGB, dfactorRGB, sfactorAlpha, dfactorAlpha)
}

auto
engine::graphics::gl::Context::multiDrawArrays(GLenum mode,
					       GLint const* first,
					       GLsizei const* count,
					       GLsizei drawcount) & noexcept -> void
{
  ENGINE_GL_VERSION_1_4_FUNCTION(glMultiDrawArrays, PFNGLMULTIDRAWARRAYSPROC, mode, first, count, drawcount)
}

auto
engine::graphics::gl::Context::multiDrawElements(GLenum mode,
						 GLsizei const* count,
						 GLenum type,
						 void const* const* indices,
						 GLsizei drawcount) & noexcept -> void
{
  ENGINE_GL_VERSION_1_4_FUNCTION(glMultiDrawElements, PFNGLMULTIDRAWELEMENTSPROC, mode, count, type, indices, drawcount)
}

auto
engine::graphics::gl::Context::pointParameterf(GLenum pname, GLfloat param) & noexcept -> void
{
  ENGINE_GL_VERSION_1_4_FUNCTION(glPointParameterf, PFNGLPOINTPARAMETERFPROC, pname, param)
}

auto
engine::graphics::gl::Context::pointParameterfv(GLenum pname, GLfloat const* params) & noexcept -> void
{
  ENGINE_GL_VERSION_1_4_FUNCTION(glPointParameterfv, PFNGLPOINTPARAMETERFVPROC, pname, params)
}

auto
engine::graphics::gl::Context::pointParameteri(GLenum pname, GLint param) & noexcept -> void
{
  ENGINE_GL_VERSION_1_4_FUNCTION(glPointParameteri, PFNGLPOINTPARAMETERIPROC, pname, param)
}

auto
engine::graphics::gl::Context::pointParameteriv(GLenum pname, GLint const* params) & noexcept -> void
{
  ENGINE_GL_VERSION_1_4_FUNCTION(glPointParameteriv, PFNGLPOINTPARAMETERIVPROC, pname, params)
}

auto
engine::graphics::gl::Context::blendColor(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha) & noexcept -> void
{
  ENGINE_GL_VERSION_1_4_FUNCTION(glBlendColor, PFNGLBLENDCOLORPROC, red, green, blue, alpha)
}

auto
engine::graphics::gl::Context::blendEquation(GLenum mode) & noexcept -> void
{
  ENGINE_GL_VERSION_1_4_FUNCTION(glBlendEquation, PFNGLBLENDEQUATIONPROC, mode)
}

auto
engine::graphics::gl::Context::load_version_1_4() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GL_VERSION_1_4_LOAD(glBlendFuncSeparate, PFNGLBLENDFUNCSEPARATEPROC)
  ENGINE_GL_VERSION_1_4_LOAD(glMultiDrawArrays, PFNGLMULTIDRAWARRAYSPROC)
  ENGINE_GL_VERSION_1_4_LOAD(glMultiDrawElements, PFNGLMULTIDRAWELEMENTSPROC)
  ENGINE_GL_VERSION_1_4_LOAD(glPointParameterf, PFNGLPOINTPARAMETERFPROC)
  ENGINE_GL_VERSION_1_4_LOAD(glPointParameterfv, PFNGLPOINTPARAMETERFVPROC)
  ENGINE_GL_VERSION_1_4_LOAD(glPointParameteri, PFNGLPOINTPARAMETERIPROC)
  ENGINE_GL_VERSION_1_4_LOAD(glPointParameteriv, PFNGLPOINTPARAMETERIVPROC)
  ENGINE_GL_VERSION_1_4_LOAD(glBlendColor, PFNGLBLENDCOLORPROC)
  ENGINE_GL_VERSION_1_4_LOAD(glBlendEquation, PFNGLBLENDEQUATIONPROC)
}

#include "engine/graphics/gl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GL_VERSION_4_4_LOAD(name, type)                                                                         \
  m_data_version_4_4.name =                                                                                            \
    reinterpret_cast<type>(m_interface_context->get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GL_VERSION_4_4_FUNCTION(name, type, ...)                                                                \
  using namespace std::literals;                                                                                       \
  if (m_data_version_4_4.name == nullptr) [[unlikely]] {                                                               \
    ENGINE_GL_VERSION_4_4_LOAD(name, type)                                                                             \
  }                                                                                                                    \
  return m_data_version_4_4.name(__VA_ARGS__);

auto
engine::graphics::gl::Context::bufferStorage(GLenum target,
					     GLsizeiptr size,
					     void const* data,
					     GLbitfield flags) & noexcept -> void
{
  ENGINE_GL_VERSION_4_4_FUNCTION(glBufferStorage, PFNGLBUFFERSTORAGEPROC, target, size, data, flags)
}

auto
engine::graphics::gl::Context::clearTexImage(GLuint texture,
					     GLint level,
					     GLenum format,
					     GLenum type,
					     void const* data) & noexcept -> void
{
  ENGINE_GL_VERSION_4_4_FUNCTION(glClearTexImage, PFNGLCLEARTEXIMAGEPROC, texture, level, format, type, data)
}

auto
engine::graphics::gl::Context::clearTexSubImage(GLuint texture,
						GLint level,
						GLint xoffset,
						GLint yoffset,
						GLint zoffset,
						GLsizei width,
						GLsizei height,
						GLsizei depth,
						GLenum format,
						GLenum type,
						void const* data) & noexcept -> void
{
  ENGINE_GL_VERSION_4_4_FUNCTION(glClearTexSubImage,
				 PFNGLCLEARTEXSUBIMAGEPROC,
				 texture,
				 level,
				 xoffset,
				 yoffset,
				 zoffset,
				 width,
				 height,
				 depth,
				 format,
				 type,
				 data)
}

auto
engine::graphics::gl::Context::bindBuffersBase(GLenum target,
					       GLuint first,
					       GLsizei count,
					       GLuint const* buffers) & noexcept -> void
{
  ENGINE_GL_VERSION_4_4_FUNCTION(glBindBuffersBase, PFNGLBINDBUFFERSBASEPROC, target, first, count, buffers)
}

auto
engine::graphics::gl::Context::bindBuffersRange(GLenum target,
						GLuint first,
						GLsizei count,
						GLuint const* buffers,
						GLintptr const* offsets,
						GLsizeiptr const* sizes) & noexcept -> void
{
  ENGINE_GL_VERSION_4_4_FUNCTION(
    glBindBuffersRange, PFNGLBINDBUFFERSRANGEPROC, target, first, count, buffers, offsets, sizes)
}

auto
engine::graphics::gl::Context::bindTextures(GLuint first, GLsizei count, GLuint const* textures) & noexcept -> void
{
  ENGINE_GL_VERSION_4_4_FUNCTION(glBindTextures, PFNGLBINDTEXTURESPROC, first, count, textures)
}

auto
engine::graphics::gl::Context::bindSamplers(GLuint first, GLsizei count, GLuint const* samplers) & noexcept -> void
{
  ENGINE_GL_VERSION_4_4_FUNCTION(glBindSamplers, PFNGLBINDSAMPLERSPROC, first, count, samplers)
}

auto
engine::graphics::gl::Context::bindImageTextures(GLuint first, GLsizei count, GLuint const* textures) & noexcept -> void
{
  ENGINE_GL_VERSION_4_4_FUNCTION(glBindImageTextures, PFNGLBINDIMAGETEXTURESPROC, first, count, textures)
}

auto
engine::graphics::gl::Context::bindVertexBuffers(GLuint first,
						 GLsizei count,
						 GLuint const* buffers,
						 GLintptr const* offsets,
						 GLsizei const* strides) & noexcept -> void
{
  ENGINE_GL_VERSION_4_4_FUNCTION(
    glBindVertexBuffers, PFNGLBINDVERTEXBUFFERSPROC, first, count, buffers, offsets, strides)
}

auto
engine::graphics::gl::Context::load_version_4_4() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GL_VERSION_4_4_LOAD(glBufferStorage, PFNGLBUFFERSTORAGEPROC)
  ENGINE_GL_VERSION_4_4_LOAD(glClearTexImage, PFNGLCLEARTEXIMAGEPROC)
  ENGINE_GL_VERSION_4_4_LOAD(glClearTexSubImage, PFNGLCLEARTEXSUBIMAGEPROC)
  ENGINE_GL_VERSION_4_4_LOAD(glBindBuffersBase, PFNGLBINDBUFFERSBASEPROC)
  ENGINE_GL_VERSION_4_4_LOAD(glBindBuffersRange, PFNGLBINDBUFFERSRANGEPROC)
  ENGINE_GL_VERSION_4_4_LOAD(glBindTextures, PFNGLBINDTEXTURESPROC)
  ENGINE_GL_VERSION_4_4_LOAD(glBindSamplers, PFNGLBINDSAMPLERSPROC)
  ENGINE_GL_VERSION_4_4_LOAD(glBindImageTextures, PFNGLBINDIMAGETEXTURESPROC)
  ENGINE_GL_VERSION_4_4_LOAD(glBindVertexBuffers, PFNGLBINDVERTEXBUFFERSPROC)
}

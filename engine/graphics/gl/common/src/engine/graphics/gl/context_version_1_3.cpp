#include "engine/graphics/gl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GL_VERSION_1_3_LOAD(name, type)                                                                         \
  m_data_version_1_3.name =                                                                                            \
    reinterpret_cast<type>(m_interface_context->get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GL_VERSION_1_3_FUNCTION(name, type, ...)                                                                \
  using namespace std::literals;                                                                                       \
  if (m_data_version_1_3.name == nullptr) [[unlikely]] {                                                               \
    ENGINE_GL_VERSION_1_3_LOAD(name, type)                                                                             \
  }                                                                                                                    \
  return m_data_version_1_3.name(__VA_ARGS__);

auto
engine::graphics::gl::Context::activeTexture(GLenum texture) & noexcept -> void
{
  ENGINE_GL_VERSION_1_3_FUNCTION(glActiveTexture, PFNGLACTIVETEXTUREPROC, texture)
}

auto
engine::graphics::gl::Context::sampleCoverage(GLfloat value, GLboolean invert) & noexcept -> void
{
  ENGINE_GL_VERSION_1_3_FUNCTION(glSampleCoverage, PFNGLSAMPLECOVERAGEPROC, value, invert)
}

auto
engine::graphics::gl::Context::compressedTexImage3D(GLenum target,
						    GLint level,
						    GLenum internalformat,
						    GLsizei width,
						    GLsizei height,
						    GLsizei depth,
						    GLint border,
						    GLsizei imageSize,
						    void const* data) & noexcept -> void
{
  ENGINE_GL_VERSION_1_3_FUNCTION(glCompressedTexImage3D,
				 PFNGLCOMPRESSEDTEXIMAGE3DPROC,
				 target,
				 level,
				 internalformat,
				 width,
				 height,
				 depth,
				 border,
				 imageSize,
				 data)
}

auto
engine::graphics::gl::Context::compressedTexImage2D(GLenum target,
						    GLint level,
						    GLenum internalformat,
						    GLsizei width,
						    GLsizei height,
						    GLint border,
						    GLsizei imageSize,
						    void const* data) & noexcept -> void
{
  ENGINE_GL_VERSION_1_3_FUNCTION(glCompressedTexImage2D,
				 PFNGLCOMPRESSEDTEXIMAGE2DPROC,
				 target,
				 level,
				 internalformat,
				 width,
				 height,
				 border,
				 imageSize,
				 data)
}

auto
engine::graphics::gl::Context::compressedTexImage1D(GLenum target,
						    GLint level,
						    GLenum internalformat,
						    GLsizei width,
						    GLint border,
						    GLsizei imageSize,
						    void const* data) & noexcept -> void
{
  ENGINE_GL_VERSION_1_3_FUNCTION(glCompressedTexImage1D,
				 PFNGLCOMPRESSEDTEXIMAGE1DPROC,
				 target,
				 level,
				 internalformat,
				 width,
				 border,
				 imageSize,
				 data)
}

auto
engine::graphics::gl::Context::compressedTexSubImage3D(GLenum target,
						       GLint level,
						       GLint xoffset,
						       GLint yoffset,
						       GLint zoffset,
						       GLsizei width,
						       GLsizei height,
						       GLsizei depth,
						       GLenum format,
						       GLsizei imageSize,
						       void const* data) & noexcept -> void
{
  ENGINE_GL_VERSION_1_3_FUNCTION(glCompressedTexSubImage3D,
				 PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC,
				 target,
				 level,
				 xoffset,
				 yoffset,
				 zoffset,
				 width,
				 height,
				 depth,
				 format,
				 imageSize,
				 data)
}

auto
engine::graphics::gl::Context::compressedTexSubImage2D(GLenum target,
						       GLint level,
						       GLint xoffset,
						       GLint yoffset,
						       GLsizei width,
						       GLsizei height,
						       GLenum format,
						       GLsizei imageSize,
						       void const* data) & noexcept -> void
{
  ENGINE_GL_VERSION_1_3_FUNCTION(glCompressedTexSubImage2D,
				 PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC,
				 target,
				 level,
				 xoffset,
				 yoffset,
				 width,
				 height,
				 format,
				 imageSize,
				 data)
}

auto
engine::graphics::gl::Context::compressedTexSubImage1D(GLenum target,
						       GLint level,
						       GLint xoffset,
						       GLsizei width,
						       GLenum format,
						       GLsizei imageSize,
						       void const* data) & noexcept -> void
{
  ENGINE_GL_VERSION_1_3_FUNCTION(
    glCompressedTexSubImage1D, PFNGLCOMPRESSEDTEXSUBIMAGE1DPROC, target, level, xoffset, width, format, imageSize, data)
}

auto
engine::graphics::gl::Context::getCompressedTexImage(GLenum target, GLint level, void* img) & noexcept -> void
{
  ENGINE_GL_VERSION_1_3_FUNCTION(glGetCompressedTexImage, PFNGLGETCOMPRESSEDTEXIMAGEPROC, target, level, img)
}

auto
engine::graphics::gl::Context::load_version_1_3() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GL_VERSION_1_3_LOAD(glActiveTexture, PFNGLACTIVETEXTUREPROC)
  ENGINE_GL_VERSION_1_3_LOAD(glSampleCoverage, PFNGLSAMPLECOVERAGEPROC)
  ENGINE_GL_VERSION_1_3_LOAD(glCompressedTexImage3D, PFNGLCOMPRESSEDTEXIMAGE3DPROC)
  ENGINE_GL_VERSION_1_3_LOAD(glCompressedTexImage2D, PFNGLCOMPRESSEDTEXIMAGE2DPROC)
  ENGINE_GL_VERSION_1_3_LOAD(glCompressedTexImage1D, PFNGLCOMPRESSEDTEXIMAGE1DPROC)
  ENGINE_GL_VERSION_1_3_LOAD(glCompressedTexSubImage3D, PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC)
  ENGINE_GL_VERSION_1_3_LOAD(glCompressedTexSubImage2D, PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC)
  ENGINE_GL_VERSION_1_3_LOAD(glCompressedTexSubImage1D, PFNGLCOMPRESSEDTEXSUBIMAGE1DPROC)
  ENGINE_GL_VERSION_1_3_LOAD(glGetCompressedTexImage, PFNGLGETCOMPRESSEDTEXIMAGEPROC)
}

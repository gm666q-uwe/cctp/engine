#include "engine/graphics/gl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GL_VERSION_3_1_LOAD(name, type)                                                                         \
  m_data_version_3_1.name =                                                                                            \
    reinterpret_cast<type>(m_interface_context->get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GL_VERSION_3_1_FUNCTION(name, type, ...)                                                                \
  using namespace std::literals;                                                                                       \
  if (m_data_version_3_1.name == nullptr) [[unlikely]] {                                                               \
    ENGINE_GL_VERSION_3_1_LOAD(name, type)                                                                             \
  }                                                                                                                    \
  return m_data_version_3_1.name(__VA_ARGS__);

auto
engine::graphics::gl::Context::drawArraysInstanced(GLenum mode,
						   GLint first,
						   GLsizei count,
						   GLsizei instancecount) & noexcept -> void
{
  ENGINE_GL_VERSION_3_1_FUNCTION(glDrawArraysInstanced, PFNGLDRAWARRAYSINSTANCEDPROC, mode, first, count, instancecount)
}

auto
engine::graphics::gl::Context::drawElementsInstanced(GLenum mode,
						     GLsizei count,
						     GLenum type,
						     void const* indices,
						     GLsizei instancecount) & noexcept -> void
{
  ENGINE_GL_VERSION_3_1_FUNCTION(
    glDrawElementsInstanced, PFNGLDRAWELEMENTSINSTANCEDPROC, mode, count, type, indices, instancecount)
}

auto
engine::graphics::gl::Context::texBuffer(GLenum target, GLenum internalformat, GLuint buffer) & noexcept -> void
{
  ENGINE_GL_VERSION_3_1_FUNCTION(glTexBuffer, PFNGLTEXBUFFERPROC, target, internalformat, buffer)
}

auto
engine::graphics::gl::Context::primitiveRestartIndex(GLuint index) & noexcept -> void
{
  ENGINE_GL_VERSION_3_1_FUNCTION(glPrimitiveRestartIndex, PFNGLPRIMITIVERESTARTINDEXPROC, index)
}

auto
engine::graphics::gl::Context::copyBufferSubData(GLenum readTarget,
						 GLenum writeTarget,
						 GLintptr readOffset,
						 GLintptr writeOffset,
						 GLsizeiptr size) & noexcept -> void
{
  ENGINE_GL_VERSION_3_1_FUNCTION(
    glCopyBufferSubData, PFNGLCOPYBUFFERSUBDATAPROC, readTarget, writeTarget, readOffset, writeOffset, size)
}

auto
engine::graphics::gl::Context::getUniformIndices(GLuint program,
						 GLsizei uniformCount,
						 GLchar const* const* uniformNames,
						 GLuint* uniformIndices) & noexcept -> void
{
  ENGINE_GL_VERSION_3_1_FUNCTION(
    glGetUniformIndices, PFNGLGETUNIFORMINDICESPROC, program, uniformCount, uniformNames, uniformIndices)
}

auto
engine::graphics::gl::Context::getActiveUniformsiv(GLuint program,
						   GLsizei uniformCount,
						   GLuint const* uniformIndices,
						   GLenum pname,
						   GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_3_1_FUNCTION(
    glGetActiveUniformsiv, PFNGLGETACTIVEUNIFORMSIVPROC, program, uniformCount, uniformIndices, pname, params)
}

auto
engine::graphics::gl::Context::getActiveUniformName(GLuint program,
						    GLuint uniformIndex,
						    GLsizei bufSize,
						    GLsizei* length,
						    GLchar* uniformName) & noexcept -> void
{
  ENGINE_GL_VERSION_3_1_FUNCTION(
    glGetActiveUniformName, PFNGLGETACTIVEUNIFORMNAMEPROC, program, uniformIndex, bufSize, length, uniformName)
}

auto
engine::graphics::gl::Context::getUniformBlockIndex(GLuint program, GLchar const* uniformBlockName) & noexcept -> GLuint
{
  ENGINE_GL_VERSION_3_1_FUNCTION(glGetUniformBlockIndex, PFNGLGETUNIFORMBLOCKINDEXPROC, program, uniformBlockName)
}

auto
engine::graphics::gl::Context::getActiveUniformBlockiv(GLuint program,
						       GLuint uniformBlockIndex,
						       GLenum pname,
						       GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_3_1_FUNCTION(
    glGetActiveUniformBlockiv, PFNGLGETACTIVEUNIFORMBLOCKIVPROC, program, uniformBlockIndex, pname, params)
}

auto
engine::graphics::gl::Context::getActiveUniformBlockName(GLuint program,
							 GLuint uniformBlockIndex,
							 GLsizei bufSize,
							 GLsizei* length,
							 GLchar* uniformBlockName) & noexcept -> void
{
  ENGINE_GL_VERSION_3_1_FUNCTION(glGetActiveUniformBlockName,
				 PFNGLGETACTIVEUNIFORMBLOCKNAMEPROC,
				 program,
				 uniformBlockIndex,
				 bufSize,
				 length,
				 uniformBlockName)
}

auto
engine::graphics::gl::Context::uniformBlockBinding(GLuint program,
						   GLuint uniformBlockIndex,
						   GLuint uniformBlockBinding) & noexcept -> void
{
  ENGINE_GL_VERSION_3_1_FUNCTION(
    glUniformBlockBinding, PFNGLUNIFORMBLOCKBINDINGPROC, program, uniformBlockIndex, uniformBlockBinding)
}

auto
engine::graphics::gl::Context::load_version_3_1() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GL_VERSION_3_1_LOAD(glDrawArraysInstanced, PFNGLDRAWARRAYSINSTANCEDPROC)
  ENGINE_GL_VERSION_3_1_LOAD(glDrawElementsInstanced, PFNGLDRAWELEMENTSINSTANCEDPROC)
  ENGINE_GL_VERSION_3_1_LOAD(glTexBuffer, PFNGLTEXBUFFERPROC)
  ENGINE_GL_VERSION_3_1_LOAD(glPrimitiveRestartIndex, PFNGLPRIMITIVERESTARTINDEXPROC)
  ENGINE_GL_VERSION_3_1_LOAD(glCopyBufferSubData, PFNGLCOPYBUFFERSUBDATAPROC)
  ENGINE_GL_VERSION_3_1_LOAD(glGetUniformIndices, PFNGLGETUNIFORMINDICESPROC)
  ENGINE_GL_VERSION_3_1_LOAD(glGetActiveUniformsiv, PFNGLGETACTIVEUNIFORMSIVPROC)
  ENGINE_GL_VERSION_3_1_LOAD(glGetActiveUniformName, PFNGLGETACTIVEUNIFORMNAMEPROC)
  ENGINE_GL_VERSION_3_1_LOAD(glGetUniformBlockIndex, PFNGLGETUNIFORMBLOCKINDEXPROC)
  ENGINE_GL_VERSION_3_1_LOAD(glGetActiveUniformBlockiv, PFNGLGETACTIVEUNIFORMBLOCKIVPROC)
  ENGINE_GL_VERSION_3_1_LOAD(glGetActiveUniformBlockName, PFNGLGETACTIVEUNIFORMBLOCKNAMEPROC)
  ENGINE_GL_VERSION_3_1_LOAD(glUniformBlockBinding, PFNGLUNIFORMBLOCKBINDINGPROC)
}

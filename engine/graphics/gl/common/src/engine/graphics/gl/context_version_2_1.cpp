#include "engine/graphics/gl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GL_VERSION_2_1_LOAD(name, type)                                                                         \
  m_data_version_2_1.name =                                                                                            \
    reinterpret_cast<type>(m_interface_context->get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GL_VERSION_2_1_FUNCTION(name, type, ...)                                                                \
  using namespace std::literals;                                                                                       \
  if (m_data_version_2_1.name == nullptr) [[unlikely]] {                                                               \
    ENGINE_GL_VERSION_2_1_LOAD(name, type)                                                                             \
  }                                                                                                                    \
  return m_data_version_2_1.name(__VA_ARGS__);

auto
engine::graphics::gl::Context::uniformMatrix2x3fv(GLint location,
						  GLsizei count,
						  GLboolean transpose,
						  GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_2_1_FUNCTION(glUniformMatrix2x3fv, PFNGLUNIFORMMATRIX2X3FVPROC, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::uniformMatrix3x2fv(GLint location,
						  GLsizei count,
						  GLboolean transpose,
						  GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_2_1_FUNCTION(glUniformMatrix3x2fv, PFNGLUNIFORMMATRIX3X2FVPROC, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::uniformMatrix2x4fv(GLint location,
						  GLsizei count,
						  GLboolean transpose,
						  GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_2_1_FUNCTION(glUniformMatrix2x4fv, PFNGLUNIFORMMATRIX2X4FVPROC, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::uniformMatrix4x2fv(GLint location,
						  GLsizei count,
						  GLboolean transpose,
						  GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_2_1_FUNCTION(glUniformMatrix4x2fv, PFNGLUNIFORMMATRIX4X2FVPROC, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::uniformMatrix3x4fv(GLint location,
						  GLsizei count,
						  GLboolean transpose,
						  GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_2_1_FUNCTION(glUniformMatrix3x4fv, PFNGLUNIFORMMATRIX3X4FVPROC, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::uniformMatrix4x3fv(GLint location,
						  GLsizei count,
						  GLboolean transpose,
						  GLfloat const* value) & noexcept -> void
{
  ENGINE_GL_VERSION_2_1_FUNCTION(glUniformMatrix4x3fv, PFNGLUNIFORMMATRIX4X3FVPROC, location, count, transpose, value)
}

auto
engine::graphics::gl::Context::load_version_2_1() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GL_VERSION_2_1_LOAD(glUniformMatrix2x3fv, PFNGLUNIFORMMATRIX2X3FVPROC)
  ENGINE_GL_VERSION_2_1_LOAD(glUniformMatrix3x2fv, PFNGLUNIFORMMATRIX3X2FVPROC)
  ENGINE_GL_VERSION_2_1_LOAD(glUniformMatrix2x4fv, PFNGLUNIFORMMATRIX2X4FVPROC)
  ENGINE_GL_VERSION_2_1_LOAD(glUniformMatrix4x2fv, PFNGLUNIFORMMATRIX4X2FVPROC)
  ENGINE_GL_VERSION_2_1_LOAD(glUniformMatrix3x4fv, PFNGLUNIFORMMATRIX3X4FVPROC)
  ENGINE_GL_VERSION_2_1_LOAD(glUniformMatrix4x3fv, PFNGLUNIFORMMATRIX4X3FVPROC)
}

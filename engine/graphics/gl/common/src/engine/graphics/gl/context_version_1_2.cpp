#include "engine/graphics/gl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GL_VERSION_1_2_LOAD(name, type)                                                                         \
  m_data_version_1_2.name =                                                                                            \
    reinterpret_cast<type>(m_interface_context->get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GL_VERSION_1_2_FUNCTION(name, type, ...)                                                                \
  using namespace std::literals;                                                                                       \
  if (m_data_version_1_2.name == nullptr) [[unlikely]] {                                                               \
    ENGINE_GL_VERSION_1_2_LOAD(name, type)                                                                             \
  }                                                                                                                    \
  return m_data_version_1_2.name(__VA_ARGS__);

auto
engine::graphics::gl::Context::drawRangeElements(GLenum mode,
						 GLuint start,
						 GLuint end,
						 GLsizei count,
						 GLenum type,
						 void const* indices) & noexcept -> void
{
  ENGINE_GL_VERSION_1_2_FUNCTION(
    glDrawRangeElements, PFNGLDRAWRANGEELEMENTSPROC, mode, start, end, count, type, indices)
}

auto
engine::graphics::gl::Context::texImage3D(GLenum target,
					  GLint level,
					  GLint internalformat,
					  GLsizei width,
					  GLsizei height,
					  GLsizei depth,
					  GLint border,
					  GLenum format,
					  GLenum type,
					  void const* pixels) & noexcept -> void
{
  ENGINE_GL_VERSION_1_2_FUNCTION(glTexImage3D,
				 PFNGLTEXIMAGE3DPROC,
				 target,
				 level,
				 internalformat,
				 width,
				 height,
				 depth,
				 border,
				 format,
				 type,
				 pixels)
}

auto
engine::graphics::gl::Context::texSubImage3D(GLenum target,
					     GLint level,
					     GLint xoffset,
					     GLint yoffset,
					     GLint zoffset,
					     GLsizei width,
					     GLsizei height,
					     GLsizei depth,
					     GLenum format,
					     GLenum type,
					     void const* pixels) & noexcept -> void
{
  ENGINE_GL_VERSION_1_2_FUNCTION(glTexSubImage3D,
				 PFNGLTEXSUBIMAGE3DPROC,
				 target,
				 level,
				 xoffset,
				 yoffset,
				 zoffset,
				 width,
				 height,
				 depth,
				 format,
				 type,
				 pixels)
}

auto
engine::graphics::gl::Context::copyTexSubImage3D(GLenum target,
						 GLint level,
						 GLint xoffset,
						 GLint yoffset,
						 GLint zoffset,
						 GLint x,
						 GLint y,
						 GLsizei width,
						 GLsizei height) & noexcept -> void
{
  ENGINE_GL_VERSION_1_2_FUNCTION(
    glCopyTexSubImage3D, PFNGLCOPYTEXSUBIMAGE3DPROC, target, level, xoffset, yoffset, zoffset, x, y, width, height)
}

auto
engine::graphics::gl::Context::load_version_1_2() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GL_VERSION_1_2_LOAD(glDrawRangeElements, PFNGLDRAWRANGEELEMENTSPROC)
  ENGINE_GL_VERSION_1_2_LOAD(glTexImage3D, PFNGLTEXIMAGE3DPROC)
  ENGINE_GL_VERSION_1_2_LOAD(glTexSubImage3D, PFNGLTEXSUBIMAGE3DPROC)
  ENGINE_GL_VERSION_1_2_LOAD(glCopyTexSubImage3D, PFNGLCOPYTEXSUBIMAGE3DPROC)
}

#include "engine/graphics/gl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GL_VERSION_1_0_LOAD(name, type)                                                                         \
  m_data_version_1_0.name =                                                                                            \
    reinterpret_cast<type>(m_interface_context->get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GL_VERSION_1_0_FUNCTION(name, type, ...)                                                                \
  using namespace std::literals;                                                                                       \
  if (m_data_version_1_0.name == nullptr) [[unlikely]] {                                                               \
    ENGINE_GL_VERSION_1_0_LOAD(name, type)                                                                             \
  }                                                                                                                    \
  return m_data_version_1_0.name(__VA_ARGS__);

auto
engine::graphics::gl::Context::cullFace(GLenum mode) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glCullFace, PFNGLCULLFACEPROC, mode)
}

auto
engine::graphics::gl::Context::frontFace(GLenum mode) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glFrontFace, PFNGLFRONTFACEPROC, mode)
}

auto
engine::graphics::gl::Context::hint(GLenum target, GLenum mode) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glHint, PFNGLHINTPROC, target, mode)
}

auto
engine::graphics::gl::Context::lineWidth(GLfloat width) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glLineWidth, PFNGLLINEWIDTHPROC, width)
}

auto
engine::graphics::gl::Context::pointSize(GLfloat size) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glPointSize, PFNGLPOINTSIZEPROC, size)
}

auto
engine::graphics::gl::Context::polygonMode(GLenum face, GLenum mode) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glPolygonMode, PFNGLPOLYGONMODEPROC, face, mode)
}

auto
engine::graphics::gl::Context::scissor(GLint x, GLint y, GLsizei width, GLsizei height) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glScissor, PFNGLSCISSORPROC, x, y, width, height)
}

auto
engine::graphics::gl::Context::texParameterf(GLenum target, GLenum pname, GLfloat param) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glTexParameterf, PFNGLTEXPARAMETERFPROC, target, pname, param)
}

auto
engine::graphics::gl::Context::texParameterfv(GLenum target, GLenum pname, GLfloat const* params) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glTexParameterfv, PFNGLTEXPARAMETERFVPROC, target, pname, params)
}

auto
engine::graphics::gl::Context::texParameteri(GLenum target, GLenum pname, GLint param) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glTexParameteri, PFNGLTEXPARAMETERIPROC, target, pname, param)
}

auto
engine::graphics::gl::Context::texParameteriv(GLenum target, GLenum pname, GLint const* params) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glTexParameteriv, PFNGLTEXPARAMETERIVPROC, target, pname, params)
}

auto
engine::graphics::gl::Context::texImage1D(GLenum target,
					  GLint level,
					  GLint internalformat,
					  GLsizei width,
					  GLint border,
					  GLenum format,
					  GLenum type,
					  void const* pixels) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(
    glTexImage1D, PFNGLTEXIMAGE1DPROC, target, level, internalformat, width, border, format, type, pixels)
}

auto
engine::graphics::gl::Context::texImage2D(GLenum target,
					  GLint level,
					  GLint internalformat,
					  GLsizei width,
					  GLsizei height,
					  GLint border,
					  GLenum format,
					  GLenum type,
					  void const* pixels) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(
    glTexImage2D, PFNGLTEXIMAGE2DPROC, target, level, internalformat, width, height, border, format, type, pixels)
}

auto
engine::graphics::gl::Context::drawBuffer(GLenum buf) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glDrawBuffer, PFNGLDRAWBUFFERPROC, buf)
}

auto
engine::graphics::gl::Context::clear(GLbitfield mask) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glClear, PFNGLCLEARPROC, mask)
}

auto
engine::graphics::gl::Context::clearColor(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glClearColor, PFNGLCLEARCOLORPROC, red, green, blue, alpha)
}

auto
engine::graphics::gl::Context::clearStencil(GLint s) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glClearStencil, PFNGLCLEARSTENCILPROC, s)
}

auto
engine::graphics::gl::Context::clearDepth(GLdouble depth) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glClearDepth, PFNGLCLEARDEPTHPROC, depth)
}

auto
engine::graphics::gl::Context::stencilMask(GLuint mask) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glStencilMask, PFNGLSTENCILMASKPROC, mask)
}

auto
engine::graphics::gl::Context::colorMask(GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha) & noexcept
  -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glColorMask, PFNGLCOLORMASKPROC, red, green, blue, alpha)
}

auto
engine::graphics::gl::Context::depthMask(GLboolean flag) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glDepthMask, PFNGLDEPTHMASKPROC, flag)
}

auto
engine::graphics::gl::Context::disable(GLenum cap) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glDisable, PFNGLDISABLEPROC, cap)
}

auto
engine::graphics::gl::Context::enable(GLenum cap) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glEnable, PFNGLENABLEPROC, cap)
}

auto
engine::graphics::gl::Context::finish() & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glFinish, PFNGLFINISHPROC)
}

auto
engine::graphics::gl::Context::flush() & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glFlush, PFNGLFLUSHPROC)
}

auto
engine::graphics::gl::Context::blendFunc(GLenum sfactor, GLenum dfactor) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glBlendFunc, PFNGLBLENDFUNCPROC, sfactor, dfactor)
}

auto
engine::graphics::gl::Context::logicOp(GLenum opcode) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glLogicOp, PFNGLLOGICOPPROC, opcode)
}

auto
engine::graphics::gl::Context::stencilFunc(GLenum func, GLint ref, GLuint mask) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glStencilFunc, PFNGLSTENCILFUNCPROC, func, ref, mask)
}

auto
engine::graphics::gl::Context::stencilOp(GLenum fail, GLenum zfail, GLenum zpass) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glStencilOp, PFNGLSTENCILOPPROC, fail, zfail, zpass)
}

auto
engine::graphics::gl::Context::depthFunc(GLenum func) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glDepthFunc, PFNGLDEPTHFUNCPROC, func)
}

auto
engine::graphics::gl::Context::pixelStoref(GLenum pname, GLfloat param) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glPixelStoref, PFNGLPIXELSTOREFPROC, pname, param)
}

auto
engine::graphics::gl::Context::pixelStorei(GLenum pname, GLint param) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glPixelStorei, PFNGLPIXELSTOREIPROC, pname, param)
}

auto
engine::graphics::gl::Context::readBuffer(GLenum src) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glReadBuffer, PFNGLREADBUFFERPROC, src)
}

auto
engine::graphics::gl::Context::readPixels(GLint x,
					  GLint y,
					  GLsizei width,
					  GLsizei height,
					  GLenum format,
					  GLenum type,
					  void* pixels) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glReadPixels, PFNGLREADPIXELSPROC, x, y, width, height, format, type, pixels)
}

auto
engine::graphics::gl::Context::getBooleanv(GLenum pname, GLboolean* data) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glGetBooleanv, PFNGLGETBOOLEANVPROC, pname, data)
}

auto
engine::graphics::gl::Context::getDoublev(GLenum pname, GLdouble* data) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glGetDoublev, PFNGLGETDOUBLEVPROC, pname, data)
}

auto
engine::graphics::gl::Context::getError() & noexcept -> GLenum
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glGetError, PFNGLGETERRORPROC)
}

auto
engine::graphics::gl::Context::getFloatv(GLenum pname, GLfloat* data) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glGetFloatv, PFNGLGETFLOATVPROC, pname, data)
}

auto
engine::graphics::gl::Context::getIntegerv(GLenum pname, GLint* data) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glGetIntegerv, PFNGLGETINTEGERVPROC, pname, data)
}

auto
engine::graphics::gl::Context::getString(GLenum name) & noexcept -> GLubyte const*
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glGetString, PFNGLGETSTRINGPROC, name)
}

auto
engine::graphics::gl::Context::getTexImage(GLenum target,
					   GLint level,
					   GLenum format,
					   GLenum type,
					   void* pixels) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glGetTexImage, PFNGLGETTEXIMAGEPROC, target, level, format, type, pixels)
}

auto
engine::graphics::gl::Context::getTexParameterfv(GLenum target, GLenum pname, GLfloat* params) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glGetTexParameterfv, PFNGLGETTEXPARAMETERFVPROC, target, pname, params)
}

auto
engine::graphics::gl::Context::getTexParameteriv(GLenum target, GLenum pname, GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glGetTexParameteriv, PFNGLGETTEXPARAMETERIVPROC, target, pname, params)
}

auto
engine::graphics::gl::Context::getTexLevelParameterfv(GLenum target,
						      GLint level,
						      GLenum pname,
						      GLfloat* params) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(
    glGetTexLevelParameterfv, PFNGLGETTEXLEVELPARAMETERFVPROC, target, level, pname, params)
}

auto
engine::graphics::gl::Context::getTexLevelParameteriv(GLenum target,
						      GLint level,
						      GLenum pname,
						      GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(
    glGetTexLevelParameteriv, PFNGLGETTEXLEVELPARAMETERIVPROC, target, level, pname, params)
}

auto
engine::graphics::gl::Context::isEnabled(GLenum cap) & noexcept -> GLboolean
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glIsEnabled, PFNGLISENABLEDPROC, cap)
}

auto
engine::graphics::gl::Context::depthRange(GLdouble n, GLdouble f) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glDepthRange, PFNGLDEPTHRANGEPROC, n, f)
}

auto
engine::graphics::gl::Context::viewport(GLint x, GLint y, GLsizei width, GLsizei height) & noexcept -> void
{
  ENGINE_GL_VERSION_1_0_FUNCTION(glViewport, PFNGLVIEWPORTPROC, x, y, width, height)
}

auto
engine::graphics::gl::Context::load_version_1_0() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GL_VERSION_1_0_LOAD(glCullFace, PFNGLCULLFACEPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glFrontFace, PFNGLFRONTFACEPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glHint, PFNGLHINTPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glLineWidth, PFNGLLINEWIDTHPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glPointSize, PFNGLPOINTSIZEPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glPolygonMode, PFNGLPOLYGONMODEPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glScissor, PFNGLSCISSORPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glTexParameterf, PFNGLTEXPARAMETERFPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glTexParameterfv, PFNGLTEXPARAMETERFVPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glTexParameteri, PFNGLTEXPARAMETERIPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glTexParameteriv, PFNGLTEXPARAMETERIVPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glTexImage1D, PFNGLTEXIMAGE1DPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glTexImage2D, PFNGLTEXIMAGE2DPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glDrawBuffer, PFNGLDRAWBUFFERPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glClear, PFNGLCLEARPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glClearColor, PFNGLCLEARCOLORPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glClearStencil, PFNGLCLEARSTENCILPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glClearDepth, PFNGLCLEARDEPTHPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glStencilMask, PFNGLSTENCILMASKPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glColorMask, PFNGLCOLORMASKPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glDepthMask, PFNGLDEPTHMASKPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glDisable, PFNGLDISABLEPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glEnable, PFNGLENABLEPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glFinish, PFNGLFINISHPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glFlush, PFNGLFLUSHPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glBlendFunc, PFNGLBLENDFUNCPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glLogicOp, PFNGLLOGICOPPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glStencilFunc, PFNGLSTENCILFUNCPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glStencilOp, PFNGLSTENCILOPPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glDepthFunc, PFNGLDEPTHFUNCPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glPixelStoref, PFNGLPIXELSTOREFPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glPixelStorei, PFNGLPIXELSTOREIPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glReadBuffer, PFNGLREADBUFFERPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glReadPixels, PFNGLREADPIXELSPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glGetBooleanv, PFNGLGETBOOLEANVPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glGetDoublev, PFNGLGETDOUBLEVPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glGetError, PFNGLGETERRORPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glGetFloatv, PFNGLGETFLOATVPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glGetIntegerv, PFNGLGETINTEGERVPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glGetString, PFNGLGETSTRINGPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glGetTexImage, PFNGLGETTEXIMAGEPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glGetTexParameterfv, PFNGLGETTEXPARAMETERFVPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glGetTexParameteriv, PFNGLGETTEXPARAMETERIVPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glGetTexLevelParameterfv, PFNGLGETTEXLEVELPARAMETERFVPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glGetTexLevelParameteriv, PFNGLGETTEXLEVELPARAMETERIVPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glIsEnabled, PFNGLISENABLEDPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glDepthRange, PFNGLDEPTHRANGEPROC)
  ENGINE_GL_VERSION_1_0_LOAD(glViewport, PFNGLVIEWPORTPROC)
}

#include "engine/graphics/gl/context.hpp"

#include <boost/preprocessor.hpp>

#define ENGINE_GL_VERSION_4_2_LOAD(name, type)                                                                         \
  m_data_version_4_2.name =                                                                                            \
    reinterpret_cast<type>(m_interface_context->get_proc_address(BOOST_PP_CAT(#name, sv)).unwrap_or(nullptr));

#define ENGINE_GL_VERSION_4_2_FUNCTION(name, type, ...)                                                                \
  using namespace std::literals;                                                                                       \
  if (m_data_version_4_2.name == nullptr) [[unlikely]] {                                                               \
    ENGINE_GL_VERSION_4_2_LOAD(name, type)                                                                             \
  }                                                                                                                    \
  return m_data_version_4_2.name(__VA_ARGS__);

auto
engine::graphics::gl::Context::drawArraysInstancedBaseInstance(GLenum mode,
							       GLint first,
							       GLsizei count,
							       GLsizei instancecount,
							       GLuint baseinstance) & noexcept -> void
{
  ENGINE_GL_VERSION_4_2_FUNCTION(glDrawArraysInstancedBaseInstance,
				 PFNGLDRAWARRAYSINSTANCEDBASEINSTANCEPROC,
				 mode,
				 first,
				 count,
				 instancecount,
				 baseinstance)
}

auto
engine::graphics::gl::Context::drawElementsInstancedBaseInstance(GLenum mode,
								 GLsizei count,
								 GLenum type,
								 void const* indices,
								 GLsizei instancecount,
								 GLuint baseinstance) & noexcept -> void
{
  ENGINE_GL_VERSION_4_2_FUNCTION(glDrawElementsInstancedBaseInstance,
				 PFNGLDRAWELEMENTSINSTANCEDBASEINSTANCEPROC,
				 mode,
				 count,
				 type,
				 indices,
				 instancecount,
				 baseinstance)
}

auto
engine::graphics::gl::Context::drawElementsInstancedBaseVertexBaseInstance(GLenum mode,
									   GLsizei count,
									   GLenum type,
									   void const* indices,
									   GLsizei instancecount,
									   GLint basevertex,
									   GLuint baseinstance) & noexcept -> void
{
  ENGINE_GL_VERSION_4_2_FUNCTION(glDrawElementsInstancedBaseVertexBaseInstance,
				 PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXBASEINSTANCEPROC,
				 mode,
				 count,
				 type,
				 indices,
				 instancecount,
				 basevertex,
				 baseinstance)
}

auto
engine::graphics::gl::Context::getInternalformativ(GLenum target,
						   GLenum internalformat,
						   GLenum pname,
						   GLsizei bufSize,
						   GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_2_FUNCTION(
    glGetInternalformativ, PFNGLGETINTERNALFORMATIVPROC, target, internalformat, pname, bufSize, params)
}

auto
engine::graphics::gl::Context::getActiveAtomicCounterBufferiv(GLuint program,
							      GLuint bufferIndex,
							      GLenum pname,
							      GLint* params) & noexcept -> void
{
  ENGINE_GL_VERSION_4_2_FUNCTION(
    glGetActiveAtomicCounterBufferiv, PFNGLGETACTIVEATOMICCOUNTERBUFFERIVPROC, program, bufferIndex, pname, params)
}

auto
engine::graphics::gl::Context::bindImageTexture(GLuint unit,
						GLuint texture,
						GLint level,
						GLboolean layered,
						GLint layer,
						GLenum access,
						GLenum format) & noexcept -> void
{
  ENGINE_GL_VERSION_4_2_FUNCTION(
    glBindImageTexture, PFNGLBINDIMAGETEXTUREPROC, unit, texture, level, layered, layer, access, format)
}

auto
engine::graphics::gl::Context::memoryBarrier(GLbitfield barriers) & noexcept -> void
{
  ENGINE_GL_VERSION_4_2_FUNCTION(glMemoryBarrier, PFNGLMEMORYBARRIERPROC, barriers)
}

auto
engine::graphics::gl::Context::texStorage1D(GLenum target,
					    GLsizei levels,
					    GLenum internalformat,
					    GLsizei width) & noexcept -> void
{
  ENGINE_GL_VERSION_4_2_FUNCTION(glTexStorage1D, PFNGLTEXSTORAGE1DPROC, target, levels, internalformat, width)
}

auto
engine::graphics::gl::Context::texStorage2D(GLenum target,
					    GLsizei levels,
					    GLenum internalformat,
					    GLsizei width,
					    GLsizei height) & noexcept -> void
{
  ENGINE_GL_VERSION_4_2_FUNCTION(glTexStorage2D, PFNGLTEXSTORAGE2DPROC, target, levels, internalformat, width, height)
}

auto
engine::graphics::gl::Context::texStorage3D(GLenum target,
					    GLsizei levels,
					    GLenum internalformat,
					    GLsizei width,
					    GLsizei height,
					    GLsizei depth) & noexcept -> void
{
  ENGINE_GL_VERSION_4_2_FUNCTION(
    glTexStorage3D, PFNGLTEXSTORAGE3DPROC, target, levels, internalformat, width, height, depth)
}

auto
engine::graphics::gl::Context::drawTransformFeedbackInstanced(GLenum mode, GLuint id, GLsizei instancecount) & noexcept
  -> void
{
  ENGINE_GL_VERSION_4_2_FUNCTION(
    glDrawTransformFeedbackInstanced, PFNGLDRAWTRANSFORMFEEDBACKINSTANCEDPROC, mode, id, instancecount)
}

auto
engine::graphics::gl::Context::drawTransformFeedbackStreamInstanced(GLenum mode,
								    GLuint id,
								    GLuint stream,
								    GLsizei instancecount) & noexcept -> void
{
  ENGINE_GL_VERSION_4_2_FUNCTION(glDrawTransformFeedbackStreamInstanced,
				 PFNGLDRAWTRANSFORMFEEDBACKSTREAMINSTANCEDPROC,
				 mode,
				 id,
				 stream,
				 instancecount)
}

auto
engine::graphics::gl::Context::load_version_4_2() & noexcept -> void
{
  using namespace std::literals;
  ENGINE_GL_VERSION_4_2_LOAD(glDrawArraysInstancedBaseInstance, PFNGLDRAWARRAYSINSTANCEDBASEINSTANCEPROC)
  ENGINE_GL_VERSION_4_2_LOAD(glDrawElementsInstancedBaseInstance, PFNGLDRAWELEMENTSINSTANCEDBASEINSTANCEPROC)
  ENGINE_GL_VERSION_4_2_LOAD(glDrawElementsInstancedBaseVertexBaseInstance,
			     PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXBASEINSTANCEPROC)
  ENGINE_GL_VERSION_4_2_LOAD(glGetInternalformativ, PFNGLGETINTERNALFORMATIVPROC)
  ENGINE_GL_VERSION_4_2_LOAD(glGetActiveAtomicCounterBufferiv, PFNGLGETACTIVEATOMICCOUNTERBUFFERIVPROC)
  ENGINE_GL_VERSION_4_2_LOAD(glBindImageTexture, PFNGLBINDIMAGETEXTUREPROC)
  ENGINE_GL_VERSION_4_2_LOAD(glMemoryBarrier, PFNGLMEMORYBARRIERPROC)
  ENGINE_GL_VERSION_4_2_LOAD(glTexStorage1D, PFNGLTEXSTORAGE1DPROC)
  ENGINE_GL_VERSION_4_2_LOAD(glTexStorage2D, PFNGLTEXSTORAGE2DPROC)
  ENGINE_GL_VERSION_4_2_LOAD(glTexStorage3D, PFNGLTEXSTORAGE3DPROC)
  ENGINE_GL_VERSION_4_2_LOAD(glDrawTransformFeedbackInstanced, PFNGLDRAWTRANSFORMFEEDBACKINSTANCEDPROC)
  ENGINE_GL_VERSION_4_2_LOAD(glDrawTransformFeedbackStreamInstanced, PFNGLDRAWTRANSFORMFEEDBACKSTREAMINSTANCEDPROC)
}

#pragma once

namespace engine::graphics {
class IBindable
{
public:
  constexpr IBindable() noexcept = default;

  constexpr IBindable(IBindable const&) noexcept = default;

  constexpr IBindable(IBindable&&) noexcept = default;

  virtual constexpr ~IBindable() noexcept = default;

  constexpr auto operator=(IBindable const&) & noexcept -> IBindable& = default;

  constexpr auto operator=(IBindable&&) & noexcept -> IBindable& = default;

  virtual auto bind() const& noexcept -> void = 0;

  //[[nodiscard]] virtual auto is_bound() const& noexcept -> bool = 0;

  // virtual auto unbind() const& noexcept -> void = 0;

protected:
private:
};
} // namespace engine::graphics

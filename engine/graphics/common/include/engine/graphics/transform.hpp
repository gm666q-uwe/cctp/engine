#pragma once

#include "engine/graphics/glm.hpp"

namespace engine::graphics {
struct Transform
  {
  public:
    // constexpr Transform() noexcept = delete;

    constexpr Transform(Transform const&) noexcept = default;

    constexpr Transform(Transform&& other) noexcept = default;

    virtual constexpr ~Transform() noexcept = default;

    constexpr auto operator=(Transform const&) & noexcept -> Transform& = default;

    constexpr auto operator=(Transform&& other) & noexcept -> Transform& = default;

    [[nodiscard]] constexpr auto forward() const& noexcept -> glm::vec3 const& { return m_forward; }

    [[nodiscard]] constexpr auto matrix() const& noexcept -> glm::mat4 const& { return m_matrix; }

    static constexpr auto new_() noexcept -> Transform { return Transform(); }

    static auto new_pointer() noexcept -> Transform*;

    [[nodiscard]] constexpr auto position() const& noexcept -> glm::vec3 const& { return m_position; }

    [[nodiscard]] constexpr auto right() const& noexcept -> glm::vec3 const& { return m_right; }

    [[nodiscard]] constexpr auto rotation() const& noexcept -> glm::quat const& { return m_rotation; }

    [[nodiscard]] auto rotation_euler() const& noexcept -> glm::vec3;

    [[nodiscard]] constexpr auto scale() const& noexcept -> glm::vec3 const& { return m_scale; }

    constexpr auto set_position(glm::vec3 const& position) & noexcept -> void { m_position = position; }

    constexpr auto set_rotation(glm::quat const& rotation) & noexcept -> void { m_rotation = rotation; }

    auto set_rotation_euler(glm::vec3 const& rotation) & noexcept -> void;

    constexpr auto set_scale(glm::vec3 const& scale) & noexcept -> void { m_scale = scale; }

    [[nodiscard]] constexpr auto up() const& noexcept -> glm::vec3 const& { return m_up; }

  protected:
  private:
    static constexpr glm::vec3 const M_FORWARD = glm::vec3(0.0f, 0.0f, -1.0f);
    static constexpr glm::vec3 const M_RIGHT = glm::vec3(1.0f, 0.0f, 0.0f);
    static constexpr glm::vec3 const M_UP = glm::vec3(0.0f, 1.0f, 0.0f);

    glm::vec3 m_forward = M_FORWARD;
    glm::mat4 m_matrix = glm::identity<glm::mat4>();
    glm::vec3 m_position = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 m_right = M_RIGHT;
    glm::quat m_rotation = glm::identity<glm::quat>();
    glm::vec3 m_scale = glm::vec3(1.0f, 1.0f, 1.0f);
    glm::vec3 m_up = M_UP;

    constexpr Transform() noexcept = default;

    auto recalculate_direction() & noexcept -> void;

    auto recalculate_matrix() & noexcept -> void;
  };
} // namespace engine::graphics

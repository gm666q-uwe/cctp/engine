#pragma once

#include "engine/graphics/i_uniform_buffer.hpp"

namespace engine::graphics {
template<typename T>
class UniformBuffer : public IUniformBuffer<T>
{
public:
  constexpr UniformBuffer<T>() noexcept = default;

  constexpr UniformBuffer<T>(UniformBuffer<T> const&) noexcept = default;

  constexpr UniformBuffer<T>(UniformBuffer<T>&&) noexcept = default;

  constexpr ~UniformBuffer() noexcept override = default;

  constexpr auto operator=(UniformBuffer<T> const&) & noexcept -> UniformBuffer<T>& = default;

  constexpr auto operator=(UniformBuffer<T>&&) & noexcept -> UniformBuffer<T>& = default;

protected:
private:
};
} // namespace engine::graphics

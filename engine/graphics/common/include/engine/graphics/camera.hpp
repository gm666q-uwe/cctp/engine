#pragma once

#include <memory>

#include <engine/types.h>

#include "engine/graphics/uniform_buffer.hpp"
#include "engine/graphics/transform.hpp"

namespace engine::graphics {
class Camera
{
public:
  struct Matrices
    {
    public:
      glm::mat4 projection = glm::identity<glm::mat4>();
      glm::mat4 view = glm::identity<glm::mat4>();

    protected:
    private:
    };

  constexpr Camera() noexcept = delete;

  constexpr Camera(Camera const&) noexcept = delete;

  Camera(Camera&& other) noexcept;

  virtual ~Camera() noexcept;

  constexpr auto operator=(Camera const&) & noexcept -> Camera& = delete;

  auto operator=(Camera&& other) & noexcept -> Camera&;

  // auto bind() const& noexcept -> void override;

  //[[nodiscard]] auto is_bound() const& noexcept -> bool override;

  auto late_update(f64 delta_time, f64 time) & noexcept -> void;

  static auto new_(UniformBuffer<Matrices>* uniform_buffer) noexcept -> Camera;

  static auto new_pointer(UniformBuffer<Matrices>* uniform_buffer) noexcept -> Camera*;

  auto on_resize(u16 height, u16 width) & noexcept -> void;

  [[nodiscard]] constexpr auto transform() const& noexcept -> Transform const& { return m_transform; }

  constexpr auto transform_mut() & noexcept -> Transform&
  {
    m_view_changed = true;
    return m_transform;
  }

  // auto unbind() const& noexcept -> void override;

protected:
private:
  static constexpr f32 const M_FOV = 60.0f;
  static constexpr f32 const M_Z_FAR = 1000.0f;
  static constexpr f32 const M_Z_NEAR = 0.3f;

  f32 m_fov = M_FOV;
  Transform m_transform = Transform::new_();
  std::unique_ptr<UniformBuffer<Matrices>> m_uniform_buffer = nullptr;
  bool m_view_changed = false;
  f32 m_z_far = M_Z_FAR;
  f32 m_z_near = M_Z_NEAR;

  explicit Camera(UniformBuffer<Matrices>* uniform_buffer) noexcept;

  auto create_projection(f32 height, f32 width) & noexcept -> void;

  auto create_view() & noexcept -> void;

  auto drop() & noexcept -> void;

  auto initialize() & noexcept -> void;
};
} // namespace engine::graphics

#pragma once

#include "engine/graphics/glm.hpp"

namespace engine::graphics {
struct Vertex
{
public:
  glm::vec3 position;
  glm::vec2 tex_coords;
  glm::vec3 normal;

protected:
private:
};
} // namespace engine::graphics

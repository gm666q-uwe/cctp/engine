#pragma once

#define GLM_ENABLE_EXPERIMENTAL
#define GLM_FORCE_ALIGNED_GENTYPES
//#define GLM_FORCE_DEPTH_ZERO_TO_ONE
//#define GLM_FORCE_MESSAGES
//#define GLM_FORCE_SWIZZLE
#include <glm/glm.hpp>
#include <glm/ext.hpp>

#pragma once

#include "engine/graphics/i_bindable.hpp"

namespace engine::graphics {
template<typename T>
class IUniformBuffer : public IBindable
{
public:
  constexpr IUniformBuffer<T>() noexcept = default;

  constexpr IUniformBuffer<T>(IUniformBuffer<T> const&) noexcept = default;

  constexpr IUniformBuffer<T>(IUniformBuffer<T>&&) noexcept = default;

  constexpr ~IUniformBuffer() noexcept override = default;

  constexpr auto operator=(IUniformBuffer<T> const&) & noexcept -> IUniformBuffer<T>& = default;

  constexpr auto operator=(IUniformBuffer<T>&&) & noexcept -> IUniformBuffer<T>& = default;

  [[nodiscard]] virtual auto data() const& noexcept -> T* = 0;

protected:
private:
};
} // namespace engine::graphics

#pragma once

#include "engine/graphics/i_graphics_context.hpp"

namespace engine::graphics {
class GraphicsContext : public IGraphicsContext
{
public:
  constexpr GraphicsContext() noexcept = default;

  constexpr GraphicsContext(GraphicsContext const&) noexcept = default;

  constexpr GraphicsContext(GraphicsContext&&) noexcept = default;

  constexpr ~GraphicsContext() noexcept override = default;

  constexpr auto operator=(GraphicsContext const&) & noexcept -> GraphicsContext& = default;

  constexpr auto operator=(GraphicsContext&&) & noexcept -> GraphicsContext& = default;

protected:
private:
};
} // namespace engine::graphics

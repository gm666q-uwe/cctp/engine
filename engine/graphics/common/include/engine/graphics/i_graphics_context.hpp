#pragma once

#include <result/result.hpp>

#include <engine/error.hpp>
#include <engine/types.h>

#include "engine/graphics/camera.hpp"

namespace engine::graphics {
class Camera;

class IGraphicsContext
{
public:
  constexpr IGraphicsContext() noexcept = default;

  constexpr IGraphicsContext(IGraphicsContext const&) noexcept = default;

  constexpr IGraphicsContext(IGraphicsContext&&) noexcept = default;

  virtual constexpr ~IGraphicsContext() noexcept = default;

  constexpr auto operator=(IGraphicsContext const&) & noexcept -> IGraphicsContext& = default;

  constexpr auto operator=(IGraphicsContext&&) & noexcept -> IGraphicsContext& = default;

  virtual auto begin_frame() & noexcept -> void = 0;

  [[nodiscard]] virtual auto create_camera() const& noexcept -> result::Result<Camera, Error> = 0;

  [[nodiscard]] virtual auto create_camera_pointer() const& noexcept -> result::Result<Camera*, Error> = 0;

  //[[nodiscard]] virtual auto create_uniform_buffer(isize size) const& noexcept -> result::Result<UniformBuffer<void>,
  // Error> = 0;

  [[nodiscard]] virtual auto create_uniform_buffer_pointer(isize size) const& noexcept
    -> result::Result<UniformBuffer<void>*, Error> = 0;

  virtual auto end_frame() & noexcept -> void = 0;

  virtual auto make_current() const& noexcept -> void = 0;

  virtual auto on_resize(u16 height, u16 width) & noexcept -> void = 0;

  virtual auto set_camera(Camera* camera) & noexcept -> void = 0;

  virtual auto set_clear_color(glm::vec4 const& color) & noexcept -> void = 0;

  virtual auto set_resize_callback(std::function<void(u16, u16)> resize_callback) & noexcept -> void = 0;

protected:
private:
};
} // namespace engine::graphics

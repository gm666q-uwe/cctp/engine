cmake_minimum_required(VERSION 3.19)

project(engine_graphics_common VERSION 0.1.0 DESCRIPTION WIP LANGUAGES C CXX)

set(PROJECT_HEADER_PATH ${PROJECT_SOURCE_DIR}/include/engine/graphics)
set(PROJECT_SOURCE_PATH ${PROJECT_SOURCE_DIR}/src/engine/graphics)

set(${PROJECT_NAME}_INCLUDE_DIRS
        ${PROJECT_SOURCE_DIR}/include)

set(${PROJECT_NAME}_HEADERS
        ${PROJECT_HEADER_PATH}/camera.hpp
        ${PROJECT_HEADER_PATH}/glm.hpp
        ${PROJECT_HEADER_PATH}/graphics_context.hpp
        ${PROJECT_HEADER_PATH}/i_bindable.hpp
        ${PROJECT_HEADER_PATH}/i_graphics_context.hpp
        ${PROJECT_HEADER_PATH}/i_uniform_buffer.hpp
        ${PROJECT_HEADER_PATH}/transform.hpp
        ${PROJECT_HEADER_PATH}/uniform_buffer.hpp
        ${PROJECT_HEADER_PATH}/vertex.hpp)

set(${PROJECT_NAME}_SOURCES
        ${PROJECT_SOURCE_PATH}/camera.cpp
        ${PROJECT_SOURCE_PATH}/transform.cpp)

add_library(${PROJECT_NAME} STATIC ${${PROJECT_NAME}_HEADERS} ${${PROJECT_NAME}_SOURCES})
target_include_directories(${PROJECT_NAME} PUBLIC ${${PROJECT_NAME}_INCLUDE_DIRS})
target_link_libraries(${PROJECT_NAME} PUBLIC glm)
target_link_libraries(${PROJECT_NAME} PUBLIC engine_common)
target_precompile_headers(${PROJECT_NAME} PRIVATE "$<$<COMPILE_LANGUAGE:CXX>:${PROJECT_SOURCE_DIR}/pch/pch.hpp>")

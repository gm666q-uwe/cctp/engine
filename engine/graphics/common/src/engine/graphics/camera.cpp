#include "engine/graphics/camera.hpp"

engine::graphics::Camera::Camera(UniformBuffer<Matrices>* uniform_buffer) noexcept
  : m_uniform_buffer(uniform_buffer)
{}

engine::graphics::Camera::Camera(engine::graphics::Camera&& other) noexcept
  : m_transform(std::move(other.m_transform))
  , m_uniform_buffer(std::move(other.m_uniform_buffer))
  , m_view_changed(std::exchange(other.m_view_changed, false))
{}

engine::graphics::Camera::~Camera() noexcept
{
  drop();
}

auto
engine::graphics::Camera::operator=(engine::graphics::Camera&& other) & noexcept -> engine::graphics::Camera&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_transform = std::move(other.m_transform);
  m_uniform_buffer = std::move(other.m_uniform_buffer);
  m_view_changed = std::exchange(other.m_view_changed, m_view_changed);

  return *this;
}

/*auto
engine::graphics::Camera::bind() const& noexcept -> void
{
  m_gl->bindBufferBase(GL_UNIFORM_BUFFER, 0, m_ubo);
}*/

auto
engine::graphics::Camera::create_projection(engine::f32 height, engine::f32 width) & noexcept -> void
{
  m_uniform_buffer->data()->projection = glm::perspective(glm::radians(m_fov), width / height, m_z_near, m_z_far);
}

auto
engine::graphics::Camera::create_view() & noexcept -> void
{
  /*auto view = glm::toMat4(glm::conjugate(m_transform.rotation()));
  view[3][0] = -(view[0][0] * m_position.x + view[1][0] * m_position.y + view[2][0] * m_position.z);
  view[3][1] = -(view[0][1] * m_position.x + view[1][1] * m_position.y + view[2][1] * m_position.z);
  view[3][2] = -(view[0][2] * m_position.x + view[1][2] * m_position.y + view[2][2] * m_position.z);
  view[3][3] = 1.0f;
  m_uniform_buffer->data()->view = view;*/

  m_uniform_buffer->data()->view =
    glm::toMat4(glm::conjugate(m_transform.rotation())) * glm::translate(-m_transform.position());
}

auto
engine::graphics::Camera::drop() & noexcept -> void
{
  /*if (is_bound()) {
    unbind();
  }*/
  m_uniform_buffer.reset();
}

auto
engine::graphics::Camera::initialize() & noexcept -> void
{
  m_uniform_buffer->data()->projection = glm::identity<glm::mat4>();
  create_view();
}

/*auto
engine::graphics::Camera::is_bound() const& noexcept -> bool
{
  if (m_ubo == GL_NONE) {
    return false;
  }
  auto ubo = static_cast<GLuint>(GL_NONE);
  m_gl->getIntegerv(GL_UNIFORM_BUFFER_BINDING, reinterpret_cast<GLint*>(&ubo));
  return m_ubo == ubo;
}*/

auto
engine::graphics::Camera::late_update(engine::f64 delta_time, engine::f64 time) & noexcept -> void
{
  if (m_view_changed) {
    create_view();
  }
  m_view_changed = false;
}

auto
engine::graphics::Camera::new_(UniformBuffer<Matrices>* uniform_buffer) noexcept -> Camera
{
  auto self = Camera(uniform_buffer);
  self.initialize();
  return std::move(self);
}

auto
engine::graphics::Camera::new_pointer(UniformBuffer<Matrices>* uniform_buffer) noexcept -> Camera*
{
  auto* self = new Camera(uniform_buffer);
  self->initialize();
  return self;
}

auto
engine::graphics::Camera::on_resize(engine::u16 height, engine::u16 width) & noexcept -> void
{
  create_projection(height == 0 ? 1.0f : static_cast<f32>(height), width == 0 ? 1.0f : static_cast<f32>(width));
}

/*auto
engine::graphics::Camera::unbind() const& noexcept -> void
{
  m_gl->bindBufferBase(GL_UNIFORM_BUFFER, 0, GL_NONE);
}*/

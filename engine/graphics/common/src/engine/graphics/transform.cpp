#include "engine/graphics/transform.hpp"

auto
engine::graphics::Transform::new_pointer() noexcept -> engine::graphics::Transform*
{
  return new Transform();
}

auto
engine::graphics::Transform::recalculate_direction() & noexcept -> void
{
  auto rotation_matrix = glm::toMat3(m_rotation);
  m_forward = -rotation_matrix[2];
  m_right = rotation_matrix[0];
  m_up = rotation_matrix[1];
}

auto
engine::graphics::Transform::recalculate_matrix() & noexcept -> void
{
  m_matrix = glm::translate(m_position) * glm::toMat4(m_rotation) * glm::scale(m_scale);
}

auto
engine::graphics::Transform::rotation_euler() const& noexcept -> glm::vec3
{
  auto r = glm::eulerAngles(m_rotation);
  return glm::degrees(glm::vec3(r.x, r.y, r.z));
}

auto
engine::graphics::Transform::set_rotation_euler(const glm::vec3& rotation) & noexcept -> void
{
  auto r = glm::radians(rotation);
  m_rotation = glm::toQuat(glm::eulerAngleZYX(r.z, -r.y, -r.x));
  recalculate_direction();
  recalculate_matrix();
}

#pragma once

#include <cstdint>
#include <functional>
#include <optional>
#include <ostream>
#include <utility>

#include <boost/preprocessor.hpp>

#pragma once

#include "engine/config/config.h"
#include "engine/types.h"

#if ENGINE_IS_WINDOWS
#include <sdkddkver.h>

#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>
#endif

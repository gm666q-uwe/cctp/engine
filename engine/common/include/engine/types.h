#pragma once

#ifdef __cplusplus
#include <cstdint>
#include <string>
#include <string_view>
#include <vector>

namespace engine {
using f32 = float;
using f64 = double;
// using f128 = long double;

using i8 = std::int8_t;
using i16 = std::int16_t;
using i32 = std::int32_t;
using i64 = std::int64_t;
// using i128 = std::int128_t;
using isize = std::intptr_t;

using u8 = std::uint8_t;
using u16 = std::uint16_t;
using u32 = std::uint32_t;
using u64 = std::uint64_t;
// using u128 = std::uint128_t;
using usize = std::uintptr_t;

using CStr = std::basic_string_view<char>;
using CString = std::basic_string<char>;
using str = std::basic_string_view<char8_t>;
using String = std::basic_string<char8_t>;

template<typename T, typename Alloc = std::allocator<T>>
using Vec = std::vector<T, Alloc>;
} // namespace engine
#endif

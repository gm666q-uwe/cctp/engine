#pragma once

#include <functional>
#include <optional>
#include <ostream>

#include <boost/preprocessor.hpp>

#include "engine/types.h"

#define ENGINE_BITFLAGS_ALL(r, data, i, elem)                                                                          \
  BOOST_PP_IF(i, | BOOST_PP_TUPLE_ELEM(2, 1, elem), BOOST_PP_TUPLE_ELEM(2, 1, elem))

#define ENGINE_BITFLAGS_CONST(r, data, elem)                                                                           \
  static constexpr auto BOOST_PP_TUPLE_ELEM(2, 0, elem)() noexcept->data                                               \
  {                                                                                                                    \
    return data(BOOST_PP_TUPLE_ELEM(2, 1, elem));                                                                      \
  }

#define ENGINE_BITFLAGS_HASH(name, type)                                                                               \
  namespace std {                                                                                                      \
  template<>                                                                                                           \
  struct hash<name>                                                                                                    \
  {                                                                                                                    \
    /*constexpr*/ auto operator()(name const& self) const /*&*/ /*noexcept*/ -> std::size_t                            \
    {                                                                                                                  \
      return std::hash<type>{}(self.m_bits);                                                                           \
    }                                                                                                                  \
  };                                                                                                                   \
  }

#define ENGINE_BITFLAGS_OSTREAM(r, data, elem)                                                                         \
  if (BOOST_PP_CAT(__, data)::BOOST_PP_TUPLE_ELEM(2, 0, elem)(self)) {                                                 \
    if (!first) {                                                                                                      \
      os << " | ";                                                                                                     \
    }                                                                                                                  \
    first = false;                                                                                                     \
    os << BOOST_PP_STRINGIZE(BOOST_PP_TUPLE_ELEM(2, 0, elem));                                                         \
  }

#define ENGINE_BITFLAGS_OSTREAM_TEST(r, data, elem)                                                                    \
  static constexpr auto BOOST_PP_TUPLE_ELEM(2, 0, elem)(data const& self) noexcept->bool                               \
  {                                                                                                                    \
    if (data::BOOST_PP_TUPLE_ELEM(2, 0, elem)().m_bits == 0 && self.m_bits != 0) {                                     \
      return false;                                                                                                    \
    } else {                                                                                                           \
      return (self.m_bits & data::BOOST_PP_TUPLE_ELEM(2, 0, elem)().m_bits) ==                                         \
	     data::BOOST_PP_TUPLE_ELEM(2, 0, elem)().m_bits;                                                           \
    }                                                                                                                  \
  }

#define ENGINE_BITFLAGS(name, type, flag_count, flags)                                                                 \
  struct name                                                                                                          \
  {                                                                                                                    \
    friend struct std::hash<name>;                                                                                     \
                                                                                                                       \
  public:                                                                                                              \
    constexpr name() noexcept = delete;                                                                                \
                                                                                                                       \
    constexpr name(name const&) noexcept = default;                                                                    \
                                                                                                                       \
    constexpr name(name&&) noexcept = default;                                                                         \
                                                                                                                       \
    friend constexpr auto operator-(name self, name other) noexcept -> name                                            \
    {                                                                                                                  \
      return name(self.m_bits & ~other.m_bits);                                                                        \
    }                                                                                                                  \
                                                                                                                       \
    friend constexpr auto operator^(name self, name other) noexcept -> name                                            \
    {                                                                                                                  \
      return name(self.m_bits ^ other.m_bits);                                                                         \
    }                                                                                                                  \
                                                                                                                       \
    friend constexpr auto operator&(name self, name other) noexcept -> name                                            \
    {                                                                                                                  \
      return name(self.m_bits & other.m_bits);                                                                         \
    }                                                                                                                  \
                                                                                                                       \
    friend constexpr auto operator|(name self, name other) noexcept -> name                                            \
    {                                                                                                                  \
      return name(self.m_bits | other.m_bits);                                                                         \
    }                                                                                                                  \
                                                                                                                       \
    friend constexpr auto operator~(name self) noexcept -> name { return name(~self.m_bits) & all(); }                 \
                                                                                                                       \
    constexpr auto operator=(name const&) & noexcept -> name& = default;                                               \
                                                                                                                       \
    constexpr auto operator=(name&&) & noexcept -> name& = default;                                                    \
                                                                                                                       \
    friend constexpr auto operator-=(name& self, name other) noexcept -> void { self.m_bits &= ~other.m_bits; }        \
                                                                                                                       \
    friend constexpr auto operator^=(name& self, name other) noexcept -> void { self.m_bits ^= other.m_bits; }         \
                                                                                                                       \
    friend constexpr auto operator&=(name& self, name other) noexcept -> void { self.m_bits &= other.m_bits; }         \
                                                                                                                       \
    friend constexpr auto operator|=(name& self, name other) noexcept -> void { self.m_bits |= other.m_bits; }         \
                                                                                                                       \
    friend constexpr auto operator<<(std::ostream& os, name const& self) -> std::ostream&                              \
    {                                                                                                                  \
      struct BOOST_PP_CAT(__, name)                                                                                    \
      {                                                                                                                \
      public:                                                                                                          \
	BOOST_PP_LIST_FOR_EACH(ENGINE_BITFLAGS_OSTREAM_TEST, name, BOOST_PP_TUPLE_TO_LIST(flag_count, flags))          \
      protected:                                                                                                       \
      private:                                                                                                         \
      };                                                                                                               \
                                                                                                                       \
      auto first = true;                                                                                               \
      BOOST_PP_LIST_FOR_EACH(ENGINE_BITFLAGS_OSTREAM, name, BOOST_PP_TUPLE_TO_LIST(flag_count, flags))                 \
      auto extra_bits = self.m_bits & ~all().m_bits;                                                                   \
      if (extra_bits != 0) {                                                                                           \
	if (!first) {                                                                                                  \
	  os << " | ";                                                                                                 \
	}                                                                                                              \
	first = false;                                                                                                 \
	os << "0x" << std::hex << extra_bits << std::dec;                                                              \
      }                                                                                                                \
      if (first) {                                                                                                     \
	os << "(empty)";                                                                                               \
      }                                                                                                                \
      return os;                                                                                                       \
    }                                                                                                                  \
                                                                                                                       \
    friend constexpr auto operator<=>(name const&, name const&) noexcept = default;                                    \
                                                                                                                       \
    BOOST_PP_LIST_FOR_EACH(ENGINE_BITFLAGS_CONST, name, BOOST_PP_TUPLE_TO_LIST(flag_count, flags))                     \
                                                                                                                       \
    static constexpr auto empty() noexcept -> name { return name(0); }                                                 \
                                                                                                                       \
    static constexpr auto all() noexcept -> name                                                                       \
    {                                                                                                                  \
      return name(BOOST_PP_LIST_FOR_EACH_I(ENGINE_BITFLAGS_ALL, , BOOST_PP_TUPLE_TO_LIST(flag_count, flags)));         \
    }                                                                                                                  \
                                                                                                                       \
    constexpr auto bits() const& noexcept -> type { return m_bits; }                                                   \
                                                                                                                       \
    static constexpr auto from_bits(type bits) noexcept -> std::optional<name>                                         \
    {                                                                                                                  \
      if ((bits & ~all().m_bits) == 0) {                                                                               \
	return std::optional<name>(name(bits));                                                                        \
      } else {                                                                                                         \
	return std::optional<name>();                                                                                  \
      }                                                                                                                \
    }                                                                                                                  \
                                                                                                                       \
    static constexpr auto from_bits_truncate(type bits) noexcept -> name { return name(bits & all().m_bits); }         \
                                                                                                                       \
    static constexpr auto from_bits_unchecked(type bits) noexcept -> name { return name(bits); }                       \
                                                                                                                       \
    constexpr auto is_empty() const& noexcept -> bool { return m_bits == empty().m_bits; }                             \
                                                                                                                       \
    constexpr auto is_all() const& noexcept -> bool { return m_bits == all().m_bits; }                                 \
                                                                                                                       \
    constexpr auto intersects(name other) const& noexcept -> bool { return !name(m_bits & other.m_bits).is_empty(); }  \
                                                                                                                       \
    constexpr auto contains(name other) const& noexcept -> bool { return (m_bits & other.m_bits) == other.m_bits; }    \
                                                                                                                       \
    constexpr auto insert(name other) & noexcept -> void { m_bits |= other.m_bits; }                                   \
                                                                                                                       \
    constexpr auto remove(name other) & noexcept -> void { m_bits &= ~other.m_bits; }                                  \
                                                                                                                       \
    constexpr auto toggle(name other) & noexcept -> void { m_bits ^= other.m_bits; }                                   \
                                                                                                                       \
    constexpr auto set(name other, bool value) & noexcept -> void                                                      \
    {                                                                                                                  \
      if (value) {                                                                                                     \
	insert(other);                                                                                                 \
      } else {                                                                                                         \
	remove(other);                                                                                                 \
      }                                                                                                                \
    }                                                                                                                  \
                                                                                                                       \
  protected:                                                                                                           \
  private:                                                                                                             \
    type m_bits;                                                                                                       \
                                                                                                                       \
    constexpr explicit name(type bits) noexcept                                                                        \
      : m_bits(bits)                                                                                                   \
    {}                                                                                                                 \
  };

#define ENGINE_BITFLAGS_FULL(ns, name, type, flag_count, flags)                                                        \
  namespace ns {                                                                                                       \
  ENGINE_BITFLAGS(name, type, flag_count, flags)                                                                       \
  }                                                                                                                    \
                                                                                                                       \
  ENGINE_BITFLAGS_HASH(ns::name, type)

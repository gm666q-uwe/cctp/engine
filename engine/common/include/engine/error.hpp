#pragma once

#include "engine/types.h"

namespace engine {
union Error
{
public:
  u64 full_code;
  struct
  {
    u32 source;
    i32 code;
  };

  static constexpr auto new_(u32 source, i32 code) noexcept -> Error { return Error(source, code); }

  static constexpr auto with_full_code(u64 full_code) noexcept -> Error { return Error(full_code); }

protected:
private:
  constexpr explicit Error(u64 full_code) noexcept
    : full_code(full_code)
  {}

  constexpr explicit Error(u32 source, i32 code) noexcept
    : code(code)
    , source(source)
  {}
};
} // namespace engine

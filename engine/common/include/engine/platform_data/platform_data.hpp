#pragma once

namespace engine::platform_data {
class PlatformData
{
public:
  constexpr PlatformData(PlatformData const&) noexcept = default;

  constexpr PlatformData(PlatformData&&) noexcept = default;

  virtual constexpr ~PlatformData() noexcept = default;

  constexpr auto operator=(PlatformData const&) & noexcept -> PlatformData& = default;

  constexpr auto operator=(PlatformData&&) & noexcept -> PlatformData& = default;

protected:
  constexpr PlatformData() noexcept = default;

private:
};
} // namespace engine::platform_data

#pragma once

#include <utility>

#include "engine/platform.h"
#include "engine/platform_data/platform_data.hpp"

namespace engine::platform_data {
#if ENGINE_IS_WINDOWS
class WindowsData : public PlatformData
{
public:
  constexpr WindowsData() noexcept = delete;

  constexpr WindowsData(WindowsData const&) noexcept = default;

  constexpr WindowsData(WindowsData&&) noexcept = default;

  constexpr ~WindowsData() noexcept override = default;

  constexpr auto operator=(WindowsData const&) & noexcept -> WindowsData& = default;

  constexpr auto operator=(WindowsData&&) & noexcept -> WindowsData& = default;

  [[nodiscard]] constexpr auto instance() const& noexcept -> HINSTANCE { return m_instance; }

  static constexpr auto new_(HINSTANCE instance, HINSTANCE previous_instance, int show_command) noexcept -> WindowsData
  {
    return std::move(WindowsData(instance, previous_instance, show_command));
  }

  static constexpr auto new_pointer(HINSTANCE instance, HINSTANCE previous_instance, int show_command) noexcept
    -> WindowsData*
  {
    return new WindowsData(instance, previous_instance, show_command);
  }

  [[nodiscard]] constexpr auto previous_instance() const& noexcept -> HINSTANCE { return m_previous_instance; }

  [[nodiscard]] constexpr auto show_command() const& noexcept -> int { return m_show_command; }

protected:
private:
  HINSTANCE m_instance = nullptr;
  HINSTANCE m_previous_instance = nullptr;
  int m_show_command = SW_SHOWDEFAULT;

  constexpr WindowsData(HINSTANCE instance, HINSTANCE previous_instance, int show_command) noexcept
    : PlatformData()
    , m_instance(instance)
    , m_previous_instance(previous_instance)
    , m_show_command(show_command)
  {}
};
#endif
} // namespace engine::platform_data

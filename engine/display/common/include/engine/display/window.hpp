#pragma once

#include "engine/display/i_window.hpp"
#include "engine/display/window_options.hpp"

namespace engine::display {
class Window : public IWindow
{
public:
  constexpr Window() noexcept = delete;

  constexpr Window(Window const&) noexcept = delete;

  constexpr Window(Window&& other) noexcept
    : IWindow(std::move(other))
    , m_height(other.m_height)
    , m_width(other.m_width)
  {}

  ~Window() noexcept override = default;

  constexpr auto operator=(Window const&) & noexcept -> Window& = delete;

  constexpr auto operator=(Window&& other) & noexcept -> Window&
  {
    if (this == &other) {
      return *this;
    }

    m_height = other.m_height;
    m_width = other.m_width;

    IWindow::operator=(std::move(other));
    return *this;
  }

  [[nodiscard]] constexpr auto height() const& noexcept -> u16 override { return m_height; }

  [[nodiscard]] constexpr auto width() const& noexcept -> u16 override { return m_width; }

protected:
  u16 m_height = 0;
  u16 m_width = 0;

  explicit Window(WindowOptions const& options) noexcept
    : IWindow()
    , m_height(options.height())
    , m_width(options.width())
  {}

private:
};
} // namespace engine::display

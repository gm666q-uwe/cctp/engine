#pragma once

#include <engine/types.h>

namespace engine::display {
using namespace std::literals;

class DisplayOptions
{
public:
  constexpr DisplayOptions(DisplayOptions const&) noexcept = delete;

  DisplayOptions(DisplayOptions&& other) noexcept
    : m_app_id(std::move(other.m_app_id))
  {}

  virtual ~DisplayOptions() noexcept = default;

  constexpr auto operator=(DisplayOptions const&) & noexcept -> DisplayOptions& = delete;

  auto operator=(DisplayOptions&& other) & noexcept -> DisplayOptions&
    {
    if (this == &other) {
      return *this;
    }

    m_app_id = std::move(other.m_app_id);

    return *this;
    }

  [[nodiscard]] auto app_id() const& noexcept -> CStr { return m_app_id; }

  auto app_id(CStr app_id) & noexcept -> DisplayOptions&
  {
    m_app_id = app_id;

    return *this;
  }

  auto app_id(CStr app_id) && noexcept -> DisplayOptions
  {
    m_app_id = app_id;

    return std::move(*this);
  }

  auto app_id(CString&& app_id) & noexcept -> DisplayOptions&
  {
    m_app_id = std::move(app_id);

    return *this;
  }

  auto app_id(CString&& app_id) && noexcept -> DisplayOptions
  {
    m_app_id = std::move(app_id);

    return std::move(*this);
  }

protected:
  constexpr DisplayOptions() noexcept = default;

private:
  CString m_app_id = "com.example.app"s;
};
} // namespace engine::display

#pragma once

#include <engine/types.h>

namespace engine::display {
using namespace std::literals;

class WindowOptions
{
public:
  enum class GraphicsAPI : u8
  {
    OpenGL,
    Vulkan
  };

  constexpr WindowOptions(WindowOptions const&) noexcept = delete;

  WindowOptions(WindowOptions&& other) noexcept
    : m_graphics_api(other.m_graphics_api)
    , m_height(other.m_height)
    , m_title(std::move(other.m_title))
    , m_width(other.m_width)
  {}

  virtual ~WindowOptions() noexcept = default;

  constexpr auto operator=(WindowOptions const&) & noexcept -> WindowOptions& = delete;

  auto operator=(WindowOptions&& other) & noexcept -> WindowOptions&
  {
    if (this == &other) {
      return *this;
    }

    m_graphics_api = other.m_graphics_api;
    m_height = other.m_height;
    m_title = std::move(other.m_title);
    m_width = other.m_width;

    return *this;
  }

  [[nodiscard]] constexpr auto graphics_api() const& noexcept -> GraphicsAPI { return m_graphics_api; }

  constexpr auto graphics_api(GraphicsAPI graphics_api) & noexcept -> WindowOptions&
  {
    m_graphics_api = graphics_api;

    return *this;
  }

  auto graphics_api(GraphicsAPI graphics_api) && noexcept -> WindowOptions
  {
    m_graphics_api = graphics_api;

    return std::move(*this);
  }

  [[nodiscard]] constexpr auto height() const& noexcept -> u16 { return m_height; }

  constexpr auto height(u16 height) & noexcept -> WindowOptions&
  {
    m_height = height;

    return *this;
  }

  auto height(u16 height) && noexcept -> WindowOptions
  {
    m_height = height;

    return std::move(*this);
  }

  [[nodiscard]] auto title() const& noexcept -> CStr { return m_title; }

  auto title(CStr title) & noexcept -> WindowOptions&
  {
    m_title = title;

    return *this;
  }

  auto title(CStr title) && noexcept -> WindowOptions
  {
    m_title = title;

    return std::move(*this);
  }

  auto title(CString&& title) & noexcept -> WindowOptions&
  {
    m_title = std::move(title);

    return *this;
  }

  auto title(CString&& title) && noexcept -> WindowOptions
  {
    m_title = std::move(title);

    return std::move(*this);
  }

  [[nodiscard]] constexpr auto width() const& noexcept -> u16 { return m_width; }

  constexpr auto width(u16 width) & noexcept -> WindowOptions&
  {
    m_width = width;

    return *this;
  }

  auto width(u16 width) && noexcept -> WindowOptions
  {
    m_width = width;

    return std::move(*this);
  }

protected:
  constexpr WindowOptions() noexcept = default;

private:
  GraphicsAPI m_graphics_api = GraphicsAPI::Vulkan;
  u16 m_height = 480;
  CString m_title = "Engine Window"s;
  u16 m_width = 640;
};
} // namespace engine::display

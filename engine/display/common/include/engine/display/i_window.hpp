#pragma once

#include <result/result.hpp>

#include <engine/error.hpp>
#include <engine/graphics/graphics_context.hpp>
#include <engine/input/manager.hpp>

namespace engine::display {
class IWindow
{
public:
  constexpr IWindow() noexcept = default;

  constexpr IWindow(IWindow const&) noexcept = delete;

  constexpr IWindow(IWindow&&) noexcept = default;

  virtual constexpr ~IWindow() noexcept = default;

  constexpr auto operator=(IWindow const&) & noexcept -> IWindow& = delete;

  constexpr auto operator=(IWindow&&) & noexcept -> IWindow& = default;

  [[nodiscard]] virtual auto graphics_context() const& noexcept -> graphics::GraphicsContext& = 0;

  [[nodiscard]] virtual constexpr auto height() const& noexcept -> u16 = 0;

  [[nodiscard]] virtual auto input_manager() const& noexcept -> input::Manager& = 0;

  [[nodiscard]] virtual auto run() & noexcept -> result::Result<bool, Error> = 0;

  [[nodiscard]] virtual constexpr auto width() const& noexcept -> u16 = 0;

protected:
private:
};
} // namespace engine::display

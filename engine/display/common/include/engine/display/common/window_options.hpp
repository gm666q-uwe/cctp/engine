#pragma once

#include "engine/display/window_options.hpp"

namespace engine::display::common {
class WindowOptions : public display::WindowOptions
{
public:
  constexpr WindowOptions(WindowOptions const&) noexcept = delete;

  WindowOptions(WindowOptions&& other) noexcept
    : display::WindowOptions(std::move(other))
  {}

  ~WindowOptions() noexcept override = default;

  constexpr auto operator=(WindowOptions const&) & noexcept -> WindowOptions& = delete;

  auto operator=(WindowOptions&& other) & noexcept -> WindowOptions&
  {
    if (this == &other) {
      return *this;
    }

    display::WindowOptions::operator=(std::move(other));
    return *this;
  }

  static auto new_() noexcept -> WindowOptions { return std::move(WindowOptions()); }

  static auto new_pointer() noexcept -> WindowOptions* { return new WindowOptions(); }

protected:
private:
  constexpr WindowOptions() noexcept
    : display::WindowOptions()
  {}
};
} // namespace engine::display::common

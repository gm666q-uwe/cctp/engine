#pragma once

#include "engine/display/display_options.hpp"
#include "engine/display/i_display.hpp"

namespace engine::display {
class Display : public IDisplay
{
public:
  constexpr Display() noexcept = delete;

  constexpr Display(Display const&) noexcept = delete;

  Display(Display&& other) noexcept
    : IDisplay(std::move(other))
    , m_app_id(std::move(other.m_app_id))
  {}

  ~Display() noexcept override = default;

  constexpr auto operator=(Display const&) & noexcept -> Display& = delete;

  auto operator=(Display&& other) & noexcept -> Display&
  {
    if (this == &other) {
      return *this;
    }

    m_app_id = std::move(other.m_app_id);

    IDisplay::operator=(std::move(other));
    return *this;
  }

protected:
  CString m_app_id = "com.example.app"s;

  explicit Display(DisplayOptions const& options) noexcept
    : IDisplay()
    , m_app_id(options.app_id())
  {}

private:
};
} // namespace engine::display

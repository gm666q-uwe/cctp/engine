#pragma once

#include "engine/display/window.hpp"

namespace engine::display {
class IDisplay
{
public:
  constexpr IDisplay() noexcept = default;

  constexpr IDisplay(IDisplay const&) noexcept = delete;

  constexpr IDisplay(IDisplay&&) noexcept = default;

  virtual constexpr ~IDisplay() noexcept = default;

  constexpr auto operator=(IDisplay const&) & noexcept -> IDisplay& = delete;

  constexpr auto operator=(IDisplay&&) & noexcept -> IDisplay& = default;

  [[nodiscard]] virtual auto create_window_pointer(WindowOptions const& options) const& noexcept
    -> result::Result<Window*, Error> = 0;

  [[nodiscard]] virtual auto run() & noexcept -> result::Result<bool, Error> = 0;

protected:
private:
};
} // namespace engine::display

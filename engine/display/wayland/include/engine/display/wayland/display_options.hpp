#pragma once

#include <engine/display/display_options.hpp>

#include "engine/display/wayland/wayland.h"

namespace engine::display::wayland {
class DisplayOptions : public display::DisplayOptions
{
public:
  constexpr DisplayOptions(DisplayOptions const&) noexcept = delete;

  DisplayOptions(DisplayOptions&& other) noexcept;

  ~DisplayOptions() noexcept override = default;

  constexpr auto operator=(DisplayOptions const&) & noexcept -> DisplayOptions& = delete;

  auto operator=(DisplayOptions&& other) & noexcept -> DisplayOptions&;

  [[nodiscard]] constexpr auto display_name() const& noexcept -> char const* { return m_display_name; }

  constexpr auto display_name(char const* display_name) & noexcept -> DisplayOptions&
  {
    m_display_name = display_name;

    return *this;
  }

  auto display_name(char const* display_name) && noexcept -> DisplayOptions;

  static auto new_() noexcept -> DisplayOptions;

  static auto new_pointer() noexcept -> DisplayOptions*;

protected:
private:
  char const* m_display_name = nullptr;

  constexpr DisplayOptions() noexcept
    : display::DisplayOptions()
  {}
};
} // namespace engine::display::wayland

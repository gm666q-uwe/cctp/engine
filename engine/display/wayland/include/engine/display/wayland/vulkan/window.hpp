#pragma once

#include "engine/display/wayland/window.hpp"

namespace engine::display::wayland {
class Display;

namespace vulkan {
#if ENGINE_HAS_VULKAN
class Window final : public wayland::Window
{
  friend wayland::Display;

  template<typename T>
  using wl_shared_ptr = std::shared_ptr<T>;

  template<typename T>
  using wl_unique_ptr = std::unique_ptr<T, std::function<void(T*)>>;

public:
  constexpr Window(Window const&) noexcept = delete;

  Window(Window&& other) noexcept;

  ~Window() noexcept override;

  constexpr auto operator=(Window const&) & noexcept -> Window& = delete;

  auto operator=(Window&& other) & noexcept -> Window&;

protected:
private:
  explicit Window(WindowOptions const& options, wl_shared_ptr<struct wl_cursor_theme> cursor_theme) noexcept;

  auto drop() & noexcept -> void;

  auto initialize_vulkan(CStr app_id,
			 struct wl_compositor* compositor,
			 struct wl_display* display,
			 CStr title,
			 wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
			 struct xdg_wm_base* xdg_wm_base,
			 wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>> xkb_keymap) & noexcept
    -> option::Option<engine::Error>;

  static auto new_(WindowOptions const& options,
		   CStr app_id,
		   struct wl_compositor* compositor,
		   wl_shared_ptr<struct wl_cursor_theme> cursor_theme,
		   struct wl_display* display,
		   wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
		   struct xdg_wm_base* xdg_wm_base,
		   wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>> xkb_keymap) noexcept
    -> result::Result<Window, Error>;

  static auto new_pointer(WindowOptions const& options,
			  CStr app_id,
			  struct wl_compositor* compositor,
			  wl_shared_ptr<struct wl_cursor_theme> cursor_theme,
			  struct wl_display* display,
			  wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
			  struct xdg_wm_base* xdg_wm_base,
			  wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>> xkb_keymap) noexcept
    -> result::Result<Window*, Error>;

  auto xdg_toplevel_configure(struct xdg_toplevel* xdg_toplevel,
			      int32_t width,
			      int32_t height,
			      struct wl_array* states) & noexcept -> void override;
};
#endif
} // namespace vulkan
} // namespace engine::display::wayland

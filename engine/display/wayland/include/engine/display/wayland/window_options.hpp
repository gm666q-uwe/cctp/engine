#pragma once

#include <engine/display/window_options.hpp>

#include "engine/display/wayland/wayland.h"

namespace engine::display::wayland {
class WindowOptions : public display::WindowOptions
{
public:
  constexpr WindowOptions(WindowOptions const&) noexcept = delete;

  WindowOptions(WindowOptions&& other) noexcept;

  ~WindowOptions() noexcept override = default;

  constexpr auto operator=(WindowOptions const&) & noexcept -> WindowOptions& = delete;

  auto operator=(WindowOptions&& other) & noexcept -> WindowOptions&;

  static auto new_() noexcept -> WindowOptions;

  static auto new_pointer() noexcept -> WindowOptions*;

protected:
private:
  constexpr WindowOptions() noexcept
    : display::WindowOptions()
  {}
};
} // namespace engine::display::wayland

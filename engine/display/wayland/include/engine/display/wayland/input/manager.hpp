#pragma once

#include <engine/input/manager.hpp>

#include "engine/display/wayland/input/keyboard.hpp"
#include "engine/display/wayland/input/mouse.hpp"

namespace engine::display::wayland {
class Window;

namespace input {
class Manager final : public engine::input::Manager
{
  friend class wayland::Window;

  template<typename T>
  using wl_shared_ptr = std::shared_ptr<T>;

  template<typename T>
  using wl_unique_ptr = std::unique_ptr<T, std::function<void(T*)>>;

public:
  constexpr Manager() noexcept = delete;

  constexpr Manager(Manager const&) noexcept = delete;

  Manager(Manager&&) noexcept = default;

  ~Manager() noexcept override;

  constexpr auto operator=(Manager const&) & noexcept -> Manager& = delete;

  auto operator=(Manager&&) & noexcept -> Manager& = default;

  auto keyboard() & noexcept -> Keyboard&;

  auto keyboard() const& noexcept -> engine::input::Keyboard const& override;

  auto mouse() & noexcept -> Mouse&;

  auto mouse() const& noexcept -> engine::input::Mouse const& override;

protected:
private:
  Keyboard m_keyboard;
  Mouse m_mouse;

  explicit Manager(struct wl_surface* surface,
		   wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
		   wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>> xkb_keymap) noexcept;

  auto drop() & noexcept -> void;

  auto initialize() & noexcept -> void;

  static auto new_(struct wl_surface* surface,
		   wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
		   wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>> xkb_keymap) noexcept -> Manager;

  static auto new_pointer(struct wl_surface* surface,
			  wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
			  wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>> xkb_keymap) noexcept -> Manager*;
};
} // namespace input
} // namespace engine::display::wayland

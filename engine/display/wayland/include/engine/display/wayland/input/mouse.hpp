#pragma once

#include <memory>

#include <engine/input/mouse.hpp>

#include "engine/display/wayland/wayland.h"

namespace engine::display::wayland::input {
class Mouse final : public engine::input::Mouse
{
  friend class Manager;

  template<typename T>
  using wl_shared_ptr = std::shared_ptr<T>;

  template<typename T>
  using wl_unique_ptr = std::unique_ptr<T, std::function<void(T*)>>;

public:
  // constexpr Mouse() noexcept = delete;

  constexpr Mouse(Mouse const&) noexcept = delete;

  Mouse(Mouse&& other) noexcept;

  ~Mouse() noexcept override;

  constexpr auto operator=(Mouse const&) & noexcept -> Mouse& = delete;

  auto operator=(Mouse&& other) & noexcept -> Mouse&;

  auto begin_update() & noexcept -> void override;

  auto confine() & noexcept -> void override;

  auto end_update() & noexcept -> void override;

  auto lock() & noexcept -> void override;

  auto pointer_axis(struct wl_pointer* wl_pointer, uint32_t time, uint32_t axis, wl_fixed_t value) & noexcept -> void;

  auto pointer_axis_discrete(struct wl_pointer* wl_pointer, uint32_t axis, int32_t discrete) & noexcept -> void;

  auto pointer_axis_source(struct wl_pointer* wl_pointer, uint32_t axis_source) & noexcept -> void;

  auto pointer_axis_stop(struct wl_pointer* wl_pointer, uint32_t time, uint32_t axis) & noexcept -> void;

  auto pointer_button(struct wl_pointer* wl_pointer,
		      uint32_t serial,
		      uint32_t time,
		      uint32_t button,
		      uint32_t state) & noexcept -> void;

  auto pointer_enter(struct wl_pointer* wl_pointer,
		     uint32_t serial,
		     struct wl_surface* surface,
		     wl_fixed_t surface_x,
		     wl_fixed_t surface_y) & noexcept -> void;

  auto pointer_frame(struct wl_pointer* wl_pointer) & noexcept -> void;

  auto pointer_leave(struct wl_pointer* wl_pointer, uint32_t serial, struct wl_surface* surface) & noexcept -> void;

  auto pointer_motion(struct wl_pointer* wl_pointer,
		      uint32_t time,
		      wl_fixed_t surface_x,
		      wl_fixed_t surface_y) & noexcept -> void;

  auto wp_relative_pointer_relative_motion(struct zwp_relative_pointer_v1* wp_relative_pointer,
					   uint32_t utime_hi,
					   uint32_t utime_lo,
					   wl_fixed_t dx,
					   wl_fixed_t dy,
					   wl_fixed_t dx_unaccel,
					   wl_fixed_t dy_unaccel) & noexcept -> void;

protected:
  virtual auto wp_confined_pointer_confined(struct zwp_confined_pointer_v1* wp_confined_pointer) & noexcept -> void;

  virtual auto wp_confined_pointer_unconfined(struct zwp_confined_pointer_v1* wp_confined_pointer) & noexcept -> void;

  virtual auto wp_locked_pointer_locked(struct zwp_locked_pointer_v1* wp_locked_pointer) -> void;

  virtual auto wp_locked_pointer_unlocked(struct zwp_locked_pointer_v1* wp_locked_pointer) -> void;

private:
  struct wl_surface* m_surface = nullptr;
  struct wl_pointer* m_pointer = nullptr;

  wl_unique_ptr<struct zwp_confined_pointer_v1> m_wp_confined_pointer = wl_unique_ptr<struct zwp_confined_pointer_v1>();
  wl_unique_ptr<struct zwp_locked_pointer_v1> m_wp_locked_pointer = wl_unique_ptr<struct zwp_locked_pointer_v1>();
  wl_shared_ptr<struct zwp_pointer_constraints_v1> m_wp_pointer_constraints =
    wl_shared_ptr<struct zwp_pointer_constraints_v1>();

  explicit Mouse(struct wl_surface* surface,
		 wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints) noexcept;

  auto drop() & noexcept -> void;

  auto initialize() & noexcept -> void;

  static auto new_(struct wl_surface* surface,
		   wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints) noexcept -> Mouse;

  static auto new_pointer(struct wl_surface* surface,
			  wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints) noexcept -> Mouse*;

  static auto wp_confined_pointer_confined(void* data, struct zwp_confined_pointer_v1* wp_confined_pointer) noexcept
    -> void;

  static auto wp_confined_pointer_unconfined(void* data, struct zwp_confined_pointer_v1* wp_confined_pointer) noexcept
    -> void;

  static constexpr struct zwp_confined_pointer_v1_listener const M_WP_CONFINED_POINTER_LISTENER = {
    wp_confined_pointer_confined,
    wp_confined_pointer_unconfined
  };

  static auto wp_locked_pointer_locked(void* data, struct zwp_locked_pointer_v1* wp_locked_pointer) -> void;

  static auto wp_locked_pointer_unlocked(void* data, struct zwp_locked_pointer_v1* wp_locked_pointer) -> void;

  static constexpr struct zwp_locked_pointer_v1_listener const M_WP_LOCKED_POINTER_LISTENER = {
    wp_locked_pointer_locked,
    wp_locked_pointer_unlocked
  };
};
} // namespace engine::display::wayland::input

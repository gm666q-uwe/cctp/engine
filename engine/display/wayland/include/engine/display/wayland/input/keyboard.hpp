#pragma once

#include <memory>

#include <engine/input/keyboard.hpp>

#include "engine/display/wayland/wayland.h"

namespace engine::display::wayland::input {
class Keyboard final : public engine::input::Keyboard
{
  friend class Manager;

  template<typename T>
  using wl_shared_ptr = std::shared_ptr<T>;

  template<typename T>
  using wl_unique_ptr = std::unique_ptr<T, std::function<void(T*)>>;

public:
  // constexpr Keyboard() noexcept = delete;

  constexpr Keyboard(Keyboard const&) noexcept = delete;

  Keyboard(Keyboard&& other) noexcept;

  ~Keyboard() noexcept override;

  constexpr auto operator=(Keyboard const&) & noexcept -> Keyboard& = delete;

  auto operator=(Keyboard&& other) & noexcept -> Keyboard&;

  auto begin_update() & noexcept -> void override;

  [[nodiscard]] auto characters() const& noexcept -> CStr override;

  auto end_update() & noexcept -> void override;

  auto keyboard_enter(struct wl_keyboard* wl_keyboard,
    uint32_t serial,
    struct wl_surface* surface,
      struct wl_array* keys) & noexcept -> void;

  auto keyboard_key(struct wl_keyboard* wl_keyboard,
    uint32_t serial,
    uint32_t time,
    uint32_t key,
    uint32_t state) & noexcept -> void;

  auto keyboard_keymap(struct wl_keyboard* wl_keyboard, uint32_t format, int32_t fd, uint32_t size) & noexcept -> void;

  auto keyboard_leave(struct wl_keyboard* wl_keyboard, uint32_t serial, struct wl_surface* surface) & noexcept -> void;

  auto keyboard_modifiers(struct wl_keyboard* wl_keyboard,
    uint32_t serial,
    uint32_t mods_depressed,
    uint32_t mods_latched,
    uint32_t mods_locked,
    uint32_t group) & noexcept -> void;

  auto keyboard_repeat_info(struct wl_keyboard* wl_keyboard, int32_t rate, int32_t delay) & noexcept -> void;

protected:
private:
  CString m_characters = CString();
  wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>> m_xkb_keymap = wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>>();
  wl_unique_ptr<struct xkb_state> m_xkb_state = wl_unique_ptr<struct xkb_state>();

  explicit Keyboard(wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>> xkb_keymap) noexcept;

  auto drop() & noexcept -> void;

  auto initialize() & noexcept -> void;

  static auto new_(wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>> xkb_keymap) noexcept -> Keyboard;

  static auto new_pointer(wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>> xkb_keymap) noexcept -> Keyboard*;
};
} // namespace engine::display::wayland::input

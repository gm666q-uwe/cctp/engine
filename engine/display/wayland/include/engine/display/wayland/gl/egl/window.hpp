#pragma once

#include "engine/display/wayland/window.hpp"

#if ENGINE_HAS_EGL
#include <engine/graphics/gl/egl/display.hpp>
#endif

namespace engine::display::wayland {
class Display;

namespace gl::egl {
#if ENGINE_HAS_EGL
class Window final : public wayland::Window
{
  friend wayland::Display;

  template<typename T>
  using wl_shared_ptr = std::shared_ptr<T>;

  template<typename T>
  using wl_unique_ptr = std::unique_ptr<T, std::function<void(T*)>>;

public:
  constexpr Window(Window const&) noexcept = delete;

  Window(Window&& other) noexcept;

  ~Window() noexcept override;

  constexpr auto operator=(Window const&) & noexcept -> Window& = delete;

  auto operator=(Window&& other) & noexcept -> Window&;

  [[nodiscard]] auto egl() const& noexcept -> graphics::gl::egl::Context&;

protected:
private:
  std::unique_ptr<graphics::gl::egl::Context> m_egl = std::unique_ptr<graphics::gl::egl::Context>();
  wl_unique_ptr<struct wl_egl_window> m_egl_window = wl_unique_ptr<struct wl_egl_window>();

  explicit Window(WindowOptions const& options, wl_shared_ptr<struct wl_cursor_theme> cursor_theme) noexcept;

  auto drop() & noexcept -> void;

  auto initialize_egl(CStr app_id,
		      struct wl_compositor* compositor,
		      graphics::gl::egl::Display const& egl_display,
		      CStr title,
		      wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
		      struct xdg_wm_base* xdg_wm_base,
		      wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>> xkb_keymap) & noexcept
    -> option::Option<engine::Error>;

  static auto new_(WindowOptions const& options,
		   CStr app_id,
		   struct wl_compositor* compositor,
		   wl_shared_ptr<struct wl_cursor_theme> cursor_theme,
		   graphics::gl::egl::Display const& egl_display,
		   wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
		   struct xdg_wm_base* xdg_wm_base,
		   wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>> xkb_keymap) noexcept
    -> result::Result<Window, Error>;

  static auto new_pointer(WindowOptions const& options,
			  CStr app_id,
			  struct wl_compositor* compositor,
			  wl_shared_ptr<struct wl_cursor_theme> cursor_theme,
			  graphics::gl::egl::Display const& egl_display,
			  wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
			  struct xdg_wm_base* xdg_wm_base,
			  wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>> xkb_keymap) noexcept
    -> result::Result<Window*, Error>;

  auto xdg_toplevel_configure(struct xdg_toplevel* xdg_toplevel,
			      int32_t width,
			      int32_t height,
			      struct wl_array* states) & noexcept -> void override;
};
#endif
} // namespace gl::egl
} // namespace engine::display::wayland

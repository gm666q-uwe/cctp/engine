#pragma once

#include <engine/display/display.hpp>

//#if ENGINE_HAS_EGL
#include <engine/graphics/gl/egl/display.hpp>
//#endif

#include "engine/display/wayland/display_options.hpp"
#include "engine/display/wayland/gl/egl/window.hpp"
#include "engine/display/wayland/vulkan/window.hpp"

namespace engine::display::wayland {
class Display final : public display::Display
{
  template<typename T>
  using wl_shared_ptr = std::shared_ptr<T>;

  template<typename T>
  using wl_unique_ptr = std::unique_ptr<T, std::function<void(T*)>>;

public:
  constexpr Display(Display const&) noexcept = delete;

  Display(Display&& other) noexcept;

  ~Display() noexcept override;

  constexpr auto operator=(Display const&) & noexcept -> Display& = delete;

  auto operator=(Display&& other) & noexcept -> Display&;

#if ENGINE_HAS_EGL
  [[nodiscard]] auto create_egl_window(WindowOptions const& options) const& noexcept
    -> result::Result<gl::egl::Window, Error>;

  [[nodiscard]] auto create_egl_window_pointer(WindowOptions const& options) const& noexcept
    -> result::Result<gl::egl::Window*, Error>;
#endif

#if ENGINE_HAS_VULKAN
  [[nodiscard]] auto create_vulkan_window(WindowOptions const& options) const& noexcept
  -> result::Result<vulkan::Window, Error>;

  [[nodiscard]] auto create_vulkan_window_pointer(WindowOptions const& options) const& noexcept
  -> result::Result<vulkan::Window*, Error>;
#endif

  [[nodiscard]] auto create_window_pointer(WindowOptions const& options) const& noexcept
    -> result::Result<display::Window*, Error> override;

  static auto new_(DisplayOptions const& options) noexcept -> result::Result<Display, Error>;

  static auto new_pointer(DisplayOptions const& options) noexcept -> result::Result<Display*, Error>;

  [[nodiscard]] auto run() & noexcept -> result::Result<bool, Error> override;

protected:
  wl_unique_ptr<struct wl_compositor> m_compositor = wl_unique_ptr<struct wl_compositor>();
  wl_shared_ptr<struct wl_cursor_theme> m_cursor_theme = wl_unique_ptr<struct wl_cursor_theme>();
  wl_unique_ptr<struct wl_display> m_display = wl_unique_ptr<struct wl_display>();
#if ENGINE_HAS_EGL
  std::unique_ptr<graphics::gl::egl::Display> m_egl_display = std::unique_ptr<graphics::gl::egl::Display>();
#endif
  wl_unique_ptr<struct wl_keyboard> m_keyboard = wl_unique_ptr<struct wl_keyboard>();
  struct wl_surface* m_keyboard_surface = nullptr;
  wl_unique_ptr<struct wl_output> m_output = wl_unique_ptr<struct wl_output>();
  wl_unique_ptr<struct wl_pointer> m_pointer = wl_unique_ptr<struct wl_pointer>();
  struct wl_surface* m_pointer_surface = nullptr;
  wl_unique_ptr<struct wl_registry> m_registry = wl_unique_ptr<struct wl_registry>();
  bool m_running = true;
  wl_unique_ptr<struct wl_seat> m_seat = wl_unique_ptr<struct wl_seat>();
  wl_unique_ptr<struct wl_shm> m_shm = wl_unique_ptr<struct wl_shm>();
  wl_shared_ptr<struct zwp_pointer_constraints_v1> m_wp_pointer_constraints =
    wl_shared_ptr<struct zwp_pointer_constraints_v1>();
  wl_unique_ptr<struct zwp_relative_pointer_v1> m_wp_relative_pointer = wl_unique_ptr<struct zwp_relative_pointer_v1>();
  wl_unique_ptr<struct zwp_relative_pointer_manager_v1> m_wp_relative_pointer_manager =
    wl_unique_ptr<struct zwp_relative_pointer_manager_v1>();
  wl_unique_ptr<struct zxdg_output_v1> m_xdg_output = wl_unique_ptr<struct zxdg_output_v1>();
  wl_unique_ptr<struct zxdg_output_manager_v1> m_xdg_output_manager = wl_unique_ptr<struct zxdg_output_manager_v1>();
  wl_unique_ptr<struct xdg_wm_base> m_xdg_wm_base = wl_unique_ptr<struct xdg_wm_base>();
  wl_unique_ptr<struct xkb_context> m_xkb_context = wl_unique_ptr<struct xkb_context>();
  wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>> m_xkb_keymap = std::shared_ptr<wl_unique_ptr<struct xkb_keymap>>();

  virtual auto keyboard_enter(struct wl_keyboard* wl_keyboard,
			      uint32_t serial,
			      struct wl_surface* surface,
			      struct wl_array* keys) & noexcept -> void;

  virtual auto keyboard_key(struct wl_keyboard* wl_keyboard,
			    uint32_t serial,
			    uint32_t time,
			    uint32_t key,
			    uint32_t state) & noexcept -> void;

  virtual auto keyboard_keymap(struct wl_keyboard* wl_keyboard, uint32_t format, int32_t fd, uint32_t size) & noexcept
    -> void;

  virtual auto keyboard_leave(struct wl_keyboard* wl_keyboard, uint32_t serial, struct wl_surface* surface) & noexcept
    -> void;

  virtual auto keyboard_modifiers(struct wl_keyboard* wl_keyboard,
				  uint32_t serial,
				  uint32_t mods_depressed,
				  uint32_t mods_latched,
				  uint32_t mods_locked,
				  uint32_t group) & noexcept -> void;

  virtual auto keyboard_repeat_info(struct wl_keyboard* wl_keyboard, int32_t rate, int32_t delay) & noexcept -> void;

  virtual auto output_done(struct wl_output* wl_output) & noexcept -> void;

  virtual auto output_geometry(struct wl_output* wl_output,
			       int32_t x,
			       int32_t y,
			       int32_t physical_width,
			       int32_t physical_height,
			       int32_t subpixel,
			       char const* make,
			       char const* model,
			       int32_t transform) & noexcept -> void;

  virtual auto output_mode(struct wl_output* wl_output,
			   uint32_t flags,
			   int32_t width,
			   int32_t height,
			   int32_t refresh) & noexcept -> void;

  virtual auto output_scale(struct wl_output* wl_output, int32_t factor) & noexcept -> void;

  virtual auto pointer_axis(struct wl_pointer* wl_pointer, uint32_t time, uint32_t axis, wl_fixed_t value) & noexcept
    -> void;

  virtual auto pointer_axis_discrete(struct wl_pointer* wl_pointer, uint32_t axis, int32_t discrete) & noexcept -> void;

  virtual auto pointer_axis_source(struct wl_pointer* wl_pointer, uint32_t axis_source) & noexcept -> void;

  virtual auto pointer_axis_stop(struct wl_pointer* wl_pointer, uint32_t time, uint32_t axis) & noexcept -> void;

  virtual auto pointer_button(struct wl_pointer* wl_pointer,
			      uint32_t serial,
			      uint32_t time,
			      uint32_t button,
			      uint32_t state) & noexcept -> void;

  virtual auto pointer_enter(struct wl_pointer* wl_pointer,
			     uint32_t serial,
			     struct wl_surface* surface,
			     wl_fixed_t surface_x,
			     wl_fixed_t surface_y) & noexcept -> void;

  virtual auto pointer_frame(struct wl_pointer* wl_pointer) & noexcept -> void;

  virtual auto pointer_leave(struct wl_pointer* wl_pointer, uint32_t serial, struct wl_surface* surface) & noexcept
    -> void;

  virtual auto pointer_motion(struct wl_pointer* wl_pointer,
			      uint32_t time,
			      wl_fixed_t surface_x,
			      wl_fixed_t surface_y) & noexcept -> void;

  virtual auto registry_global(struct wl_registry* wl_registry,
			       uint32_t name,
			       char const* interface,
			       uint32_t version) & noexcept -> void;

  virtual auto registry_global_remove(struct wl_registry* wl_registry, uint32_t name) & noexcept -> void;

  virtual auto seat_capabilities(struct wl_seat* wl_seat, uint32_t capabilities) & noexcept -> void;

  virtual auto seat_name(struct wl_seat* wl_seat, char const* name) & noexcept -> void;

  virtual auto shm_format(struct wl_shm* wl_shm, uint32_t format) & noexcept -> void;

  virtual auto wp_relative_pointer_relative_motion(struct zwp_relative_pointer_v1* wp_relative_pointer,
						   uint32_t utime_hi,
						   uint32_t utime_lo,
						   wl_fixed_t dx,
						   wl_fixed_t dy,
						   wl_fixed_t dx_unaccel,
						   wl_fixed_t dy_unaccel) & noexcept -> void;

  virtual auto xdg_output_description(struct zxdg_output_v1* xdg_output, char const* description) & noexcept -> void;

  virtual auto xdg_output_done(struct zxdg_output_v1* xdg_output) & noexcept -> void;

  virtual auto xdg_output_logical_position(struct zxdg_output_v1* xdg_output, int32_t x, int32_t y) & noexcept -> void;

  virtual auto xdg_output_logical_size(struct zxdg_output_v1* xdg_output, int32_t width, int32_t height) & noexcept
    -> void;

  virtual auto xdg_output_name(struct zxdg_output_v1* xdg_output, char const* name) & noexcept -> void;

  virtual auto xdg_wm_base_ping(struct xdg_wm_base* xdg_wm_base, uint32_t serial) & noexcept -> void;

private:
  explicit Display(DisplayOptions const& options) noexcept;

  auto drop() & noexcept -> void;

  auto initialize(char const* display_name) & noexcept -> option::Option<Error>;

  auto initialize_egl(bool pre_load) & noexcept -> option::Option<Error>;

  static auto keyboard_enter(void* data,
			     struct wl_keyboard* wl_keyboard,
			     uint32_t serial,
			     struct wl_surface* surface,
			     struct wl_array* keys) noexcept -> void;

  static auto keyboard_key(void* data,
			   struct wl_keyboard* wl_keyboard,
			   uint32_t serial,
			   uint32_t time,
			   uint32_t key,
			   uint32_t state) noexcept -> void;

  static auto keyboard_keymap(void* data,
			      struct wl_keyboard* wl_keyboard,
			      uint32_t format,
			      int32_t fd,
			      uint32_t size) noexcept -> void;

  static auto keyboard_leave(void* data,
			     struct wl_keyboard* wl_keyboard,
			     uint32_t serial,
			     struct wl_surface* surface) noexcept -> void;

  static auto keyboard_modifiers(void* data,
				 struct wl_keyboard* wl_keyboard,
				 uint32_t serial,
				 uint32_t mods_depressed,
				 uint32_t mods_latched,
				 uint32_t mods_locked,
				 uint32_t group) noexcept -> void;

  static auto keyboard_repeat_info(void* data, struct wl_keyboard* wl_keyboard, int32_t rate, int32_t delay) noexcept
    -> void;

  static constexpr struct wl_keyboard_listener const M_KEYBOARD_LISTENER = { keyboard_keymap,	 keyboard_enter,
									     keyboard_leave,	 keyboard_key,
									     keyboard_modifiers, keyboard_repeat_info };

  static auto output_done(void* data, struct wl_output* wl_output) noexcept -> void;

  static auto output_geometry(void* data,
			      struct wl_output* wl_output,
			      int32_t x,
			      int32_t y,
			      int32_t physical_width,
			      int32_t physical_height,
			      int32_t subpixel,
			      char const* make,
			      char const* model,
			      int32_t transform) noexcept -> void;

  static auto output_mode(void* data,
			  struct wl_output* wl_output,
			  uint32_t flags,
			  int32_t width,
			  int32_t height,
			  int32_t refresh) noexcept -> void;

  static auto output_scale(void* data, struct wl_output* wl_output, int32_t factor) noexcept -> void;

  static constexpr struct wl_output_listener const M_OUTPUT_LISTENER = { output_geometry,
									 output_mode,
									 output_done,
									 output_scale };

  static auto pointer_axis(void* data,
			   struct wl_pointer* wl_pointer,
			   uint32_t time,
			   uint32_t axis,
			   wl_fixed_t value) noexcept -> void;

  static auto pointer_axis_discrete(void* data, struct wl_pointer* wl_pointer, uint32_t axis, int32_t discrete) noexcept
    -> void;

  static auto pointer_axis_source(void* data, struct wl_pointer* wl_pointer, uint32_t axis_source) noexcept -> void;

  static auto pointer_axis_stop(void* data, struct wl_pointer* wl_pointer, uint32_t time, uint32_t axis) noexcept
    -> void;

  static auto pointer_button(void* data,
			     struct wl_pointer* wl_pointer,
			     uint32_t serial,
			     uint32_t time,
			     uint32_t button,
			     uint32_t state) noexcept -> void;

  static auto pointer_enter(void* data,
			    struct wl_pointer* wl_pointer,
			    uint32_t serial,
			    struct wl_surface* surface,
			    wl_fixed_t surface_x,
			    wl_fixed_t surface_y) noexcept -> void;

  static auto pointer_frame(void* data, struct wl_pointer* wl_pointer) noexcept -> void;

  static auto pointer_leave(void* data,
			    struct wl_pointer* wl_pointer,
			    uint32_t serial,
			    struct wl_surface* surface) noexcept -> void;

  static auto pointer_motion(void* data,
			     struct wl_pointer* wl_pointer,
			     uint32_t time,
			     wl_fixed_t surface_x,
			     wl_fixed_t surface_y) noexcept -> void;

  static constexpr struct wl_pointer_listener const M_POINTER_LISTENER = {
    pointer_enter, pointer_leave,	pointer_motion,	   pointer_button,	 pointer_axis,
    pointer_frame, pointer_axis_source, pointer_axis_stop, pointer_axis_discrete
  };

  static auto registry_global(void* data,
			      struct wl_registry* wl_registry,
			      uint32_t name,
			      char const* interface,
			      uint32_t version) noexcept -> void;

  static auto registry_global_remove(void* data, struct wl_registry* wl_registry, uint32_t name) noexcept -> void;

  static constexpr struct wl_registry_listener const M_REGISTRY_LISTENER = { registry_global, registry_global_remove };

  static auto seat_capabilities(void* data, struct wl_seat* wl_seat, uint32_t capabilities) noexcept -> void;

  static auto seat_name(void* data, struct wl_seat* wl_seat, char const* name) noexcept -> void;

  static constexpr struct wl_seat_listener const M_SEAT_LISTENER = { seat_capabilities, seat_name };

  static auto shm_format(void* data, struct wl_shm* wl_shm, uint32_t format) noexcept -> void;

  static constexpr struct wl_shm_listener const M_SHM_LISTENER = { shm_format };

  static auto wp_relative_pointer_relative_motion(void* data,
						  struct zwp_relative_pointer_v1* wp_relative_pointer,
						  uint32_t utime_hi,
						  uint32_t utime_lo,
						  wl_fixed_t dx,
						  wl_fixed_t dy,
						  wl_fixed_t dx_unaccel,
						  wl_fixed_t dy_unaccel) noexcept -> void;

  static constexpr struct zwp_relative_pointer_v1_listener const M_WP_RELATIVE_POINTER_LISTENER = {
    wp_relative_pointer_relative_motion
  };

  static auto xdg_output_description(void* data, struct zxdg_output_v1* xdg_output, char const* description) noexcept
    -> void;

  static auto xdg_output_done(void* data, struct zxdg_output_v1* xdg_output) noexcept -> void;

  static auto xdg_output_logical_position(void* data, struct zxdg_output_v1* xdg_output, int32_t x, int32_t y) noexcept
    -> void;

  static auto xdg_output_logical_size(void* data,
				      struct zxdg_output_v1* xdg_output,
				      int32_t width,
				      int32_t height) noexcept -> void;

  static auto xdg_output_name(void* data, struct zxdg_output_v1* xdg_output, char const* name) noexcept -> void;

  static constexpr struct zxdg_output_v1_listener const M_XDG_OUTPUT_LISTENER = { xdg_output_logical_position,
										  xdg_output_logical_size,
										  xdg_output_done,
										  xdg_output_name,
										  xdg_output_description };

  static auto xdg_wm_base_ping(void* data, struct xdg_wm_base* xdg_wm_base, uint32_t serial) noexcept -> void;

  static constexpr struct xdg_wm_base_listener const M_XDG_WM_BASE_LISTENER = { xdg_wm_base_ping };
};
} // namespace engine::display::wayland

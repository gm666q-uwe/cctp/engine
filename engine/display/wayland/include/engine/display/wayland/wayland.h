#pragma once

#include <engine/platform.h>

#include <wayland-client.h>
#include <wayland-cursor.h>
#include <wayland-egl.h>

//#include "wayland-fullscreen-shell-client-protocol.h"
//#include "wayland-idle-inhibit-client-protocol.h"
#include "wayland-pointer-constraints-client-protocol.h"
#include "wayland-relative-pointer-client-protocol.h"
//#include "wayland-tablet-client-protocol.h"
//#include "wayland-xdg-decoration-client-protocol.h"
#include "wayland-xdg-output-client-protocol.h"
#include "wayland-xdg-shell-client-protocol.h"

#include <xkbcommon/xkbcommon.h>

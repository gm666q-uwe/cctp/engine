#pragma once

#include <memory>

#include <engine/display/window.hpp>

#include "engine/display/wayland/input/manager.hpp"
//#include "engine/display/wayland/window_options.hpp"

namespace engine::display::wayland {
class Window : public display::Window
{
  friend class Display;

  template<typename T>
  using wl_shared_ptr = std::shared_ptr<T>;

  template<typename T>
  using wl_unique_ptr = std::unique_ptr<T, std::function<void(T*)>>;

public:
  constexpr Window(Window const&) noexcept = delete;

  Window(Window&& other) noexcept;

  ~Window() noexcept override;

  constexpr auto operator=(Window const&) & noexcept -> Window& = delete;

  auto operator=(Window&& other) & noexcept -> Window&;

  [[nodiscard]] auto graphics_context() const& noexcept -> graphics::GraphicsContext& override;

  [[nodiscard]] auto input_manager() const& noexcept -> engine::input::Manager& override;

  [[nodiscard]] auto run() & noexcept -> result::Result<bool, Error> override;

protected:
  struct wl_cursor_image* m_cursor_image = nullptr;
  wl_unique_ptr<struct wl_surface> m_cursor_surface = wl_unique_ptr<struct wl_surface>();
  wl_shared_ptr<struct wl_cursor_theme> m_cursor_theme = wl_shared_ptr<struct wl_cursor_theme>();
  std::unique_ptr<graphics::GraphicsContext> m_graphics_context = std::unique_ptr<graphics::GraphicsContext>();
  std::unique_ptr<input::Manager> m_input_manager = std::unique_ptr<input::Manager>();
  bool m_running = true;
  wl_unique_ptr<struct wl_surface> m_surface = wl_unique_ptr<struct wl_surface>();
  wl_unique_ptr<struct xdg_surface> m_xdg_surface = wl_unique_ptr<struct xdg_surface>();
  wl_unique_ptr<struct xdg_toplevel> m_xdg_toplevel = wl_unique_ptr<struct xdg_toplevel>();

  explicit Window(WindowOptions const& options, wl_shared_ptr<struct wl_cursor_theme> cursor_theme) noexcept;

  auto initialize(CStr app_id,
		  struct wl_compositor* compositor,
		  CStr title,
		  wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
		  struct xdg_wm_base* xdg_wm_base,
		  wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>> xkb_keymap) & noexcept -> option::Option<Error>;

  virtual auto keyboard_enter(struct wl_keyboard* wl_keyboard,
			      uint32_t serial,
			      struct wl_surface* surface,
			      struct wl_array* keys) & noexcept -> void;

  virtual auto keyboard_key(struct wl_keyboard* wl_keyboard,
			    uint32_t serial,
			    uint32_t time,
			    uint32_t key,
			    uint32_t state) & noexcept -> void;

  virtual auto keyboard_keymap(struct wl_keyboard* wl_keyboard, uint32_t format, int32_t fd, uint32_t size) & noexcept
    -> void;

  virtual auto keyboard_leave(struct wl_keyboard* wl_keyboard, uint32_t serial, struct wl_surface* surface) & noexcept
    -> void;

  virtual auto keyboard_modifiers(struct wl_keyboard* wl_keyboard,
				  uint32_t serial,
				  uint32_t mods_depressed,
				  uint32_t mods_latched,
				  uint32_t mods_locked,
				  uint32_t group) & noexcept -> void;

  virtual auto keyboard_repeat_info(struct wl_keyboard* wl_keyboard, int32_t rate, int32_t delay) & noexcept -> void;

  virtual auto pointer_axis(struct wl_pointer* wl_pointer, uint32_t time, uint32_t axis, wl_fixed_t value) & noexcept
    -> void;

  virtual auto pointer_axis_discrete(struct wl_pointer* wl_pointer, uint32_t axis, int32_t discrete) & noexcept -> void;

  virtual auto pointer_axis_source(struct wl_pointer* wl_pointer, uint32_t axis_source) & noexcept -> void;

  virtual auto pointer_axis_stop(struct wl_pointer* wl_pointer, uint32_t time, uint32_t axis) & noexcept -> void;

  virtual auto pointer_button(struct wl_pointer* wl_pointer,
			      uint32_t serial,
			      uint32_t time,
			      uint32_t button,
			      uint32_t state) & noexcept -> void;

  virtual auto pointer_enter(struct wl_pointer* wl_pointer,
			     uint32_t serial,
			     struct wl_surface* surface,
			     wl_fixed_t surface_x,
			     wl_fixed_t surface_y) & noexcept -> void;

  virtual auto pointer_frame(struct wl_pointer* wl_pointer) & noexcept -> void;

  virtual auto pointer_leave(struct wl_pointer* wl_pointer, uint32_t serial, struct wl_surface* surface) & noexcept
    -> void;

  virtual auto pointer_motion(struct wl_pointer* wl_pointer,
			      uint32_t time,
			      wl_fixed_t surface_x,
			      wl_fixed_t surface_y) & noexcept -> void;

  virtual auto surface_enter(struct wl_surface* wl_surface, struct wl_output* output) & noexcept -> void;

  virtual auto surface_leave(struct wl_surface* wl_surface, struct wl_output* output) & noexcept -> void;

  virtual auto wp_relative_pointer_relative_motion(struct zwp_relative_pointer_v1* wp_relative_pointer,
						   uint32_t utime_hi,
						   uint32_t utime_lo,
						   wl_fixed_t dx,
						   wl_fixed_t dy,
						   wl_fixed_t dx_unaccel,
						   wl_fixed_t dy_unaccel) & noexcept -> void;

  virtual auto xdg_surface_configure(struct xdg_surface* xdg_surface, uint32_t serial) & noexcept -> void;

  virtual auto xdg_toplevel_close(struct xdg_toplevel* xdg_toplevel) & noexcept -> void;

  virtual auto xdg_toplevel_configure(struct xdg_toplevel* xdg_toplevel,
				      int32_t width,
				      int32_t height,
				      struct wl_array* states) & noexcept -> void;

private:
  auto drop() & noexcept -> void;

  static auto surface_enter(void* data, struct wl_surface* wl_surface, struct wl_output* output) noexcept -> void;

  static auto surface_leave(void* data, struct wl_surface* wl_surface, struct wl_output* output) noexcept -> void;

  static constexpr struct wl_surface_listener const M_SURFACE_LISTENER{ surface_enter, surface_leave };

  static auto xdg_surface_configure(void* data, struct xdg_surface* xdg_surface, uint32_t serial) noexcept -> void;

  static constexpr struct xdg_surface_listener const M_XDG_SURFACE_LISTENER = { xdg_surface_configure };

  static auto xdg_toplevel_close(void* data, struct xdg_toplevel* xdg_toplevel) noexcept -> void;

  static auto xdg_toplevel_configure(void* data,
				     struct xdg_toplevel* xdg_toplevel,
				     int32_t width,
				     int32_t height,
				     struct wl_array* states) noexcept -> void;

  static constexpr struct xdg_toplevel_listener const M_XDG_TOPLEVEL_LISTENER = { xdg_toplevel_configure,
										  xdg_toplevel_close };
};
} // namespace engine::display::wayland

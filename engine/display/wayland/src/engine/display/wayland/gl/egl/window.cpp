#include "engine/display/wayland/gl/egl/window.hpp"

#if ENGINE_HAS_EGL
#include <engine/graphics/gl/graphics_context.hpp>

engine::display::wayland::gl::egl::Window::Window(
  const engine::display::WindowOptions& options,
  engine::display::wayland::gl::egl::Window::wl_shared_ptr<struct wl_cursor_theme> cursor_theme) noexcept
  : wayland::Window(options,
		    std::forward<wl_shared_ptr<struct wl_cursor_theme>>(cursor_theme))
{}

engine::display::wayland::gl::egl::Window::Window(engine::display::wayland::gl::egl::Window&& other) noexcept
  : wayland::Window(std::move(other))
  , m_egl(std::move(other.m_egl))
  , m_egl_window(std::move(other.m_egl_window))
{}

engine::display::wayland::gl::egl::Window::~Window() noexcept
{
  drop();
}

auto
engine::display::wayland::gl::egl::Window::operator=(engine::display::wayland::gl::egl::Window&& other) & noexcept
  -> engine::display::wayland::gl::egl::Window&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_egl = std::move(other.m_egl);
  m_egl_window = std::move(other.m_egl_window);

  wayland::Window::operator=(std::move(other));
  return *this;
}

auto
engine::display::wayland::gl::egl::Window::drop() & noexcept -> void
{
  m_graphics_context.reset();
  m_egl.reset();
  m_egl_window.reset();
}

auto
engine::display::wayland::gl::egl::Window::egl() const& noexcept -> engine::graphics::gl::egl::Context&
{
  return *m_egl;
}

auto
engine::display::wayland::gl::egl::Window::initialize_egl(
  engine::CStr app_id,
  struct wl_compositor* compositor,
  engine::graphics::gl::egl::Display const& egl_display,
  engine::CStr title,
  engine::display::wayland::gl::egl::Window::wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
  struct xdg_wm_base* xdg_wm_base,
  engine::display::wayland::gl::egl::Window::wl_shared_ptr<
    engine::display::wayland::gl::egl::Window::wl_unique_ptr<struct xkb_keymap>> xkb_keymap) & noexcept
  -> option::Option<engine::Error>
{
  using namespace graphics::gl::egl;

  if (auto result = initialize(app_id, compositor, title, wp_pointer_constraints, xdg_wm_base, xkb_keymap);
      result.is_some()) {
    return std::move(result);
  }

  auto* egl_config = static_cast<EGLConfig>(nullptr);
  if (auto result = egl_display.choose_config(); result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  } else {
    egl_config = std::move(result).unwrap();
  }

  m_egl_window = wl_unique_ptr<struct wl_egl_window>(wl_egl_window_create(m_surface.get(), m_width, m_height),
						     wl_egl_window_destroy);

  if (auto result = egl_display.create_context_pointer(egl_config, m_egl_window.get(), false); result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  } else {
    m_egl = std::unique_ptr<graphics::gl::egl::Context>(std::move(result).unwrap());
  }

  if (auto result = engine::graphics::gl::GraphicsContext::new_pointer(m_height, *m_egl, m_width); result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  } else {
    m_graphics_context = std::unique_ptr<graphics::gl::GraphicsContext>(std::move(result).unwrap());
  }

  return std::move(option::Option<Error>::None());
}

auto
engine::display::wayland::gl::egl::Window::new_(
  engine::display::WindowOptions const& options,
  engine::CStr app_id,
  struct wl_compositor* compositor,
  engine::display::wayland::gl::egl::Window::wl_shared_ptr<struct wl_cursor_theme> cursor_theme,
  engine::graphics::gl::egl::Display const& egl_display,
  engine::display::wayland::gl::egl::Window::wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
  struct xdg_wm_base* xdg_wm_base,
  engine::display::wayland::gl::egl::Window::wl_shared_ptr<
    engine::display::wayland::gl::egl::Window::wl_unique_ptr<struct xkb_keymap>> xkb_keymap) noexcept
  -> result::Result<Window, engine::Error>
{
  auto self = Window(options, std::forward<wl_shared_ptr<struct wl_cursor_theme>>(cursor_theme));
  if (auto result =
	self.initialize_egl(app_id,
			    compositor,
			    egl_display,
			    options.title(),
			    std::forward<wl_shared_ptr<struct zwp_pointer_constraints_v1>>(wp_pointer_constraints),
			    xdg_wm_base,
			    std::forward<wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>>>(xkb_keymap));
      result.is_some()) {
    return std::move(result::Result<Window, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Window, Error>::Ok(std::move(self)));
}

auto
engine::display::wayland::gl::egl::Window::new_pointer(
  engine::display::WindowOptions const& options,
  engine::CStr app_id,
  struct wl_compositor* compositor,
  engine::display::wayland::gl::egl::Window::wl_shared_ptr<struct wl_cursor_theme> cursor_theme,
  engine::graphics::gl::egl::Display const& egl_display,
  engine::display::wayland::gl::egl::Window::wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
  struct xdg_wm_base* xdg_wm_base,
  engine::display::wayland::gl::egl::Window::wl_shared_ptr<
    engine::display::wayland::gl::egl::Window::wl_unique_ptr<struct xkb_keymap>> xkb_keymap) noexcept
  -> result::Result<Window*, engine::Error>
{
  auto self =
    std::unique_ptr<Window>(new Window(options, std::forward<wl_shared_ptr<struct wl_cursor_theme>>(cursor_theme)));
  if (auto result =
	self->initialize_egl(app_id,
			     compositor,
			     egl_display,
			     options.title(),
			     std::forward<wl_shared_ptr<struct zwp_pointer_constraints_v1>>(wp_pointer_constraints),
			     xdg_wm_base,
			     std::forward<wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>>>(xkb_keymap));
      result.is_some()) {
    return std::move(result::Result<Window*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Window*, Error>::Ok(self.release()));
}

auto
engine::display::wayland::gl::egl::Window::xdg_toplevel_configure(struct xdg_toplevel* xdg_toplevel,
								  int32_t width,
								  int32_t height,
								  struct wl_array* states) & noexcept -> void
{
  wayland::Window::xdg_toplevel_configure(xdg_toplevel, width, height, states);
  if (m_height != 0 && m_width != 0) {
    wl_egl_window_resize(m_egl_window.get(), m_width, m_height, 0, 0);
  }
}
#endif

#include "engine/display/wayland/window.hpp"

#include <iostream>

#include "utility.hpp"

engine::display::wayland::Window::Window(
  const engine::display::WindowOptions& options,
  engine::display::wayland::Window::wl_shared_ptr<struct wl_cursor_theme> cursor_theme) noexcept
  : display::Window(options)
  , m_cursor_theme(std::forward<wl_shared_ptr<struct wl_cursor_theme>>(cursor_theme))
{}

engine::display::wayland::Window::Window(engine::display::wayland::Window&& other) noexcept
  : display::Window(std::move(other))
  , m_cursor_image(std::exchange(other.m_cursor_image, nullptr))
  , m_cursor_surface(std::move(other.m_cursor_surface))
  , m_cursor_theme(std::move(other.m_cursor_theme))
  , m_graphics_context(std::move(other.m_graphics_context))
  , m_input_manager(std::move(other.m_input_manager))
  , m_running(std::exchange(other.m_running, false))
  , m_surface(std::move(other.m_surface))
  , m_xdg_surface(std::move(other.m_xdg_surface))
  , m_xdg_toplevel(std::move(other.m_xdg_toplevel))
{
  if (m_surface) {
    wl_surface_set_user_data(m_surface.get(), this);
  }

  if (m_xdg_surface) {
    xdg_surface_set_user_data(m_xdg_surface.get(), this);
  }

  if (m_xdg_toplevel) {
    xdg_toplevel_set_user_data(m_xdg_toplevel.get(), this);
  }
}

engine::display::wayland::Window::~Window() noexcept
{
  drop();
}

auto
engine::display::wayland::Window::operator=(engine::display::wayland::Window&& other) & noexcept
  -> engine::display::wayland::Window&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_cursor_image = std::exchange(other.m_cursor_image, m_cursor_image);
  m_cursor_surface = std::move(other.m_cursor_surface);
  m_cursor_theme = std::move(other.m_cursor_theme);
  m_graphics_context = std::move(other.m_graphics_context);
  m_running = std::exchange(other.m_running, m_running);
  m_surface = std::move(other.m_surface);
  m_xdg_surface = std::move(other.m_xdg_surface);
  m_xdg_toplevel = std::move(other.m_xdg_toplevel);

  if (m_surface) {
    wl_surface_set_user_data(m_surface.get(), this);
  }

  if (m_xdg_surface) {
    xdg_surface_set_user_data(m_xdg_surface.get(), this);
  }

  if (m_xdg_toplevel) {
    xdg_toplevel_set_user_data(m_xdg_toplevel.get(), this);
  }

  display::Window::operator=(std::move(other));
  return *this;
}

auto
engine::display::wayland::Window::graphics_context() const& noexcept -> engine::graphics::GraphicsContext&
{
  return *m_graphics_context;
}

auto
engine::display::wayland::Window::drop() & noexcept -> void
{
  m_running = false;

  m_xdg_toplevel.reset();
  m_xdg_surface.reset();
  m_surface.reset();

  m_cursor_surface.reset();
  m_cursor_image = nullptr;
}

/*auto
engine::display::wayland::Window::gl() const& noexcept -> engine::graphics::gl::Context*
{
  return static_cast<graphics::gl::Context*>(m_egl->gl());
}*/

auto
engine::display::wayland::Window::initialize(
  CStr app_id,
  struct wl_compositor* compositor,
  CStr title,
  engine::display::wayland::Window::wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
  struct xdg_wm_base* xdg_wm_base,
  engine::display::wayland::Window::wl_shared_ptr<engine::display::wayland::Window::wl_unique_ptr<struct xkb_keymap>>
    xkb_keymap) & noexcept -> option::Option<engine::Error>
{
  auto* cursor = wl_cursor_theme_get_cursor(m_cursor_theme.get(), "left_ptr");
  m_cursor_image = cursor->images[0];
  m_cursor_surface = wl_unique_ptr<struct wl_surface>(wl_compositor_create_surface(compositor), wl_surface_destroy);
  wl_surface_attach(m_cursor_surface.get(), wl_cursor_image_get_buffer(m_cursor_image), 0, 0);
  wl_surface_commit(m_cursor_surface.get());

  m_surface = wl_unique_ptr<struct wl_surface>(wl_compositor_create_surface(compositor), wl_surface_destroy);
  wl_surface_add_listener(m_surface.get(), &M_SURFACE_LISTENER, this);
  m_xdg_surface =
    wl_unique_ptr<struct xdg_surface>(xdg_wm_base_get_xdg_surface(xdg_wm_base, m_surface.get()), xdg_surface_destroy);
  xdg_surface_add_listener(m_xdg_surface.get(), &M_XDG_SURFACE_LISTENER, this);

  m_xdg_toplevel =
    wl_unique_ptr<struct xdg_toplevel>(xdg_surface_get_toplevel(m_xdg_surface.get()), xdg_toplevel_destroy);
  xdg_toplevel_add_listener(m_xdg_toplevel.get(), &M_XDG_TOPLEVEL_LISTENER, this);
  xdg_toplevel_set_app_id(m_xdg_toplevel.get(), app_id.data());
  xdg_toplevel_set_title(m_xdg_toplevel.get(), title.data());

  wl_surface_commit(m_surface.get());

  m_input_manager = std::unique_ptr<input::Manager>(
    input::Manager::new_pointer(m_surface.get(),
				std::forward<wl_shared_ptr<struct zwp_pointer_constraints_v1>>(wp_pointer_constraints),
				std::forward<wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>>>(xkb_keymap)));

  return std::move(option::Option<Error>::None());
}

auto
engine::display::wayland::Window::input_manager() const& noexcept -> engine::input::Manager&
{
  return *m_input_manager;
}

auto
engine::display::wayland::Window::keyboard_enter(struct wl_keyboard* wl_keyboard,
						 uint32_t serial,
						 struct wl_surface* surface,
						 struct wl_array* keys) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Window][keyboard_enter]: serial " << serial << ", surface " << surface << '.'
	    << std::endl;
  m_input_manager->keyboard().keyboard_enter(wl_keyboard, serial, surface, keys);
}

auto
engine::display::wayland::Window::keyboard_key(struct wl_keyboard* wl_keyboard,
					       uint32_t serial,
					       uint32_t time,
					       uint32_t key,
					       uint32_t state) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Window][keyboard_key]: serial " << serial << ", time " << time << ", key "
	    << key << ", state " << state << '.' << std::endl;
  m_input_manager->keyboard().keyboard_key(wl_keyboard, serial, time, key, state);
}

auto
engine::display::wayland::Window::keyboard_keymap(struct wl_keyboard* wl_keyboard,
						  uint32_t format,
						  int32_t fd,
						  uint32_t size) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Window][keyboard_keymap]: format " << format << ", fd " << fd << ", size "
	    << size << '.' << std::endl;
  m_input_manager->keyboard().keyboard_keymap(wl_keyboard, format, fd, size);
}

auto
engine::display::wayland::Window::keyboard_leave(struct wl_keyboard* wl_keyboard,
						 uint32_t serial,
						 struct wl_surface* surface) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Window][keyboard_leave]: serial " << serial << ", surface " << surface << '.'
	    << std::endl;
  m_input_manager->keyboard().keyboard_leave(wl_keyboard, serial, surface);
}

auto
engine::display::wayland::Window::keyboard_modifiers(struct wl_keyboard* wl_keyboard,
						     uint32_t serial,
						     uint32_t mods_depressed,
						     uint32_t mods_latched,
						     uint32_t mods_locked,
						     uint32_t group) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Window][keyboard_modifiers]: serial " << serial << ", mods_depressed "
	    << mods_depressed << ", mods_latched " << mods_latched << ", mods_locked " << mods_locked << ", group "
	    << group << '.' << std::endl;
  m_input_manager->keyboard().keyboard_modifiers(wl_keyboard, serial, mods_depressed, mods_latched, mods_locked, group);
}

auto
engine::display::wayland::Window::keyboard_repeat_info(struct wl_keyboard* wl_keyboard,
						       int32_t rate,
						       int32_t delay) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Window][keyboard_repeat_info]: rate " << rate << ", delay " << delay << '.'
	    << std::endl;
  m_input_manager->keyboard().keyboard_repeat_info(wl_keyboard, rate, delay);
}

/*auto
engine::display::wayland::Window::make_current() const& noexcept -> result::Result<std::nullptr_t, std::int8_t>
{
  return std::move(m_egl->make_current());
}*/

auto
engine::display::wayland::Window::pointer_axis(struct wl_pointer* wl_pointer,
					       uint32_t time,
					       uint32_t axis,
					       wl_fixed_t value) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Window][pointer_axis]: time " << time << ", axis " << axis << ", value "
	    << value << '.' << std::endl;
  m_input_manager->mouse().pointer_axis(wl_pointer, time, axis, value);
}

auto
engine::display::wayland::Window::pointer_axis_discrete(struct wl_pointer* wl_pointer,
							uint32_t axis,
							int32_t discrete) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Window][pointer_axis_discrete]: axis " << axis << ", discrete " << discrete
	    << '.' << std::endl;
  m_input_manager->mouse().pointer_axis_discrete(wl_pointer, axis, discrete);
}

auto
engine::display::wayland::Window::pointer_axis_source(struct wl_pointer* wl_pointer, uint32_t axis_source) & noexcept
  -> void
{
  std::cout << "[engine::display::wayland][Window][pointer_axis_source]: axis_source " << axis_source << '.'
	    << std::endl;
  m_input_manager->mouse().pointer_axis_source(wl_pointer, axis_source);
}

auto
engine::display::wayland::Window::pointer_axis_stop(struct wl_pointer* wl_pointer,
						    uint32_t time,
						    uint32_t axis) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Window][pointer_axis_stop]: time " << time << ", axis " << axis << '.'
	    << std::endl;
  m_input_manager->mouse().pointer_axis_stop(wl_pointer, time, axis);
}

auto
engine::display::wayland::Window::pointer_button(struct wl_pointer* wl_pointer,
						 uint32_t serial,
						 uint32_t time,
						 uint32_t button,
						 uint32_t state) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Window][pointer_button]: serial " << serial << ", time " << time
	    << ", button " << button << ", state " << state << '.' << std::endl;
  m_input_manager->mouse().pointer_button(wl_pointer, serial, time, button, state);
}

auto
engine::display::wayland::Window::pointer_enter(struct wl_pointer* wl_pointer,
						uint32_t serial,
						struct wl_surface* surface,
						wl_fixed_t surface_x,
						wl_fixed_t surface_y) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Window][pointer_enter]: serial " << serial << ", surface " << surface
	    << ", surface_x " << wl_fixed_to_double(surface_x) << ", surface_y " << wl_fixed_to_double(surface_y) << '.'
	    << std::endl;
  wl_pointer_set_cursor(wl_pointer,
			serial,
			m_cursor_surface.get(),
			static_cast<int32_t>(m_cursor_image->hotspot_x),
			static_cast<int32_t>(m_cursor_image->hotspot_y));
  m_input_manager->mouse().pointer_enter(wl_pointer, serial, surface, surface_x, surface_y);
}

auto
engine::display::wayland::Window::pointer_frame(struct wl_pointer* wl_pointer) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Window][pointer_frame]: ." << std::endl;
  m_input_manager->mouse().pointer_frame(wl_pointer);
}

auto
engine::display::wayland::Window::pointer_leave(struct wl_pointer* wl_pointer,
						uint32_t serial,
						struct wl_surface* surface) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Window][pointer_leave]: serial " << serial << ", surface " << surface << '.'
	    << std::endl;
  m_input_manager->mouse().pointer_leave(wl_pointer, serial, surface);
}

auto
engine::display::wayland::Window::pointer_motion(struct wl_pointer* wl_pointer,
						 uint32_t time,
						 wl_fixed_t surface_x,
						 wl_fixed_t surface_y) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Window][pointer_motion]: time " << time << ", surface_x "
	    << wl_fixed_to_double(surface_x) << ", surface_y " << wl_fixed_to_double(surface_y) << '.' << std::endl;
  m_input_manager->mouse().pointer_motion(wl_pointer, time, surface_x, surface_y);
}

/*auto
engine::display::wayland::Window::release_current() const& noexcept -> result::Result<std::nullptr_t, std::int8_t>
{
  return std::move(m_egl->release_current());
}*/

auto
engine::display::wayland::Window::run() & noexcept -> result::Result<bool, engine::Error>
{
  return std::move(result::Result<bool, Error>::Ok(m_running));
}

auto
engine::display::wayland::Window::surface_enter(struct wl_surface* wl_surface, struct wl_output* output) & noexcept
  -> void
{}

auto
engine::display::wayland::Window::surface_enter(void* data,
						struct wl_surface* wl_surface,
						struct wl_output* output) noexcept -> void
{
  static_cast<Window*>(data)->surface_enter(wl_surface, output);
}

auto
engine::display::wayland::Window::surface_leave(struct wl_surface* wl_surface, struct wl_output* output) & noexcept
  -> void
{}

auto
engine::display::wayland::Window::surface_leave(void* data,
						struct wl_surface* wl_surface,
						struct wl_output* output) noexcept -> void
{
  static_cast<Window*>(data)->surface_leave(wl_surface, output);
}

/*auto
engine::display::wayland::Window::swap_buffers() const& noexcept -> result::Result<std::nullptr_t, std::int8_t>
{
  const_cast<Window*>(this)->m_size_changed = false;
  return std::move(m_egl->swap_buffers());
}*/

auto
engine::display::wayland::Window::wp_relative_pointer_relative_motion(
  struct zwp_relative_pointer_v1* wp_relative_pointer,
  uint32_t utime_hi,
  uint32_t utime_lo,
  wl_fixed_t dx,
  wl_fixed_t dy,
  wl_fixed_t dx_unaccel,
  wl_fixed_t dy_unaccel) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Window][wp_relative_pointer_relative_motion]: utime_hi " << utime_hi
	    << ", utime_lo " << utime_lo << ", dx " << wl_fixed_to_double(dx) << ", dy " << wl_fixed_to_double(dy)
	    << ", dx_unaccel " << wl_fixed_to_double(dx_unaccel) << ", dy_unaccel " << wl_fixed_to_double(dy_unaccel)
	    << '.' << std::endl;
  m_input_manager->mouse().wp_relative_pointer_relative_motion(
    wp_relative_pointer, utime_hi, utime_lo, dx, dy, dx_unaccel, dy_unaccel);
}

auto
engine::display::wayland::Window::xdg_surface_configure(struct xdg_surface* xdg_surface, uint32_t serial) & noexcept
  -> void
{
  xdg_surface_ack_configure(xdg_surface, serial);
}

auto
engine::display::wayland::Window::xdg_surface_configure(void* data,
							struct xdg_surface* xdg_surface,
							uint32_t serial) noexcept -> void
{
  static_cast<Window*>(data)->xdg_surface_configure(xdg_surface, serial);
}

auto
engine::display::wayland::Window::xdg_toplevel_close(struct xdg_toplevel* xdg_toplevel) & noexcept -> void
{
  m_running = false;
}

auto
engine::display::wayland::Window::xdg_toplevel_close(void* data, struct xdg_toplevel* xdg_toplevel) noexcept -> void
{
  static_cast<Window*>(data)->xdg_toplevel_close(xdg_toplevel);
}

auto
engine::display::wayland::Window::xdg_toplevel_configure(struct xdg_toplevel* xdg_toplevel,
							 int32_t width,
							 int32_t height,
							 struct wl_array* states) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Window][xdg_toplevel_configure]: width " << width << ", height " << height
	    << '.' << std::endl;
  // array_for_each<xdg_toplevel_state>(states, [&](auto* state) { std::cout << '\t' << *state << std::endl; });
  if (height != m_height || width != m_width) {
    m_height = height;
    m_width = width;
    if (m_height != 0 && m_width != 0) {
      m_graphics_context->on_resize(m_height, m_width);
    }
  }
}

auto
engine::display::wayland::Window::xdg_toplevel_configure(void* data,
							 struct xdg_toplevel* xdg_toplevel,
							 int32_t width,
							 int32_t height,
							 struct wl_array* states) noexcept -> void
{
  static_cast<Window*>(data)->xdg_toplevel_configure(xdg_toplevel, width, height, states);
}

#include "engine/display/wayland/display_options.hpp"

engine::display::wayland::DisplayOptions::DisplayOptions(engine::display::wayland::DisplayOptions&& other) noexcept
  : display::DisplayOptions(std::move(other))
  , m_display_name(other.m_display_name)
{}

auto
engine::display::wayland::DisplayOptions::operator=(engine::display::wayland::DisplayOptions&& other) & noexcept
  -> engine::display::wayland::DisplayOptions&
{
  if (this == &other) {
    return *this;
  }

  m_display_name = other.m_display_name;

  display::DisplayOptions::operator=(std::move(other));
  return *this;
}

auto
engine::display::wayland::DisplayOptions::display_name(char const* display_name) && noexcept
  -> engine::display::wayland::DisplayOptions
{
  m_display_name = display_name;

  return std::move(*this);
}

auto
engine::display::wayland::DisplayOptions::new_() noexcept -> engine::display::wayland::DisplayOptions
{
  return std::move(DisplayOptions());
}

auto
engine::display::wayland::DisplayOptions::new_pointer() noexcept -> engine::display::wayland::DisplayOptions*
{
  return new DisplayOptions();
}

#include "engine/display/wayland/input/mouse.hpp"

engine::display::wayland::input::Mouse::Mouse(
  struct wl_surface* surface,
  engine::display::wayland::input::Mouse::wl_shared_ptr<struct zwp_pointer_constraints_v1>
    wp_pointer_constraints) noexcept
  : engine::input::Mouse()
  , m_surface(std::forward<struct wl_surface*>(surface))
  , m_wp_pointer_constraints(std::forward<wl_shared_ptr<struct zwp_pointer_constraints_v1>>(wp_pointer_constraints))
{}

engine::display::wayland::input::Mouse::Mouse(engine::display::wayland::input::Mouse&& other) noexcept
  : engine::input::Mouse(std::move(other))
  , m_surface(std::exchange(other.m_surface, nullptr))
  , m_pointer(std::exchange(other.m_pointer, nullptr))
  , m_wp_confined_pointer(std::move(other.m_wp_confined_pointer))
  , m_wp_locked_pointer(std::move(other.m_wp_locked_pointer))
  , m_wp_pointer_constraints(std::move(other.m_wp_pointer_constraints))
{
  if (m_wp_confined_pointer) {
    zwp_confined_pointer_v1_set_user_data(m_wp_confined_pointer.get(), this);
  }

  if (m_wp_locked_pointer) {
    zwp_locked_pointer_v1_set_user_data(m_wp_locked_pointer.get(), this);
  }
}

engine::display::wayland::input::Mouse::~Mouse() noexcept
{
  drop();
}

auto
engine::display::wayland::input::Mouse::operator=(engine::display::wayland::input::Mouse&& other) & noexcept
  -> engine::display::wayland::input::Mouse&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_surface = std::exchange(other.m_surface, m_surface);
  m_pointer = std::exchange(other.m_pointer, m_pointer);
  m_wp_confined_pointer = std::move(other.m_wp_confined_pointer);
  m_wp_locked_pointer = std::move(other.m_wp_locked_pointer);
  m_wp_pointer_constraints = std::move(other.m_wp_pointer_constraints);

  if (m_wp_confined_pointer) {
    zwp_confined_pointer_v1_set_user_data(m_wp_confined_pointer.get(), this);
  }

  if (m_wp_locked_pointer) {
    zwp_locked_pointer_v1_set_user_data(m_wp_locked_pointer.get(), this);
  }

  engine::input::Mouse::operator=(std::move(other));
  return *this;
}

auto
engine::display::wayland::input::Mouse::begin_update() & noexcept -> void
{}

auto
engine::display::wayland::input::Mouse::confine() & noexcept -> void
{
  m_wp_confined_pointer = wl_unique_ptr<struct zwp_confined_pointer_v1>(
    zwp_pointer_constraints_v1_confine_pointer(
      m_wp_pointer_constraints.get(), m_surface, m_pointer, nullptr, ZWP_POINTER_CONSTRAINTS_V1_LIFETIME_PERSISTENT),
    zwp_confined_pointer_v1_destroy);
  // zwp_confined_pointer_v1_add_listener(m_wp_confined_pointer.get(), , this);
}

auto
engine::display::wayland::input::Mouse::drop() & noexcept -> void
{
  m_surface = nullptr;
  m_pointer = nullptr;
}

auto
engine::display::wayland::input::Mouse::end_update() & noexcept -> void
{}

auto
engine::display::wayland::input::Mouse::initialize() & noexcept -> void
{}

auto
engine::display::wayland::input::Mouse::lock() & noexcept -> void
{
  m_wp_locked_pointer = wl_unique_ptr<struct zwp_locked_pointer_v1>(
    zwp_pointer_constraints_v1_lock_pointer(
      m_wp_pointer_constraints.get(), m_surface, m_pointer, nullptr, ZWP_POINTER_CONSTRAINTS_V1_LIFETIME_PERSISTENT),
    zwp_locked_pointer_v1_destroy);
  // zwp_locked_pointer_v1_add_listener(m_wp_locked_pointer.get(), , this);
}

auto
engine::display::wayland::input::Mouse::new_(
  struct wl_surface* surface,
  engine::display::wayland::input::Mouse::wl_shared_ptr<struct zwp_pointer_constraints_v1>
    wp_pointer_constraints) noexcept -> engine::display::wayland::input::Mouse
{
  auto self = Mouse(std::forward<struct wl_surface*>(surface),
		    std::forward<wl_shared_ptr<struct zwp_pointer_constraints_v1>>(wp_pointer_constraints));
  self.initialize();
  return std::move(self);
  /*if (auto result = self.initialize(); result.is_some()) {
    return std::move(result::Result<Mouse, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Mouse, Error>::Ok(std::move(self)));*/
}

auto
engine::display::wayland::input::Mouse::new_pointer(
  struct wl_surface* surface,
  engine::display::wayland::input::Mouse::wl_shared_ptr<struct zwp_pointer_constraints_v1>
    wp_pointer_constraints) noexcept -> engine::display::wayland::input::Mouse*
{
  auto self = std::unique_ptr<Mouse>(
    new Mouse(std::forward<struct wl_surface*>(surface),
	      std::forward<wl_shared_ptr<struct zwp_pointer_constraints_v1>>(wp_pointer_constraints)));
  self->initialize();
  return self.release();
  /*if (auto result = self->initialize(); result.is_some()) {
    return std::move(result::Result<Mouse*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Mouse*, Error>::Ok(self.release()));*/
}

auto
engine::display::wayland::input::Mouse::pointer_axis(struct wl_pointer* wl_pointer,
						     uint32_t time,
						     uint32_t axis,
						     wl_fixed_t value) & noexcept -> void
{}

auto
engine::display::wayland::input::Mouse::pointer_axis_discrete(struct wl_pointer* wl_pointer,
							      uint32_t axis,
							      int32_t discrete) & noexcept -> void
{}

auto
engine::display::wayland::input::Mouse::pointer_axis_source(struct wl_pointer* wl_pointer,
							    uint32_t axis_source) & noexcept -> void
{}

auto
engine::display::wayland::input::Mouse::pointer_axis_stop(struct wl_pointer* wl_pointer,
							  uint32_t time,
							  uint32_t axis) & noexcept -> void
{}

auto
engine::display::wayland::input::Mouse::pointer_button(struct wl_pointer* wl_pointer,
						       uint32_t serial,
						       uint32_t time,
						       uint32_t button,
						       uint32_t state) & noexcept -> void
{
  m_events.emplace(static_cast<engine::input::Key>(button), state == WL_POINTER_BUTTON_STATE_PRESSED ? 1.0f : 0.0f);
}

auto
engine::display::wayland::input::Mouse::pointer_enter(struct wl_pointer* wl_pointer,
						      uint32_t serial,
						      struct wl_surface* surface,
						      wl_fixed_t surface_x,
						      wl_fixed_t surface_y) & noexcept -> void
{
  m_pointer = wl_pointer;
}

auto
engine::display::wayland::input::Mouse::pointer_frame(struct wl_pointer* wl_pointer) & noexcept -> void
{}

auto
engine::display::wayland::input::Mouse::pointer_leave(struct wl_pointer* wl_pointer,
						      uint32_t serial,
						      struct wl_surface* surface) & noexcept -> void
{}

auto
engine::display::wayland::input::Mouse::pointer_motion(struct wl_pointer* wl_pointer,
						       uint32_t time,
						       wl_fixed_t surface_x,
						       wl_fixed_t surface_y) & noexcept -> void
{}

auto
engine::display::wayland::input::Mouse::wp_confined_pointer_confined(
  struct zwp_confined_pointer_v1* wp_confined_pointer) & noexcept -> void
{}

auto
engine::display::wayland::input::Mouse::wp_confined_pointer_confined(
  void* data,
  struct zwp_confined_pointer_v1* wp_confined_pointer) noexcept -> void
{
  static_cast<Mouse*>(data)->wp_confined_pointer_confined(wp_confined_pointer);
}

auto
engine::display::wayland::input::Mouse::wp_confined_pointer_unconfined(
  struct zwp_confined_pointer_v1* wp_confined_pointer) & noexcept -> void
{}

auto
engine::display::wayland::input::Mouse::wp_confined_pointer_unconfined(
  void* data,
  struct zwp_confined_pointer_v1* wp_confined_pointer) noexcept -> void
{
  static_cast<Mouse*>(data)->wp_confined_pointer_unconfined(wp_confined_pointer);
}

auto
engine::display::wayland::input::Mouse::wp_locked_pointer_locked(struct zwp_locked_pointer_v1* wp_locked_pointer)
  -> void
{}

auto
engine::display::wayland::input::Mouse::wp_locked_pointer_locked(void* data,
								 struct zwp_locked_pointer_v1* wp_locked_pointer)
  -> void
{
  static_cast<Mouse*>(data)->wp_locked_pointer_locked(wp_locked_pointer);
}

auto
engine::display::wayland::input::Mouse::wp_locked_pointer_unlocked(struct zwp_locked_pointer_v1* wp_locked_pointer)
  -> void
{}

auto
engine::display::wayland::input::Mouse::wp_locked_pointer_unlocked(void* data,
								   struct zwp_locked_pointer_v1* wp_locked_pointer)
  -> void
{
  static_cast<Mouse*>(data)->wp_locked_pointer_unlocked(wp_locked_pointer);
}

auto
engine::display::wayland::input::Mouse::wp_relative_pointer_relative_motion(
  struct zwp_relative_pointer_v1* wp_relative_pointer,
  uint32_t utime_hi,
  uint32_t utime_lo,
  wl_fixed_t dx,
  wl_fixed_t dy,
  wl_fixed_t dx_unaccel,
  wl_fixed_t dy_unaccel) & noexcept -> void
{
  m_events.emplace(engine::input::RelativeAxis::LeftX, dx_unaccel);
  m_events.emplace(engine::input::RelativeAxis::LeftY, dy_unaccel);
}

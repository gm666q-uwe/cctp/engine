#include "engine/display/wayland/input/keyboard.hpp"

#include "../utility.hpp"

engine::display::wayland::input::Keyboard::Keyboard(
  engine::display::wayland::input::Keyboard::wl_shared_ptr<
    engine::display::wayland::input::Keyboard::wl_unique_ptr<struct xkb_keymap>> xkb_keymap) noexcept
  : engine::input::Keyboard()
  , m_xkb_keymap(std::forward<wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>>>(xkb_keymap))
{}

engine::display::wayland::input::Keyboard::Keyboard(engine::display::wayland::input::Keyboard&& other) noexcept
  : engine::input::Keyboard(std::move(other))
  , m_xkb_keymap(std::move(other.m_xkb_keymap))
  , m_xkb_state(std::move(other.m_xkb_state))
{}

engine::display::wayland::input::Keyboard::~Keyboard() noexcept
{
  drop();
}

auto
engine::display::wayland::input::Keyboard::operator=(engine::display::wayland::input::Keyboard&& other) & noexcept
  -> engine::display::wayland::input::Keyboard&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_xkb_keymap = std::move(other.m_xkb_keymap);
  m_xkb_state = std::move(other.m_xkb_state);

  engine::input::Keyboard::operator=(std::move(other));
  return *this;
}

auto
engine::display::wayland::input::Keyboard::begin_update() & noexcept -> void
{}

auto
engine::display::wayland::input::Keyboard::characters() const& noexcept -> engine::CStr
{
  return m_characters;
}

auto
engine::display::wayland::input::Keyboard::drop() & noexcept -> void
{
  m_characters.clear();
}

auto
engine::display::wayland::input::Keyboard::end_update() & noexcept -> void
{
  m_characters.clear();
}

auto
engine::display::wayland::input::Keyboard::keyboard_enter(struct wl_keyboard* wl_keyboard,
							  uint32_t serial,
							  struct wl_surface* surface,
							  struct wl_array* keys) & noexcept -> void
{
  if (m_xkb_keymap->get() != xkb_state_get_keymap(m_xkb_state.get())) {
    m_xkb_state = wl_unique_ptr<struct xkb_state>(xkb_state_new(m_xkb_keymap->get()), xkb_state_unref);
  }

  array_for_each<uint32_t>(keys, [&](auto* key) {
    m_events.emplace(static_cast<engine::input::Key>(*key), 1.0f);

    /*char buf[128];
    //auto sym = xkb_state_key_get_one_sym(m_xkb_state.get(), *key + 8);
    //xkb_keysym_get_name(sym, buf, sizeof(buf));
    xkb_state_key_get_utf8(m_xkb_state.get(), *key + 8, buf, sizeof(buf));
    m_characters.append(buf);*/
  });
}

auto
engine::display::wayland::input::Keyboard::keyboard_key(struct wl_keyboard* wl_keyboard,
							uint32_t serial,
							uint32_t time,
							uint32_t key,
							uint32_t state) & noexcept -> void
{
  m_events.emplace(static_cast<engine::input::Key>(key), state == WL_KEYBOARD_KEY_STATE_PRESSED ? 1.0f : 0.0f);

  /*char buf[128];
  //auto sym = xkb_state_key_get_one_sym(m_xkb_state.get(), key + 8);
  //xkb_keysym_get_name(sym, buf, sizeof(buf));
  xkb_state_key_get_utf8(m_xkb_state.get(), key + 8, buf, sizeof(buf));
  m_characters.append(buf);*/
}

auto
engine::display::wayland::input::Keyboard::keyboard_keymap(struct wl_keyboard* wl_keyboard,
							   uint32_t format,
							   int32_t fd,
							   uint32_t size) & noexcept -> void
{
  m_xkb_state = wl_unique_ptr<struct xkb_state>(xkb_state_new(m_xkb_keymap->get()), xkb_state_unref);
}

auto
engine::display::wayland::input::Keyboard::keyboard_leave(struct wl_keyboard* wl_keyboard,
							  uint32_t serial,
							  struct wl_surface* surface) & noexcept -> void
{}

auto
engine::display::wayland::input::Keyboard::keyboard_modifiers(struct wl_keyboard* wl_keyboard,
							      uint32_t serial,
							      uint32_t mods_depressed,
							      uint32_t mods_latched,
							      uint32_t mods_locked,
							      uint32_t group) & noexcept -> void
{
  xkb_state_update_mask(m_xkb_state.get(), mods_depressed, mods_latched, mods_locked, 0, 0, group);
}

auto
engine::display::wayland::input::Keyboard::keyboard_repeat_info(struct wl_keyboard* wl_keyboard,
								int32_t rate,
								int32_t delay) & noexcept -> void
{}

auto
engine::display::wayland::input::Keyboard::initialize() & noexcept -> void
{
  if (m_xkb_keymap->operator bool()) {
    m_xkb_state = wl_unique_ptr<struct xkb_state>(xkb_state_new(m_xkb_keymap->get()), xkb_state_unref);
  }
}

auto
engine::display::wayland::input::Keyboard::new_(
  engine::display::wayland::input::Keyboard::wl_shared_ptr<
    engine::display::wayland::input::Keyboard::wl_unique_ptr<xkb_keymap>> xkb_keymap) noexcept
  -> engine::display::wayland::input::Keyboard
{
  auto self = Keyboard(std::forward<wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>>>(xkb_keymap));
  self.initialize();
  return std::move(self);
  /*if (auto result = self.initialize(); result.is_some()) {
    return std::move(result::Result<Keyboard, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Keyboard, Error>::Ok(std::move(self)));*/
}

auto
engine::display::wayland::input::Keyboard::new_pointer(
  engine::display::wayland::input::Keyboard::wl_shared_ptr<
    engine::display::wayland::input::Keyboard::wl_unique_ptr<xkb_keymap>> xkb_keymap) noexcept
  -> engine::display::wayland::input::Keyboard*
{
  auto self =
    std::unique_ptr<Keyboard>(new Keyboard(std::forward<wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>>>(xkb_keymap)));
  self->initialize();
  return self.release();
  /*if (auto result = self->initialize(); result.is_some()) {
    return std::move(result::Result<Keyboard*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Keyboard*, Error>::Ok(self.release()));*/
}

#include "engine/display/wayland/input/manager.hpp"

engine::display::wayland::input::Manager::Manager(
  struct wl_surface* surface,
  engine::display::wayland::input::Manager::wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
  engine::display::wayland::input::Manager::wl_shared_ptr<
    engine::display::wayland::input::Manager::wl_unique_ptr<struct xkb_keymap>> xkb_keymap) noexcept
  : engine::input::Manager({ &m_keyboard, &m_mouse })
  , m_keyboard(Keyboard::new_(std::forward<wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>>>(xkb_keymap)))
  , m_mouse(Mouse::new_(std::forward<struct wl_surface*>(surface),
			std::forward<wl_shared_ptr<struct zwp_pointer_constraints_v1>>(wp_pointer_constraints)))
{}

// engine::display::wayland::input::Manager::Manager(engine::display::wayland::input::Manager&&) noexcept {}

engine::display::wayland::input::Manager::~Manager() noexcept
{
  drop();
}

/*auto
engine::display::wayland::input::Manager::operator=(engine::display::wayland::input::Manager&&) & noexcept
  -> engine::display::wayland::input::Manager&
{
  return <#initializer #>;
}*/

auto
engine::display::wayland::input::Manager::drop() & noexcept -> void
{}

auto
engine::display::wayland::input::Manager::initialize() & noexcept -> void
{}

auto
engine::display::wayland::input::Manager::keyboard() & noexcept -> engine::display::wayland::input::Keyboard&
{
  return m_keyboard;
}

auto
engine::display::wayland::input::Manager::keyboard() const& noexcept -> engine::input::Keyboard const&
{
  return m_keyboard;
}

auto
engine::display::wayland::input::Manager::mouse() & noexcept -> engine::display::wayland::input::Mouse&
{
  return m_mouse;
}

auto
engine::display::wayland::input::Manager::mouse() const& noexcept -> engine::input::Mouse const&
{
  return m_mouse;
}

auto
engine::display::wayland::input::Manager::new_(
  struct wl_surface* surface,
  engine::display::wayland::input::Manager::wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
  engine::display::wayland::input::Manager::wl_shared_ptr<
    engine::display::wayland::input::Manager::wl_unique_ptr<struct xkb_keymap>> xkb_keymap) noexcept
  -> engine::display::wayland::input::Manager
{
  auto self = Manager(surface,
		      std::forward<wl_shared_ptr<struct zwp_pointer_constraints_v1>>(wp_pointer_constraints),
		      std::forward<wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>>>(xkb_keymap));
  self.initialize();
  return std::move(self);
  /*if (auto result = self.initialize(); result.is_some()) {
    return std::move(result::Result<Manager, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Manager, Error>::Ok(std::move(self)));*/
}

auto
engine::display::wayland::input::Manager::new_pointer(
  struct wl_surface* surface,
  engine::display::wayland::input::Manager::wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
  engine::display::wayland::input::Manager::wl_shared_ptr<
    engine::display::wayland::input::Manager::wl_unique_ptr<struct xkb_keymap>> xkb_keymap) noexcept
  -> engine::display::wayland::input::Manager*
{
  auto self = std::unique_ptr<Manager>(
    new Manager(surface,
		std::forward<wl_shared_ptr<struct zwp_pointer_constraints_v1>>(wp_pointer_constraints),
		std::forward<wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>>>(xkb_keymap)));
  self->initialize();
  return self.release();
  /*if (auto result = self->initialize(); result.is_some()) {
    return std::move(result::Result<Manager*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Manager*, Error>::Ok(self.release()));*/
}

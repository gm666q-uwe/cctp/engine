#include "engine/display/wayland/window_options.hpp"

engine::display::wayland::WindowOptions::WindowOptions(engine::display::wayland::WindowOptions&& other) noexcept
  : display::WindowOptions(std::move(other))
{}

auto
engine::display::wayland::WindowOptions::operator=(engine::display::wayland::WindowOptions&& other) & noexcept
  -> engine::display::wayland::WindowOptions&
{
  if (this == &display::WindowOptions::operator=(std::move(other))) {
    return *this;
  }

  return *this;
}

auto
engine::display::wayland::WindowOptions::new_() noexcept -> engine::display::wayland::WindowOptions
{
  return std::move(WindowOptions());
}

auto
engine::display::wayland::WindowOptions::new_pointer() noexcept -> engine::display::wayland::WindowOptions*
{
  return new WindowOptions();
}

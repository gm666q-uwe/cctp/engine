#include "engine/display/wayland/vulkan/window.hpp"

#if ENGINE_HAS_VULKAN
#include <engine/graphics/vulkan/graphics_context.hpp>

engine::display::wayland::vulkan::Window::Window(
  const engine::display::WindowOptions& options,
  engine::display::wayland::vulkan::Window::wl_shared_ptr<struct wl_cursor_theme> cursor_theme) noexcept
  : wayland::Window(options, std::forward<wl_shared_ptr<struct wl_cursor_theme>>(cursor_theme))
{}

engine::display::wayland::vulkan::Window::Window(engine::display::wayland::vulkan::Window&& other) noexcept
  : wayland::Window(std::move(other))
{}

engine::display::wayland::vulkan::Window::~Window() noexcept
{
  drop();
}

auto
engine::display::wayland::vulkan::Window::operator=(engine::display::wayland::vulkan::Window&& other) & noexcept
  -> engine::display::wayland::vulkan::Window&
{
  if (this == &other) {
    return *this;
  }

  drop();

  wayland::Window::operator=(std::move(other));
  return *this;
}

auto
engine::display::wayland::vulkan::Window::drop() & noexcept -> void
{
  m_graphics_context.reset();
}

auto
engine::display::wayland::vulkan::Window::initialize_vulkan(
  engine::CStr app_id,
  struct wl_compositor* compositor,
  struct wl_display* display,
  engine::CStr title,
  engine::display::wayland::vulkan::Window::wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
  struct xdg_wm_base* xdg_wm_base,
  engine::display::wayland::vulkan::Window::wl_shared_ptr<
    engine::display::wayland::vulkan::Window::wl_unique_ptr<struct xkb_keymap>> xkb_keymap) & noexcept
  -> option::Option<engine::Error>
{
  if (auto result = initialize(app_id, compositor, title, wp_pointer_constraints, xdg_wm_base, xkb_keymap);
      result.is_some()) {
    return std::move(result);
  }

  auto* surface = m_surface.get();
  if (auto result = graphics::vulkan::GraphicsContext::new_pointer(
	app_id,
	1,
	[=](auto vk_instance, auto* vk_surface_khr) -> VkResult {
	  auto vk_wayland_surface_create_info_khr = VkWaylandSurfaceCreateInfoKHR{
	    VK_STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR, nullptr, 0, display, surface
	  };

	  return vkCreateWaylandSurfaceKHR(vk_instance, &vk_wayland_surface_create_info_khr, nullptr, vk_surface_khr);
	},
	m_height,
	VK_KHR_WAYLAND_SURFACE_EXTENSION_NAME,
	m_width);
      result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  } else {
    m_graphics_context = std::unique_ptr<graphics::vulkan::GraphicsContext>(std::move(result).unwrap());
  }

  return std::move(option::Option<Error>::None());
}

auto
engine::display::wayland::vulkan::Window::new_(
  engine::display::WindowOptions const& options,
  engine::CStr app_id,
  struct wl_compositor* compositor,
  engine::display::wayland::vulkan::Window::wl_shared_ptr<struct wl_cursor_theme> cursor_theme,
  struct wl_display* display,
  engine::display::wayland::vulkan::Window::wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
  struct xdg_wm_base* xdg_wm_base,
  engine::display::wayland::vulkan::Window::wl_shared_ptr<
    engine::display::wayland::vulkan::Window::wl_unique_ptr<struct xkb_keymap>> xkb_keymap) noexcept
  -> result::Result<Window, engine::Error>
{
  auto self = Window(options, std::forward<wl_shared_ptr<struct wl_cursor_theme>>(cursor_theme));
  if (auto result =
	self.initialize_vulkan(app_id,
			       compositor,
			       display,
			       options.title(),
			       std::forward<wl_shared_ptr<struct zwp_pointer_constraints_v1>>(wp_pointer_constraints),
			       xdg_wm_base,
			       std::forward<wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>>>(xkb_keymap));
      result.is_some()) {
    return std::move(result::Result<Window, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Window, Error>::Ok(std::move(self)));
}

auto
engine::display::wayland::vulkan::Window::new_pointer(
  engine::display::WindowOptions const& options,
  engine::CStr app_id,
  struct wl_compositor* compositor,
  engine::display::wayland::vulkan::Window::wl_shared_ptr<struct wl_cursor_theme> cursor_theme,
  struct wl_display* display,
  engine::display::wayland::vulkan::Window::wl_shared_ptr<struct zwp_pointer_constraints_v1> wp_pointer_constraints,
  struct xdg_wm_base* xdg_wm_base,
  engine::display::wayland::vulkan::Window::wl_shared_ptr<
    engine::display::wayland::vulkan::Window::wl_unique_ptr<struct xkb_keymap>> xkb_keymap) noexcept
  -> result::Result<Window*, engine::Error>
{
  auto self =
    std::unique_ptr<Window>(new Window(options, std::forward<wl_shared_ptr<struct wl_cursor_theme>>(cursor_theme)));
  if (auto result =
	self->initialize_vulkan(app_id,
				compositor,
				display,
				options.title(),
				std::forward<wl_shared_ptr<struct zwp_pointer_constraints_v1>>(wp_pointer_constraints),
				xdg_wm_base,
				std::forward<wl_shared_ptr<wl_unique_ptr<struct xkb_keymap>>>(xkb_keymap));
      result.is_some()) {
    return std::move(result::Result<Window*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Window*, Error>::Ok(self.release()));
}

auto
engine::display::wayland::vulkan::Window::xdg_toplevel_configure(struct xdg_toplevel* xdg_toplevel,
								 int32_t width,
								 int32_t height,
								 struct wl_array* states) & noexcept -> void
{
  wayland::Window::xdg_toplevel_configure(xdg_toplevel, width, height, states);
}
#endif

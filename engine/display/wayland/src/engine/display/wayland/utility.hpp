#pragma once

#include <functional>

#include "engine/display/wayland/wayland.h"

namespace engine::display::wayland {
template<typename T>
auto
array_for_each(struct wl_array* array, std::function<void(T*)> op) noexcept -> void
{
  for (auto pos = static_cast<T*>(array->data);
       reinterpret_cast<char const*>(pos) < (static_cast<const char*>(array->data) + array->size);
       pos++) {
    op(pos);
  }
}
} // namespace engine::display::wayland

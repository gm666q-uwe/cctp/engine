#include "engine/display/wayland/display.hpp"

#include <iostream>

#include <sys/mman.h>

engine::display::wayland::Display::Display(const engine::display::wayland::DisplayOptions& options) noexcept
  : display::Display(options)
  , m_xkb_keymap(std::make_shared<wl_unique_ptr<struct xkb_keymap>>())
{}

engine::display::wayland::Display::Display(engine::display::wayland::Display&& other) noexcept
  : display::Display(std::move(other))
  , m_compositor(std::move(other.m_compositor))
  , m_cursor_theme(std::move(other.m_cursor_theme))
  , m_display(std::move(other.m_display))
#if ENGINE_HAS_EGL
  , m_egl_display(std::move(other.m_egl_display))
#endif
  , m_keyboard(std::move(other.m_keyboard))
  , m_keyboard_surface(std::exchange(other.m_keyboard_surface, m_keyboard_surface))
  , m_output(std::move(other.m_output))
  , m_pointer(std::move(other.m_pointer))
  , m_pointer_surface(std::exchange(other.m_pointer_surface, m_pointer_surface))
  , m_registry(std::move(other.m_registry))
  , m_seat(std::move(other.m_seat))
  , m_shm(std::move(other.m_shm))
  , m_wp_pointer_constraints(std::move(other.m_wp_pointer_constraints))
  , m_wp_relative_pointer(std::move(other.m_wp_relative_pointer))
  , m_wp_relative_pointer_manager(std::move(other.m_wp_relative_pointer_manager))
  , m_xdg_output(std::move(other.m_xdg_output))
  , m_xdg_output_manager(std::move(other.m_xdg_output_manager))
  , m_xdg_wm_base(std::move(other.m_xdg_wm_base))
  , m_xkb_context(std::move(other.m_xkb_context))
  , m_xkb_keymap(std::move(other.m_xkb_keymap))
{
  if (m_keyboard) {
    wl_keyboard_set_user_data(m_keyboard.get(), this);
  }

  if (m_output) {
    wl_output_set_user_data(m_output.get(), this);
  }

  if (m_pointer) {
    wl_pointer_set_user_data(m_pointer.get(), this);
  }

  if (m_registry) {
    wl_registry_set_user_data(m_registry.get(), this);
  }

  if (m_seat) {
    wl_seat_set_user_data(m_seat.get(), this);
  }

  if (m_shm) {
    wl_shm_set_user_data(m_shm.get(), this);
  }

  if (m_wp_relative_pointer) {
    zwp_relative_pointer_v1_set_user_data(m_wp_relative_pointer.get(), this);
  }

  if (m_xdg_output) {
    zxdg_output_v1_set_user_data(m_xdg_output.get(), this);
  }

  if (m_xdg_wm_base) {
    xdg_wm_base_set_user_data(m_xdg_wm_base.get(), this);
  }
}

engine::display::wayland::Display::~Display() noexcept
{
  drop();
}

auto
engine::display::wayland::Display::operator=(engine::display::wayland::Display&& other) & noexcept
  -> display::wayland::Display&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_compositor = std::move(other.m_compositor);
  m_cursor_theme = std::move(other.m_cursor_theme);
  m_display = std::move(other.m_display);
#if ENGINE_HAS_EGL
  m_egl_display = std::move(other.m_egl_display);
#endif
  m_keyboard = std::move(other.m_keyboard);
  m_keyboard_surface = std::exchange(other.m_keyboard_surface, m_keyboard_surface);
  m_output = std::move(other.m_output);
  m_pointer = std::move(other.m_pointer);
  m_pointer_surface = std::exchange(other.m_pointer_surface, m_pointer_surface);
  m_registry = std::move(other.m_registry);
  m_seat = std::move(other.m_seat);
  m_shm = std::move(other.m_shm);
  m_wp_pointer_constraints = std::move(other.m_wp_pointer_constraints);
  m_wp_relative_pointer = std::move(other.m_wp_relative_pointer);
  m_wp_relative_pointer_manager = std::move(other.m_wp_relative_pointer_manager);
  m_xdg_output = std::move(other.m_xdg_output);
  m_xdg_output_manager = std::move(other.m_xdg_output_manager);
  m_xdg_wm_base = std::move(other.m_xdg_wm_base);
  m_xkb_context = std::move(other.m_xkb_context);
  m_xkb_keymap = std::move(other.m_xkb_keymap);

  if (m_keyboard) {
    wl_keyboard_set_user_data(m_keyboard.get(), this);
  }

  if (m_output) {
    wl_output_set_user_data(m_output.get(), this);
  }

  if (m_pointer) {
    wl_pointer_set_user_data(m_pointer.get(), this);
  }

  if (m_registry) {
    wl_registry_set_user_data(m_registry.get(), this);
  }

  if (m_seat) {
    wl_seat_set_user_data(m_seat.get(), this);
  }

  if (m_shm) {
    wl_shm_set_user_data(m_shm.get(), this);
  }

  if (m_wp_relative_pointer) {
    zwp_relative_pointer_v1_set_user_data(m_wp_relative_pointer.get(), this);
  }

  if (m_xdg_output) {
    zxdg_output_v1_set_user_data(m_xdg_output.get(), this);
  }

  if (m_xdg_wm_base) {
    xdg_wm_base_set_user_data(m_xdg_wm_base.get(), this);
  }

  display::Display::operator=(std::move(other));
  return *this;
}

#if ENGINE_HAS_EGL
auto
engine::display::wayland::Display::create_egl_window(engine::display::WindowOptions const& options) const& noexcept
  -> result::Result<engine::display::wayland::gl::egl::Window, engine::Error>
{
  return std::move(gl::egl::Window::new_(options,
					 m_app_id,
					 m_compositor.get(),
					 m_cursor_theme,
					 *m_egl_display,
					 m_wp_pointer_constraints,
					 m_xdg_wm_base.get(),
					 m_xkb_keymap));
}

auto
engine::display::wayland::Display::create_egl_window_pointer(engine::display::WindowOptions const& options)
  const& noexcept -> result::Result<engine::display::wayland::gl::egl::Window*, Error>
{
  return std::move(gl::egl::Window::new_pointer(options,
						m_app_id,
						m_compositor.get(),
						m_cursor_theme,
						*m_egl_display,
						m_wp_pointer_constraints,
						m_xdg_wm_base.get(),
						m_xkb_keymap));
}
#endif

#if ENGINE_HAS_VULKAN
auto
engine::display::wayland::Display::create_vulkan_window(const engine::display::WindowOptions& options) const& noexcept
  -> result::Result<vulkan::Window, Error>
{
  return std::move(vulkan::Window::new_(options,
					m_app_id,
					m_compositor.get(),
					m_cursor_theme,
					m_display.get(),
					m_wp_pointer_constraints,
					m_xdg_wm_base.get(),
					m_xkb_keymap));
}

auto
engine::display::wayland::Display::create_vulkan_window_pointer(
  const engine::display::WindowOptions& options) const& noexcept -> result::Result<vulkan::Window*, Error>
{
  return std::move(vulkan::Window::new_pointer(options,
					       m_app_id,
					       m_compositor.get(),
					       m_cursor_theme,
					       m_display.get(),
					       m_wp_pointer_constraints,
					       m_xdg_wm_base.get(),
					       m_xkb_keymap));
}
#endif

auto
engine::display::wayland::Display::create_window_pointer(engine::display::WindowOptions const& options) const& noexcept
  -> result::Result<engine::display::Window*, engine::Error>
{
  switch (options.graphics_api()) {
    case WindowOptions::GraphicsAPI::OpenGL:
#if ENGINE_HAS_EGL
      if (auto result = create_egl_window_pointer(options); result.is_err()) {
	return std::move(result::Result<display::Window*, Error>::Err(std::move(result).unwrap_err()));
      } else {
	return std::move(result::Result<display::Window*, Error>::Ok(std::move(result).unwrap()));
      }
#else
      return std::move(result::Result<display::Window*, Error>::Err(Error::new_(0, -1)));
#endif
    case WindowOptions::GraphicsAPI::Vulkan:
#if ENGINE_HAS_VULKAN
      if (auto result = create_vulkan_window_pointer(options); result.is_err()) {
	return std::move(result::Result<display::Window*, Error>::Err(std::move(result).unwrap_err()));
      } else {
	return std::move(result::Result<display::Window*, Error>::Ok(std::move(result).unwrap()));
      }
#else
      return std::move(result::Result<display::Window*, Error>::Err(Error::new_(0, -1)));
#endif
  }
}

auto
engine::display::wayland::Display::drop() & noexcept -> void
{
  m_running = false;

#if ENGINE_HAS_EGL
  m_egl_display.reset();
#endif

  m_xdg_output.reset();

  m_wp_relative_pointer.reset();
  m_pointer.reset();
  m_pointer_surface = nullptr;

  m_keyboard.reset();
  m_keyboard_surface = nullptr;

  m_cursor_theme.reset();

  m_xdg_wm_base.reset();
  m_xdg_output_manager.reset();
  m_wp_relative_pointer_manager.reset();
  m_wp_pointer_constraints.reset();
  m_shm.reset();
  m_seat.reset();
  m_output.reset();
  m_compositor.reset();

  m_registry.reset();
  m_display.reset();

  // m_xkb_keymap.reset();
  // m_xkb_context.reset();
}

auto
engine::display::wayland::Display::initialize(char const* display_name) & noexcept -> option::Option<engine::Error>
{
  m_xkb_context = wl_unique_ptr<struct xkb_context>(xkb_context_new(XKB_CONTEXT_NO_FLAGS), xkb_context_unref);
  if (!m_xkb_context) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  m_display = wl_unique_ptr<struct wl_display>(wl_display_connect(display_name), wl_display_disconnect);
  if (!m_display) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  m_registry = wl_unique_ptr<struct wl_registry>(wl_display_get_registry(m_display.get()), wl_registry_destroy);
  wl_registry_add_listener(m_registry.get(), &M_REGISTRY_LISTENER, this);
  wl_display_dispatch(m_display.get());
  wl_display_roundtrip(m_display.get());

  if (!m_compositor || !m_cursor_theme || !m_output || !m_seat || !m_shm || !m_wp_pointer_constraints ||
      !m_wp_relative_pointer_manager || !m_xdg_output_manager || !m_xdg_wm_base) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  m_xdg_output = wl_unique_ptr<struct zxdg_output_v1>(
    zxdg_output_manager_v1_get_xdg_output(m_xdg_output_manager.get(), m_output.get()), zxdg_output_v1_destroy);
  zxdg_output_v1_add_listener(m_xdg_output.get(), &M_XDG_OUTPUT_LISTENER, this);
  wl_display_roundtrip(m_display.get());

  if (auto result = initialize_egl(true); result.is_some()) {
    return std::move(result);
  }

  return std::move(option::Option<Error>::None());
}

auto
engine::display::wayland::Display::initialize_egl(bool pre_load) & noexcept -> option::Option<engine::Error>
{
  if (auto result =
	graphics::gl::egl::Display::new_pointer(m_display.get(), graphics::gl::egl::Platform::Wayland, pre_load);
      result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  } else {
    m_egl_display = std::unique_ptr<graphics::gl::egl::Display>(std::move(result).unwrap());
  }
  return std::move(option::Option<Error>::None());
}

auto
engine::display::wayland::Display::keyboard_enter(struct wl_keyboard* wl_keyboard,
						  uint32_t serial,
						  struct wl_surface* surface,
						  struct wl_array* keys) & noexcept -> void
{
  m_keyboard_surface = surface;
  static_cast<Window*>(wl_surface_get_user_data(surface))->keyboard_enter(wl_keyboard, serial, surface, keys);
}

auto
engine::display::wayland::Display::keyboard_enter(void* data,
						  struct wl_keyboard* wl_keyboard,
						  uint32_t serial,
						  struct wl_surface* surface,
						  struct wl_array* keys) noexcept -> void
{
  static_cast<Display*>(data)->keyboard_enter(wl_keyboard, serial, surface, keys);
}

auto
engine::display::wayland::Display::keyboard_key(struct wl_keyboard* wl_keyboard,
						uint32_t serial,
						uint32_t time,
						uint32_t key,
						uint32_t state) & noexcept -> void
{
  if (m_keyboard_surface != nullptr) {
    static_cast<Window*>(wl_surface_get_user_data(m_keyboard_surface))
      ->keyboard_key(wl_keyboard, serial, time, key, state);
  }
}

auto
engine::display::wayland::Display::keyboard_key(void* data,
						struct wl_keyboard* wl_keyboard,
						uint32_t serial,
						uint32_t time,
						uint32_t key,
						uint32_t state) noexcept -> void
{
  static_cast<Display*>(data)->keyboard_key(wl_keyboard, serial, time, key, state);
}

auto
engine::display::wayland::Display::keyboard_keymap(struct wl_keyboard* wl_keyboard,
						   uint32_t format,
						   int32_t fd,
						   uint32_t size) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Display][keyboard_keymap]: format " << format << ", fd " << fd << ", size "
	    << size << '.' << std::endl;

  if (format == WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1) {
    m_xkb_keymap->reset();

    auto* map_shm = mmap(nullptr, size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (map_shm == MAP_FAILED) {
      return;
    }

    *m_xkb_keymap = wl_unique_ptr<struct xkb_keymap>(
      xkb_keymap_new_from_string(
	m_xkb_context.get(), static_cast<char const*>(map_shm), XKB_KEYMAP_FORMAT_TEXT_V1, XKB_KEYMAP_COMPILE_NO_FLAGS),
      xkb_keymap_unref);
    munmap(map_shm, size);
    close(fd);
  }

  if (m_keyboard_surface != nullptr) {
    static_cast<Window*>(wl_surface_get_user_data(m_keyboard_surface))->keyboard_keymap(wl_keyboard, format, fd, size);
  }
}

auto
engine::display::wayland::Display::keyboard_keymap(void* data,
						   struct wl_keyboard* wl_keyboard,
						   uint32_t format,
						   int32_t fd,
						   uint32_t size) noexcept -> void
{
  static_cast<Display*>(data)->keyboard_keymap(wl_keyboard, format, fd, size);
}

auto
engine::display::wayland::Display::keyboard_leave(struct wl_keyboard* wl_keyboard,
						  uint32_t serial,
						  struct wl_surface* surface) & noexcept -> void
{
  m_keyboard_surface = nullptr;
  static_cast<Window*>(wl_surface_get_user_data(surface))->keyboard_leave(wl_keyboard, serial, surface);
}

auto
engine::display::wayland::Display::keyboard_leave(void* data,
						  struct wl_keyboard* wl_keyboard,
						  uint32_t serial,
						  struct wl_surface* surface) noexcept -> void
{
  static_cast<Display*>(data)->keyboard_leave(wl_keyboard, serial, surface);
}

auto
engine::display::wayland::Display::keyboard_modifiers(struct wl_keyboard* wl_keyboard,
						      uint32_t serial,
						      uint32_t mods_depressed,
						      uint32_t mods_latched,
						      uint32_t mods_locked,
						      uint32_t group) & noexcept -> void
{
  if (m_keyboard_surface != nullptr) {
    static_cast<Window*>(wl_surface_get_user_data(m_keyboard_surface))
      ->keyboard_modifiers(wl_keyboard, serial, mods_depressed, mods_latched, mods_locked, group);
  }
}

auto
engine::display::wayland::Display::keyboard_modifiers(void* data,
						      struct wl_keyboard* wl_keyboard,
						      uint32_t serial,
						      uint32_t mods_depressed,
						      uint32_t mods_latched,
						      uint32_t mods_locked,
						      uint32_t group) noexcept -> void
{
  static_cast<Display*>(data)->keyboard_modifiers(
    wl_keyboard, serial, mods_depressed, mods_latched, mods_locked, group);
}

auto
engine::display::wayland::Display::keyboard_repeat_info(struct wl_keyboard* wl_keyboard,
							int32_t rate,
							int32_t delay) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Display][keyboard_repeat_info]: rate " << rate << ", delay " << delay << '.'
	    << std::endl;
  if (m_keyboard_surface != nullptr) {
    static_cast<Window*>(wl_surface_get_user_data(m_keyboard_surface))->keyboard_repeat_info(wl_keyboard, rate, delay);
  }
}

auto
engine::display::wayland::Display::keyboard_repeat_info(void* data,
							struct wl_keyboard* wl_keyboard,
							int32_t rate,
							int32_t delay) noexcept -> void
{
  static_cast<Display*>(data)->keyboard_repeat_info(wl_keyboard, rate, delay);
}

auto
engine::display::wayland::Display::new_(const engine::display::wayland::DisplayOptions& options) noexcept
  -> result::Result<engine::display::wayland::Display, engine::Error>
{
  auto self = Display(options);
  if (auto result = self.initialize(options.display_name()); result.is_some()) {
    return std::move(result::Result<Display, Error>::Err(Error::new_(0, -1)));
  }
  return std::move(result::Result<Display, Error>::Ok(std::move(self)));
}

auto
engine::display::wayland::Display::new_pointer(const engine::display::wayland::DisplayOptions& options) noexcept
  -> result::Result<engine::display::wayland::Display*, engine::Error>
{
  auto self = std::unique_ptr<Display>(new Display(options));
  if (auto result = self->initialize(options.display_name()); result.is_some()) {
    return std::move(result::Result<Display*, Error>::Err(Error::new_(0, -1)));
  }
  return std::move(result::Result<Display*, Error>::Ok(self.release()));
}

auto
engine::display::wayland::Display::output_done(struct wl_output* wl_output) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Display][output_done]: ." << std::endl;
}

auto
engine::display::wayland::Display::output_done(void* data, struct wl_output* wl_output) noexcept -> void
{
  static_cast<Display*>(data)->output_done(wl_output);
}

auto
engine::display::wayland::Display::output_geometry(struct wl_output* wl_output,
						   int32_t x,
						   int32_t y,
						   int32_t physical_width,
						   int32_t physical_height,
						   int32_t subpixel,
						   char const* make,
						   char const* model,
						   int32_t transform) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Display][output_geometry]: x " << x << ", y " << y << ", physical_width "
	    << physical_width << ", physical_height " << physical_height << ", subpixel " << subpixel << ", make "
	    << make << ", model " << model << ", transform " << transform << '.' << std::endl;
}

auto
engine::display::wayland::Display::output_geometry(void* data,
						   struct wl_output* wl_output,
						   int32_t x,
						   int32_t y,
						   int32_t physical_width,
						   int32_t physical_height,
						   int32_t subpixel,
						   char const* make,
						   char const* model,
						   int32_t transform) noexcept -> void
{
  static_cast<Display*>(data)->output_geometry(
    wl_output, x, y, physical_width, physical_height, subpixel, make, model, transform);
}

auto
engine::display::wayland::Display::output_mode(struct wl_output* wl_output,
					       uint32_t flags,
					       int32_t width,
					       int32_t height,
					       int32_t refresh) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Display][output_mode]: flags " << flags << ", width " << width << ", height "
	    << height << ", refresh " << refresh << '.' << std::endl;
}

auto
engine::display::wayland::Display::output_mode(void* data,
					       struct wl_output* wl_output,
					       uint32_t flags,
					       int32_t width,
					       int32_t height,
					       int32_t refresh) noexcept -> void
{
  static_cast<Display*>(data)->output_mode(wl_output, flags, width, height, refresh);
}

auto
engine::display::wayland::Display::output_scale(struct wl_output* wl_output, int32_t factor) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Display][output_scale]: factor " << factor << '.' << std::endl;
}

auto
engine::display::wayland::Display::output_scale(void* data, struct wl_output* wl_output, int32_t factor) noexcept
  -> void
{
  static_cast<Display*>(data)->output_scale(wl_output, factor);
}

auto
engine::display::wayland::Display::pointer_axis(struct wl_pointer* wl_pointer,
						uint32_t time,
						uint32_t axis,
						wl_fixed_t value) & noexcept -> void
{
  if (m_pointer_surface != nullptr) {
    static_cast<Window*>(wl_surface_get_user_data(m_pointer_surface))->pointer_axis(wl_pointer, time, axis, value);
  }
}

auto
engine::display::wayland::Display::pointer_axis(void* data,
						struct wl_pointer* wl_pointer,
						uint32_t time,
						uint32_t axis,
						wl_fixed_t value) noexcept -> void
{
  static_cast<Display*>(data)->pointer_axis(wl_pointer, time, axis, value);
}

auto
engine::display::wayland::Display::pointer_axis_discrete(struct wl_pointer* wl_pointer,
							 uint32_t axis,
							 int32_t discrete) & noexcept -> void
{
  if (m_pointer_surface != nullptr) {
    static_cast<Window*>(wl_surface_get_user_data(m_pointer_surface))
      ->pointer_axis_discrete(wl_pointer, axis, discrete);
  }
}

auto
engine::display::wayland::Display::pointer_axis_discrete(void* data,
							 struct wl_pointer* wl_pointer,
							 uint32_t axis,
							 int32_t discrete) noexcept -> void
{
  static_cast<Display*>(data)->pointer_axis_discrete(wl_pointer, axis, discrete);
}

auto
engine::display::wayland::Display::pointer_axis_source(struct wl_pointer* wl_pointer, uint32_t axis_source) & noexcept
  -> void
{
  if (m_pointer_surface != nullptr) {
    static_cast<Window*>(wl_surface_get_user_data(m_pointer_surface))->pointer_axis_source(wl_pointer, axis_source);
  }
}

auto
engine::display::wayland::Display::pointer_axis_source(void* data,
						       struct wl_pointer* wl_pointer,
						       uint32_t axis_source) noexcept -> void
{
  static_cast<Display*>(data)->pointer_axis_source(wl_pointer, axis_source);
}

auto
engine::display::wayland::Display::pointer_axis_stop(struct wl_pointer* wl_pointer,
						     uint32_t time,
						     uint32_t axis) & noexcept -> void
{
  if (m_pointer_surface != nullptr) {
    static_cast<Window*>(wl_surface_get_user_data(m_pointer_surface))->pointer_axis_stop(wl_pointer, time, axis);
  }
}

auto
engine::display::wayland::Display::pointer_axis_stop(void* data,
						     struct wl_pointer* wl_pointer,
						     uint32_t time,
						     uint32_t axis) noexcept -> void
{
  static_cast<Display*>(data)->pointer_axis_stop(wl_pointer, time, axis);
}

auto
engine::display::wayland::Display::pointer_button(struct wl_pointer* wl_pointer,
						  uint32_t serial,
						  uint32_t time,
						  uint32_t button,
						  uint32_t state) & noexcept -> void
{
  if (m_pointer_surface != nullptr) {
    static_cast<Window*>(wl_surface_get_user_data(m_pointer_surface))
      ->pointer_button(wl_pointer, serial, time, button, state);
  }
}

auto
engine::display::wayland::Display::pointer_button(void* data,
						  struct wl_pointer* wl_pointer,
						  uint32_t serial,
						  uint32_t time,
						  uint32_t button,
						  uint32_t state) noexcept -> void
{
  static_cast<Display*>(data)->pointer_button(wl_pointer, serial, time, button, state);
}

auto
engine::display::wayland::Display::pointer_enter(struct wl_pointer* wl_pointer,
						 uint32_t serial,
						 struct wl_surface* surface,
						 wl_fixed_t surface_x,
						 wl_fixed_t surface_y) & noexcept -> void
{
  m_pointer_surface = surface;
  static_cast<Window*>(wl_surface_get_user_data(surface))
    ->pointer_enter(wl_pointer, serial, surface, surface_x, surface_y);
}

auto
engine::display::wayland::Display::pointer_enter(void* data,
						 struct wl_pointer* wl_pointer,
						 uint32_t serial,
						 struct wl_surface* surface,
						 wl_fixed_t surface_x,
						 wl_fixed_t surface_y) noexcept -> void
{
  static_cast<Display*>(data)->pointer_enter(wl_pointer, serial, surface, surface_x, surface_y);
}

auto
engine::display::wayland::Display::pointer_frame(struct wl_pointer* wl_pointer) & noexcept -> void
{
  if (m_pointer_surface != nullptr) {
    static_cast<Window*>(wl_surface_get_user_data(m_pointer_surface))->pointer_frame(wl_pointer);
  }
}

auto
engine::display::wayland::Display::pointer_frame(void* data, struct wl_pointer* wl_pointer) noexcept -> void
{
  static_cast<Display*>(data)->pointer_frame(wl_pointer);
}

auto
engine::display::wayland::Display::pointer_leave(struct wl_pointer* wl_pointer,
						 uint32_t serial,
						 struct wl_surface* surface) & noexcept -> void
{
  m_pointer_surface = nullptr;
  static_cast<Window*>(wl_surface_get_user_data(surface))->pointer_leave(wl_pointer, serial, surface);
}

auto
engine::display::wayland::Display::pointer_leave(void* data,
						 struct wl_pointer* wl_pointer,
						 uint32_t serial,
						 struct wl_surface* surface) noexcept -> void
{
  static_cast<Display*>(data)->pointer_leave(wl_pointer, serial, surface);
}

auto
engine::display::wayland::Display::pointer_motion(struct wl_pointer* wl_pointer,
						  uint32_t time,
						  wl_fixed_t surface_x,
						  wl_fixed_t surface_y) & noexcept -> void
{
  if (m_pointer_surface != nullptr) {
    static_cast<Window*>(wl_surface_get_user_data(m_pointer_surface))
      ->pointer_motion(wl_pointer, time, surface_x, surface_y);
  }
}

auto
engine::display::wayland::Display::pointer_motion(void* data,
						  struct wl_pointer* wl_pointer,
						  uint32_t time,
						  wl_fixed_t surface_x,
						  wl_fixed_t surface_y) noexcept -> void
{
  static_cast<Display*>(data)->pointer_motion(wl_pointer, time, surface_x, surface_y);
}

auto
engine::display::wayland::Display::registry_global(struct wl_registry* wl_registry,
						   uint32_t name,
						   char const* interface,
						   uint32_t version) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Display][registry_global]: name " << name << ", interface "
	    << interface << ", version " << version << '.' << std::endl;
  auto interface_view = std::string_view(interface);
  if (interface_view.compare(wl_compositor_interface.name) == 0) {
    m_compositor = wl_unique_ptr<struct wl_compositor>(
      static_cast<struct wl_compositor*>(wl_registry_bind(wl_registry, name, &wl_compositor_interface, 4)),
      wl_compositor_destroy);
  } else if (interface_view.compare(wl_output_interface.name) == 0) {
    m_output = wl_unique_ptr<wl_output>(
      static_cast<struct wl_output*>(wl_registry_bind(wl_registry, name, &wl_output_interface, 2)), wl_output_release);
    wl_output_add_listener(m_output.get(), &M_OUTPUT_LISTENER, this);
  } else if (interface_view.compare(wl_seat_interface.name) == 0) {
    m_seat = wl_unique_ptr<struct wl_seat>(
      static_cast<struct wl_seat*>(wl_registry_bind(wl_registry, name, &wl_seat_interface, 5)), wl_seat_release);
    wl_seat_add_listener(m_seat.get(), &M_SEAT_LISTENER, this);
  } else if (interface_view.compare(wl_shm_interface.name) == 0) {
    m_shm = wl_unique_ptr<struct wl_shm>(
      static_cast<struct wl_shm*>(wl_registry_bind(wl_registry, name, &wl_shm_interface, 1)), wl_shm_destroy);
    wl_shm_add_listener(m_shm.get(), &M_SHM_LISTENER, this);
    m_cursor_theme =
      wl_shared_ptr<struct wl_cursor_theme>(wl_cursor_theme_load(nullptr, 24, m_shm.get()), wl_cursor_theme_destroy);
  } else if (interface_view.compare(zwp_pointer_constraints_v1_interface.name) == 0) {
    m_wp_pointer_constraints =
      wl_shared_ptr<struct zwp_pointer_constraints_v1>(static_cast<struct zwp_pointer_constraints_v1*>(wl_registry_bind(
							 wl_registry, name, &zwp_pointer_constraints_v1_interface, 1)),
						       zwp_pointer_constraints_v1_destroy);
  } else if (interface_view.compare(zwp_relative_pointer_manager_v1_interface.name) == 0) {
    m_wp_relative_pointer_manager = wl_unique_ptr<struct zwp_relative_pointer_manager_v1>(
      static_cast<struct zwp_relative_pointer_manager_v1*>(
	wl_registry_bind(wl_registry, name, &zwp_relative_pointer_manager_v1_interface, 1)),
      zwp_relative_pointer_manager_v1_destroy);
  } else if (interface_view.compare(zxdg_output_manager_v1_interface.name) == 0) {
    m_xdg_output_manager =
      wl_unique_ptr<struct zxdg_output_manager_v1>(static_cast<struct zxdg_output_manager_v1*>(wl_registry_bind(
						     wl_registry, name, &zxdg_output_manager_v1_interface, 3)),
						   zxdg_output_manager_v1_destroy);
  } else if (interface_view.compare(xdg_wm_base_interface.name) == 0) {
    m_xdg_wm_base = wl_unique_ptr<xdg_wm_base>(
      static_cast<struct xdg_wm_base*>(wl_registry_bind(wl_registry, name, &xdg_wm_base_interface, 3)),
      xdg_wm_base_destroy);
    xdg_wm_base_add_listener(m_xdg_wm_base.get(), &M_XDG_WM_BASE_LISTENER, this);
  }
}

auto
engine::display::wayland::Display::registry_global(void* data,
						   struct wl_registry* wl_registry,
						   uint32_t name,
						   char const* interface,
						   uint32_t version) noexcept -> void
{
  static_cast<Display*>(data)->registry_global(wl_registry, name, interface, version);
}

auto
engine::display::wayland::Display::registry_global_remove(struct wl_registry* wl_registry, uint32_t name) & noexcept
  -> void
{}

auto
engine::display::wayland::Display::registry_global_remove(void* data,
							  struct wl_registry* wl_registry,
							  uint32_t name) noexcept -> void
{
  static_cast<Display*>(data)->registry_global_remove(wl_registry, name);
}

auto
engine::display::wayland::Display::run() & noexcept -> result::Result<bool, engine::Error>
{
  if (m_running) {
    m_running = wl_display_dispatch_pending(m_display.get()) != -1;
    /*auto fd = (struct pollfd){ wl_display_get_fd(m_display), POLLIN };
    while (wl_display_prepare_read(m_display) != 0) {
      wl_display_dispatch_pending(m_display);
    }
    if (wl_display_flush(m_display) < 0 && errno != EAGAIN) {
      wl_display_cancel_read(m_display);
      m_running = false;
      return result::Result<bool, std::int8_t>::Ok(m_running);
    }
    if (poll(&fd, 1, 0) > 0) {
      wl_display_read_events(m_display);
      wl_display_dispatch_pending(m_display);
    } else {
      wl_display_cancel_read(m_display);
    }*/
  }
  return result::Result<bool, Error>::Ok(m_running);
}

auto
engine::display::wayland::Display::seat_capabilities(struct wl_seat* wl_seat, uint32_t capabilities) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Display][seat_capabilities]: capabilities " << capabilities << '.'
	    << std::endl;
  if ((capabilities & WL_SEAT_CAPABILITY_POINTER) == WL_SEAT_CAPABILITY_POINTER) {
    if (!m_pointer) {
      m_pointer = wl_unique_ptr<struct wl_pointer>(wl_seat_get_pointer(wl_seat), wl_pointer_release);
      wl_pointer_add_listener(m_pointer.get(), &M_POINTER_LISTENER, this);
      m_wp_relative_pointer = wl_unique_ptr<struct zwp_relative_pointer_v1>(
	zwp_relative_pointer_manager_v1_get_relative_pointer(m_wp_relative_pointer_manager.get(), m_pointer.get()),
	zwp_relative_pointer_v1_destroy);
      zwp_relative_pointer_v1_add_listener(m_wp_relative_pointer.get(), &M_WP_RELATIVE_POINTER_LISTENER, this);
    }
  } else {
    if (m_pointer) {
      m_wp_relative_pointer.reset();
      m_pointer.reset();
      m_pointer_surface = nullptr;
    }
  }
  if ((capabilities & WL_SEAT_CAPABILITY_KEYBOARD) == WL_SEAT_CAPABILITY_KEYBOARD) {
    if (!m_keyboard) {
      m_keyboard = wl_unique_ptr<struct wl_keyboard>(wl_seat_get_keyboard(wl_seat), wl_keyboard_release);
      wl_keyboard_add_listener(m_keyboard.get(), &M_KEYBOARD_LISTENER, this);
    }
  } else {
    if (m_keyboard) {
      m_keyboard.reset();
      m_keyboard_surface = nullptr;
    }
  }
  /*if ((capabilities & WL_SEAT_CAPABILITY_TOUCH) == WL_SEAT_CAPABILITY_TOUCH) {
  } else {
  }*/
}

auto
engine::display::wayland::Display::seat_capabilities(void* data,
						     struct wl_seat* wl_seat,
						     uint32_t capabilities) noexcept -> void
{
  static_cast<Display*>(data)->seat_capabilities(wl_seat, capabilities);
}

auto
engine::display::wayland::Display::seat_name(struct wl_seat* wl_seat, char const* name) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Display][seat_name]: name " << name << '.' << std::endl;
}

auto
engine::display::wayland::Display::seat_name(void* data, struct wl_seat* wl_seat, char const* name) noexcept -> void
{
  static_cast<Display*>(data)->seat_name(wl_seat, name);
}

auto
engine::display::wayland::Display::shm_format(struct wl_shm* wl_shm, uint32_t format) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Display][shm_format]: format 0x" << std::hex << format << std::dec << '.'
	    << std::endl;
}

auto
engine::display::wayland::Display::shm_format(void* data, struct wl_shm* wl_shm, uint32_t format) noexcept -> void
{
  static_cast<Display*>(data)->shm_format(wl_shm, format);
}

auto
engine::display::wayland::Display::wp_relative_pointer_relative_motion(
  struct zwp_relative_pointer_v1* wp_relative_pointer,
  uint32_t utime_hi,
  uint32_t utime_lo,
  wl_fixed_t dx,
  wl_fixed_t dy,
  wl_fixed_t dx_unaccel,
  wl_fixed_t dy_unaccel) & noexcept -> void
{
  if (m_pointer_surface != nullptr) {
    static_cast<Window*>(wl_surface_get_user_data(m_pointer_surface))
      ->wp_relative_pointer_relative_motion(wp_relative_pointer, utime_hi, utime_lo, dx, dy, dx_unaccel, dy_unaccel);
  }
}

auto
engine::display::wayland::Display::wp_relative_pointer_relative_motion(
  void* data,
  struct zwp_relative_pointer_v1* wp_relative_pointer,
  uint32_t utime_hi,
  uint32_t utime_lo,
  wl_fixed_t dx,
  wl_fixed_t dy,
  wl_fixed_t dx_unaccel,
  wl_fixed_t dy_unaccel) noexcept -> void
{
  static_cast<Display*>(data)->wp_relative_pointer_relative_motion(
    wp_relative_pointer, utime_hi, utime_lo, dx, dy, dx_unaccel, dy_unaccel);
}

auto
engine::display::wayland::Display::xdg_output_description(struct zxdg_output_v1* xdg_output,
							  char const* description) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Display][xdg_output_description]: description " << description << '.'
	    << std::endl;
}

auto
engine::display::wayland::Display::xdg_output_description(void* data,
							  struct zxdg_output_v1* xdg_output,
							  char const* description) noexcept -> void
{
  static_cast<Display*>(data)->xdg_output_description(xdg_output, description);
}

auto
engine::display::wayland::Display::xdg_output_done(struct zxdg_output_v1* xdg_output) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Display][xdg_output_done]: ." << std::endl;
}

auto
engine::display::wayland::Display::xdg_output_done(void* data, struct zxdg_output_v1* xdg_output) noexcept -> void
{
  static_cast<Display*>(data)->xdg_output_done(xdg_output);
}

auto
engine::display::wayland::Display::xdg_output_logical_position(struct zxdg_output_v1* xdg_output,
							       int32_t x,
							       int32_t y) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Display][xdg_output_logical_position]: x " << x << ", y " << y << '.'
	    << std::endl;
}

auto
engine::display::wayland::Display::xdg_output_logical_position(void* data,
							       struct zxdg_output_v1* xdg_output,
							       int32_t x,
							       int32_t y) noexcept -> void
{
  static_cast<Display*>(data)->xdg_output_logical_position(xdg_output, x, y);
}

auto
engine::display::wayland::Display::xdg_output_logical_size(struct zxdg_output_v1* xdg_output,
							   int32_t width,
							   int32_t height) & noexcept -> void
{
  std::cout << "[engine::display::wayland][Display][xdg_output_logical_size]: width " << width << ", height " << height
	    << '.' << std::endl;
}

auto
engine::display::wayland::Display::xdg_output_logical_size(void* data,
							   struct zxdg_output_v1* xdg_output,
							   int32_t width,
							   int32_t height) noexcept -> void
{
  static_cast<Display*>(data)->xdg_output_logical_size(xdg_output, width, height);
}

auto
engine::display::wayland::Display::xdg_output_name(struct zxdg_output_v1* xdg_output, char const* name) & noexcept
  -> void
{
  std::cout << "[engine::display::wayland][Display][xdg_output_name]: name " << name << '.' << std::endl;
}

auto
engine::display::wayland::Display::xdg_output_name(void* data,
						   struct zxdg_output_v1* xdg_output,
						   char const* name) noexcept -> void
{
  static_cast<Display*>(data)->xdg_output_name(xdg_output, name);
}

auto
engine::display::wayland::Display::xdg_wm_base_ping(struct xdg_wm_base* xdg_wm_base, uint32_t serial) & noexcept -> void
{
  xdg_wm_base_pong(xdg_wm_base, serial);
}

auto
engine::display::wayland::Display::xdg_wm_base_ping(void* data,
						    struct xdg_wm_base* xdg_wm_base,
						    uint32_t serial) noexcept -> void
{
  static_cast<Display*>(data)->xdg_wm_base_ping(xdg_wm_base, serial);
}

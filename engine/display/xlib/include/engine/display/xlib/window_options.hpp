#pragma once

#include "engine/display/window_options.hpp"
#include "engine/display/xlib/xlib.h"

namespace engine::display::xlib {
class WindowOptions : public display::WindowOptions
{
public:
  enum class OpenGLInterface : std::uint8_t
  {
    EGL,
    GLX
  };

  constexpr WindowOptions(WindowOptions const&) noexcept = delete;

  WindowOptions(WindowOptions&& other) noexcept;

  ~WindowOptions() noexcept override = default;

  constexpr auto operator=(WindowOptions const&) & noexcept -> WindowOptions& = delete;

  auto operator=(WindowOptions&& other) & noexcept -> WindowOptions&;

  static auto new_() noexcept -> WindowOptions;

  static auto new_pointer() noexcept -> WindowOptions*;

  [[nodiscard]] constexpr auto opengl_interface() const& noexcept -> OpenGLInterface { return m_opengl_interface; }

  constexpr auto opengl_interface(OpenGLInterface opengl_interface) & noexcept -> WindowOptions&
  {
    m_opengl_interface = opengl_interface;

    return *this;
  }

  auto opengl_interface(OpenGLInterface opengl_interface) && noexcept -> WindowOptions;

protected:
private:
  OpenGLInterface m_opengl_interface = OpenGLInterface::GLX;

  constexpr WindowOptions() noexcept
    : display::WindowOptions()
  {}
};
} // namespace engine::display::xlib

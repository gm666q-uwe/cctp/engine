#pragma once

#include "engine/bitflags.hpp"
#include "engine/display/display_options.hpp"
#include "engine/display/xlib/xlib.h"

namespace engine::display::xlib {
class DisplayOptions : public display::DisplayOptions
{
public:
  ENGINE_BITFLAGS(Flags, std::uint8_t, 2, ((USE_GLX, 0x01), (USE_EGL, 0x02)))

  constexpr DisplayOptions(DisplayOptions const&) noexcept = delete;

  DisplayOptions(DisplayOptions&& other) noexcept;

  ~DisplayOptions() noexcept override = default;

  constexpr auto operator=(DisplayOptions const&) & noexcept -> DisplayOptions& = delete;

  auto operator=(DisplayOptions&& other) & noexcept -> DisplayOptions&;

  [[nodiscard]] constexpr auto display_name() const& noexcept -> char const* { return m_display_name; }

  constexpr auto display_name(char const* display_name) & noexcept -> DisplayOptions&
  {
    m_display_name = display_name;

    return *this;
  }

  auto display_name(char const* display_name) && noexcept -> DisplayOptions;

  [[nodiscard]] constexpr auto flags() const& noexcept -> Flags { return m_flags; }

  constexpr auto flags(Flags flags) & noexcept -> DisplayOptions&
  {
    m_flags = flags;

    return *this;
  }

  auto flags(Flags flags) && noexcept -> DisplayOptions;

  static auto new_() noexcept -> DisplayOptions;

  static auto new_pointer() noexcept -> DisplayOptions*;

protected:
private:
  char const* m_display_name = nullptr;
  Flags m_flags = Flags::all();

  constexpr DisplayOptions() noexcept
    : display::DisplayOptions()
  {}
};
} // namespace engine::display::xlib

ENGINE_BITFLAGS_HASH(engine::display::xlib::DisplayOptions::Flags, std::uint8_t)

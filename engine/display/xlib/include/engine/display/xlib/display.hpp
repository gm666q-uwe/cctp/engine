#pragma once

#include <engine/display/display.hpp>

#include "engine/display/xlib/xlib.h"

#if ENGINE_HAS_EGL
#include <engine/graphics/gl/egl/display.hpp>
#endif

#include "engine/display/xlib/display_options.hpp"
#include "engine/display/xlib/gl/egl/window.hpp"
#include "engine/display/xlib/gl/glx/window.hpp"
#include "engine/display/xlib/vulkan/window.hpp"

namespace engine::display::xlib {
class Display : public display::Display
{
public:
  constexpr Display(Display const&) noexcept = delete;

  Display(Display&& other) noexcept;

  ~Display() noexcept override;

  constexpr auto operator=(Display const&) & noexcept -> Display& = delete;

  auto operator=(Display&& other) & noexcept -> Display&;

#if ENGINE_HAS_EGL
  [[nodiscard]] auto create_egl_window(WindowOptions const& options) const& noexcept
    -> result::Result<gl::egl::Window, Error>;

  [[nodiscard]] auto create_egl_window_pointer(WindowOptions const& options) const& noexcept
    -> result::Result<gl::egl::Window*, Error>;
#endif

#if ENGINE_HAS_GLX
  [[nodiscard]] auto create_glx_window(WindowOptions const& options) const& noexcept
    -> result::Result<gl::glx::Window, Error>;

  [[nodiscard]] auto create_glx_window_pointer(WindowOptions const& options) const& noexcept
    -> result::Result<gl::glx::Window*, Error>;
#endif

#if ENGINE_HAS_VULKAN
  [[nodiscard]] auto create_vulkan_window(WindowOptions const& options) const& noexcept
    -> result::Result<vulkan::Window, Error>;

  [[nodiscard]] auto create_vulkan_window_pointer(WindowOptions const& options) const& noexcept
    -> result::Result<vulkan::Window*, Error>;
#endif

  [[nodiscard]] auto create_window_pointer(display::WindowOptions const& options) const& noexcept
    -> result::Result<display::Window*, Error> override;

  [[nodiscard]] auto create_window_pointer(WindowOptions const& options) const& noexcept
    -> result::Result<display::Window*, Error>;

  static auto new_(DisplayOptions const& options) noexcept -> result::Result<Display, Error>;

  static auto new_pointer(DisplayOptions const& options) noexcept -> result::Result<Display*, Error>;

  [[nodiscard]] auto run() & noexcept -> result::Result<bool, Error> override;

protected:
private:
  ::Display* m_display = nullptr;
#if ENGINE_HAS_EGL
  std::unique_ptr<graphics::gl::egl::Display> m_egl_display = std::unique_ptr<graphics::gl::egl::Display>();
#endif
#if ENGINE_HAS_GLX
  std::unique_ptr<graphics::gl::glx::Context> m_glx = nullptr;
#endif
  bool m_running = true;
  Atom m_wm_delete_window_atom = XLIB_NONE;
  Atom m_wm_protocols_atom = XLIB_NONE;

  explicit Display(DisplayOptions const& options) noexcept;

  auto drop() & noexcept -> void;

  auto initialize(char const* display_name, DisplayOptions::Flags flags) & noexcept -> option::Option<Error>;

  auto initialize_glx() & noexcept -> option::Option<Error>;

  auto initialize_egl(bool pre_load) & noexcept -> option::Option<Error>;
};
} // namespace engine::display::xlib

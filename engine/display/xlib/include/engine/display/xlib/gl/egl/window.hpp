#pragma once

#include "engine/display/xlib/window.hpp"

#if ENGINE_HAS_EGL
#include <engine/graphics/gl/egl/display.hpp>
#endif

namespace engine::display::xlib {
class Display;

namespace gl::egl {
#if ENGINE_HAS_EGL
class Window final : public xlib::Window
{
  friend xlib::Display;

public:
  constexpr Window(Window const&) noexcept = delete;

  Window(Window&& other) noexcept;

  ~Window() noexcept override;

  constexpr auto operator=(Window const&) & noexcept -> Window& = delete;

  auto operator=(Window&& other) & noexcept -> Window&;

  [[nodiscard]] auto egl() const& noexcept -> engine::graphics::gl::egl::Context&;

protected:
private:
  std::unique_ptr<graphics::gl::egl::Context> m_egl = std::unique_ptr<graphics::gl::egl::Context>();

  Window(WindowOptions const& options, ::Display* display, Atom wm_delete_window_atom, Atom wm_protocols_atom) noexcept;

  auto drop() & noexcept -> void;

  auto initialize_egl(graphics::gl::egl::Display const& egl_display, CStr title) & noexcept
    -> option::Option<engine::Error>;

  static auto new_(WindowOptions const& options,
		   ::Display* display,
		   graphics::gl::egl::Display const& egl_display,
		   Atom wm_delete_window_atom,
		   Atom wm_protocols_atom) noexcept -> result::Result<Window, Error>;

  static auto new_pointer(WindowOptions const& options,
			  ::Display* display,
			  graphics::gl::egl::Display const& egl_display,
			  Atom wm_delete_window_atom,
			  Atom wm_protocols_atom) noexcept -> result::Result<Window*, Error>;
};
#endif
} // namespace gl::egl
} // namespace engine::display::xlib

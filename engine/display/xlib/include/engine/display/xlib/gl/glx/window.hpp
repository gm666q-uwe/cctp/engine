#pragma once

#include "engine/display/xlib/window.hpp"

#if ENGINE_HAS_GLX
#include <engine/graphics/gl/glx/context.hpp>
#endif

namespace engine::display::xlib {
class Display;

namespace gl::glx {
#if ENGINE_HAS_GLX
class Window final : public xlib::Window
  {
  friend xlib::Display;

  public:
    constexpr Window(Window const&) noexcept = delete;

    Window(Window&& other) noexcept;

    ~Window() noexcept override;

    constexpr auto operator=(Window const&) & noexcept -> Window& = delete;

    auto operator=(Window&& other) & noexcept -> Window&;

    [[nodiscard]] auto glx() const& noexcept -> engine::graphics::gl::glx::Context&;

  protected:
  private:
    std::unique_ptr<graphics::gl::glx::Context> m_glx = std::unique_ptr<graphics::gl::glx::Context>();

  Window(WindowOptions const& options, ::Display* display, Atom wm_delete_window_atom, Atom wm_protocols_atom) noexcept;

  auto drop() & noexcept -> void;

  auto initialize_glx(graphics::gl::glx::Context const& glx, CStr title) & noexcept
  -> option::Option<engine::Error>;

  static auto new_(WindowOptions const& options,
		   ::Display* display,
		   graphics::gl::glx::Context const& glx,
		   Atom wm_delete_window_atom,
		   Atom wm_protocols_atom) noexcept -> result::Result<Window, Error>;

  static auto new_pointer(WindowOptions const& options,
			  ::Display* display,
			  graphics::gl::glx::Context const& glx,
			  Atom wm_delete_window_atom,
			  Atom wm_protocols_atom) noexcept -> result::Result<Window*, Error>;
  };
#endif
} // namespace gl::egl
} // namespace engine::display::xlib

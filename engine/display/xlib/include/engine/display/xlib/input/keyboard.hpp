#pragma once

#include <memory>

#include <engine/input/keyboard.hpp>

#include "engine/display/xlib/xlib.h"

namespace engine::display::xlib::input {
class Keyboard final : public engine::input::Keyboard
{
  friend class Manager;

public:
  // constexpr Keyboard() noexcept = delete;

  constexpr Keyboard(Keyboard const&) noexcept = delete;

  Keyboard(Keyboard&& other) noexcept = default;

  ~Keyboard() noexcept override;

  constexpr auto operator=(Keyboard const&) & noexcept -> Keyboard& = delete;

  auto operator=(Keyboard&& other) & noexcept -> Keyboard& = default;

  auto begin_update() & noexcept -> void override;

  [[nodiscard]] auto characters() const& noexcept -> CStr override;

  auto end_update() & noexcept -> void override;

  auto process_key_press(XKeyPressedEvent& key_pressed_event) & noexcept -> void;

  auto process_key_release(XKeyReleasedEvent& key_released_event) & noexcept -> void;

protected:
private:
  CString m_characters = CString();

  explicit Keyboard() noexcept;

  auto drop() & noexcept -> void;

  auto initialize() & noexcept -> void;

  static auto new_() noexcept -> Keyboard;

  static auto new_pointer() noexcept -> Keyboard*;
};
} // namespace engine::display::xlib::input

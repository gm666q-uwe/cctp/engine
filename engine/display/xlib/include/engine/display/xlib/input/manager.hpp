#pragma once

#include <engine/input/manager.hpp>

#include "engine/display/xlib/input/keyboard.hpp"
#include "engine/display/xlib/input/mouse.hpp"

namespace engine::display::xlib {
class Window;

namespace input {
class Manager final : public engine::input::Manager
{
  friend class xlib::Window;

public:
  // constexpr Manager() noexcept = delete;

  constexpr Manager(Manager const&) noexcept = delete;

  Manager(Manager&&) noexcept = default;

  ~Manager() noexcept override;

  constexpr auto operator=(Manager const&) & noexcept -> Manager& = delete;

  auto operator=(Manager&&) & noexcept -> Manager& = default;

  auto keyboard() & noexcept -> Keyboard&;

  auto keyboard() const& noexcept -> engine::input::Keyboard const& override;

  auto mouse() & noexcept -> Mouse&;

  auto mouse() const& noexcept -> engine::input::Mouse const& override;

protected:
private:
  Keyboard m_keyboard;
  Mouse m_mouse;

  explicit Manager() noexcept;

  auto drop() & noexcept -> void;

  auto initialize() & noexcept -> void;

  static auto new_() noexcept -> Manager;

  static auto new_pointer() noexcept -> Manager*;
};
} // namespace input
} // namespace engine::display::xlib

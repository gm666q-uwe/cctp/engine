#pragma once

#include <memory>

#include <engine/input/mouse.hpp>

#include "engine/display/xlib/xlib.h"

namespace engine::display::xlib::input {
class Mouse final : public engine::input::Mouse
{
  friend class Manager;

public:
  // constexpr Mouse() noexcept = delete;

  constexpr Mouse(Mouse const&) noexcept = delete;

  Mouse(Mouse&& other) noexcept = default;

  ~Mouse() noexcept override;

  constexpr auto operator=(Mouse const&) & noexcept -> Mouse& = delete;

  auto operator=(Mouse&& other) & noexcept -> Mouse& = default;

  auto begin_update() & noexcept -> void override;

  auto confine() & noexcept -> void override;

  auto end_update() & noexcept -> void override;

  auto lock() & noexcept -> void override;

  auto process_button_pass(XButtonPressedEvent& button_pressed_event) & noexcept -> void;

  auto process_button_release(XButtonReleasedEvent& button_released_event) & noexcept -> void;

  auto process_motion_notify(XPointerMovedEvent& pointer_moved_event) & noexcept -> void;

protected:
private:
  int m_position_x = 0;
  int m_position_y = 0;
  int m_previous_position_x = 0;
  int m_previous_position_y = 0;

  explicit Mouse() noexcept;

  auto drop() & noexcept -> void;

  auto initialize() & noexcept -> void;

  static auto new_() noexcept -> Mouse;

  static auto new_pointer() noexcept -> Mouse*;
};
} // namespace engine::display::xlib::input

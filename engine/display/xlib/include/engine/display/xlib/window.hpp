#pragma once

#include <memory>

#include <engine/display/window.hpp>

#include "engine/display/xlib/input/manager.hpp"
#include "engine/display/xlib/window_options.hpp"

namespace engine::display::xlib {
class Window : public display::Window
{
  friend class Display;

public:
  constexpr Window(Window const&) noexcept = delete;

  Window(Window&& other) noexcept;

  ~Window() noexcept override;

  constexpr auto operator=(Window const&) & noexcept -> Window& = delete;

  auto operator=(Window&& other) & noexcept -> Window&;

  [[nodiscard]] auto graphics_context() const& noexcept -> graphics::GraphicsContext& override;

  [[nodiscard]] auto input_manager() const& noexcept -> engine::input::Manager& override;

  [[nodiscard]] auto run() & noexcept -> result::Result<bool, Error> override;

protected:
  ::Display* m_display = nullptr;
  std::unique_ptr<graphics::GraphicsContext> m_graphics_context = std::unique_ptr<graphics::GraphicsContext>();
  std::unique_ptr<input::Manager> m_input_manager = std::unique_ptr<input::Manager>();
  bool m_running = true;
  ::Window m_window = XLIB_NONE;
  Atom m_wm_delete_window_atom = XLIB_NONE;
  Atom m_wm_protocols_atom = XLIB_NONE;

  Window(WindowOptions const& options, ::Display* display, Atom wm_delete_window_atom, Atom wm_protocols_atom) noexcept;

  // virtual auto event_configure_notify(XConfigureEvent const& configure_event) & noexcept -> void;

  auto initialize(CStr title, XVisualInfo const& visual_info) & noexcept -> option::Option<Error>;

private:
  static constexpr long const M_EVENT_MASK =
    ButtonPressMask | ButtonReleaseMask | KeyPressMask | KeyReleaseMask | PointerMotionMask | StructureNotifyMask;

  auto drop() & noexcept -> void;
};
} // namespace engine::display::xlib

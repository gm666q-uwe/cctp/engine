#pragma once

#include "engine/display/xlib/window.hpp"

namespace engine::display::xlib {
class Display;

namespace vulkan {
#if ENGINE_HAS_VULKAN
class Window final : public xlib::Window
{
  friend xlib::Display;

public:
  constexpr Window(Window const&) noexcept = delete;

  Window(Window&& other) noexcept;

  ~Window() noexcept override;

  constexpr auto operator=(Window const&) & noexcept -> Window& = delete;

  auto operator=(Window&& other) & noexcept -> Window&;

protected:
private:
  Window(WindowOptions const& options, ::Display* display, Atom wm_delete_window_atom, Atom wm_protocols_atom) noexcept;

  auto drop() & noexcept -> void;

  auto initialize_vulkan(CStr app_id, CStr title) & noexcept -> option::Option<engine::Error>;

  static auto new_(WindowOptions const& options,
		   CStr app_id,
		   ::Display* display,
		   Atom wm_delete_window_atom,
		   Atom wm_protocols_atom) noexcept -> result::Result<Window, Error>;

  static auto new_pointer(WindowOptions const& options,
			  CStr app_id,
			  ::Display* display,
			  Atom wm_delete_window_atom,
			  Atom wm_protocols_atom) noexcept -> result::Result<Window*, Error>;
};
#endif
} // namespace vulkan
} // namespace engine::display::xlib

#pragma once

#include "engine/platform.h"

#include <X11/Xatom.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#ifdef None
constexpr int const XLIB_NONE = None;
#undef None
#endif

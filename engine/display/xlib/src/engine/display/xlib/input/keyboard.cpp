#include "engine/display/xlib/input/keyboard.hpp"

#include <iostream>

static constexpr auto
key_sym_to_key(KeySym key_sym) noexcept -> engine::input::Key
{
  using namespace engine::input;
  switch (key_sym) {
    case XK_space:
      return Key::KeySpace;
    case XK_exclam:
      return Key::Key1;
    case XK_quotedbl:
      return Key::KeyApostrophe;
    case XK_numbersign:
      return Key::Key3;
    case XK_dollar:
      return Key::Key4;
    case XK_percent:
      return Key::Key5;
    case XK_ampersand:
      return Key::Key7;
    case XK_apostrophe:
      return Key::KeyApostrophe;
    case XK_parenleft:
      return Key::Key9;
    case XK_parenright:
      return Key::Key0;
    case XK_asterisk:
      return Key::Key8;
    case XK_plus:
      return Key::KeyEqual;
    case XK_comma:
      return Key::KeyComma;
    case XK_minus:
      return Key::KeyMinus;
    case XK_period:
      return Key::KeyDot;
    case XK_slash:
      return Key::KeySlash;
    case XK_0:
      return Key::Key0;
    case XK_1:
      return Key::Key1;
    case XK_2:
      return Key::Key2;
    case XK_3:
      return Key::Key3;
    case XK_4:
      return Key::Key4;
    case XK_5:
      return Key::Key5;
    case XK_6:
      return Key::Key6;
    case XK_7:
      return Key::Key7;
    case XK_8:
      return Key::Key8;
    case XK_9:
      return Key::Key9;
    case XK_colon:
      return Key::KeySemicolon;
    case XK_semicolon:
      return Key::KeySemicolon;
    case XK_less:
      return Key::KeyComma;
    case XK_equal:
      return Key::KeyEqual;
    case XK_greater:
      return Key::KeyDot;
    case XK_question:
      return Key::KeySlash;
    case XK_at:
      return Key::Key2;
    case XK_A:
      return Key::KeyA;
    case XK_B:
      return Key::KeyB;
    case XK_C:
      return Key::KeyC;
    case XK_D:
      return Key::KeyD;
    case XK_E:
      return Key::KeyE;
    case XK_F:
      return Key::KeyF;
    case XK_G:
      return Key::KeyG;
    case XK_H:
      return Key::KeyH;
    case XK_I:
      return Key::KeyI;
    case XK_J:
      return Key::KeyJ;
    case XK_K:
      return Key::KeyK;
    case XK_L:
      return Key::KeyL;
    case XK_M:
      return Key::KeyM;
    case XK_N:
      return Key::KeyN;
    case XK_O:
      return Key::KeyO;
    case XK_P:
      return Key::KeyP;
    case XK_Q:
      return Key::KeyQ;
    case XK_R:
      return Key::KeyR;
    case XK_S:
      return Key::KeyS;
    case XK_T:
      return Key::KeyT;
    case XK_U:
      return Key::KeyU;
    case XK_V:
      return Key::KeyV;
    case XK_W:
      return Key::KeyW;
    case XK_X:
      return Key::KeyX;
    case XK_Y:
      return Key::KeyY;
    case XK_Z:
      return Key::KeyZ;
    case XK_bracketleft:
      return Key::KeyLeftBrace;
    case XK_backslash:
      return Key::KeyBackslash;
    case XK_bracketright:
      return Key::KeyRightBrace;
    case XK_asciicircum:
      return Key::Key6;
    case XK_underscore:
      return Key::KeyMinus;
    case XK_grave:
      return Key::KeyGrave;
    case XK_a:
      return Key::KeyA;
    case XK_b:
      return Key::KeyB;
    case XK_c:
      return Key::KeyC;
    case XK_d:
      return Key::KeyD;
    case XK_e:
      return Key::KeyE;
    case XK_f:
      return Key::KeyF;
    case XK_g:
      return Key::KeyG;
    case XK_h:
      return Key::KeyH;
    case XK_i:
      return Key::KeyI;
    case XK_j:
      return Key::KeyJ;
    case XK_k:
      return Key::KeyK;
    case XK_l:
      return Key::KeyL;
    case XK_m:
      return Key::KeyM;
    case XK_n:
      return Key::KeyN;
    case XK_o:
      return Key::KeyO;
    case XK_p:
      return Key::KeyP;
    case XK_q:
      return Key::KeyQ;
    case XK_r:
      return Key::KeyR;
    case XK_s:
      return Key::KeyS;
    case XK_t:
      return Key::KeyT;
    case XK_u:
      return Key::KeyU;
    case XK_v:
      return Key::KeyV;
    case XK_w:
      return Key::KeyW;
    case XK_x:
      return Key::KeyX;
    case XK_y:
      return Key::KeyY;
    case XK_z:
      return Key::KeyZ;
    case XK_braceleft:
      return Key::KeyLeftBrace;
    case XK_bar:
      return Key::KeyBackslash;
    case XK_braceright:
      return Key::KeyRightBrace;
    case XK_asciitilde:
      return Key::KeyGrave;
    /*case XK_nobreakspace:
      return;
    case XK_exclamdown:
      return;
    case XK_cent:
      return;
    case XK_sterling:
      return;
    case XK_currency:
      return;
    case XK_yen:
      return;
    case XK_brokenbar:
      return;
    case XK_section:
      return;
    case XK_diaeresis:
      return;
    case XK_copyright:
      return;
    case XK_ordfeminine:
      return;
    case XK_guillemotleft:
      return;
    case XK_notsign:
      return;
    case XK_hyphen:
      return;
    case XK_registered:
      return;
    case XK_macron:
      return;
    case XK_degree:
      return;
    case XK_plusminus:
      return;
    case XK_twosuperior:
      return;
    case XK_threesuperior:
      return;
    case XK_acute:
      return;
    case XK_mu:
      return;
    case XK_paragraph:
      return;
    case XK_periodcentered:
      return;
    case XK_cedilla:
      return;
    case XK_onesuperior:
      return;
    case XK_masculine:
      return;
    case XK_guillemotright:
      return;
    case XK_onequarter:
      return;
    case XK_onehalf:
      return;
    case XK_threequarters:
      return;
    case XK_questiondown:
      return;
    case XK_Agrave:
      return;
    case XK_Aacute:
      return;
    case XK_Acircumflex:
      return;
    case XK_Atilde:
      return;
    case XK_Adiaeresis:
      return;
    case XK_Aring:
      return;
    case XK_AE:
      return;
    case XK_Ccedilla:
      return;
    case XK_Egrave:
      return;
    case XK_Eacute:
      return;
    case XK_Ecircumflex:
      return;
    case XK_Ediaeresis:
      return;
    case XK_Igrave:
      return;
    case XK_Iacute:
      return;
    case XK_Icircumflex:
      return;
    case XK_Idiaeresis:
      return;
    case XK_ETH:
      return;
    case XK_Eth:
      return;
    case XK_Ntilde:
      return;
    case XK_Ograve:
      return;
    case XK_Oacute:
      return;
    case XK_Ocircumflex:
      return;
    case XK_Otilde:
      return;
    case XK_Odiaeresis:
      return;
    case XK_multiply:
      return;
    case XK_Oslash:
      return;
    case XK_Ooblique:
      return;
    case XK_Ugrave:
      return;
    case XK_Uacute:
      return;
    case XK_Ucircumflex:
      return;
    case XK_Udiaeresis:
      return;
    case XK_Yacute:
      return;
    case XK_THORN:
      return;
    case XK_Thorn:
      return;
    case XK_ssharp:
      return;
    case XK_agrave:
      return;
    case XK_aacute:
      return;
    case XK_acircumflex:
      return;
    case XK_atilde:
      return;
    case XK_adiaeresis:
      return;
    case XK_aring:
      return;
    case XK_ae:
      return;
    case XK_ccedilla:
      return;
    case XK_egrave:
      return;
    case XK_eacute:
      return;
    case XK_ecircumflex:
      return;
    case XK_ediaeresis:
      return;
    case XK_igrave:
      return;
    case XK_iacute:
      return;
    case XK_icircumflex:
      return;
    case XK_idiaeresis:
      return;
    case XK_eth:
      return;
    case XK_ntilde:
      return;
    case XK_ograve:
      return;
    case XK_oacute:
      return;
    case XK_ocircumflex:
      return;
    case XK_otilde:
      return;
    case XK_odiaeresis:
      return;
    case XK_division:
      return;
    case XK_oslash:
      return;
    case XK_ooblique:
      return;
    case XK_ugrave:
      return;
    case XK_uacute:
      return;
    case XK_ucircumflex:
      return;
    case XK_udiaeresis:
      return;
    case XK_yacute:
      return;
    case XK_thorn:
      return;
    case XK_ydiaeresis:
      return;*/
    case XK_Escape:
      return Key::KeyEscape;
    case XK_Home:
      return Key::KeyHome;
    case XK_Left:
      return Key::KeyLeft;
    case XK_Up:
      return Key::KeyUp;
    case XK_Right:
      return Key::KeyRight;
    case XK_Down:
      return Key::KeyDown;
    case XK_Page_Up:
      return Key::KeyPageUp;
    case XK_Page_Down:
      return Key::KeyPageDown;
    case XK_End:
      return Key::KeyEnd;
    /*case XK_Begin:
      return;*/
    case XK_Shift_L:
      return Key::KeyLeftShift;
    case XK_Shift_R:
      return Key::KeyRightShift;
    case XK_Control_L:
      return Key::KeyLeftControl;
    case XK_Control_R:
      return Key::KeyRightControl;
    case XK_Caps_Lock:
      return Key::KeyCapsLock;
    /*case XK_Shift_Lock:
      return;
    case XK_Meta_L:
      return;
    case XK_Meta_R:
      return;*/
    case XK_Alt_L:
      return Key::KeyLeftAlternate;
    case XK_Alt_R:
      return Key::KeyRightAlternate;
    /*case XK_Super_L:
      return;
    case XK_Super_R:
      return;
    case XK_Hyper_L:
      return;
    case XK_Hyper_R:
      return;*/
    default:
      return Key::KeyReserved;
  }
}

engine::display::xlib::input::Keyboard::Keyboard() noexcept {}

engine::display::xlib::input::Keyboard::~Keyboard() noexcept
{
  drop();
}

auto
engine::display::xlib::input::Keyboard::begin_update() & noexcept -> void
{}

auto
engine::display::xlib::input::Keyboard::characters() const& noexcept -> engine::CStr
{
  return m_characters;
}

auto
engine::display::xlib::input::Keyboard::drop() & noexcept -> void
{
  m_characters.clear();
}

auto
engine::display::xlib::input::Keyboard::end_update() & noexcept -> void
{
  m_characters.clear();
}

auto
engine::display::xlib::input::Keyboard::initialize() & noexcept -> void
{}

auto
engine::display::xlib::input::Keyboard::new_() noexcept -> engine::display::xlib::input::Keyboard
{
  auto self = Keyboard();
  self.initialize();
  return std::move(self);
  /*if (auto result = self.initialize(); result.is_some()) {
    return std::move(result::Result<Keyboard, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Keyboard, Error>::Ok(std::move(self)));*/
}

auto
engine::display::xlib::input::Keyboard::new_pointer() noexcept -> engine::display::xlib::input::Keyboard*
{
  auto self = std::unique_ptr<Keyboard>();
  self->initialize();
  return self.release();
  /*if (auto result = self->initialize(); result.is_some()) {
    return std::move(result::Result<Keyboard*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Keyboard*, Error>::Ok(self.release()));*/
}

auto
engine::display::xlib::input::Keyboard::process_key_press(XKeyPressedEvent& key_pressed_event) & noexcept -> void
{
  m_events.emplace(key_sym_to_key(XLookupKeysym(&key_pressed_event, 0)), 1.0f);
}

auto
engine::display::xlib::input::Keyboard::process_key_release(XKeyReleasedEvent& key_released_event) & noexcept -> void
{
  m_events.emplace(key_sym_to_key(XLookupKeysym(&key_released_event, 0)), 0.0f);
}

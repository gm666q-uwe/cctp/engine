#include "engine/display/xlib/input/manager.hpp"

engine::display::xlib::input::Manager::Manager() noexcept
  : engine::input::Manager({ &m_keyboard, &m_mouse })
  , m_keyboard(Keyboard::new_())
  , m_mouse(Mouse::new_())
{}

engine::display::xlib::input::Manager::~Manager() noexcept
{
  drop();
}

auto
engine::display::xlib::input::Manager::drop() & noexcept -> void
{}

auto
engine::display::xlib::input::Manager::keyboard() & noexcept -> engine::display::xlib::input::Keyboard&
{
  return m_keyboard;
}

auto
engine::display::xlib::input::Manager::keyboard() const& noexcept -> engine::input::Keyboard const&
{
  return m_keyboard;
}

auto
engine::display::xlib::input::Manager::initialize() & noexcept -> void
{}

auto
engine::display::xlib::input::Manager::mouse() & noexcept -> engine::display::xlib::input::Mouse&
{
  return m_mouse;
}

auto
engine::display::xlib::input::Manager::mouse() const& noexcept -> engine::input::Mouse const&
{
  return m_mouse;
}

auto
engine::display::xlib::input::Manager::new_() noexcept -> engine::display::xlib::input::Manager
{
  auto self = Manager();
  self.initialize();
  return std::move(self);
  /*if (auto result = self.initialize(); result.is_some()) {
    return std::move(result::Result<Manager, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Manager, Error>::Ok(std::move(self)));*/
}

auto
engine::display::xlib::input::Manager::new_pointer() noexcept -> engine::display::xlib::input::Manager*
{
  auto self = std::unique_ptr<Manager>(new Manager());
  self->initialize();
  return self.release();
  /*if (auto result = self->initialize(); result.is_some()) {
    return std::move(result::Result<Manager*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Manager*, Error>::Ok(self.release()));*/
}

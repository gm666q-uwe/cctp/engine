#include "engine/display/xlib/input/mouse.hpp"

#include <iostream>

static constexpr auto
button_to_key(unsigned int button) noexcept -> engine::input::Key
{
  using namespace engine::input;
  switch (button) {
    case Button1:
      return Key::ButtonLeftMouse;
    case Button2:
      return Key::ButtonMiddleMouse;
    case Button3:
      return Key::ButtonRightMouse;
    /*case Button4:
      break;
    case Button5:
      break;*/
    default:
      return Key::KeyReserved;
  }
}

engine::display::xlib::input::Mouse::Mouse() noexcept {}

engine::display::xlib::input::Mouse::~Mouse() noexcept
{
  drop();
}

auto
engine::display::xlib::input::Mouse::drop() & noexcept -> void
{
  m_position_x = 0;
  m_position_y = 0;
  m_previous_position_x = 0;
  m_previous_position_y = 0;
}

auto
engine::display::xlib::input::Mouse::begin_update() & noexcept -> void
{
  auto relative_x = m_position_x - m_previous_position_x;
  auto relative_y = m_position_y - m_previous_position_y;
  m_events.emplace(engine::input::RelativeAxis::LeftX, static_cast<float>(relative_x));
  m_events.emplace(engine::input::RelativeAxis::LeftY, static_cast<float>(relative_y));
}

auto
engine::display::xlib::input::Mouse::confine() & noexcept -> void
{}

auto
engine::display::xlib::input::Mouse::end_update() & noexcept -> void
{
  m_previous_position_x = m_position_x;
  m_previous_position_y = m_position_y;
}

auto
engine::display::xlib::input::Mouse::initialize() & noexcept -> void
{}

auto
engine::display::xlib::input::Mouse::lock() & noexcept -> void
{}

auto
engine::display::xlib::input::Mouse::new_() noexcept -> engine::display::xlib::input::Mouse
{
  auto self = Mouse();
  self.initialize();
  return std::move(self);
  /*if (auto result = self.initialize(); result.is_some()) {
    return std::move(result::Result<Mouse, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Mouse, Error>::Ok(std::move(self)));*/
}

auto
engine::display::xlib::input::Mouse::new_pointer() noexcept -> engine::display::xlib::input::Mouse*
{
  auto self = std::unique_ptr<Mouse>(new Mouse());
  self->initialize();
  return self.release();
  /*if (auto result = self->initialize(); result.is_some()) {
    return std::move(result::Result<Mouse*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Mouse*, Error>::Ok(self.release()));*/
}

auto
engine::display::xlib::input::Mouse::process_button_pass(XButtonPressedEvent& button_pressed_event) & noexcept -> void
{
  m_events.emplace(button_to_key(button_pressed_event.button), 1.0f);
}

auto
engine::display::xlib::input::Mouse::process_button_release(XButtonReleasedEvent& button_released_event) & noexcept
  -> void
{
  m_events.emplace(button_to_key(button_released_event.button), 0.0f);
}

auto
engine::display::xlib::input::Mouse::process_motion_notify(XPointerMovedEvent& pointer_moved_event) & noexcept -> void
{
  m_position_x = pointer_moved_event.x;
  m_position_y = pointer_moved_event.y;
  // m_events.emplace(engine::input::AbsoluteAxis::LeftX, static_cast<float>(m_position_x));
  // m_events.emplace(engine::input::AbsoluteAxis::LeftY, static_cast<float>(m_position_y));
}

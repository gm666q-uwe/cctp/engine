#include "engine/display/xlib/display.hpp"

#include <iostream>

engine::display::xlib::Display::Display(const engine::display::xlib::DisplayOptions& options) noexcept
  : display::Display(options)
{}

engine::display::xlib::Display::Display(engine::display::xlib::Display&& other) noexcept
  : display::Display(std::move(other))
  , m_display(std::exchange(other.m_display, nullptr))
#if ENGINE_HAS_EGL
  , m_egl_display(std::move(other.m_egl_display))
#endif
#if ENGINE_HAS_GLX
  , m_glx(std::move(other.m_glx))
#endif
  , m_running(std::exchange(other.m_running, false))
  , m_wm_delete_window_atom(other.m_wm_delete_window_atom)
  , m_wm_protocols_atom(other.m_wm_protocols_atom)
{}

engine::display::xlib::Display::~Display() noexcept
{
  drop();
}

auto
engine::display::xlib::Display::operator=(engine::display::xlib::Display&& other) & noexcept -> display::xlib::Display&
{
  if (this == &display::Display::operator=(std::move(other))) {
    return *this;
  }

  drop();

  m_display = std::exchange(other.m_display, m_display);
#if ENGINE_HAS_EGL
  m_egl_display = std::move(other.m_egl_display);
#endif
#if ENGINE_HAS_GLX
  m_glx = std::move(other.m_glx);
#endif
  m_running = std::exchange(other.m_running, m_running);
  m_wm_delete_window_atom = other.m_wm_delete_window_atom;
  m_wm_protocols_atom = other.m_wm_protocols_atom;

  return *this;
}

#if ENGINE_HAS_EGL
auto
engine::display::xlib::Display::create_egl_window(engine::display::xlib::WindowOptions const& options) const& noexcept
  -> result::Result<gl::egl::Window, Error>
{
  if (!m_egl_display) {
    std::cerr << "[engine::display::xlib][Display][create_window]: "
		 "EGL requested but not initialized. "
		 "Enable EGL in DisplayOptions."
	      << std::endl;
    return std::move(result::Result<gl::egl::Window, Error>::Err(Error::new_(0, -1)));
  }
  return std::move(
    gl::egl::Window::new_(options, m_display, *m_egl_display, m_wm_delete_window_atom, m_wm_protocols_atom));
}

auto
engine::display::xlib::Display::create_egl_window_pointer(
  engine::display::xlib::WindowOptions const& options) const& noexcept -> result::Result<gl::egl::Window*, Error>
{
  if (!m_egl_display) {
    std::cerr << "[engine::display::xlib][Display][create_window]: "
		 "EGL requested but not initialized. "
		 "Enable EGL in DisplayOptions."
	      << std::endl;
    return std::move(result::Result<gl::egl::Window*, Error>::Err(Error::new_(0, -1)));
  }
  return std::move(
    gl::egl::Window::new_pointer(options, m_display, *m_egl_display, m_wm_delete_window_atom, m_wm_protocols_atom));
}
#endif

#if ENGINE_HAS_GLX
auto
engine::display::xlib::Display::create_glx_window(const engine::display::xlib::WindowOptions& options) const& noexcept
  -> result::Result<gl::glx::Window, Error>
{
  if (!m_glx) {
    std::cerr << "[engine::display::xlib][Display][create_window]: "
		 "GLX requested but not initialized. "
		 "Enable GLX in DisplayOptions."
	      << std::endl;
    return std::move(result::Result<gl::glx::Window, Error>::Err(Error::new_(0, -1)));
  }
  return std::move(gl::glx::Window::new_(options, m_display, *m_glx, m_wm_delete_window_atom, m_wm_protocols_atom));
}

auto
engine::display::xlib::Display::create_glx_window_pointer(
  const engine::display::xlib::WindowOptions& options) const& noexcept -> result::Result<gl::glx::Window*, Error>
{
  if (!m_glx) {
    std::cerr << "[engine::display::xlib][Display][create_window]: "
		 "GLX requested but not initialized. "
		 "Enable GLX in DisplayOptions."
	      << std::endl;
    return std::move(result::Result<gl::glx::Window*, Error>::Err(Error::new_(0, -1)));
  }
  return std::move(
    gl::glx::Window::new_pointer(options, m_display, *m_glx, m_wm_delete_window_atom, m_wm_protocols_atom));
}
#endif

#if ENGINE_HAS_VULKAN
auto
engine::display::xlib::Display::create_vulkan_window(
  engine::display::xlib::WindowOptions const& options) const& noexcept -> result::Result<vulkan::Window, Error>
{
  return std::move(vulkan::Window::new_(options, m_app_id, m_display, m_wm_delete_window_atom, m_wm_protocols_atom));
}

auto
engine::display::xlib::Display::create_vulkan_window_pointer(
  engine::display::xlib::WindowOptions const& options) const& noexcept -> result::Result<vulkan::Window*, Error>
{
  return std::move(
    vulkan::Window::new_pointer(options, m_app_id, m_display, m_wm_delete_window_atom, m_wm_protocols_atom));
}
#endif

auto
engine::display::xlib::Display::create_window_pointer(engine::display::WindowOptions const& options) const& noexcept
  -> result::Result<engine::display::Window*, engine::Error>
{
  try {
    auto const& xlib_options = dynamic_cast<WindowOptions const&>(options);
    return std::move(create_window_pointer(xlib_options));
  } catch (std::bad_cast const&) {
#if ENGINE_HAS_GLX
    auto opengl_interface = WindowOptions::OpenGLInterface::GLX;
#else
    auto opengl_interface = WindowOptions::OpenGLInterface::EGL;
#endif
    auto xlib_options = WindowOptions::new_();
    xlib_options.opengl_interface(opengl_interface)
      .graphics_api(options.graphics_api())
      .height(options.height())
      .title(options.title())
      .width(options.width());
    return std::move(create_window_pointer(xlib_options));
  }
}

auto
engine::display::xlib::Display::create_window_pointer(
  const engine::display::xlib::WindowOptions& options) const& noexcept -> result::Result<display::Window*, Error>
{
  switch (options.graphics_api()) {
    case WindowOptions::GraphicsAPI::OpenGL:
      switch (options.opengl_interface()) {
	case WindowOptions::OpenGLInterface::EGL:
#if ENGINE_HAS_EGL
	  if (auto result = create_egl_window_pointer(options); result.is_err()) {
	    return std::move(result::Result<display::Window*, Error>::Err(std::move(result).unwrap_err()));
	  } else {
	    return std::move(result::Result<display::Window*, Error>::Ok(std::move(result).unwrap()));
	  }
#else
	  return std::move(result::Result<display::Window*, Error>::Err(Error{ .source = 0, .code = -1 }));
#endif
	case WindowOptions::OpenGLInterface::GLX:
#if ENGINE_HAS_GLX
	  if (auto result = create_glx_window_pointer(options); result.is_err()) {
	    return std::move(result::Result<display::Window*, Error>::Err(std::move(result).unwrap_err()));
	  } else {
	    return std::move(result::Result<display::Window*, Error>::Ok(std::move(result).unwrap()));
	  }
#else
	  return std::move(result::Result<display::Window*, Error>::Err(Error::new_(0, -1)));
#endif
      }
      break;
    case WindowOptions::GraphicsAPI::Vulkan:
#if ENGINE_HAS_VULKAN
      if (auto result = create_vulkan_window_pointer(options); result.is_err()) {
	return std::move(result::Result<display::Window*, Error>::Err(std::move(result).unwrap_err()));
      } else {
	return std::move(result::Result<display::Window*, Error>::Ok(std::move(result).unwrap()));
      }
#else
      return std::move(result::Result<display::Window*, Error>::Err(Error::new_(0, -1)));
#endif
  }
}

auto
engine::display::xlib::Display::drop() & noexcept -> void
{
  m_running = false;
#if ENGINE_HAS_EGL
  m_egl_display.reset();
#endif
#if ENGINE_HAS_GLX
  m_glx.reset();
#endif
  if (m_display != nullptr) {
    XCloseDisplay(m_display);
    m_display = nullptr;
  }
}

auto
engine::display::xlib::Display::initialize(char const* display_name, DisplayOptions::Flags flags) & noexcept
  -> option::Option<engine::Error>
{
  XInitThreads();
  m_display = XOpenDisplay(display_name);
  if (m_display == nullptr) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  m_wm_delete_window_atom = XInternAtom(m_display, "WM_DELETE_WINDOW", True);
  m_wm_protocols_atom = XInternAtom(m_display, "WM_PROTOCOLS", True);

  if (flags.contains(DisplayOptions::Flags::USE_GLX())) {
    if (auto result = initialize_glx(); result.is_some()) {
      return std::move(result);
    }
  }

  if (flags.contains(DisplayOptions::Flags::USE_EGL())) {
    if (auto result = initialize_egl(true); result.is_some()) {
      return std::move(result);
    }
  }

  return std::move(option::Option<Error>::None());
}

#if ENGINE_HAS_GLX
auto
engine::display::xlib::Display::initialize_glx() & noexcept -> option::Option<engine::Error>
{
  auto glx_major = 0;
  auto glx_minor = 0;
  glXQueryVersion(m_display, &glx_major, &glx_minor);
  std::cout << "[engine::display::xlib][Display][initialize_glx]: GLX " << glx_major << '.' << glx_minor
	    << " initialized." << std::endl;

  auto attrib_list = static_cast<int>(XLIB_NONE);
  auto config_count = 0;
  auto configs = glXChooseFBConfig(m_display, DefaultScreen(m_display), &attrib_list, &config_count);
  if (configs == nullptr || config_count <= 0) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  if (auto result = graphics::gl::glx::Context::new_pointer(configs[0], m_display); result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  } else {
    m_glx = std::unique_ptr<graphics::gl::glx::Context>(std::move(result).unwrap());
  }
  XFree(configs);

  return std::move(option::Option<Error>::None());
}
#else
auto
engine::display::xlib::Display::initialize_glx() & noexcept -> option::Option<engine::Error>
{
  std::cerr << "[engine::display::xlib][Display][initialize_glx]: "
	       "GLX requested but excluded from the build. "
	       "Recompile with GLX support."
	    << std::endl;
  return std::move(option::Option<Error>::None());
}
#endif

#if ENGINE_HAS_EGL
auto
engine::display::xlib::Display::initialize_egl(bool pre_load) & noexcept -> option::Option<engine::Error>
{
  if (auto result = graphics::gl::egl::Display::new_pointer(m_display, graphics::gl::egl::Platform::Xlib, pre_load);
      result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  } else {
    m_egl_display = std::unique_ptr<graphics::gl::egl::Display>(std::move(result).unwrap());
  }
  return std::move(option::Option<Error>::None());
}
#else
auto
engine::display::xlib::Display::initialize_egl(bool pre_load) & noexcept -> option::Option<engine::Error>
{
  std::cerr << "[engine::display::xlib][Display][initialize_egl]: "
	       "EGL requested but excluded from the build. "
	       "Recompile with EGL support."
	    << std::endl;
  return std::move(option::Option<Error>::None());
}
#endif

auto
engine::display::xlib::Display::new_(const engine::display::xlib::DisplayOptions& options) noexcept
  -> result::Result<engine::display::xlib::Display, engine::Error>
{
  auto self = Display(options);
  if (auto result = self.initialize(options.display_name(), options.flags()); result.is_some()) {
    return std::move(result::Result<Display, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Display, Error>::Ok(std::move(self)));
}

auto
engine::display::xlib::Display::new_pointer(const engine::display::xlib::DisplayOptions& options) noexcept
  -> result::Result<engine::display::xlib::Display*, engine::Error>
{
  auto self = std::unique_ptr<Display>(new Display(options));
  if (auto result = self->initialize(options.display_name(), options.flags()); result.is_some()) {
    return std::move(result::Result<Display*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Display*, Error>::Ok(self.release()));
}

auto
engine::display::xlib::Display::run() & noexcept -> result::Result<bool, engine::Error>
{
  return result::Result<bool, Error>::Ok(m_running);
}

#include "engine/display/xlib/vulkan/window.hpp"

#if ENGINE_HAS_VULKAN
#include <engine/graphics/vulkan/graphics_context.hpp>

engine::display::xlib::vulkan::Window::Window(const engine::display::xlib::WindowOptions& options,
					      ::Display* display,
					      Atom wm_delete_window_atom,
					      Atom wm_protocols_atom) noexcept
  : xlib::Window(options, display, wm_delete_window_atom, wm_protocols_atom)
{}

engine::display::xlib::vulkan::Window::Window(engine::display::xlib::vulkan::Window&& other) noexcept
  : xlib::Window(std::move(other))
{}

engine::display::xlib::vulkan::Window::~Window() noexcept
{
  drop();
}

auto
engine::display::xlib::vulkan::Window::operator=(engine::display::xlib::vulkan::Window&& other) & noexcept
  -> engine::display::xlib::vulkan::Window&
{
  if (this == &other) {
    return *this;
  }

  drop();

  xlib::Window::operator=(std::move(other));
  return *this;
}

auto
engine::display::xlib::vulkan::Window::drop() & noexcept -> void
{
  m_graphics_context.reset();
}

auto
engine::display::xlib::vulkan::Window::initialize_vulkan(engine::CStr app_id, engine::CStr title) & noexcept
  -> option::Option<engine::Error>
{
  using namespace graphics::vulkan;

  auto visual_info_count = 0;
  auto visual_info_template = XVisualInfo{ .screen = DefaultScreen(m_display) };
  auto* visual_info = XGetVisualInfo(m_display, VisualScreenMask, &visual_info_template, &visual_info_count);
  if (visual_info == nullptr || visual_info_count == 0) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  if (auto result = initialize(title, *visual_info); result.is_some()) {
    XFree(visual_info);
    return std::move(result);
  }
  XFree(visual_info);

  auto* display = m_display;
  auto window = m_window;
  if (auto result = GraphicsContext::new_pointer(
	app_id,
	1,
	[=](auto vk_instance, auto* vk_surface_khr) -> VkResult {
	  auto vk_xlib_surface_create_info_khr =
	    VkXlibSurfaceCreateInfoKHR{ VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR, nullptr, 0, display, window };

	  return vkCreateXlibSurfaceKHR(vk_instance, &vk_xlib_surface_create_info_khr, nullptr, vk_surface_khr);
	},
	m_height,
	VK_KHR_XLIB_SURFACE_EXTENSION_NAME,
	m_width);
      result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  } else {
    m_graphics_context = std::unique_ptr<graphics::vulkan::GraphicsContext>(std::move(result).unwrap());
  }

  return std::move(option::Option<Error>::None());
}

auto
engine::display::xlib::vulkan::Window::new_(const engine::display::xlib::WindowOptions& options,
					    engine::CStr app_id,
					    ::Display* display,
					    Atom wm_delete_window_atom,
					    Atom wm_protocols_atom) noexcept -> result::Result<Window, Error>
{
  auto self = Window(options,
		     std::forward<::Display*>(display),
		     std::forward<Atom>(wm_delete_window_atom),
		     std::forward<Atom>(wm_protocols_atom));
  if (auto result = self.initialize_vulkan(app_id, options.title()); result.is_some()) {
    return std::move(result::Result<Window, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Window, Error>::Ok(std::move(self)));
}

auto
engine::display::xlib::vulkan::Window::new_pointer(const engine::display::xlib::WindowOptions& options,
						   engine::CStr app_id,
						   ::Display* display,
						   Atom wm_delete_window_atom,
						   Atom wm_protocols_atom) noexcept -> result::Result<Window*, Error>
{
  auto self = std::unique_ptr<Window>(new Window(options,
						 std::forward<::Display*>(display),
						 std::forward<Atom>(wm_delete_window_atom),
						 std::forward<Atom>(wm_protocols_atom)));
  if (auto result = self->initialize_vulkan(app_id, options.title()); result.is_some()) {
    return std::move(result::Result<Window*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Window*, Error>::Ok(self.release()));
}
#endif

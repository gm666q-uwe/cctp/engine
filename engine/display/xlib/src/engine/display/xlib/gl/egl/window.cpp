#include "engine/display/xlib/gl/egl/window.hpp"

#if ENGINE_HAS_EGL
#include <engine/graphics/gl/graphics_context.hpp>

engine::display::xlib::gl::egl::Window::Window(const engine::display::xlib::WindowOptions& options,
					       ::Display* display,
					       Atom wm_delete_window_atom,
					       Atom wm_protocols_atom) noexcept
  : xlib::Window(options, display, wm_delete_window_atom, wm_protocols_atom)
{}

engine::display::xlib::gl::egl::Window::Window(engine::display::xlib::gl::egl::Window&& other) noexcept
  : xlib::Window(std::move(other))
  , m_egl(std::move(other.m_egl))
{}

engine::display::xlib::gl::egl::Window::~Window() noexcept
{
  drop();
}

auto
engine::display::xlib::gl::egl::Window::operator=(engine::display::xlib::gl::egl::Window&& other) & noexcept
  -> engine::display::xlib::gl::egl::Window&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_egl = std::move(other.m_egl);

  xlib::Window::operator=(std::move(other));
  return *this;
}

auto
engine::display::xlib::gl::egl::Window::egl() const& noexcept -> engine::graphics::gl::egl::Context&
{
  return *m_egl;
}

auto
engine::display::xlib::gl::egl::Window::drop() & noexcept -> void
{
  m_graphics_context.reset();
  m_egl.reset();
}

auto
engine::display::xlib::gl::egl::Window::initialize_egl(const engine::graphics::gl::egl::Display& egl_display,
						       engine::CStr title) & noexcept -> option::Option<engine::Error>
{
  using namespace graphics::gl::egl;

  auto* egl_config = static_cast<EGLConfig>(nullptr);
  if (auto result = egl_display.choose_config(); result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  } else {
    egl_config = std::move(result).unwrap();
  }

  auto visual_id = static_cast<VisualID>(XLIB_NONE);
  if (auto result = egl_display.get_config_attribute(egl_config, EGL_NATIVE_VISUAL_ID); result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  } else {
    visual_id = std::move(result).unwrap();
  }

  auto visual_info_count = 0;
  auto visual_info_template = XVisualInfo{ .visualid = visual_id };
  auto* visual_info = XGetVisualInfo(m_display, VisualIDMask, &visual_info_template, &visual_info_count);
  if (visual_info == nullptr || visual_info_count != 1) {
    return std::move(option::Option<Error>::Some(Error::new_(0, -1)));
  }

  if (auto result = initialize(title, *visual_info); result.is_some()) {
    XFree(visual_info);
    return std::move(result);
  }
  XFree(visual_info);

  if (auto result = egl_display.create_context_pointer(egl_config, &m_window, false); result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  } else {
    m_egl = std::unique_ptr<graphics::gl::egl::Context>(std::move(result).unwrap());
  }

  if (auto result = engine::graphics::gl::GraphicsContext::new_pointer(m_height, *m_egl, m_width); result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  } else {
    m_graphics_context = std::unique_ptr<graphics::gl::GraphicsContext>(std::move(result).unwrap());
  }

  return std::move(option::Option<Error>::None());
}

auto
engine::display::xlib::gl::egl::Window::new_(const engine::display::xlib::WindowOptions& options,
					     ::Display* display,
					     const engine::graphics::gl::egl::Display& egl_display,
					     Atom wm_delete_window_atom,
					     Atom wm_protocols_atom) noexcept -> result::Result<Window, Error>
{
  auto self = Window(options,
		     std::forward<::Display*>(display),
		     std::forward<Atom>(wm_delete_window_atom),
		     std::forward<Atom>(wm_protocols_atom));
  if (auto result = self.initialize_egl(egl_display, options.title()); result.is_some()) {
    return std::move(result::Result<Window, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Window, Error>::Ok(std::move(self)));
}

auto
engine::display::xlib::gl::egl::Window::new_pointer(const engine::display::xlib::WindowOptions& options,
						    ::Display* display,
						    const engine::graphics::gl::egl::Display& egl_display,
						    Atom wm_delete_window_atom,
						    Atom wm_protocols_atom) noexcept -> result::Result<Window*, Error>
{
  auto self = std::unique_ptr<Window>(new Window(options,
						 std::forward<::Display*>(display),
						 std::forward<Atom>(wm_delete_window_atom),
						 std::forward<Atom>(wm_protocols_atom)));
  if (auto result = self->initialize_egl(egl_display, options.title()); result.is_some()) {
    return std::move(result::Result<Window*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Window*, Error>::Ok(self.release()));
}
#endif

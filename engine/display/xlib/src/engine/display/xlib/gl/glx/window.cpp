#include "engine/display/xlib/gl/glx/window.hpp"

#if ENGINE_HAS_GLX
#include <engine/graphics/gl/graphics_context.hpp>

engine::display::xlib::gl::glx::Window::Window(const engine::display::xlib::WindowOptions& options,
					       ::Display* display,
					       Atom wm_delete_window_atom,
					       Atom wm_protocols_atom) noexcept
  : xlib::Window(options, display, wm_delete_window_atom, wm_protocols_atom)
{}

engine::display::xlib::gl::glx::Window::Window(engine::display::xlib::gl::glx::Window&& other) noexcept
  : xlib::Window(std::move(other))
  , m_glx(std::move(other.m_glx))
{}

engine::display::xlib::gl::glx::Window::~Window() noexcept
{
  drop();
}

auto
engine::display::xlib::gl::glx::Window::operator=(engine::display::xlib::gl::glx::Window&& other) & noexcept
  -> engine::display::xlib::gl::glx::Window&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_glx = std::move(other.m_glx);

  xlib::Window::operator=(std::move(other));
  return *this;
}

auto
engine::display::xlib::gl::glx::Window::glx() const& noexcept -> engine::graphics::gl::glx::Context&
{
  return *m_glx;
}

auto
engine::display::xlib::gl::glx::Window::drop() & noexcept -> void
{
  m_graphics_context.reset();
  m_glx.reset();
}

auto
engine::display::xlib::gl::glx::Window::initialize_glx(const engine::graphics::gl::glx::Context& glx,
						       engine::CStr title) & noexcept -> option::Option<engine::Error>
{
  using namespace graphics::gl::glx;

  auto* fb_config = static_cast<GLXFBConfig>(nullptr);
  if (auto result = Context::choose_fb_config(m_display); result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  } else {
    fb_config = std::move(result).unwrap();
  }

  auto visual_info = glXGetVisualFromFBConfig(m_display, fb_config);
  if (auto result = initialize(title, *visual_info); result.is_some()) {
    XFree(visual_info);
    return std::move(result);
  }
  XFree(visual_info);

  if (auto result = Context::with_drawable_pointer(fb_config, glx, m_window); result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  } else {
    m_glx = std::unique_ptr<graphics::gl::glx::Context>(std::move(result).unwrap());
  }

  if (auto result = engine::graphics::gl::GraphicsContext::new_pointer(m_height, *m_glx, m_width); result.is_err()) {
    return std::move(option::Option<Error>::Some(std::move(result).unwrap_err()));
  } else {
    m_graphics_context = std::unique_ptr<graphics::gl::GraphicsContext>(std::move(result).unwrap());
  }

  return std::move(option::Option<Error>::None());
}

auto
engine::display::xlib::gl::glx::Window::new_(const engine::display::xlib::WindowOptions& options,
					     ::Display* display,
					     engine::graphics::gl::glx::Context const& glx,
					     Atom wm_delete_window_atom,
					     Atom wm_protocols_atom) noexcept -> result::Result<Window, Error>
{
  auto self = Window(options,
		     std::forward<::Display*>(display),
		     std::forward<Atom>(wm_delete_window_atom),
		     std::forward<Atom>(wm_protocols_atom));
  if (auto result = self.initialize_glx(glx, options.title()); result.is_some()) {
    return std::move(result::Result<Window, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Window, Error>::Ok(std::move(self)));
}

auto
engine::display::xlib::gl::glx::Window::new_pointer(const engine::display::xlib::WindowOptions& options,
						    ::Display* display,
						    engine::graphics::gl::glx::Context const& glx,
						    Atom wm_delete_window_atom,
						    Atom wm_protocols_atom) noexcept -> result::Result<Window*, Error>
{
  auto self = std::unique_ptr<Window>(new Window(options,
						 std::forward<::Display*>(display),
						 std::forward<Atom>(wm_delete_window_atom),
						 std::forward<Atom>(wm_protocols_atom)));
  if (auto result = self->initialize_glx(glx, options.title()); result.is_some()) {
    return std::move(result::Result<Window*, Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Window*, Error>::Ok(self.release()));
}
#endif

#include "engine/display/xlib/display_options.hpp"

#if ENGINE_HAS_XLIB
engine::display::xlib::DisplayOptions::DisplayOptions(engine::display::xlib::DisplayOptions&& other) noexcept
  : display::DisplayOptions(std::move(other))
  , m_display_name(other.m_display_name)
  , m_flags(other.m_flags)
{}

auto
engine::display::xlib::DisplayOptions::operator=(engine::display::xlib::DisplayOptions&& other) & noexcept
  -> engine::display::xlib::DisplayOptions&
{
  if (this == &display::DisplayOptions::operator=(std::move(other))) {
    return *this;
  }

  m_display_name = other.m_display_name;
  m_flags = other.m_flags;

  return *this;
}

auto
engine::display::xlib::DisplayOptions::display_name(char const* display_name) && noexcept
  -> engine::display::xlib::DisplayOptions
{
  m_display_name = display_name;

  return std::move(*this);
}

auto
engine::display::xlib::DisplayOptions::flags(engine::display::xlib::DisplayOptions::Flags flags) && noexcept
  -> engine::display::xlib::DisplayOptions
{
  m_flags = flags;

  return std::move(*this);
}

auto
engine::display::xlib::DisplayOptions::new_() noexcept -> engine::display::xlib::DisplayOptions
{
  return std::move(DisplayOptions());
}

auto
engine::display::xlib::DisplayOptions::new_pointer() noexcept -> engine::display::xlib::DisplayOptions*
{
  return new DisplayOptions();
}
#endif

#include "engine/display/xlib/window_options.hpp"

#if ENGINE_HAS_XLIB
engine::display::xlib::WindowOptions::WindowOptions(engine::display::xlib::WindowOptions&& other) noexcept
  : display::WindowOptions(std::move(other))
  , m_opengl_interface(other.m_opengl_interface)
{}

auto
engine::display::xlib::WindowOptions::operator=(engine::display::xlib::WindowOptions&& other) & noexcept
  -> engine::display::xlib::WindowOptions&
{
  if (this == &display::WindowOptions::operator=(std::move(other))) {
    return *this;
  }

  m_opengl_interface = other.m_opengl_interface;

  return *this;
}

auto
engine::display::xlib::WindowOptions::new_() noexcept -> engine::display::xlib::WindowOptions
{
  return std::move(WindowOptions());
}

auto
engine::display::xlib::WindowOptions::new_pointer() noexcept -> engine::display::xlib::WindowOptions*
{
  return new WindowOptions();
}

auto
engine::display::xlib::WindowOptions::opengl_interface(
  engine::display::xlib::WindowOptions::OpenGLInterface opengl_interface) && noexcept
  -> engine::display::xlib::WindowOptions
{
  m_opengl_interface = opengl_interface;

  return std::move(*this);
}
#endif

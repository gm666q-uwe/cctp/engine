#include "engine/display/xlib/window.hpp"

#include <iostream>

engine::display::xlib::Window::Window(WindowOptions const& options,
				      ::Display* display,
				      Atom wm_delete_window_atom,
				      Atom wm_protocols_atom) noexcept
  : display::Window(options)
  , m_display(display)
  , m_input_manager(input::Manager::new_pointer())
  , m_wm_delete_window_atom(wm_delete_window_atom)
  , m_wm_protocols_atom(wm_protocols_atom)
{}

engine::display::xlib::Window::Window(engine::display::xlib::Window&& other) noexcept
  : display::Window(std::move(other))
  , m_display(other.m_display)
  , m_graphics_context(std::move(other.m_graphics_context))
  , m_running(std::exchange(other.m_running, false))
  , m_window(std::exchange(other.m_window, XLIB_NONE))
  , m_wm_delete_window_atom(other.m_wm_delete_window_atom)
  , m_wm_protocols_atom(other.m_wm_protocols_atom)
{}

engine::display::xlib::Window::~Window() noexcept
{
  drop();
}

auto
engine::display::xlib::Window::operator=(engine::display::xlib::Window&& other) & noexcept
  -> engine::display::xlib::Window&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_display = other.m_display;
  m_graphics_context = std::move(other.m_graphics_context);
  m_running = std::exchange(other.m_running, m_running);
  m_window = std::exchange(other.m_window, m_window);
  m_wm_delete_window_atom = other.m_wm_delete_window_atom;
  m_wm_protocols_atom = other.m_wm_protocols_atom;

  display::Window::operator=(std::move(other));
  return *this;
}

auto
engine::display::xlib::Window::drop() & noexcept -> void
{
  m_running = false;

  if (m_window != XLIB_NONE) {
    XDestroyWindow(m_display, m_window);
    m_window = XLIB_NONE;
  }
}

auto
engine::display::xlib::Window::graphics_context() const& noexcept -> engine::graphics::GraphicsContext&
{
  return *m_graphics_context;
}

auto
engine::display::xlib::Window::initialize(engine::CStr title, XVisualInfo const& visual_info) & noexcept
  -> option::Option<Error>
{
  auto root_window = RootWindow(m_display, visual_info.screen);
  auto window_attributes =
    XSetWindowAttributes{ .event_mask = M_EVENT_MASK,
			  .colormap = XCreateColormap(m_display, root_window, visual_info.visual, AllocNone) };

  m_window = XCreateWindow(m_display,
			   root_window,
			   0,
			   0,
			   m_width,
			   m_height,
			   0,
			   visual_info.depth,
			   InputOutput,
			   visual_info.visual,
			   CWColormap | CWEventMask,
			   &window_attributes);

  XStoreName(m_display, m_window, title.data());
  XSetWMProtocols(m_display, m_window, &m_wm_delete_window_atom, 1);

  XMapWindow(m_display, m_window);
  // XFlush(m_display);

  return std::move(option::Option<Error>::None());
}

auto
engine::display::xlib::Window::input_manager() const& noexcept -> engine::input::Manager&
{
  return *m_input_manager;
}

auto
engine::display::xlib::Window::run() & noexcept -> result::Result<bool, engine::Error>
{
  auto event = XEvent{};
  while (XCheckIfEvent(
	   m_display,
	   &event,
	   [](auto* display, auto* event, auto arg) -> auto {
	     return event->xany.window == *reinterpret_cast<::Window*>(arg) ? True : False;
	   },
	   reinterpret_cast<XPointer>(&m_window)) == True) {
    switch (event.type) {
      case ButtonPress:
	m_input_manager->mouse().process_button_pass(event.xbutton);
	break;
      case ButtonRelease:
	m_input_manager->mouse().process_button_release(event.xbutton);
	break;
      case ClientMessage:
	if (event.xclient.message_type == m_wm_protocols_atom) {
	  if (event.xclient.data.l[0] == m_wm_delete_window_atom) {
	    m_running = false;
	  }
	} else {
	  std::cout << "Unknown client message type: " << event.xclient.message_type << std::endl;
	}
	break;
      case ConfigureNotify:
	if (auto height = event.xconfigure.height, width = event.xconfigure.width;
	    height != m_height || width != m_width) {
	  m_height = height;
	  m_width = width;
	  if (m_height != 0 && m_width != 0) {
	    m_graphics_context->on_resize(m_height, m_width);
	  }
	}
	break;
      case KeyPress:
	m_input_manager->keyboard().process_key_press(event.xkey);
	break;
      case KeyRelease:
	m_input_manager->keyboard().process_key_release(event.xkey);
	break;
      case MotionNotify:
	m_input_manager->mouse().process_motion_notify(event.xmotion);
	break;
      default:
	std::cout << "Unknown event type: " << event.type << std::endl;
	break;
    }
  }
  return std::move(result::Result<bool, Error>::Ok(m_running));
}

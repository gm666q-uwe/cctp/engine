# Engine

## Dependencies

- Boost
- CMake 3.19+
- ECM 5.74.0+ (Wayland support)
- EGL 1.5 (EGL support)
- glm
- glslc
- GLX 1.4 (GLX support)
- OpenGL 4.6 (OpenGL support)
- Vulkan 1.2 (Vulkan support)
- Wayland (Wayland support)
- WaylandProtocols 1.15+ (Wayland support)
- WaylandScanner (Wayland support)
- xkbcommon (Wayland/Xlib support)
- Xlib (Xlib support)

## Tested Compilers

- x86_64-unknown-linux-gnu Clang 12.0.1
- x86_64-unknown-linux-gnu GCC 11.1.0


## Project structure

- engine/common - Common headers
- engine/core - Main engine project
- engine/display - Display backends
- engine/graphics - Graphics backends
- engine/input - Common input library
- engine/sandbox - Test application showcasing the engine

## Sandbox Controls

- Up - look up
- Down - look down
- Left - look left
- Right - look right
- W - move forward
- S - move back
- A - move left
- D - move right
- Shift - move up
- Ctrl - move down
- Esc - quit
